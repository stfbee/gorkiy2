package p;

import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: Segment.kt */
public final class Segment {
    public final byte[] a;
    public int b;
    public int c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f3070e;

    /* renamed from: f  reason: collision with root package name */
    public Segment f3071f;
    public Segment g;

    public Segment() {
        this.a = new byte[8192];
        this.f3070e = true;
        this.d = false;
    }

    public final Segment a() {
        Segment segment = this.f3071f;
        if (segment == this) {
            segment = null;
        }
        Segment segment2 = this.g;
        if (segment2 != null) {
            segment2.f3071f = this.f3071f;
            Segment segment3 = this.f3071f;
            if (segment3 != null) {
                segment3.g = segment2;
                this.f3071f = null;
                this.g = null;
                return segment;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a();
        throw null;
    }

    public final Segment b() {
        this.d = true;
        return new Segment(this.a, this.b, this.c, true, false);
    }

    public Segment(byte[] bArr, int i2, int i3, boolean z, boolean z2) {
        if (bArr != null) {
            this.a = bArr;
            this.b = i2;
            this.c = i3;
            this.d = z;
            this.f3070e = z2;
            return;
        }
        Intrinsics.a("data");
        throw null;
    }

    public final Segment a(Segment segment) {
        if (segment != null) {
            segment.g = this;
            segment.f3071f = this.f3071f;
            Segment segment2 = this.f3071f;
            if (segment2 != null) {
                segment2.g = segment;
                this.f3071f = segment;
                return segment;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("segment");
        throw null;
    }

    public final void a(Segment segment, int i2) {
        if (segment == null) {
            Intrinsics.a("sink");
            throw null;
        } else if (segment.f3070e) {
            int i3 = segment.c;
            if (i3 + i2 > 8192) {
                if (!segment.d) {
                    int i4 = segment.b;
                    if ((i3 + i2) - i4 <= 8192) {
                        byte[] bArr = segment.a;
                        Collections.b(bArr, i4, bArr, 0, i3 - i4);
                        segment.c -= segment.b;
                        segment.b = 0;
                    } else {
                        throw new IllegalArgumentException();
                    }
                } else {
                    throw new IllegalArgumentException();
                }
            }
            Collections.b(this.a, this.b, segment.a, segment.c, i2);
            segment.c += i2;
            this.b += i2;
        } else {
            throw new IllegalStateException("only owner can write".toString());
        }
    }
}
