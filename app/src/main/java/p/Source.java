package p;

import java.io.Closeable;

/* compiled from: Source.kt */
public interface Source extends Closeable {
    long b(Buffer buffer, long j2);

    Timeout b();

    void close();
}
