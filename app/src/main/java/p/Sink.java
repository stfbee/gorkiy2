package p;

import java.io.Closeable;
import java.io.Flushable;

/* compiled from: Sink.kt */
public interface Sink extends Closeable, Flushable {
    void a(Buffer buffer, long j2);

    Timeout b();

    void close();

    void flush();
}
