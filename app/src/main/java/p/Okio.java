package p;

import n.n.c.Intrinsics;

/* compiled from: Okio.kt */
public final class Okio implements Sink {
    public void a(Buffer buffer, long j2) {
        if (buffer != null) {
            buffer.skip(j2);
        } else {
            Intrinsics.a("source");
            throw null;
        }
    }

    public Timeout b() {
        return Timeout.d;
    }

    public void close() {
    }

    public void flush() {
    }
}
