package p;

import java.io.InputStream;
import java.nio.channels.ReadableByteChannel;
import java.nio.charset.Charset;

/* compiled from: BufferedSource.kt */
public interface BufferedSource extends Source, ReadableByteChannel {
    int a(Options options);

    long a(Sink sink);

    String a(Charset charset);

    ByteString b(long j2);

    String c(long j2);

    long d();

    boolean d(long j2);

    String e();

    void g(long j2);

    Buffer getBuffer();

    byte[] i(long j2);

    boolean j();

    long k();

    InputStream m();

    byte readByte();

    int readInt();

    short readShort();

    void skip(long j2);
}
