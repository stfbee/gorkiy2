package p;

import java.nio.channels.WritableByteChannel;

/* compiled from: BufferedSink.kt */
public interface BufferedSink extends Sink, WritableByteChannel {
    BufferedSink a(long j2);

    BufferedSink a(String str);

    BufferedSink a(ByteString byteString);

    BufferedSink c();

    void flush();

    Buffer getBuffer();

    BufferedSink h(long j2);

    BufferedSink write(byte[] bArr);

    BufferedSink write(byte[] bArr, int i2, int i3);

    BufferedSink writeByte(int i2);

    BufferedSink writeInt(int i2);

    BufferedSink writeShort(int i2);
}
