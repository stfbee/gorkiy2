package p;

import j.a.a.a.outline;
import java.nio.ByteBuffer;
import n.n.c.Intrinsics;

/* compiled from: RealBufferedSink.kt */
public final class RealBufferedSink implements BufferedSink {
    public final Buffer b;
    public boolean c;
    public final Sink d;

    public RealBufferedSink(Sink sink) {
        if (sink != null) {
            this.d = sink;
            this.b = new Buffer();
            return;
        }
        Intrinsics.a("sink");
        throw null;
    }

    public void a(Buffer buffer, long j2) {
        if (buffer == null) {
            Intrinsics.a("source");
            throw null;
        } else if (!this.c) {
            this.b.a(buffer, j2);
            c();
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public Timeout b() {
        return this.d.b();
    }

    public BufferedSink c() {
        if (!this.c) {
            long i2 = this.b.i();
            if (i2 > 0) {
                this.d.a(this.b, i2);
            }
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public void close() {
        if (!this.c) {
            Throwable th = null;
            try {
                if (this.b.c > 0) {
                    this.d.a(this.b, this.b.c);
                }
            } catch (Throwable th2) {
                th = th2;
            }
            try {
                this.d.close();
            } catch (Throwable th3) {
                if (th == null) {
                    th = th3;
                }
            }
            this.c = true;
            if (th != null) {
                throw th;
            }
        }
    }

    public void flush() {
        if (!this.c) {
            Buffer buffer = this.b;
            long j2 = buffer.c;
            if (j2 > 0) {
                this.d.a(buffer, j2);
            }
            this.d.flush();
            return;
        }
        throw new IllegalStateException("closed".toString());
    }

    public Buffer getBuffer() {
        return this.b;
    }

    public BufferedSink h(long j2) {
        if (!this.c) {
            this.b.h(j2);
            c();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public boolean isOpen() {
        return !this.c;
    }

    public String toString() {
        StringBuilder a = outline.a("buffer(");
        a.append(this.d);
        a.append(')');
        return a.toString();
    }

    public BufferedSink write(byte[] bArr) {
        if (bArr == null) {
            Intrinsics.a("source");
            throw null;
        } else if (!this.c) {
            this.b.write(bArr);
            c();
            return this;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public BufferedSink writeByte(int i2) {
        if (!this.c) {
            this.b.writeByte(i2);
            c();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink writeInt(int i2) {
        if (!this.c) {
            this.b.writeInt(i2);
            return c();
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink writeShort(int i2) {
        if (!this.c) {
            this.b.writeShort(i2);
            c();
            return this;
        }
        throw new IllegalStateException("closed".toString());
    }

    public BufferedSink a(ByteString byteString) {
        if (byteString == null) {
            Intrinsics.a("byteString");
            throw null;
        } else if (!this.c) {
            this.b.a(byteString);
            c();
            return this;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: p.Buffer.write(byte[], int, int):p.Buffer
     arg types: [byte[], int, int]
     candidates:
      p.Buffer.write(byte[], int, int):p.BufferedSink
      p.BufferedSink.write(byte[], int, int):p.BufferedSink
      p.Buffer.write(byte[], int, int):p.Buffer */
    public BufferedSink write(byte[] bArr, int i2, int i3) {
        if (bArr == null) {
            Intrinsics.a("source");
            throw null;
        } else if (!this.c) {
            this.b.write(bArr, i2, i3);
            c();
            return this;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public BufferedSink a(String str) {
        if (str == null) {
            Intrinsics.a("string");
            throw null;
        } else if (!this.c) {
            this.b.a(str);
            return c();
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public int write(ByteBuffer byteBuffer) {
        if (byteBuffer == null) {
            Intrinsics.a("source");
            throw null;
        } else if (!this.c) {
            int write = this.b.write(byteBuffer);
            c();
            return write;
        } else {
            throw new IllegalStateException("closed".toString());
        }
    }

    public BufferedSink a(long j2) {
        if (!this.c) {
            this.b.a(j2);
            return c();
        }
        throw new IllegalStateException("closed".toString());
    }
}
