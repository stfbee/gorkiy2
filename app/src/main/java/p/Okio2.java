package p;

import j.a.a.a.outline;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.logging.Level;
import java.util.logging.Logger;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: Okio.kt */
public final class Okio2 extends AsyncTimeout {

    /* renamed from: l  reason: collision with root package name */
    public final Logger f3068l;

    /* renamed from: m  reason: collision with root package name */
    public final Socket f3069m;

    public Okio2(Socket socket) {
        if (socket != null) {
            this.f3069m = socket;
            this.f3068l = Logger.getLogger("okio.Okio");
            return;
        }
        Intrinsics.a("socket");
        throw null;
    }

    public IOException b(IOException iOException) {
        SocketTimeoutException socketTimeoutException = new SocketTimeoutException("timeout");
        if (iOException != null) {
            socketTimeoutException.initCause(iOException);
        }
        return socketTimeoutException;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.AssertionError]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void}
     arg types: [java.util.logging.Level, java.lang.String, java.lang.Exception]
     candidates:
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.Throwable, java.util.function.Supplier<java.lang.String>):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object[]):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Object):void}
      ClspMth{java.util.logging.Logger.log(java.util.logging.Level, java.lang.String, java.lang.Throwable):void} */
    public void h() {
        try {
            this.f3069m.close();
        } catch (Exception e2) {
            Logger logger = this.f3068l;
            Level level = Level.WARNING;
            StringBuilder a = outline.a("Failed to close timed out socket ");
            a.append(this.f3069m);
            logger.log(level, a.toString(), (Throwable) e2);
        } catch (AssertionError e3) {
            if (Collections.a(e3)) {
                Logger logger2 = this.f3068l;
                Level level2 = Level.WARNING;
                StringBuilder a2 = outline.a("Failed to close timed out socket ");
                a2.append(this.f3069m);
                logger2.log(level2, a2.toString(), (Throwable) e3);
                return;
            }
            throw e3;
        }
    }
}
