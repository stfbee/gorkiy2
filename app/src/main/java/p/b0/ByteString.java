package p.b0;

import j.a.a.a.outline;
import java.util.Arrays;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Charsets;
import p.ByteString;
import p.a;

/* compiled from: ByteString.kt */
public final class ByteString {
    public static final char[] a = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f'};
    public static final p.ByteString b = a(new byte[0]);

    static {
        ByteString.a aVar = p.ByteString.f3063f;
    }

    public static final String a(p.ByteString byteString) {
        if (byteString != null) {
            return a.a(byteString.d, null, 1);
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final int b(p.ByteString byteString) {
        if (byteString != null) {
            return byteString.d.length;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final int c(p.ByteString byteString) {
        if (byteString != null) {
            int i2 = byteString.b;
            if (i2 != 0) {
                return i2;
            }
            int hashCode = Arrays.hashCode(byteString.d);
            byteString.b = hashCode;
            return hashCode;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final String d(p.ByteString byteString) {
        if (byteString != null) {
            byte[] bArr = byteString.d;
            char[] cArr = new char[(bArr.length * 2)];
            int i2 = 0;
            for (byte b2 : bArr) {
                int i3 = i2 + 1;
                char[] cArr2 = a;
                cArr[i2] = cArr2[(b2 >> 4) & 15];
                i2 = i3 + 1;
                cArr[i3] = cArr2[b2 & 15];
            }
            return new String(cArr);
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final byte[] e(p.ByteString byteString) {
        if (byteString != null) {
            return byteString.d;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final p.ByteString f(p.ByteString byteString) {
        byte b2;
        if (byteString != null) {
            int i2 = 0;
            while (true) {
                byte[] bArr = byteString.d;
                if (i2 >= bArr.length) {
                    return byteString;
                }
                byte b3 = bArr[i2];
                byte b4 = (byte) 65;
                if (b3 < b4 || b3 > (b2 = (byte) 90)) {
                    i2++;
                } else {
                    byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
                    Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
                    copyOf[i2] = (byte) (b3 + 32);
                    for (int i3 = i2 + 1; i3 < copyOf.length; i3++) {
                        byte b5 = copyOf[i3];
                        if (b5 >= b4 && b5 <= b2) {
                            copyOf[i3] = (byte) (b5 + 32);
                        }
                    }
                    return new p.ByteString(copyOf);
                }
            }
        } else {
            Intrinsics.a("$receiver");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String */
    /* JADX WARNING: Code restructure failed: missing block: B:100:0x011a, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:103:0x0123, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:129:0x0161, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:135:0x0174, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0185, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:147:0x0194, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:150:0x01aa, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:153:0x01b2, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:156:0x01b9, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:179:0x01ee, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:180:0x01f1, code lost:
        r4 = -1;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:0x008c, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x009d, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00a8, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:85:0x00e6, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x00f9, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:97:0x0108, code lost:
        if (r5 == 64) goto L_0x01f2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String g(p.ByteString r16) {
        /*
            r0 = r16
            if (r0 == 0) goto L_0x02d4
            byte[] r1 = r0.d
            int r1 = r1.length
            if (r1 != 0) goto L_0x000b
            r1 = 1
            goto L_0x000c
        L_0x000b:
            r1 = 0
        L_0x000c:
            if (r1 == 0) goto L_0x0011
            java.lang.String r0 = "[size=0]"
            return r0
        L_0x0011:
            byte[] r1 = r0.d
            int r2 = r1.length
            r3 = 0
            r4 = 0
            r5 = 0
        L_0x0017:
            r6 = 64
            if (r3 >= r2) goto L_0x01f2
            byte r7 = r1[r3]
            r8 = 13
            r9 = 10
            r10 = 31
            r11 = 65533(0xfffd, float:9.1831E-41)
            r12 = 159(0x9f, float:2.23E-43)
            r13 = 127(0x7f, float:1.78E-43)
            r14 = 65536(0x10000, float:9.18355E-41)
            if (r7 < 0) goto L_0x0081
            int r15 = r5 + 1
            if (r5 != r6) goto L_0x0034
            goto L_0x01f2
        L_0x0034:
            if (r7 == r9) goto L_0x0046
            if (r7 == r8) goto L_0x0046
            if (r7 < 0) goto L_0x003c
            if (r10 >= r7) goto L_0x0041
        L_0x003c:
            if (r13 <= r7) goto L_0x003f
            goto L_0x0043
        L_0x003f:
            if (r12 < r7) goto L_0x0043
        L_0x0041:
            r5 = 1
            goto L_0x0044
        L_0x0043:
            r5 = 0
        L_0x0044:
            if (r5 != 0) goto L_0x01f1
        L_0x0046:
            if (r7 != r11) goto L_0x004a
            goto L_0x01f1
        L_0x004a:
            if (r7 >= r14) goto L_0x004e
            r5 = 1
            goto L_0x004f
        L_0x004e:
            r5 = 2
        L_0x004f:
            int r4 = r4 + r5
            int r3 = r3 + 1
        L_0x0052:
            r5 = r15
            if (r3 >= r2) goto L_0x0017
            byte r7 = r1[r3]
            if (r7 < 0) goto L_0x0017
            int r7 = r3 + 1
            byte r3 = r1[r3]
            int r15 = r5 + 1
            if (r5 != r6) goto L_0x0063
            goto L_0x01f2
        L_0x0063:
            if (r3 == r9) goto L_0x0075
            if (r3 == r8) goto L_0x0075
            if (r3 < 0) goto L_0x006b
            if (r10 >= r3) goto L_0x0070
        L_0x006b:
            if (r13 <= r3) goto L_0x006e
            goto L_0x0072
        L_0x006e:
            if (r12 < r3) goto L_0x0072
        L_0x0070:
            r5 = 1
            goto L_0x0073
        L_0x0072:
            r5 = 0
        L_0x0073:
            if (r5 != 0) goto L_0x01f1
        L_0x0075:
            if (r3 != r11) goto L_0x0079
            goto L_0x01f1
        L_0x0079:
            if (r3 >= r14) goto L_0x007d
            r3 = 1
            goto L_0x007e
        L_0x007d:
            r3 = 2
        L_0x007e:
            int r4 = r4 + r3
            r3 = r7
            goto L_0x0052
        L_0x0081:
            int r11 = r7 >> 5
            r14 = -2
            r15 = 128(0x80, float:1.794E-43)
            if (r11 != r14) goto L_0x00d8
            int r7 = r3 + 1
            if (r2 > r7) goto L_0x0090
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x0090:
            byte r11 = r1[r3]
            byte r7 = r1[r7]
            r14 = r7 & 192(0xc0, float:2.69E-43)
            if (r14 != r15) goto L_0x009a
            r14 = 1
            goto L_0x009b
        L_0x009a:
            r14 = 0
        L_0x009b:
            if (r14 != 0) goto L_0x00a1
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x00a1:
            r7 = r7 ^ 3968(0xf80, float:5.56E-42)
            int r11 = r11 << 6
            r7 = r7 ^ r11
            if (r7 >= r15) goto L_0x00ac
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x00ac:
            int r11 = r5 + 1
            if (r5 != r6) goto L_0x00b2
            goto L_0x01f2
        L_0x00b2:
            if (r7 == r9) goto L_0x00c4
            if (r7 == r8) goto L_0x00c4
            if (r7 < 0) goto L_0x00ba
            if (r10 >= r7) goto L_0x00bf
        L_0x00ba:
            if (r13 <= r7) goto L_0x00bd
            goto L_0x00c1
        L_0x00bd:
            if (r12 < r7) goto L_0x00c1
        L_0x00bf:
            r5 = 1
            goto L_0x00c2
        L_0x00c1:
            r5 = 0
        L_0x00c2:
            if (r5 != 0) goto L_0x01f1
        L_0x00c4:
            r5 = 65533(0xfffd, float:9.1831E-41)
            if (r7 != r5) goto L_0x00cb
            goto L_0x01f1
        L_0x00cb:
            r5 = 65536(0x10000, float:9.18355E-41)
            if (r7 >= r5) goto L_0x00d1
            r15 = 1
            goto L_0x00d2
        L_0x00d1:
            r15 = 2
        L_0x00d2:
            int r4 = r4 + r15
            int r3 = r3 + 2
            r5 = r11
            goto L_0x0017
        L_0x00d8:
            int r10 = r7 >> 4
            r11 = 55296(0xd800, float:7.7486E-41)
            r12 = 57343(0xdfff, float:8.0355E-41)
            if (r10 != r14) goto L_0x0159
            int r7 = r3 + 2
            if (r2 > r7) goto L_0x00ea
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x00ea:
            byte r10 = r1[r3]
            int r13 = r3 + 1
            byte r13 = r1[r13]
            r14 = r13 & 192(0xc0, float:2.69E-43)
            if (r14 != r15) goto L_0x00f6
            r14 = 1
            goto L_0x00f7
        L_0x00f6:
            r14 = 0
        L_0x00f7:
            if (r14 != 0) goto L_0x00fd
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x00fd:
            byte r7 = r1[r7]
            r14 = r7 & 192(0xc0, float:2.69E-43)
            if (r14 != r15) goto L_0x0105
            r14 = 1
            goto L_0x0106
        L_0x0105:
            r14 = 0
        L_0x0106:
            if (r14 != 0) goto L_0x010c
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x010c:
            r14 = -123008(0xfffffffffffe1f80, float:NaN)
            r7 = r7 ^ r14
            int r13 = r13 << 6
            r7 = r7 ^ r13
            int r10 = r10 << 12
            r7 = r7 ^ r10
            r10 = 2048(0x800, float:2.87E-42)
            if (r7 >= r10) goto L_0x011e
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x011e:
            if (r11 <= r7) goto L_0x0121
            goto L_0x0127
        L_0x0121:
            if (r12 < r7) goto L_0x0127
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x0127:
            int r10 = r5 + 1
            if (r5 != r6) goto L_0x012d
            goto L_0x01f2
        L_0x012d:
            if (r7 == r9) goto L_0x0145
            if (r7 == r8) goto L_0x0145
            if (r7 < 0) goto L_0x0137
            r5 = 31
            if (r5 >= r7) goto L_0x0140
        L_0x0137:
            r5 = 127(0x7f, float:1.78E-43)
            if (r5 <= r7) goto L_0x013c
            goto L_0x0142
        L_0x013c:
            r5 = 159(0x9f, float:2.23E-43)
            if (r5 < r7) goto L_0x0142
        L_0x0140:
            r5 = 1
            goto L_0x0143
        L_0x0142:
            r5 = 0
        L_0x0143:
            if (r5 != 0) goto L_0x01f1
        L_0x0145:
            r5 = 65533(0xfffd, float:9.1831E-41)
            if (r7 != r5) goto L_0x014c
            goto L_0x01f1
        L_0x014c:
            r5 = 65536(0x10000, float:9.18355E-41)
            if (r7 >= r5) goto L_0x0152
            r15 = 1
            goto L_0x0153
        L_0x0152:
            r15 = 2
        L_0x0153:
            int r4 = r4 + r15
            int r3 = r3 + 3
            r5 = r10
            goto L_0x0017
        L_0x0159:
            int r7 = r7 >> 3
            if (r7 != r14) goto L_0x01ee
            int r7 = r3 + 3
            if (r2 > r7) goto L_0x0165
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x0165:
            byte r8 = r1[r3]
            int r10 = r3 + 1
            byte r10 = r1[r10]
            r13 = r10 & 192(0xc0, float:2.69E-43)
            if (r13 != r15) goto L_0x0171
            r13 = 1
            goto L_0x0172
        L_0x0171:
            r13 = 0
        L_0x0172:
            if (r13 != 0) goto L_0x0178
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x0178:
            int r13 = r3 + 2
            byte r13 = r1[r13]
            r14 = r13 & 192(0xc0, float:2.69E-43)
            if (r14 != r15) goto L_0x0182
            r14 = 1
            goto L_0x0183
        L_0x0182:
            r14 = 0
        L_0x0183:
            if (r14 != 0) goto L_0x0189
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x0189:
            byte r7 = r1[r7]
            r14 = r7 & 192(0xc0, float:2.69E-43)
            if (r14 != r15) goto L_0x0191
            r14 = 1
            goto L_0x0192
        L_0x0191:
            r14 = 0
        L_0x0192:
            if (r14 != 0) goto L_0x0198
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x0198:
            r14 = 3678080(0x381f80, float:5.154088E-39)
            r7 = r7 ^ r14
            int r13 = r13 << 6
            r7 = r7 ^ r13
            int r10 = r10 << 12
            r7 = r7 ^ r10
            int r8 = r8 << 18
            r7 = r7 ^ r8
            r8 = 1114111(0x10ffff, float:1.561202E-39)
            if (r7 <= r8) goto L_0x01ad
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x01ad:
            if (r11 <= r7) goto L_0x01b0
            goto L_0x01b5
        L_0x01b0:
            if (r12 < r7) goto L_0x01b5
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x01b5:
            r8 = 65536(0x10000, float:9.18355E-41)
            if (r7 >= r8) goto L_0x01bc
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x01bc:
            int r8 = r5 + 1
            if (r5 != r6) goto L_0x01c1
            goto L_0x01f2
        L_0x01c1:
            if (r7 == r9) goto L_0x01db
            r5 = 13
            if (r7 == r5) goto L_0x01db
            if (r7 < 0) goto L_0x01cd
            r5 = 31
            if (r5 >= r7) goto L_0x01d6
        L_0x01cd:
            r5 = 127(0x7f, float:1.78E-43)
            if (r5 <= r7) goto L_0x01d2
            goto L_0x01d8
        L_0x01d2:
            r5 = 159(0x9f, float:2.23E-43)
            if (r5 < r7) goto L_0x01d8
        L_0x01d6:
            r5 = 1
            goto L_0x01d9
        L_0x01d8:
            r5 = 0
        L_0x01d9:
            if (r5 != 0) goto L_0x01f1
        L_0x01db:
            r5 = 65533(0xfffd, float:9.1831E-41)
            if (r7 != r5) goto L_0x01e1
            goto L_0x01f1
        L_0x01e1:
            r5 = 65536(0x10000, float:9.18355E-41)
            if (r7 >= r5) goto L_0x01e7
            r15 = 1
            goto L_0x01e8
        L_0x01e7:
            r15 = 2
        L_0x01e8:
            int r4 = r4 + r15
            int r3 = r3 + 4
            r5 = r8
            goto L_0x0017
        L_0x01ee:
            if (r5 != r6) goto L_0x01f1
            goto L_0x01f2
        L_0x01f1:
            r4 = -1
        L_0x01f2:
            r1 = 93
            java.lang.String r2 = "…]"
            java.lang.String r3 = "[size="
            r5 = -1
            if (r4 != r5) goto L_0x026e
            byte[] r4 = r0.d
            int r4 = r4.length
            if (r4 > r6) goto L_0x0215
            java.lang.String r2 = "[hex="
            java.lang.StringBuilder r2 = j.a.a.a.outline.a(r2)
            java.lang.String r0 = r16.h()
            r2.append(r0)
            r2.append(r1)
            java.lang.String r0 = r2.toString()
            goto L_0x024e
        L_0x0215:
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r3)
            byte[] r3 = r0.d
            int r3 = r3.length
            r1.append(r3)
            java.lang.String r3 = " hex="
            r1.append(r3)
            byte[] r3 = r0.d
            int r3 = r3.length
            if (r6 > r3) goto L_0x022b
            r3 = 1
            goto L_0x022c
        L_0x022b:
            r3 = 0
        L_0x022c:
            if (r3 == 0) goto L_0x024f
            byte[] r3 = r0.d
            int r4 = r3.length
            if (r6 != r4) goto L_0x0234
            goto L_0x0240
        L_0x0234:
            byte[] r0 = new byte[r6]
            r4 = 0
            n.i.Collections.b(r3, r4, r0, r4, r6)
            p.ByteString r3 = new p.ByteString
            r3.<init>(r0)
            r0 = r3
        L_0x0240:
            java.lang.String r0 = r0.h()
            r1.append(r0)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
        L_0x024e:
            return r0
        L_0x024f:
            java.lang.String r1 = "endIndex > length("
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r1)
            byte[] r0 = r0.d
            int r0 = r0.length
            r1.append(r0)
            r0 = 41
            r1.append(r0)
            java.lang.String r0 = r1.toString()
            java.lang.IllegalArgumentException r1 = new java.lang.IllegalArgumentException
            java.lang.String r0 = r0.toString()
            r1.<init>(r0)
            throw r1
        L_0x026e:
            java.lang.String r5 = h(r16)
            if (r5 == 0) goto L_0x02cc
            r6 = 0
            java.lang.String r7 = r5.substring(r6, r4)
            java.lang.String r8 = "(this as java.lang.Strin…ing(startIndex, endIndex)"
            n.n.c.Intrinsics.a(r7, r8)
            r8 = 4
            java.lang.String r9 = "\\"
            java.lang.String r10 = "\\\\"
            java.lang.String r7 = n.r.Indent.a(r7, r9, r10, r6, r8)
            java.lang.String r9 = "\n"
            java.lang.String r10 = "\\n"
            java.lang.String r7 = n.r.Indent.a(r7, r9, r10, r6, r8)
            java.lang.String r9 = "\r"
            java.lang.String r10 = "\\r"
            java.lang.String r6 = n.r.Indent.a(r7, r9, r10, r6, r8)
            int r5 = r5.length()
            if (r4 >= r5) goto L_0x02b7
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r3)
            byte[] r0 = r0.d
            int r0 = r0.length
            r1.append(r0)
            java.lang.String r0 = " text="
            r1.append(r0)
            r1.append(r6)
            r1.append(r2)
            java.lang.String r0 = r1.toString()
            goto L_0x02cb
        L_0x02b7:
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r2 = "[text="
            r0.append(r2)
            r0.append(r6)
            r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x02cb:
            return r0
        L_0x02cc:
            kotlin.TypeCastException r0 = new kotlin.TypeCastException
            java.lang.String r1 = "null cannot be cast to non-null type java.lang.String"
            r0.<init>(r1)
            throw r0
        L_0x02d4:
            java.lang.String r0 = "$receiver"
            n.n.c.Intrinsics.a(r0)
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: p.b0.ByteString.g(p.ByteString):java.lang.String");
    }

    public static final String h(p.ByteString byteString) {
        if (byteString != null) {
            String str = byteString.c;
            if (str != null) {
                return str;
            }
            byte[] i2 = byteString.i();
            if (i2 != null) {
                String str2 = new String(i2, Charsets.a);
                byteString.c = str2;
                return str2;
            }
            Intrinsics.a("$receiver");
            throw null;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final byte a(p.ByteString byteString, int i2) {
        if (byteString != null) {
            return byteString.d[i2];
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final boolean b(p.ByteString byteString, p.ByteString byteString2) {
        if (byteString == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (byteString2 != null) {
            return byteString.a(0, byteString2, 0, byteString2.g());
        } else {
            Intrinsics.a("prefix");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final p.ByteString c(String str) {
        if (str != null) {
            byte[] bytes = str.getBytes(Charsets.a);
            Intrinsics.a((Object) bytes, "(this as java.lang.String).getBytes(charset)");
            p.ByteString byteString = new p.ByteString(bytes);
            byteString.c = str;
            return byteString;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final boolean a(p.ByteString byteString, int i2, p.ByteString byteString2, int i3, int i4) {
        if (byteString == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (byteString2 != null) {
            return byteString2.a(i3, byteString.d, i2, i4);
        } else {
            Intrinsics.a("other");
            throw null;
        }
    }

    public static final p.ByteString b(String str) {
        if (str != null) {
            if (str.length() % 2 == 0) {
                int length = str.length() / 2;
                byte[] bArr = new byte[length];
                for (int i2 = 0; i2 < length; i2++) {
                    int i3 = i2 * 2;
                    bArr[i2] = (byte) (a(str.charAt(i3 + 1)) + (a(str.charAt(i3)) << 4));
                }
                return new p.ByteString(bArr);
            }
            throw new IllegalArgumentException(outline.a("Unexpected hex string: ", str).toString());
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final boolean a(p.ByteString byteString, int i2, byte[] bArr, int i3, int i4) {
        if (byteString == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (bArr != null) {
            if (i2 >= 0) {
                byte[] bArr2 = byteString.d;
                return i2 <= bArr2.length - i4 && i3 >= 0 && i3 <= bArr.length - i4 && Collections.a(bArr2, i2, bArr, i3, i4);
            }
        } else {
            Intrinsics.a("other");
            throw null;
        }
    }

    public static final int a(p.ByteString byteString, p.ByteString byteString2) {
        if (byteString == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (byteString2 != null) {
            int g = byteString.g();
            int g2 = byteString2.g();
            int min = Math.min(g, g2);
            int i2 = 0;
            while (i2 < min) {
                byte a2 = byteString.a(i2) & 255;
                byte a3 = byteString2.a(i2) & 255;
                if (a2 == a3) {
                    i2++;
                } else if (a2 < a3) {
                    return -1;
                } else {
                    return 1;
                }
            }
            if (g == g2) {
                return 0;
            }
            if (g < g2) {
                return -1;
            }
            return 1;
        } else {
            Intrinsics.a("other");
            throw null;
        }
    }

    public static final boolean a(p.ByteString byteString, Object obj) {
        if (byteString == null) {
            Intrinsics.a("$receiver");
            throw null;
        } else if (obj == byteString) {
            return true;
        } else {
            if (obj instanceof p.ByteString) {
                p.ByteString byteString2 = (p.ByteString) obj;
                int g = byteString2.g();
                byte[] bArr = byteString.d;
                if (g != bArr.length || !byteString2.a(0, bArr, 0, bArr.length)) {
                    return false;
                }
                return true;
            }
            return false;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [byte[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final p.ByteString a(byte[] bArr) {
        if (bArr != null) {
            byte[] copyOf = Arrays.copyOf(bArr, bArr.length);
            Intrinsics.a((Object) copyOf, "java.util.Arrays.copyOf(this, size)");
            return new p.ByteString(copyOf);
        }
        Intrinsics.a("data");
        throw null;
    }

    public static final p.ByteString a(String str) {
        if (str != null) {
            byte[] a2 = a.a(str);
            if (a2 != null) {
                return new p.ByteString(a2);
            }
            return null;
        }
        Intrinsics.a("$receiver");
        throw null;
    }

    public static final int a(char c) {
        if ('0' <= c && '9' >= c) {
            return c - '0';
        }
        char c2 = 'a';
        if ('a' > c || 'f' < c) {
            c2 = 'A';
            if ('A' > c || 'F' < c) {
                throw new IllegalArgumentException(outline.a("Unexpected hex digit: ", c));
            }
        }
        return (c - c2) + 10;
    }
}
