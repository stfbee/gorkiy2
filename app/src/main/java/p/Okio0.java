package p;

import j.a.a.a.outline;
import java.io.IOException;
import java.io.InputStream;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: Okio.kt */
public final class Okio0 implements Source {
    public final InputStream b;
    public final Timeout c;

    public Okio0(InputStream inputStream, Timeout timeout) {
        if (inputStream == null) {
            Intrinsics.a("input");
            throw null;
        } else if (timeout != null) {
            this.b = inputStream;
            this.c = timeout;
        } else {
            Intrinsics.a("timeout");
            throw null;
        }
    }

    public long b(Buffer buffer, long j2) {
        if (buffer != null) {
            int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
            if (i2 == 0) {
                return 0;
            }
            if (i2 >= 0) {
                try {
                    this.c.e();
                    Segment a = buffer.a(1);
                    int read = this.b.read(a.a, a.c, (int) Math.min(j2, (long) (8192 - a.c)));
                    if (read == -1) {
                        return -1;
                    }
                    a.c += read;
                    long j3 = (long) read;
                    buffer.c += j3;
                    return j3;
                } catch (AssertionError e2) {
                    if (Collections.a(e2)) {
                        throw new IOException(e2);
                    }
                    throw e2;
                }
            } else {
                throw new IllegalArgumentException(("byteCount < 0: " + j2).toString());
            }
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public void close() {
        this.b.close();
    }

    public String toString() {
        StringBuilder a = outline.a("source(");
        a.append(this.b);
        a.append(')');
        return a.toString();
    }

    public Timeout b() {
        return this.c;
    }
}
