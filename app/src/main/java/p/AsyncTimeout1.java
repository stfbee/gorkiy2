package p;

import j.a.a.a.outline;
import java.io.IOException;
import n.n.c.Intrinsics;

/* compiled from: AsyncTimeout.kt */
public final class AsyncTimeout1 implements Source {
    public final /* synthetic */ AsyncTimeout b;
    public final /* synthetic */ Source c;

    public AsyncTimeout1(AsyncTimeout asyncTimeout, z zVar) {
        this.b = asyncTimeout;
        this.c = zVar;
    }

    public long b(Buffer buffer, long j2) {
        if (buffer != null) {
            this.b.f();
            try {
                long b2 = this.c.b(buffer, j2);
                this.b.a(true);
                return b2;
            } catch (IOException e2) {
                throw this.b.a(e2);
            } catch (Throwable th) {
                this.b.a(false);
                throw th;
            }
        } else {
            Intrinsics.a("sink");
            throw null;
        }
    }

    public void close() {
        this.b.f();
        try {
            this.c.close();
            this.b.a(true);
        } catch (IOException e2) {
            throw this.b.a(e2);
        } catch (Throwable th) {
            this.b.a(false);
            throw th;
        }
    }

    public String toString() {
        StringBuilder a = outline.a("AsyncTimeout.source(");
        a.append(this.c);
        a.append(')');
        return a.toString();
    }

    public Timeout b() {
        return this.b;
    }
}
