package m.a;

public interface Provider<T> {
    T get();
}
