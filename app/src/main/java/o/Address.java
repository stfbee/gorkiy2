package o;

import j.a.a.a.outline;
import java.net.Proxy;
import java.net.ProxySelector;
import java.util.List;
import java.util.Objects;
import javax.net.SocketFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLSocketFactory;
import n.i.Collections;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.HttpUrl;
import o.m0.Util;

/* compiled from: Address.kt */
public final class Address {
    public final HttpUrl a;
    public final List<c0> b;
    public final List<m> c;
    public final Dns d;

    /* renamed from: e  reason: collision with root package name */
    public final SocketFactory f2796e;

    /* renamed from: f  reason: collision with root package name */
    public final SSLSocketFactory f2797f;
    public final HostnameVerifier g;
    public final CertificatePinner h;

    /* renamed from: i  reason: collision with root package name */
    public final Authenticator f2798i;

    /* renamed from: j  reason: collision with root package name */
    public final Proxy f2799j;

    /* renamed from: k  reason: collision with root package name */
    public final ProxySelector f2800k;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
    public Address(String str, int i2, s sVar, SocketFactory socketFactory, SSLSocketFactory sSLSocketFactory, HostnameVerifier hostnameVerifier, h hVar, c cVar, Proxy proxy, List<? extends c0> list, List<m> list2, ProxySelector proxySelector) {
        int i3 = i2;
        s sVar2 = sVar;
        SocketFactory socketFactory2 = socketFactory;
        c cVar2 = cVar;
        ProxySelector proxySelector2 = proxySelector;
        if (str == null) {
            Intrinsics.a("uriHost");
            throw null;
        } else if (sVar2 == null) {
            Intrinsics.a("dns");
            throw null;
        } else if (socketFactory2 == null) {
            Intrinsics.a("socketFactory");
            throw null;
        } else if (cVar2 == null) {
            Intrinsics.a("proxyAuthenticator");
            throw null;
        } else if (list == null) {
            Intrinsics.a("protocols");
            throw null;
        } else if (list2 == null) {
            Intrinsics.a("connectionSpecs");
            throw null;
        } else if (proxySelector2 != null) {
            this.d = sVar2;
            this.f2796e = socketFactory2;
            this.f2797f = sSLSocketFactory;
            this.g = hostnameVerifier;
            this.h = hVar;
            this.f2798i = cVar2;
            this.f2799j = proxy;
            this.f2800k = proxySelector2;
            HttpUrl.a aVar = new HttpUrl.a();
            String str2 = this.f2797f != null ? "https" : "http";
            boolean z = true;
            if (Indent.a(str2, "http", true)) {
                aVar.a = "http";
            } else if (Indent.a(str2, "https", true)) {
                aVar.a = "https";
            } else {
                throw new IllegalArgumentException(outline.a("unexpected scheme: ", str2));
            }
            String a2 = Collections.a(HttpUrl.b.a(HttpUrl.f2850l, str, 0, 0, false, 7));
            if (a2 != null) {
                aVar.d = a2;
                if ((1 > i3 || 65535 < i3) ? false : z) {
                    aVar.f2856e = i3;
                    this.a = aVar.a();
                    this.b = Util.b(list);
                    this.c = Util.b(list2);
                    return;
                }
                throw new IllegalArgumentException(outline.b("unexpected port: ", i2).toString());
            }
            throw new IllegalArgumentException(outline.a("unexpected host: ", str));
        } else {
            Intrinsics.a("proxySelector");
            throw null;
        }
    }

    public final boolean a(Address address) {
        if (address != null) {
            return Intrinsics.a(this.d, address.d) && Intrinsics.a(this.f2798i, address.f2798i) && Intrinsics.a(this.b, address.b) && Intrinsics.a(this.c, address.c) && Intrinsics.a(this.f2800k, address.f2800k) && Intrinsics.a(this.f2799j, address.f2799j) && Intrinsics.a(this.f2797f, address.f2797f) && Intrinsics.a(this.g, address.g) && Intrinsics.a(this.h, address.h) && this.a.f2852f == address.a.f2852f;
        }
        Intrinsics.a("that");
        throw null;
    }

    public boolean equals(Object obj) {
        if (obj instanceof Address) {
            Address address = (Address) obj;
            return Intrinsics.a(this.a, address.a) && a(address);
        }
    }

    public int hashCode() {
        int hashCode = this.d.hashCode();
        int hashCode2 = this.f2798i.hashCode();
        int hashCode3 = this.b.hashCode();
        int hashCode4 = this.c.hashCode();
        int hashCode5 = this.f2800k.hashCode();
        int hashCode6 = Objects.hashCode(this.f2799j);
        int hashCode7 = Objects.hashCode(this.f2797f);
        int hashCode8 = Objects.hashCode(this.g);
        return Objects.hashCode(this.h) + ((hashCode8 + ((hashCode7 + ((hashCode6 + ((hashCode5 + ((hashCode4 + ((hashCode3 + ((hashCode2 + ((hashCode + ((this.a.hashCode() + 527) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31)) * 31);
    }

    public String toString() {
        Object obj;
        StringBuilder sb;
        StringBuilder a2 = outline.a("Address{");
        a2.append(this.a.f2851e);
        a2.append(':');
        a2.append(this.a.f2852f);
        a2.append(", ");
        if (this.f2799j != null) {
            sb = outline.a("proxy=");
            obj = this.f2799j;
        } else {
            sb = outline.a("proxySelector=");
            obj = this.f2800k;
        }
        sb.append(obj);
        a2.append(sb.toString());
        a2.append("}");
        return a2.toString();
    }
}
