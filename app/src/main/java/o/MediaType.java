package o;

import j.a.a.a.outline;
import java.nio.charset.Charset;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: MediaType.kt */
public final class MediaType {
    public static final Pattern d = Pattern.compile("([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)/([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)");

    /* renamed from: e  reason: collision with root package name */
    public static final Pattern f2858e = Pattern.compile(";\\s*(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)=(?:([a-zA-Z0-9-!#$%&'*+.^_`{|}~]+)|\"([^\"]*)\"))?");

    /* renamed from: f  reason: collision with root package name */
    public static final a f2859f = null;
    public final String a;
    public final String b;
    public final String c;

    /* compiled from: MediaType.kt */
    public static final class a {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Locale, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
         arg types: [java.lang.String, java.lang.String, int]
         candidates:
          n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
         arg types: [java.lang.String, java.lang.String, int, int]
         candidates:
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
          n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
          n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
          n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
          n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean */
        public static final MediaType a(String str) {
            String str2 = null;
            if (str != null) {
                Matcher matcher = MediaType.d.matcher(str);
                if (matcher.lookingAt()) {
                    String group = matcher.group(1);
                    Intrinsics.a((Object) group, "typeSubtype.group(1)");
                    Locale locale = Locale.US;
                    Intrinsics.a((Object) locale, "Locale.US");
                    String lowerCase = group.toLowerCase(locale);
                    Intrinsics.a((Object) lowerCase, "(this as java.lang.String).toLowerCase(locale)");
                    String group2 = matcher.group(2);
                    Intrinsics.a((Object) group2, "typeSubtype.group(2)");
                    Locale locale2 = Locale.US;
                    Intrinsics.a((Object) locale2, "Locale.US");
                    String lowerCase2 = group2.toLowerCase(locale2);
                    Intrinsics.a((Object) lowerCase2, "(this as java.lang.String).toLowerCase(locale)");
                    Matcher matcher2 = MediaType.f2858e.matcher(str);
                    int end = matcher.end();
                    while (true) {
                        String str3 = str2;
                        while (end < str.length()) {
                            matcher2.region(end, str.length());
                            if (matcher2.lookingAt()) {
                                String group3 = matcher2.group(1);
                                if (group3 == null || !Indent.a(group3, "charset", true)) {
                                    end = matcher2.end();
                                } else {
                                    str2 = matcher2.group(2);
                                    boolean z = false;
                                    if (str2 == null) {
                                        str2 = matcher2.group(3);
                                        Intrinsics.a((Object) str2, "parameter.group(3)");
                                    } else if (Indent.b(str2, "'", false, 2) && Indent.a(str2, "'", false, 2) && str2.length() > 2) {
                                        str2 = str2.substring(1, str2.length() - 1);
                                        Intrinsics.a((Object) str2, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                                    }
                                    if (str3 == null || Indent.a(str2, str3, true)) {
                                        z = true;
                                    }
                                    if (z) {
                                        end = matcher2.end();
                                    } else {
                                        throw new IllegalArgumentException(("Multiple charsets defined: \"" + str3 + "\" and: \"" + str2 + "\" for: \"" + str + '\"').toString());
                                    }
                                }
                            } else {
                                StringBuilder a = outline.a("Parameter is not formatted correctly: \"");
                                String substring = str.substring(end);
                                Intrinsics.a((Object) substring, "(this as java.lang.String).substring(startIndex)");
                                a.append(substring);
                                a.append("\" for: \"");
                                a.append(str);
                                a.append('\"');
                                throw new IllegalArgumentException(a.toString().toString());
                            }
                        }
                        return new MediaType(str, lowerCase, lowerCase2, str3, null);
                    }
                }
                throw new IllegalArgumentException(("No subtype found for: \"" + str + '\"').toString());
            }
            Intrinsics.a("$this$toMediaType");
            throw null;
        }

        public static final MediaType b(String str) {
            if (str != null) {
                try {
                    return a(str);
                } catch (IllegalArgumentException unused) {
                    return null;
                }
            } else {
                Intrinsics.a("$this$toMediaTypeOrNull");
                throw null;
            }
        }
    }

    public /* synthetic */ MediaType(String str, String str2, String str3, String str4, DefaultConstructorMarker defaultConstructorMarker) {
        this.a = str;
        this.b = str2;
        this.c = str4;
    }

    public static /* synthetic */ Charset a(MediaType mediaType, Charset charset, int i2) {
        if ((i2 & 1) != 0) {
            charset = null;
        }
        return mediaType.a(charset);
    }

    public static final MediaType a(String str) {
        return a.a(str);
    }

    public boolean equals(Object obj) {
        return (obj instanceof MediaType) && Intrinsics.a(((MediaType) obj).a, this.a);
    }

    public int hashCode() {
        return this.a.hashCode();
    }

    public String toString() {
        return this.a;
    }

    public final Charset a(Charset charset) {
        try {
            return this.c != null ? Charset.forName(this.c) : charset;
        } catch (IllegalArgumentException unused) {
            return charset;
        }
    }
}
