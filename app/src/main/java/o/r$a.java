package o;

import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.List;
import n.i.Collections2;
import n.n.c.Intrinsics;

/* compiled from: Dns.kt */
public final class r$a implements Dns {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.net.InetAddress[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public List<InetAddress> a(String str) {
        if (str != null) {
            try {
                InetAddress[] allByName = InetAddress.getAllByName(str);
                Intrinsics.a((Object) allByName, "InetAddress.getAllByName(hostname)");
                int length = allByName.length;
                if (length == 0) {
                    return Collections2.b;
                }
                if (length != 1) {
                    return c.a((Object[]) allByName);
                }
                return c.c(allByName[0]);
            } catch (NullPointerException e2) {
                UnknownHostException unknownHostException = new UnknownHostException(outline.a("Broken system behaviour for dns lookup of ", str));
                unknownHostException.initCause(e2);
                throw unknownHostException;
            }
        } else {
            Intrinsics.a("hostname");
            throw null;
        }
    }
}
