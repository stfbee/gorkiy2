package o;

import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import javax.net.ssl.SSLSocket;
import kotlin.TypeCastException;
import n.i._Arrays;
import n.j.Comparisons;
import n.n.c.Intrinsics;
import o.CipherSuite;
import o.m0.Util;

/* compiled from: ConnectionSpec.kt */
public final class ConnectionSpec {

    /* renamed from: e  reason: collision with root package name */
    public static final CipherSuite[] f2835e = {CipherSuite.f2831q, CipherSuite.f2832r, CipherSuite.f2833s, CipherSuite.f2825k, CipherSuite.f2827m, CipherSuite.f2826l, CipherSuite.f2828n, CipherSuite.f2830p, CipherSuite.f2829o};

    /* renamed from: f  reason: collision with root package name */
    public static final CipherSuite[] f2836f = {CipherSuite.f2831q, CipherSuite.f2832r, CipherSuite.f2833s, CipherSuite.f2825k, CipherSuite.f2827m, CipherSuite.f2826l, CipherSuite.f2828n, CipherSuite.f2830p, CipherSuite.f2829o, CipherSuite.f2823i, CipherSuite.f2824j, CipherSuite.g, CipherSuite.h, CipherSuite.f2821e, CipherSuite.f2822f, CipherSuite.d};
    public static final ConnectionSpec g;
    public static final ConnectionSpec h = new ConnectionSpec(false, false, null, null);
    public final boolean a;
    public final boolean b;
    public final String[] c;
    public final String[] d;

    /* compiled from: ConnectionSpec.kt */
    public static final class a {
        public boolean a;
        public String[] b;
        public String[] c;
        public boolean d;

        public a(boolean z) {
            this.a = z;
        }

        public final a a(CipherSuite... cipherSuiteArr) {
            if (cipherSuiteArr == null) {
                Intrinsics.a("cipherSuites");
                throw null;
            } else if (this.a) {
                ArrayList arrayList = new ArrayList(cipherSuiteArr.length);
                for (CipherSuite cipherSuite : cipherSuiteArr) {
                    arrayList.add(cipherSuite.a);
                }
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    String[] strArr = (String[]) array;
                    a((String[]) Arrays.copyOf(strArr, strArr.length));
                    return this;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            } else {
                throw new IllegalArgumentException("no cipher suites for cleartext connections".toString());
            }
        }

        public final a b(String... strArr) {
            if (strArr == null) {
                Intrinsics.a("tlsVersions");
                throw null;
            } else if (this.a) {
                if (!(strArr.length == 0)) {
                    Object clone = strArr.clone();
                    if (clone != null) {
                        this.c = (String[]) clone;
                        return this;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                }
                throw new IllegalArgumentException("At least one TLS version is required".toString());
            } else {
                throw new IllegalArgumentException("no TLS versions for cleartext connections".toString());
            }
        }

        public a(ConnectionSpec connectionSpec) {
            if (connectionSpec != null) {
                this.a = connectionSpec.a;
                this.b = connectionSpec.c;
                this.c = connectionSpec.d;
                this.d = connectionSpec.b;
                return;
            }
            Intrinsics.a("connectionSpec");
            throw null;
        }

        public final a a(String... strArr) {
            if (strArr == null) {
                Intrinsics.a("cipherSuites");
                throw null;
            } else if (this.a) {
                if (!(strArr.length == 0)) {
                    Object clone = strArr.clone();
                    if (clone != null) {
                        this.b = (String[]) clone;
                        return this;
                    }
                    throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<kotlin.String>");
                }
                throw new IllegalArgumentException("At least one cipher suite is required".toString());
            } else {
                throw new IllegalArgumentException("no cipher suites for cleartext connections".toString());
            }
        }

        public final a a(TlsVersion... tlsVersionArr) {
            if (tlsVersionArr == null) {
                Intrinsics.a("tlsVersions");
                throw null;
            } else if (this.a) {
                ArrayList arrayList = new ArrayList(tlsVersionArr.length);
                for (TlsVersion tlsVersion : tlsVersionArr) {
                    arrayList.add(tlsVersion.javaName);
                }
                Object[] array = arrayList.toArray(new String[0]);
                if (array != null) {
                    String[] strArr = (String[]) array;
                    b((String[]) Arrays.copyOf(strArr, strArr.length));
                    return this;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            } else {
                throw new IllegalArgumentException("no TLS versions for cleartext connections".toString());
            }
        }

        public final a a(boolean z) {
            if (this.a) {
                this.d = z;
                return this;
            }
            throw new IllegalArgumentException("no TLS extensions for cleartext connections".toString());
        }

        public final ConnectionSpec a() {
            return new ConnectionSpec(this.a, this.d, this.b, this.c);
        }
    }

    static {
        a aVar = new a(true);
        CipherSuite[] cipherSuiteArr = f2835e;
        aVar.a((CipherSuite[]) Arrays.copyOf(cipherSuiteArr, cipherSuiteArr.length));
        aVar.a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2);
        aVar.a(true);
        aVar.a();
        a aVar2 = new a(true);
        CipherSuite[] cipherSuiteArr2 = f2836f;
        aVar2.a((CipherSuite[]) Arrays.copyOf(cipherSuiteArr2, cipherSuiteArr2.length));
        aVar2.a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2);
        aVar2.a(true);
        g = aVar2.a();
        a aVar3 = new a(true);
        CipherSuite[] cipherSuiteArr3 = f2836f;
        aVar3.a((CipherSuite[]) Arrays.copyOf(cipherSuiteArr3, cipherSuiteArr3.length));
        aVar3.a(TlsVersion.TLS_1_3, TlsVersion.TLS_1_2, TlsVersion.TLS_1_1, TlsVersion.TLS_1_0);
        aVar3.a(true);
        aVar3.a();
    }

    public ConnectionSpec(boolean z, boolean z2, String[] strArr, String[] strArr2) {
        this.a = z;
        this.b = z2;
        this.c = strArr;
        this.d = strArr2;
    }

    public final List<j> a() {
        String[] strArr = this.c;
        if (strArr == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String a2 : strArr) {
            arrayList.add(CipherSuite.f2834t.a(a2));
        }
        return _Arrays.a((Iterable) arrayList);
    }

    public final List<l0> b() {
        String[] strArr = this.d;
        if (strArr == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(strArr.length);
        for (String a2 : strArr) {
            arrayList.add(TlsVersion.Companion.a(a2));
        }
        return _Arrays.a((Iterable) arrayList);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ConnectionSpec)) {
            return false;
        }
        if (obj == this) {
            return true;
        }
        boolean z = this.a;
        ConnectionSpec connectionSpec = (ConnectionSpec) obj;
        if (z != connectionSpec.a) {
            return false;
        }
        return !z || (Arrays.equals(this.c, connectionSpec.c) && Arrays.equals(this.d, connectionSpec.d) && this.b == connectionSpec.b);
    }

    public int hashCode() {
        if (!this.a) {
            return 17;
        }
        String[] strArr = this.c;
        int i2 = 0;
        int hashCode = (527 + (strArr != null ? Arrays.hashCode(strArr) : 0)) * 31;
        String[] strArr2 = this.d;
        if (strArr2 != null) {
            i2 = Arrays.hashCode(strArr2);
        }
        return ((hashCode + i2) * 31) + (this.b ^ true ? 1 : 0);
    }

    public String toString() {
        if (!this.a) {
            return "ConnectionSpec()";
        }
        StringBuilder b2 = outline.b("ConnectionSpec(", "cipherSuites=");
        b2.append(Objects.toString(a(), "[all enabled]"));
        b2.append(", ");
        b2.append("tlsVersions=");
        b2.append(Objects.toString(b(), "[all enabled]"));
        b2.append(", ");
        b2.append("supportsTlsExtensions=");
        b2.append(this.b);
        b2.append(')');
        return b2.toString();
    }

    public final boolean a(SSLSocket sSLSocket) {
        if (sSLSocket == null) {
            Intrinsics.a("socket");
            throw null;
        } else if (!this.a) {
            return false;
        } else {
            String[] strArr = this.d;
            if (strArr != null && !Util.a(strArr, sSLSocket.getEnabledProtocols(), Comparisons.b)) {
                return false;
            }
            String[] strArr2 = this.c;
            if (strArr2 == null) {
                return true;
            }
            String[] enabledCipherSuites = sSLSocket.getEnabledCipherSuites();
            CipherSuite.b bVar = CipherSuite.f2834t;
            if (!Util.a(strArr2, enabledCipherSuites, CipherSuite.b)) {
                return false;
            }
            return true;
        }
    }
}
