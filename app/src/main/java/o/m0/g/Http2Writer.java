package o.m0.g;

import com.crashlytics.android.answers.AnswersPreferenceManager;
import j.a.a.a.outline;
import java.io.Closeable;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import n.n.c.Intrinsics;
import o.m0.Util;
import o.m0.g.Hpack;
import p.Buffer;
import p.BufferedSink;

/* compiled from: Http2Writer.kt */
public final class Http2Writer implements Closeable {
    public static final Logger h = Logger.getLogger(Http2.class.getName());
    public final Buffer b;
    public int c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final Hpack.b f3037e;

    /* renamed from: f  reason: collision with root package name */
    public final BufferedSink f3038f;
    public final boolean g;

    public Http2Writer(BufferedSink bufferedSink, boolean z) {
        if (bufferedSink != null) {
            this.f3038f = bufferedSink;
            this.g = z;
            Buffer buffer = new Buffer();
            this.b = buffer;
            this.c = 16384;
            this.f3037e = new Hpack.b(0, false, buffer, 3);
            return;
        }
        Intrinsics.a("sink");
        throw null;
    }

    public final synchronized void a() {
        if (this.d) {
            throw new IOException("closed");
        } else if (this.g) {
            if (h.isLoggable(Level.FINE)) {
                Logger logger = h;
                logger.fine(Util.a(">> CONNECTION " + Http2.a.h(), new Object[0]));
            }
            this.f3038f.a(Http2.a);
            this.f3038f.flush();
        }
    }

    public final synchronized void b(Settings settings) {
        if (settings == null) {
            Intrinsics.a(AnswersPreferenceManager.PREF_STORE_NAME);
            throw null;
        } else if (!this.d) {
            a(0, Integer.bitCount(settings.a) * 6, 4, 0);
            int i2 = 0;
            while (i2 < 10) {
                boolean z = true;
                if (((1 << i2) & settings.a) == 0) {
                    z = false;
                }
                if (z) {
                    this.f3038f.writeShort(i2 != 4 ? i2 != 7 ? i2 : 4 : 3);
                    this.f3038f.writeInt(settings.b[i2]);
                }
                i2++;
            }
            this.f3038f.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public synchronized void close() {
        this.d = true;
        this.f3038f.close();
    }

    public final synchronized void flush() {
        if (!this.d) {
            this.f3038f.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void a(Settings settings) {
        if (settings == null) {
            Intrinsics.a("peerSettings");
            throw null;
        } else if (!this.d) {
            int i2 = this.c;
            if ((settings.a & 32) != 0) {
                i2 = settings.b[5];
            }
            this.c = i2;
            int i3 = -1;
            if (((settings.a & 2) != 0 ? settings.b[1] : -1) != -1) {
                Hpack.b bVar = this.f3037e;
                if ((settings.a & 2) != 0) {
                    i3 = settings.b[1];
                }
                bVar.h = i3;
                int min = Math.min(i3, 16384);
                int i4 = bVar.c;
                if (i4 != min) {
                    if (min < i4) {
                        bVar.a = Math.min(bVar.a, min);
                    }
                    bVar.b = true;
                    bVar.c = min;
                    int i5 = bVar.g;
                    if (min < i5) {
                        if (min == 0) {
                            bVar.a();
                        } else {
                            bVar.a(i5 - min);
                        }
                    }
                }
            }
            a(0, 0, 4, 1);
            this.f3038f.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public final void b(int i2, long j2) {
        while (j2 > 0) {
            long min = Math.min((long) this.c, j2);
            j2 -= min;
            a(i2, (int) min, 9, j2 == 0 ? 4 : 0);
            this.f3038f.a(this.b, min);
        }
    }

    public final synchronized void a(int i2, int i3, List<b> list) {
        if (list == null) {
            Intrinsics.a("requestHeaders");
            throw null;
        } else if (!this.d) {
            this.f3037e.a(list);
            long j2 = this.b.c;
            int min = (int) Math.min(((long) this.c) - 4, j2);
            int i4 = min + 4;
            long j3 = (long) min;
            int i5 = (j2 > j3 ? 1 : (j2 == j3 ? 0 : -1));
            a(i2, i4, 5, i5 == 0 ? 4 : 0);
            this.f3038f.writeInt(i3 & Integer.MAX_VALUE);
            this.f3038f.a(this.b, j3);
            if (i5 > 0) {
                b(i2, j2 - j3);
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void a(int i2, ErrorCode errorCode) {
        if (errorCode == null) {
            Intrinsics.a("errorCode");
            throw null;
        } else if (!this.d) {
            if (errorCode.httpCode != -1) {
                a(i2, 4, 3, 0);
                this.f3038f.writeInt(errorCode.httpCode);
                this.f3038f.flush();
            } else {
                throw new IllegalArgumentException("Failed requirement.".toString());
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void a(boolean z, int i2, Buffer buffer, int i3) {
        if (!this.d) {
            a(i2, i3, 0, z ? 1 : 0);
            if (i3 > 0) {
                BufferedSink bufferedSink = this.f3038f;
                if (buffer != null) {
                    bufferedSink.a(buffer, (long) i3);
                } else {
                    Intrinsics.a();
                    throw null;
                }
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void a(boolean z, int i2, int i3) {
        if (!this.d) {
            a(0, 8, 6, z ? 1 : 0);
            this.f3038f.writeInt(i2);
            this.f3038f.writeInt(i3);
            this.f3038f.flush();
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void a(int i2, ErrorCode errorCode, byte[] bArr) {
        if (errorCode == null) {
            Intrinsics.a("errorCode");
            throw null;
        } else if (bArr == null) {
            Intrinsics.a("debugData");
            throw null;
        } else if (!this.d) {
            boolean z = false;
            if (errorCode.httpCode != -1) {
                a(0, bArr.length + 8, 7, 0);
                this.f3038f.writeInt(i2);
                this.f3038f.writeInt(errorCode.httpCode);
                if (bArr.length == 0) {
                    z = true;
                }
                if (!z) {
                    this.f3038f.write(bArr);
                }
                this.f3038f.flush();
            } else {
                throw new IllegalArgumentException("errorCode.httpCode == -1".toString());
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final synchronized void a(int i2, long j2) {
        if (!this.d) {
            if (j2 != 0 && j2 <= 2147483647L) {
                a(i2, 4, 8, 0);
                this.f3038f.writeInt((int) j2);
                this.f3038f.flush();
            } else {
                throw new IllegalArgumentException(("windowSizeIncrement == 0 || windowSizeIncrement > 0x7fffffffL: " + j2).toString());
            }
        } else {
            throw new IOException("closed");
        }
    }

    public final void a(int i2, int i3, int i4, int i5) {
        if (h.isLoggable(Level.FINE)) {
            h.fine(Http2.f2997e.a(false, i2, i3, i4, i5));
        }
        boolean z = true;
        if (i3 <= this.c) {
            if ((((int) 2147483648L) & i2) != 0) {
                z = false;
            }
            if (z) {
                Util.a(this.f3038f, i3);
                this.f3038f.writeByte(i4 & 255);
                this.f3038f.writeByte(i5 & 255);
                this.f3038f.writeInt(i2 & Integer.MAX_VALUE);
                return;
            }
            throw new IllegalArgumentException(outline.b("reserved bit set: ", i2).toString());
        }
        StringBuilder a = outline.a("FRAME_SIZE_ERROR length > ");
        a.append(this.c);
        a.append(": ");
        a.append(i3);
        throw new IllegalArgumentException(a.toString().toString());
    }

    public final synchronized void a(boolean z, int i2, List<b> list) {
        if (list == null) {
            Intrinsics.a("headerBlock");
            throw null;
        } else if (!this.d) {
            this.f3037e.a(list);
            long j2 = this.b.c;
            long min = Math.min((long) this.c, j2);
            int i3 = (j2 > min ? 1 : (j2 == min ? 0 : -1));
            int i4 = i3 == 0 ? 4 : 0;
            if (z) {
                i4 |= 1;
            }
            a(i2, (int) min, 1, i4);
            this.f3038f.a(this.b, min);
            if (i3 > 0) {
                b(i2, j2 - min);
            }
        } else {
            throw new IOException("closed");
        }
    }
}
