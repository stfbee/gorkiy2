package o.m0.e;

import n.n.c.Intrinsics;
import o.MediaType;
import o.ResponseBody;
import p.BufferedSource;

/* compiled from: RealResponseBody.kt */
public final class RealResponseBody extends ResponseBody {
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final long f2979e;

    /* renamed from: f  reason: collision with root package name */
    public final BufferedSource f2980f;

    public RealResponseBody(String str, long j2, BufferedSource bufferedSource) {
        if (bufferedSource != null) {
            this.d = str;
            this.f2979e = j2;
            this.f2980f = bufferedSource;
            return;
        }
        Intrinsics.a("source");
        throw null;
    }

    public long a() {
        return this.f2979e;
    }

    public MediaType f() {
        String str = this.d;
        if (str == null) {
            return null;
        }
        MediaType.a aVar = MediaType.f2859f;
        return MediaType.a.b(str);
    }

    public BufferedSource g() {
        return this.f2980f;
    }
}
