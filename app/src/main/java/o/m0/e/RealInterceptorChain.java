package o.m0.e;

import java.util.List;
import n.n.c.Intrinsics;
import o.Call;
import o.Connection;
import o.Interceptor;
import o.Request;
import o.Response;
import o.e0;
import o.f;
import o.m0.d.Exchange;
import o.m0.d.Transmitter;
import o.m0.d.c;
import o.m0.d.l;
import o.y;

/* compiled from: RealInterceptorChain.kt */
public final class RealInterceptorChain implements Interceptor.a {
    public int a;
    public final List<y> b;
    public final Transmitter c;
    public final Exchange d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2975e;

    /* renamed from: f  reason: collision with root package name */
    public final Request f2976f;
    public final Call g;
    public final int h;

    /* renamed from: i  reason: collision with root package name */
    public final int f2977i;

    /* renamed from: j  reason: collision with root package name */
    public final int f2978j;

    public RealInterceptorChain(List<? extends y> list, l lVar, c cVar, int i2, e0 e0Var, f fVar, int i3, int i4, int i5) {
        if (list == null) {
            Intrinsics.a("interceptors");
            throw null;
        } else if (lVar == null) {
            Intrinsics.a("transmitter");
            throw null;
        } else if (e0Var == null) {
            Intrinsics.a("request");
            throw null;
        } else if (fVar != null) {
            this.b = list;
            this.c = lVar;
            this.d = cVar;
            this.f2975e = i2;
            this.f2976f = e0Var;
            this.g = fVar;
            this.h = i3;
            this.f2977i = i4;
            this.f2978j = i5;
        } else {
            Intrinsics.a("call");
            throw null;
        }
    }

    public Connection a() {
        Exchange exchange = this.d;
        if (exchange != null) {
            return exchange.a();
        }
        return null;
    }

    public int b() {
        return this.f2978j;
    }

    public int c() {
        return this.h;
    }

    public int d() {
        return this.f2977i;
    }

    public Request f() {
        return this.f2976f;
    }

    public Response a(Request request) {
        if (request != null) {
            return a(request, this.c, this.d);
        }
        Intrinsics.a("request");
        throw null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:17:0x0036  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x0109  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final o.Response a(o.Request r16, o.m0.d.Transmitter r17, o.m0.d.Exchange r18) {
        /*
            r15 = this;
            r0 = r15
            r6 = r16
            r1 = 0
            if (r6 == 0) goto L_0x013a
            if (r17 == 0) goto L_0x0134
            int r2 = r0.f2975e
            java.util.List<o.y> r3 = r0.b
            int r3 = r3.size()
            if (r2 >= r3) goto L_0x012e
            int r2 = r0.a
            r11 = 1
            int r2 = r2 + r11
            r0.a = r2
            o.m0.d.Exchange r2 = r0.d
            if (r2 == 0) goto L_0x0031
            o.m0.d.RealConnection r2 = r2.a()
            if (r2 == 0) goto L_0x002d
            o.HttpUrl r1 = r6.b
            boolean r1 = r2.a(r1)
            if (r1 == 0) goto L_0x002b
            goto L_0x0031
        L_0x002b:
            r1 = 0
            goto L_0x0032
        L_0x002d:
            n.n.c.Intrinsics.a()
            throw r1
        L_0x0031:
            r1 = 1
        L_0x0032:
            java.lang.String r12 = "network interceptor "
            if (r1 == 0) goto L_0x0109
            o.m0.d.Exchange r1 = r0.d
            if (r1 == 0) goto L_0x0041
            int r1 = r0.a
            if (r1 > r11) goto L_0x003f
            goto L_0x0041
        L_0x003f:
            r1 = 0
            goto L_0x0042
        L_0x0041:
            r1 = 1
        L_0x0042:
            java.lang.String r13 = " must call proceed() exactly once"
            if (r1 == 0) goto L_0x00e6
            o.m0.e.RealInterceptorChain r14 = new o.m0.e.RealInterceptorChain
            java.util.List<o.y> r2 = r0.b
            int r1 = r0.f2975e
            int r5 = r1 + 1
            o.Call r7 = r0.g
            int r8 = r0.h
            int r9 = r0.f2977i
            int r10 = r0.f2978j
            r1 = r14
            r3 = r17
            r4 = r18
            r6 = r16
            r1.<init>(r2, r3, r4, r5, r6, r7, r8, r9, r10)
            java.util.List<o.y> r1 = r0.b
            int r2 = r0.f2975e
            java.lang.Object r1 = r1.get(r2)
            o.Interceptor r1 = (o.Interceptor) r1
            o.Response r2 = r1.a(r14)
            java.lang.String r3 = "interceptor "
            if (r2 == 0) goto L_0x00cc
            if (r18 == 0) goto L_0x0086
            int r4 = r0.f2975e
            int r4 = r4 + r11
            java.util.List<o.y> r5 = r0.b
            int r5 = r5.size()
            if (r4 >= r5) goto L_0x0086
            int r4 = r14.a
            if (r4 != r11) goto L_0x0084
            goto L_0x0086
        L_0x0084:
            r4 = 0
            goto L_0x0087
        L_0x0086:
            r4 = 1
        L_0x0087:
            if (r4 == 0) goto L_0x00b0
            o.ResponseBody r4 = r2.f2902i
            if (r4 == 0) goto L_0x008e
            goto L_0x008f
        L_0x008e:
            r11 = 0
        L_0x008f:
            if (r11 == 0) goto L_0x0092
            return r2
        L_0x0092:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r3)
            r2.append(r1)
            java.lang.String r1 = " returned a response with no body"
            r2.append(r1)
            java.lang.String r1 = r2.toString()
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x00b0:
            java.lang.StringBuilder r2 = new java.lang.StringBuilder
            r2.<init>()
            r2.append(r12)
            r2.append(r1)
            r2.append(r13)
            java.lang.String r1 = r2.toString()
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x00cc:
            java.lang.NullPointerException r2 = new java.lang.NullPointerException
            java.lang.StringBuilder r4 = new java.lang.StringBuilder
            r4.<init>()
            r4.append(r3)
            r4.append(r1)
            java.lang.String r1 = " returned null"
            r4.append(r1)
            java.lang.String r1 = r4.toString()
            r2.<init>(r1)
            throw r2
        L_0x00e6:
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r12)
            java.util.List<o.y> r2 = r0.b
            int r3 = r0.f2975e
            int r3 = r3 - r11
            java.lang.Object r2 = r2.get(r3)
            o.Interceptor r2 = (o.Interceptor) r2
            r1.append(r2)
            r1.append(r13)
            java.lang.String r1 = r1.toString()
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x0109:
            java.lang.StringBuilder r1 = j.a.a.a.outline.a(r12)
            java.util.List<o.y> r2 = r0.b
            int r3 = r0.f2975e
            int r3 = r3 - r11
            java.lang.Object r2 = r2.get(r3)
            o.Interceptor r2 = (o.Interceptor) r2
            r1.append(r2)
            java.lang.String r2 = " must retain the same host and port"
            r1.append(r2)
            java.lang.String r1 = r1.toString()
            java.lang.IllegalStateException r2 = new java.lang.IllegalStateException
            java.lang.String r1 = r1.toString()
            r2.<init>(r1)
            throw r2
        L_0x012e:
            java.lang.AssertionError r1 = new java.lang.AssertionError
            r1.<init>()
            throw r1
        L_0x0134:
            java.lang.String r2 = "transmitter"
            n.n.c.Intrinsics.a(r2)
            throw r1
        L_0x013a:
            java.lang.String r2 = "request"
            n.n.c.Intrinsics.a(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.e.RealInterceptorChain.a(o.Request, o.m0.d.Transmitter, o.m0.d.Exchange):o.Response");
    }
}
