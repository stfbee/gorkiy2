package o.m0.e;

import n.n.c.Intrinsics;
import n.r.Regex;
import o.HttpUrl;
import o.Interceptor;
import o.OkHttpClient;
import o.Request;
import o.RequestBody;
import o.Response;
import o.m0.Util;

/* compiled from: RetryAndFollowUpInterceptor.kt */
public final class RetryAndFollowUpInterceptor implements Interceptor {
    public final OkHttpClient b;

    public RetryAndFollowUpInterceptor(OkHttpClient okHttpClient) {
        if (okHttpClient != null) {
            this.b = okHttpClient;
        } else {
            Intrinsics.a("client");
            throw null;
        }
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v0 */
    /* JADX WARN: Type inference failed for: r0v1, types: [java.lang.Throwable, java.io.IOException, o.m0.d.ExchangeFinder] */
    /* JADX WARN: Type inference failed for: r0v8 */
    /* JADX WARN: Type inference failed for: r21v0, types: [javax.net.ssl.HostnameVerifier] */
    /* JADX WARN: Type inference failed for: r21v3 */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.d.Transmitter.a(java.io.IOException, boolean):E
     arg types: [?, int]
     candidates:
      o.m0.d.Transmitter.a(o.Interceptor$a, boolean):o.m0.d.Exchange
      o.m0.d.Transmitter.a(java.io.IOException, boolean):E */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX WARNING: Code restructure failed: missing block: B:141:0x0229, code lost:
        r10.c();
        r12 = r30;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x01d5  */
    /* JADX WARNING: Removed duplicated region for block: B:162:0x01b7 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x023a A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:36:0x00c5  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public o.Response a(o.Interceptor.a r30) {
        /*
            r29 = this;
            r1 = r29
            r0 = 0
            if (r30 == 0) goto L_0x024c
            o.Request r2 = r30.f()
            r3 = r30
            o.m0.e.RealInterceptorChain r3 = (o.m0.e.RealInterceptorChain) r3
            o.m0.d.Transmitter r10 = r3.c
            r4 = 0
            r11 = r2
            r12 = 0
            r2 = r0
        L_0x0013:
            if (r11 == 0) goto L_0x0244
            o.Request r4 = r10.f2965e
            java.lang.String r13 = "Check failed."
            r5 = 1
            if (r4 == 0) goto L_0x0058
            o.HttpUrl r4 = r4.b
            o.HttpUrl r6 = r11.b
            boolean r4 = o.m0.Util.a(r4, r6)
            if (r4 == 0) goto L_0x003b
            o.m0.d.ExchangeFinder r4 = r10.f2966f
            if (r4 == 0) goto L_0x0037
            boolean r4 = r4.a()
            if (r4 == 0) goto L_0x003b
            r0 = 1
            r30 = r12
            r28 = r13
            goto L_0x00bf
        L_0x0037:
            n.n.c.Intrinsics.a()
            throw r0
        L_0x003b:
            o.m0.d.Exchange r4 = r10.h
            if (r4 != 0) goto L_0x0041
            r4 = 1
            goto L_0x0042
        L_0x0041:
            r4 = 0
        L_0x0042:
            if (r4 == 0) goto L_0x004e
            o.m0.d.ExchangeFinder r4 = r10.f2966f
            if (r4 == 0) goto L_0x0058
            r10.a(r0, r5)
            r10.f2966f = r0
            goto L_0x0058
        L_0x004e:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = r13.toString()
            r0.<init>(r2)
            throw r0
        L_0x0058:
            r10.f2965e = r11
            o.m0.d.ExchangeFinder r14 = new o.m0.d.ExchangeFinder
            o.m0.d.RealConnectionPool r6 = r10.a
            o.HttpUrl r4 = r11.b
            boolean r5 = r4.a
            if (r5 == 0) goto L_0x007d
            o.OkHttpClient r0 = r10.f2972n
            javax.net.ssl.SSLSocketFactory r5 = r0.f2877r
            if (r5 == 0) goto L_0x0075
            javax.net.ssl.HostnameVerifier r7 = r0.v
            o.CertificatePinner r0 = r0.w
            r22 = r0
            r20 = r5
            r21 = r7
            goto L_0x0083
        L_0x0075:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r2 = "CLEARTEXT-only client"
            r0.<init>(r2)
            throw r0
        L_0x007d:
            r20 = r0
            r21 = r20
            r22 = r21
        L_0x0083:
            o.Address r7 = new o.Address
            java.lang.String r0 = r4.f2851e
            int r4 = r4.f2852f
            o.OkHttpClient r5 = r10.f2972n
            o.Dns r8 = r5.f2872m
            javax.net.SocketFactory r9 = r5.f2876q
            o.Authenticator r15 = r5.f2875p
            r30 = r12
            java.net.Proxy r12 = r5.f2873n
            r28 = r13
            java.util.List<o.c0> r13 = r5.u
            java.util.List<o.m> r1 = r5.f2879t
            java.net.ProxySelector r5 = r5.f2874o
            r23 = r15
            r15 = r7
            r16 = r0
            r17 = r4
            r18 = r8
            r19 = r9
            r24 = r12
            r25 = r13
            r26 = r1
            r27 = r5
            r15.<init>(r16, r17, r18, r19, r20, r21, r22, r23, r24, r25, r26, r27)
            o.Call r8 = r10.f2973o
            o.EventListener r9 = r10.b
            r0 = 1
            r4 = r14
            r5 = r10
            r4.<init>(r5, r6, r7, r8, r9)
            r10.f2966f = r14
        L_0x00bf:
            boolean r1 = r10.e()
            if (r1 != 0) goto L_0x023a
            r1 = 0
            o.Response r4 = r3.a(r11, r10, r1)     // Catch:{ RouteException -> 0x021c, IOException -> 0x0208, all -> 0x0204 }
            if (r2 == 0) goto L_0x00fb
            if (r4 == 0) goto L_0x00fa
            o.Response$a r5 = new o.Response$a
            r5.<init>(r4)
            o.Response$a r4 = new o.Response$a
            r4.<init>(r2)
            r4.g = r1
            o.Response r1 = r4.a()
            o.ResponseBody r2 = r1.f2902i
            if (r2 != 0) goto L_0x00e4
            r2 = 1
            goto L_0x00e5
        L_0x00e4:
            r2 = 0
        L_0x00e5:
            if (r2 == 0) goto L_0x00ee
            r5.f2912j = r1
            o.Response r4 = r5.a()
            goto L_0x00fb
        L_0x00ee:
            java.lang.IllegalArgumentException r0 = new java.lang.IllegalArgumentException
            java.lang.String r1 = "priorResponse.body != null"
            java.lang.String r1 = r1.toString()
            r0.<init>(r1)
            throw r0
        L_0x00fa:
            throw r1
        L_0x00fb:
            o.m0.d.Exchange r1 = r4.f2908o
            if (r1 == 0) goto L_0x0108
            o.m0.d.RealConnection r2 = r1.a()
            if (r2 == 0) goto L_0x0108
            o.Route r2 = r2.f2959q
            goto L_0x0109
        L_0x0108:
            r2 = 0
        L_0x0109:
            int r5 = r4.f2901f
            o.Request r6 = r4.c
            java.lang.String r6 = r6.c
            r7 = 307(0x133, float:4.3E-43)
            if (r5 == r7) goto L_0x019b
            r7 = 308(0x134, float:4.32E-43)
            if (r5 == r7) goto L_0x019b
            r7 = 401(0x191, float:5.62E-43)
            if (r5 == r7) goto L_0x0190
            r7 = 503(0x1f7, float:7.05E-43)
            if (r5 == r7) goto L_0x0179
            r7 = 407(0x197, float:5.7E-43)
            if (r5 == r7) goto L_0x0155
            r2 = 408(0x198, float:5.72E-43)
            if (r5 == r2) goto L_0x0136
            switch(r5) {
                case 300: goto L_0x012e;
                case 301: goto L_0x012e;
                case 302: goto L_0x012e;
                case 303: goto L_0x012e;
                default: goto L_0x012a;
            }
        L_0x012a:
            r5 = r29
            goto L_0x01af
        L_0x012e:
            r5 = r29
            o.Request r2 = r5.a(r4, r6)
            goto L_0x01b5
        L_0x0136:
            r5 = r29
            o.OkHttpClient r6 = r5.b
            boolean r6 = r6.g
            if (r6 != 0) goto L_0x0140
            goto L_0x01af
        L_0x0140:
            o.Response r6 = r4.f2905l
            if (r6 == 0) goto L_0x014a
            int r6 = r6.f2901f
            if (r6 != r2) goto L_0x014a
            goto L_0x01af
        L_0x014a:
            r2 = 0
            int r2 = r5.a(r4, r2)
            if (r2 <= 0) goto L_0x0152
            goto L_0x01af
        L_0x0152:
            o.Request r2 = r4.c
            goto L_0x01b5
        L_0x0155:
            r5 = r29
            if (r2 == 0) goto L_0x0174
            java.net.Proxy r6 = r2.b
            java.net.Proxy$Type r6 = r6.type()
            java.net.Proxy$Type r7 = java.net.Proxy.Type.HTTP
            if (r6 != r7) goto L_0x016c
            o.OkHttpClient r6 = r5.b
            o.Authenticator r6 = r6.f2875p
            o.Request r2 = r6.a(r2, r4)
            goto L_0x01b5
        L_0x016c:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Received HTTP_PROXY_AUTH (407) code while not using proxy"
            r0.<init>(r1)
            throw r0
        L_0x0174:
            n.n.c.Intrinsics.a()
            r0 = 0
            throw r0
        L_0x0179:
            r5 = r29
            o.Response r2 = r4.f2905l
            if (r2 == 0) goto L_0x0184
            int r2 = r2.f2901f
            if (r2 != r7) goto L_0x0184
            goto L_0x01af
        L_0x0184:
            r2 = 2147483647(0x7fffffff, float:NaN)
            int r2 = r5.a(r4, r2)
            if (r2 != 0) goto L_0x01af
            o.Request r2 = r4.c
            goto L_0x01b5
        L_0x0190:
            r5 = r29
            o.OkHttpClient r6 = r5.b
            o.Authenticator r6 = r6.h
            o.Request r2 = r6.a(r2, r4)
            goto L_0x01b5
        L_0x019b:
            r5 = r29
            java.lang.String r2 = "GET"
            boolean r2 = n.n.c.Intrinsics.a(r6, r2)
            r2 = r2 ^ r0
            if (r2 == 0) goto L_0x01b1
            java.lang.String r2 = "HEAD"
            boolean r2 = n.n.c.Intrinsics.a(r6, r2)
            r2 = r2 ^ r0
            if (r2 == 0) goto L_0x01b1
        L_0x01af:
            r2 = 0
            goto L_0x01b5
        L_0x01b1:
            o.Request r2 = r5.a(r4, r6)
        L_0x01b5:
            if (r2 != 0) goto L_0x01d5
            if (r1 == 0) goto L_0x01d4
            boolean r1 = r1.a
            if (r1 == 0) goto L_0x01d4
            boolean r1 = r10.f2970l
            r1 = r1 ^ r0
            if (r1 == 0) goto L_0x01ca
            r10.f2970l = r0
            o.m0.d.Transmitter$b r0 = r10.c
            r0.g()
            goto L_0x01d4
        L_0x01ca:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = r28.toString()
            r0.<init>(r1)
            throw r0
        L_0x01d4:
            return r4
        L_0x01d5:
            o.ResponseBody r6 = r4.f2902i
            if (r6 == 0) goto L_0x01dc
            o.m0.Util.a(r6)
        L_0x01dc:
            boolean r6 = r10.d()
            if (r6 == 0) goto L_0x01ef
            if (r1 == 0) goto L_0x01ef
            o.m0.e.ExchangeCode r6 = r1.f2940f
            r6.cancel()
            o.m0.d.Transmitter r6 = r1.b
            r7 = 0
            r6.a(r1, r0, r0, r7)
        L_0x01ef:
            int r12 = r30 + 1
            r0 = 20
            if (r12 > r0) goto L_0x01f8
            r11 = r2
            r2 = r4
            goto L_0x022e
        L_0x01f8:
            java.net.ProtocolException r0 = new java.net.ProtocolException
            java.lang.String r1 = "Too many follow-up requests: "
            java.lang.String r1 = j.a.a.a.outline.b(r1, r12)
            r0.<init>(r1)
            throw r0
        L_0x0204:
            r0 = move-exception
            r5 = r29
            goto L_0x0236
        L_0x0208:
            r0 = move-exception
            r5 = r29
            r1 = r0
            boolean r0 = r1 instanceof okhttp3.internal.http2.ConnectionShutdownException     // Catch:{ all -> 0x0235 }
            if (r0 != 0) goto L_0x0213
            r0 = 1
            goto L_0x0214
        L_0x0213:
            r0 = 0
        L_0x0214:
            boolean r0 = r5.a(r1, r10, r0, r11)     // Catch:{ all -> 0x0235 }
            if (r0 == 0) goto L_0x021b
            goto L_0x0229
        L_0x021b:
            throw r1     // Catch:{ all -> 0x0235 }
        L_0x021c:
            r0 = move-exception
            r5 = r29
            r1 = r0
            java.io.IOException r0 = r1.b     // Catch:{ all -> 0x0235 }
            r4 = 0
            boolean r0 = r5.a(r0, r10, r4, r11)     // Catch:{ all -> 0x0235 }
            if (r0 == 0) goto L_0x0232
        L_0x0229:
            r10.c()
            r12 = r30
        L_0x022e:
            r0 = 0
            r1 = r5
            goto L_0x0013
        L_0x0232:
            java.io.IOException r0 = r1.c     // Catch:{ all -> 0x0235 }
            throw r0     // Catch:{ all -> 0x0235 }
        L_0x0235:
            r0 = move-exception
        L_0x0236:
            r10.c()
            throw r0
        L_0x023a:
            r5 = r29
            java.io.IOException r0 = new java.io.IOException
            java.lang.String r1 = "Canceled"
            r0.<init>(r1)
            throw r0
        L_0x0244:
            r5 = r1
            java.lang.String r0 = "request"
            n.n.c.Intrinsics.a(r0)
            r0 = 0
            throw r0
        L_0x024c:
            r5 = r1
            java.lang.String r1 = "chain"
            n.n.c.Intrinsics.a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.e.RetryAndFollowUpInterceptor.a(o.Interceptor$a):o.Response");
    }

    /* JADX WARNING: Removed duplicated region for block: B:37:0x0058 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:38:0x0059 A[RETURN] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(java.io.IOException r3, o.m0.d.Transmitter r4, boolean r5, o.Request r6) {
        /*
            r2 = this;
            o.OkHttpClient r0 = r2.b
            boolean r0 = r0.g
            r1 = 0
            if (r0 != 0) goto L_0x0008
            return r1
        L_0x0008:
            if (r5 == 0) goto L_0x0011
            o.RequestBody r6 = r6.f2896e
            boolean r6 = r3 instanceof java.io.FileNotFoundException
            if (r6 == 0) goto L_0x0011
            return r1
        L_0x0011:
            boolean r6 = r3 instanceof java.net.ProtocolException
            r0 = 1
            if (r6 == 0) goto L_0x0017
            goto L_0x0023
        L_0x0017:
            boolean r6 = r3 instanceof java.io.InterruptedIOException
            if (r6 == 0) goto L_0x0025
            boolean r3 = r3 instanceof java.net.SocketTimeoutException
            if (r3 == 0) goto L_0x0023
            if (r5 != 0) goto L_0x0023
        L_0x0021:
            r3 = 1
            goto L_0x0037
        L_0x0023:
            r3 = 0
            goto L_0x0037
        L_0x0025:
            boolean r5 = r3 instanceof javax.net.ssl.SSLHandshakeException
            if (r5 == 0) goto L_0x0032
            java.lang.Throwable r5 = r3.getCause()
            boolean r5 = r5 instanceof java.security.cert.CertificateException
            if (r5 == 0) goto L_0x0032
            goto L_0x0023
        L_0x0032:
            boolean r3 = r3 instanceof javax.net.ssl.SSLPeerUnverifiedException
            if (r3 == 0) goto L_0x0021
            goto L_0x0023
        L_0x0037:
            if (r3 != 0) goto L_0x003a
            return r1
        L_0x003a:
            o.m0.d.ExchangeFinder r3 = r4.f2966f
            r5 = 0
            if (r3 == 0) goto L_0x005a
            boolean r3 = r3.b()
            if (r3 == 0) goto L_0x0055
            o.m0.d.ExchangeFinder r3 = r4.f2966f
            if (r3 == 0) goto L_0x0051
            boolean r3 = r3.a()
            if (r3 == 0) goto L_0x0055
            r3 = 1
            goto L_0x0056
        L_0x0051:
            n.n.c.Intrinsics.a()
            throw r5
        L_0x0055:
            r3 = 0
        L_0x0056:
            if (r3 != 0) goto L_0x0059
            return r1
        L_0x0059:
            return r0
        L_0x005a:
            n.n.c.Intrinsics.a()
            throw r5
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.e.RetryAndFollowUpInterceptor.a(java.io.IOException, o.m0.d.Transmitter, boolean, o.Request):boolean");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public final Request a(Response response, String str) {
        String a;
        HttpUrl b2;
        RequestBody requestBody = null;
        if (!this.b.f2868i || (a = Response.a(response, "Location", null, 2)) == null || (b2 = response.c.b.b(a)) == null) {
            return null;
        }
        if (!Intrinsics.a((Object) b2.b, (Object) response.c.b.b) && !this.b.f2869j) {
            return null;
        }
        Request request = response.c;
        if (request != null) {
            Request.a aVar = new Request.a(request);
            if (HttpMethod.a(str)) {
                boolean a2 = Intrinsics.a((Object) str, (Object) "PROPFIND");
                if (!Intrinsics.a((Object) str, (Object) "PROPFIND")) {
                    aVar.a("GET", (RequestBody) null);
                } else {
                    if (a2) {
                        requestBody = response.c.f2896e;
                    }
                    aVar.a(str, requestBody);
                }
                if (!a2) {
                    aVar.a("Transfer-Encoding");
                    aVar.a("Content-Length");
                    aVar.a("Content-Type");
                }
            }
            if (!Util.a(response.c.b, b2)) {
                aVar.a("Authorization");
            }
            aVar.a = b2;
            return aVar.a();
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Integer, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final int a(Response response, int i2) {
        String a = Response.a(response, "Retry-After", null, 2);
        if (a == null) {
            return i2;
        }
        if (!new Regex("\\d+").a(a)) {
            return Integer.MAX_VALUE;
        }
        Integer valueOf = Integer.valueOf(a);
        Intrinsics.a((Object) valueOf, "Integer.valueOf(header)");
        return valueOf.intValue();
    }
}
