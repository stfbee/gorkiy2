package o.m0;

import n.n.c.Intrinsics;
import o.Call;
import o.EventListener;

/* compiled from: Util.kt */
public final class Util0 implements EventListener.b {
    public final /* synthetic */ EventListener a;

    public Util0(EventListener eventListener) {
        this.a = eventListener;
    }

    public EventListener a(Call call) {
        if (call != null) {
            return this.a;
        }
        Intrinsics.a("call");
        throw null;
    }
}
