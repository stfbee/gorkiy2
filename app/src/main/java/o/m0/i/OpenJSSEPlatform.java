package o.m0.i;

import j.a.a.a.outline;
import java.security.KeyStore;
import java.security.Provider;
import java.util.Arrays;
import java.util.List;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocket;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;
import kotlin.TypeCastException;
import n.n.c.Intrinsics;
import o.c0;
import org.openjsse.javax.net.ssl.SSLParameters;
import org.openjsse.net.ssl.OpenJSSE;

/* compiled from: OpenJSSEPlatform.kt */
public final class OpenJSSEPlatform extends Platform {

    /* renamed from: e  reason: collision with root package name */
    public static final boolean f3051e;

    /* renamed from: f  reason: collision with root package name */
    public static final OpenJSSEPlatform f3052f = null;
    public final Provider d = new OpenJSSE();

    static {
        boolean z;
        try {
            Class.forName("org.openjsse.net.ssl.OpenJSSE");
            z = true;
        } catch (ClassNotFoundException unused) {
            z = false;
        }
        f3051e = z;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.SSLContext, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public SSLContext a() {
        SSLContext instance = SSLContext.getInstance("TLSv1.3", this.d);
        Intrinsics.a((Object) instance, "SSLContext.getInstance(\"TLSv1.3\", provider)");
        return instance;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [javax.net.ssl.TrustManagerFactory, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public X509TrustManager b() {
        TrustManagerFactory instance = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm(), this.d);
        instance.init((KeyStore) null);
        Intrinsics.a((Object) instance, "factory");
        TrustManager[] trustManagers = instance.getTrustManagers();
        if (trustManagers != null) {
            boolean z = true;
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                z = false;
            }
            if (z) {
                TrustManager trustManager = trustManagers[0];
                if (trustManager != null) {
                    return (X509TrustManager) trustManager;
                }
                throw new TypeCastException("null cannot be cast to non-null type javax.net.ssl.X509TrustManager");
            }
            StringBuilder a = outline.a("Unexpected default trust managers: ");
            String arrays = Arrays.toString(trustManagers);
            Intrinsics.a((Object) arrays, "java.util.Arrays.toString(this)");
            a.append(arrays);
            throw new IllegalStateException(a.toString().toString());
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: o.m0.i.Platform.a(javax.net.ssl.SSLSocket, java.lang.String, java.util.List<o.c0>):void
     arg types: [javax.net.ssl.SSLSocket, java.lang.String, java.util.List<? extends o.c0>]
     candidates:
      o.m0.i.Platform.a(int, java.lang.String, java.lang.Throwable):void
      o.m0.i.Platform.a(java.net.Socket, java.net.InetSocketAddress, int):void
      o.m0.i.Platform.a(javax.net.ssl.SSLSocket, java.lang.String, java.util.List<o.c0>):void */
    public void a(SSLSocket sSLSocket, String str, List<? extends c0> list) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (list == null) {
            Intrinsics.a("protocols");
            throw null;
        } else if (sSLSocket instanceof org.openjsse.javax.net.ssl.SSLSocket) {
            org.openjsse.javax.net.ssl.SSLSocket sSLSocket2 = (org.openjsse.javax.net.ssl.SSLSocket) sSLSocket;
            SSLParameters sSLParameters = sSLSocket2.getSSLParameters();
            if (sSLParameters instanceof SSLParameters) {
                SSLParameters sSLParameters2 = sSLParameters;
                Object[] array = Platform.c.a(list).toArray(new String[0]);
                if (array != null) {
                    sSLParameters2.setApplicationProtocols((String[]) array);
                    sSLSocket2.setSSLParameters(sSLParameters);
                    return;
                }
                throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
            }
        } else {
            super.a(sSLSocket, str, (List<c0>) list);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public String b(SSLSocket sSLSocket) {
        if (sSLSocket == null) {
            Intrinsics.a("sslSocket");
            throw null;
        } else if (sSLSocket instanceof org.openjsse.javax.net.ssl.SSLSocket) {
            String applicationProtocol = ((org.openjsse.javax.net.ssl.SSLSocket) sSLSocket).getApplicationProtocol();
            if (applicationProtocol != null && !Intrinsics.a((Object) applicationProtocol, (Object) "")) {
                return applicationProtocol;
            }
            return null;
        } else {
            super.b(sSLSocket);
            return null;
        }
    }
}
