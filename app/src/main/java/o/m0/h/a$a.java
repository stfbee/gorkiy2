package o.m0.h;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import n.i.Collections;
import n.n.c.Intrinsics;
import p.Okio0;
import p.Sink;
import p.Source;
import p.Timeout;

/* compiled from: FileSystem.kt */
public final class a$a implements FileSystem {
    public void a(File file) {
        if (file == null) {
            Intrinsics.a("file");
            throw null;
        } else if (!file.delete() && file.exists()) {
            throw new IOException("failed to delete " + file);
        }
    }

    public Source b(File file) {
        if (file != null) {
            return new Okio0(new FileInputStream(file), new Timeout());
        }
        Intrinsics.a("file");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.io.File, boolean, int):p.Sink
     arg types: [java.io.File, int, int]
     candidates:
      n.i.Collections.a(java.lang.String, java.lang.String, int):java.lang.Integer
      n.i.Collections.a(int, java.lang.String, int):java.lang.String
      n.i.Collections.a(java.lang.String, int, int):java.net.InetAddress
      n.i.Collections.a(int, java.lang.String, java.lang.Throwable):void
      n.i.Collections.a(int, byte[], int):void
      n.i.Collections.a(long, long, long):void
      n.i.Collections.a(android.content.SharedPreferences, java.lang.String, java.lang.Object):void
      n.i.Collections.a(java.lang.Appendable, java.lang.Object, n.n.b.Functions0):void
      n.i.Collections.a(char, char, boolean):boolean
      n.i.Collections.a(java.io.File, boolean, int):p.Sink */
    public Sink c(File file) {
        if (file != null) {
            try {
                return Collections.a(file, false, 1);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return Collections.a(file, false, 1);
            }
        } else {
            Intrinsics.a("file");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.File, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void d(File file) {
        if (file != null) {
            File[] listFiles = file.listFiles();
            if (listFiles != null) {
                int length = listFiles.length;
                int i2 = 0;
                while (i2 < length) {
                    File file2 = listFiles[i2];
                    Intrinsics.a((Object) file2, "file");
                    if (file2.isDirectory()) {
                        d(file2);
                    }
                    if (file2.delete()) {
                        i2++;
                    } else {
                        throw new IOException("failed to delete " + file2);
                    }
                }
                return;
            }
            throw new IOException("not a readable directory: " + file);
        }
        Intrinsics.a("directory");
        throw null;
    }

    public Sink e(File file) {
        if (file != null) {
            try {
                return Collections.a(file);
            } catch (FileNotFoundException unused) {
                file.getParentFile().mkdirs();
                return Collections.a(file);
            }
        } else {
            Intrinsics.a("file");
            throw null;
        }
    }

    public boolean f(File file) {
        if (file != null) {
            return file.exists();
        }
        Intrinsics.a("file");
        throw null;
    }

    public long g(File file) {
        if (file != null) {
            return file.length();
        }
        Intrinsics.a("file");
        throw null;
    }

    public void a(File file, File file2) {
        if (file == null) {
            Intrinsics.a("from");
            throw null;
        } else if (file2 != null) {
            a(file2);
            if (!file.renameTo(file2)) {
                throw new IOException("failed to rename " + file + " to " + file2);
            }
        } else {
            Intrinsics.a("to");
            throw null;
        }
    }
}
