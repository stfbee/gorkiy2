package o.m0;

import com.crashlytics.android.answers.RetryManager;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import j.a.a.a.outline;
import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import kotlin.TypeCastException;
import n.i.Maps;
import n.i._Arrays;
import n.n.c.Intrinsics;
import n.r.Charsets;
import n.r.Indent;
import n.r.Regex;
import o.Headers;
import o.HttpUrl;
import o.RequestBody;
import o.Response;
import o.ResponseBody;
import o.ResponseBody0;
import o.m0.g.Header;
import o.m0.g.b;
import o.w;
import p.Buffer;
import p.BufferedSink;
import p.BufferedSource;
import p.ByteString;
import p.Options;
import p.Source;

/* compiled from: Util.kt */
public final class Util {
    public static final byte[] a = new byte[0];
    public static final Headers b = Headers.c.a(new String[0]);
    public static final ResponseBody c;
    public static final Options d = Options.d.a(ByteString.f3063f.a("efbbbf"), ByteString.f3063f.a("feff"), ByteString.f3063f.a("fffe"), ByteString.f3063f.a("0000ffff"), ByteString.f3063f.a("ffff0000"));

    /* renamed from: e  reason: collision with root package name */
    public static final TimeZone f2919e;

    /* renamed from: f  reason: collision with root package name */
    public static final Regex f2920f = new Regex("([0-9a-fA-F]*:[0-9a-fA-F:.]*)|([\\d.]+)");

    /* compiled from: Util.kt */
    public static final class a implements ThreadFactory {
        public final /* synthetic */ String b;
        public final /* synthetic */ boolean c;

        public a(String str, boolean z) {
            this.b = str;
            this.c = z;
        }

        public final Thread newThread(Runnable runnable) {
            Thread thread = new Thread(runnable, this.b);
            thread.setDaemon(this.c);
            return thread;
        }
    }

    static {
        ResponseBody.b bVar = ResponseBody.c;
        byte[] bArr = a;
        if (bArr != null) {
            Buffer buffer = new Buffer();
            buffer.write(bArr);
            c = new ResponseBody0(buffer, null, (long) bArr.length);
            RequestBody.a.a(RequestBody.a, a, null, 0, 0, 7);
            TimeZone timeZone = TimeZone.getTimeZone("GMT");
            if (timeZone != null) {
                f2919e = timeZone;
                return;
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("$this$toResponseBody");
        throw null;
    }

    public static final int a(byte b2, int i2) {
        return b2 & i2;
    }

    public static final int a(char c2) {
        if ('0' <= c2 && '9' >= c2) {
            return c2 - '0';
        }
        char c3 = 'a';
        if ('a' > c2 || 'f' < c2) {
            c3 = 'A';
            if ('A' > c2 || 'F' < c2) {
                return -1;
            }
        }
        return (c2 - c3) + 10;
    }

    public static final long a(Response response) {
        if (response != null) {
            String a2 = response.h.a("Content-Length");
            if (a2 == null) {
                return -1;
            }
            try {
                return Long.parseLong(a2);
            } catch (NumberFormatException unused) {
                return -1;
            }
        } else {
            Intrinsics.a("$this$headersContentLength");
            throw null;
        }
    }

    public static final String[] b(String[] strArr, String[] strArr2, Comparator<? super String> comparator) {
        if (strArr == null) {
            Intrinsics.a("$this$intersect");
            throw null;
        } else if (strArr2 == null) {
            Intrinsics.a("other");
            throw null;
        } else if (comparator != null) {
            ArrayList arrayList = new ArrayList();
            for (String str : strArr) {
                int length = strArr2.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length) {
                        break;
                    } else if (comparator.compare(str, strArr2[i2]) == 0) {
                        arrayList.add(str);
                        break;
                    } else {
                        i2++;
                    }
                }
            }
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                return (String[]) array;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        } else {
            Intrinsics.a("comparator");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String c(String str, int i2, int i3) {
        if (str != null) {
            int a2 = a(str, i2, i3);
            String substring = str.substring(a2, b(str, a2, i3));
            Intrinsics.a((Object) substring, "(this as java.lang.Strin…ing(startIndex, endIndex)");
            return substring;
        }
        Intrinsics.a("$this$trimSubstring");
        throw null;
    }

    public static final void a(long j2, long j3, long j4) {
        if ((j3 | j4) < 0 || j3 > j2 || j2 - j3 < j4) {
            throw new ArrayIndexOutOfBoundsException();
        }
    }

    public static final ThreadFactory a(String str, boolean z) {
        if (str != null) {
            return new a(str, z);
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public static final boolean a(String[] strArr, String[] strArr2, Comparator<? super String> comparator) {
        if (strArr == null) {
            Intrinsics.a("$this$hasIntersection");
            throw null;
        } else if (comparator != null) {
            if (!(strArr.length == 0) && strArr2 != null) {
                if (!(strArr2.length == 0)) {
                    for (String str : strArr) {
                        for (String compare : strArr2) {
                            if (comparator.compare(str, compare) == 0) {
                                return true;
                            }
                        }
                    }
                }
            }
            return false;
        } else {
            Intrinsics.a("comparator");
            throw null;
        }
    }

    public static final int b(String str, int i2, int i3) {
        if (str != null) {
            int i4 = i3 - 1;
            if (i4 >= i2) {
                while (true) {
                    char charAt = str.charAt(i4);
                    if (charAt == 9 || charAt == 10 || charAt == 12 || charAt == 13 || charAt == ' ') {
                        if (i4 == i2) {
                            break;
                        }
                        i4--;
                    } else {
                        return i4 + 1;
                    }
                }
            }
            return i2;
        }
        Intrinsics.a("$this$indexOfLastNonAsciiWhitespace");
        throw null;
    }

    public static final int b(String str) {
        if (str != null) {
            int length = str.length();
            for (int i2 = 0; i2 < length; i2++) {
                char charAt = str.charAt(i2);
                if (charAt <= 31 || charAt >= 127) {
                    return i2;
                }
            }
            return -1;
        }
        Intrinsics.a("$this$indexOfControlOrNonAscii");
        throw null;
    }

    public static /* synthetic */ int a(String str, int i2, int i3, int i4) {
        if ((i4 & 1) != 0) {
            i2 = 0;
        }
        if ((i4 & 2) != 0) {
            i3 = str.length();
        }
        return a(str, i2, i3);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x004e, code lost:
        if (r5 == androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L_0x0050;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:15:0x0050, code lost:
        r11.b().a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:16:0x0058, code lost:
        r11.b().a(r0 + r5);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:25:0x007b, code lost:
        if (r5 != androidx.recyclerview.widget.RecyclerView.FOREVER_NS) goto L_0x0058;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x007e, code lost:
        return r12;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean b(p.Source r11, int r12, java.util.concurrent.TimeUnit r13) {
        /*
            r0 = 0
            if (r11 == 0) goto L_0x0085
            if (r13 == 0) goto L_0x007f
            long r0 = java.lang.System.nanoTime()
            p.Timeout r2 = r11.b()
            boolean r2 = r2.d()
            r3 = 9223372036854775807(0x7fffffffffffffff, double:NaN)
            if (r2 == 0) goto L_0x0022
            p.Timeout r2 = r11.b()
            long r5 = r2.c()
            long r5 = r5 - r0
            goto L_0x0023
        L_0x0022:
            r5 = r3
        L_0x0023:
            p.Timeout r2 = r11.b()
            long r7 = (long) r12
            long r12 = r13.toNanos(r7)
            long r12 = java.lang.Math.min(r5, r12)
            long r12 = r12 + r0
            r2.a(r12)
            p.Buffer r12 = new p.Buffer     // Catch:{ InterruptedIOException -> 0x0077, all -> 0x0061 }
            r12.<init>()     // Catch:{ InterruptedIOException -> 0x0077, all -> 0x0061 }
        L_0x0039:
            r7 = 8192(0x2000, double:4.0474E-320)
            long r7 = r11.b(r12, r7)     // Catch:{ InterruptedIOException -> 0x0077, all -> 0x0061 }
            r9 = -1
            int r13 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r13 == 0) goto L_0x004b
            long r7 = r12.c     // Catch:{ InterruptedIOException -> 0x0077, all -> 0x0061 }
            r12.skip(r7)     // Catch:{ InterruptedIOException -> 0x0077, all -> 0x0061 }
            goto L_0x0039
        L_0x004b:
            r12 = 1
            int r13 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r13 != 0) goto L_0x0058
        L_0x0050:
            p.Timeout r11 = r11.b()
            r11.a()
            goto L_0x007e
        L_0x0058:
            p.Timeout r11 = r11.b()
            long r0 = r0 + r5
            r11.a(r0)
            goto L_0x007e
        L_0x0061:
            r12 = move-exception
            int r13 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r13 != 0) goto L_0x006e
            p.Timeout r11 = r11.b()
            r11.a()
            goto L_0x0076
        L_0x006e:
            p.Timeout r11 = r11.b()
            long r0 = r0 + r5
            r11.a(r0)
        L_0x0076:
            throw r12
        L_0x0077:
            r12 = 0
            int r13 = (r5 > r3 ? 1 : (r5 == r3 ? 0 : -1))
            if (r13 != 0) goto L_0x0058
            goto L_0x0050
        L_0x007e:
            return r12
        L_0x007f:
            java.lang.String r11 = "timeUnit"
            n.n.c.Intrinsics.a(r11)
            throw r0
        L_0x0085:
            java.lang.String r11 = "$this$skipAll"
            n.n.c.Intrinsics.a(r11)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.Util.b(p.Source, int, java.util.concurrent.TimeUnit):boolean");
    }

    public static final int a(String str, int i2, int i3) {
        if (str != null) {
            while (i2 < i3) {
                char charAt = str.charAt(i2);
                if (charAt != 9 && charAt != 10 && charAt != 12 && charAt != 13 && charAt != ' ') {
                    return i2;
                }
                i2++;
            }
            return i3;
        }
        Intrinsics.a("$this$indexOfFirstNonAsciiWhitespace");
        throw null;
    }

    public static final w a(List<b> list) {
        if (list != null) {
            ArrayList arrayList = new ArrayList(20);
            for (Header next : list) {
                ByteString byteString = next.b;
                ByteString byteString2 = next.c;
                if (byteString != null) {
                    String h = p.b0.ByteString.h(byteString);
                    if (byteString2 != null) {
                        String h2 = p.b0.ByteString.h(byteString2);
                        if (h == null) {
                            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
                            throw null;
                        } else if (h2 != null) {
                            arrayList.add(h);
                            arrayList.add(Indent.c(h2).toString());
                        } else {
                            Intrinsics.a("value");
                            throw null;
                        }
                    } else {
                        throw null;
                    }
                } else {
                    throw null;
                }
            }
            Object[] array = arrayList.toArray(new String[0]);
            if (array != null) {
                return new Headers((String[]) array, null);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        Intrinsics.a("$this$toHeaders");
        throw null;
    }

    public static final int b(String str, int i2) {
        if (str != null) {
            try {
                long parseLong = Long.parseLong(str);
                if (parseLong > ((long) Integer.MAX_VALUE)) {
                    return Integer.MAX_VALUE;
                }
                if (parseLong < 0) {
                    return 0;
                }
                return (int) parseLong;
            } catch (NumberFormatException unused) {
            }
        }
        return i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T> List<T> b(List<? extends T> list) {
        if (list != null) {
            List<T> unmodifiableList = Collections.unmodifiableList(_Arrays.a((Collection) list));
            Intrinsics.a((Object) unmodifiableList, "Collections.unmodifiableList(toMutableList())");
            return unmodifiableList;
        }
        Intrinsics.a("$this$toImmutableList");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
     arg types: [java.lang.String, char, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean */
    public static final int a(String str, String str2, int i2, int i3) {
        if (str == null) {
            Intrinsics.a("$this$delimiterOffset");
            throw null;
        } else if (str2 != null) {
            while (i2 < i3) {
                if (Indent.a((CharSequence) str2, str.charAt(i2), false, 2)) {
                    return i2;
                }
                i2++;
            }
            return i3;
        } else {
            Intrinsics.a("delimiters");
            throw null;
        }
    }

    public static /* synthetic */ int a(String str, char c2, int i2, int i3, int i4) {
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            i3 = str.length();
        }
        return a(str, c2, i2, i3);
    }

    public static final int a(String str, char c2, int i2, int i3) {
        if (str != null) {
            while (i2 < i3) {
                if (str.charAt(i2) == c2) {
                    return i2;
                }
                i2++;
            }
            return i3;
        }
        Intrinsics.a("$this$delimiterOffset");
        throw null;
    }

    public static final boolean a(String str) {
        if (str != null) {
            return f2920f.a(str);
        }
        Intrinsics.a("$this$canParseAsIpAddress");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final String a(String str, Object... objArr) {
        if (str == null) {
            Intrinsics.a("format");
            throw null;
        } else if (objArr != null) {
            Locale locale = Locale.US;
            Intrinsics.a((Object) locale, "Locale.US");
            Object[] copyOf = Arrays.copyOf(objArr, objArr.length);
            String format = String.format(locale, str, Arrays.copyOf(copyOf, copyOf.length));
            Intrinsics.a((Object) format, "java.lang.String.format(locale, format, *args)");
            return format;
        } else {
            Intrinsics.a("args");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.nio.charset.Charset, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final Charset a(BufferedSource bufferedSource, Charset charset) {
        Charset charset2;
        if (bufferedSource == null) {
            Intrinsics.a("$this$readBomAsCharset");
            throw null;
        } else if (charset != null) {
            int a2 = bufferedSource.a(d);
            if (a2 == -1) {
                return charset;
            }
            if (a2 == 0) {
                Charset charset3 = StandardCharsets.UTF_8;
                Intrinsics.a((Object) charset3, "UTF_8");
                return charset3;
            } else if (a2 == 1) {
                Charset charset4 = StandardCharsets.UTF_16BE;
                Intrinsics.a((Object) charset4, "UTF_16BE");
                return charset4;
            } else if (a2 != 2) {
                if (a2 == 3) {
                    Charsets charsets = Charsets.d;
                    charset2 = Charsets.c;
                    if (charset2 == null) {
                        charset2 = Charset.forName("UTF-32BE");
                        Intrinsics.a((Object) charset2, "Charset.forName(\"UTF-32BE\")");
                        Charsets.c = charset2;
                    }
                } else if (a2 == 4) {
                    Charsets charsets2 = Charsets.d;
                    charset2 = Charsets.b;
                    if (charset2 == null) {
                        charset2 = Charset.forName("UTF-32LE");
                        Intrinsics.a((Object) charset2, "Charset.forName(\"UTF-32LE\")");
                        Charsets.b = charset2;
                    }
                } else {
                    throw new AssertionError();
                }
                return charset2;
            } else {
                Charset charset5 = StandardCharsets.UTF_16LE;
                Intrinsics.a((Object) charset5, "UTF_16LE");
                return charset5;
            }
        } else {
            Intrinsics.a("default");
            throw null;
        }
    }

    public static final int a(String str, long j2, TimeUnit timeUnit) {
        if (str != null) {
            boolean z = true;
            int i2 = (j2 > 0 ? 1 : (j2 == 0 ? 0 : -1));
            if (i2 >= 0) {
                if (timeUnit != null) {
                    long millis = timeUnit.toMillis(j2);
                    if (millis <= ((long) Integer.MAX_VALUE)) {
                        if (millis == 0 && i2 > 0) {
                            z = false;
                        }
                        if (z) {
                            return (int) millis;
                        }
                        throw new IllegalArgumentException(outline.a(str, " too small.").toString());
                    }
                    throw new IllegalArgumentException(outline.a(str, " too large.").toString());
                }
                throw new IllegalStateException("unit == null".toString());
            }
            throw new IllegalStateException(outline.a(str, " < 0").toString());
        }
        Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
        throw null;
    }

    public static final void a(BufferedSink bufferedSink, int i2) {
        if (bufferedSink != null) {
            bufferedSink.writeByte((i2 >>> 16) & 255);
            bufferedSink.writeByte((i2 >>> 8) & 255);
            bufferedSink.writeByte(i2 & 255);
            return;
        }
        Intrinsics.a("$this$writeMedium");
        throw null;
    }

    public static final int a(BufferedSource bufferedSource) {
        if (bufferedSource != null) {
            return (bufferedSource.readByte() & 255) | ((bufferedSource.readByte() & 255) << 16) | ((bufferedSource.readByte() & 255) << 8);
        }
        Intrinsics.a("$this$readMedium");
        throw null;
    }

    public static final boolean a(HttpUrl httpUrl, HttpUrl httpUrl2) {
        if (httpUrl == null) {
            Intrinsics.a("$this$canReuseConnectionFor");
            throw null;
        } else if (httpUrl2 != null) {
            return Intrinsics.a(httpUrl.f2851e, httpUrl2.f2851e) && httpUrl.f2852f == httpUrl2.f2852f && Intrinsics.a(httpUrl.b, httpUrl2.b);
        } else {
            Intrinsics.a("other");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean */
    public static final String a(HttpUrl httpUrl, boolean z) {
        String str;
        if (httpUrl != null) {
            if (Indent.a((CharSequence) httpUrl.f2851e, (CharSequence) ":", false, 2)) {
                str = '[' + httpUrl.f2851e + ']';
            } else {
                str = httpUrl.f2851e;
            }
            if (!z && httpUrl.f2852f == HttpUrl.f2850l.a(httpUrl.b)) {
                return str;
            }
            return str + ':' + httpUrl.f2852f;
        }
        Intrinsics.a("$this$toHostHeader");
        throw null;
    }

    public static final boolean a(Source source, int i2, TimeUnit timeUnit) {
        if (source == null) {
            Intrinsics.a("$this$discard");
            throw null;
        } else if (timeUnit != null) {
            try {
                return b(source, i2, timeUnit);
            } catch (IOException unused) {
                return false;
            }
        } else {
            Intrinsics.a("timeUnit");
            throw null;
        }
    }

    public static final int a(String str, int i2) {
        if (str != null) {
            int length = str.length();
            while (i2 < length) {
                char charAt = str.charAt(i2);
                if (charAt != ' ' && charAt != 9) {
                    return i2;
                }
                i2++;
            }
            return str.length();
        }
        Intrinsics.a("$this$indexOfNonWhitespace");
        throw null;
    }

    public static final long a(String str, long j2) {
        if (str != null) {
            try {
                return Long.parseLong(str);
            } catch (NumberFormatException unused) {
                return j2;
            }
        } else {
            Intrinsics.a("$this$toLongOrDefault");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    @SafeVarargs
    public static final <T> List<T> a(Object... objArr) {
        if (objArr != null) {
            Object[] objArr2 = (Object[]) objArr.clone();
            List<T> unmodifiableList = Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(objArr2, objArr2.length)));
            Intrinsics.a((Object) unmodifiableList, "Collections.unmodifiable…sList(*elements.clone()))");
            return unmodifiableList;
        }
        Intrinsics.a("elements");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Map<K, V>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <K, V> Map<K, V> a(Map map) {
        if (map == null) {
            Intrinsics.a("$this$toImmutableMap");
            throw null;
        } else if (map.isEmpty()) {
            return Maps.b;
        } else {
            Map<K, V> unmodifiableMap = Collections.unmodifiableMap(new LinkedHashMap(map));
            Intrinsics.a((Object) unmodifiableMap, "Collections.unmodifiableMap(LinkedHashMap(this))");
            return unmodifiableMap;
        }
    }

    public static final void a(Closeable closeable) {
        if (closeable != null) {
            try {
                closeable.close();
            } catch (RuntimeException e2) {
                throw e2;
            } catch (Exception unused) {
            }
        } else {
            Intrinsics.a("$this$closeQuietly");
            throw null;
        }
    }

    public static final void a(Socket socket) {
        if (socket != null) {
            try {
                socket.close();
            } catch (AssertionError e2) {
                throw e2;
            } catch (RuntimeException e3) {
                throw e3;
            } catch (Exception unused) {
            }
        } else {
            Intrinsics.a("$this$closeQuietly");
            throw null;
        }
    }

    public static final void a(Object obj, long j2) {
        if (obj != null) {
            long j3 = j2 / RetryManager.NANOSECONDS_IN_MS;
            long j4 = j2 - (RetryManager.NANOSECONDS_IN_MS * j3);
            synchronized (obj) {
                int i2 = (int) j4;
                if (j3 > 0 || i2 > 0) {
                    obj.wait(j3, i2);
                }
            }
            return;
        }
        Intrinsics.a("$this$lockAndWaitNanos");
        throw null;
    }

    public static final int a(String[] strArr, String str, Comparator<String> comparator) {
        if (strArr == null) {
            Intrinsics.a("$this$indexOf");
            throw null;
        } else if (str == null) {
            Intrinsics.a("value");
            throw null;
        } else if (comparator != null) {
            int length = strArr.length;
            for (int i2 = 0; i2 < length; i2++) {
                if (comparator.compare(strArr[i2], str) == 0) {
                    return i2;
                }
            }
            return -1;
        } else {
            Intrinsics.a("comparator");
            throw null;
        }
    }
}
