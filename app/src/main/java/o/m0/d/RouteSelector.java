package o.m0.d;

import j.c.a.a.c.n.c;
import java.net.InetSocketAddress;
import java.net.Proxy;
import java.util.ArrayList;
import java.util.List;
import n.i.Collections2;
import n.n.c.Intrinsics;
import o.Address;
import o.Call;
import o.EventListener;
import o.HttpUrl;
import o.k0;
import o.m0.Util;

/* compiled from: RouteSelector.kt */
public final class RouteSelector {
    public List<? extends Proxy> a;
    public int b;
    public List<? extends InetSocketAddress> c;
    public final List<k0> d;

    /* renamed from: e  reason: collision with root package name */
    public final Address f2963e;

    /* renamed from: f  reason: collision with root package name */
    public final RouteDatabase f2964f;
    public final Call g;
    public final EventListener h;

    /* compiled from: RouteSelector.kt */
    public static final class a {
        public int a;
        public final List<k0> b;

        public a(List<k0> list) {
            if (list != null) {
                this.b = list;
            } else {
                Intrinsics.a("routes");
                throw null;
            }
        }

        public final boolean a() {
            return this.a < this.b.size();
        }
    }

    public RouteSelector(Address address, RouteDatabase routeDatabase, Call call, EventListener eventListener) {
        List<? extends Proxy> list;
        if (address == null) {
            Intrinsics.a("address");
            throw null;
        } else if (routeDatabase == null) {
            Intrinsics.a("routeDatabase");
            throw null;
        } else if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (eventListener != null) {
            this.f2963e = address;
            this.f2964f = routeDatabase;
            this.g = call;
            this.h = eventListener;
            Collections2 collections2 = Collections2.b;
            this.a = collections2;
            this.c = collections2;
            this.d = new ArrayList();
            Address address2 = this.f2963e;
            HttpUrl httpUrl = address2.a;
            Proxy proxy = address2.f2799j;
            EventListener eventListener2 = this.h;
            Call call2 = this.g;
            if (eventListener2 == null) {
                throw null;
            } else if (call2 == null) {
                Intrinsics.a("call");
                throw null;
            } else if (httpUrl != null) {
                if (proxy != null) {
                    list = c.c(proxy);
                } else {
                    List<Proxy> select = address2.f2800k.select(httpUrl.g());
                    if (select == null || !(!select.isEmpty())) {
                        list = Util.a(Proxy.NO_PROXY);
                    } else {
                        list = Util.b(select);
                    }
                }
                this.a = list;
                this.b = 0;
                EventListener eventListener3 = this.h;
                Call call3 = this.g;
                if (eventListener3 == null) {
                    throw null;
                } else if (call3 == null) {
                    Intrinsics.a("call");
                    throw null;
                } else if (list == null) {
                    Intrinsics.a("proxies");
                    throw null;
                }
            } else {
                Intrinsics.a("url");
                throw null;
            }
        } else {
            Intrinsics.a("eventListener");
            throw null;
        }
    }

    public final boolean a() {
        return b() || (this.d.isEmpty() ^ true);
    }

    public final boolean b() {
        return this.b < this.a.size();
    }
}
