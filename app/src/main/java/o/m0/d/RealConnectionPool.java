package o.m0.d;

import j.a.a.a.outline;
import java.io.IOException;
import java.lang.ref.Reference;
import java.net.Proxy;
import java.security.cert.Certificate;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.SynchronousQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;
import javax.net.ssl.SSLPeerUnverifiedException;
import n.AssertionsJVM;
import n.n.c.Intrinsics;
import o.Address;
import o.CertificatePinner;
import o.CertificatePinner0;
import o.Handshake;
import o.Route;
import o.k0;
import o.m0.Util;
import o.m0.d.Transmitter;
import o.m0.i.Platform;
import o.m0.k.OkHostnameVerifier;

/* compiled from: RealConnectionPool.kt */
public final class RealConnectionPool {
    public static final ThreadPoolExecutor g = new ThreadPoolExecutor(0, Integer.MAX_VALUE, 60, TimeUnit.SECONDS, new SynchronousQueue(), Util.a("OkHttp ConnectionPool", true));
    public final long a;
    public final a b;
    public final ArrayDeque<h> c;
    public final RouteDatabase d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f2961e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2962f;

    /* compiled from: RealConnectionPool.kt */
    public static final class a implements Runnable {
        public final /* synthetic */ RealConnectionPool b;

        public a(RealConnectionPool realConnectionPool) {
            this.b = realConnectionPool;
        }

        public void run() {
            while (true) {
                long a = this.b.a(System.nanoTime());
                if (a != -1) {
                    try {
                        Util.a(this.b, a);
                    } catch (InterruptedException unused) {
                        this.b.a();
                    }
                } else {
                    return;
                }
            }
        }
    }

    public RealConnectionPool(int i2, long j2, TimeUnit timeUnit) {
        if (timeUnit != null) {
            this.f2962f = i2;
            this.a = timeUnit.toNanos(j2);
            this.b = new a(this);
            this.c = new ArrayDeque<>();
            this.d = new RouteDatabase();
            if (!(j2 > 0)) {
                throw new IllegalArgumentException(("keepAliveDuration <= 0: " + j2).toString());
            }
            return;
        }
        Intrinsics.a("timeUnit");
        throw null;
    }

    public final void a(Route route, IOException iOException) {
        if (route == null) {
            Intrinsics.a("failedRoute");
            throw null;
        } else if (iOException != null) {
            if (route.b.type() != Proxy.Type.DIRECT) {
                Address address = route.a;
                address.f2800k.connectFailed(address.a.g(), route.b.address(), iOException);
            }
            this.d.b(route);
        } else {
            Intrinsics.a("failure");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.d.RealConnection, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final boolean a(o.a aVar, l lVar, List<k0> list, boolean z) {
        boolean z2;
        boolean z3;
        if (aVar == null) {
            Intrinsics.a("address");
            throw null;
        } else if (lVar != null) {
            boolean holdsLock = Thread.holdsLock(this);
            if (!AssertionsJVM.a || holdsLock) {
                Iterator<h> it = this.c.iterator();
                while (true) {
                    boolean z4 = false;
                    if (!it.hasNext()) {
                        return false;
                    }
                    RealConnection next = it.next();
                    if (!z || next.b()) {
                        if (next.f2956n.size() < next.f2955m && !next.f2951i && next.f2959q.a.a(aVar)) {
                            if (!Intrinsics.a((Object) aVar.a.f2851e, (Object) next.f2959q.a.a.f2851e)) {
                                if (!(next.f2950f == null || list == null)) {
                                    if (!list.isEmpty()) {
                                        Iterator<T> it2 = list.iterator();
                                        while (true) {
                                            if (!it2.hasNext()) {
                                                break;
                                            }
                                            Route route = (Route) it2.next();
                                            if (route.b.type() == Proxy.Type.DIRECT && next.f2959q.b.type() == Proxy.Type.DIRECT && Intrinsics.a(next.f2959q.c, route.c)) {
                                                z3 = true;
                                                continue;
                                            } else {
                                                z3 = false;
                                                continue;
                                            }
                                            if (z3) {
                                                z2 = true;
                                                break;
                                            }
                                        }
                                    }
                                    z2 = false;
                                    if (z2 && aVar.g == OkHostnameVerifier.a && next.a(aVar.a)) {
                                        try {
                                            CertificatePinner certificatePinner = aVar.h;
                                            if (certificatePinner != null) {
                                                String str = aVar.a.f2851e;
                                                Handshake handshake = next.d;
                                                if (handshake != null) {
                                                    List<Certificate> a2 = handshake.a();
                                                    if (str == null) {
                                                        Intrinsics.a("hostname");
                                                        throw null;
                                                    } else if (a2 != null) {
                                                        certificatePinner.a(str, new CertificatePinner0(certificatePinner, a2, str));
                                                    } else {
                                                        Intrinsics.a("peerCertificates");
                                                        throw null;
                                                    }
                                                } else {
                                                    Intrinsics.a();
                                                    throw null;
                                                }
                                            } else {
                                                Intrinsics.a();
                                                throw null;
                                            }
                                        } catch (SSLPeerUnverifiedException unused) {
                                        }
                                    }
                                }
                            }
                            z4 = true;
                        }
                        if (z4) {
                            Intrinsics.a((Object) next, "connection");
                            lVar.a(next);
                            return true;
                        }
                    }
                }
            } else {
                throw new AssertionError("Assertion failed");
            }
        } else {
            Intrinsics.a("transmitter");
            throw null;
        }
    }

    public final void a(RealConnection realConnection) {
        if (realConnection != null) {
            boolean holdsLock = Thread.holdsLock(this);
            if (!AssertionsJVM.a || holdsLock) {
                if (!this.f2961e) {
                    this.f2961e = true;
                    g.execute(this.b);
                }
                this.c.add(realConnection);
                return;
            }
            throw new AssertionError("Assertion failed");
        }
        Intrinsics.a("connection");
        throw null;
    }

    public final int a(RealConnection realConnection, long j2) {
        List<Reference<l>> list = realConnection.f2956n;
        int i2 = 0;
        while (i2 < list.size()) {
            Reference reference = list.get(i2);
            if (reference.get() != null) {
                i2++;
            } else {
                StringBuilder a2 = outline.a("A connection to ");
                a2.append(realConnection.f2959q.a.a);
                a2.append(" was leaked. ");
                a2.append("Did you forget to close a response body?");
                String sb = a2.toString();
                Platform.a aVar = Platform.c;
                Platform.a.a(sb, ((Transmitter.a) reference).a);
                list.remove(i2);
                realConnection.f2951i = true;
                if (list.isEmpty()) {
                    realConnection.f2957o = j2 - this.a;
                    return 0;
                }
            }
        }
        return list.size();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Iterator<o.m0.d.h>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.d.RealConnection, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void a() {
        ArrayList<RealConnection> arrayList = new ArrayList<>();
        synchronized (this) {
            Iterator<h> it = this.c.iterator();
            Intrinsics.a((Object) it, "connections.iterator()");
            while (it.hasNext()) {
                RealConnection next = it.next();
                if (next.f2956n.isEmpty()) {
                    next.f2951i = true;
                    Intrinsics.a((Object) next, "connection");
                    arrayList.add(next);
                    it.remove();
                }
            }
        }
        for (RealConnection d2 : arrayList) {
            Util.a(d2.d());
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.m0.d.RealConnection, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x0059, code lost:
        if (r6 == null) goto L_0x0065;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x005b, code lost:
        o.m0.Util.a(r6.d());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0064, code lost:
        return 0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0065, code lost:
        n.n.c.Intrinsics.a();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x0068, code lost:
        throw null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final long a(long r13) {
        /*
            r12 = this;
            monitor-enter(r12)
            java.util.ArrayDeque<o.m0.d.h> r0 = r12.c     // Catch:{ all -> 0x0069 }
            java.util.Iterator r0 = r0.iterator()     // Catch:{ all -> 0x0069 }
            r1 = 0
            r2 = 0
            r3 = -9223372036854775808
            r6 = r1
            r5 = 0
            r7 = 0
        L_0x000e:
            boolean r8 = r0.hasNext()     // Catch:{ all -> 0x0069 }
            if (r8 == 0) goto L_0x0035
            java.lang.Object r8 = r0.next()     // Catch:{ all -> 0x0069 }
            o.m0.d.RealConnection r8 = (o.m0.d.RealConnection) r8     // Catch:{ all -> 0x0069 }
            java.lang.String r9 = "connection"
            n.n.c.Intrinsics.a(r8, r9)     // Catch:{ all -> 0x0069 }
            int r9 = r12.a(r8, r13)     // Catch:{ all -> 0x0069 }
            if (r9 <= 0) goto L_0x0028
            int r7 = r7 + 1
            goto L_0x000e
        L_0x0028:
            int r5 = r5 + 1
            long r9 = r8.f2957o     // Catch:{ all -> 0x0069 }
            long r9 = r13 - r9
            int r11 = (r9 > r3 ? 1 : (r9 == r3 ? 0 : -1))
            if (r11 <= 0) goto L_0x000e
            r6 = r8
            r3 = r9
            goto L_0x000e
        L_0x0035:
            long r13 = r12.a     // Catch:{ all -> 0x0069 }
            int r0 = (r3 > r13 ? 1 : (r3 == r13 ? 0 : -1))
            if (r0 >= 0) goto L_0x0053
            int r13 = r12.f2962f     // Catch:{ all -> 0x0069 }
            if (r5 <= r13) goto L_0x0040
            goto L_0x0053
        L_0x0040:
            if (r5 <= 0) goto L_0x0047
            long r13 = r12.a     // Catch:{ all -> 0x0069 }
            long r13 = r13 - r3
            monitor-exit(r12)
            return r13
        L_0x0047:
            if (r7 <= 0) goto L_0x004d
            long r13 = r12.a     // Catch:{ all -> 0x0069 }
            monitor-exit(r12)
            return r13
        L_0x004d:
            r12.f2961e = r2     // Catch:{ all -> 0x0069 }
            r13 = -1
            monitor-exit(r12)
            return r13
        L_0x0053:
            java.util.ArrayDeque<o.m0.d.h> r13 = r12.c     // Catch:{ all -> 0x0069 }
            r13.remove(r6)     // Catch:{ all -> 0x0069 }
            monitor-exit(r12)
            if (r6 == 0) goto L_0x0065
            java.net.Socket r13 = r6.d()
            o.m0.Util.a(r13)
            r13 = 0
            return r13
        L_0x0065:
            n.n.c.Intrinsics.a()
            throw r1
        L_0x0069:
            r13 = move-exception
            monitor-exit(r12)
            throw r13
        */
        throw new UnsupportedOperationException("Method not decompiled: o.m0.d.RealConnectionPool.a(long):long");
    }
}
