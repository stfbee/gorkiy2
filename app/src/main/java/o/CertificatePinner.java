package o;

import j.a.a.a.outline;
import java.security.Principal;
import java.security.PublicKey;
import java.security.cert.Certificate;
import java.security.cert.X509Certificate;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import javax.net.ssl.SSLPeerUnverifiedException;
import kotlin.TypeCastException;
import n.i.Collections;
import n.i.Collections2;
import n.i.Sets;
import n.i._Arrays;
import n.n.b.Functions;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import n.n.c.TypeIntrinsics;
import n.n.c.t.KMarkers;
import n.r.Indent;
import o.h;
import o.m0.k.CertificateChainCleaner;
import o.m0.k.c;
import p.ByteString;

/* compiled from: CertificatePinner.kt */
public final class CertificatePinner {
    public static final CertificatePinner c;
    public static final a d = new a(null);
    public final Set<h.b> a;
    public final CertificateChainCleaner b;

    /* compiled from: CertificatePinner.kt */
    public static final class b {
        public final String a;
        public final String b;
        public final String c;
        public final ByteString d;

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof b)) {
                return false;
            }
            b bVar = (b) obj;
            return Intrinsics.a(null, bVar.a) && Intrinsics.a(null, bVar.b) && Intrinsics.a(null, bVar.c) && Intrinsics.a(null, bVar.d);
        }

        public int hashCode() {
            return 0;
        }

        public String toString() {
            new StringBuilder().append((String) null);
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
     arg types: [java.util.ArrayList, java.util.Set]
     candidates:
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C */
    static {
        Set set;
        ArrayList arrayList = new ArrayList();
        int size = arrayList.size();
        if (size == 0) {
            set = Sets.b;
        } else if (size != 1) {
            set = new LinkedHashSet(Collections.a(arrayList.size()));
            _Arrays.a((Iterable) arrayList, (Collection) set);
        } else {
            set = Collections.a(arrayList.get(0));
        }
        c = new CertificatePinner(set, null);
    }

    public CertificatePinner(Set<h.b> set, c cVar) {
        if (set != null) {
            this.a = set;
            this.b = cVar;
            return;
        }
        Intrinsics.a("pins");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
     arg types: [java.lang.String, int, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.security.Principal, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void a(String str, Functions<? extends List<? extends X509Certificate>> functions) {
        if (str == null) {
            Intrinsics.a("hostname");
            throw null;
        } else if (functions != null) {
            List<b> list = Collections2.b;
            for (b next : this.a) {
                if (next == null) {
                    throw null;
                } else if (Indent.b(null, "*.", false, 2)) {
                    Indent.a((CharSequence) str, '.', 0, false, 6);
                    str.length();
                    throw null;
                } else if (Intrinsics.a(str, (Object) null)) {
                    if (list.isEmpty()) {
                        list = new ArrayList<>();
                    }
                    if (!(list instanceof KMarkers)) {
                        list.add(next);
                    } else {
                        TypeIntrinsics.a(list, "kotlin.collections.MutableList");
                        throw null;
                    }
                }
            }
            if (!list.isEmpty()) {
                List<X509Certificate> list2 = (List) functions.b();
                for (X509Certificate x509Certificate : list2) {
                    Iterator it = list.iterator();
                    if (it.hasNext()) {
                        if (((b) it.next()) != null) {
                            throw null;
                        }
                        throw null;
                    }
                }
                StringBuilder b2 = outline.b("Certificate pinning failure!", "\n  Peer certificate chain:");
                for (X509Certificate x509Certificate2 : list2) {
                    if (x509Certificate2 != null) {
                        b2.append("\n    ");
                        b2.append(d.a((Certificate) x509Certificate2));
                        b2.append(": ");
                        Principal subjectDN = x509Certificate2.getSubjectDN();
                        Intrinsics.a((Object) subjectDN, "x509Certificate.subjectDN");
                        b2.append(subjectDN.getName());
                    } else {
                        throw new TypeCastException("null cannot be cast to non-null type java.security.cert.X509Certificate");
                    }
                }
                b2.append("\n  Pinned certificates for ");
                b2.append(str);
                b2.append(":");
                for (b append : list) {
                    b2.append("\n    ");
                    b2.append(append);
                }
                String sb = b2.toString();
                Intrinsics.a((Object) sb, "StringBuilder().apply(builderAction).toString()");
                throw new SSLPeerUnverifiedException(sb);
            }
        } else {
            Intrinsics.a("cleanedPeerCertificatesFn");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof CertificatePinner) {
            CertificatePinner certificatePinner = (CertificatePinner) obj;
            return Intrinsics.a(certificatePinner.a, this.a) && Intrinsics.a(certificatePinner.b, this.b);
        }
    }

    public int hashCode() {
        int hashCode = (this.a.hashCode() + 1517) * 41;
        CertificateChainCleaner certificateChainCleaner = this.b;
        return hashCode + (certificateChainCleaner != null ? certificateChainCleaner.hashCode() : 0);
    }

    /* compiled from: CertificatePinner.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        public final String a(Certificate certificate) {
            if (certificate == null) {
                Intrinsics.a("certificate");
                throw null;
            } else if (certificate instanceof X509Certificate) {
                StringBuilder a = outline.a("sha256/");
                a.append(a((X509Certificate) certificate).f());
                return a.toString();
            } else {
                throw new IllegalArgumentException("Certificate pinning requires X509 certificates".toString());
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.security.PublicKey, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [byte[], java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final ByteString a(X509Certificate x509Certificate) {
            if (x509Certificate != null) {
                ByteString.a aVar = ByteString.f3063f;
                PublicKey publicKey = x509Certificate.getPublicKey();
                Intrinsics.a((Object) publicKey, "publicKey");
                byte[] encoded = publicKey.getEncoded();
                Intrinsics.a((Object) encoded, "publicKey.encoded");
                return ByteString.a.a(aVar, encoded, 0, 0, 3).a("SHA-256");
            }
            Intrinsics.a("$this$toSha256ByteString");
            throw null;
        }
    }
}
