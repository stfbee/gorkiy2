package o;

import j.a.a.a.outline;
import java.io.IOException;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: Protocol.kt */
public enum Protocol {
    HTTP_1_0("http/1.0"),
    HTTP_1_1("http/1.1"),
    SPDY_3("spdy/3.1"),
    HTTP_2("h2"),
    H2_PRIOR_KNOWLEDGE("h2_prior_knowledge"),
    QUIC("quic");
    
    public static final a Companion = new a(null);
    public final String protocol;

    /* compiled from: Protocol.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public final Protocol a(String str) {
            if (str == null) {
                Intrinsics.a("protocol");
                throw null;
            } else if (Intrinsics.a((Object) str, (Object) Protocol.HTTP_1_0.protocol)) {
                return Protocol.HTTP_1_0;
            } else {
                if (Intrinsics.a((Object) str, (Object) Protocol.HTTP_1_1.protocol)) {
                    return Protocol.HTTP_1_1;
                }
                if (Intrinsics.a((Object) str, (Object) Protocol.H2_PRIOR_KNOWLEDGE.protocol)) {
                    return Protocol.H2_PRIOR_KNOWLEDGE;
                }
                if (Intrinsics.a((Object) str, (Object) Protocol.HTTP_2.protocol)) {
                    return Protocol.HTTP_2;
                }
                if (Intrinsics.a((Object) str, (Object) Protocol.SPDY_3.protocol)) {
                    return Protocol.SPDY_3;
                }
                if (Intrinsics.a((Object) str, (Object) Protocol.QUIC.protocol)) {
                    return Protocol.QUIC;
                }
                throw new IOException(outline.a("Unexpected protocol: ", str));
            }
        }
    }

    /* access modifiers changed from: public */
    Protocol(String str) {
        this.protocol = str;
    }

    public String toString() {
        return this.protocol;
    }
}
