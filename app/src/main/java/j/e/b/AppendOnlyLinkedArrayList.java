package j.e.b;

import l.b.t.Predicate;

public class AppendOnlyLinkedArrayList<T> {
    public final int a;
    public final Object[] b;
    public Object[] c;
    public int d;

    public interface a<T> extends Predicate<T> {
    }

    public AppendOnlyLinkedArrayList(int i2) {
        this.a = i2;
        Object[] objArr = new Object[(i2 + 1)];
        this.b = objArr;
        this.c = objArr;
    }
}
