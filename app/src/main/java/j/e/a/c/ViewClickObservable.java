package j.e.a.c;

import android.view.View;
import j.c.a.a.c.n.c;
import j.e.a.b.Notification;
import l.b.Observable;
import l.b.Observer;
import l.b.r.MainThreadDisposable;

public final class ViewClickObservable extends Observable<Object> {
    public final View b;

    public static final class a extends MainThreadDisposable implements View.OnClickListener {
        public final View c;
        public final Observer<? super Object> d;

        public a(View view, Observer<? super Object> observer) {
            this.c = view;
            this.d = observer;
        }

        public void a() {
            this.c.setOnClickListener(null);
        }

        public void onClick(View view) {
            if (!g()) {
                this.d.b(Notification.INSTANCE);
            }
        }
    }

    public ViewClickObservable(View view) {
        this.b = view;
    }

    public void b(Observer<? super Object> observer) {
        if (c.a((Observer<?>) observer)) {
            a aVar = new a(this.b, observer);
            observer.a(aVar);
            this.b.setOnClickListener(aVar);
        }
    }
}
