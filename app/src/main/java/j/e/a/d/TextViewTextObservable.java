package j.e.a.d;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;
import j.e.a.InitialValueObservable;
import l.b.Observer;
import l.b.r.MainThreadDisposable;

public final class TextViewTextObservable extends InitialValueObservable<CharSequence> {
    public final TextView b;

    public static final class a extends MainThreadDisposable implements TextWatcher {
        public final TextView c;
        public final Observer<? super CharSequence> d;

        public a(TextView textView, Observer<? super CharSequence> observer) {
            this.c = textView;
            this.d = observer;
        }

        public void a() {
            this.c.removeTextChangedListener(this);
        }

        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }

        public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            if (!g()) {
                this.d.b(charSequence);
            }
        }
    }

    public TextViewTextObservable(TextView textView) {
        this.b = textView;
    }

    public void c(Observer<? super CharSequence> observer) {
        a aVar = new a(this.b, observer);
        observer.a(aVar);
        this.b.addTextChangedListener(aVar);
    }

    public Object c() {
        return this.b.getText();
    }
}
