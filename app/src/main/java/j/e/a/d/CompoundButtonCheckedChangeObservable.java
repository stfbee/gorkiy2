package j.e.a.d;

import android.widget.CompoundButton;
import j.c.a.a.c.n.c;
import j.e.a.InitialValueObservable;
import l.b.Observer;
import l.b.r.MainThreadDisposable;

public final class CompoundButtonCheckedChangeObservable extends InitialValueObservable<Boolean> {
    public final CompoundButton b;

    public static final class a extends MainThreadDisposable implements CompoundButton.OnCheckedChangeListener {
        public final CompoundButton c;
        public final Observer<? super Boolean> d;

        public a(CompoundButton compoundButton, Observer<? super Boolean> observer) {
            this.c = compoundButton;
            this.d = observer;
        }

        public void a() {
            this.c.setOnCheckedChangeListener(null);
        }

        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (!g()) {
                this.d.b(Boolean.valueOf(z));
            }
        }
    }

    public CompoundButtonCheckedChangeObservable(CompoundButton compoundButton) {
        this.b = compoundButton;
    }

    public void c(Observer<? super Boolean> observer) {
        if (c.a((Observer<?>) observer)) {
            a aVar = new a(this.b, observer);
            observer.a(aVar);
            this.b.setOnCheckedChangeListener(aVar);
        }
    }

    public Object c() {
        return Boolean.valueOf(this.b.isChecked());
    }
}
