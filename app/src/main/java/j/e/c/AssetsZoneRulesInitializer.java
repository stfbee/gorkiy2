package j.e.c;

import android.content.Context;
import java.io.IOException;
import java.io.InputStream;
import q.b.a.w.TzdbZoneRulesProvider;
import q.b.a.w.ZoneRulesInitializer;
import q.b.a.w.ZoneRulesProvider;

public final class AssetsZoneRulesInitializer extends ZoneRulesInitializer {
    public final Context c;
    public final String d;

    public AssetsZoneRulesInitializer(Context context, String str) {
        this.c = context;
        this.d = str;
    }

    public void a() {
        InputStream inputStream = null;
        try {
            inputStream = this.c.getAssets().open(this.d);
            TzdbZoneRulesProvider tzdbZoneRulesProvider = new TzdbZoneRulesProvider(inputStream);
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused) {
                }
            }
            ZoneRulesProvider.a(tzdbZoneRulesProvider);
        } catch (IOException e2) {
            throw new IllegalStateException(this.d + " missing from assets", e2);
        } catch (Throwable th) {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException unused2) {
                }
            }
            throw th;
        }
    }
}
