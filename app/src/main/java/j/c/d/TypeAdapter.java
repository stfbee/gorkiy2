package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;

public abstract class TypeAdapter<T> {
    public abstract T a(JsonReader jsonReader);

    public abstract void a(JsonWriter jsonWriter, Object obj);
}
