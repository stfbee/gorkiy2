package j.c.d.b0;

import j.a.a.a.outline;
import java.io.ObjectInputStream;
import java.io.ObjectStreamClass;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Type;

/* compiled from: ConstructorConstructor */
public class ConstructorConstructor4 implements ObjectConstructor<T> {
    public final UnsafeAllocator a;
    public final /* synthetic */ Class b;
    public final /* synthetic */ Type c;

    public ConstructorConstructor4(ConstructorConstructor constructorConstructor, Class cls, Type type) {
        UnsafeAllocator unsafeAllocator;
        this.b = cls;
        this.c = type;
        try {
            Class<?> cls2 = Class.forName("sun.misc.Unsafe");
            Field declaredField = cls2.getDeclaredField("theUnsafe");
            declaredField.setAccessible(true);
            unsafeAllocator = new UnsafeAllocator0(cls2.getMethod("allocateInstance", Class.class), declaredField.get(null));
        } catch (Exception unused) {
            try {
                Method declaredMethod = ObjectStreamClass.class.getDeclaredMethod("getConstructorId", Class.class);
                declaredMethod.setAccessible(true);
                int intValue = ((Integer) declaredMethod.invoke(null, Object.class)).intValue();
                Method declaredMethod2 = ObjectStreamClass.class.getDeclaredMethod("newInstance", Class.class, Integer.TYPE);
                declaredMethod2.setAccessible(true);
                unsafeAllocator = new UnsafeAllocator1(declaredMethod2, intValue);
            } catch (Exception unused2) {
                try {
                    Method declaredMethod3 = ObjectInputStream.class.getDeclaredMethod("newInstance", Class.class, Class.class);
                    declaredMethod3.setAccessible(true);
                    unsafeAllocator = new UnsafeAllocator2(declaredMethod3);
                } catch (Exception unused3) {
                    unsafeAllocator = new UnsafeAllocator3();
                }
            }
        }
        this.a = unsafeAllocator;
    }

    public T a() {
        try {
            return this.a.a(this.b);
        } catch (Exception e2) {
            StringBuilder a2 = outline.a("Unable to invoke no-args constructor for ");
            a2.append(this.c);
            a2.append(". Registering an InstanceCreator with Gson for this type may fix this problem.");
            throw new RuntimeException(a2.toString(), e2);
        }
    }
}
