package j.c.d.b0;

import java.lang.reflect.Method;

/* compiled from: UnsafeAllocator */
public final class UnsafeAllocator1 extends UnsafeAllocator {
    public final /* synthetic */ Method a;
    public final /* synthetic */ int b;

    public UnsafeAllocator1(Method method, int i2) {
        this.a = method;
        this.b = i2;
    }

    public <T> T a(Class<T> cls) {
        UnsafeAllocator.b(cls);
        return this.a.invoke(null, cls, Integer.valueOf(this.b));
    }
}
