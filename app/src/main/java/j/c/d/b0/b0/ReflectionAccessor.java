package j.c.d.b0.b0;

import j.c.d.b0.JavaVersion;
import java.lang.reflect.AccessibleObject;

public abstract class ReflectionAccessor {
    public static final ReflectionAccessor a = (JavaVersion.a < 9 ? new PreJava9ReflectionAccessor() : new UnsafeReflectionAccessor());

    public abstract void a(AccessibleObject accessibleObject);
}
