package j.c.d.b0.a0;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import j.a.a.a.outline;
import j.c.d.JsonArray;
import j.c.d.JsonNull;
import j.c.d.JsonObject;
import j.c.d.JsonPrimitive;
import java.io.Reader;
import java.util.Iterator;
import java.util.Map;

public final class JsonTreeReader extends JsonReader {

    /* renamed from: f  reason: collision with root package name */
    public static final Object f2558f = new Object();
    public Object[] b;
    public int c;
    public String[] d;

    /* renamed from: e  reason: collision with root package name */
    public int[] f2559e;

    public static class a extends Reader {
        public void close() {
            throw new AssertionError();
        }

        public int read(char[] cArr, int i2, int i3) {
            throw new AssertionError();
        }
    }

    static {
        new a();
    }

    private String locationString() {
        StringBuilder a2 = outline.a(" at path ");
        a2.append(getPath());
        return a2.toString();
    }

    public final Object a() {
        return this.b[this.c - 1];
    }

    public void beginArray() {
        a(JsonToken.BEGIN_ARRAY);
        a(((JsonArray) a()).iterator());
        this.f2559e[this.c - 1] = 0;
    }

    public void beginObject() {
        a(JsonToken.BEGIN_OBJECT);
        a(((JsonObject) a()).a.entrySet().iterator());
    }

    public void close() {
        this.b = new Object[]{f2558f};
        this.c = 1;
    }

    public void endArray() {
        a(JsonToken.END_ARRAY);
        f();
        f();
        int i2 = this.c;
        if (i2 > 0) {
            int[] iArr = this.f2559e;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    public void endObject() {
        a(JsonToken.END_OBJECT);
        f();
        f();
        int i2 = this.c;
        if (i2 > 0) {
            int[] iArr = this.f2559e;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    public final Object f() {
        Object[] objArr = this.b;
        int i2 = this.c - 1;
        this.c = i2;
        Object obj = objArr[i2];
        objArr[i2] = null;
        return obj;
    }

    public String getPath() {
        StringBuilder sb = new StringBuilder();
        sb.append('$');
        int i2 = 0;
        while (i2 < this.c) {
            Object[] objArr = this.b;
            if (objArr[i2] instanceof JsonArray) {
                i2++;
                if (objArr[i2] instanceof Iterator) {
                    sb.append('[');
                    sb.append(this.f2559e[i2]);
                    sb.append(']');
                }
            } else if (objArr[i2] instanceof JsonObject) {
                i2++;
                if (objArr[i2] instanceof Iterator) {
                    sb.append('.');
                    String[] strArr = this.d;
                    if (strArr[i2] != null) {
                        sb.append(strArr[i2]);
                    }
                }
            }
            i2++;
        }
        return sb.toString();
    }

    public boolean hasNext() {
        JsonToken peek = peek();
        return (peek == JsonToken.END_OBJECT || peek == JsonToken.END_ARRAY) ? false : true;
    }

    public boolean nextBoolean() {
        a(JsonToken.BOOLEAN);
        boolean e2 = ((JsonPrimitive) f()).e();
        int i2 = this.c;
        if (i2 > 0) {
            int[] iArr = this.f2559e;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
        return e2;
    }

    public double nextDouble() {
        JsonToken peek = peek();
        if (peek == JsonToken.NUMBER || peek == JsonToken.STRING) {
            JsonPrimitive jsonPrimitive = (JsonPrimitive) a();
            double doubleValue = jsonPrimitive.a instanceof Number ? jsonPrimitive.f().doubleValue() : Double.parseDouble(jsonPrimitive.d());
            if (isLenient() || (!Double.isNaN(doubleValue) && !Double.isInfinite(doubleValue))) {
                f();
                int i2 = this.c;
                if (i2 > 0) {
                    int[] iArr = this.f2559e;
                    int i3 = i2 - 1;
                    iArr[i3] = iArr[i3] + 1;
                }
                return doubleValue;
            }
            throw new NumberFormatException("JSON forbids NaN and infinities: " + doubleValue);
        }
        StringBuilder a2 = outline.a("Expected ");
        a2.append(JsonToken.NUMBER);
        a2.append(" but was ");
        a2.append(peek);
        a2.append(locationString());
        throw new IllegalStateException(a2.toString());
    }

    public int nextInt() {
        JsonToken peek = peek();
        if (peek == JsonToken.NUMBER || peek == JsonToken.STRING) {
            JsonPrimitive jsonPrimitive = (JsonPrimitive) a();
            int intValue = jsonPrimitive.a instanceof Number ? jsonPrimitive.f().intValue() : Integer.parseInt(jsonPrimitive.d());
            f();
            int i2 = this.c;
            if (i2 > 0) {
                int[] iArr = this.f2559e;
                int i3 = i2 - 1;
                iArr[i3] = iArr[i3] + 1;
            }
            return intValue;
        }
        StringBuilder a2 = outline.a("Expected ");
        a2.append(JsonToken.NUMBER);
        a2.append(" but was ");
        a2.append(peek);
        a2.append(locationString());
        throw new IllegalStateException(a2.toString());
    }

    public long nextLong() {
        JsonToken peek = peek();
        if (peek == JsonToken.NUMBER || peek == JsonToken.STRING) {
            JsonPrimitive jsonPrimitive = (JsonPrimitive) a();
            long longValue = jsonPrimitive.a instanceof Number ? jsonPrimitive.f().longValue() : Long.parseLong(jsonPrimitive.d());
            f();
            int i2 = this.c;
            if (i2 > 0) {
                int[] iArr = this.f2559e;
                int i3 = i2 - 1;
                iArr[i3] = iArr[i3] + 1;
            }
            return longValue;
        }
        StringBuilder a2 = outline.a("Expected ");
        a2.append(JsonToken.NUMBER);
        a2.append(" but was ");
        a2.append(peek);
        a2.append(locationString());
        throw new IllegalStateException(a2.toString());
    }

    public String nextName() {
        a(JsonToken.NAME);
        Map.Entry entry = (Map.Entry) ((Iterator) a()).next();
        String str = (String) entry.getKey();
        this.d[this.c - 1] = str;
        a(entry.getValue());
        return str;
    }

    public void nextNull() {
        a(JsonToken.NULL);
        f();
        int i2 = this.c;
        if (i2 > 0) {
            int[] iArr = this.f2559e;
            int i3 = i2 - 1;
            iArr[i3] = iArr[i3] + 1;
        }
    }

    public String nextString() {
        JsonToken peek = peek();
        if (peek == JsonToken.STRING || peek == JsonToken.NUMBER) {
            String d2 = ((JsonPrimitive) f()).d();
            int i2 = this.c;
            if (i2 > 0) {
                int[] iArr = this.f2559e;
                int i3 = i2 - 1;
                iArr[i3] = iArr[i3] + 1;
            }
            return d2;
        }
        StringBuilder a2 = outline.a("Expected ");
        a2.append(JsonToken.STRING);
        a2.append(" but was ");
        a2.append(peek);
        a2.append(locationString());
        throw new IllegalStateException(a2.toString());
    }

    public JsonToken peek() {
        if (this.c == 0) {
            return JsonToken.END_DOCUMENT;
        }
        Object a2 = a();
        if (a2 instanceof Iterator) {
            boolean z = this.b[this.c - 2] instanceof JsonObject;
            Iterator it = (Iterator) a2;
            if (!it.hasNext()) {
                return z ? JsonToken.END_OBJECT : JsonToken.END_ARRAY;
            }
            if (z) {
                return JsonToken.NAME;
            }
            a(it.next());
            return peek();
        } else if (a2 instanceof JsonObject) {
            return JsonToken.BEGIN_OBJECT;
        } else {
            if (a2 instanceof JsonArray) {
                return JsonToken.BEGIN_ARRAY;
            }
            if (a2 instanceof JsonPrimitive) {
                Object obj = ((JsonPrimitive) a2).a;
                if (obj instanceof String) {
                    return JsonToken.STRING;
                }
                if (obj instanceof Boolean) {
                    return JsonToken.BOOLEAN;
                }
                if (obj instanceof Number) {
                    return JsonToken.NUMBER;
                }
                throw new AssertionError();
            } else if (a2 instanceof JsonNull) {
                return JsonToken.NULL;
            } else {
                if (a2 == f2558f) {
                    throw new IllegalStateException("JsonReader is closed");
                }
                throw new AssertionError();
            }
        }
    }

    public void skipValue() {
        if (peek() == JsonToken.NAME) {
            nextName();
            this.d[this.c - 2] = "null";
        } else {
            f();
            int i2 = this.c;
            if (i2 > 0) {
                this.d[i2 - 1] = "null";
            }
        }
        int i3 = this.c;
        if (i3 > 0) {
            int[] iArr = this.f2559e;
            int i4 = i3 - 1;
            iArr[i4] = iArr[i4] + 1;
        }
    }

    public String toString() {
        return JsonTreeReader.class.getSimpleName();
    }

    public final void a(JsonToken jsonToken) {
        if (peek() != jsonToken) {
            throw new IllegalStateException("Expected " + jsonToken + " but was " + peek() + locationString());
        }
    }

    public final void a(Object obj) {
        int i2 = this.c;
        Object[] objArr = this.b;
        if (i2 == objArr.length) {
            Object[] objArr2 = new Object[(i2 * 2)];
            int[] iArr = new int[(i2 * 2)];
            String[] strArr = new String[(i2 * 2)];
            System.arraycopy(objArr, 0, objArr2, 0, i2);
            System.arraycopy(this.f2559e, 0, iArr, 0, this.c);
            System.arraycopy(this.d, 0, strArr, 0, this.c);
            this.b = objArr2;
            this.f2559e = iArr;
            this.d = strArr;
        }
        Object[] objArr3 = this.b;
        int i3 = this.c;
        this.c = i3 + 1;
        objArr3[i3] = obj;
    }
}
