package j.c.d.b0;

import java.lang.reflect.Method;

/* compiled from: UnsafeAllocator */
public final class UnsafeAllocator2 extends UnsafeAllocator {
    public final /* synthetic */ Method a;

    public UnsafeAllocator2(Method method) {
        this.a = method;
    }

    public <T> T a(Class<T> cls) {
        UnsafeAllocator.b(cls);
        return this.a.invoke(null, cls, Object.class);
    }
}
