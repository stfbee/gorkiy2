package j.c.d.b0.b0;

import java.lang.reflect.AccessibleObject;

public final class PreJava9ReflectionAccessor extends ReflectionAccessor {
    public void a(AccessibleObject accessibleObject) {
        accessibleObject.setAccessible(true);
    }
}
