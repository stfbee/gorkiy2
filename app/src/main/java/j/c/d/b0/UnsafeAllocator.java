package j.c.d.b0;

import j.a.a.a.outline;
import java.lang.reflect.Modifier;

public abstract class UnsafeAllocator {
    public static void b(Class<?> cls) {
        int modifiers = cls.getModifiers();
        if (Modifier.isInterface(modifiers)) {
            StringBuilder a = outline.a("Interface can't be instantiated! Interface name: ");
            a.append(cls.getName());
            throw new UnsupportedOperationException(a.toString());
        } else if (Modifier.isAbstract(modifiers)) {
            StringBuilder a2 = outline.a("Abstract class can't be instantiated! Class name: ");
            a2.append(cls.getName());
            throw new UnsupportedOperationException(a2.toString());
        }
    }

    public abstract <T> T a(Class<T> cls);
}
