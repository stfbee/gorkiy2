package j.c.d.b0;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import j.c.d.ExclusionStrategy;
import j.c.d.Gson;
import j.c.d.TypeAdapter;
import j.c.d.TypeAdapterFactory;
import j.c.d.a0.Since;
import j.c.d.a0.Until;
import j.c.d.b;
import j.c.d.c0.TypeToken;
import j.c.d.k;
import j.c.d.z;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public final class Excluder implements TypeAdapterFactory, Cloneable {
    public static final Excluder g = new Excluder();
    public double b = -1.0d;
    public int c = 136;
    public boolean d = true;

    /* renamed from: e  reason: collision with root package name */
    public List<b> f2547e = Collections.emptyList();

    /* renamed from: f  reason: collision with root package name */
    public List<b> f2548f = Collections.emptyList();

    public <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken) {
        Class<? super T> cls = typeToken.a;
        boolean a2 = a(cls);
        boolean z = a2 || a(cls, true);
        boolean z2 = a2 || a(cls, false);
        if (z || z2) {
            return new a(z2, z, kVar, typeToken);
        }
        return null;
    }

    public final boolean b(Class<?> cls) {
        return !Enum.class.isAssignableFrom(cls) && (cls.isAnonymousClass() || cls.isLocalClass());
    }

    public final boolean c(Class<?> cls) {
        if (cls.isMemberClass()) {
            if (!((cls.getModifiers() & 8) != 0)) {
                return true;
            }
        }
        return false;
    }

    public Object clone() {
        try {
            return (Excluder) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new AssertionError(e2);
        }
    }

    public class a extends TypeAdapter<T> {
        public TypeAdapter<T> a;
        public final /* synthetic */ boolean b;
        public final /* synthetic */ boolean c;
        public final /* synthetic */ Gson d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ TypeToken f2549e;

        public a(boolean z, boolean z2, Gson gson, TypeToken typeToken) {
            this.b = z;
            this.c = z2;
            this.d = gson;
            this.f2549e = typeToken;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T>
         arg types: [j.c.d.b0.Excluder, j.c.d.c0.TypeToken]
         candidates:
          j.c.d.Gson.a(java.lang.String, java.lang.Class):T
          j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T> */
        public T a(JsonReader jsonReader) {
            if (this.b) {
                jsonReader.skipValue();
                return null;
            }
            TypeAdapter<T> typeAdapter = this.a;
            if (typeAdapter == null) {
                typeAdapter = this.d.a((z) Excluder.this, this.f2549e);
                this.a = super;
            }
            return super.a(jsonReader);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T>
         arg types: [j.c.d.b0.Excluder, j.c.d.c0.TypeToken]
         candidates:
          j.c.d.Gson.a(java.lang.String, java.lang.Class):T
          j.c.d.Gson.a(j.c.d.z, j.c.d.c0.TypeToken):j.c.d.TypeAdapter<T> */
        public void a(JsonWriter jsonWriter, T t2) {
            if (this.c) {
                jsonWriter.nullValue();
                return;
            }
            TypeAdapter<T> typeAdapter = this.a;
            if (typeAdapter == null) {
                typeAdapter = this.d.a((z) Excluder.this, this.f2549e);
                this.a = super;
            }
            super.a(jsonWriter, t2);
        }
    }

    public final boolean a(Class<?> cls) {
        if (this.b != -1.0d && !a((Since) cls.getAnnotation(Since.class), (Until) cls.getAnnotation(Until.class))) {
            return true;
        }
        if ((this.d || !c(cls)) && !b(cls)) {
            return false;
        }
        return true;
    }

    public final boolean a(Class<?> cls, boolean z) {
        Iterator it = (z ? this.f2547e : this.f2548f).iterator();
        while (it.hasNext()) {
            if (((ExclusionStrategy) it.next()).a(cls)) {
                return true;
            }
        }
        return false;
    }

    public final boolean a(Since since, Until until) {
        if (!(since == null || since.value() <= this.b)) {
            return false;
        }
        if (until == null || until.value() > this.b) {
            return true;
        }
        return false;
    }
}
