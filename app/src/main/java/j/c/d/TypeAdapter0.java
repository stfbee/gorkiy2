package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/* compiled from: TypeAdapter */
public class TypeAdapter0 extends TypeAdapter<T> {
    public final /* synthetic */ TypeAdapter a;

    public TypeAdapter0(TypeAdapter typeAdapter) {
        this.a = super;
    }

    public void a(JsonWriter jsonWriter, T t2) {
        if (t2 == null) {
            jsonWriter.nullValue();
        } else {
            this.a.a(jsonWriter, t2);
        }
    }

    public T a(JsonReader jsonReader) {
        if (jsonReader.peek() != JsonToken.NULL) {
            return this.a.a(jsonReader);
        }
        jsonReader.nextNull();
        return null;
    }
}
