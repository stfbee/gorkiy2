package j.c.d;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public final class JsonArray extends q implements Iterable<q> {
    public final List<q> b = new ArrayList();

    public String d() {
        if (this.b.size() == 1) {
            return this.b.get(0).d();
        }
        throw new IllegalStateException();
    }

    public boolean equals(Object obj) {
        return obj == this || ((obj instanceof JsonArray) && ((JsonArray) obj).b.equals(this.b));
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public Iterator<q> iterator() {
        return this.b.iterator();
    }
}
