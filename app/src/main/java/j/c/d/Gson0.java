package j.c.d;

import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;

/* compiled from: Gson */
public class Gson0 extends TypeAdapter<Number> {
    public Gson0(Gson gson) {
    }

    public void a(JsonWriter jsonWriter, Object obj) {
        Number number = (Number) obj;
        if (number == null) {
            jsonWriter.nullValue();
            return;
        }
        Gson.a(number.doubleValue());
        jsonWriter.value(number);
    }

    public Object a(JsonReader jsonReader) {
        if (jsonReader.peek() != JsonToken.NULL) {
            return Double.valueOf(jsonReader.nextDouble());
        }
        jsonReader.nextNull();
        return null;
    }
}
