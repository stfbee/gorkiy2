package j.c.d;

import j.c.d.c0.TypeToken;

public interface TypeAdapterFactory {
    <T> TypeAdapter<T> a(k kVar, TypeToken<T> typeToken);
}
