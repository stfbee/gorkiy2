package j.c.a.a.g.a;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class a5 implements Callable<List<g9>> {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ s4 f1934e;

    public a5(s4 s4Var, d9 d9Var, String str, String str2) {
        this.f1934e = s4Var;
        this.b = d9Var;
        this.c = str;
        this.d = str2;
    }

    public final /* synthetic */ Object call() {
        this.f1934e.a.o();
        return this.f1934e.a.e().b(this.b.b, this.c, this.d);
    }
}
