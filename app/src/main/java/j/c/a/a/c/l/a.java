package j.c.a.a.c.l;

import android.accounts.Account;
import android.os.Binder;
import android.os.RemoteException;
import android.util.Log;
import j.c.a.a.c.l.j;

public class a extends j.a {
    public static Account a(j jVar) {
        if (jVar != null) {
            long clearCallingIdentity = Binder.clearCallingIdentity();
            try {
                return jVar.e();
            } catch (RemoteException unused) {
                Log.w("AccountAccessor", "Remote account accessor probably died");
            } finally {
                Binder.restoreCallingIdentity(clearCallingIdentity);
            }
        }
        return null;
    }
}
