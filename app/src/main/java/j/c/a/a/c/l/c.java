package j.c.a.a.c.l;

import android.accounts.Account;
import com.google.android.gms.common.api.Scope;
import j.c.a.a.h.a;
import java.util.Set;

public final class c {
    public final Account a;
    public final Set<Scope> b;
    public final String c;
    public final String d;

    /* renamed from: e  reason: collision with root package name */
    public final a f1816e;

    /* renamed from: f  reason: collision with root package name */
    public Integer f1817f;
}
