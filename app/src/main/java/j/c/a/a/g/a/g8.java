package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g8 implements Runnable {
    public final /* synthetic */ f8 b;

    public g8(f8 f8Var) {
        this.b = f8Var;
    }

    public final void run() {
        l4 i2 = this.b.i();
        j8 j8Var = new j8(this);
        i2.o();
        ResourcesFlusher.b(j8Var);
        i2.a((p4<?>) new p4(i2, j8Var, "Task exception on worker thread"));
    }
}
