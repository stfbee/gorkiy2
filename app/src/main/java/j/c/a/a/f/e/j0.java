package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class j0 extends x3<j0, a> implements g5 {
    public static final j0 zzh;
    public static volatile m5<j0> zzi;
    public int zzc;
    public String zzd = "";
    public boolean zze;
    public boolean zzf;
    public int zzg;

    static {
        j0 j0Var = new j0();
        zzh = j0Var;
        x3.zzd.put(j0.class, j0Var);
    }

    public static /* synthetic */ void a(j0 j0Var, String str) {
        if (str != null) {
            j0Var.zzc |= 1;
            j0Var.zzd = str;
            return;
        }
        throw null;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<j0, a> implements g5 {
        public a() {
            super(j0.zzh);
        }

        public final String a() {
            return ((j0) super.c).zzd;
        }

        public final int n() {
            return ((j0) super.c).zzg;
        }

        public /* synthetic */ a(n0 n0Var) {
            super(j0.zzh);
        }
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (n0.a[i2 - 1]) {
            case 1:
                return new j0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\b\u0000\u0002\u0007\u0001\u0003\u0007\u0002\u0004\u0004\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                m5<j0> m5Var = zzi;
                if (m5Var == null) {
                    synchronized (j0.class) {
                        m5Var = zzi;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzh);
                            zzi = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
