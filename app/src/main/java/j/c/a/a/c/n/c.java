package j.c.a.a.c.n;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.TargetApi;
import android.app.AppOpsManager;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.os.Binder;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.ViewParent;
import android.widget.CompoundButton;
import android.widget.TextView;
import com.crashlytics.android.core.CrashlyticsPinningInfoProvider;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import com.google.android.gms.internal.measurement.zzfn;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.b.q.TintTypedArray;
import i.h.f.ColorUtils;
import i.h.l.ViewCompat;
import io.reactivex.exceptions.CompositeException;
import io.reactivex.exceptions.MissingBackpressureException;
import io.reactivex.exceptions.OnErrorNotImplementedException;
import io.reactivex.exceptions.UndeliverableException;
import j.a.a.a.outline;
import j.c.a.a.c.g;
import j.c.a.a.c.h;
import j.c.a.a.c.o.a;
import j.c.a.a.c.o.b;
import j.c.a.a.f.e.b2;
import j.c.a.a.f.e.e2;
import j.c.a.a.f.e.e4;
import j.c.a.a.f.e.f6;
import j.c.a.a.f.e.g2;
import j.c.a.a.f.e.h5;
import j.c.a.a.f.e.i6;
import j.c.a.a.f.e.k1;
import j.c.a.a.f.e.p6;
import j.c.a.a.f.e.s2;
import j.c.a.a.f.e.t5;
import j.c.a.a.f.e.w2;
import j.c.a.a.f.e.x3;
import j.c.a.a.f.e.y3;
import j.c.a.a.f.e.z3;
import j.c.a.a.g.a.n3;
import j.c.a.a.i.e;
import j.c.a.a.i.y;
import j.c.a.b.g0.CornerTreatment;
import j.c.a.b.g0.CutCornerTreatment;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.RoundedCornerTreatment;
import j.c.a.b.w.CircularRevealWidget;
import j.c.a.b.y.ElevationOverlayProvider;
import j.c.b.a.Registry;
import j.c.b.a.c0.EllipticCurves;
import j.c.b.a.c0.EllipticCurves0;
import j.c.b.a.c0.EllipticCurves1;
import j.c.b.a.c0.EngineFactory;
import j.c.b.a.c0.Enums;
import j.c.b.a.z.EcPointFormat;
import j.c.b.a.z.EcdsaParams;
import j.c.b.a.z.EcdsaSignatureEncoding;
import j.c.b.a.z.EciesAeadHkdfParams;
import j.c.b.a.z.EllipticCurveType;
import j.c.b.a.z.HashType;
import j.c.b.a.z.KeyTypeEntry;
import j.c.b.a.z.RegistryConfig;
import j.c.b.a.z.z1;
import j.c.c.e.d;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.e.a.InitialValueObservable;
import j.e.a.c.ViewClickObservable;
import j.e.a.d.CompoundButtonCheckedChangeObservable;
import j.e.a.d.TextViewTextObservable;
import java.io.File;
import java.io.Serializable;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.ECPrivateKey;
import java.security.interfaces.ECPublicKey;
import java.security.spec.ECField;
import java.security.spec.ECFieldFp;
import java.security.spec.ECParameterSpec;
import java.security.spec.ECPoint;
import java.security.spec.ECPrivateKeySpec;
import java.security.spec.ECPublicKeySpec;
import java.security.spec.EllipticCurve;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.Callable;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import kotlin.TypeCastException;
import l.a.a.a.o.b.ExecutorUtils0;
import l.a.a.a.o.d.EventsFilesManager;
import l.a.a.a.o.e.PinningTrustManager;
import l.a.a.a.o.e.SystemKeyStore;
import l.b.Observable;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.Scheduler;
import l.b.l;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.s.RunnableDisposable;
import l.b.t.Function;
import l.b.u.a.EmptyDisposable;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableScalarXMap;
import l.b.u.h.ExceptionHelper;
import n.Lazy;
import n.LazyJVM;
import n.Result;
import n.i.Collections0;
import n.i._Arrays;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.r.Indent;

public final class c {
    public static Boolean a;
    public static Boolean b;
    public static Boolean c;
    public static Context d;

    /* renamed from: e  reason: collision with root package name */
    public static Boolean f1828e;

    public static float a(float f2, float f3, float f4) {
        return (f4 * f3) + ((1.0f - f4) * f2);
    }

    public static int a(byte[] bArr, int i2, s2 s2Var) {
        int i3 = i2 + 1;
        byte b2 = bArr[i2];
        if (b2 < 0) {
            return a(b2, bArr, i3, s2Var);
        }
        s2Var.a = b2;
        return i3;
    }

    public static /* synthetic */ boolean a(byte b2) {
        return b2 >= 0;
    }

    public static <TResult> e<TResult> b(Object obj) {
        y yVar = new y();
        yVar.a(obj);
        return yVar;
    }

    public static boolean b(byte b2) {
        return b2 > -65;
    }

    public static int c(int i2) {
        if (i2 >= 200 && i2 <= 299) {
            return 0;
        }
        if (i2 >= 300 && i2 <= 399) {
            return 1;
        }
        if (i2 >= 400 && i2 <= 499) {
            return 0;
        }
        if (i2 >= 500) {
        }
        return 1;
    }

    @TargetApi(20)
    public static boolean c(Context context) {
        if (a == null) {
            a = Boolean.valueOf(context.getPackageManager().hasSystemFeature("android.hardware.type.watch"));
        }
        return a.booleanValue();
    }

    @TargetApi(26)
    public static boolean d(Context context) {
        if (c(context)) {
            if (!(Build.VERSION.SDK_INT >= 24) || (b(context) && !e())) {
                return true;
            }
        }
        return false;
    }

    public static boolean e() {
        return Build.VERSION.SDK_INT >= 26;
    }

    public static float e(byte[] bArr, int i2) {
        return Float.intBitsToFloat(b(bArr, i2));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.h.a(android.content.pm.PackageInfo, boolean):boolean
     arg types: [android.content.pm.PackageInfo, int]
     candidates:
      j.c.a.a.c.h.a(android.content.pm.PackageInfo, j.c.a.a.c.t[]):j.c.a.a.c.t
      j.c.a.a.c.h.a(android.content.pm.PackageInfo, boolean):boolean */
    public static boolean b(Context context, int i2) {
        if (!b(context, i2, "com.google.android.gms")) {
            return false;
        }
        try {
            PackageInfo packageInfo = context.getPackageManager().getPackageInfo("com.google.android.gms", 64);
            h a2 = h.a(context);
            if (a2 == null) {
                throw null;
            } else if (packageInfo == null) {
                return false;
            } else {
                if (!h.a(packageInfo, false)) {
                    if (!h.a(packageInfo, true)) {
                        return false;
                    }
                    if (!g.honorsDebugCertificates(a2.a)) {
                        Log.w("GoogleSignatureVerifier", "Test-keys aren't accepted on this build.");
                        return false;
                    }
                }
                return true;
            }
        } catch (PackageManager.NameNotFoundException unused) {
            if (Log.isLoggable("UidVerifier", 3)) {
                Log.d("UidVerifier", "Package manager can't find google play services package, defaulting to false");
            }
            return false;
        }
    }

    public static int e(byte[] bArr, int i2, s2 s2Var) {
        int a2 = a(bArr, i2, s2Var);
        int i3 = s2Var.a;
        if (i3 < 0) {
            throw zzfn.b();
        } else if (i3 > bArr.length - a2) {
            throw zzfn.a();
        } else if (i3 == 0) {
            s2Var.c = w2.c;
            return a2;
        } else {
            s2Var.c = w2.a(bArr, a2, i3);
            return a2 + i3;
        }
    }

    public static <T> b2<T> a(b2 b2Var) {
        if ((b2Var instanceof g2) || (b2Var instanceof e2)) {
            return b2Var;
        }
        if (b2Var instanceof Serializable) {
            return new e2(b2Var);
        }
        return new g2(b2Var);
    }

    public static int d(int i2) {
        return 1 << (32 - Integer.numberOfLeadingZeros(i2 - 1));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.List<T>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final <T> List<T> c(Object obj) {
        List<T> singletonList = Collections.singletonList(obj);
        Intrinsics.a((Object) singletonList, "java.util.Collections.singletonList(element)");
        return singletonList;
    }

    public static double d(byte[] bArr, int i2) {
        return Double.longBitsToDouble(c(bArr, i2));
    }

    public static String c(byte[] bArr) {
        StringBuilder sb = new StringBuilder(bArr.length * 2);
        for (byte b2 : bArr) {
            byte b3 = b2 & 255;
            sb.append("0123456789abcdef".charAt(b3 / 16));
            sb.append("0123456789abcdef".charAt(b3 % 16));
        }
        return sb.toString();
    }

    public static boolean d() {
        try {
            Class.forName("android.app.Application", false, null);
            return true;
        } catch (Exception unused) {
            return false;
        }
    }

    public static int d(byte[] bArr, int i2, s2 s2Var) {
        int a2 = a(bArr, i2, s2Var);
        int i3 = s2Var.a;
        if (i3 < 0) {
            throw zzfn.b();
        } else if (i3 == 0) {
            s2Var.c = "";
            return a2;
        } else {
            s2Var.c = p6.a.a(bArr, a2, i3);
            return a2 + i3;
        }
    }

    public static <V> V a(k1 k1Var) {
        long clearCallingIdentity;
        try {
            return k1Var.a();
        } catch (SecurityException unused) {
            clearCallingIdentity = Binder.clearCallingIdentity();
            V a2 = k1Var.a();
            Binder.restoreCallingIdentity(clearCallingIdentity);
            return a2;
        } catch (Throwable th) {
            Binder.restoreCallingIdentity(clearCallingIdentity);
            throw th;
        }
    }

    public static String c(HashType hashType) {
        int ordinal = hashType.ordinal();
        if (ordinal == 1) {
            return "HmacSha1";
        }
        if (ordinal == 2) {
            return "HmacSha256";
        }
        if (ordinal == 3) {
            return "HmacSha512";
        }
        throw new NoSuchAlgorithmException("hash unsupported for HMAC: " + hashType);
    }

    public static String a(w2 w2Var) {
        f6 f6Var = new f6(w2Var);
        StringBuilder sb = new StringBuilder(f6Var.a.a());
        for (int i2 = 0; i2 < f6Var.a.a(); i2++) {
            byte a2 = f6Var.a.a(i2);
            if (a2 == 34) {
                sb.append("\\\"");
            } else if (a2 == 39) {
                sb.append("\\'");
            } else if (a2 != 92) {
                switch (a2) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (a2 < 32 || a2 > 126) {
                            sb.append('\\');
                            sb.append((char) (((a2 >>> 6) & 3) + 48));
                            sb.append((char) (((a2 >>> 3) & 7) + 48));
                            sb.append((char) ((a2 & 7) + 48));
                            break;
                        } else {
                            sb.append((char) a2);
                            continue;
                        }
                        break;
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }

    public static byte[] c(String str) {
        if (str.length() % 2 == 0) {
            int length = str.length() / 2;
            byte[] bArr = new byte[length];
            for (int i2 = 0; i2 < length; i2++) {
                int i3 = i2 * 2;
                int digit = Character.digit(str.charAt(i3), 16);
                int digit2 = Character.digit(str.charAt(i3 + 1), 16);
                if (digit == -1 || digit2 == -1) {
                    throw new IllegalArgumentException("input is not hexadecimal");
                }
                bArr[i2] = (byte) ((digit * 16) + digit2);
            }
            return bArr;
        }
        throw new IllegalArgumentException("Expected a string of even length");
    }

    public static final String d(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (Character.isUpperCase(charAt)) {
                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    @TargetApi(19)
    public static boolean b(Context context, int i2, String str) {
        a b2 = b.b(context);
        if (b2 != null) {
            try {
                ((AppOpsManager) b2.a.getSystemService("appops")).checkPackage(i2, str);
                return true;
            } catch (SecurityException unused) {
                return false;
            }
        } else {
            throw null;
        }
    }

    @TargetApi(21)
    public static boolean b(Context context) {
        if (b == null) {
            b = Boolean.valueOf(context.getPackageManager().hasSystemFeature("cn.google"));
        }
        return b.booleanValue();
    }

    public static TypedValue c(Context context, int i2) {
        TypedValue typedValue = new TypedValue();
        if (context.getTheme().resolveAttribute(i2, typedValue, true)) {
            return typedValue;
        }
        return null;
    }

    public static int b(byte[] bArr, int i2, s2 s2Var) {
        int i3 = i2 + 1;
        long j2 = (long) bArr[i2];
        if (j2 >= 0) {
            s2Var.b = j2;
            return i3;
        }
        int i4 = i3 + 1;
        byte b2 = bArr[i3];
        long j3 = (j2 & 127) | (((long) (b2 & Byte.MAX_VALUE)) << 7);
        int i5 = 7;
        while (b2 < 0) {
            int i6 = i4 + 1;
            byte b3 = bArr[i4];
            i5 += 7;
            j3 |= ((long) (b3 & Byte.MAX_VALUE)) << i5;
            int i7 = i6;
            b2 = b3;
            i4 = i7;
        }
        s2Var.b = j3;
        return i4;
    }

    public static long c(byte[] bArr, int i2) {
        return ((((long) bArr[i2 + 7]) & 255) << 56) | (((long) bArr[i2]) & 255) | ((((long) bArr[i2 + 1]) & 255) << 8) | ((((long) bArr[i2 + 2]) & 255) << 16) | ((((long) bArr[i2 + 3]) & 255) << 24) | ((((long) bArr[i2 + 4]) & 255) << 32) | ((((long) bArr[i2 + 5]) & 255) << 40) | ((((long) bArr[i2 + 6]) & 255) << 48);
    }

    public static int c(byte[] bArr, int i2, s2 s2Var) {
        int a2 = a(bArr, i2, s2Var);
        int i3 = s2Var.a;
        if (i3 < 0) {
            throw zzfn.b();
        } else if (i3 == 0) {
            s2Var.c = "";
            return a2;
        } else {
            s2Var.c = new String(bArr, a2, i3, y3.a);
            return a2 + i3;
        }
    }

    public static final boolean b(byte[] bArr, byte[] bArr2) {
        if (bArr == null || bArr2 == null || bArr.length != bArr2.length) {
            return false;
        }
        byte b2 = 0;
        for (int i2 = 0; i2 < bArr.length; i2++) {
            b2 |= bArr[i2] ^ bArr2[i2];
        }
        if (b2 == 0) {
            return true;
        }
        return false;
    }

    public static Calendar c() {
        return b((Calendar) null);
    }

    public static void c(Throwable th) {
        if (th instanceof VirtualMachineError) {
            throw ((VirtualMachineError) th);
        } else if (th instanceof ThreadDeath) {
            throw ((ThreadDeath) th);
        } else if (th instanceof LinkageError) {
            throw ((LinkageError) th);
        }
    }

    public static byte[] b(byte[] bArr) {
        int length = bArr.length;
        byte[] bArr2 = new byte[length];
        for (int i2 = 0; i2 < length; i2++) {
            bArr2[i2] = (byte) ((bArr[i2] << 1) & 254);
            if (i2 < length - 1) {
                bArr2[i2] = (byte) (bArr2[i2] | ((byte) ((bArr[i2 + 1] >> 7) & 1)));
            }
        }
        bArr2[15] = (byte) (((byte) ((bArr[0] >> 7) & 135)) ^ bArr2[15]);
        return bArr2;
    }

    public static Set<String> a(SQLiteDatabase sQLiteDatabase, String str) {
        HashSet hashSet = new HashSet();
        StringBuilder sb = new StringBuilder(outline.a(str, 22));
        sb.append("SELECT * FROM ");
        sb.append(str);
        sb.append(" LIMIT 0");
        Cursor rawQuery = sQLiteDatabase.rawQuery(sb.toString(), null);
        try {
            Collections.addAll(hashSet, rawQuery.getColumnNames());
            return hashSet;
        } finally {
            rawQuery.close();
        }
    }

    public static int b(byte[] bArr, int i2) {
        return ((bArr[i2 + 3] & 255) << 24) | (bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16);
    }

    public static Calendar b() {
        return a(Calendar.getInstance());
    }

    public static final byte[] c(byte[] bArr, byte[] bArr2) {
        if (bArr.length == bArr2.length) {
            return a(bArr, 0, bArr2, 0, bArr.length);
        }
        throw new IllegalArgumentException("The lengths of x and y should match.");
    }

    public static String b(HashType hashType) {
        int ordinal = hashType.ordinal();
        if (ordinal == 1) {
            return "HmacSha1";
        }
        if (ordinal == 2) {
            return "HmacSha256";
        }
        if (ordinal == 3) {
            return "HmacSha512";
        }
        throw new NoSuchAlgorithmException("hash unsupported for HMAC: " + hashType);
    }

    public static <TResult> TResult b(e eVar) {
        if (eVar.d()) {
            return eVar.b();
        }
        if (((y) eVar).d) {
            throw new CancellationException("Task is already canceled");
        }
        throw new ExecutionException(eVar.a());
    }

    public static void a(Bundle bundle, Object obj) {
        if (obj instanceof Double) {
            bundle.putDouble("value", ((Double) obj).doubleValue());
        } else if (obj instanceof Long) {
            bundle.putLong("value", ((Long) obj).longValue());
        } else {
            bundle.putString("value", obj.toString());
        }
    }

    public static boolean b(View view) {
        return ViewCompat.k(view) == 1;
    }

    public static synchronized boolean a(Context context) {
        synchronized (c.class) {
            Context applicationContext = context.getApplicationContext();
            if (d == null || f1828e == null || d != applicationContext) {
                f1828e = null;
                if (e()) {
                    f1828e = Boolean.valueOf(applicationContext.getPackageManager().isInstantApp());
                } else {
                    try {
                        context.getClassLoader().loadClass("com.google.android.instantapps.supervisor.InstantAppsRuntime");
                        f1828e = true;
                    } catch (ClassNotFoundException unused) {
                        f1828e = false;
                    }
                }
                d = applicationContext;
                boolean booleanValue = f1828e.booleanValue();
                return booleanValue;
            }
            boolean booleanValue2 = f1828e.booleanValue();
            return booleanValue2;
        }
    }

    public static Calendar b(Calendar calendar) {
        Calendar instance = Calendar.getInstance(a());
        if (calendar == null) {
            instance.clear();
        } else {
            instance.setTimeInMillis(calendar.getTimeInMillis());
        }
        return instance;
    }

    public static <K, V> LinkedHashMap<K, V> b(int i2) {
        return new LinkedHashMap<>(i2 < 3 ? i2 + 1 : i2 < 1073741824 ? (int) ((((float) i2) / 0.75f) + 1.0f) : Integer.MAX_VALUE);
    }

    public static EllipticCurves b(EllipticCurveType ellipticCurveType) {
        int ordinal = ellipticCurveType.ordinal();
        if (ordinal == 1) {
            return EllipticCurves.NIST_P256;
        }
        if (ordinal == 2) {
            return EllipticCurves.NIST_P384;
        }
        if (ordinal == 3) {
            return EllipticCurves.NIST_P521;
        }
        throw new GeneralSecurityException("unknown curve type: " + ellipticCurveType);
    }

    public static final void b(StringBuilder sb, int i2, String str, Object obj) {
        if (obj instanceof List) {
            for (Object b2 : (List) obj) {
                b(sb, i2, str, b2);
            }
        } else if (obj instanceof Map) {
            for (Map.Entry b3 : ((Map) obj).entrySet()) {
                b(sb, i2, str, b3);
            }
        } else {
            sb.append(10);
            int i3 = 0;
            for (int i4 = 0; i4 < i2; i4++) {
                sb.append(' ');
            }
            sb.append(str);
            if (obj instanceof String) {
                sb.append(": \"");
                sb.append(a(w2.a((String) obj)));
                sb.append('\"');
            } else if (obj instanceof w2) {
                sb.append(": \"");
                sb.append(a((w2) obj));
                sb.append('\"');
            } else if (obj instanceof x3) {
                sb.append(" {");
                a((x3) obj, sb, i2 + 2);
                sb.append("\n");
                while (i3 < i2) {
                    sb.append(' ');
                    i3++;
                }
                sb.append("}");
            } else if (obj instanceof Map.Entry) {
                sb.append(" {");
                Map.Entry entry = (Map.Entry) obj;
                int i5 = i2 + 2;
                b(sb, i5, "key", entry.getKey());
                b(sb, i5, "value", entry.getValue());
                sb.append("\n");
                while (i3 < i2) {
                    sb.append(' ');
                    i3++;
                }
                sb.append("}");
            } else {
                sb.append(": ");
                sb.append(obj.toString());
            }
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:77:0x01e6, code lost:
        if (((java.lang.Boolean) r11).booleanValue() == false) goto L_0x01e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:83:0x01f8, code lost:
        if (((java.lang.Integer) r11).intValue() == 0) goto L_0x01e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:87:0x0209, code lost:
        if (((java.lang.Float) r11).floatValue() == 0.0f) goto L_0x01e8;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:91:0x021b, code lost:
        if (((java.lang.Double) r11).doubleValue() == 0.0d) goto L_0x01e8;
     */
    /* JADX WARNING: Removed duplicated region for block: B:107:0x0251  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(j.c.a.a.f.e.f5 r18, java.lang.StringBuilder r19, int r20) {
        /*
            r0 = r18
            r1 = r19
            r2 = r20
            java.util.HashMap r3 = new java.util.HashMap
            r3.<init>()
            java.util.HashMap r4 = new java.util.HashMap
            r4.<init>()
            java.util.TreeSet r5 = new java.util.TreeSet
            r5.<init>()
            java.lang.Class r6 = r18.getClass()
            java.lang.reflect.Method[] r6 = r6.getDeclaredMethods()
            int r7 = r6.length
            r8 = 0
            r9 = 0
        L_0x0020:
            java.lang.String r10 = "get"
            if (r9 >= r7) goto L_0x004f
            r11 = r6[r9]
            java.lang.String r12 = r11.getName()
            r4.put(r12, r11)
            java.lang.Class[] r12 = r11.getParameterTypes()
            int r12 = r12.length
            if (r12 != 0) goto L_0x004c
            java.lang.String r12 = r11.getName()
            r3.put(r12, r11)
            java.lang.String r12 = r11.getName()
            boolean r10 = r12.startsWith(r10)
            if (r10 == 0) goto L_0x004c
            java.lang.String r10 = r11.getName()
            r5.add(r10)
        L_0x004c:
            int r9 = r9 + 1
            goto L_0x0020
        L_0x004f:
            java.util.Iterator r5 = r5.iterator()
        L_0x0053:
            boolean r6 = r5.hasNext()
            if (r6 == 0) goto L_0x026a
            java.lang.Object r6 = r5.next()
            java.lang.String r6 = (java.lang.String) r6
            java.lang.String r7 = ""
            java.lang.String r9 = r6.replaceFirst(r10, r7)
            java.lang.String r11 = "List"
            boolean r12 = r9.endsWith(r11)
            r13 = 1
            if (r12 == 0) goto L_0x00c9
            java.lang.String r12 = "OrBuilderList"
            boolean r12 = r9.endsWith(r12)
            if (r12 != 0) goto L_0x00c9
            boolean r11 = r9.equals(r11)
            if (r11 != 0) goto L_0x00c9
            java.lang.String r11 = r9.substring(r8, r13)
            java.lang.String r11 = r11.toLowerCase()
            java.lang.String r11 = java.lang.String.valueOf(r11)
            int r12 = r9.length()
            int r12 = r12 + -4
            java.lang.String r12 = r9.substring(r13, r12)
            java.lang.String r12 = java.lang.String.valueOf(r12)
            int r14 = r12.length()
            if (r14 == 0) goto L_0x00a1
            java.lang.String r11 = r11.concat(r12)
            goto L_0x00a7
        L_0x00a1:
            java.lang.String r12 = new java.lang.String
            r12.<init>(r11)
            r11 = r12
        L_0x00a7:
            java.lang.Object r12 = r3.get(r6)
            java.lang.reflect.Method r12 = (java.lang.reflect.Method) r12
            if (r12 == 0) goto L_0x00c9
            java.lang.Class r14 = r12.getReturnType()
            java.lang.Class<java.util.List> r15 = java.util.List.class
            boolean r14 = r14.equals(r15)
            if (r14 == 0) goto L_0x00c9
            java.lang.String r6 = d(r11)
            java.lang.Object[] r7 = new java.lang.Object[r8]
            java.lang.Object r7 = j.c.a.a.f.e.x3.a(r12, r0, r7)
            b(r1, r2, r6, r7)
            goto L_0x0053
        L_0x00c9:
            java.lang.String r11 = "Map"
            boolean r12 = r9.endsWith(r11)
            if (r12 == 0) goto L_0x0137
            boolean r11 = r9.equals(r11)
            if (r11 != 0) goto L_0x0137
            java.lang.String r11 = r9.substring(r8, r13)
            java.lang.String r11 = r11.toLowerCase()
            java.lang.String r11 = java.lang.String.valueOf(r11)
            int r12 = r9.length()
            int r12 = r12 + -3
            java.lang.String r12 = r9.substring(r13, r12)
            java.lang.String r12 = java.lang.String.valueOf(r12)
            int r14 = r12.length()
            if (r14 == 0) goto L_0x00fc
            java.lang.String r11 = r11.concat(r12)
            goto L_0x0102
        L_0x00fc:
            java.lang.String r12 = new java.lang.String
            r12.<init>(r11)
            r11 = r12
        L_0x0102:
            java.lang.Object r6 = r3.get(r6)
            java.lang.reflect.Method r6 = (java.lang.reflect.Method) r6
            if (r6 == 0) goto L_0x0137
            java.lang.Class r12 = r6.getReturnType()
            java.lang.Class<java.util.Map> r14 = java.util.Map.class
            boolean r12 = r12.equals(r14)
            if (r12 == 0) goto L_0x0137
            java.lang.Class<java.lang.Deprecated> r12 = java.lang.Deprecated.class
            boolean r12 = r6.isAnnotationPresent(r12)
            if (r12 != 0) goto L_0x0137
            int r12 = r6.getModifiers()
            boolean r12 = java.lang.reflect.Modifier.isPublic(r12)
            if (r12 == 0) goto L_0x0137
            java.lang.String r7 = d(r11)
            java.lang.Object[] r9 = new java.lang.Object[r8]
            java.lang.Object r6 = j.c.a.a.f.e.x3.a(r6, r0, r9)
            b(r1, r2, r7, r6)
            goto L_0x0053
        L_0x0137:
            java.lang.String r6 = "set"
            int r11 = r9.length()
            if (r11 == 0) goto L_0x0144
            java.lang.String r6 = r6.concat(r9)
            goto L_0x014a
        L_0x0144:
            java.lang.String r11 = new java.lang.String
            r11.<init>(r6)
            r6 = r11
        L_0x014a:
            java.lang.Object r6 = r4.get(r6)
            java.lang.reflect.Method r6 = (java.lang.reflect.Method) r6
            if (r6 == 0) goto L_0x0053
            java.lang.String r6 = "Bytes"
            boolean r6 = r9.endsWith(r6)
            if (r6 == 0) goto L_0x017e
            int r6 = r9.length()
            int r6 = r6 + -5
            java.lang.String r6 = r9.substring(r8, r6)
            java.lang.String r6 = java.lang.String.valueOf(r6)
            int r11 = r6.length()
            if (r11 == 0) goto L_0x0173
            java.lang.String r6 = r10.concat(r6)
            goto L_0x0178
        L_0x0173:
            java.lang.String r6 = new java.lang.String
            r6.<init>(r10)
        L_0x0178:
            boolean r6 = r3.containsKey(r6)
            if (r6 != 0) goto L_0x0053
        L_0x017e:
            java.lang.String r6 = r9.substring(r8, r13)
            java.lang.String r6 = r6.toLowerCase()
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r11 = r9.substring(r13)
            java.lang.String r11 = java.lang.String.valueOf(r11)
            int r12 = r11.length()
            if (r12 == 0) goto L_0x019d
            java.lang.String r6 = r6.concat(r11)
            goto L_0x01a3
        L_0x019d:
            java.lang.String r11 = new java.lang.String
            r11.<init>(r6)
            r6 = r11
        L_0x01a3:
            int r11 = r9.length()
            if (r11 == 0) goto L_0x01ae
            java.lang.String r11 = r10.concat(r9)
            goto L_0x01b3
        L_0x01ae:
            java.lang.String r11 = new java.lang.String
            r11.<init>(r10)
        L_0x01b3:
            java.lang.Object r11 = r3.get(r11)
            java.lang.reflect.Method r11 = (java.lang.reflect.Method) r11
            java.lang.String r12 = "has"
            int r14 = r9.length()
            if (r14 == 0) goto L_0x01c6
            java.lang.String r9 = r12.concat(r9)
            goto L_0x01cb
        L_0x01c6:
            java.lang.String r9 = new java.lang.String
            r9.<init>(r12)
        L_0x01cb:
            java.lang.Object r9 = r3.get(r9)
            java.lang.reflect.Method r9 = (java.lang.reflect.Method) r9
            if (r11 == 0) goto L_0x0053
            java.lang.Object[] r12 = new java.lang.Object[r8]
            java.lang.Object r11 = j.c.a.a.f.e.x3.a(r11, r0, r12)
            if (r9 != 0) goto L_0x0253
            boolean r9 = r11 instanceof java.lang.Boolean
            if (r9 == 0) goto L_0x01ed
            r7 = r11
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r7 = r7.booleanValue()
            if (r7 != 0) goto L_0x01eb
        L_0x01e8:
            r7 = 1
            goto L_0x024e
        L_0x01eb:
            r7 = 0
            goto L_0x024e
        L_0x01ed:
            boolean r9 = r11 instanceof java.lang.Integer
            if (r9 == 0) goto L_0x01fb
            r7 = r11
            java.lang.Integer r7 = (java.lang.Integer) r7
            int r7 = r7.intValue()
            if (r7 != 0) goto L_0x01eb
            goto L_0x01e8
        L_0x01fb:
            boolean r9 = r11 instanceof java.lang.Float
            if (r9 == 0) goto L_0x020c
            r7 = r11
            java.lang.Float r7 = (java.lang.Float) r7
            float r7 = r7.floatValue()
            r9 = 0
            int r7 = (r7 > r9 ? 1 : (r7 == r9 ? 0 : -1))
            if (r7 != 0) goto L_0x01eb
            goto L_0x01e8
        L_0x020c:
            boolean r9 = r11 instanceof java.lang.Double
            if (r9 == 0) goto L_0x021e
            r7 = r11
            java.lang.Double r7 = (java.lang.Double) r7
            double r14 = r7.doubleValue()
            r16 = 0
            int r7 = (r14 > r16 ? 1 : (r14 == r16 ? 0 : -1))
            if (r7 != 0) goto L_0x01eb
            goto L_0x01e8
        L_0x021e:
            boolean r9 = r11 instanceof java.lang.String
            if (r9 == 0) goto L_0x0227
            boolean r7 = r11.equals(r7)
            goto L_0x024e
        L_0x0227:
            boolean r7 = r11 instanceof j.c.a.a.f.e.w2
            if (r7 == 0) goto L_0x0232
            j.c.a.a.f.e.w2 r7 = j.c.a.a.f.e.w2.c
            boolean r7 = r11.equals(r7)
            goto L_0x024e
        L_0x0232:
            boolean r7 = r11 instanceof j.c.a.a.f.e.f5
            if (r7 == 0) goto L_0x0240
            r7 = r11
            j.c.a.a.f.e.f5 r7 = (j.c.a.a.f.e.f5) r7
            j.c.a.a.f.e.f5 r7 = r7.k()
            if (r11 != r7) goto L_0x01eb
            goto L_0x01e8
        L_0x0240:
            boolean r7 = r11 instanceof java.lang.Enum
            if (r7 == 0) goto L_0x01eb
            r7 = r11
            java.lang.Enum r7 = (java.lang.Enum) r7
            int r7 = r7.ordinal()
            if (r7 != 0) goto L_0x01eb
            goto L_0x01e8
        L_0x024e:
            if (r7 != 0) goto L_0x0251
            goto L_0x025f
        L_0x0251:
            r13 = 0
            goto L_0x025f
        L_0x0253:
            java.lang.Object[] r7 = new java.lang.Object[r8]
            java.lang.Object r7 = j.c.a.a.f.e.x3.a(r9, r0, r7)
            java.lang.Boolean r7 = (java.lang.Boolean) r7
            boolean r13 = r7.booleanValue()
        L_0x025f:
            if (r13 == 0) goto L_0x0053
            java.lang.String r6 = d(r6)
            b(r1, r2, r6, r11)
            goto L_0x0053
        L_0x026a:
            boolean r3 = r0 instanceof j.c.a.a.f.e.x3.b
            if (r3 == 0) goto L_0x028d
            r3 = r0
            j.c.a.a.f.e.x3$b r3 = (j.c.a.a.f.e.x3.b) r3
            j.c.a.a.f.e.m3<java.lang.Object> r3 = r3.zzc
            java.util.Iterator r3 = r3.a()
            boolean r4 = r3.hasNext()
            if (r4 != 0) goto L_0x027e
            goto L_0x028d
        L_0x027e:
            java.lang.Object r0 = r3.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            r0.getKey()
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x028d:
            j.c.a.a.f.e.x3 r0 = (j.c.a.a.f.e.x3) r0
            j.c.a.a.f.e.i6 r0 = r0.zzb
            if (r0 == 0) goto L_0x02ab
        L_0x0293:
            int r3 = r0.a
            if (r8 >= r3) goto L_0x02ab
            int[] r3 = r0.b
            r3 = r3[r8]
            int r3 = r3 >>> 3
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.Object[] r4 = r0.c
            r4 = r4[r8]
            b(r1, r2, r3, r4)
            int r8 = r8 + 1
            goto L_0x0293
        L_0x02ab:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.n.c.a(j.c.a.a.f.e.f5, java.lang.StringBuilder, int):void");
    }

    public static Drawable b(Context context, TypedArray typedArray, int i2) {
        int resourceId;
        Drawable c2;
        if (!typedArray.hasValue(i2) || (resourceId = typedArray.getResourceId(i2, 0)) == 0 || (c2 = AppCompatResources.c(context, resourceId)) == null) {
            return typedArray.getDrawable(i2);
        }
        return c2;
    }

    public static final String b(String str) {
        StringBuilder sb = new StringBuilder();
        for (int i2 = 0; i2 < str.length(); i2++) {
            char charAt = str.charAt(i2);
            if (Character.isUpperCase(charAt)) {
                sb.append(EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR);
            }
            sb.append(Character.toLowerCase(charAt));
        }
        return sb.toString();
    }

    public static void b(Throwable th) {
        if (th == null) {
            th = new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        } else {
            boolean z = true;
            if (!(th instanceof OnErrorNotImplementedException) && !(th instanceof MissingBackpressureException) && !(th instanceof IllegalStateException) && !(th instanceof NullPointerException) && !(th instanceof IllegalArgumentException) && !(th instanceof CompositeException)) {
                z = false;
            }
            if (!z) {
                th = new UndeliverableException(th);
            }
        }
        th.printStackTrace();
        Thread currentThread = Thread.currentThread();
        currentThread.getUncaughtExceptionHandler().uncaughtException(currentThread, th);
    }

    public static ECParameterSpec b(EllipticCurves ellipticCurves) {
        int ordinal = ellipticCurves.ordinal();
        if (ordinal == 0) {
            return a("115792089210356248762697446949407573530086143415290314195533631308867097853951", "115792089210356248762697446949407573529996955224135760342422259061068512044369", "5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b", "6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296", "4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5");
        }
        if (ordinal == 1) {
            return a("39402006196394479212279040100143613805079739270465446667948293404245721771496870329047266088258938001861606973112319", "39402006196394479212279040100143613805079739270465446667946905279627659399113263569398956308152294913554433653942643", "b3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef", "aa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7", "3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f");
        }
        if (ordinal == 2) {
            return a("6864797660130609714981900799081393217269435300143305409394463459185543183397656052122559640661454554977296311391480858037121987999716643812574028291115057151", "6864797660130609714981900799081393217269435300143305409394463459185543183397655394245057746333217197532963996371363321113864768612440380340372808892707005449", "051953eb9618e1c9a1f929a21a0b68540eea2da725b99b315f3b8b489918ef109e156193951ec7e937b1652c0bd3bb1bf073573df883d2c34f1ef451fd46b503f00", "c6858e06b70404e9cd9e3ecb662395b4429c648139053fb521f828af606b4d3dbaa14b5e77efe75928fe1dc127a2ffa8de3348b3c1856a429bf97e7e31c2e5bd66", "11839296a789a3bc0045c8a5fb42c7d1bd998f54449579b446817afbd17273e662c97ee72995ef42640c550b9013fad0761353c7086a272c24088be94769fd16650");
        }
        throw new NoSuchAlgorithmException("curve not implemented:" + ellipticCurves);
    }

    public static final List<Character> b(char[] cArr) {
        if (cArr != null) {
            int length = cArr.length;
            Character[] chArr = new Character[length];
            int length2 = cArr.length;
            for (int i2 = 0; i2 < length2; i2++) {
                chArr[i2] = Character.valueOf(cArr[i2]);
            }
            if (length > 1) {
                Arrays.sort(chArr);
            }
            return _Arrays.a(chArr);
        }
        Intrinsics.a("$this$sorted");
        throw null;
    }

    public static /* synthetic */ void a(byte b2, byte b3, char[] cArr, int i2) {
        if (b2 < -62 || b(b3)) {
            throw zzfn.f();
        }
        cArr[i2] = (char) (((b2 & 31) << 6) | (b3 & 63));
    }

    public static int a(int i2, byte[] bArr, int i3, s2 s2Var) {
        int i4 = i2 & 127;
        int i5 = i3 + 1;
        byte b2 = bArr[i3];
        if (b2 >= 0) {
            s2Var.a = i4 | (b2 << 7);
            return i5;
        }
        int i6 = i4 | ((b2 & Byte.MAX_VALUE) << 7);
        int i7 = i5 + 1;
        byte b3 = bArr[i5];
        if (b3 >= 0) {
            s2Var.a = i6 | (b3 << 14);
            return i7;
        }
        int i8 = i6 | ((b3 & Byte.MAX_VALUE) << 14);
        int i9 = i7 + 1;
        byte b4 = bArr[i7];
        if (b4 >= 0) {
            s2Var.a = i8 | (b4 << 21);
            return i9;
        }
        int i10 = i8 | ((b4 & Byte.MAX_VALUE) << 21);
        int i11 = i9 + 1;
        byte b5 = bArr[i9];
        if (b5 >= 0) {
            s2Var.a = i10 | (b5 << 28);
            return i11;
        }
        int i12 = i10 | ((b5 & Byte.MAX_VALUE) << 28);
        while (true) {
            int i13 = i11 + 1;
            if (bArr[i11] >= 0) {
                s2Var.a = i12;
                return i13;
            }
            i11 = i13;
        }
    }

    public static <T> T a(Bundle bundle, String str, Class<T> cls, T t2) {
        T t3 = bundle.get(str);
        if (t3 == null) {
            return t2;
        }
        if (cls.isAssignableFrom(t3.getClass())) {
            return t3;
        }
        throw new IllegalStateException(String.format("Invalid conditional user property field type. '%s' expected [%s] but was [%s]", str, cls.getCanonicalName(), t3.getClass().getCanonicalName()));
    }

    public static /* synthetic */ void a(byte b2, byte b3, byte b4, char[] cArr, int i2) {
        if (b(b3) || ((b2 == -32 && b3 < -96) || ((b2 == -19 && b3 >= -96) || b(b4)))) {
            throw zzfn.f();
        }
        cArr[i2] = (char) (((b2 & 15) << 12) | ((b3 & 63) << 6) | (b4 & 63));
    }

    public static final Disposable a(Disposable disposable, CompositeDisposable compositeDisposable) {
        if (disposable == null) {
            Intrinsics.a("$this$addTo");
            throw null;
        } else if (compositeDisposable != null) {
            compositeDisposable.c(disposable);
            return disposable;
        } else {
            Intrinsics.a("compositeDisposable");
            throw null;
        }
    }

    public static final <T> Lazy<T> a(Functions functions) {
        if (functions != null) {
            return new LazyJVM(functions, null, 2);
        }
        Intrinsics.a("initializer");
        throw null;
    }

    public static String a(String str, String[] strArr, String[] strArr2) {
        ResourcesFlusher.b(strArr);
        ResourcesFlusher.b(strArr2);
        int min = Math.min(strArr.length, strArr2.length);
        for (int i2 = 0; i2 < min; i2++) {
            String str2 = strArr[i2];
            if ((str == null && str2 == null) ? true : str == null ? false : str.equals(str2)) {
                return strArr2[i2];
            }
        }
        return null;
    }

    public static /* synthetic */ void a(byte b2, byte b3, byte b4, byte b5, char[] cArr, int i2) {
        if (!b(b3)) {
            if ((((b3 + 112) + (b2 << 28)) >> 30) == 0 && !b(b4) && !b(b5)) {
                byte b6 = ((b2 & 7) << 18) | ((b3 & 63) << 12) | ((b4 & 63) << 6) | (b5 & 63);
                cArr[i2] = (char) ((b6 >>> 10) + 55232);
                cArr[i2 + 1] = (char) ((b6 & 1023) + 56320);
                return;
            }
        }
        throw zzfn.f();
    }

    public static d<?> a(String str, String str2) {
        j.c.c.j.a aVar = new j.c.c.j.a(str, str2);
        d.b a2 = d.a(j.c.c.j.e.class);
        a2.d = 1;
        a2.a(new j.c.c.e.c(aVar));
        return a2.a();
    }

    /* renamed from: a  reason: collision with other method in class */
    public static void m12a(Object obj, String str) {
        if (obj == null) {
            throw new NullPointerException(str);
        }
    }

    public static float a(float f2, float f3, float f4, float f5) {
        return (float) Math.hypot((double) (f4 - f2), (double) (f5 - f3));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.CompoundButton, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void */
    public static InitialValueObservable<Boolean> a(CompoundButton compoundButton) {
        m12a((Object) compoundButton, "view == null");
        return new CompoundButtonCheckedChangeObservable(compoundButton);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static boolean a(Observer<?> observer) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            return true;
        }
        Runnable runnable = l.b.u.b.Functions.b;
        ObjectHelper.a((Object) runnable, "run is null");
        observer.a(new RunnableDisposable(runnable));
        StringBuilder a2 = outline.a("Expected to be called on the main thread but was ");
        a2.append(Thread.currentThread().getName());
        observer.a(new IllegalStateException(a2.toString()));
        return false;
    }

    public static void a(EciesAeadHkdfParams eciesAeadHkdfParams) {
        b(a(eciesAeadHkdfParams.k().g()));
        b(eciesAeadHkdfParams.k().h());
        if (eciesAeadHkdfParams.h() != EcPointFormat.UNKNOWN_FORMAT) {
            Registry.b((z1) eciesAeadHkdfParams.g().g());
            return;
        }
        throw new GeneralSecurityException("unknown EC point format");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String */
    public static final /* synthetic */ String a(String str) {
        if (str != null) {
            String a2 = Indent.a(Indent.a(Indent.a(Indent.a(n.i.Collections.a((CharSequence) str).toString(), "[\\", "\\]", false, 4), "]\\", "\\[", false, 4), "{\\", "\\}", false, 4), "}\\", "\\{", false, 4);
            ArrayList arrayList = new ArrayList(a2.length());
            for (int i2 = 0; i2 < a2.length(); i2++) {
                char charAt = a2.charAt(i2);
                if (charAt == '[') {
                    charAt = ']';
                } else if (charAt == ']') {
                    charAt = '[';
                } else if (charAt == '{') {
                    charAt = '}';
                } else if (charAt == '}') {
                    charAt = '{';
                }
                arrayList.add(Character.valueOf(charAt));
            }
            return _Arrays.a(arrayList, "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Functions0) null, 62);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    public static final SSLSocketFactory a(CrashlyticsPinningInfoProvider crashlyticsPinningInfoProvider) {
        SSLContext instance = SSLContext.getInstance("TLS");
        instance.init(null, new TrustManager[]{new PinningTrustManager(new SystemKeyStore(crashlyticsPinningInfoProvider.getKeyStoreStream(), crashlyticsPinningInfoProvider.getKeyStorePassword()), crashlyticsPinningInfoProvider)}, null);
        return instance.getSocketFactory();
    }

    public static void a(EcdsaParams ecdsaParams) {
        EcdsaSignatureEncoding h = ecdsaParams.h();
        HashType k2 = ecdsaParams.k();
        EllipticCurveType g = ecdsaParams.g();
        int ordinal = h.ordinal();
        if (ordinal == 1 || ordinal == 2) {
            int ordinal2 = g.ordinal();
            if (ordinal2 != 1) {
                if (ordinal2 != 2 && ordinal2 != 3) {
                    throw new GeneralSecurityException("Invalid ECDSA parameters");
                } else if (k2 != HashType.SHA512) {
                    throw new GeneralSecurityException("Invalid ECDSA parameters");
                }
            } else if (k2 != HashType.SHA256) {
                throw new GeneralSecurityException("Invalid ECDSA parameters");
            }
        } else {
            throw new GeneralSecurityException("unsupported signature encoding");
        }
    }

    public static void a(n3 n3Var, SQLiteDatabase sQLiteDatabase, String str, String str2, String str3, String[] strArr) {
        boolean z;
        if (n3Var != null) {
            Cursor cursor = null;
            try {
                Cursor query = sQLiteDatabase.query("SQLITE_MASTER", new String[]{DefaultAppMeasurementEventListenerRegistrar.NAME}, "name=?", new String[]{str}, null, null, null);
                z = query.moveToFirst();
                query.close();
            } catch (SQLiteException e2) {
                n3Var.f2047i.a("Error querying for table", str, e2);
                if (cursor != null) {
                    cursor.close();
                }
                z = false;
            } catch (Throwable th) {
                if (cursor != null) {
                    cursor.close();
                }
                throw th;
            }
            if (!z) {
                sQLiteDatabase.execSQL(str2);
            }
            try {
                Set<String> a2 = a(sQLiteDatabase, str);
                String[] split = str3.split(",");
                int length = split.length;
                int i2 = 0;
                while (i2 < length) {
                    String str4 = split[i2];
                    if (((HashSet) a2).remove(str4)) {
                        i2++;
                    } else {
                        StringBuilder sb = new StringBuilder(String.valueOf(str).length() + 35 + String.valueOf(str4).length());
                        sb.append("Table ");
                        sb.append(str);
                        sb.append(" is missing required column: ");
                        sb.append(str4);
                        throw new SQLiteException(sb.toString());
                    }
                }
                if (strArr != null) {
                    for (int i3 = 0; i3 < strArr.length; i3 += 2) {
                        if (!((HashSet) a2).remove(strArr[i3])) {
                            sQLiteDatabase.execSQL(strArr[i3 + 1]);
                        }
                    }
                }
                if (!((HashSet) a2).isEmpty()) {
                    n3Var.f2047i.a("Table has extra columns. table, columns", str, TextUtils.join(", ", a2));
                }
            } catch (SQLiteException e3) {
                n3Var.f2046f.a("Failed to verify columns on table that was just created", str);
                throw e3;
            }
        } else {
            throw new IllegalArgumentException("Monitor must not be null");
        }
    }

    public static <T> T a(Object obj) {
        if (obj != null) {
            return obj;
        }
        throw null;
    }

    public static DateFormat a(int i2, int i3) {
        String str;
        String str2;
        StringBuilder sb = new StringBuilder();
        if (i2 == 0) {
            str = "EEEE, MMMM d, yyyy";
        } else if (i2 == 1) {
            str = "MMMM d, yyyy";
        } else if (i2 == 2) {
            str = "MMM d, yyyy";
        } else if (i2 == 3) {
            str = "M/d/yy";
        } else {
            throw new IllegalArgumentException(outline.b("Unknown DateFormat style: ", i2));
        }
        sb.append(str);
        sb.append(" ");
        if (i3 == 0 || i3 == 1) {
            str2 = "h:mm:ss a z";
        } else if (i3 == 2) {
            str2 = "h:mm:ss a";
        } else if (i3 == 3) {
            str2 = "h:mm a";
        } else {
            throw new IllegalArgumentException(outline.b("Unknown DateFormat style: ", i3));
        }
        sb.append(str2);
        return new SimpleDateFormat(sb.toString(), Locale.US);
    }

    public static long a(byte[] bArr, int i2) {
        return ((long) (((bArr[i2 + 3] & 255) << 24) | (bArr[i2] & 255) | ((bArr[i2 + 1] & 255) << 8) | ((bArr[i2 + 2] & 255) << 16))) & 4294967295L;
    }

    public static void a(AnimatorSet animatorSet, List<Animator> list) {
        int size = list.size();
        long j2 = 0;
        for (int i2 = 0; i2 < size; i2++) {
            Animator animator = list.get(i2);
            j2 = Math.max(j2, animator.getDuration() + animator.getStartDelay());
        }
        ValueAnimator ofInt = ValueAnimator.ofInt(0, 0);
        ofInt.setDuration(j2);
        list.add(0, ofInt);
        animatorSet.playTogether(list);
    }

    public static TimeZone a() {
        return TimeZone.getTimeZone("UTC");
    }

    public static CornerTreatment a(int i2) {
        if (i2 == 0) {
            return new RoundedCornerTreatment();
        }
        if (i2 != 1) {
            return new RoundedCornerTreatment();
        }
        return new CutCornerTreatment();
    }

    public static void a(boolean z) {
        if (!z) {
            throw new IllegalArgumentException();
        }
    }

    public static long a(byte[] bArr, int i2, int i3) {
        return (a(bArr, i2) >> i3) & 67108863;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.h]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.h]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.h]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult> */
    public static <TResult> TResult a(e eVar) {
        ResourcesFlusher.c("Must not be called on the main application thread");
        ResourcesFlusher.b(eVar, "Task must not be null");
        if (eVar.c()) {
            return b(eVar);
        }
        j.c.a.a.i.h hVar = new j.c.a.a.i.h(null);
        eVar.a(j.c.a.a.i.g.a, (j.c.a.a.i.d) hVar);
        eVar.a(j.c.a.a.i.g.a, (j.c.a.a.i.c) hVar);
        eVar.a(j.c.a.a.i.g.a, (j.c.a.a.i.b) hVar);
        hVar.a.await();
        return b(eVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.h]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.h]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.h]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult> */
    public static <TResult> TResult a(e<TResult> eVar, long j2, TimeUnit timeUnit) {
        ResourcesFlusher.c("Must not be called on the main application thread");
        ResourcesFlusher.b(eVar, "Task must not be null");
        ResourcesFlusher.b(timeUnit, "TimeUnit must not be null");
        if (eVar.c()) {
            return b((e) eVar);
        }
        j.c.a.a.i.h hVar = new j.c.a.a.i.h(null);
        eVar.a(j.c.a.a.i.g.a, (j.c.a.a.i.d) hVar);
        eVar.a(j.c.a.a.i.g.a, (j.c.a.a.i.c) hVar);
        eVar.a(j.c.a.a.i.g.a, (j.c.a.a.i.b) hVar);
        if (hVar.a.await(j2, timeUnit)) {
            return b((e) eVar);
        }
        throw new TimeoutException("Timed out waiting for Task");
    }

    public static <T> T a(Object obj, String str) {
        if (obj != null) {
            return obj;
        }
        throw new NullPointerException(str);
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r3v2, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r3v5, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r3v6, types: [l.b.s.Disposable, l.b.u.e.c.ObservableScalarXMap] */
    /* JADX WARN: Type inference failed for: r1v11, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX WARN: Type inference failed for: r1v12, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [? extends l.b.ObservableSource<? extends R>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static <T, R> boolean a(ObservableSource<T> observableSource, Observer<? super R> observer, Function<? super T, ? extends ObservableSource<? extends R>> function) {
        if (!(observableSource instanceof Callable)) {
            return false;
        }
        try {
            Object call = ((Callable) observableSource).call();
            if (call == null) {
                observer.a((Disposable) EmptyDisposable.INSTANCE);
                observer.a();
                return true;
            }
            try {
                Object a2 = function.a(call);
                ObjectHelper.a((Object) a2, "The mapper returned a null ObservableSource");
                ObservableSource observableSource2 = (ObservableSource) a2;
                if (observableSource2 instanceof Callable) {
                    try {
                        Object call2 = ((Callable) observableSource2).call();
                        if (call2 == null) {
                            observer.a((Disposable) EmptyDisposable.INSTANCE);
                            observer.a();
                            return true;
                        }
                        ? observableScalarXMap = new ObservableScalarXMap(observer, call2);
                        observer.a((Disposable) observableScalarXMap);
                        observableScalarXMap.run();
                    } catch (Throwable th) {
                        c(th);
                        observer.a((Disposable) EmptyDisposable.INSTANCE);
                        observer.a(th);
                        return true;
                    }
                } else {
                    observableSource2.a(observer);
                }
                return true;
            } catch (Throwable th2) {
                c(th2);
                observer.a((Disposable) EmptyDisposable.INSTANCE);
                observer.a(th2);
                return true;
            }
        } catch (Throwable th3) {
            c(th3);
            observer.a((Disposable) EmptyDisposable.INSTANCE);
            observer.a(th3);
            return true;
        }
    }

    public static ColorStateList a(Context context, TypedArray typedArray, int i2) {
        int resourceId;
        ColorStateList b2;
        if (!typedArray.hasValue(i2) || (resourceId = typedArray.getResourceId(i2, 0)) == 0 || (b2 = AppCompatResources.b(context, resourceId)) == null) {
            return typedArray.getColorStateList(i2);
        }
        return b2;
    }

    public static void a(byte[] bArr, long j2, int i2) {
        int i3 = 0;
        while (i3 < 4) {
            bArr[i2 + i3] = (byte) ((int) (255 & j2));
            i3++;
            j2 >>= 8;
        }
    }

    public static void a(n3 n3Var, SQLiteDatabase sQLiteDatabase) {
        if (n3Var != null) {
            File file = new File(sQLiteDatabase.getPath());
            if (!file.setReadable(false, false)) {
                n3Var.f2047i.a("Failed to turn off database read permission");
            }
            if (!file.setWritable(false, false)) {
                n3Var.f2047i.a("Failed to turn off database write permission");
            }
            if (!file.setReadable(true, true)) {
                n3Var.f2047i.a("Failed to turn on database read permission for owner");
            }
            if (!file.setWritable(true, true)) {
                n3Var.f2047i.a("Failed to turn on database write permission for owner");
                return;
            }
            return;
        }
        throw new IllegalArgumentException("Monitor must not be null");
    }

    public static String a(ByteString byteString) {
        StringBuilder sb = new StringBuilder(byteString.size());
        for (int i2 = 0; i2 < byteString.size(); i2++) {
            byte c2 = byteString.c(i2);
            if (c2 == 34) {
                sb.append("\\\"");
            } else if (c2 == 39) {
                sb.append("\\'");
            } else if (c2 != 92) {
                switch (c2) {
                    case 7:
                        sb.append("\\a");
                        continue;
                    case 8:
                        sb.append("\\b");
                        continue;
                    case 9:
                        sb.append("\\t");
                        continue;
                    case 10:
                        sb.append("\\n");
                        continue;
                    case 11:
                        sb.append("\\v");
                        continue;
                    case 12:
                        sb.append("\\f");
                        continue;
                    case 13:
                        sb.append("\\r");
                        continue;
                    default:
                        if (c2 < 32 || c2 > 126) {
                            sb.append('\\');
                            sb.append((char) (((c2 >>> 6) & 3) + 48));
                            sb.append((char) (((c2 >>> 3) & 7) + 48));
                            sb.append((char) ((c2 & 7) + 48));
                            break;
                        } else {
                            sb.append((char) c2);
                            continue;
                        }
                        break;
                }
            } else {
                sb.append("\\\\");
            }
        }
        return sb.toString();
    }

    public static byte[] a(byte[]... bArr) {
        int length = bArr.length;
        int i2 = 0;
        int i3 = 0;
        while (i2 < length) {
            byte[] bArr2 = bArr[i2];
            if (i3 <= Integer.MAX_VALUE - bArr2.length) {
                i3 += bArr2.length;
                i2++;
            } else {
                throw new GeneralSecurityException("exceeded size limit");
            }
        }
        byte[] bArr3 = new byte[i3];
        int i4 = 0;
        for (byte[] bArr4 : bArr) {
            System.arraycopy(bArr4, 0, bArr3, i4, bArr4.length);
            i4 += bArr4.length;
        }
        return bArr3;
    }

    public static Animator a(CircularRevealWidget circularRevealWidget, float f2, float f3, float f4) {
        ObjectAnimator ofObject = ObjectAnimator.ofObject(circularRevealWidget, CircularRevealWidget.c.a, CircularRevealWidget.b.b, new CircularRevealWidget.e(f2, f3, f4));
        CircularRevealWidget.e revealInfo = circularRevealWidget.getRevealInfo();
        if (revealInfo != null) {
            Animator createCircularReveal = ViewAnimationUtils.createCircularReveal((View) circularRevealWidget, (int) f2, (int) f3, revealInfo.c, f4);
            AnimatorSet animatorSet = new AnimatorSet();
            animatorSet.playTogether(ofObject, createCircularReveal);
            return animatorSet;
        }
        throw new IllegalStateException("Caller must set a non-null RevealInfo before calling this.");
    }

    public static void a(View view, float f2) {
        Drawable background = view.getBackground();
        if (background instanceof MaterialShapeDrawable) {
            MaterialShapeDrawable materialShapeDrawable = (MaterialShapeDrawable) background;
            MaterialShapeDrawable.b bVar = materialShapeDrawable.b;
            if (bVar.f2236o != f2) {
                bVar.f2236o = f2;
                materialShapeDrawable.j();
            }
        }
    }

    public static byte[] a(byte[] bArr) {
        if (bArr.length < 16) {
            byte[] copyOf = Arrays.copyOf(bArr, 16);
            copyOf[bArr.length] = Byte.MIN_VALUE;
            return copyOf;
        }
        throw new IllegalArgumentException("x must be smaller than a block.");
    }

    public static int a(Context context, int i2, String str) {
        TypedValue c2 = c(context, i2);
        if (c2 != null) {
            return c2.data;
        }
        throw new IllegalArgumentException(String.format("%1$s requires a value for the %2$s attribute to be set in your app theme. You can either set the attribute in your theme or update your theme to inherit from Theme.MaterialComponents (or a descendant).", str, context.getResources().getResourceName(i2)));
    }

    public static PorterDuffColorFilter a(Drawable drawable, ColorStateList colorStateList, PorterDuff.Mode mode) {
        if (colorStateList == null || mode == null) {
            return null;
        }
        return new PorterDuffColorFilter(colorStateList.getColorForState(drawable.getState(), 0), mode);
    }

    public static byte[] a(byte[] bArr, byte[] bArr2) {
        byte[] bArr3 = bArr;
        byte[] bArr4 = bArr2;
        if (bArr3.length == 32) {
            long a2 = a(bArr3, 0, 0) & 67108863;
            int i2 = 2;
            long a3 = a(bArr3, 3, 2) & 67108611;
            long a4 = a(bArr3, 6, 4) & 67092735;
            long a5 = a(bArr3, 9, 6) & 66076671;
            long a6 = a(bArr3, 12, 8) & 1048575;
            long j2 = a3 * 5;
            long j3 = a4 * 5;
            long j4 = a5 * 5;
            long j5 = a6 * 5;
            int i3 = 17;
            byte[] bArr5 = new byte[17];
            long j6 = 0;
            long j7 = 0;
            long j8 = 0;
            long j9 = 0;
            long j10 = 0;
            int i4 = 0;
            while (i4 < bArr4.length) {
                int min = Math.min(16, bArr4.length - i4);
                System.arraycopy(bArr4, i4, bArr5, 0, min);
                bArr5[min] = 1;
                if (min != 16) {
                    Arrays.fill(bArr5, min + 1, i3, (byte) 0);
                }
                long a7 = a(bArr5, 0, 0) + j10;
                long a8 = a(bArr5, 3, i2) + j6;
                long a9 = a(bArr5, 6, 4) + j7;
                long a10 = a(bArr5, 9, 6) + j8;
                long a11 = j9 + (a(bArr5, 12, 8) | ((long) (bArr5[16] << 24)));
                long j11 = (a11 * j2) + (a10 * j3) + (a9 * j4) + (a8 * j5) + (a7 * a2);
                long j12 = (a11 * j3) + (a10 * j4) + (a9 * j5) + (a8 * a2) + (a7 * a3);
                long j13 = (a11 * j4) + (a10 * j5) + (a9 * a2) + (a8 * a3) + (a7 * a4);
                long j14 = (a10 * a2) + (a9 * a3) + (a8 * a4) + (a7 * a5);
                long j15 = a10 * a3;
                long j16 = a11 * a2;
                long j17 = j12 + (j11 >> 26);
                long j18 = j13 + (j17 >> 26);
                long j19 = (a11 * j5) + j14 + (j18 >> 26);
                long j20 = j16 + j15 + (a9 * a4) + (a8 * a5) + (a7 * a6) + (j19 >> 26);
                long j21 = j20 >> 26;
                j9 = j20 & 67108863;
                long j22 = (j21 * 5) + (j11 & 67108863);
                long j23 = (j17 & 67108863) + (j22 >> 26);
                i4 += 16;
                j7 = j18 & 67108863;
                j8 = j19 & 67108863;
                i3 = 17;
                i2 = 2;
                j10 = j22 & 67108863;
                j6 = j23;
            }
            long j24 = j7 + (j6 >> 26);
            long j25 = j24 & 67108863;
            long j26 = j8 + (j24 >> 26);
            long j27 = j26 & 67108863;
            long j28 = j9 + (j26 >> 26);
            long j29 = j28 & 67108863;
            long j30 = ((j28 >> 26) * 5) + j10;
            long j31 = j30 >> 26;
            long j32 = j30 & 67108863;
            long j33 = (j6 & 67108863) + j31;
            long j34 = j32 + 5;
            long j35 = j34 & 67108863;
            long j36 = (j34 >> 26) + j33;
            long j37 = j25 + (j36 >> 26);
            long j38 = j27 + (j37 >> 26);
            long j39 = (j29 + (j38 >> 26)) - 67108864;
            long j40 = j39 >> 63;
            long j41 = j32 & j40;
            long j42 = j33 & j40;
            long j43 = j25 & j40;
            long j44 = j27 & j40;
            long j45 = j29 & j40;
            long j46 = ~j40;
            long j47 = j42 | (j36 & 67108863 & j46);
            long j48 = j43 | (j37 & 67108863 & j46);
            long j49 = j44 | (j38 & 67108863 & j46);
            long j50 = j45 | (j39 & j46);
            long a12 = a(bArr3, 16) + ((j41 | (j35 & j46) | (j47 << 26)) & 4294967295L);
            long a13 = a(bArr3, 20) + (((j47 >> 6) | (j48 << 20)) & 4294967295L) + (a12 >> 32);
            long j51 = a13 & 4294967295L;
            long a14 = a(bArr3, 24) + (((j48 >> 12) | (j49 << 14)) & 4294967295L) + (a13 >> 32);
            long a15 = a(bArr3, 28);
            byte[] bArr6 = new byte[16];
            a(bArr6, a12 & 4294967295L, 0);
            a(bArr6, j51, 4);
            a(bArr6, a14 & 4294967295L, 8);
            a(bArr6, (a15 + (((j49 >> 18) | (j50 << 8)) & 4294967295L) + (a14 >> 32)) & 4294967295L, 12);
            return bArr6;
        }
        throw new IllegalArgumentException("The key length in bytes must be 32.");
    }

    public static PorterDuff.Mode a(int i2, PorterDuff.Mode mode) {
        if (i2 == 3) {
            return PorterDuff.Mode.SRC_OVER;
        }
        if (i2 == 5) {
            return PorterDuff.Mode.SRC_IN;
        }
        if (i2 == 9) {
            return PorterDuff.Mode.SRC_ATOP;
        }
        switch (i2) {
            case 14:
                return PorterDuff.Mode.MULTIPLY;
            case 15:
                return PorterDuff.Mode.SCREEN;
            case 16:
                return PorterDuff.Mode.ADD;
            default:
                return mode;
        }
    }

    public static EllipticCurves a(EllipticCurveType ellipticCurveType) {
        int ordinal = ellipticCurveType.ordinal();
        if (ordinal == 1) {
            return EllipticCurves.NIST_P256;
        }
        if (ordinal == 2) {
            return EllipticCurves.NIST_P384;
        }
        if (ordinal == 3) {
            return EllipticCurves.NIST_P521;
        }
        throw new GeneralSecurityException("unknown curve type: " + ellipticCurveType);
    }

    public static Enums a(HashType hashType) {
        int ordinal = hashType.ordinal();
        if (ordinal == 1) {
            return Enums.SHA1;
        }
        if (ordinal == 2) {
            return Enums.SHA256;
        }
        if (ordinal == 3) {
            return Enums.SHA512;
        }
        throw new GeneralSecurityException("unknown hash type: " + hashType);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0160, code lost:
        if (((java.lang.Integer) r9).intValue() == 0) goto L_0x01b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0171, code lost:
        if (((java.lang.Float) r9).floatValue() == 0.0f) goto L_0x01b5;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x0183, code lost:
        if (((java.lang.Double) r9).doubleValue() == 0.0d) goto L_0x01b5;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void a(j.c.e.MessageLite r13, java.lang.StringBuilder r14, int r15) {
        /*
            java.util.HashMap r0 = new java.util.HashMap
            r0.<init>()
            java.util.HashMap r1 = new java.util.HashMap
            r1.<init>()
            java.util.TreeSet r2 = new java.util.TreeSet
            r2.<init>()
            java.lang.Class r3 = r13.getClass()
            java.lang.reflect.Method[] r3 = r3.getDeclaredMethods()
            int r4 = r3.length
            r5 = 0
            r6 = 0
        L_0x001a:
            java.lang.String r7 = "get"
            if (r6 >= r4) goto L_0x0049
            r8 = r3[r6]
            java.lang.String r9 = r8.getName()
            r1.put(r9, r8)
            java.lang.Class[] r9 = r8.getParameterTypes()
            int r9 = r9.length
            if (r9 != 0) goto L_0x0046
            java.lang.String r9 = r8.getName()
            r0.put(r9, r8)
            java.lang.String r9 = r8.getName()
            boolean r7 = r9.startsWith(r7)
            if (r7 == 0) goto L_0x0046
            java.lang.String r7 = r8.getName()
            r2.add(r7)
        L_0x0046:
            int r6 = r6 + 1
            goto L_0x001a
        L_0x0049:
            java.util.Iterator r2 = r2.iterator()
        L_0x004d:
            boolean r3 = r2.hasNext()
            if (r3 == 0) goto L_0x01d4
            java.lang.Object r3 = r2.next()
            java.lang.String r3 = (java.lang.String) r3
            java.lang.String r4 = ""
            java.lang.String r3 = r3.replaceFirst(r7, r4)
            java.lang.String r6 = "List"
            boolean r6 = r3.endsWith(r6)
            r8 = 1
            if (r6 == 0) goto L_0x00b6
            java.lang.String r6 = "OrBuilderList"
            boolean r6 = r3.endsWith(r6)
            if (r6 != 0) goto L_0x00b6
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = r3.substring(r5, r8)
            java.lang.String r9 = r9.toLowerCase()
            r6.append(r9)
            int r9 = r3.length()
            int r9 = r9 + -4
            java.lang.String r9 = r3.substring(r8, r9)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r7)
            r9.append(r3)
            java.lang.String r9 = r9.toString()
            java.lang.Object r9 = r0.get(r9)
            java.lang.reflect.Method r9 = (java.lang.reflect.Method) r9
            if (r9 == 0) goto L_0x00b6
            java.lang.String r3 = b(r6)
            java.lang.Object[] r4 = new java.lang.Object[r5]
            java.lang.Object r4 = j.c.e.GeneratedMessageLite.a(r9, r13, r4)
            a(r14, r15, r3, r4)
            goto L_0x004d
        L_0x00b6:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = "set"
            r6.append(r9)
            r6.append(r3)
            java.lang.String r6 = r6.toString()
            java.lang.Object r6 = r1.get(r6)
            java.lang.reflect.Method r6 = (java.lang.reflect.Method) r6
            if (r6 != 0) goto L_0x00d1
            goto L_0x004d
        L_0x00d1:
            java.lang.String r6 = "Bytes"
            boolean r6 = r3.endsWith(r6)
            if (r6 == 0) goto L_0x00f6
            java.lang.StringBuilder r6 = j.a.a.a.outline.a(r7)
            int r9 = r3.length()
            int r9 = r9 + -5
            java.lang.String r9 = r3.substring(r5, r9)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            boolean r6 = r0.containsKey(r6)
            if (r6 == 0) goto L_0x00f6
            goto L_0x004d
        L_0x00f6:
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>()
            java.lang.String r9 = r3.substring(r5, r8)
            java.lang.String r9 = r9.toLowerCase()
            r6.append(r9)
            java.lang.String r9 = r3.substring(r8)
            r6.append(r9)
            java.lang.String r6 = r6.toString()
            java.lang.StringBuilder r9 = new java.lang.StringBuilder
            r9.<init>()
            r9.append(r7)
            r9.append(r3)
            java.lang.String r9 = r9.toString()
            java.lang.Object r9 = r0.get(r9)
            java.lang.reflect.Method r9 = (java.lang.reflect.Method) r9
            java.lang.StringBuilder r10 = new java.lang.StringBuilder
            r10.<init>()
            java.lang.String r11 = "has"
            r10.append(r11)
            r10.append(r3)
            java.lang.String r3 = r10.toString()
            java.lang.Object r3 = r0.get(r3)
            java.lang.reflect.Method r3 = (java.lang.reflect.Method) r3
            if (r9 == 0) goto L_0x004d
            java.lang.Object[] r10 = new java.lang.Object[r5]
            java.lang.Object r9 = j.c.e.GeneratedMessageLite.a(r9, r13, r10)
            if (r3 != 0) goto L_0x01bd
            boolean r3 = r9 instanceof java.lang.Boolean
            if (r3 == 0) goto L_0x0155
            r3 = r9
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r3 = r3.booleanValue()
            r3 = r3 ^ r8
            goto L_0x01b8
        L_0x0155:
            boolean r3 = r9 instanceof java.lang.Integer
            if (r3 == 0) goto L_0x0163
            r3 = r9
            java.lang.Integer r3 = (java.lang.Integer) r3
            int r3 = r3.intValue()
            if (r3 != 0) goto L_0x01b7
            goto L_0x01b5
        L_0x0163:
            boolean r3 = r9 instanceof java.lang.Float
            if (r3 == 0) goto L_0x0174
            r3 = r9
            java.lang.Float r3 = (java.lang.Float) r3
            float r3 = r3.floatValue()
            r4 = 0
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 != 0) goto L_0x01b7
            goto L_0x01b5
        L_0x0174:
            boolean r3 = r9 instanceof java.lang.Double
            if (r3 == 0) goto L_0x0186
            r3 = r9
            java.lang.Double r3 = (java.lang.Double) r3
            double r3 = r3.doubleValue()
            r10 = 0
            int r12 = (r3 > r10 ? 1 : (r3 == r10 ? 0 : -1))
            if (r12 != 0) goto L_0x01b7
            goto L_0x01b5
        L_0x0186:
            boolean r3 = r9 instanceof java.lang.String
            if (r3 == 0) goto L_0x018f
            boolean r3 = r9.equals(r4)
            goto L_0x01b8
        L_0x018f:
            boolean r3 = r9 instanceof j.c.e.ByteString
            if (r3 == 0) goto L_0x019a
            j.c.e.ByteString r3 = j.c.e.ByteString.c
            boolean r3 = r9.equals(r3)
            goto L_0x01b8
        L_0x019a:
            boolean r3 = r9 instanceof j.c.e.MessageLite
            if (r3 == 0) goto L_0x01a8
            r3 = r9
            j.c.e.MessageLite r3 = (j.c.e.MessageLite) r3
            j.c.e.MessageLite r3 = r3.d()
            if (r9 != r3) goto L_0x01b7
            goto L_0x01b5
        L_0x01a8:
            boolean r3 = r9 instanceof java.lang.Enum
            if (r3 == 0) goto L_0x01b7
            r3 = r9
            java.lang.Enum r3 = (java.lang.Enum) r3
            int r3 = r3.ordinal()
            if (r3 != 0) goto L_0x01b7
        L_0x01b5:
            r3 = 1
            goto L_0x01b8
        L_0x01b7:
            r3 = 0
        L_0x01b8:
            if (r3 != 0) goto L_0x01bb
            goto L_0x01c9
        L_0x01bb:
            r8 = 0
            goto L_0x01c9
        L_0x01bd:
            java.lang.Object[] r4 = new java.lang.Object[r5]
            java.lang.Object r3 = j.c.e.GeneratedMessageLite.a(r3, r13, r4)
            java.lang.Boolean r3 = (java.lang.Boolean) r3
            boolean r8 = r3.booleanValue()
        L_0x01c9:
            if (r8 == 0) goto L_0x004d
            java.lang.String r3 = b(r6)
            a(r14, r15, r3, r9)
            goto L_0x004d
        L_0x01d4:
            boolean r0 = r13 instanceof j.c.e.GeneratedMessageLite.e
            if (r0 == 0) goto L_0x021b
            r0 = r13
            j.c.e.GeneratedMessageLite$e r0 = (j.c.e.GeneratedMessageLite.e) r0
            j.c.e.FieldSet<j.c.e.k$g> r0 = r0.f2595e
            boolean r1 = r0.c
            if (r1 == 0) goto L_0x01f1
            j.c.e.LazyField$c r1 = new j.c.e.LazyField$c
            j.c.e.SmallSortedMap<FieldDescriptorType, java.lang.Object> r0 = r0.a
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r0 = r0.iterator()
            r1.<init>(r0)
            goto L_0x01fb
        L_0x01f1:
            j.c.e.SmallSortedMap<FieldDescriptorType, java.lang.Object> r0 = r0.a
            java.util.Set r0 = r0.entrySet()
            java.util.Iterator r1 = r0.iterator()
        L_0x01fb:
            boolean r0 = r1.hasNext()
            if (r0 == 0) goto L_0x021b
            java.lang.Object r0 = r1.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            java.lang.Object r2 = r0.getKey()
            j.c.e.GeneratedMessageLite$g r2 = (j.c.e.GeneratedMessageLite.g) r2
            if (r2 == 0) goto L_0x0219
            java.lang.Object r0 = r0.getValue()
            java.lang.String r2 = "[0]"
            a(r14, r15, r2, r0)
            goto L_0x01fb
        L_0x0219:
            r13 = 0
            throw r13
        L_0x021b:
            j.c.e.GeneratedMessageLite r13 = (j.c.e.GeneratedMessageLite) r13
            j.c.e.UnknownFieldSetLite r13 = r13.c
            if (r13 == 0) goto L_0x0239
        L_0x0221:
            int r0 = r13.a
            if (r5 >= r0) goto L_0x0239
            int[] r0 = r13.b
            r0 = r0[r5]
            int r0 = r0 >>> 3
            java.lang.String r0 = java.lang.String.valueOf(r0)
            java.lang.Object[] r1 = r13.c
            r1 = r1[r5]
            a(r14, r15, r0, r1)
            int r5 = r5 + 1
            goto L_0x0221
        L_0x0239:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.n.c.a(j.c.e.MessageLite, java.lang.StringBuilder, int):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
     arg types: [android.view.View, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void */
    public static Observable<Object> a(View view) {
        m12a((Object) view, "view == null");
        return new ViewClickObservable(view);
    }

    public static float a(Context context, int i2) {
        return TypedValue.applyDimension(1, (float) i2, context.getResources().getDisplayMetrics());
    }

    public static void a(RegistryConfig registryConfig) {
        for (KeyTypeEntry next : registryConfig.g) {
            if (next.f2486f.isEmpty()) {
                throw new GeneralSecurityException("Missing type_url.");
            } else if (next.f2485e.isEmpty()) {
                throw new GeneralSecurityException("Missing primitive_name.");
            } else if (!next.f2487i.isEmpty()) {
                Registry.a(Registry.a(next.f2487i).a(next.f2486f, next.f2485e, next.g), next.h);
            } else {
                throw new GeneralSecurityException("Missing catalogue_name.");
            }
        }
    }

    public static final byte[] a(byte[] bArr, int i2, byte[] bArr2, int i3, int i4) {
        if (i4 < 0 || bArr.length - i4 < i2 || bArr2.length - i4 < i3) {
            throw new IllegalArgumentException("That combination of buffers, offsets and length to xor result in out-of-bond accesses.");
        }
        byte[] bArr3 = new byte[i4];
        for (int i5 = 0; i5 < i4; i5++) {
            bArr3[i5] = (byte) (bArr[i5 + i2] ^ bArr2[i5 + i3]);
        }
        return bArr3;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX WARN: Type inference failed for: r0v1 */
    /* JADX WARN: Type inference failed for: r8v4, types: [int] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static int a(j.c.a.a.f.e.t5 r6, byte[] r7, int r8, int r9, j.c.a.a.f.e.s2 r10) {
        /*
            int r0 = r8 + 1
            byte r8 = r7[r8]
            if (r8 >= 0) goto L_0x000c
            int r0 = a(r8, r7, r0, r10)
            int r8 = r10.a
        L_0x000c:
            r3 = r0
            if (r8 < 0) goto L_0x0025
            int r9 = r9 - r3
            if (r8 > r9) goto L_0x0025
            java.lang.Object r9 = r6.a()
            int r8 = r8 + r3
            r0 = r6
            r1 = r9
            r2 = r7
            r4 = r8
            r5 = r10
            r0.a(r1, r2, r3, r4, r5)
            r6.b(r9)
            r10.c = r9
            return r8
        L_0x0025:
            com.google.android.gms.internal.measurement.zzfn r6 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int");
    }

    public static EllipticCurves1 a(EcPointFormat ecPointFormat) {
        int ordinal = ecPointFormat.ordinal();
        if (ordinal == 1) {
            return EllipticCurves1.UNCOMPRESSED;
        }
        if (ordinal == 2) {
            return EllipticCurves1.COMPRESSED;
        }
        if (ordinal == 3) {
            return EllipticCurves1.DO_NOT_USE_CRUNCHY_UNCOMPRESSED;
        }
        throw new GeneralSecurityException("unknown point format: " + ecPointFormat);
    }

    public static int a(View view, int i2) {
        return a(view.getContext(), i2, view.getClass().getCanonicalName());
    }

    public static final void a(String str, ExecutorService executorService) {
        Runtime.getRuntime().addShutdownHook(new Thread(new ExecutorUtils0(str, executorService, 2, TimeUnit.SECONDS), outline.a("Crashlytics Shutdown Hook for ", str)));
    }

    public static int a(Context context, int i2, int i3) {
        TypedValue c2 = c(context, i2);
        return c2 != null ? c2.data : i3;
    }

    public static int a(t5 t5Var, byte[] bArr, int i2, int i3, int i4, s2 s2Var) {
        h5 h5Var = (h5) t5Var;
        Object a2 = h5Var.f1860l.a(h5Var.f1855e);
        int a3 = h5Var.a(a2, bArr, i2, i3, i4, s2Var);
        h5Var.b(a2);
        s2Var.c = a2;
        return a3;
    }

    public static Calendar a(Calendar calendar) {
        Calendar b2 = b(calendar);
        Calendar c2 = c();
        c2.set(b2.get(1), b2.get(2), b2.get(5));
        return c2;
    }

    public static <T> void a(Object obj, Class cls) {
        if (obj == null) {
            throw new IllegalStateException(cls.getCanonicalName() + " must be set");
        }
    }

    public static final void a(ByteBuffer byteBuffer, ByteBuffer byteBuffer2, ByteBuffer byteBuffer3, int i2) {
        if (i2 < 0 || byteBuffer2.remaining() < i2 || byteBuffer3.remaining() < i2 || byteBuffer.remaining() < i2) {
            throw new IllegalArgumentException("That combination of buffers, offsets and length to xor result in out-of-bond accesses.");
        }
        for (int i3 = 0; i3 < i2; i3++) {
            byteBuffer.put((byte) (byteBuffer2.get() ^ byteBuffer3.get()));
        }
    }

    public static int a(int i2, byte[] bArr, int i3, int i4, e4<?> e4Var, s2 s2Var) {
        z3 z3Var = (z3) e4Var;
        int a2 = a(bArr, i3, s2Var);
        z3Var.d(s2Var.a);
        while (a2 < i4) {
            int a3 = a(bArr, a2, s2Var);
            if (i2 != s2Var.a) {
                break;
            }
            a2 = a(bArr, a3, s2Var);
            z3Var.d(s2Var.a);
        }
        return a2;
    }

    public static EllipticCurves0 a(EcdsaSignatureEncoding ecdsaSignatureEncoding) {
        int ordinal = ecdsaSignatureEncoding.ordinal();
        if (ordinal == 1) {
            return EllipticCurves0.IEEE_P1363;
        }
        if (ordinal == 2) {
            return EllipticCurves0.DER;
        }
        throw new GeneralSecurityException("unknown ECDSA encoding: " + ecdsaSignatureEncoding);
    }

    public static boolean a(Context context, int i2, boolean z) {
        TypedValue c2 = c(context, i2);
        if (c2 == null || c2.type != 18) {
            return z;
        }
        return c2.data != 0;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void */
    public static InitialValueObservable<CharSequence> a(TextView textView) {
        m12a((Object) textView, "view == null");
        return new TextViewTextObservable(textView);
    }

    public static int a(byte[] bArr, int i2, e4<?> e4Var, s2 s2Var) {
        z3 z3Var = (z3) e4Var;
        int a2 = a(bArr, i2, s2Var);
        int i3 = s2Var.a + a2;
        while (a2 < i3) {
            a2 = a(bArr, a2, s2Var);
            z3Var.d(s2Var.a);
        }
        if (a2 == i3) {
            return a2;
        }
        throw zzfn.a();
    }

    public static final Object a(Throwable th) {
        if (th != null) {
            return new Result.a(th);
        }
        Intrinsics.a("exception");
        throw null;
    }

    public static int a(t5<?> t5Var, int i2, byte[] bArr, int i3, int i4, e4<?> e4Var, s2 s2Var) {
        int a2 = a(t5Var, bArr, i3, i4, s2Var);
        e4Var.add(s2Var.c);
        while (a2 < i4) {
            int a3 = a(bArr, a2, s2Var);
            if (i2 != s2Var.a) {
                break;
            }
            a2 = a(t5Var, bArr, a3, i4, s2Var);
            e4Var.add(s2Var.c);
        }
        return a2;
    }

    public static int a(int i2, int i3, float f2) {
        return ColorUtils.a(ColorUtils.b(i3, Math.round(((float) Color.alpha(i3)) * f2)), i2);
    }

    public static int a(int i2, byte[] bArr, int i3, int i4, i6 i6Var, s2 s2Var) {
        if ((i2 >>> 3) != 0) {
            int i5 = i2 & 7;
            if (i5 == 0) {
                int b2 = b(bArr, i3, s2Var);
                i6Var.a(i2, Long.valueOf(s2Var.b));
                return b2;
            } else if (i5 == 1) {
                i6Var.a(i2, Long.valueOf(c(bArr, i3)));
                return i3 + 8;
            } else if (i5 == 2) {
                int a2 = a(bArr, i3, s2Var);
                int i6 = s2Var.a;
                if (i6 < 0) {
                    throw zzfn.b();
                } else if (i6 <= bArr.length - a2) {
                    if (i6 == 0) {
                        i6Var.a(i2, w2.c);
                    } else {
                        i6Var.a(i2, w2.a(bArr, a2, i6));
                    }
                    return a2 + i6;
                } else {
                    throw zzfn.a();
                }
            } else if (i5 == 3) {
                i6 b3 = i6.b();
                int i7 = (i2 & -8) | 4;
                int i8 = 0;
                while (true) {
                    if (i3 >= i4) {
                        break;
                    }
                    int a3 = a(bArr, i3, s2Var);
                    int i9 = s2Var.a;
                    i8 = i9;
                    if (i9 == i7) {
                        i3 = a3;
                        break;
                    }
                    int a4 = a(i8, bArr, a3, i4, b3, s2Var);
                    i8 = i9;
                    i3 = a4;
                }
                if (i3 > i4 || i8 != i7) {
                    throw zzfn.e();
                }
                i6Var.a(i2, b3);
                return i3;
            } else if (i5 == 5) {
                i6Var.a(i2, Integer.valueOf(b(bArr, i3)));
                return i3 + 4;
            } else {
                throw zzfn.c();
            }
        } else {
            throw zzfn.c();
        }
    }

    public static int a(int i2, byte[] bArr, int i3, int i4, s2 s2Var) {
        if ((i2 >>> 3) != 0) {
            int i5 = i2 & 7;
            if (i5 == 0) {
                return b(bArr, i3, s2Var);
            }
            if (i5 == 1) {
                return i3 + 8;
            }
            if (i5 == 2) {
                return a(bArr, i3, s2Var) + s2Var.a;
            }
            if (i5 == 3) {
                int i6 = (i2 & -8) | 4;
                int i7 = 0;
                while (i3 < i4) {
                    i3 = a(bArr, i3, s2Var);
                    i7 = s2Var.a;
                    if (i7 == i6) {
                        break;
                    }
                    i3 = a(i7, bArr, i3, i4, s2Var);
                }
                if (i3 <= i4 && i7 == i6) {
                    return i3;
                }
                throw zzfn.e();
            } else if (i5 == 5) {
                return i3 + 4;
            } else {
                throw zzfn.c();
            }
        } else {
            throw zzfn.c();
        }
    }

    /* JADX WARN: Type inference failed for: r7v1, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public static final void a(StringBuilder sb, int i2, String str, Object obj) {
        if (obj instanceof List) {
            for (Object a2 : (List) obj) {
                a(sb, i2, str, a2);
            }
            return;
        }
        sb.append(10);
        for (int i3 = 0; i3 < i2; i3++) {
            sb.append(' ');
        }
        sb.append(str);
        if (obj instanceof String) {
            sb.append(": \"");
            sb.append(a(ByteString.a((String) obj)));
            sb.append('\"');
        } else if (obj instanceof ByteString) {
            sb.append(": \"");
            sb.append(a((ByteString) obj));
            sb.append('\"');
        } else if (obj instanceof GeneratedMessageLite) {
            sb.append(" {");
            a((MessageLite) ((GeneratedMessageLite) obj), sb, i2 + 2);
            sb.append("\n");
            for (int i4 = 0; i4 < i2; i4++) {
                sb.append(' ');
            }
            sb.append("}");
        } else {
            sb.append(": ");
            sb.append(obj.toString());
        }
    }

    public static void a(ECPoint eCPoint, EllipticCurve ellipticCurve) {
        ECField field = ellipticCurve.getField();
        if (field instanceof ECFieldFp) {
            BigInteger p2 = ((ECFieldFp) field).getP();
            BigInteger affineX = eCPoint.getAffineX();
            BigInteger affineY = eCPoint.getAffineY();
            if (affineX == null || affineY == null) {
                throw new GeneralSecurityException("point is at infinity");
            } else if (affineX.signum() == -1 || affineX.compareTo(p2) != -1) {
                throw new GeneralSecurityException("x is out of range");
            } else if (affineY.signum() == -1 || affineY.compareTo(p2) != -1) {
                throw new GeneralSecurityException("y is out of range");
            } else if (!affineY.multiply(affineY).mod(p2).equals(affineX.multiply(affineX).add(ellipticCurve.getA()).multiply(affineX).add(ellipticCurve.getB()).mod(p2))) {
                throw new GeneralSecurityException("Point is not on curve");
            }
        } else {
            throw new GeneralSecurityException("Only curves over prime order fields are supported");
        }
    }

    public static ColorStateList a(Context context, TintTypedArray tintTypedArray, int i2) {
        int resourceId;
        ColorStateList b2;
        if (!tintTypedArray.b.hasValue(i2) || (resourceId = tintTypedArray.b.getResourceId(i2, 0)) == 0 || (b2 = AppCompatResources.b(context, resourceId)) == null) {
            return tintTypedArray.a(i2);
        }
        return b2;
    }

    public static ECParameterSpec a(String str, String str2, String str3, String str4, String str5) {
        BigInteger bigInteger = new BigInteger(str);
        return new ECParameterSpec(new EllipticCurve(new ECFieldFp(bigInteger), bigInteger.subtract(new BigInteger("3")), new BigInteger(str3, 16)), new ECPoint(new BigInteger(str4, 16), new BigInteger(str5, 16)), new BigInteger(str2), 1);
    }

    public static KeyTypeEntry a(String str, String str2, String str3, int i2, boolean z) {
        KeyTypeEntry.b bVar = (KeyTypeEntry.b) KeyTypeEntry.f2483j.e();
        bVar.m();
        KeyTypeEntry.a((KeyTypeEntry) bVar.c, str2);
        bVar.m();
        KeyTypeEntry.c((KeyTypeEntry) bVar.c, "type.googleapis.com/google.crypto.tink." + str3);
        bVar.m();
        ((KeyTypeEntry) bVar.c).g = i2;
        bVar.m();
        ((KeyTypeEntry) bVar.c).h = z;
        bVar.m();
        KeyTypeEntry.b((KeyTypeEntry) bVar.c, str);
        return (KeyTypeEntry) bVar.k();
    }

    public static void a(View view, MaterialShapeDrawable materialShapeDrawable) {
        ElevationOverlayProvider elevationOverlayProvider = materialShapeDrawable.b.b;
        if (elevationOverlayProvider != null && elevationOverlayProvider.a) {
            float f2 = 0.0f;
            for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
                f2 += ViewCompat.g((View) parent);
            }
            MaterialShapeDrawable.b bVar = materialShapeDrawable.b;
            if (bVar.f2235n != f2) {
                bVar.f2235n = f2;
                materialShapeDrawable.j();
            }
        }
    }

    public static ECPublicKey a(EllipticCurves ellipticCurves, byte[] bArr, byte[] bArr2) {
        ECParameterSpec b2 = b(ellipticCurves);
        ECPoint eCPoint = new ECPoint(new BigInteger(1, bArr), new BigInteger(1, bArr2));
        a(eCPoint, b2.getCurve());
        return (ECPublicKey) EngineFactory.f2384j.a("EC").generatePublic(new ECPublicKeySpec(eCPoint, b2));
    }

    public static ECPrivateKey a(EllipticCurves ellipticCurves, byte[] bArr) {
        return (ECPrivateKey) EngineFactory.f2384j.a("EC").generatePrivate(new ECPrivateKeySpec(new BigInteger(1, bArr), b(ellipticCurves)));
    }

    public static KeyPair a(EllipticCurves ellipticCurves) {
        ECParameterSpec b2 = b(ellipticCurves);
        KeyPairGenerator a2 = EngineFactory.f2383i.a("EC");
        a2.initialize(b2);
        return a2.generateKeyPair();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Scheduler, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static l a(Callable<l> callable) {
        try {
            Scheduler call = callable.call();
            ObjectHelper.a((Object) call, "Scheduler Callable result can't be null");
            return call;
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    public static final char a(char[] cArr) {
        if (cArr != null) {
            int length = cArr.length;
            if (length == 0) {
                throw new NoSuchElementException("Array is empty.");
            } else if (length == 1) {
                return cArr[0];
            } else {
                throw new IllegalArgumentException("Array has more than one element.");
            }
        } else {
            Intrinsics.a("$this$single");
            throw null;
        }
    }

    public static final <T> List<T> a(Object[] objArr) {
        if (objArr != null) {
            return new ArrayList(new Collections0(objArr, false));
        }
        Intrinsics.a("$this$toMutableList");
        throw null;
    }
}
