package j.c.a.a.g.a;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
public final class b7 implements Runnable {
    public final /* synthetic */ fb b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ boolean f1942e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ AppMeasurementDynamiteService f1943f;

    public b7(AppMeasurementDynamiteService appMeasurementDynamiteService, fb fbVar, String str, String str2, boolean z) {
        this.f1943f = appMeasurementDynamiteService;
        this.b = fbVar;
        this.c = str;
        this.d = str2;
        this.f1942e = z;
    }

    public final void run() {
        z6 r2 = this.f1943f.a.r();
        fb fbVar = this.b;
        String str = this.c;
        String str2 = this.d;
        boolean z = this.f1942e;
        r2.d();
        r2.w();
        r2.a(new s7(r2, str, str2, z, r2.a(false), fbVar));
    }
}
