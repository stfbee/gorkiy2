package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class c2 extends o5 implements p5 {
    public c2(r4 r4Var) {
        super(r4Var);
        ResourcesFlusher.b(r4Var);
    }

    public void b() {
        if (super.a == null) {
            throw null;
        }
    }

    public void c() {
        super.a.i().c();
    }

    public void d() {
        super.a.i().d();
    }

    public void n() {
        super.a.d();
        throw null;
    }

    public a o() {
        return super.a.u();
    }

    public x5 p() {
        return super.a.m();
    }

    public g3 q() {
        return super.a.t();
    }

    public z6 r() {
        return super.a.r();
    }

    public y6 s() {
        return super.a.q();
    }

    public j3 t() {
        r4 r4Var = super.a;
        r4.a((e5) r4Var.f2097s);
        return r4Var.f2097s;
    }

    public f8 u() {
        r4 r4Var = super.a;
        r4.a((e5) r4Var.f2089k);
        return r4Var.f2089k;
    }
}
