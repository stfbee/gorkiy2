package j.c.a.a.f.e;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class z3 extends r2<Integer> implements c4, p5, RandomAccess {

    /* renamed from: e  reason: collision with root package name */
    public static final z3 f1932e;
    public int[] c;
    public int d;

    static {
        z3 z3Var = new z3(new int[0], 0);
        f1932e = z3Var;
        super.b = false;
    }

    public z3() {
        this.c = new int[10];
        this.d = 0;
    }

    public final /* synthetic */ void add(int i2, Object obj) {
        int i3;
        int intValue = ((Integer) obj).intValue();
        c();
        if (i2 < 0 || i2 > (i3 = this.d)) {
            throw new IndexOutOfBoundsException(f(i2));
        }
        int[] iArr = this.c;
        if (i3 < iArr.length) {
            System.arraycopy(iArr, i2, iArr, i2 + 1, i3 - i2);
        } else {
            int[] iArr2 = new int[(((i3 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i2);
            System.arraycopy(this.c, i2, iArr2, i2 + 1, this.d - i2);
            this.c = iArr2;
        }
        this.c[i2] = intValue;
        this.d++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Integer> collection) {
        c();
        y3.a(collection);
        if (!(collection instanceof z3)) {
            return super.addAll(collection);
        }
        z3 z3Var = (z3) collection;
        int i2 = z3Var.d;
        if (i2 == 0) {
            return false;
        }
        int i3 = this.d;
        if (Integer.MAX_VALUE - i3 >= i2) {
            int i4 = i3 + i2;
            int[] iArr = this.c;
            if (i4 > iArr.length) {
                this.c = Arrays.copyOf(iArr, i4);
            }
            System.arraycopy(z3Var.c, 0, this.c, this.d, z3Var.d);
            this.d = i4;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    /* renamed from: b */
    public final c4 a(int i2) {
        if (i2 >= this.d) {
            return new z3(Arrays.copyOf(this.c, i2), this.d);
        }
        throw new IllegalArgumentException();
    }

    public final int c(int i2) {
        e(i2);
        return this.c[i2];
    }

    public final void d(int i2) {
        c();
        int i3 = this.d;
        int[] iArr = this.c;
        if (i3 == iArr.length) {
            int[] iArr2 = new int[(((i3 * 3) / 2) + 1)];
            System.arraycopy(iArr, 0, iArr2, 0, i3);
            this.c = iArr2;
        }
        int[] iArr3 = this.c;
        int i4 = this.d;
        this.d = i4 + 1;
        iArr3[i4] = i2;
    }

    public final void e(int i2) {
        if (i2 < 0 || i2 >= this.d) {
            throw new IndexOutOfBoundsException(f(i2));
        }
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof z3)) {
            return super.equals(obj);
        }
        z3 z3Var = (z3) obj;
        if (this.d != z3Var.d) {
            return false;
        }
        int[] iArr = z3Var.c;
        for (int i2 = 0; i2 < this.d; i2++) {
            if (this.c[i2] != iArr[i2]) {
                return false;
            }
        }
        return true;
    }

    public final String f(int i2) {
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i2);
        sb.append(", Size:");
        sb.append(i3);
        return sb.toString();
    }

    public final /* synthetic */ Object get(int i2) {
        e(i2);
        return Integer.valueOf(this.c[i2]);
    }

    public final int hashCode() {
        int i2 = 1;
        for (int i3 = 0; i3 < this.d; i3++) {
            i2 = (i2 * 31) + this.c[i3];
        }
        return i2;
    }

    public final boolean remove(Object obj) {
        c();
        for (int i2 = 0; i2 < this.d; i2++) {
            if (obj.equals(Integer.valueOf(this.c[i2]))) {
                int[] iArr = this.c;
                System.arraycopy(iArr, i2 + 1, iArr, i2, (this.d - i2) - 1);
                this.d--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    public final void removeRange(int i2, int i3) {
        c();
        if (i3 >= i2) {
            int[] iArr = this.c;
            System.arraycopy(iArr, i3, iArr, i2, this.d - i3);
            this.d -= i3 - i2;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i2, Object obj) {
        int intValue = ((Integer) obj).intValue();
        c();
        e(i2);
        int[] iArr = this.c;
        int i3 = iArr[i2];
        iArr[i2] = intValue;
        return Integer.valueOf(i3);
    }

    public final int size() {
        return this.d;
    }

    public z3(int[] iArr, int i2) {
        this.c = iArr;
        this.d = i2;
    }

    public final /* synthetic */ Object remove(int i2) {
        c();
        e(i2);
        int[] iArr = this.c;
        int i3 = iArr[i2];
        int i4 = this.d;
        if (i2 < i4 - 1) {
            System.arraycopy(iArr, i2 + 1, iArr, i2, (i4 - i2) - 1);
        }
        this.d--;
        this.modCount++;
        return Integer.valueOf(i3);
    }

    public final /* synthetic */ boolean add(Object obj) {
        d(((Integer) obj).intValue());
        return true;
    }
}
