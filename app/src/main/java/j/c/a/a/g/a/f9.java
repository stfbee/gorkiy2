package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.f.e.a0;
import j.c.a.a.f.e.b0;
import j.c.a.a.f.e.c0;
import j.c.a.a.f.e.d0;
import j.c.a.a.f.e.e0;
import j.c.a.a.f.e.p0;
import j.c.a.a.f.e.s0;
import j.c.a.a.f.e.x3;
import j.c.a.a.f.e.y0;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class f9 extends o8 {
    public f9(q8 q8Var) {
        super(q8Var);
    }

    public static void b(Map<Integer, List<Long>> map, int i2, long j2) {
        Object obj = map.get(Integer.valueOf(i2));
        if (obj == null) {
            obj = new ArrayList();
            map.put(Integer.valueOf(i2), obj);
        }
        obj.add(Long.valueOf(j2 / 1000));
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v23, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r19v6, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r12v42, resolved type: java.lang.Long} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r0v118, resolved type: java.lang.Object} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v49, resolved type: java.lang.String} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v36, resolved type: java.util.ArrayList} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v37, resolved type: j.c.a.a.f.e.e4<j.c.a.a.f.e.s0>} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r3v101, resolved type: j.c.a.a.f.e.q0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v70, resolved type: j.c.a.a.f.e.e4<j.c.a.a.f.e.s0>} */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void}
     arg types: [java.lang.String, int]
     candidates:
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Byte):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Float):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.String):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Long):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Boolean):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, byte[]):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Double):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Short):void}
      ClspMth{android.content.ContentValues.put(java.lang.String, java.lang.Integer):void} */
    /* JADX WARNING: Code restructure failed: missing block: B:422:0x0d37, code lost:
        if (r10.a() == false) goto L_0x0d42;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:423:0x0d39, code lost:
        r6 = java.lang.Integer.valueOf(r10.i());
     */
    /* JADX WARNING: Code restructure failed: missing block: B:424:0x0d42, code lost:
        r6 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:425:0x0d43, code lost:
        r0.a("Invalid property filter ID. appId, id", r2, java.lang.String.valueOf(r6));
        r15.add(java.lang.Integer.valueOf(r16));
        r10 = r84;
        r11 = r85;
        r9 = r17;
        r13 = r18;
        r8 = r20;
        r6 = r22;
        r14 = r23;
        r0 = r27;
        r22 = r43;
        r7 = r44;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:113:0x036d  */
    /* JADX WARNING: Removed duplicated region for block: B:163:0x048f  */
    /* JADX WARNING: Removed duplicated region for block: B:168:0x04ac  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x04b0  */
    /* JADX WARNING: Removed duplicated region for block: B:186:0x0546  */
    /* JADX WARNING: Removed duplicated region for block: B:191:0x05c4  */
    /* JADX WARNING: Removed duplicated region for block: B:198:0x0653  */
    /* JADX WARNING: Removed duplicated region for block: B:205:0x0674  */
    /* JADX WARNING: Removed duplicated region for block: B:223:0x076b  */
    /* JADX WARNING: Removed duplicated region for block: B:306:0x09dc  */
    /* JADX WARNING: Removed duplicated region for block: B:338:0x0b4c  */
    /* JADX WARNING: Removed duplicated region for block: B:521:0x10da  */
    /* JADX WARNING: Removed duplicated region for block: B:568:0x0d6b A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:65:0x01d8  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final java.util.List<j.c.a.a.f.e.o0> a(java.lang.String r83, java.util.List<j.c.a.a.f.e.q0> r84, java.util.List<j.c.a.a.f.e.y0> r85, java.lang.Long r86) {
        /*
            r82 = this;
            r7 = r82
            r9 = r83
            i.b.k.ResourcesFlusher.b(r83)
            i.b.k.ResourcesFlusher.b(r84)
            i.b.k.ResourcesFlusher.b(r85)
            java.util.HashSet r15 = new java.util.HashSet
            r15.<init>()
            i.e.ArrayMap r13 = new i.e.ArrayMap
            r13.<init>()
            i.e.ArrayMap r14 = new i.e.ArrayMap
            r14.<init>()
            i.e.ArrayMap r11 = new i.e.ArrayMap
            r11.<init>()
            i.e.ArrayMap r12 = new i.e.ArrayMap
            r12.<init>()
            i.e.ArrayMap r10 = new i.e.ArrayMap
            r10.<init>()
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.w0
            boolean r0 = r0.d(r9, r1)
            r8 = 1
            r6 = 0
            if (r0 != 0) goto L_0x0045
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.x0
            boolean r0 = r0.d(r9, r1)
            if (r0 == 0) goto L_0x0062
        L_0x0045:
            java.util.Iterator r0 = r84.iterator()
        L_0x0049:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x0062
            java.lang.Object r1 = r0.next()
            j.c.a.a.f.e.q0 r1 = (j.c.a.a.f.e.q0) r1
            java.lang.String r1 = r1.zze
            java.lang.String r2 = "_s"
            boolean r1 = r2.equals(r1)
            if (r1 == 0) goto L_0x0049
            r25 = 1
            goto L_0x0064
        L_0x0062:
            r25 = 0
        L_0x0064:
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            boolean r1 = r0.d(r9)
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.w0
            boolean r2 = r0.d(r9, r2)
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r3 = j.c.a.a.g.a.k.x0
            boolean r0 = r0.d(r9, r3)
            if (r25 == 0) goto L_0x00c8
            if (r0 == 0) goto L_0x00c8
            j.c.a.a.g.a.n9 r3 = r82.s()
            r3.o()
            r3.d()
            i.b.k.ResourcesFlusher.b(r83)
            android.content.ContentValues r0 = new android.content.ContentValues
            r0.<init>()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r6)
            java.lang.String r5 = "current_session_count"
            r0.put(r5, r4)
            android.database.sqlite.SQLiteDatabase r4 = r3.v()     // Catch:{ SQLiteException -> 0x00b5 }
            java.lang.String r5 = "events"
            java.lang.String r6 = "app_id = ?"
            r17 = r15
            java.lang.String[] r15 = new java.lang.String[r8]     // Catch:{ SQLiteException -> 0x00b3 }
            r16 = 0
            r15[r16] = r9     // Catch:{ SQLiteException -> 0x00b3 }
            r4.update(r5, r0, r6, r15)     // Catch:{ SQLiteException -> 0x00b3 }
            goto L_0x00ca
        L_0x00b3:
            r0 = move-exception
            goto L_0x00b8
        L_0x00b5:
            r0 = move-exception
            r17 = r15
        L_0x00b8:
            j.c.a.a.g.a.n3 r3 = r3.a()
            j.c.a.a.g.a.p3 r3 = r3.f2046f
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r83)
            java.lang.String r5 = "Error resetting session-scoped event counts. appId"
            r3.a(r5, r4, r0)
            goto L_0x00ca
        L_0x00c8:
            r17 = r15
        L_0x00ca:
            j.c.a.a.g.a.n9 r0 = r82.s()
            java.util.Map r0 = r0.e(r9)
            if (r0 == 0) goto L_0x033d
            boolean r3 = r0.isEmpty()
            if (r3 != 0) goto L_0x033d
            java.util.HashSet r3 = new java.util.HashSet
            java.util.Set r4 = r0.keySet()
            r3.<init>(r4)
            if (r2 == 0) goto L_0x01cd
            if (r25 == 0) goto L_0x01cd
            j.c.a.a.g.a.f9 r4 = r82.n()
            i.b.k.ResourcesFlusher.b(r83)
            i.b.k.ResourcesFlusher.b(r0)
            i.e.ArrayMap r5 = new i.e.ArrayMap
            r5.<init>()
            boolean r6 = r0.isEmpty()
            if (r6 != 0) goto L_0x01ce
            j.c.a.a.g.a.n9 r6 = r4.s()
            java.util.Map r6 = r6.d(r9)
            java.util.Set r18 = r0.keySet()
            java.util.Iterator r18 = r18.iterator()
        L_0x010c:
            boolean r19 = r18.hasNext()
            if (r19 == 0) goto L_0x01ce
            java.lang.Object r19 = r18.next()
            java.lang.Integer r19 = (java.lang.Integer) r19
            int r19 = r19.intValue()
            java.lang.Integer r15 = java.lang.Integer.valueOf(r19)
            java.lang.Object r15 = r0.get(r15)
            j.c.a.a.f.e.w0 r15 = (j.c.a.a.f.e.w0) r15
            java.lang.Integer r8 = java.lang.Integer.valueOf(r19)
            java.lang.Object r8 = r6.get(r8)
            java.util.List r8 = (java.util.List) r8
            if (r8 == 0) goto L_0x01b9
            boolean r22 = r8.isEmpty()
            if (r22 == 0) goto L_0x013a
            goto L_0x01b9
        L_0x013a:
            r22 = r6
            j.c.a.a.g.a.u8 r6 = r4.r()
            j.c.a.a.f.e.f4 r9 = r15.zzd
            java.util.List r6 = r6.a(r9, r8)
            boolean r9 = r6.isEmpty()
            if (r9 != 0) goto L_0x01b6
            j.c.a.a.f.e.x3$a r9 = r15.h()
            j.c.a.a.f.e.w0$a r9 = (j.c.a.a.f.e.w0.a) r9
            r9.n()
            r9.b(r6)
            j.c.a.a.g.a.u8 r6 = r4.r()
            r23 = r4
            j.c.a.a.f.e.f4 r4 = r15.zzc
            java.util.List r4 = r6.a(r4, r8)
            r9.a()
            r9.a(r4)
            r4 = 0
        L_0x016b:
            int r6 = r15.m()
            if (r4 >= r6) goto L_0x0187
            j.c.a.a.f.e.p0 r6 = r15.a(r4)
            int r6 = r6.zzd
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            boolean r6 = r8.contains(r6)
            if (r6 == 0) goto L_0x0184
            r9.a(r4)
        L_0x0184:
            int r4 = r4 + 1
            goto L_0x016b
        L_0x0187:
            r4 = 0
        L_0x0188:
            int r6 = r15.o()
            if (r4 >= r6) goto L_0x01a6
            j.c.a.a.f.e.x0 r6 = r15.b(r4)
            int r6 = r6.i()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            boolean r6 = r8.contains(r6)
            if (r6 == 0) goto L_0x01a3
            r9.b(r4)
        L_0x01a3:
            int r4 = r4 + 1
            goto L_0x0188
        L_0x01a6:
            java.lang.Integer r4 = java.lang.Integer.valueOf(r19)
            j.c.a.a.f.e.f5 r6 = r9.m()
            j.c.a.a.f.e.x3 r6 = (j.c.a.a.f.e.x3) r6
            j.c.a.a.f.e.w0 r6 = (j.c.a.a.f.e.w0) r6
            r5.put(r4, r6)
            goto L_0x01c4
        L_0x01b6:
            r23 = r4
            goto L_0x01c4
        L_0x01b9:
            r23 = r4
            r22 = r6
            java.lang.Integer r4 = java.lang.Integer.valueOf(r19)
            r5.put(r4, r15)
        L_0x01c4:
            r9 = r83
            r6 = r22
            r4 = r23
            r8 = 1
            goto L_0x010c
        L_0x01cd:
            r5 = r0
        L_0x01ce:
            java.util.Iterator r3 = r3.iterator()
        L_0x01d2:
            boolean r4 = r3.hasNext()
            if (r4 == 0) goto L_0x033d
            java.lang.Object r4 = r3.next()
            java.lang.Integer r4 = (java.lang.Integer) r4
            int r4 = r4.intValue()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r4)
            java.lang.Object r6 = r5.get(r6)
            j.c.a.a.f.e.w0 r6 = (j.c.a.a.f.e.w0) r6
            java.lang.Integer r8 = java.lang.Integer.valueOf(r4)
            java.lang.Object r8 = r14.get(r8)
            java.util.BitSet r8 = (java.util.BitSet) r8
            java.lang.Integer r9 = java.lang.Integer.valueOf(r4)
            java.lang.Object r9 = r11.get(r9)
            java.util.BitSet r9 = (java.util.BitSet) r9
            if (r1 == 0) goto L_0x025d
            i.e.ArrayMap r15 = new i.e.ArrayMap
            r15.<init>()
            if (r6 == 0) goto L_0x0251
            int r18 = r6.m()
            if (r18 != 0) goto L_0x0210
            goto L_0x0251
        L_0x0210:
            r18 = r3
            j.c.a.a.f.e.e4<j.c.a.a.f.e.p0> r3 = r6.zze
            java.util.Iterator r3 = r3.iterator()
        L_0x0218:
            boolean r19 = r3.hasNext()
            if (r19 == 0) goto L_0x0253
            java.lang.Object r19 = r3.next()
            r22 = r3
            r3 = r19
            j.c.a.a.f.e.p0 r3 = (j.c.a.a.f.e.p0) r3
            boolean r19 = r3.a()
            if (r19 == 0) goto L_0x024a
            r19 = r5
            int r5 = r3.zzd
            java.lang.Integer r5 = java.lang.Integer.valueOf(r5)
            boolean r23 = r3.i()
            if (r23 == 0) goto L_0x0245
            long r23 = r3.j()
            java.lang.Long r3 = java.lang.Long.valueOf(r23)
            goto L_0x0246
        L_0x0245:
            r3 = 0
        L_0x0246:
            r15.put(r5, r3)
            goto L_0x024c
        L_0x024a:
            r19 = r5
        L_0x024c:
            r5 = r19
            r3 = r22
            goto L_0x0218
        L_0x0251:
            r18 = r3
        L_0x0253:
            r19 = r5
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r12.put(r3, r15)
            goto L_0x0262
        L_0x025d:
            r18 = r3
            r19 = r5
            r15 = 0
        L_0x0262:
            if (r8 != 0) goto L_0x027c
            java.util.BitSet r8 = new java.util.BitSet
            r8.<init>()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r14.put(r3, r8)
            java.util.BitSet r9 = new java.util.BitSet
            r9.<init>()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r4)
            r11.put(r3, r9)
        L_0x027c:
            if (r6 == 0) goto L_0x02d7
            r3 = 0
        L_0x027f:
            int r5 = r6.i()
            int r5 = r5 << 6
            if (r3 >= r5) goto L_0x02d7
            j.c.a.a.f.e.f4 r5 = r6.zzc
            boolean r5 = j.c.a.a.g.a.u8.a(r5, r3)
            if (r5 == 0) goto L_0x02bc
            j.c.a.a.g.a.n3 r5 = r82.a()
            j.c.a.a.g.a.p3 r5 = r5.y()
            r22 = r11
            java.lang.Integer r11 = java.lang.Integer.valueOf(r4)
            r23 = r12
            java.lang.Integer r12 = java.lang.Integer.valueOf(r3)
            r24 = r14
            java.lang.String r14 = "Filter already evaluated. audience ID, filter ID"
            r5.a(r14, r11, r12)
            r9.set(r3)
            java.util.List r5 = r6.j()
            boolean r5 = j.c.a.a.g.a.u8.a(r5, r3)
            if (r5 == 0) goto L_0x02c2
            r8.set(r3)
            r5 = 1
            goto L_0x02c3
        L_0x02bc:
            r22 = r11
            r23 = r12
            r24 = r14
        L_0x02c2:
            r5 = 0
        L_0x02c3:
            if (r15 == 0) goto L_0x02ce
            if (r5 != 0) goto L_0x02ce
            java.lang.Integer r5 = java.lang.Integer.valueOf(r3)
            r15.remove(r5)
        L_0x02ce:
            int r3 = r3 + 1
            r11 = r22
            r12 = r23
            r14 = r24
            goto L_0x027f
        L_0x02d7:
            r22 = r11
            r23 = r12
            r24 = r14
            j.c.a.a.f.e.o0$a r3 = j.c.a.a.f.e.o0.i()
            r5 = 0
            r3.a(r5)
            if (r2 == 0) goto L_0x02f5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r4)
            java.lang.Object r5 = r0.get(r5)
            j.c.a.a.f.e.w0 r5 = (j.c.a.a.f.e.w0) r5
            r3.a(r5)
            goto L_0x02f8
        L_0x02f5:
            r3.a(r6)
        L_0x02f8:
            j.c.a.a.f.e.w0$a r5 = j.c.a.a.f.e.w0.p()
            java.util.List r6 = j.c.a.a.g.a.u8.a(r8)
            r5.b(r6)
            java.util.List r6 = j.c.a.a.g.a.u8.a(r9)
            r5.a(r6)
            if (r1 == 0) goto L_0x031f
            java.util.List r6 = a(r15)
            r5.c(r6)
            java.lang.Integer r6 = java.lang.Integer.valueOf(r4)
            i.e.ArrayMap r8 = new i.e.ArrayMap
            r8.<init>()
            r10.put(r6, r8)
        L_0x031f:
            r3.a(r5)
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            j.c.a.a.f.e.f5 r3 = r3.m()
            j.c.a.a.f.e.x3 r3 = (j.c.a.a.f.e.x3) r3
            j.c.a.a.f.e.o0 r3 = (j.c.a.a.f.e.o0) r3
            r13.put(r4, r3)
            r3 = r18
            r5 = r19
            r11 = r22
            r12 = r23
            r14 = r24
            goto L_0x01d2
        L_0x033d:
            r22 = r11
            r23 = r12
            r24 = r14
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            r9 = r83
            boolean r26 = r0.d(r9)
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.w0
            boolean r27 = r0.d(r9, r1)
            j.c.a.a.g.a.r4 r0 = r7.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r1 = j.c.a.a.g.a.k.x0
            boolean r28 = r0.d(r9, r1)
            boolean r0 = r84.isEmpty()
            java.lang.String r15 = "Filter definition"
            java.lang.String r14 = "Skipping failed audience ID"
            java.lang.String r29 = "null"
            if (r0 != 0) goto L_0x09bb
            i.e.ArrayMap r8 = new i.e.ArrayMap
            r8.<init>()
            java.util.Iterator r30 = r84.iterator()
            r31 = 0
            r0 = r31
            r2 = 0
            r3 = 0
        L_0x037c:
            boolean r4 = r30.hasNext()
            if (r4 == 0) goto L_0x09bb
            java.lang.Object r4 = r30.next()
            r6 = r4
            j.c.a.a.f.e.q0 r6 = (j.c.a.a.f.e.q0) r6
            java.lang.String r4 = r6.zze
            j.c.a.a.f.e.e4<j.c.a.a.f.e.s0> r5 = r6.zzd
            r82.r()
            java.lang.String r11 = "_eid"
            java.lang.Object r19 = j.c.a.a.g.a.u8.b(r6, r11)
            r12 = r19
            java.lang.Long r12 = (java.lang.Long) r12
            if (r12 == 0) goto L_0x039f
            r19 = 1
            goto L_0x03a1
        L_0x039f:
            r19 = 0
        L_0x03a1:
            r34 = r0
            if (r19 == 0) goto L_0x03af
            java.lang.String r0 = "_ep"
            boolean r0 = r4.equals(r0)
            if (r0 == 0) goto L_0x03af
            r0 = 1
            goto L_0x03b0
        L_0x03af:
            r0 = 0
        L_0x03b0:
            r36 = 1
            if (r0 == 0) goto L_0x04e0
            r82.r()
            java.lang.String r0 = "_en"
            java.lang.Object r0 = j.c.a.a.g.a.u8.b(r6, r0)
            r4 = r0
            java.lang.String r4 = (java.lang.String) r4
            boolean r0 = android.text.TextUtils.isEmpty(r4)
            if (r0 == 0) goto L_0x03d9
            j.c.a.a.g.a.n3 r0 = r82.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r1 = "Extra parameter without an event name. eventId"
            r0.a(r1, r12)
            r38 = r8
            r16 = r15
            r21 = 1
            goto L_0x04d8
        L_0x03d9:
            if (r3 == 0) goto L_0x03f0
            if (r2 == 0) goto L_0x03f0
            long r0 = r12.longValue()
            long r38 = r2.longValue()
            int r19 = (r0 > r38 ? 1 : (r0 == r38 ? 0 : -1))
            if (r19 == 0) goto L_0x03ea
            goto L_0x03f0
        L_0x03ea:
            r11 = r2
            r19 = r3
            r0 = r34
            goto L_0x0417
        L_0x03f0:
            j.c.a.a.g.a.n9 r0 = r82.s()
            android.util.Pair r0 = r0.a(r9, r12)
            if (r0 == 0) goto L_0x04c6
            java.lang.Object r1 = r0.first
            if (r1 != 0) goto L_0x0400
            goto L_0x04c6
        L_0x0400:
            r3 = r1
            j.c.a.a.f.e.q0 r3 = (j.c.a.a.f.e.q0) r3
            java.lang.Object r0 = r0.second
            java.lang.Long r0 = (java.lang.Long) r0
            long r0 = r0.longValue()
            r82.r()
            java.lang.Object r2 = j.c.a.a.g.a.u8.b(r3, r11)
            java.lang.Long r2 = (java.lang.Long) r2
            r11 = r2
            r19 = r3
        L_0x0417:
            long r34 = r0 - r36
            int r0 = (r34 > r31 ? 1 : (r34 == r31 ? 0 : -1))
            if (r0 > 0) goto L_0x0462
            j.c.a.a.g.a.n9 r1 = r82.s()
            r1.d()
            j.c.a.a.g.a.n3 r0 = r1.a()
            j.c.a.a.g.a.p3 r0 = r0.y()
            java.lang.String r2 = "Clearing complex main event info. appId"
            r0.a(r2, r9)
            android.database.sqlite.SQLiteDatabase r0 = r1.v()     // Catch:{ SQLiteException -> 0x0448 }
            java.lang.String r2 = "delete from main_event_params where app_id=?"
            r3 = 1
            java.lang.String[] r12 = new java.lang.String[r3]     // Catch:{ SQLiteException -> 0x0444 }
            r16 = 0
            r12[r16] = r9     // Catch:{ SQLiteException -> 0x0442 }
            r0.execSQL(r2, r12)     // Catch:{ SQLiteException -> 0x0442 }
            goto L_0x0458
        L_0x0442:
            r0 = move-exception
            goto L_0x044b
        L_0x0444:
            r0 = move-exception
        L_0x0445:
            r16 = 0
            goto L_0x044b
        L_0x0448:
            r0 = move-exception
            r3 = 1
            goto L_0x0445
        L_0x044b:
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.u()
            java.lang.String r2 = "Error clearing complex main event"
            r1.a(r2, r0)
        L_0x0458:
            r12 = r5
            r38 = r8
            r16 = r15
            r21 = 1
            r8 = r4
            r15 = r6
            goto L_0x047c
        L_0x0462:
            r3 = 1
            r16 = 0
            j.c.a.a.g.a.n9 r1 = r82.s()
            r2 = r83
            r21 = 1
            r3 = r12
            r12 = r5
            r38 = r8
            r8 = r4
            r4 = r34
            r16 = r15
            r15 = r6
            r6 = r19
            r1.a(r2, r3, r4, r6)
        L_0x047c:
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.List r0 = r19.a()
            java.util.Iterator r0 = r0.iterator()
        L_0x0489:
            boolean r1 = r0.hasNext()
            if (r1 == 0) goto L_0x04a6
            java.lang.Object r1 = r0.next()
            j.c.a.a.f.e.s0 r1 = (j.c.a.a.f.e.s0) r1
            r82.r()
            java.lang.String r2 = r1.a()
            j.c.a.a.f.e.s0 r2 = j.c.a.a.g.a.u8.a(r15, r2)
            if (r2 != 0) goto L_0x0489
            r5.add(r1)
            goto L_0x0489
        L_0x04a6:
            boolean r0 = r5.isEmpty()
            if (r0 != 0) goto L_0x04b0
            r5.addAll(r12)
            goto L_0x04be
        L_0x04b0:
            j.c.a.a.g.a.n3 r0 = r82.a()
            j.c.a.a.g.a.p3 r0 = r0.v()
            java.lang.String r1 = "No unique parameters in main event. eventName"
            r0.a(r1, r8)
            r5 = r12
        L_0x04be:
            r40 = r5
            r5 = r8
            r0 = r11
            r39 = r19
            goto L_0x0530
        L_0x04c6:
            r38 = r8
            r16 = r15
            r21 = 1
            r8 = r4
            j.c.a.a.g.a.n3 r0 = r82.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r1 = "Extra parameter without existing main event. eventName, eventId"
            r0.a(r1, r8, r12)
        L_0x04d8:
            r15 = r16
            r0 = r34
            r8 = r38
            goto L_0x037c
        L_0x04e0:
            r38 = r8
            r16 = r15
            r21 = 1
            r8 = r5
            r15 = r6
            if (r19 == 0) goto L_0x0529
            r82.r()
            java.lang.Long r0 = java.lang.Long.valueOf(r31)
            java.lang.String r1 = "_epc"
            java.lang.Object r1 = j.c.a.a.g.a.u8.b(r15, r1)
            if (r1 != 0) goto L_0x04fa
            goto L_0x04fb
        L_0x04fa:
            r0 = r1
        L_0x04fb:
            java.lang.Long r0 = (java.lang.Long) r0
            long r34 = r0.longValue()
            int r0 = (r34 > r31 ? 1 : (r34 == r31 ? 0 : -1))
            if (r0 > 0) goto L_0x0514
            j.c.a.a.g.a.n3 r0 = r82.a()
            j.c.a.a.g.a.p3 r0 = r0.v()
            java.lang.String r1 = "Complex event with zero extra param count. eventName"
            r0.a(r1, r4)
            r0 = r4
            goto L_0x0522
        L_0x0514:
            j.c.a.a.g.a.n9 r1 = r82.s()
            r2 = r83
            r3 = r12
            r0 = r4
            r4 = r34
            r6 = r15
            r1.a(r2, r3, r4, r6)
        L_0x0522:
            r5 = r0
            r40 = r8
            r0 = r12
            r39 = r15
            goto L_0x0530
        L_0x0529:
            r0 = r4
            r5 = r0
            r0 = r2
            r39 = r3
            r40 = r8
        L_0x0530:
            j.c.a.a.g.a.r4 r1 = r7.a
            j.c.a.a.g.a.i9 r1 = r1.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.x0
            boolean r1 = r1.d(r9, r2)
            j.c.a.a.g.a.n9 r2 = r82.s()
            java.lang.String r3 = r15.zze
            j.c.a.a.g.a.e r2 = r2.a(r9, r3)
            if (r2 != 0) goto L_0x05c4
            j.c.a.a.g.a.n3 r2 = r82.a()
            j.c.a.a.g.a.p3 r2 = r2.f2047i
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r83)
            j.c.a.a.g.a.l3 r4 = r82.g()
            java.lang.String r4 = r4.a(r5)
            java.lang.String r6 = "Event aggregate wasn't created during raw event logging. appId, event"
            r2.a(r6, r3, r4)
            if (r1 == 0) goto L_0x0598
            j.c.a.a.g.a.e r1 = new j.c.a.a.g.a.e
            r4 = r38
            r6 = 1
            r8 = r1
            java.lang.String r2 = r15.i()
            r3 = r10
            r10 = r2
            r11 = 1
            r2 = r22
            r6 = r23
            r18 = 1
            r43 = r13
            r45 = r14
            r44 = r24
            r13 = r18
            r38 = r15
            r47 = r16
            r46 = r17
            r15 = r18
            long r17 = r38.m()
            r19 = 0
            r21 = 0
            r22 = 0
            r23 = 0
            r24 = 0
            r7 = r9
            r9 = r83
            r8.<init>(r9, r10, r11, r13, r15, r17, r19, r21, r22, r23, r24)
            goto L_0x05c0
        L_0x0598:
            r7 = r9
            r3 = r10
            r43 = r13
            r45 = r14
            r47 = r16
            r46 = r17
            r2 = r22
            r6 = r23
            r44 = r24
            r4 = r38
            r38 = r15
            j.c.a.a.g.a.e r1 = new j.c.a.a.g.a.e
            java.lang.String r10 = r38.i()
            r11 = 1
            r13 = 1
            long r15 = r38.m()
            r8 = r1
            r9 = r83
            r8.<init>(r9, r10, r11, r13, r15)
        L_0x05c0:
            r9 = r1
            r8 = r2
            goto L_0x0642
        L_0x05c4:
            r7 = r9
            r3 = r10
            r43 = r13
            r45 = r14
            r47 = r16
            r46 = r17
            r8 = r22
            r6 = r23
            r44 = r24
            r4 = r38
            r38 = r15
            if (r1 == 0) goto L_0x060e
            j.c.a.a.g.a.e r1 = new j.c.a.a.g.a.e
            r48 = r1
            java.lang.String r9 = r2.a
            r49 = r9
            java.lang.String r9 = r2.b
            r50 = r9
            long r9 = r2.c
            long r51 = r9 + r36
            long r9 = r2.d
            long r53 = r9 + r36
            long r9 = r2.f1965e
            long r55 = r9 + r36
            long r9 = r2.f1966f
            r57 = r9
            long r9 = r2.g
            r59 = r9
            java.lang.Long r9 = r2.h
            r61 = r9
            java.lang.Long r9 = r2.f1967i
            r62 = r9
            java.lang.Long r9 = r2.f1968j
            r63 = r9
            java.lang.Boolean r2 = r2.f1969k
            r64 = r2
            r48.<init>(r49, r50, r51, r53, r55, r57, r59, r61, r62, r63, r64)
            goto L_0x0641
        L_0x060e:
            j.c.a.a.g.a.e r1 = new j.c.a.a.g.a.e
            r64 = r1
            java.lang.String r9 = r2.a
            r65 = r9
            java.lang.String r9 = r2.b
            r66 = r9
            long r9 = r2.c
            long r67 = r9 + r36
            long r9 = r2.d
            long r69 = r9 + r36
            long r9 = r2.f1965e
            r71 = r9
            long r9 = r2.f1966f
            r73 = r9
            long r9 = r2.g
            r75 = r9
            java.lang.Long r9 = r2.h
            r77 = r9
            java.lang.Long r9 = r2.f1967i
            r78 = r9
            java.lang.Long r9 = r2.f1968j
            r79 = r9
            java.lang.Boolean r2 = r2.f1969k
            r80 = r2
            r64.<init>(r65, r66, r67, r69, r71, r73, r75, r77, r78, r79, r80)
        L_0x0641:
            r9 = r1
        L_0x0642:
            j.c.a.a.g.a.n9 r1 = r82.s()
            r1.a(r9)
            long r10 = r9.c
            java.lang.Object r1 = r4.get(r5)
            java.util.Map r1 = (java.util.Map) r1
            if (r1 != 0) goto L_0x0665
            j.c.a.a.g.a.n9 r1 = r82.s()
            java.util.Map r1 = r1.f(r7, r5)
            if (r1 != 0) goto L_0x0662
            i.e.ArrayMap r1 = new i.e.ArrayMap
            r1.<init>()
        L_0x0662:
            r4.put(r5, r1)
        L_0x0665:
            r12 = r1
            java.util.Set r1 = r12.keySet()
            java.util.Iterator r13 = r1.iterator()
        L_0x066e:
            boolean r1 = r13.hasNext()
            if (r1 == 0) goto L_0x099d
            java.lang.Object r1 = r13.next()
            java.lang.Integer r1 = (java.lang.Integer) r1
            int r14 = r1.intValue()
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r15 = r46
            boolean r1 = r15.contains(r1)
            if (r1 == 0) goto L_0x069e
            j.c.a.a.g.a.n3 r1 = r82.a()
            j.c.a.a.g.a.p3 r1 = r1.y()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r14)
            r14 = r45
            r1.a(r14, r2)
            r46 = r15
            goto L_0x066e
        L_0x069e:
            r2 = r45
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r7 = r44
            java.lang.Object r1 = r7.get(r1)
            java.util.BitSet r1 = (java.util.BitSet) r1
            r84 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            java.lang.Object r0 = r8.get(r0)
            java.util.BitSet r0 = (java.util.BitSet) r0
            if (r26 == 0) goto L_0x06d5
            r16 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            java.lang.Object r0 = r6.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            r17 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            java.lang.Object r0 = r3.get(r0)
            java.util.Map r0 = (java.util.Map) r0
            r18 = r0
            goto L_0x06db
        L_0x06d5:
            r16 = r0
            r17 = 0
            r18 = 0
        L_0x06db:
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            r19 = r10
            r10 = r43
            java.lang.Object r0 = r10.get(r0)
            j.c.a.a.f.e.o0 r0 = (j.c.a.a.f.e.o0) r0
            if (r0 != 0) goto L_0x074d
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            j.c.a.a.f.e.o0$a r1 = j.c.a.a.f.e.o0.i()
            r11 = 1
            r1.a(r11)
            j.c.a.a.f.e.f5 r1 = r1.m()
            j.c.a.a.f.e.x3 r1 = (j.c.a.a.f.e.x3) r1
            j.c.a.a.f.e.o0 r1 = (j.c.a.a.f.e.o0) r1
            r10.put(r0, r1)
            java.util.BitSet r1 = new java.util.BitSet
            r1.<init>()
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            r7.put(r0, r1)
            java.util.BitSet r0 = new java.util.BitSet
            r0.<init>()
            java.lang.Integer r11 = java.lang.Integer.valueOf(r14)
            r8.put(r11, r0)
            if (r26 == 0) goto L_0x0746
            i.e.ArrayMap r11 = new i.e.ArrayMap
            r11.<init>()
            r16 = r0
            java.lang.Integer r0 = java.lang.Integer.valueOf(r14)
            r6.put(r0, r11)
            i.e.ArrayMap r0 = new i.e.ArrayMap
            r0.<init>()
            r22 = r1
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r3.put(r1, r0)
            r45 = r2
            r1 = r22
            r81 = r11
            r11 = r0
            r0 = r16
            r16 = r13
            r13 = r81
            goto L_0x0757
        L_0x0746:
            r16 = r0
            r22 = r1
            r45 = r2
            goto L_0x0751
        L_0x074d:
            r45 = r2
            r0 = r16
        L_0x0751:
            r11 = r18
            r16 = r13
            r13 = r17
        L_0x0757:
            java.lang.Integer r2 = java.lang.Integer.valueOf(r14)
            java.lang.Object r2 = r12.get(r2)
            java.util.List r2 = (java.util.List) r2
            java.util.Iterator r17 = r2.iterator()
        L_0x0765:
            boolean r2 = r17.hasNext()
            if (r2 == 0) goto L_0x098b
            java.lang.Object r2 = r17.next()
            j.c.a.a.f.e.a0 r2 = (j.c.a.a.f.e.a0) r2
            if (r28 == 0) goto L_0x0784
            if (r27 == 0) goto L_0x0784
            boolean r18 = r2.o()
            if (r18 == 0) goto L_0x0784
            r22 = r3
            r18 = r4
            long r3 = r9.f1965e
            r23 = r3
            goto L_0x078a
        L_0x0784:
            r22 = r3
            r18 = r4
            r23 = r19
        L_0x078a:
            j.c.a.a.g.a.n3 r3 = r82.a()
            r4 = 2
            boolean r3 = r3.a(r4)
            if (r3 == 0) goto L_0x07e6
            j.c.a.a.g.a.n3 r3 = r82.a()
            j.c.a.a.g.a.p3 r3 = r3.y()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r14)
            boolean r33 = r2.a()
            if (r33 == 0) goto L_0x07b8
            int r33 = r2.i()
            java.lang.Integer r33 = java.lang.Integer.valueOf(r33)
            r36 = r9
            r81 = r33
            r33 = r6
            r6 = r81
            goto L_0x07bd
        L_0x07b8:
            r33 = r6
            r36 = r9
            r6 = 0
        L_0x07bd:
            j.c.a.a.g.a.l3 r9 = r82.g()
            r37 = r12
            java.lang.String r12 = r2.j()
            java.lang.String r9 = r9.a(r12)
            java.lang.String r12 = "Evaluating filter. audience, filter, event"
            r3.a(r12, r4, r6, r9)
            j.c.a.a.g.a.n3 r3 = r82.a()
            j.c.a.a.g.a.p3 r3 = r3.y()
            j.c.a.a.g.a.u8 r4 = r82.r()
            java.lang.String r4 = r4.a(r2)
            r9 = r47
            r3.a(r9, r4)
            goto L_0x07ee
        L_0x07e6:
            r33 = r6
            r36 = r9
            r37 = r12
            r9 = r47
        L_0x07ee:
            boolean r3 = r2.a()
            if (r3 == 0) goto L_0x0946
            int r3 = r2.i()
            r12 = 256(0x100, float:3.59E-43)
            if (r3 <= r12) goto L_0x07fe
            goto L_0x0946
        L_0x07fe:
            java.lang.String r6 = "Event filter result"
            if (r26 == 0) goto L_0x08cd
            boolean r3 = r2.m()
            boolean r41 = r2.n()
            if (r27 == 0) goto L_0x0814
            boolean r4 = r2.o()
            if (r4 == 0) goto L_0x0814
            r4 = 1
            goto L_0x0815
        L_0x0814:
            r4 = 0
        L_0x0815:
            if (r3 != 0) goto L_0x081f
            if (r41 != 0) goto L_0x081f
            if (r4 == 0) goto L_0x081c
            goto L_0x081f
        L_0x081c:
            r43 = 0
            goto L_0x0821
        L_0x081f:
            r43 = 1
        L_0x0821:
            int r3 = r2.i()
            boolean r3 = r1.get(r3)
            if (r3 == 0) goto L_0x085c
            if (r43 != 0) goto L_0x085c
            j.c.a.a.g.a.n3 r3 = r82.a()
            j.c.a.a.g.a.p3 r3 = r3.y()
            java.lang.Integer r4 = java.lang.Integer.valueOf(r14)
            boolean r6 = r2.a()
            if (r6 == 0) goto L_0x0848
            int r2 = r2.i()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            goto L_0x0849
        L_0x0848:
            r2 = 0
        L_0x0849:
            java.lang.String r6 = "Event filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            r3.a(r6, r4, r2)
            r47 = r9
            r4 = r18
            r3 = r22
            r6 = r33
            r9 = r36
            r12 = r37
            goto L_0x0765
        L_0x085c:
            r4 = r1
            r1 = r82
            r44 = r2
            r3 = r45
            r47 = r9
            r12 = r22
            r9 = r3
            r3 = r5
            r22 = r10
            r10 = r4
            r4 = r40
            r21 = r5
            r42 = r12
            r12 = r6
            r5 = r23
            java.lang.Boolean r1 = r1.a(r2, r3, r4, r5)
            j.c.a.a.g.a.n3 r2 = r82.a()
            j.c.a.a.g.a.p3 r2 = r2.y()
            if (r1 != 0) goto L_0x0886
            r3 = r29
            goto L_0x0887
        L_0x0886:
            r3 = r1
        L_0x0887:
            r2.a(r12, r3)
            if (r1 != 0) goto L_0x0895
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r15.add(r1)
            goto L_0x0978
        L_0x0895:
            int r2 = r44.i()
            r0.set(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0978
            int r1 = r44.i()
            r10.set(r1)
            if (r43 == 0) goto L_0x0978
            boolean r1 = r38.j()
            if (r1 == 0) goto L_0x0978
            if (r41 == 0) goto L_0x08c0
            int r1 = r44.i()
            long r2 = r38.m()
            b(r11, r1, r2)
            goto L_0x0978
        L_0x08c0:
            int r1 = r44.i()
            long r2 = r38.m()
            a(r13, r1, r2)
            goto L_0x0978
        L_0x08cd:
            r44 = r2
            r21 = r5
            r12 = r6
            r47 = r9
            r42 = r22
            r9 = r45
            r22 = r10
            r10 = r1
            int r1 = r44.i()
            boolean r1 = r10.get(r1)
            if (r1 == 0) goto L_0x0908
            j.c.a.a.g.a.n3 r1 = r82.a()
            j.c.a.a.g.a.p3 r1 = r1.y()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r14)
            boolean r3 = r44.a()
            if (r3 == 0) goto L_0x0900
            int r3 = r44.i()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            goto L_0x0901
        L_0x0900:
            r3 = 0
        L_0x0901:
            java.lang.String r4 = "Event filter already evaluated true. audience ID, filter ID"
            r1.a(r4, r2, r3)
            goto L_0x0978
        L_0x0908:
            r1 = r82
            r2 = r44
            r3 = r21
            r4 = r40
            r5 = r23
            java.lang.Boolean r1 = r1.a(r2, r3, r4, r5)
            j.c.a.a.g.a.n3 r2 = r82.a()
            j.c.a.a.g.a.p3 r2 = r2.y()
            if (r1 != 0) goto L_0x0923
            r3 = r29
            goto L_0x0924
        L_0x0923:
            r3 = r1
        L_0x0924:
            r2.a(r12, r3)
            if (r1 != 0) goto L_0x0931
            java.lang.Integer r1 = java.lang.Integer.valueOf(r14)
            r15.add(r1)
            goto L_0x0978
        L_0x0931:
            int r2 = r44.i()
            r0.set(r2)
            boolean r1 = r1.booleanValue()
            if (r1 == 0) goto L_0x0978
            int r1 = r44.i()
            r10.set(r1)
            goto L_0x0978
        L_0x0946:
            r44 = r2
            r21 = r5
            r47 = r9
            r42 = r22
            r9 = r45
            r22 = r10
            r10 = r1
            j.c.a.a.g.a.n3 r1 = r82.a()
            j.c.a.a.g.a.p3 r1 = r1.v()
            java.lang.Object r2 = j.c.a.a.g.a.n3.a(r83)
            boolean r3 = r44.a()
            if (r3 == 0) goto L_0x096e
            int r3 = r44.i()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
            goto L_0x096f
        L_0x096e:
            r3 = 0
        L_0x096f:
            java.lang.String r3 = java.lang.String.valueOf(r3)
            java.lang.String r4 = "Invalid event filter ID. appId, id"
            r1.a(r4, r2, r3)
        L_0x0978:
            r45 = r9
            r1 = r10
            r4 = r18
            r5 = r21
            r10 = r22
            r6 = r33
            r9 = r36
            r12 = r37
            r3 = r42
            goto L_0x0765
        L_0x098b:
            r36 = r9
            r0 = r84
            r44 = r7
            r43 = r10
            r46 = r15
            r13 = r16
            r10 = r19
            r7 = r83
            goto L_0x066e
        L_0x099d:
            r84 = r0
            r7 = r82
            r9 = r83
            r2 = r84
            r10 = r3
            r23 = r6
            r22 = r8
            r0 = r34
            r3 = r39
            r13 = r43
            r24 = r44
            r14 = r45
            r17 = r46
            r15 = r47
            r8 = r4
            goto L_0x037c
        L_0x09bb:
            r42 = r10
            r9 = r14
            r47 = r15
            r15 = r17
            r8 = r22
            r33 = r23
            r7 = r24
            r22 = r13
            r1 = r82
            r2 = r83
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.i9 r0 = r0.g
            boolean r0 = r0.d(r2)
            j.c.a.a.g.a.r4 r3 = r1.a
            j.c.a.a.g.a.i9 r3 = r3.g
            if (r3 == 0) goto L_0x10da
            j.c.a.a.g.a.b3<java.lang.Boolean> r4 = j.c.a.a.g.a.k.p0
            boolean r3 = r3.d(r2, r4)
            j.c.a.a.g.a.r4 r4 = r1.a
            j.c.a.a.g.a.i9 r4 = r4.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r5 = j.c.a.a.g.a.k.w0
            boolean r4 = r4.d(r2, r5)
            j.c.a.a.g.a.r4 r5 = r1.a
            j.c.a.a.g.a.i9 r5 = r5.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r6 = j.c.a.a.g.a.k.A0
            boolean r5 = r5.d(r2, r6)
            java.util.ArrayList r6 = new java.util.ArrayList
            r6.<init>()
            boolean r10 = r85.isEmpty()
            if (r10 != 0) goto L_0x0d81
            i.e.ArrayMap r10 = new i.e.ArrayMap
            r10.<init>()
            java.util.Iterator r11 = r85.iterator()
        L_0x0a0a:
            boolean r12 = r11.hasNext()
            if (r12 == 0) goto L_0x0d81
            java.lang.Object r12 = r11.next()
            j.c.a.a.f.e.y0 r12 = (j.c.a.a.f.e.y0) r12
            java.lang.String r13 = r12.zze
            r6.add(r13)
            java.lang.String r13 = r12.zze
            java.lang.Object r13 = r10.get(r13)
            java.util.Map r13 = (java.util.Map) r13
            if (r13 != 0) goto L_0x0a3b
            j.c.a.a.g.a.n9 r13 = r82.s()
            java.lang.String r14 = r12.zze
            java.util.Map r13 = r13.g(r2, r14)
            if (r13 != 0) goto L_0x0a36
            i.e.ArrayMap r13 = new i.e.ArrayMap
            r13.<init>()
        L_0x0a36:
            java.lang.String r14 = r12.zze
            r10.put(r14, r13)
        L_0x0a3b:
            java.util.Set r14 = r13.keySet()
            java.util.Iterator r14 = r14.iterator()
        L_0x0a43:
            boolean r16 = r14.hasNext()
            if (r16 == 0) goto L_0x0d7b
            java.lang.Object r16 = r14.next()
            java.lang.Integer r16 = (java.lang.Integer) r16
            int r16 = r16.intValue()
            r84 = r10
            java.lang.Integer r10 = java.lang.Integer.valueOf(r16)
            boolean r10 = r15.contains(r10)
            if (r10 == 0) goto L_0x0a73
            j.c.a.a.g.a.n3 r10 = r82.a()
            j.c.a.a.g.a.p3 r10 = r10.f2052n
            r85 = r11
            java.lang.Integer r11 = java.lang.Integer.valueOf(r16)
            r10.a(r9, r11)
            r10 = r84
            r11 = r85
            goto L_0x0a43
        L_0x0a73:
            r85 = r11
            java.lang.Integer r10 = java.lang.Integer.valueOf(r16)
            java.lang.Object r10 = r7.get(r10)
            java.util.BitSet r10 = (java.util.BitSet) r10
            java.lang.Integer r11 = java.lang.Integer.valueOf(r16)
            java.lang.Object r11 = r8.get(r11)
            java.util.BitSet r11 = (java.util.BitSet) r11
            if (r0 == 0) goto L_0x0aae
            r17 = r9
            java.lang.Integer r9 = java.lang.Integer.valueOf(r16)
            r18 = r10
            r10 = r33
            java.lang.Object r9 = r10.get(r9)
            java.util.Map r9 = (java.util.Map) r9
            r19 = r9
            java.lang.Integer r9 = java.lang.Integer.valueOf(r16)
            r20 = r11
            r11 = r42
            java.lang.Object r9 = r11.get(r9)
            java.util.Map r9 = (java.util.Map) r9
            r21 = r9
            goto L_0x0abc
        L_0x0aae:
            r17 = r9
            r18 = r10
            r20 = r11
            r10 = r33
            r11 = r42
            r19 = 0
            r21 = 0
        L_0x0abc:
            java.lang.Integer r9 = java.lang.Integer.valueOf(r16)
            r23 = r14
            r14 = r22
            java.lang.Object r9 = r14.get(r9)
            j.c.a.a.f.e.o0 r9 = (j.c.a.a.f.e.o0) r9
            if (r9 != 0) goto L_0x0b2a
            java.lang.Integer r9 = java.lang.Integer.valueOf(r16)
            r22 = r6
            j.c.a.a.f.e.o0$a r6 = j.c.a.a.f.e.o0.i()
            r2 = 1
            r6.a(r2)
            j.c.a.a.f.e.f5 r6 = r6.m()
            j.c.a.a.f.e.x3 r6 = (j.c.a.a.f.e.x3) r6
            j.c.a.a.f.e.o0 r6 = (j.c.a.a.f.e.o0) r6
            r14.put(r9, r6)
            java.util.BitSet r6 = new java.util.BitSet
            r6.<init>()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r16)
            r7.put(r9, r6)
            java.util.BitSet r9 = new java.util.BitSet
            r9.<init>()
            java.lang.Integer r2 = java.lang.Integer.valueOf(r16)
            r8.put(r2, r9)
            if (r0 == 0) goto L_0x0b25
            i.e.ArrayMap r2 = new i.e.ArrayMap
            r2.<init>()
            r18 = r6
            java.lang.Integer r6 = java.lang.Integer.valueOf(r16)
            r10.put(r6, r2)
            i.e.ArrayMap r6 = new i.e.ArrayMap
            r6.<init>()
            r19 = r2
            java.lang.Integer r2 = java.lang.Integer.valueOf(r16)
            r11.put(r2, r6)
            r2 = r6
            r33 = r10
            r42 = r11
            r6 = r18
            r11 = r19
            goto L_0x0b38
        L_0x0b25:
            r18 = r6
            r20 = r9
            goto L_0x0b2c
        L_0x0b2a:
            r22 = r6
        L_0x0b2c:
            r33 = r10
            r42 = r11
            r6 = r18
            r11 = r19
            r9 = r20
            r2 = r21
        L_0x0b38:
            java.lang.Integer r10 = java.lang.Integer.valueOf(r16)
            java.lang.Object r10 = r13.get(r10)
            java.util.List r10 = (java.util.List) r10
            java.util.Iterator r10 = r10.iterator()
        L_0x0b46:
            boolean r18 = r10.hasNext()
            if (r18 == 0) goto L_0x0d6b
            java.lang.Object r18 = r10.next()
            r19 = r10
            r10 = r18
            j.c.a.a.f.e.d0 r10 = (j.c.a.a.f.e.d0) r10
            r18 = r13
            j.c.a.a.g.a.n3 r13 = r82.a()
            r20 = r8
            r8 = 2
            boolean r13 = r13.a(r8)
            if (r13 == 0) goto L_0x0bb4
            j.c.a.a.g.a.n3 r13 = r82.a()
            j.c.a.a.g.a.p3 r13 = r13.y()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r16)
            boolean r21 = r10.a()
            if (r21 == 0) goto L_0x0b86
            int r21 = r10.i()
            java.lang.Integer r21 = java.lang.Integer.valueOf(r21)
            r44 = r7
            r43 = r14
            r14 = r21
            goto L_0x0b8b
        L_0x0b86:
            r44 = r7
            r43 = r14
            r14 = 0
        L_0x0b8b:
            j.c.a.a.g.a.l3 r7 = r82.g()
            r21 = r11
            java.lang.String r11 = r10.j()
            java.lang.String r7 = r7.c(r11)
            java.lang.String r11 = "Evaluating filter. audience, filter, property"
            r13.a(r11, r8, r14, r7)
            j.c.a.a.g.a.n3 r7 = r82.a()
            j.c.a.a.g.a.p3 r7 = r7.y()
            j.c.a.a.g.a.u8 r8 = r82.r()
            java.lang.String r8 = r8.a(r10)
            r11 = r47
            r7.a(r11, r8)
            goto L_0x0bbc
        L_0x0bb4:
            r44 = r7
            r21 = r11
            r43 = r14
            r11 = r47
        L_0x0bbc:
            boolean r7 = r10.a()
            if (r7 == 0) goto L_0x0d23
            int r7 = r10.i()
            r8 = 256(0x100, float:3.59E-43)
            if (r7 <= r8) goto L_0x0bcc
            goto L_0x0d23
        L_0x0bcc:
            java.lang.String r7 = "Property filter result"
            if (r0 == 0) goto L_0x0cb0
            boolean r13 = r10.m()
            boolean r14 = r10.n()
            if (r4 == 0) goto L_0x0be3
            boolean r24 = r10.o()
            if (r24 == 0) goto L_0x0be3
            r24 = 1
            goto L_0x0be5
        L_0x0be3:
            r24 = 0
        L_0x0be5:
            if (r13 != 0) goto L_0x0bee
            if (r14 != 0) goto L_0x0bee
            if (r24 == 0) goto L_0x0bec
            goto L_0x0bee
        L_0x0bec:
            r13 = 0
            goto L_0x0bef
        L_0x0bee:
            r13 = 1
        L_0x0bef:
            int r8 = r10.i()
            boolean r8 = r6.get(r8)
            if (r8 == 0) goto L_0x0c24
            if (r13 != 0) goto L_0x0c24
            j.c.a.a.g.a.n3 r7 = r82.a()
            j.c.a.a.g.a.p3 r7 = r7.y()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r16)
            boolean r13 = r10.a()
            if (r13 == 0) goto L_0x0c16
            int r10 = r10.i()
            java.lang.Integer r10 = java.lang.Integer.valueOf(r10)
            goto L_0x0c17
        L_0x0c16:
            r10 = 0
        L_0x0c17:
            java.lang.String r13 = "Property filter already evaluated true and it is not associated with an enhanced audience. audience ID, filter ID"
            r7.a(r13, r8, r10)
            r27 = r0
            r47 = r11
        L_0x0c20:
            r11 = r21
            goto L_0x0d15
        L_0x0c24:
            java.lang.Boolean r8 = r1.a(r10, r12)
            j.c.a.a.g.a.n3 r26 = r82.a()
            r27 = r0
            j.c.a.a.g.a.p3 r0 = r26.y()
            r47 = r11
            if (r8 != 0) goto L_0x0c39
            r11 = r29
            goto L_0x0c3a
        L_0x0c39:
            r11 = r8
        L_0x0c3a:
            r0.a(r7, r11)
            if (r8 != 0) goto L_0x0c47
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            r15.add(r0)
            goto L_0x0c20
        L_0x0c47:
            int r0 = r10.i()
            r9.set(r0)
            if (r4 == 0) goto L_0x0c58
            if (r24 == 0) goto L_0x0c58
            boolean r0 = r8.booleanValue()
            if (r0 == 0) goto L_0x0c20
        L_0x0c58:
            if (r3 == 0) goto L_0x0c76
            int r0 = r10.i()
            boolean r0 = r6.get(r0)
            if (r0 == 0) goto L_0x0c6a
            boolean r0 = r10.m()
            if (r0 == 0) goto L_0x0c81
        L_0x0c6a:
            int r0 = r10.i()
            boolean r7 = r8.booleanValue()
            r6.set(r0, r7)
            goto L_0x0c81
        L_0x0c76:
            int r0 = r10.i()
            boolean r7 = r8.booleanValue()
            r6.set(r0, r7)
        L_0x0c81:
            boolean r0 = r8.booleanValue()
            if (r0 == 0) goto L_0x0c20
            if (r13 == 0) goto L_0x0c20
            boolean r0 = r12.a()
            if (r0 == 0) goto L_0x0c20
            long r7 = r12.i()
            if (r5 == 0) goto L_0x0c9b
            if (r86 == 0) goto L_0x0c9b
            long r7 = r86.longValue()
        L_0x0c9b:
            if (r14 == 0) goto L_0x0ca6
            int r0 = r10.i()
            b(r2, r0, r7)
            goto L_0x0c20
        L_0x0ca6:
            int r0 = r10.i()
            r11 = r21
            a(r11, r0, r7)
            goto L_0x0d15
        L_0x0cb0:
            r27 = r0
            r47 = r11
            r11 = r21
            int r0 = r10.i()
            boolean r0 = r6.get(r0)
            if (r0 == 0) goto L_0x0ce2
            j.c.a.a.g.a.n3 r0 = r82.a()
            j.c.a.a.g.a.p3 r0 = r0.y()
            java.lang.Integer r7 = java.lang.Integer.valueOf(r16)
            boolean r8 = r10.a()
            if (r8 == 0) goto L_0x0cdb
            int r8 = r10.i()
            java.lang.Integer r8 = java.lang.Integer.valueOf(r8)
            goto L_0x0cdc
        L_0x0cdb:
            r8 = 0
        L_0x0cdc:
            java.lang.String r10 = "Property filter already evaluated true. audience ID, filter ID"
            r0.a(r10, r7, r8)
            goto L_0x0d15
        L_0x0ce2:
            java.lang.Boolean r0 = r1.a(r10, r12)
            j.c.a.a.g.a.n3 r8 = r82.a()
            j.c.a.a.g.a.p3 r8 = r8.y()
            if (r0 != 0) goto L_0x0cf3
            r13 = r29
            goto L_0x0cf4
        L_0x0cf3:
            r13 = r0
        L_0x0cf4:
            r8.a(r7, r13)
            if (r0 != 0) goto L_0x0d01
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            r15.add(r0)
            goto L_0x0d15
        L_0x0d01:
            int r7 = r10.i()
            r9.set(r7)
            boolean r0 = r0.booleanValue()
            if (r0 == 0) goto L_0x0d15
            int r0 = r10.i()
            r6.set(r0)
        L_0x0d15:
            r13 = r18
            r10 = r19
            r8 = r20
            r0 = r27
            r14 = r43
            r7 = r44
            goto L_0x0b46
        L_0x0d23:
            r27 = r0
            r47 = r11
            j.c.a.a.g.a.n3 r0 = r82.a()
            j.c.a.a.g.a.p3 r0 = r0.v()
            java.lang.Object r2 = j.c.a.a.g.a.n3.a(r83)
            boolean r6 = r10.a()
            if (r6 == 0) goto L_0x0d42
            int r6 = r10.i()
            java.lang.Integer r6 = java.lang.Integer.valueOf(r6)
            goto L_0x0d43
        L_0x0d42:
            r6 = 0
        L_0x0d43:
            java.lang.String r6 = java.lang.String.valueOf(r6)
            java.lang.String r7 = "Invalid property filter ID. appId, id"
            r0.a(r7, r2, r6)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r16)
            r15.add(r0)
            r2 = r83
            r10 = r84
            r11 = r85
            r9 = r17
            r13 = r18
            r8 = r20
            r6 = r22
            r14 = r23
            r0 = r27
            r22 = r43
            r7 = r44
            goto L_0x0a43
        L_0x0d6b:
            r2 = r83
            r10 = r84
            r11 = r85
            r9 = r17
            r6 = r22
            r22 = r14
            r14 = r23
            goto L_0x0a43
        L_0x0d7b:
            r43 = r22
            r2 = r83
            goto L_0x0a0a
        L_0x0d81:
            r44 = r7
            r20 = r8
            r43 = r22
            r22 = r6
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.i9 r0 = r0.g
            r2 = r83
            r3 = 1
            boolean r4 = r0.d(r2)
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.i9 r0 = r0.g
            if (r0 == 0) goto L_0x10d8
            j.c.a.a.g.a.b3<java.lang.Boolean> r5 = j.c.a.a.g.a.k.p0
            boolean r5 = r0.d(r2, r5)
            j.c.a.a.g.a.r4 r0 = r1.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.b3<java.lang.Boolean> r6 = j.c.a.a.g.a.k.A0
            boolean r0 = r0.d(r2, r6)
            i.e.ArrayMap r6 = new i.e.ArrayMap
            r6.<init>()
            if (r0 == 0) goto L_0x0dbb
            j.c.a.a.g.a.n9 r0 = r82.s()
            r6 = r22
            java.util.Map r6 = r0.a(r2, r6)
        L_0x0dbb:
            java.util.ArrayList r7 = new java.util.ArrayList
            r7.<init>()
            java.util.Set r0 = r44.keySet()
            java.util.Iterator r8 = r0.iterator()
        L_0x0dc8:
            boolean r0 = r8.hasNext()
            if (r0 == 0) goto L_0x10d7
            java.lang.Object r0 = r8.next()
            java.lang.Integer r0 = (java.lang.Integer) r0
            int r0 = r0.intValue()
            java.lang.Integer r9 = java.lang.Integer.valueOf(r0)
            boolean r9 = r15.contains(r9)
            if (r9 != 0) goto L_0x10d1
            java.lang.Integer r9 = java.lang.Integer.valueOf(r0)
            r10 = r43
            java.lang.Object r9 = r10.get(r9)
            j.c.a.a.f.e.o0 r9 = (j.c.a.a.f.e.o0) r9
            if (r9 != 0) goto L_0x0df5
            j.c.a.a.f.e.o0$a r9 = j.c.a.a.f.e.o0.i()
            goto L_0x0dfb
        L_0x0df5:
            j.c.a.a.f.e.x3$a r9 = r9.h()
            j.c.a.a.f.e.o0$a r9 = (j.c.a.a.f.e.o0.a) r9
        L_0x0dfb:
            r9.i()
            MessageType r11 = r9.c
            j.c.a.a.f.e.o0 r11 = (j.c.a.a.f.e.o0) r11
            int r12 = r11.zzc
            r12 = r12 | r3
            r11.zzc = r12
            r11.zzd = r0
            j.c.a.a.f.e.w0$a r11 = j.c.a.a.f.e.w0.p()
            java.lang.Integer r12 = java.lang.Integer.valueOf(r0)
            r13 = r44
            java.lang.Object r12 = r13.get(r12)
            java.util.BitSet r12 = (java.util.BitSet) r12
            java.util.List r12 = j.c.a.a.g.a.u8.a(r12)
            r11.b(r12)
            java.lang.Integer r12 = java.lang.Integer.valueOf(r0)
            r14 = r20
            java.lang.Object r12 = r14.get(r12)
            java.util.BitSet r12 = (java.util.BitSet) r12
            java.util.List r12 = j.c.a.a.g.a.u8.a(r12)
            r11.a(r12)
            if (r4 == 0) goto L_0x1025
            java.lang.Integer r12 = java.lang.Integer.valueOf(r0)
            r3 = r33
            java.lang.Object r12 = r3.get(r12)
            java.util.Map r12 = (java.util.Map) r12
            java.util.List r12 = a(r12)
            r11.c(r12)
            java.lang.Integer r12 = java.lang.Integer.valueOf(r0)
            r1 = r42
            java.lang.Object r12 = r1.get(r12)
            java.util.Map r12 = (java.util.Map) r12
            if (r12 != 0) goto L_0x0e66
            java.util.List r12 = java.util.Collections.emptyList()
            r22 = r1
            r23 = r3
            r85 = r4
            r86 = r8
            r44 = r13
            goto L_0x0ee6
        L_0x0e66:
            r22 = r1
            java.util.ArrayList r1 = new java.util.ArrayList
            r23 = r3
            int r3 = r12.size()
            r1.<init>(r3)
            java.util.Set r3 = r12.keySet()
            java.util.Iterator r3 = r3.iterator()
        L_0x0e7b:
            boolean r16 = r3.hasNext()
            if (r16 == 0) goto L_0x0edf
            java.lang.Object r16 = r3.next()
            r84 = r3
            r3 = r16
            java.lang.Integer r3 = (java.lang.Integer) r3
            r85 = r4
            j.c.a.a.f.e.x0$a r4 = j.c.a.a.f.e.x0.n()
            r86 = r8
            int r8 = r3.intValue()
            r4.a(r8)
            java.lang.Object r3 = r12.get(r3)
            java.util.List r3 = (java.util.List) r3
            if (r3 == 0) goto L_0x0ec5
            java.util.Collections.sort(r3)
            java.util.Iterator r3 = r3.iterator()
        L_0x0ea9:
            boolean r8 = r3.hasNext()
            if (r8 == 0) goto L_0x0ec5
            java.lang.Object r8 = r3.next()
            java.lang.Long r8 = (java.lang.Long) r8
            r16 = r12
            r44 = r13
            long r12 = r8.longValue()
            r4.a(r12)
            r12 = r16
            r13 = r44
            goto L_0x0ea9
        L_0x0ec5:
            r16 = r12
            r44 = r13
            j.c.a.a.f.e.f5 r3 = r4.m()
            j.c.a.a.f.e.x3 r3 = (j.c.a.a.f.e.x3) r3
            j.c.a.a.f.e.x0 r3 = (j.c.a.a.f.e.x0) r3
            r1.add(r3)
            r3 = r84
            r4 = r85
            r8 = r86
            r12 = r16
            r13 = r44
            goto L_0x0e7b
        L_0x0edf:
            r85 = r4
            r86 = r8
            r44 = r13
            r12 = r1
        L_0x0ee6:
            if (r5 == 0) goto L_0x101f
            boolean r1 = r9.n()
            if (r1 == 0) goto L_0x101f
            j.c.a.a.f.e.da.b()
            j.c.a.a.g.a.i9 r1 = r82.m()
            j.c.a.a.g.a.b3<java.lang.Boolean> r3 = j.c.a.a.g.a.k.B0
            boolean r1 = r1.d(r2, r3)
            if (r1 == 0) goto L_0x0eff
            if (r25 != 0) goto L_0x101f
        L_0x0eff:
            j.c.a.a.f.e.w0 r1 = r9.o()
            java.util.List r1 = r1.n()
            java.lang.Integer r3 = java.lang.Integer.valueOf(r0)
            java.lang.Object r3 = r6.get(r3)
            java.util.List r3 = (java.util.List) r3
            boolean r4 = r1.isEmpty()
            if (r4 == 0) goto L_0x0f19
            goto L_0x101f
        L_0x0f19:
            java.util.ArrayList r4 = new java.util.ArrayList
            r4.<init>(r12)
            i.e.ArrayMap r8 = new i.e.ArrayMap
            r8.<init>()
            java.util.Iterator r1 = r1.iterator()
        L_0x0f27:
            boolean r12 = r1.hasNext()
            if (r12 == 0) goto L_0x0f60
            java.lang.Object r12 = r1.next()
            j.c.a.a.f.e.x0 r12 = (j.c.a.a.f.e.x0) r12
            boolean r13 = r12.a()
            if (r13 == 0) goto L_0x0f5b
            int r13 = r12.m()
            if (r13 <= 0) goto L_0x0f5b
            int r13 = r12.i()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            int r16 = r12.m()
            r84 = r1
            int r1 = r16 + -1
            long r16 = r12.a(r1)
            java.lang.Long r1 = java.lang.Long.valueOf(r16)
            r8.put(r13, r1)
            goto L_0x0f5d
        L_0x0f5b:
            r84 = r1
        L_0x0f5d:
            r1 = r84
            goto L_0x0f27
        L_0x0f60:
            r1 = 0
        L_0x0f61:
            int r12 = r4.size()
            if (r1 >= r12) goto L_0x0fdf
            java.lang.Object r12 = r4.get(r1)
            j.c.a.a.f.e.x0 r12 = (j.c.a.a.f.e.x0) r12
            boolean r13 = r12.a()
            if (r13 == 0) goto L_0x0f7c
            int r13 = r12.i()
            java.lang.Integer r13 = java.lang.Integer.valueOf(r13)
            goto L_0x0f7d
        L_0x0f7c:
            r13 = 0
        L_0x0f7d:
            java.lang.Object r13 = r8.remove(r13)
            java.lang.Long r13 = (java.lang.Long) r13
            if (r13 == 0) goto L_0x0fd3
            if (r3 == 0) goto L_0x0f9b
            int r16 = r12.i()
            r84 = r5
            java.lang.Integer r5 = java.lang.Integer.valueOf(r16)
            boolean r5 = r3.contains(r5)
            if (r5 != 0) goto L_0x0f98
            goto L_0x0f9d
        L_0x0f98:
            r18 = r3
            goto L_0x0fd7
        L_0x0f9b:
            r84 = r5
        L_0x0f9d:
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            long r16 = r13.longValue()
            r18 = r3
            r3 = 0
            long r19 = r12.a(r3)
            int r24 = (r16 > r19 ? 1 : (r16 == r19 ? 0 : -1))
            if (r24 >= 0) goto L_0x0fb4
            r5.add(r13)
        L_0x0fb4:
            java.util.List r13 = r12.j()
            r5.addAll(r13)
            j.c.a.a.f.e.x3$a r12 = r12.h()
            j.c.a.a.f.e.x0$a r12 = (j.c.a.a.f.e.x0.a) r12
            r12.a()
            r12.a(r5)
            j.c.a.a.f.e.f5 r5 = r12.m()
            j.c.a.a.f.e.x3 r5 = (j.c.a.a.f.e.x3) r5
            j.c.a.a.f.e.x0 r5 = (j.c.a.a.f.e.x0) r5
            r4.set(r1, r5)
            goto L_0x0fd8
        L_0x0fd3:
            r18 = r3
            r84 = r5
        L_0x0fd7:
            r3 = 0
        L_0x0fd8:
            int r1 = r1 + 1
            r5 = r84
            r3 = r18
            goto L_0x0f61
        L_0x0fdf:
            r84 = r5
            r3 = 0
            java.util.Set r1 = r8.keySet()
            java.util.Iterator r1 = r1.iterator()
        L_0x0fea:
            boolean r5 = r1.hasNext()
            if (r5 == 0) goto L_0x101d
            java.lang.Object r5 = r1.next()
            java.lang.Integer r5 = (java.lang.Integer) r5
            j.c.a.a.f.e.x0$a r12 = j.c.a.a.f.e.x0.n()
            int r13 = r5.intValue()
            r12.a(r13)
            java.lang.Object r5 = r8.get(r5)
            java.lang.Long r5 = (java.lang.Long) r5
            r13 = r4
            long r3 = r5.longValue()
            r12.a(r3)
            j.c.a.a.f.e.f5 r3 = r12.m()
            j.c.a.a.f.e.x3 r3 = (j.c.a.a.f.e.x3) r3
            j.c.a.a.f.e.x0 r3 = (j.c.a.a.f.e.x0) r3
            r4 = r13
            r4.add(r3)
            r3 = 0
            goto L_0x0fea
        L_0x101d:
            r12 = r4
            goto L_0x1021
        L_0x101f:
            r84 = r5
        L_0x1021:
            r11.d(r12)
            goto L_0x1031
        L_0x1025:
            r85 = r4
            r84 = r5
            r86 = r8
            r44 = r13
            r23 = r33
            r22 = r42
        L_0x1031:
            r9.i()
            MessageType r1 = r9.c
            j.c.a.a.f.e.o0 r1 = (j.c.a.a.f.e.o0) r1
            j.c.a.a.f.e.o0.a(r1, r11)
            java.lang.Integer r1 = java.lang.Integer.valueOf(r0)
            j.c.a.a.f.e.f5 r3 = r9.m()
            j.c.a.a.f.e.x3 r3 = (j.c.a.a.f.e.x3) r3
            j.c.a.a.f.e.o0 r3 = (j.c.a.a.f.e.o0) r3
            r10.put(r1, r3)
            j.c.a.a.f.e.f5 r1 = r9.m()
            j.c.a.a.f.e.x3 r1 = (j.c.a.a.f.e.x3) r1
            j.c.a.a.f.e.o0 r1 = (j.c.a.a.f.e.o0) r1
            r7.add(r1)
            j.c.a.a.g.a.n9 r1 = r82.s()
            j.c.a.a.f.e.w0 r3 = r9.a()
            r1.o()
            r1.d()
            i.b.k.ResourcesFlusher.b(r83)
            i.b.k.ResourcesFlusher.b(r3)
            byte[] r3 = r3.f()
            android.content.ContentValues r4 = new android.content.ContentValues
            r4.<init>()
            java.lang.String r5 = "app_id"
            r4.put(r5, r2)
            java.lang.Integer r0 = java.lang.Integer.valueOf(r0)
            java.lang.String r5 = "audience_id"
            r4.put(r5, r0)
            java.lang.String r0 = "current_results"
            r4.put(r0, r3)
            android.database.sqlite.SQLiteDatabase r0 = r1.v()     // Catch:{ SQLiteException -> 0x10ab }
            java.lang.String r3 = "audience_filter_values"
            r5 = 5
            r8 = 0
            long r3 = r0.insertWithOnConflict(r3, r8, r4, r5)     // Catch:{ SQLiteException -> 0x10a9 }
            r11 = -1
            int r0 = (r3 > r11 ? 1 : (r3 == r11 ? 0 : -1))
            if (r0 != 0) goto L_0x10be
            j.c.a.a.g.a.n3 r0 = r1.a()     // Catch:{ SQLiteException -> 0x10a9 }
            j.c.a.a.g.a.p3 r0 = r0.u()     // Catch:{ SQLiteException -> 0x10a9 }
            java.lang.String r3 = "Failed to insert filter results (got -1). appId"
            java.lang.Object r4 = j.c.a.a.g.a.n3.a(r83)     // Catch:{ SQLiteException -> 0x10a9 }
            r0.a(r3, r4)     // Catch:{ SQLiteException -> 0x10a9 }
            goto L_0x10be
        L_0x10a9:
            r0 = move-exception
            goto L_0x10ad
        L_0x10ab:
            r0 = move-exception
            r8 = 0
        L_0x10ad:
            j.c.a.a.g.a.n3 r1 = r1.a()
            j.c.a.a.g.a.p3 r1 = r1.u()
            java.lang.Object r3 = j.c.a.a.g.a.n3.a(r83)
            java.lang.String r4 = "Error storing filter results. appId"
            r1.a(r4, r3, r0)
        L_0x10be:
            r1 = r82
            r5 = r84
            r4 = r85
            r8 = r86
            r43 = r10
            r20 = r14
            r42 = r22
            r33 = r23
            r3 = 1
            goto L_0x0dc8
        L_0x10d1:
            r86 = r8
            r1 = r82
            goto L_0x0dc8
        L_0x10d7:
            return r7
        L_0x10d8:
            r8 = 0
            throw r8
        L_0x10da:
            r8 = 0
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.f9.a(java.lang.String, java.util.List, java.util.List, java.lang.Long):java.util.List");
    }

    public final boolean q() {
        return false;
    }

    public final Boolean a(d0 d0Var, y0 y0Var) {
        b0 b0Var = d0Var.zzf;
        if (b0Var == null) {
            b0Var = b0.zzh;
        }
        boolean z = b0Var.zzf;
        boolean z2 = false;
        Boolean bool = null;
        if (!((y0Var.zzc & 8) != 0)) {
            if (!((y0Var.zzc & 32) != 0)) {
                if ((y0Var.zzc & 4) != 0) {
                    if ((b0Var.zzc & 1) != 0) {
                        z2 = true;
                    }
                    if (z2) {
                        return a(a(y0Var.zzf, b0Var.i()), z);
                    }
                    if (!b0Var.j()) {
                        a().f2047i.a("No string or number filter defined. property", g().c(y0Var.zze));
                    } else if (u8.a(y0Var.zzf)) {
                        return a(a(y0Var.zzf, b0Var.m()), z);
                    } else {
                        a().f2047i.a("Invalid user property value for Numeric number filter. property, value", g().c(y0Var.zze), y0Var.zzf);
                    }
                    return null;
                }
                a().f2047i.a("User property has no value, property", g().c(y0Var.zze));
                return null;
            } else if (!b0Var.j()) {
                a().f2047i.a("No number filter for double property. property", g().c(y0Var.zze));
                return null;
            } else {
                double d = y0Var.zzi;
                try {
                    bool = a(new BigDecimal(d), b0Var.m(), Math.ulp(d));
                } catch (NumberFormatException unused) {
                }
                return a(bool, z);
            }
        } else if (b0Var.j()) {
            return a(a(y0Var.zzg, b0Var.m()), z);
        } else {
            a().f2047i.a("No number filter for long property. property", g().c(y0Var.zze));
            return null;
        }
    }

    public final Boolean a(a0 a0Var, String str, List<s0> list, long j2) {
        Boolean bool;
        Boolean bool2;
        if ((a0Var.zzc & 8) != 0) {
            c0 c0Var = a0Var.zzh;
            if (c0Var == null) {
                c0Var = c0.zzi;
            }
            Boolean a = a(j2, c0Var);
            if (a == null) {
                return null;
            }
            if (!a.booleanValue()) {
                return false;
            }
        }
        HashSet hashSet = new HashSet();
        for (b0 next : a0Var.zzf) {
            if (next.zzg.isEmpty()) {
                a().f2047i.a("null or empty param name in filter. event", g().a(str));
                return null;
            }
            hashSet.add(next.zzg);
        }
        ArrayMap arrayMap = new ArrayMap();
        for (s0 next2 : list) {
            if (hashSet.contains(next2.zzd)) {
                if (next2.j()) {
                    arrayMap.put(next2.zzd, next2.j() ? Long.valueOf(next2.zzf) : null);
                } else if (next2.n()) {
                    arrayMap.put(next2.zzd, next2.n() ? Double.valueOf(next2.zzh) : null);
                } else {
                    if ((next2.zzc & 2) != 0) {
                        arrayMap.put(next2.zzd, next2.zze);
                    } else {
                        a().f2047i.a("Unknown value for param. event, param", g().a(str), g().b(next2.zzd));
                        return null;
                    }
                }
            }
        }
        for (b0 next3 : a0Var.zzf) {
            boolean z = ((next3.zzc & 4) != 0) && next3.zzf;
            String str2 = next3.zzg;
            if (str2.isEmpty()) {
                a().f2047i.a("Event has empty param name. event", g().a(str));
                return null;
            }
            Object orDefault = arrayMap.getOrDefault(str2, null);
            if (orDefault instanceof Long) {
                if (!next3.j()) {
                    a().f2047i.a("No number filter for long param. event, param", g().a(str), g().b(str2));
                    return null;
                }
                Boolean a2 = a(((Long) orDefault).longValue(), next3.m());
                if (a2 == null) {
                    return null;
                }
                if (a2.booleanValue() == z) {
                    return false;
                }
            } else if (orDefault instanceof Double) {
                if (!next3.j()) {
                    a().f2047i.a("No number filter for double param. event, param", g().a(str), g().b(str2));
                    return null;
                }
                double doubleValue = ((Double) orDefault).doubleValue();
                try {
                    bool2 = a(new BigDecimal(doubleValue), next3.m(), Math.ulp(doubleValue));
                } catch (NumberFormatException unused) {
                    bool2 = null;
                }
                if (bool2 == null) {
                    return null;
                }
                if (bool2.booleanValue() == z) {
                    return false;
                }
            } else if (orDefault instanceof String) {
                if ((next3.zzc & 1) != 0) {
                    bool = a((String) orDefault, next3.i());
                } else if (next3.j()) {
                    String str3 = (String) orDefault;
                    if (u8.a(str3)) {
                        bool = a(str3, next3.m());
                    } else {
                        a().f2047i.a("Invalid param value for number filter. event, param", g().a(str), g().b(str2));
                        return null;
                    }
                } else {
                    a().f2047i.a("No filter for String param. event, param", g().a(str), g().b(str2));
                    return null;
                }
                if (bool == null) {
                    return null;
                }
                if (bool.booleanValue() == z) {
                    return false;
                }
            } else if (orDefault == null) {
                a().f2052n.a("Missing param for filter. event, param", g().a(str), g().b(str2));
                return false;
            } else {
                a().f2047i.a("Unknown param type. event, param", g().a(str), g().b(str2));
                return null;
            }
        }
        return true;
    }

    public static Boolean a(Boolean bool, boolean z) {
        if (bool == null) {
            return null;
        }
        return Boolean.valueOf(bool.booleanValue() != z);
    }

    public final Boolean a(String str, e0 e0Var) {
        String str2;
        List<String> list;
        ResourcesFlusher.b(e0Var);
        if (str == null) {
            return null;
        }
        boolean z = true;
        int i2 = 0;
        if (!((e0Var.zzc & 1) != 0) || e0Var.i() == e0.b.UNKNOWN_MATCH_TYPE) {
            return null;
        }
        if (e0Var.i() != e0.b.IN_LIST) {
            if ((e0Var.zzc & 2) == 0) {
                z = false;
            }
            if (!z) {
                return null;
            }
        } else if (e0Var.j() == 0) {
            return null;
        }
        e0.b i3 = e0Var.i();
        boolean z2 = e0Var.zzf;
        if (z2 || i3 == e0.b.REGEXP || i3 == e0.b.IN_LIST) {
            str2 = e0Var.zze;
        } else {
            str2 = e0Var.zze.toUpperCase(Locale.ENGLISH);
        }
        if (e0Var.j() == 0) {
            list = null;
        } else {
            list = e0Var.zzg;
            if (!z2) {
                ArrayList arrayList = new ArrayList(list.size());
                for (String upperCase : list) {
                    arrayList.add(upperCase.toUpperCase(Locale.ENGLISH));
                }
                list = Collections.unmodifiableList(arrayList);
            }
        }
        String str3 = i3 == e0.b.REGEXP ? str2 : null;
        if (i3 == e0.b.IN_LIST) {
            if (list == null || list.size() == 0) {
                return null;
            }
        } else if (str2 == null) {
            return null;
        }
        if (!z2 && i3 != e0.b.REGEXP) {
            str = str.toUpperCase(Locale.ENGLISH);
        }
        switch (e9.a[i3.ordinal()]) {
            case 1:
                if (!z2) {
                    i2 = 66;
                }
                try {
                    return Boolean.valueOf(Pattern.compile(str3, i2).matcher(str).matches());
                } catch (PatternSyntaxException unused) {
                    a().f2047i.a("Invalid regular expression in REGEXP audience filter. expression", str3);
                    return null;
                }
            case 2:
                return Boolean.valueOf(str.startsWith(str2));
            case 3:
                return Boolean.valueOf(str.endsWith(str2));
            case 4:
                return Boolean.valueOf(str.contains(str2));
            case 5:
                return Boolean.valueOf(str.equals(str2));
            case 6:
                return Boolean.valueOf(list.contains(str));
            default:
                return null;
        }
    }

    public final Boolean a(long j2, c0 c0Var) {
        try {
            return a(new BigDecimal(j2), c0Var, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    public final Boolean a(String str, c0 c0Var) {
        if (!u8.a(str)) {
            return null;
        }
        try {
            return a(new BigDecimal(str), c0Var, 0.0d);
        } catch (NumberFormatException unused) {
            return null;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:19:0x0037, code lost:
        if (((r11.zzc & 16) != 0) == false) goto L_0x0039;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x008e, code lost:
        if (r4 != null) goto L_0x0090;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.Boolean a(java.math.BigDecimal r10, j.c.a.a.f.e.c0 r11, double r12) {
        /*
            i.b.k.ResourcesFlusher.b(r11)
            int r0 = r11.zzc
            r1 = 1
            r0 = r0 & r1
            r2 = 0
            if (r0 == 0) goto L_0x000c
            r0 = 1
            goto L_0x000d
        L_0x000c:
            r0 = 0
        L_0x000d:
            r3 = 0
            if (r0 == 0) goto L_0x011c
            j.c.a.a.f.e.c0$a r0 = r11.i()
            j.c.a.a.f.e.c0$a r4 = j.c.a.a.f.e.c0.a.UNKNOWN_COMPARISON_TYPE
            if (r0 != r4) goto L_0x001a
            goto L_0x011c
        L_0x001a:
            j.c.a.a.f.e.c0$a r0 = r11.i()
            j.c.a.a.f.e.c0$a r4 = j.c.a.a.f.e.c0.a.BETWEEN
            r5 = 4
            if (r0 != r4) goto L_0x003a
            int r0 = r11.zzc
            r0 = r0 & 8
            if (r0 == 0) goto L_0x002b
            r0 = 1
            goto L_0x002c
        L_0x002b:
            r0 = 0
        L_0x002c:
            if (r0 == 0) goto L_0x0039
            int r0 = r11.zzc
            r0 = r0 & 16
            if (r0 == 0) goto L_0x0036
            r0 = 1
            goto L_0x0037
        L_0x0036:
            r0 = 0
        L_0x0037:
            if (r0 != 0) goto L_0x0045
        L_0x0039:
            return r3
        L_0x003a:
            int r0 = r11.zzc
            r0 = r0 & r5
            if (r0 == 0) goto L_0x0041
            r0 = 1
            goto L_0x0042
        L_0x0041:
            r0 = 0
        L_0x0042:
            if (r0 != 0) goto L_0x0045
            return r3
        L_0x0045:
            j.c.a.a.f.e.c0$a r0 = r11.i()
            j.c.a.a.f.e.c0$a r4 = r11.i()
            j.c.a.a.f.e.c0$a r6 = j.c.a.a.f.e.c0.a.BETWEEN
            if (r4 != r6) goto L_0x0074
            java.lang.String r4 = r11.zzg
            boolean r4 = j.c.a.a.g.a.u8.a(r4)
            if (r4 == 0) goto L_0x0073
            java.lang.String r4 = r11.zzh
            boolean r4 = j.c.a.a.g.a.u8.a(r4)
            if (r4 != 0) goto L_0x0062
            goto L_0x0073
        L_0x0062:
            java.math.BigDecimal r4 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0073 }
            java.lang.String r6 = r11.zzg     // Catch:{ NumberFormatException -> 0x0073 }
            r4.<init>(r6)     // Catch:{ NumberFormatException -> 0x0073 }
            java.math.BigDecimal r6 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x0073 }
            java.lang.String r11 = r11.zzh     // Catch:{ NumberFormatException -> 0x0073 }
            r6.<init>(r11)     // Catch:{ NumberFormatException -> 0x0073 }
            r11 = r4
            r4 = r3
            goto L_0x0086
        L_0x0073:
            return r3
        L_0x0074:
            java.lang.String r4 = r11.zzf
            boolean r4 = j.c.a.a.g.a.u8.a(r4)
            if (r4 != 0) goto L_0x007d
            return r3
        L_0x007d:
            java.math.BigDecimal r4 = new java.math.BigDecimal     // Catch:{ NumberFormatException -> 0x011c }
            java.lang.String r11 = r11.zzf     // Catch:{ NumberFormatException -> 0x011c }
            r4.<init>(r11)     // Catch:{ NumberFormatException -> 0x011c }
            r11 = r3
            r6 = r11
        L_0x0086:
            j.c.a.a.f.e.c0$a r7 = j.c.a.a.f.e.c0.a.BETWEEN
            if (r0 != r7) goto L_0x008e
            if (r11 == 0) goto L_0x008d
            goto L_0x0090
        L_0x008d:
            return r3
        L_0x008e:
            if (r4 == 0) goto L_0x011c
        L_0x0090:
            int[] r7 = j.c.a.a.g.a.e9.b
            int r0 = r0.ordinal()
            r0 = r7[r0]
            r7 = -1
            if (r0 == r1) goto L_0x010f
            r8 = 2
            if (r0 == r8) goto L_0x0102
            r9 = 3
            if (r0 == r9) goto L_0x00b8
            if (r0 == r5) goto L_0x00a5
            goto L_0x011c
        L_0x00a5:
            int r11 = r10.compareTo(r11)
            if (r11 == r7) goto L_0x00b2
            int r10 = r10.compareTo(r6)
            if (r10 == r1) goto L_0x00b2
            goto L_0x00b3
        L_0x00b2:
            r1 = 0
        L_0x00b3:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r1)
            return r10
        L_0x00b8:
            r5 = 0
            int r11 = (r12 > r5 ? 1 : (r12 == r5 ? 0 : -1))
            if (r11 == 0) goto L_0x00f5
            java.math.BigDecimal r11 = new java.math.BigDecimal
            r11.<init>(r12)
            java.math.BigDecimal r0 = new java.math.BigDecimal
            r0.<init>(r8)
            java.math.BigDecimal r11 = r11.multiply(r0)
            java.math.BigDecimal r11 = r4.subtract(r11)
            int r11 = r10.compareTo(r11)
            if (r11 != r1) goto L_0x00ef
            java.math.BigDecimal r11 = new java.math.BigDecimal
            r11.<init>(r12)
            java.math.BigDecimal r12 = new java.math.BigDecimal
            r12.<init>(r8)
            java.math.BigDecimal r11 = r11.multiply(r12)
            java.math.BigDecimal r11 = r4.add(r11)
            int r10 = r10.compareTo(r11)
            if (r10 != r7) goto L_0x00ef
            goto L_0x00f0
        L_0x00ef:
            r1 = 0
        L_0x00f0:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r1)
            return r10
        L_0x00f5:
            int r10 = r10.compareTo(r4)
            if (r10 != 0) goto L_0x00fc
            goto L_0x00fd
        L_0x00fc:
            r1 = 0
        L_0x00fd:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r1)
            return r10
        L_0x0102:
            int r10 = r10.compareTo(r4)
            if (r10 != r1) goto L_0x0109
            goto L_0x010a
        L_0x0109:
            r1 = 0
        L_0x010a:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r1)
            return r10
        L_0x010f:
            int r10 = r10.compareTo(r4)
            if (r10 != r7) goto L_0x0116
            goto L_0x0117
        L_0x0116:
            r1 = 0
        L_0x0117:
            java.lang.Boolean r10 = java.lang.Boolean.valueOf(r1)
            return r10
        L_0x011c:
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.f9.a(java.math.BigDecimal, j.c.a.a.f.e.c0, double):java.lang.Boolean");
    }

    public static List<p0> a(Map<Integer, Long> map) {
        if (map == null) {
            return null;
        }
        ArrayList arrayList = new ArrayList(map.size());
        for (Integer intValue : map.keySet()) {
            int intValue2 = intValue.intValue();
            p0.a aVar = (p0.a) p0.zzf.g();
            aVar.i();
            p0 p0Var = (p0) aVar.c;
            p0Var.zzc |= 1;
            p0Var.zzd = intValue2;
            long longValue = map.get(Integer.valueOf(intValue2)).longValue();
            aVar.i();
            p0 p0Var2 = (p0) aVar.c;
            p0Var2.zzc |= 2;
            p0Var2.zze = longValue;
            arrayList.add((p0) ((x3) aVar.m()));
        }
        return arrayList;
    }

    public static void a(Map<Integer, Long> map, int i2, long j2) {
        Long l2 = map.get(Integer.valueOf(i2));
        long j3 = j2 / 1000;
        if (l2 == null || j3 > l2.longValue()) {
            map.put(Integer.valueOf(i2), Long.valueOf(j3));
        }
    }
}
