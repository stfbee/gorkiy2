package j.c.a.a.f.e;

import android.content.SharedPreferences;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final /* synthetic */ class x1 implements SharedPreferences.OnSharedPreferenceChangeListener {
    public final y1 a;

    public x1(y1 y1Var) {
        this.a = y1Var;
    }

    public final void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String str) {
        this.a.a();
    }
}
