package j.c.a.a.g.a;

import android.os.IInterface;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public interface f3 extends IInterface {
    List<x8> a(d9 d9Var, boolean z);

    List<g9> a(String str, String str2, d9 d9Var);

    List<g9> a(String str, String str2, String str3);

    List<x8> a(String str, String str2, String str3, boolean z);

    List<x8> a(String str, String str2, boolean z, d9 d9Var);

    void a(long j2, String str, String str2, String str3);

    void a(d9 d9Var);

    void a(g9 g9Var);

    void a(g9 g9Var, d9 d9Var);

    void a(i iVar, d9 d9Var);

    void a(i iVar, String str, String str2);

    void a(x8 x8Var, d9 d9Var);

    byte[] a(i iVar, String str);

    String b(d9 d9Var);

    void c(d9 d9Var);

    void d(d9 d9Var);
}
