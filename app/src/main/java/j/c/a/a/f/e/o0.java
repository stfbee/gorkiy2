package j.c.a.a.f.e;

import j.c.a.a.f.e.w0;
import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class o0 extends x3<o0, a> implements g5 {
    public static final o0 zzh;
    public static volatile m5<o0> zzi;
    public int zzc;
    public int zzd;
    public w0 zze;
    public w0 zzf;
    public boolean zzg;

    static {
        o0 o0Var = new o0();
        zzh = o0Var;
        x3.zzd.put(o0.class, o0Var);
    }

    public static /* synthetic */ void a(o0 o0Var, w0.a aVar) {
        if (o0Var != null) {
            o0Var.zze = (w0) ((x3) aVar.m());
            o0Var.zzc |= 2;
            return;
        }
        throw null;
    }

    public static a i() {
        return (a) zzh.g();
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<o0, a> implements g5 {
        public a() {
            super(o0.zzh);
        }

        public final w0 a() {
            w0 w0Var = ((o0) super.c).zze;
            return w0Var == null ? w0.zzg : w0Var;
        }

        public final boolean n() {
            return (((o0) super.c).zzc & 4) != 0;
        }

        public final w0 o() {
            w0 w0Var = ((o0) super.c).zzf;
            return w0Var == null ? w0.zzg : w0Var;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(o0.zzh);
        }

        public final a a(w0.a aVar) {
            i();
            o0.a((o0) super.c, aVar);
            return this;
        }

        public final a a(w0 w0Var) {
            i();
            o0.a((o0) super.c, w0Var);
            return this;
        }

        public final a a(boolean z) {
            i();
            o0 o0Var = (o0) super.c;
            o0Var.zzc |= 8;
            o0Var.zzg = z;
            return this;
        }
    }

    public static /* synthetic */ void a(o0 o0Var, w0 w0Var) {
        if (w0Var != null) {
            o0Var.zzf = w0Var;
            o0Var.zzc |= 4;
            return;
        }
        throw null;
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new o0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzh, "\u0001\u0004\u0000\u0001\u0001\u0004\u0004\u0000\u0000\u0000\u0001\u0004\u0000\u0002\t\u0001\u0003\t\u0002\u0004\u0007\u0003", new Object[]{"zzc", "zzd", "zze", "zzf", "zzg"});
            case 4:
                return zzh;
            case 5:
                m5<o0> m5Var = zzi;
                if (m5Var == null) {
                    synchronized (o0.class) {
                        m5Var = zzi;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzh);
                            zzi = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
