package j.c.a.a.g.a;

import android.content.Context;
import android.content.Intent;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.nb;
import j.c.a.a.g.a.c8;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class x7<T extends Context & c8> {
    public final T a;

    public x7(T t2) {
        ResourcesFlusher.b((Object) t2);
        this.a = t2;
    }

    public final void a(Runnable runnable) {
        q8 a2 = q8.a((Context) this.a);
        l4 i2 = a2.i();
        d8 d8Var = new d8(a2, runnable);
        i2.o();
        ResourcesFlusher.b(d8Var);
        i2.a((p4<?>) new p4(i2, d8Var, "Task exception on worker thread"));
    }

    public final void b(Intent intent) {
        if (intent == null) {
            a().f2046f.a("onRebind called with null intent");
            return;
        }
        a().f2052n.a("onRebind called. action", intent.getAction());
    }

    public final boolean a(Intent intent) {
        if (intent == null) {
            a().f2046f.a("onUnbind called with null intent");
            return true;
        }
        a().f2052n.a("onUnbind called for intent. action", intent.getAction());
        return true;
    }

    public final n3 a() {
        return r4.a(this.a, (nb) null).a();
    }
}
