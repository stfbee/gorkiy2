package j.c.a.a.f.d;

import java.lang.ref.Reference;
import java.util.List;
import java.util.Vector;

public final class i extends e {
    public final h a = new h();

    public final void a(Throwable th, Throwable th2) {
        List putIfAbsent;
        if (th2 == th) {
            throw new IllegalArgumentException("Self suppression is not allowed.", th2);
        } else if (th2 != null) {
            h hVar = this.a;
            for (Reference<? extends Throwable> poll = hVar.b.poll(); poll != null; poll = hVar.b.poll()) {
                hVar.a.remove(poll);
            }
            List list = hVar.a.get(new g(th, null));
            if (list == null && (putIfAbsent = hVar.a.putIfAbsent(new g(th, hVar.b), (list = new Vector(2)))) != null) {
                list = putIfAbsent;
            }
            list.add(th2);
        } else {
            throw new NullPointerException("The suppressed exception cannot be null.");
        }
    }
}
