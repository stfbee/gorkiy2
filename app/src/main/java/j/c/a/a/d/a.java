package j.c.a.a.d;

import android.os.IBinder;
import android.os.IInterface;
import j.c.a.a.f.c.b;

public interface a extends IInterface {

    /* renamed from: j.c.a.a.d.a$a  reason: collision with other inner class name */
    public static abstract class C0025a extends b implements a {

        /* renamed from: j.c.a.a.d.a$a$a  reason: collision with other inner class name */
        public static class C0026a extends j.c.a.a.f.c.a implements a {
            public C0026a(IBinder iBinder) {
                super(iBinder, "com.google.android.gms.dynamic.IObjectWrapper");
            }
        }

        public C0025a() {
            super("com.google.android.gms.dynamic.IObjectWrapper");
        }

        public static a a(IBinder iBinder) {
            if (iBinder == null) {
                return null;
            }
            IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.dynamic.IObjectWrapper");
            if (queryLocalInterface instanceof a) {
                return (a) queryLocalInterface;
            }
            return new C0026a(iBinder);
        }
    }
}
