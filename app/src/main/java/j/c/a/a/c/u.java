package j.c.a.a.c;

import java.util.Arrays;

public final class u extends t {
    public final byte[] b;

    public u(byte[] bArr) {
        super(Arrays.copyOfRange(bArr, 0, 25));
        this.b = bArr;
    }

    public final byte[] g() {
        return this.b;
    }
}
