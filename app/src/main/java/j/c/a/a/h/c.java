package j.c.a.a.h;

import com.google.android.gms.common.api.Scope;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.k.a;

public final class c {
    public static final a.f<j.c.a.a.h.b.a> a = new a.f<>();
    public static final a.f<j.c.a.a.h.b.a> b = new a.f<>();
    public static final a.C0018a<j.c.a.a.h.b.a, a> c = new d();
    public static final a.C0018a<j.c.a.a.h.b.a, Object> d = new e();

    static {
        new Scope(1, "profile");
        new Scope(1, "email");
        a.C0018a<j.c.a.a.h.b.a, a> aVar = c;
        a.f<j.c.a.a.h.b.a> fVar = a;
        ResourcesFlusher.b(aVar, "Cannot construct an Api with a null ClientBuilder");
        ResourcesFlusher.b(fVar, "Cannot construct an Api with a null ClientKey");
        a.C0018a<j.c.a.a.h.b.a, Object> aVar2 = d;
        a.f<j.c.a.a.h.b.a> fVar2 = b;
        ResourcesFlusher.b(aVar2, "Cannot construct an Api with a null ClientBuilder");
        ResourcesFlusher.b(fVar2, "Cannot construct an Api with a null ClientKey");
    }
}
