package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n5 {
    public static final l5 a;
    public static final l5 b = new k5();

    static {
        l5 l5Var;
        try {
            l5Var = (l5) Class.forName("com.google.protobuf.NewInstanceSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            l5Var = null;
        }
        a = l5Var;
    }
}
