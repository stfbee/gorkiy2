package j.c.a.a.f.e;

import android.app.Activity;
import j.c.a.a.d.b;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class d extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Activity f1834f;
    public final /* synthetic */ String g;
    public final /* synthetic */ String h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ pb f1835i;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d(pb pbVar, Activity activity, String str, String str2) {
        super(true);
        this.f1835i = pbVar;
        this.f1834f = activity;
        this.g = str;
        this.h = str2;
    }

    public final void a() {
        this.f1835i.g.setCurrentScreen(new b(this.f1834f), this.g, this.h, super.b);
    }
}
