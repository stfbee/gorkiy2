package j.c.a.a.g.a;

import android.database.sqlite.SQLiteException;
import android.os.SystemClock;
import j.c.a.a.c.n.b;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class s8 implements t3 {
    public final /* synthetic */ String a;
    public final /* synthetic */ q8 b;

    public s8(q8 q8Var, String str) {
        this.b = q8Var;
        this.a = str;
    }

    /* JADX INFO: finally extract failed */
    public final void a(String str, int i2, Throwable th, byte[] bArr, Map<String, List<String>> map) {
        n9 e2;
        q8 q8Var = this.b;
        q8Var.r();
        q8Var.m();
        if (bArr == null) {
            try {
                bArr = new byte[0];
            } catch (Throwable th2) {
                q8Var.f2082r = false;
                q8Var.b();
                throw th2;
            }
        }
        List<Long> list = q8Var.v;
        q8Var.v = null;
        boolean z = true;
        if ((i2 == 200 || i2 == 204) && th == null) {
            try {
                b4 b4Var = q8Var.f2073i.l().f2111e;
                if (((b) q8Var.f2073i.f2092n) != null) {
                    b4Var.a(System.currentTimeMillis());
                    q8Var.f2073i.l().f2112f.a(0);
                    q8Var.u();
                    q8Var.f2073i.a().f2052n.a("Successful upload. Got network response. code, size", Integer.valueOf(i2), Integer.valueOf(bArr.length));
                    q8Var.e().z();
                    try {
                        for (Long next : list) {
                            try {
                                e2 = q8Var.e();
                                long longValue = next.longValue();
                                e2.d();
                                e2.o();
                                if (e2.v().delete("queue", "rowid=?", new String[]{String.valueOf(longValue)}) != 1) {
                                    throw new SQLiteException("Deleted fewer rows from queue than expected");
                                }
                            } catch (SQLiteException e3) {
                                e2.a().f2046f.a("Failed to delete a bundle in a queue table", e3);
                                throw e3;
                            } catch (SQLiteException e4) {
                                if (q8Var.w == null || !q8Var.w.contains(next)) {
                                    throw e4;
                                }
                            }
                        }
                        q8Var.e().u();
                        q8Var.e().A();
                        q8Var.w = null;
                        if (!q8Var.d().u() || !q8Var.t()) {
                            q8Var.x = -1;
                            q8Var.u();
                        } else {
                            q8Var.n();
                        }
                        q8Var.f2077m = 0;
                    } catch (Throwable th3) {
                        q8Var.e().A();
                        throw th3;
                    }
                } else {
                    throw null;
                }
            } catch (SQLiteException e5) {
                q8Var.f2073i.a().f2046f.a("Database error while trying to delete uploaded bundles", e5);
                if (((b) q8Var.f2073i.f2092n) != null) {
                    q8Var.f2077m = SystemClock.elapsedRealtime();
                    q8Var.f2073i.a().f2052n.a("Disable upload, time", Long.valueOf(q8Var.f2077m));
                } else {
                    throw null;
                }
            }
        } else {
            q8Var.f2073i.a().f2052n.a("Network upload failed. Will retry later. code, error", Integer.valueOf(i2), th);
            b4 b4Var2 = q8Var.f2073i.l().f2112f;
            if (((b) q8Var.f2073i.f2092n) != null) {
                b4Var2.a(System.currentTimeMillis());
                if (i2 != 503) {
                    if (i2 != 429) {
                        z = false;
                    }
                }
                if (z) {
                    b4 b4Var3 = q8Var.f2073i.l().g;
                    if (((b) q8Var.f2073i.f2092n) != null) {
                        b4Var3.a(System.currentTimeMillis());
                    } else {
                        throw null;
                    }
                }
                q8Var.e().a(list);
                q8Var.u();
            } else {
                throw null;
            }
        }
        q8Var.f2082r = false;
        q8Var.b();
    }
}
