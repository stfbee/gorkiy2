package j.c.a.a.f.e;

import android.annotation.SuppressLint;
import android.content.Context;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicInteger;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class p1<T> {

    /* renamed from: f  reason: collision with root package name */
    public static final Object f1893f = new Object();
    @SuppressLint({"StaticFieldLeak"})
    public static Context g;
    public static b2<m1> h;

    /* renamed from: i  reason: collision with root package name */
    public static final AtomicInteger f1894i = new AtomicInteger();
    public final v1 a;
    public final String b;
    public final T c;
    public volatile int d = -1;

    /* renamed from: e  reason: collision with root package name */
    public volatile T f1895e;

    public /* synthetic */ p1(v1 v1Var, String str, Object obj, r1 r1Var) {
        if (v1Var.a != null) {
            this.a = v1Var;
            this.b = str;
            this.c = obj;
            return;
        }
        throw new IllegalArgumentException("Must pass a valid SharedPreferences file name or ContentProvider URI");
    }

    public static void a(Context context) {
        synchronized (f1893f) {
            Context applicationContext = context.getApplicationContext();
            if (applicationContext != null) {
                context = applicationContext;
            }
            if (g != context) {
                e1.c();
                y1.b();
                l1.a();
                f1894i.incrementAndGet();
                g = context;
                h = c.a(s1.b);
            }
        }
    }

    public abstract T a(Object obj);

    /* JADX WARNING: Removed duplicated region for block: B:22:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0061  */
    /* JADX WARNING: Removed duplicated region for block: B:50:0x00e8  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x00e9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final T b() {
        /*
            r7 = this;
            java.util.concurrent.atomic.AtomicInteger r0 = j.c.a.a.f.e.p1.f1894i
            int r0 = r0.get()
            int r1 = r7.d
            if (r1 >= r0) goto L_0x0119
            monitor-enter(r7)
            int r1 = r7.d     // Catch:{ all -> 0x0116 }
            if (r1 >= r0) goto L_0x0114
            android.content.Context r1 = j.c.a.a.f.e.p1.g     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x010c
            j.c.a.a.f.e.b2<j.c.a.a.f.e.m1> r1 = j.c.a.a.f.e.p1.h     // Catch:{ all -> 0x0116 }
            java.lang.Object r1 = r1.a()     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.m1 r1 = (j.c.a.a.f.e.m1) r1     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.v1 r2 = r7.a     // Catch:{ all -> 0x0116 }
            android.net.Uri r2 = r2.a     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.v1 r3 = r7.a     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = r3.c     // Catch:{ all -> 0x0116 }
            java.lang.String r4 = r7.b     // Catch:{ all -> 0x0116 }
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> r5 = r1.a     // Catch:{ all -> 0x0116 }
            r6 = 0
            if (r5 != 0) goto L_0x002c
        L_0x002a:
            r1 = r6
            goto L_0x0059
        L_0x002c:
            if (r2 == 0) goto L_0x002a
            java.lang.String r2 = r2.toString()     // Catch:{ all -> 0x0116 }
            java.util.Map<java.lang.String, java.util.Map<java.lang.String, java.lang.String>> r1 = r1.a     // Catch:{ all -> 0x0116 }
            java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x0116 }
            java.util.Map r1 = (java.util.Map) r1     // Catch:{ all -> 0x0116 }
            if (r1 != 0) goto L_0x003d
            goto L_0x002a
        L_0x003d:
            if (r3 == 0) goto L_0x0053
            java.lang.String r2 = java.lang.String.valueOf(r4)     // Catch:{ all -> 0x0116 }
            int r4 = r2.length()     // Catch:{ all -> 0x0116 }
            if (r4 == 0) goto L_0x004e
            java.lang.String r4 = r3.concat(r2)     // Catch:{ all -> 0x0116 }
            goto L_0x0053
        L_0x004e:
            java.lang.String r4 = new java.lang.String     // Catch:{ all -> 0x0116 }
            r4.<init>(r3)     // Catch:{ all -> 0x0116 }
        L_0x0053:
            java.lang.Object r1 = r1.get(r4)     // Catch:{ all -> 0x0116 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0116 }
        L_0x0059:
            if (r1 == 0) goto L_0x0061
            java.lang.Object r1 = r7.a(r1)     // Catch:{ all -> 0x0116 }
            goto L_0x0107
        L_0x0061:
            android.content.Context r1 = j.c.a.a.f.e.p1.g     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.l1 r1 = j.c.a.a.f.e.l1.a(r1)     // Catch:{ all -> 0x0116 }
            java.lang.String r2 = "gms:phenotype:phenotype_flag:debug_bypass_phenotype"
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x0116 }
            java.lang.String r1 = (java.lang.String) r1     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x007f
            java.util.regex.Pattern r2 = j.c.a.a.f.e.d1.c     // Catch:{ all -> 0x0116 }
            java.util.regex.Matcher r1 = r2.matcher(r1)     // Catch:{ all -> 0x0116 }
            boolean r1 = r1.matches()     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x007f
            r1 = 1
            goto L_0x0080
        L_0x007f:
            r1 = 0
        L_0x0080:
            if (r1 != 0) goto L_0x00bc
            j.c.a.a.f.e.v1 r1 = r7.a     // Catch:{ all -> 0x0116 }
            android.net.Uri r1 = r1.a     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x00a5
            android.content.Context r1 = j.c.a.a.f.e.p1.g     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.v1 r2 = r7.a     // Catch:{ all -> 0x0116 }
            android.net.Uri r2 = r2.a     // Catch:{ all -> 0x0116 }
            boolean r1 = j.c.a.a.f.e.o1.a(r1, r2)     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x00a3
            android.content.Context r1 = j.c.a.a.f.e.p1.g     // Catch:{ all -> 0x0116 }
            android.content.ContentResolver r1 = r1.getContentResolver()     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.v1 r2 = r7.a     // Catch:{ all -> 0x0116 }
            android.net.Uri r2 = r2.a     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.e1 r1 = j.c.a.a.f.e.e1.a(r1, r2)     // Catch:{ all -> 0x0116 }
            goto L_0x00ab
        L_0x00a3:
            r1 = r6
            goto L_0x00ab
        L_0x00a5:
            android.content.Context r1 = j.c.a.a.f.e.p1.g     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.y1 r1 = j.c.a.a.f.e.y1.a(r1, r6)     // Catch:{ all -> 0x0116 }
        L_0x00ab:
            if (r1 == 0) goto L_0x00e5
            java.lang.String r2 = r7.a()     // Catch:{ all -> 0x0116 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x00e5
            java.lang.Object r1 = r7.a(r1)     // Catch:{ all -> 0x0116 }
            goto L_0x00e6
        L_0x00bc:
            java.lang.String r1 = "PhenotypeFlag"
            r2 = 3
            boolean r1 = android.util.Log.isLoggable(r1, r2)     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x00e5
            java.lang.String r1 = "PhenotypeFlag"
            java.lang.String r2 = "Bypass reading Phenotype values for flag: "
            java.lang.String r3 = r7.a()     // Catch:{ all -> 0x0116 }
            java.lang.String r3 = java.lang.String.valueOf(r3)     // Catch:{ all -> 0x0116 }
            int r4 = r3.length()     // Catch:{ all -> 0x0116 }
            if (r4 == 0) goto L_0x00dc
            java.lang.String r2 = r2.concat(r3)     // Catch:{ all -> 0x0116 }
            goto L_0x00e2
        L_0x00dc:
            java.lang.String r3 = new java.lang.String     // Catch:{ all -> 0x0116 }
            r3.<init>(r2)     // Catch:{ all -> 0x0116 }
            r2 = r3
        L_0x00e2:
            android.util.Log.d(r1, r2)     // Catch:{ all -> 0x0116 }
        L_0x00e5:
            r1 = r6
        L_0x00e6:
            if (r1 == 0) goto L_0x00e9
            goto L_0x0107
        L_0x00e9:
            android.content.Context r1 = j.c.a.a.f.e.p1.g     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.l1 r1 = j.c.a.a.f.e.l1.a(r1)     // Catch:{ all -> 0x0116 }
            j.c.a.a.f.e.v1 r2 = r7.a     // Catch:{ all -> 0x0116 }
            java.lang.String r2 = r2.b     // Catch:{ all -> 0x0116 }
            java.lang.String r2 = r7.a(r2)     // Catch:{ all -> 0x0116 }
            java.lang.Object r1 = r1.a(r2)     // Catch:{ all -> 0x0116 }
            if (r1 == 0) goto L_0x0101
            java.lang.Object r6 = r7.a(r1)     // Catch:{ all -> 0x0116 }
        L_0x0101:
            if (r6 == 0) goto L_0x0105
            r1 = r6
            goto L_0x0107
        L_0x0105:
            T r1 = r7.c     // Catch:{ all -> 0x0116 }
        L_0x0107:
            r7.f1895e = r1     // Catch:{ all -> 0x0116 }
            r7.d = r0     // Catch:{ all -> 0x0116 }
            goto L_0x0114
        L_0x010c:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException     // Catch:{ all -> 0x0116 }
            java.lang.String r1 = "Must call PhenotypeFlag.init() first"
            r0.<init>(r1)     // Catch:{ all -> 0x0116 }
            throw r0     // Catch:{ all -> 0x0116 }
        L_0x0114:
            monitor-exit(r7)     // Catch:{ all -> 0x0116 }
            goto L_0x0119
        L_0x0116:
            r0 = move-exception
            monitor-exit(r7)     // Catch:{ all -> 0x0116 }
            throw r0
        L_0x0119:
            T r0 = r7.f1895e
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.p1.b():java.lang.Object");
    }

    public final String a(String str) {
        if (str != null && str.isEmpty()) {
            return this.b;
        }
        String valueOf = String.valueOf(str);
        String valueOf2 = String.valueOf(this.b);
        return valueOf2.length() != 0 ? valueOf.concat(valueOf2) : new String(valueOf);
    }

    public final String a() {
        return a(this.a.c);
    }

    public static /* synthetic */ p1 a(v1 v1Var, String str, long j2) {
        return new r1(v1Var, str, Long.valueOf(j2));
    }

    public static /* synthetic */ p1 a(v1 v1Var, String str, boolean z) {
        return new u1(v1Var, str, Boolean.valueOf(z));
    }

    public static /* synthetic */ p1 a(v1 v1Var, String str) {
        return new t1(v1Var, str, Double.valueOf(-3.0d));
    }

    public static /* synthetic */ p1 a(v1 v1Var, String str, String str2) {
        return new w1(v1Var, str, str2);
    }
}
