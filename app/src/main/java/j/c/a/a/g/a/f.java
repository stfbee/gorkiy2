package j.c.a.a.g.a;

import android.os.Bundle;
import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f {
    public final String a;
    public final String b;
    public final String c;
    public final long d;

    /* renamed from: e  reason: collision with root package name */
    public final long f1985e;

    /* renamed from: f  reason: collision with root package name */
    public final h f1986f;

    public f(r4 r4Var, String str, String str2, String str3, long j2, long j3, h hVar) {
        ResourcesFlusher.b(str2);
        ResourcesFlusher.b(str3);
        ResourcesFlusher.b(hVar);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j2;
        this.f1985e = j3;
        if (j3 != 0 && j3 > j2) {
            r4Var.a().f2047i.a("Event created with reverse previous/current timestamps. appId, name", n3.a(str2), n3.a(str3));
        }
        this.f1986f = hVar;
    }

    public final f a(r4 r4Var, long j2) {
        return new f(r4Var, this.c, this.a, this.b, this.d, j2, this.f1986f);
    }

    public final String toString() {
        String str = this.a;
        String str2 = this.b;
        String valueOf = String.valueOf(this.f1986f);
        StringBuilder sb = new StringBuilder(valueOf.length() + outline.a(str2, outline.a(str, 33)));
        sb.append("Event{appId='");
        sb.append(str);
        sb.append("', name='");
        sb.append(str2);
        sb.append("', params=");
        sb.append(valueOf);
        sb.append('}');
        return sb.toString();
    }

    public f(r4 r4Var, String str, String str2, String str3, long j2, long j3, Bundle bundle) {
        h hVar;
        ResourcesFlusher.b(str2);
        ResourcesFlusher.b(str3);
        this.a = str2;
        this.b = str3;
        this.c = TextUtils.isEmpty(str) ? null : str;
        this.d = j2;
        this.f1985e = j3;
        if (j3 != 0 && j3 > j2) {
            r4Var.a().f2047i.a("Event created with reverse previous/current timestamps. appId", n3.a(str2));
        }
        if (bundle == null || bundle.isEmpty()) {
            hVar = new h(new Bundle());
        } else {
            Bundle bundle2 = new Bundle(bundle);
            Iterator<String> it = bundle2.keySet().iterator();
            while (it.hasNext()) {
                String next = it.next();
                if (next == null) {
                    r4Var.a().f2046f.a("Param name can't be null");
                    it.remove();
                } else {
                    Object a2 = r4Var.n().a(next, bundle2.get(next));
                    if (a2 == null) {
                        r4Var.a().f2047i.a("Param value can't be null", r4Var.o().b(next));
                        it.remove();
                    } else {
                        r4Var.n().a(bundle2, next, a2);
                    }
                }
            }
            hVar = new h(bundle2);
        }
        this.f1986f = hVar;
    }
}
