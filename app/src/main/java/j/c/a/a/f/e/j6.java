package j.c.a.a.f.e;

import java.util.ListIterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class j6 implements ListIterator<String> {
    public ListIterator<String> b = this.d.b.listIterator(this.c);
    public final /* synthetic */ int c;
    public final /* synthetic */ k6 d;

    public j6(k6 k6Var, int i2) {
        this.d = k6Var;
        this.c = i2;
    }

    public final /* synthetic */ void add(Object obj) {
        throw new UnsupportedOperationException();
    }

    public final boolean hasNext() {
        return this.b.hasNext();
    }

    public final boolean hasPrevious() {
        return this.b.hasPrevious();
    }

    public final /* synthetic */ Object next() {
        return this.b.next();
    }

    public final int nextIndex() {
        return this.b.nextIndex();
    }

    public final /* synthetic */ Object previous() {
        return this.b.previous();
    }

    public final int previousIndex() {
        return this.b.previousIndex();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }

    public final /* synthetic */ void set(Object obj) {
        throw new UnsupportedOperationException();
    }
}
