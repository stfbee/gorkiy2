package j.c.a.a.f.e;

import java.util.Arrays;
import java.util.Collection;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class u4 extends r2<Long> implements f4, p5, RandomAccess {

    /* renamed from: e  reason: collision with root package name */
    public static final u4 f1909e;
    public long[] c;
    public int d;

    static {
        u4 u4Var = new u4(new long[0], 0);
        f1909e = u4Var;
        super.b = false;
    }

    public u4() {
        this.c = new long[10];
        this.d = 0;
    }

    public final void a(long j2) {
        c();
        int i2 = this.d;
        long[] jArr = this.c;
        if (i2 == jArr.length) {
            long[] jArr2 = new long[(((i2 * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i2);
            this.c = jArr2;
        }
        long[] jArr3 = this.c;
        int i3 = this.d;
        this.d = i3 + 1;
        jArr3[i3] = j2;
    }

    public final /* synthetic */ void add(int i2, Object obj) {
        int i3;
        long longValue = ((Long) obj).longValue();
        c();
        if (i2 < 0 || i2 > (i3 = this.d)) {
            throw new IndexOutOfBoundsException(e(i2));
        }
        long[] jArr = this.c;
        if (i3 < jArr.length) {
            System.arraycopy(jArr, i2, jArr, i2 + 1, i3 - i2);
        } else {
            long[] jArr2 = new long[(((i3 * 3) / 2) + 1)];
            System.arraycopy(jArr, 0, jArr2, 0, i2);
            System.arraycopy(this.c, i2, jArr2, i2 + 1, this.d - i2);
            this.c = jArr2;
        }
        this.c[i2] = longValue;
        this.d++;
        this.modCount++;
    }

    public final boolean addAll(Collection<? extends Long> collection) {
        c();
        y3.a(collection);
        if (!(collection instanceof u4)) {
            return super.addAll(collection);
        }
        u4 u4Var = (u4) collection;
        int i2 = u4Var.d;
        if (i2 == 0) {
            return false;
        }
        int i3 = this.d;
        if (Integer.MAX_VALUE - i3 >= i2) {
            int i4 = i3 + i2;
            long[] jArr = this.c;
            if (i4 > jArr.length) {
                this.c = Arrays.copyOf(jArr, i4);
            }
            System.arraycopy(u4Var.c, 0, this.c, this.d, u4Var.d);
            this.d = i4;
            this.modCount++;
            return true;
        }
        throw new OutOfMemoryError();
    }

    public final long b(int i2) {
        d(i2);
        return this.c[i2];
    }

    /* renamed from: c */
    public final f4 a(int i2) {
        if (i2 >= this.d) {
            return new u4(Arrays.copyOf(this.c, i2), this.d);
        }
        throw new IllegalArgumentException();
    }

    public final void d(int i2) {
        if (i2 < 0 || i2 >= this.d) {
            throw new IndexOutOfBoundsException(e(i2));
        }
    }

    public final String e(int i2) {
        int i3 = this.d;
        StringBuilder sb = new StringBuilder(35);
        sb.append("Index:");
        sb.append(i2);
        sb.append(", Size:");
        sb.append(i3);
        return sb.toString();
    }

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof u4)) {
            return super.equals(obj);
        }
        u4 u4Var = (u4) obj;
        if (this.d != u4Var.d) {
            return false;
        }
        long[] jArr = u4Var.c;
        for (int i2 = 0; i2 < this.d; i2++) {
            if (this.c[i2] != jArr[i2]) {
                return false;
            }
        }
        return true;
    }

    public final /* synthetic */ Object get(int i2) {
        d(i2);
        return Long.valueOf(this.c[i2]);
    }

    public final int hashCode() {
        int i2 = 1;
        for (int i3 = 0; i3 < this.d; i3++) {
            i2 = (i2 * 31) + y3.a(this.c[i3]);
        }
        return i2;
    }

    public final boolean remove(Object obj) {
        c();
        for (int i2 = 0; i2 < this.d; i2++) {
            if (obj.equals(Long.valueOf(this.c[i2]))) {
                long[] jArr = this.c;
                System.arraycopy(jArr, i2 + 1, jArr, i2, (this.d - i2) - 1);
                this.d--;
                this.modCount++;
                return true;
            }
        }
        return false;
    }

    public final void removeRange(int i2, int i3) {
        c();
        if (i3 >= i2) {
            long[] jArr = this.c;
            System.arraycopy(jArr, i3, jArr, i2, this.d - i3);
            this.d -= i3 - i2;
            this.modCount++;
            return;
        }
        throw new IndexOutOfBoundsException("toIndex < fromIndex");
    }

    public final /* synthetic */ Object set(int i2, Object obj) {
        long longValue = ((Long) obj).longValue();
        c();
        d(i2);
        long[] jArr = this.c;
        long j2 = jArr[i2];
        jArr[i2] = longValue;
        return Long.valueOf(j2);
    }

    public final int size() {
        return this.d;
    }

    public u4(long[] jArr, int i2) {
        this.c = jArr;
        this.d = i2;
    }

    public final /* synthetic */ Object remove(int i2) {
        c();
        d(i2);
        long[] jArr = this.c;
        long j2 = jArr[i2];
        int i3 = this.d;
        if (i2 < i3 - 1) {
            System.arraycopy(jArr, i2 + 1, jArr, i2, (i3 - i2) - 1);
        }
        this.d--;
        this.modCount++;
        return Long.valueOf(j2);
    }

    public final /* synthetic */ boolean add(Object obj) {
        a(((Long) obj).longValue());
        return true;
    }
}
