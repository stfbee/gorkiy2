package j.c.a.a.c.k.e;

public final class f<L> {
    public final L a;

    public final boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof f) || ((f) obj).a != null) {
            return false;
        }
        throw null;
    }

    public final int hashCode() {
        System.identityHashCode(null);
        throw null;
    }
}
