package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h4 implements Runnable {
    public final /* synthetic */ r4 b;
    public final /* synthetic */ n3 c;

    public h4(r4 r4Var, n3 n3Var) {
        this.b = r4Var;
        this.c = n3Var;
    }

    public final void run() {
        c4 c4Var = this.b.w;
        if (c4Var == null) {
            this.c.f2046f.a("Install Referrer Reporter is null");
            return;
        }
        r4 r4Var = c4Var.a;
        if (r4Var != null) {
            c4Var.a(r4Var.a.getPackageName());
            return;
        }
        throw null;
    }
}
