package j.c.a.a.f.e;

import android.util.Log;
import j.a.a.a.outline;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class u1 extends p1<Boolean> {
    public u1(v1 v1Var, String str, Boolean bool) {
        super(v1Var, str, bool, null);
    }

    public final /* synthetic */ Object a(Object obj) {
        if (obj instanceof Boolean) {
            return (Boolean) obj;
        }
        if (obj instanceof String) {
            String str = (String) obj;
            if (d1.c.matcher(str).matches()) {
                return true;
            }
            if (d1.d.matcher(str).matches()) {
                return false;
            }
        }
        String a = super.a();
        String valueOf = String.valueOf(obj);
        StringBuilder sb = new StringBuilder(valueOf.length() + outline.a(a, 28));
        sb.append("Invalid boolean value for ");
        sb.append(a);
        sb.append(": ");
        sb.append(valueOf);
        Log.e("PhenotypeFlag", sb.toString());
        return null;
    }
}
