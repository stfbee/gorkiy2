package j.c.a.a.f.e;

import android.content.ContentResolver;
import android.net.Uri;
import java.util.HashMap;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.regex.Pattern;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class d1 {
    public static final Uri a = Uri.parse("content://com.google.android.gsf.gservices");
    public static final Uri b = Uri.parse("content://com.google.android.gsf.gservices/prefix");
    public static final Pattern c = Pattern.compile("^(1|true|t|on|yes|y)$", 2);
    public static final Pattern d = Pattern.compile("^(0|false|f|off|no|n)$", 2);

    /* renamed from: e  reason: collision with root package name */
    public static final AtomicBoolean f1836e = new AtomicBoolean();

    /* renamed from: f  reason: collision with root package name */
    public static HashMap<String, String> f1837f;
    public static final HashMap<String, Boolean> g = new HashMap<>();
    public static final HashMap<String, Integer> h = new HashMap<>();

    /* renamed from: i  reason: collision with root package name */
    public static final HashMap<String, Long> f1838i = new HashMap<>();

    /* renamed from: j  reason: collision with root package name */
    public static final HashMap<String, Float> f1839j = new HashMap<>();

    /* renamed from: k  reason: collision with root package name */
    public static Object f1840k;

    /* renamed from: l  reason: collision with root package name */
    public static boolean f1841l;

    /* renamed from: m  reason: collision with root package name */
    public static String[] f1842m = new String[0];

    public static void a(ContentResolver contentResolver) {
        if (f1837f == null) {
            f1836e.set(false);
            f1837f = new HashMap<>();
            f1840k = new Object();
            f1841l = false;
            contentResolver.registerContentObserver(a, true, new c1());
        } else if (f1836e.getAndSet(false)) {
            f1837f.clear();
            g.clear();
            h.clear();
            f1838i.clear();
            f1839j.clear();
            f1840k = new Object();
            f1841l = false;
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:33:0x007e, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:35:0x0080, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:41:0x008a, code lost:
        r14 = r14.query(j.c.a.a.f.e.d1.a, null, null, new java.lang.String[]{r15}, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x0098, code lost:
        if (r14 != null) goto L_0x00a0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:43:0x009a, code lost:
        if (r14 == null) goto L_0x009f;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:44:0x009c, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:45:0x009f, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:48:0x00a4, code lost:
        if (r14.moveToFirst() != false) goto L_0x00ad;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:49:0x00a6, code lost:
        a(r1, r15, null);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:51:0x00ac, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:53:?, code lost:
        r0 = r14.getString(1);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00b1, code lost:
        if (r0 == null) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:56:0x00b7, code lost:
        if (r0.equals(null) == false) goto L_0x00ba;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:57:0x00b9, code lost:
        r0 = null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x00ba, code lost:
        a(r1, r15, r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:59:0x00bd, code lost:
        if (r0 == null) goto L_0x00c0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:60:0x00bf, code lost:
        r3 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:61:0x00c0, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x00c3, code lost:
        return r3;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:63:0x00c4, code lost:
        r15 = move-exception;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:64:0x00c5, code lost:
        r14.close();
     */
    /* JADX WARNING: Code restructure failed: missing block: B:65:0x00c8, code lost:
        throw r15;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:9:0x001d, code lost:
        return r3;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static java.lang.String a(android.content.ContentResolver r14, java.lang.String r15) {
        /*
            java.lang.Class<j.c.a.a.f.e.d1> r0 = j.c.a.a.f.e.d1.class
            monitor-enter(r0)
            a(r14)     // Catch:{ all -> 0x00c9 }
            java.lang.Object r1 = j.c.a.a.f.e.d1.f1840k     // Catch:{ all -> 0x00c9 }
            java.util.HashMap<java.lang.String, java.lang.String> r2 = j.c.a.a.f.e.d1.f1837f     // Catch:{ all -> 0x00c9 }
            boolean r2 = r2.containsKey(r15)     // Catch:{ all -> 0x00c9 }
            r3 = 0
            if (r2 == 0) goto L_0x001e
            java.util.HashMap<java.lang.String, java.lang.String> r14 = j.c.a.a.f.e.d1.f1837f     // Catch:{ all -> 0x00c9 }
            java.lang.Object r14 = r14.get(r15)     // Catch:{ all -> 0x00c9 }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ all -> 0x00c9 }
            if (r14 == 0) goto L_0x001c
            r3 = r14
        L_0x001c:
            monitor-exit(r0)     // Catch:{ all -> 0x00c9 }
            return r3
        L_0x001e:
            java.lang.String[] r2 = j.c.a.a.f.e.d1.f1842m     // Catch:{ all -> 0x00c9 }
            int r4 = r2.length     // Catch:{ all -> 0x00c9 }
            r5 = 0
            r6 = 0
        L_0x0023:
            r7 = 1
            if (r6 >= r4) goto L_0x0089
            r8 = r2[r6]     // Catch:{ all -> 0x00c9 }
            boolean r8 = r15.startsWith(r8)     // Catch:{ all -> 0x00c9 }
            if (r8 == 0) goto L_0x0086
            boolean r1 = j.c.a.a.f.e.d1.f1841l     // Catch:{ all -> 0x00c9 }
            if (r1 == 0) goto L_0x003a
            java.util.HashMap<java.lang.String, java.lang.String> r1 = j.c.a.a.f.e.d1.f1837f     // Catch:{ all -> 0x00c9 }
            boolean r1 = r1.isEmpty()     // Catch:{ all -> 0x00c9 }
            if (r1 == 0) goto L_0x007f
        L_0x003a:
            java.lang.String[] r12 = j.c.a.a.f.e.d1.f1842m     // Catch:{ all -> 0x00c9 }
            java.util.HashMap<java.lang.String, java.lang.String> r1 = j.c.a.a.f.e.d1.f1837f     // Catch:{ all -> 0x00c9 }
            android.net.Uri r9 = j.c.a.a.f.e.d1.b     // Catch:{ all -> 0x00c9 }
            r10 = 0
            r11 = 0
            r13 = 0
            r8 = r14
            android.database.Cursor r14 = r8.query(r9, r10, r11, r12, r13)     // Catch:{ all -> 0x00c9 }
            java.util.TreeMap r2 = new java.util.TreeMap     // Catch:{ all -> 0x00c9 }
            r2.<init>()     // Catch:{ all -> 0x00c9 }
            if (r14 != 0) goto L_0x0050
            goto L_0x0065
        L_0x0050:
            boolean r4 = r14.moveToNext()     // Catch:{ all -> 0x0081 }
            if (r4 == 0) goto L_0x0062
            java.lang.String r4 = r14.getString(r5)     // Catch:{ all -> 0x0081 }
            java.lang.String r6 = r14.getString(r7)     // Catch:{ all -> 0x0081 }
            r2.put(r4, r6)     // Catch:{ all -> 0x0081 }
            goto L_0x0050
        L_0x0062:
            r14.close()     // Catch:{ all -> 0x00c9 }
        L_0x0065:
            r1.putAll(r2)     // Catch:{ all -> 0x00c9 }
            j.c.a.a.f.e.d1.f1841l = r7     // Catch:{ all -> 0x00c9 }
            java.util.HashMap<java.lang.String, java.lang.String> r14 = j.c.a.a.f.e.d1.f1837f     // Catch:{ all -> 0x00c9 }
            boolean r14 = r14.containsKey(r15)     // Catch:{ all -> 0x00c9 }
            if (r14 == 0) goto L_0x007f
            java.util.HashMap<java.lang.String, java.lang.String> r14 = j.c.a.a.f.e.d1.f1837f     // Catch:{ all -> 0x00c9 }
            java.lang.Object r14 = r14.get(r15)     // Catch:{ all -> 0x00c9 }
            java.lang.String r14 = (java.lang.String) r14     // Catch:{ all -> 0x00c9 }
            if (r14 == 0) goto L_0x007d
            r3 = r14
        L_0x007d:
            monitor-exit(r0)     // Catch:{ all -> 0x00c9 }
            return r3
        L_0x007f:
            monitor-exit(r0)     // Catch:{ all -> 0x00c9 }
            return r3
        L_0x0081:
            r15 = move-exception
            r14.close()     // Catch:{ all -> 0x00c9 }
            throw r15     // Catch:{ all -> 0x00c9 }
        L_0x0086:
            int r6 = r6 + 1
            goto L_0x0023
        L_0x0089:
            monitor-exit(r0)     // Catch:{ all -> 0x00c9 }
            android.net.Uri r9 = j.c.a.a.f.e.d1.a
            r10 = 0
            r11 = 0
            java.lang.String[] r12 = new java.lang.String[r7]
            r12[r5] = r15
            r13 = 0
            r8 = r14
            android.database.Cursor r14 = r8.query(r9, r10, r11, r12, r13)
            if (r14 != 0) goto L_0x00a0
            if (r14 == 0) goto L_0x009f
            r14.close()
        L_0x009f:
            return r3
        L_0x00a0:
            boolean r0 = r14.moveToFirst()     // Catch:{ all -> 0x00c4 }
            if (r0 != 0) goto L_0x00ad
            a(r1, r15, r3)     // Catch:{ all -> 0x00c4 }
            r14.close()
            return r3
        L_0x00ad:
            java.lang.String r0 = r14.getString(r7)     // Catch:{ all -> 0x00c4 }
            if (r0 == 0) goto L_0x00ba
            boolean r2 = r0.equals(r3)     // Catch:{ all -> 0x00c4 }
            if (r2 == 0) goto L_0x00ba
            r0 = r3
        L_0x00ba:
            a(r1, r15, r0)     // Catch:{ all -> 0x00c4 }
            if (r0 == 0) goto L_0x00c0
            r3 = r0
        L_0x00c0:
            r14.close()
            return r3
        L_0x00c4:
            r15 = move-exception
            r14.close()
            throw r15
        L_0x00c9:
            r14 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x00c9 }
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.d1.a(android.content.ContentResolver, java.lang.String):java.lang.String");
    }

    public static void a(Object obj, String str, String str2) {
        synchronized (d1.class) {
            if (obj == f1840k) {
                f1837f.put(str, str2);
            }
        }
    }
}
