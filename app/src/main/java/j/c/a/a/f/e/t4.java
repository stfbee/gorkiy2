package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class t4 implements v5 {
    public static final c5 b = new w4();
    public final c5 a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T
     arg types: [j.c.a.a.f.e.v4, java.lang.String]
     candidates:
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.Object):java.lang.Object
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T */
    public t4() {
        c5 c5Var;
        c5[] c5VarArr = new c5[2];
        c5VarArr[0] = v3.a;
        try {
            c5Var = (c5) Class.forName("com.google.protobuf.DescriptorMessageInfoFactory").getDeclaredMethod("getInstance", new Class[0]).invoke(null, new Object[0]);
        } catch (Exception unused) {
            c5Var = b;
        }
        c5VarArr[1] = c5Var;
        v4 v4Var = new v4(c5VarArr);
        y3.a((Object) v4Var, "messageInfoFactory");
        this.a = v4Var;
    }
}
