package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class fa implements ga {
    public static final p1<Boolean> a;
    public static final p1<Boolean> b;
    public static final p1<Boolean> c;
    public static final p1<Boolean> d;

    /* renamed from: e  reason: collision with root package name */
    public static final p1<Boolean> f1849e;

    /* renamed from: f  reason: collision with root package name */
    public static final p1<Boolean> f1850f;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1
     arg types: [j.c.a.a.f.e.v1, java.lang.String, int]
     candidates:
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, long):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, java.lang.String):j.c.a.a.f.e.p1
      j.c.a.a.f.e.p1.a(j.c.a.a.f.e.v1, java.lang.String, boolean):j.c.a.a.f.e.p1 */
    static {
        v1 v1Var = new v1(q1.a("com.google.android.gms.measurement"));
        a = p1.a(v1Var, "measurement.service.audience.scoped_filters_v27", false);
        b = p1.a(v1Var, "measurement.service.audience.session_scoped_user_engagement", false);
        c = p1.a(v1Var, "measurement.service.audience.session_scoped_event_aggregates", false);
        d = p1.a(v1Var, "measurement.service.audience.use_bundle_timestamp_for_property_filters", false);
        p1.a(v1Var, "measurement.id.scoped_audience_filters", 0);
        f1849e = p1.a(v1Var, "measurement.service.audience.not_prepend_timestamps_for_sequence_session_scoped_filters", false);
        f1850f = p1.a(v1Var, "measurement.service.audience.remove_disabled_session_scoped_user_engagement", false);
    }

    public final boolean a() {
        return a.b().booleanValue();
    }

    public final boolean b() {
        return b.b().booleanValue();
    }

    public final boolean c() {
        return c.b().booleanValue();
    }

    public final boolean d() {
        return d.b().booleanValue();
    }

    public final boolean e() {
        return f1849e.b().booleanValue();
    }

    public final boolean g() {
        return f1850f.b().booleanValue();
    }
}
