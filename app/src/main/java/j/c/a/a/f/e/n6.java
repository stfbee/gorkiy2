package j.c.a.a.f.e;

import java.util.Iterator;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n6 implements Iterator<String> {
    public Iterator<String> b = this.c.b.iterator();
    public final /* synthetic */ k6 c;

    public n6(k6 k6Var) {
        this.c = k6Var;
    }

    public final boolean hasNext() {
        return this.b.hasNext();
    }

    public final /* synthetic */ Object next() {
        return this.b.next();
    }

    public final void remove() {
        throw new UnsupportedOperationException();
    }
}
