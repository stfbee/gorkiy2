package j.c.a.a.f.e;

import j.c.a.a.d.b;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class l extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ int f1870f = 5;
    public final /* synthetic */ String g;
    public final /* synthetic */ Object h;

    /* renamed from: i  reason: collision with root package name */
    public final /* synthetic */ Object f1871i;

    /* renamed from: j  reason: collision with root package name */
    public final /* synthetic */ Object f1872j;

    /* renamed from: k  reason: collision with root package name */
    public final /* synthetic */ pb f1873k;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public l(pb pbVar, String str, Object obj) {
        super(false);
        this.f1873k = pbVar;
        this.g = str;
        this.h = obj;
        this.f1871i = null;
        this.f1872j = null;
    }

    public final void a() {
        this.f1873k.g.logHealthData(this.f1870f, this.g, new b(this.h), new b(this.f1871i), new b(this.f1872j));
    }
}
