package j.c.a.a.f.e;

import android.app.Activity;
import j.c.a.a.d.b;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class w extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Activity f1925f;
    public final /* synthetic */ m9 g;
    public final /* synthetic */ pb.b h;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public w(pb.b bVar, Activity activity, m9 m9Var) {
        super(true);
        this.h = bVar;
        this.f1925f = activity;
        this.g = m9Var;
    }

    public final void a() {
        pb.this.g.onActivitySaveInstanceState(new b(this.f1925f), this.g, super.c);
    }
}
