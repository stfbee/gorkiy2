package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class m8 extends b {

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ q8 f2042e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ n8 f2043f;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public m8(n8 n8Var, p5 p5Var, q8 q8Var) {
        super(p5Var);
        this.f2043f = n8Var;
        this.f2042e = q8Var;
    }

    public final void a() {
        this.f2043f.u();
        this.f2043f.a().f2052n.a("Starting upload from DelayedRunnable");
        this.f2042e.n();
    }
}
