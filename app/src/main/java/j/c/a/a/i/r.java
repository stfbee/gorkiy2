package j.c.a.a.i;

import java.util.concurrent.Executor;
import javax.annotation.concurrent.GuardedBy;

public final class r<TResult> implements v<TResult> {
    public final Executor a;
    public final Object b = new Object();
    @GuardedBy("mLock")
    public d<? super TResult> c;

    public r(Executor executor, d<? super TResult> dVar) {
        this.a = executor;
        this.c = dVar;
    }

    public final void a(e<TResult> eVar) {
        if (eVar.d()) {
            synchronized (this.b) {
                if (this.c != null) {
                    this.a.execute(new s(this, eVar));
                }
            }
        }
    }
}
