package j.c.a.a.g.a;

import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g3 extends e5 {
    public String c;
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public int f1991e;

    /* renamed from: f  reason: collision with root package name */
    public String f1992f;
    public long g;
    public long h;

    /* renamed from: i  reason: collision with root package name */
    public long f1993i;

    /* renamed from: j  reason: collision with root package name */
    public List<String> f1994j;

    /* renamed from: k  reason: collision with root package name */
    public int f1995k;

    /* renamed from: l  reason: collision with root package name */
    public String f1996l;

    /* renamed from: m  reason: collision with root package name */
    public String f1997m;

    public g3(r4 r4Var, long j2) {
        super(r4Var);
        this.f1993i = j2;
    }

    /* JADX WARNING: Removed duplicated region for block: B:106:0x0232  */
    /* JADX WARNING: Removed duplicated region for block: B:116:0x0263  */
    /* JADX WARNING: Removed duplicated region for block: B:119:0x0269  */
    /* JADX WARNING: Removed duplicated region for block: B:121:0x0274  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x009f  */
    /* JADX WARNING: Removed duplicated region for block: B:37:0x00b7  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x00c7  */
    /* JADX WARNING: Removed duplicated region for block: B:47:0x00ea  */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x017b A[Catch:{ IllegalStateException -> 0x01ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:74:0x017c A[Catch:{ IllegalStateException -> 0x01ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:77:0x0185 A[Catch:{ IllegalStateException -> 0x01ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:83:0x01aa A[Catch:{ IllegalStateException -> 0x01ba }] */
    /* JADX WARNING: Removed duplicated region for block: B:88:0x01da  */
    /* JADX WARNING: Removed duplicated region for block: B:98:0x020c A[SYNTHETIC, Splitter:B:98:0x020c] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void v() {
        /*
            r13 = this;
            j.c.a.a.g.a.r4 r0 = r13.a
            android.content.Context r0 = r0.a
            java.lang.String r0 = r0.getPackageName()
            j.c.a.a.g.a.r4 r1 = r13.a
            android.content.Context r1 = r1.a
            android.content.pm.PackageManager r1 = r1.getPackageManager()
            r2 = 0
            java.lang.String r3 = "Unknown"
            java.lang.String r4 = ""
            java.lang.String r5 = "unknown"
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            if (r1 != 0) goto L_0x002b
            j.c.a.a.g.a.n3 r7 = r13.a()
            j.c.a.a.g.a.p3 r7 = r7.f2046f
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r0)
            java.lang.String r9 = "PackageManager is null, app identity information might be inaccurate. appId"
            r7.a(r9, r8)
            goto L_0x0086
        L_0x002b:
            java.lang.String r5 = r1.getInstallerPackageName(r0)     // Catch:{ IllegalArgumentException -> 0x0030 }
            goto L_0x003f
        L_0x0030:
            j.c.a.a.g.a.n3 r7 = r13.a()
            j.c.a.a.g.a.p3 r7 = r7.f2046f
            java.lang.Object r8 = j.c.a.a.g.a.n3.a(r0)
            java.lang.String r9 = "Error retrieving app installer package name. appId"
            r7.a(r9, r8)
        L_0x003f:
            if (r5 != 0) goto L_0x0044
            java.lang.String r5 = "manual_install"
            goto L_0x004d
        L_0x0044:
            java.lang.String r7 = "com.android.vending"
            boolean r7 = r7.equals(r5)
            if (r7 == 0) goto L_0x004d
            r5 = r4
        L_0x004d:
            j.c.a.a.g.a.r4 r7 = r13.a     // Catch:{ NameNotFoundException -> 0x0075 }
            android.content.Context r7 = r7.a     // Catch:{ NameNotFoundException -> 0x0075 }
            java.lang.String r7 = r7.getPackageName()     // Catch:{ NameNotFoundException -> 0x0075 }
            android.content.pm.PackageInfo r7 = r1.getPackageInfo(r7, r2)     // Catch:{ NameNotFoundException -> 0x0075 }
            if (r7 == 0) goto L_0x0086
            android.content.pm.ApplicationInfo r8 = r7.applicationInfo     // Catch:{ NameNotFoundException -> 0x0075 }
            java.lang.CharSequence r8 = r1.getApplicationLabel(r8)     // Catch:{ NameNotFoundException -> 0x0075 }
            boolean r9 = android.text.TextUtils.isEmpty(r8)     // Catch:{ NameNotFoundException -> 0x0075 }
            if (r9 != 0) goto L_0x006c
            java.lang.String r8 = r8.toString()     // Catch:{ NameNotFoundException -> 0x0075 }
            goto L_0x006d
        L_0x006c:
            r8 = r3
        L_0x006d:
            java.lang.String r3 = r7.versionName     // Catch:{ NameNotFoundException -> 0x0072 }
            int r6 = r7.versionCode     // Catch:{ NameNotFoundException -> 0x0072 }
            goto L_0x0086
        L_0x0072:
            r7 = r3
            r3 = r8
            goto L_0x0076
        L_0x0075:
            r7 = r3
        L_0x0076:
            j.c.a.a.g.a.n3 r8 = r13.a()
            j.c.a.a.g.a.p3 r8 = r8.f2046f
            java.lang.Object r9 = j.c.a.a.g.a.n3.a(r0)
            java.lang.String r10 = "Error retrieving package info. appId, appName"
            r8.a(r10, r9, r3)
            r3 = r7
        L_0x0086:
            r13.c = r0
            r13.f1992f = r5
            r13.d = r3
            r13.f1991e = r6
            r5 = 0
            r13.g = r5
            j.c.a.a.g.a.r4 r3 = r13.a
            j.c.a.a.g.a.h9 r7 = r3.f2086f
            android.content.Context r3 = r3.a
            com.google.android.gms.common.api.Status r3 = j.c.a.a.c.k.e.c.a(r3)
            r7 = 1
            if (r3 == 0) goto L_0x00aa
            int r8 = r3.c
            if (r8 > 0) goto L_0x00a5
            r8 = 1
            goto L_0x00a6
        L_0x00a5:
            r8 = 0
        L_0x00a6:
            if (r8 == 0) goto L_0x00aa
            r8 = 1
            goto L_0x00ab
        L_0x00aa:
            r8 = 0
        L_0x00ab:
            j.c.a.a.g.a.r4 r9 = r13.a
            java.lang.String r9 = r9.b
            boolean r9 = android.text.TextUtils.isEmpty(r9)
            java.lang.String r10 = "am"
            if (r9 != 0) goto L_0x00c3
            j.c.a.a.g.a.r4 r9 = r13.a
            java.lang.String r9 = r9.c
            boolean r9 = r10.equals(r9)
            if (r9 == 0) goto L_0x00c3
            r9 = 1
            goto L_0x00c4
        L_0x00c3:
            r9 = 0
        L_0x00c4:
            r8 = r8 | r9
            if (r8 != 0) goto L_0x00e8
            if (r3 != 0) goto L_0x00d5
            j.c.a.a.g.a.n3 r3 = r13.a()
            j.c.a.a.g.a.p3 r3 = r3.f2046f
            java.lang.String r9 = "GoogleService failed to initialize (no status)"
            r3.a(r9)
            goto L_0x00e8
        L_0x00d5:
            j.c.a.a.g.a.n3 r9 = r13.a()
            j.c.a.a.g.a.p3 r9 = r9.f2046f
            int r11 = r3.c
            java.lang.Integer r11 = java.lang.Integer.valueOf(r11)
            java.lang.String r3 = r3.d
            java.lang.String r12 = "GoogleService failed to initialize, status"
            r9.a(r12, r11, r3)
        L_0x00e8:
            if (r8 == 0) goto L_0x014d
            j.c.a.a.g.a.r4 r3 = r13.a
            j.c.a.a.g.a.i9 r3 = r3.g
            java.lang.Boolean r3 = r3.r()
            j.c.a.a.g.a.r4 r8 = r13.a
            j.c.a.a.g.a.i9 r8 = r8.g
            boolean r8 = r8.q()
            if (r8 == 0) goto L_0x0110
            j.c.a.a.g.a.r4 r3 = r13.a
            boolean r3 = r3.p()
            if (r3 == 0) goto L_0x014d
            j.c.a.a.g.a.n3 r3 = r13.a()
            j.c.a.a.g.a.p3 r3 = r3.f2050l
            java.lang.String r8 = "Collection disabled with firebase_analytics_collection_deactivated=1"
            r3.a(r8)
            goto L_0x014d
        L_0x0110:
            if (r3 == 0) goto L_0x012c
            boolean r8 = r3.booleanValue()
            if (r8 != 0) goto L_0x012c
            j.c.a.a.g.a.r4 r3 = r13.a
            boolean r3 = r3.p()
            if (r3 == 0) goto L_0x014d
            j.c.a.a.g.a.n3 r3 = r13.a()
            j.c.a.a.g.a.p3 r3 = r3.f2050l
            java.lang.String r8 = "Collection disabled with firebase_analytics_collection_enabled=0"
            r3.a(r8)
            goto L_0x014d
        L_0x012c:
            if (r3 != 0) goto L_0x0140
            boolean r3 = j.c.a.a.c.k.e.c.b()
            if (r3 == 0) goto L_0x0140
            j.c.a.a.g.a.n3 r3 = r13.a()
            j.c.a.a.g.a.p3 r3 = r3.f2050l
            java.lang.String r8 = "Collection disabled with google_app_measurement_enable=0"
            r3.a(r8)
            goto L_0x014d
        L_0x0140:
            j.c.a.a.g.a.n3 r3 = r13.a()
            j.c.a.a.g.a.p3 r3 = r3.f2052n
            java.lang.String r8 = "Collection enabled"
            r3.a(r8)
            r3 = 1
            goto L_0x014e
        L_0x014d:
            r3 = 0
        L_0x014e:
            r13.f1996l = r4
            r13.f1997m = r4
            r13.h = r5
            j.c.a.a.g.a.r4 r5 = r13.a
            j.c.a.a.g.a.h9 r6 = r5.f2086f
            java.lang.String r5 = r5.b
            boolean r5 = android.text.TextUtils.isEmpty(r5)
            if (r5 != 0) goto L_0x0170
            j.c.a.a.g.a.r4 r5 = r13.a
            java.lang.String r5 = r5.c
            boolean r5 = r10.equals(r5)
            if (r5 == 0) goto L_0x0170
            j.c.a.a.g.a.r4 r5 = r13.a
            java.lang.String r5 = r5.b
            r13.f1997m = r5
        L_0x0170:
            r5 = 0
            java.lang.String r6 = j.c.a.a.c.k.e.c.a()     // Catch:{ IllegalStateException -> 0x01ba }
            boolean r8 = android.text.TextUtils.isEmpty(r6)     // Catch:{ IllegalStateException -> 0x01ba }
            if (r8 == 0) goto L_0x017c
            goto L_0x017d
        L_0x017c:
            r4 = r6
        L_0x017d:
            r13.f1996l = r4     // Catch:{ IllegalStateException -> 0x01ba }
            boolean r4 = android.text.TextUtils.isEmpty(r6)     // Catch:{ IllegalStateException -> 0x01ba }
            if (r4 != 0) goto L_0x01a8
            j.c.a.a.g.a.r4 r4 = r13.a     // Catch:{ IllegalStateException -> 0x01ba }
            android.content.Context r4 = r4.a     // Catch:{ IllegalStateException -> 0x01ba }
            i.b.k.ResourcesFlusher.b(r4)     // Catch:{ IllegalStateException -> 0x01ba }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ IllegalStateException -> 0x01ba }
            int r6 = j.c.a.a.c.i.common_google_play_services_unknown_issue     // Catch:{ IllegalStateException -> 0x01ba }
            java.lang.String r6 = r4.getResourcePackageName(r6)     // Catch:{ IllegalStateException -> 0x01ba }
            java.lang.String r8 = "admob_app_id"
            java.lang.String r9 = "string"
            int r6 = r4.getIdentifier(r8, r9, r6)     // Catch:{ IllegalStateException -> 0x01ba }
            if (r6 != 0) goto L_0x01a2
            r4 = r5
            goto L_0x01a6
        L_0x01a2:
            java.lang.String r4 = r4.getString(r6)     // Catch:{ IllegalStateException -> 0x01ba }
        L_0x01a6:
            r13.f1997m = r4     // Catch:{ IllegalStateException -> 0x01ba }
        L_0x01a8:
            if (r3 == 0) goto L_0x01ca
            j.c.a.a.g.a.n3 r3 = r13.a()     // Catch:{ IllegalStateException -> 0x01ba }
            j.c.a.a.g.a.p3 r3 = r3.f2052n     // Catch:{ IllegalStateException -> 0x01ba }
            java.lang.String r4 = "App package, google app id"
            java.lang.String r6 = r13.c     // Catch:{ IllegalStateException -> 0x01ba }
            java.lang.String r8 = r13.f1996l     // Catch:{ IllegalStateException -> 0x01ba }
            r3.a(r4, r6, r8)     // Catch:{ IllegalStateException -> 0x01ba }
            goto L_0x01ca
        L_0x01ba:
            r3 = move-exception
            j.c.a.a.g.a.n3 r4 = r13.a()
            j.c.a.a.g.a.p3 r4 = r4.f2046f
            java.lang.Object r0 = j.c.a.a.g.a.n3.a(r0)
            java.lang.String r6 = "getGoogleAppId or isMeasurementEnabled failed with exception. appId"
            r4.a(r6, r0, r3)
        L_0x01ca:
            r13.f1994j = r5
            j.c.a.a.g.a.r4 r0 = r13.a
            j.c.a.a.g.a.i9 r0 = r0.g
            java.lang.String r3 = r13.c
            j.c.a.a.g.a.b3<java.lang.Boolean> r4 = j.c.a.a.g.a.k.v0
            boolean r0 = r0.d(r3, r4)
            if (r0 == 0) goto L_0x0267
            j.c.a.a.g.a.r4 r0 = r13.a
            j.c.a.a.g.a.h9 r3 = r0.f2086f
            j.c.a.a.g.a.i9 r0 = r0.g
            java.lang.String r3 = "analytics.safelisted_events"
            if (r0 == 0) goto L_0x0266
            i.b.k.ResourcesFlusher.b(r3)
            android.os.Bundle r4 = r0.n()
            if (r4 != 0) goto L_0x01f9
            j.c.a.a.g.a.n3 r3 = r0.a()
            j.c.a.a.g.a.p3 r3 = r3.f2046f
            java.lang.String r4 = "Failed to load metadata: Metadata bundle is null"
            r3.a(r4)
            goto L_0x01ff
        L_0x01f9:
            boolean r6 = r4.containsKey(r3)
            if (r6 != 0) goto L_0x0201
        L_0x01ff:
            r3 = r5
            goto L_0x0209
        L_0x0201:
            int r3 = r4.getInt(r3)
            java.lang.Integer r3 = java.lang.Integer.valueOf(r3)
        L_0x0209:
            if (r3 != 0) goto L_0x020c
            goto L_0x0230
        L_0x020c:
            j.c.a.a.g.a.r4 r4 = r0.a     // Catch:{ NotFoundException -> 0x0224 }
            android.content.Context r4 = r4.a     // Catch:{ NotFoundException -> 0x0224 }
            android.content.res.Resources r4 = r4.getResources()     // Catch:{ NotFoundException -> 0x0224 }
            int r3 = r3.intValue()     // Catch:{ NotFoundException -> 0x0224 }
            java.lang.String[] r3 = r4.getStringArray(r3)     // Catch:{ NotFoundException -> 0x0224 }
            if (r3 != 0) goto L_0x021f
            goto L_0x0230
        L_0x021f:
            java.util.List r5 = java.util.Arrays.asList(r3)     // Catch:{ NotFoundException -> 0x0224 }
            goto L_0x0230
        L_0x0224:
            r3 = move-exception
            j.c.a.a.g.a.n3 r0 = r0.a()
            j.c.a.a.g.a.p3 r0 = r0.f2046f
            java.lang.String r4 = "Failed to load string array from metadata: resource not found"
            r0.a(r4, r3)
        L_0x0230:
            if (r5 == 0) goto L_0x0261
            int r0 = r5.size()
            if (r0 != 0) goto L_0x0244
            j.c.a.a.g.a.n3 r0 = r13.a()
            j.c.a.a.g.a.p3 r0 = r0.f2047i
            java.lang.String r3 = "Safelisted event list cannot be empty. Ignoring"
            r0.a(r3)
            goto L_0x0260
        L_0x0244:
            java.util.Iterator r0 = r5.iterator()
        L_0x0248:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0261
            java.lang.Object r3 = r0.next()
            java.lang.String r3 = (java.lang.String) r3
            j.c.a.a.g.a.y8 r4 = r13.k()
            java.lang.String r6 = "safelisted event"
            boolean r3 = r4.b(r6, r3)
            if (r3 != 0) goto L_0x0248
        L_0x0260:
            r7 = 0
        L_0x0261:
            if (r7 == 0) goto L_0x0267
            r13.f1994j = r5
            goto L_0x0267
        L_0x0266:
            throw r5
        L_0x0267:
            if (r1 == 0) goto L_0x0274
            j.c.a.a.g.a.r4 r0 = r13.a
            android.content.Context r0 = r0.a
            boolean r0 = j.c.a.a.c.n.c.a(r0)
            r13.f1995k = r0
            return
        L_0x0274:
            r13.f1995k = r2
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.g3.v():void");
    }

    public final boolean y() {
        return true;
    }
}
