package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import com.google.android.gms.internal.measurement.zzfn;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.c.a.a.f.e.x3;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import sun.misc.Unsafe;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h5<T> implements t5<T> {

    /* renamed from: q  reason: collision with root package name */
    public static final int[] f1853q = new int[0];

    /* renamed from: r  reason: collision with root package name */
    public static final Unsafe f1854r = m6.a();
    public final int[] a;
    public final Object[] b;
    public final int c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final f5 f1855e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f1856f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final int[] f1857i;

    /* renamed from: j  reason: collision with root package name */
    public final int f1858j;

    /* renamed from: k  reason: collision with root package name */
    public final int f1859k;

    /* renamed from: l  reason: collision with root package name */
    public final l5 f1860l;

    /* renamed from: m  reason: collision with root package name */
    public final p4 f1861m;

    /* renamed from: n  reason: collision with root package name */
    public final g6<?, ?> f1862n;

    /* renamed from: o  reason: collision with root package name */
    public final l3<?> f1863o;

    /* renamed from: p  reason: collision with root package name */
    public final y4 f1864p;

    public h5(int[] iArr, Object[] objArr, int i2, int i3, f5 f5Var, boolean z, int[] iArr2, int i4, int i5, l5 l5Var, p4 p4Var, g6 g6Var, l3 l3Var, y4 y4Var) {
        this.a = iArr;
        this.b = objArr;
        this.c = i2;
        this.d = i3;
        boolean z2 = f5Var instanceof x3;
        this.g = z;
        this.f1856f = l3Var != null && (f5Var instanceof x3.b);
        this.h = false;
        this.f1857i = iArr2;
        this.f1858j = i4;
        this.f1859k = i5;
        this.f1860l = l5Var;
        this.f1861m = p4Var;
        this.f1862n = g6Var;
        this.f1863o = l3Var;
        this.f1855e = f5Var;
        this.f1864p = y4Var;
    }

    public static <T> h5<T> a(Class<T> cls, d5 d5Var, l5 l5Var, p4 p4Var, g6<?, ?> g6Var, l3<?> l3Var, y4 y4Var) {
        int i2;
        int i3;
        char c2;
        char c3;
        int[] iArr;
        char c4;
        char c5;
        char c6;
        char c7;
        int i4;
        boolean z;
        int i5;
        int[] iArr2;
        int i6;
        r5 r5Var;
        String str;
        int i7;
        int i8;
        int i9;
        int i10;
        Field field;
        char charAt;
        int i11;
        Field field2;
        Field field3;
        int i12;
        char charAt2;
        int i13;
        char charAt3;
        int i14;
        char charAt4;
        int i15;
        int i16;
        char charAt5;
        int i17;
        char charAt6;
        int i18;
        char charAt7;
        int i19;
        char charAt8;
        int i20;
        char charAt9;
        int i21;
        char charAt10;
        int i22;
        char charAt11;
        int i23;
        char charAt12;
        int i24;
        char charAt13;
        char charAt14;
        d5 d5Var2 = d5Var;
        if (d5Var2 instanceof r5) {
            r5 r5Var2 = (r5) d5Var2;
            char c8 = 0;
            boolean z2 = ((r5Var2.d & 1) == 1 ? (char) 1 : 2) == 2;
            String str2 = r5Var2.b;
            int length = str2.length();
            char charAt15 = str2.charAt(0);
            if (charAt15 >= 55296) {
                char c9 = charAt15 & 8191;
                int i25 = 1;
                int i26 = 13;
                while (true) {
                    i2 = i25 + 1;
                    charAt14 = str2.charAt(i25);
                    if (charAt14 < 55296) {
                        break;
                    }
                    c9 |= (charAt14 & 8191) << i26;
                    i26 += 13;
                    i25 = i2;
                }
                charAt15 = c9 | (charAt14 << i26);
            } else {
                i2 = 1;
            }
            int i27 = i2 + 1;
            char charAt16 = str2.charAt(i2);
            if (charAt16 >= 55296) {
                char c10 = charAt16 & 8191;
                int i28 = 13;
                while (true) {
                    i24 = i27 + 1;
                    charAt13 = str2.charAt(i27);
                    if (charAt13 < 55296) {
                        break;
                    }
                    c10 |= (charAt13 & 8191) << i28;
                    i28 += 13;
                    i27 = i24;
                }
                charAt16 = c10 | (charAt13 << i28);
                i27 = i24;
            }
            if (charAt16 == 0) {
                iArr = f1853q;
                c6 = 0;
                c5 = 0;
                c4 = 0;
                c3 = 0;
                c2 = 0;
                i3 = 0;
            } else {
                int i29 = i27 + 1;
                char charAt17 = str2.charAt(i27);
                if (charAt17 >= 55296) {
                    char c11 = charAt17 & 8191;
                    int i30 = 13;
                    while (true) {
                        i23 = i29 + 1;
                        charAt12 = str2.charAt(i29);
                        if (charAt12 < 55296) {
                            break;
                        }
                        c11 |= (charAt12 & 8191) << i30;
                        i30 += 13;
                        i29 = i23;
                    }
                    charAt17 = c11 | (charAt12 << i30);
                    i29 = i23;
                }
                int i31 = i29 + 1;
                char charAt18 = str2.charAt(i29);
                if (charAt18 >= 55296) {
                    char c12 = charAt18 & 8191;
                    int i32 = 13;
                    while (true) {
                        i22 = i31 + 1;
                        charAt11 = str2.charAt(i31);
                        if (charAt11 < 55296) {
                            break;
                        }
                        c12 |= (charAt11 & 8191) << i32;
                        i32 += 13;
                        i31 = i22;
                    }
                    charAt18 = c12 | (charAt11 << i32);
                    i31 = i22;
                }
                int i33 = i31 + 1;
                c4 = str2.charAt(i31);
                if (c4 >= 55296) {
                    char c13 = c4 & 8191;
                    int i34 = 13;
                    while (true) {
                        i21 = i33 + 1;
                        charAt10 = str2.charAt(i33);
                        if (charAt10 < 55296) {
                            break;
                        }
                        c13 |= (charAt10 & 8191) << i34;
                        i34 += 13;
                        i33 = i21;
                    }
                    c4 = c13 | (charAt10 << i34);
                    i33 = i21;
                }
                int i35 = i33 + 1;
                char charAt19 = str2.charAt(i33);
                if (charAt19 >= 55296) {
                    char c14 = charAt19 & 8191;
                    int i36 = 13;
                    while (true) {
                        i20 = i35 + 1;
                        charAt9 = str2.charAt(i35);
                        if (charAt9 < 55296) {
                            break;
                        }
                        c14 |= (charAt9 & 8191) << i36;
                        i36 += 13;
                        i35 = i20;
                    }
                    charAt19 = c14 | (charAt9 << i36);
                    i35 = i20;
                }
                int i37 = i35 + 1;
                c3 = str2.charAt(i35);
                if (c3 >= 55296) {
                    char c15 = c3 & 8191;
                    int i38 = 13;
                    while (true) {
                        i19 = i37 + 1;
                        charAt8 = str2.charAt(i37);
                        if (charAt8 < 55296) {
                            break;
                        }
                        c15 |= (charAt8 & 8191) << i38;
                        i38 += 13;
                        i37 = i19;
                    }
                    c3 = c15 | (charAt8 << i38);
                    i37 = i19;
                }
                int i39 = i37 + 1;
                char charAt20 = str2.charAt(i37);
                if (charAt20 >= 55296) {
                    char c16 = charAt20 & 8191;
                    int i40 = 13;
                    while (true) {
                        i18 = i39 + 1;
                        charAt7 = str2.charAt(i39);
                        if (charAt7 < 55296) {
                            break;
                        }
                        c16 |= (charAt7 & 8191) << i40;
                        i40 += 13;
                        i39 = i18;
                    }
                    charAt20 = c16 | (charAt7 << i40);
                    i39 = i18;
                }
                int i41 = i39 + 1;
                char charAt21 = str2.charAt(i39);
                if (charAt21 >= 55296) {
                    char c17 = charAt21 & 8191;
                    int i42 = i41;
                    int i43 = 13;
                    while (true) {
                        i17 = i42 + 1;
                        charAt6 = str2.charAt(i42);
                        if (charAt6 < 55296) {
                            break;
                        }
                        c17 |= (charAt6 & 8191) << i43;
                        i43 += 13;
                        i42 = i17;
                    }
                    charAt21 = c17 | (charAt6 << i43);
                    i15 = i17;
                } else {
                    i15 = i41;
                }
                int i44 = i15 + 1;
                char charAt22 = str2.charAt(i15);
                if (charAt22 >= 55296) {
                    char c18 = charAt22 & 8191;
                    int i45 = i44;
                    int i46 = 13;
                    while (true) {
                        i16 = i45 + 1;
                        charAt5 = str2.charAt(i45);
                        if (charAt5 < 55296) {
                            break;
                        }
                        c18 |= (charAt5 & 8191) << i46;
                        i46 += 13;
                        i45 = i16;
                    }
                    charAt22 = c18 | (charAt5 << i46);
                    i44 = i16;
                }
                i3 = (charAt17 << 1) + charAt18;
                c5 = charAt19;
                iArr = new int[(charAt22 + charAt20 + charAt21)];
                c6 = charAt20;
                c2 = charAt22;
                c8 = charAt17;
                i27 = i44;
            }
            Unsafe unsafe = f1854r;
            Object[] objArr = r5Var2.c;
            int i47 = i27;
            Class<?> cls2 = r5Var2.a.getClass();
            int i48 = i3;
            int[] iArr3 = new int[(c3 * 3)];
            Object[] objArr2 = new Object[(c3 << 1)];
            int i49 = c2 + c6;
            char c19 = c2;
            int i50 = i47;
            int i51 = i49;
            int i52 = 0;
            int i53 = 0;
            while (i50 < length) {
                int i54 = i50 + 1;
                int charAt23 = str2.charAt(i50);
                int i55 = length;
                if (charAt23 >= 55296) {
                    int i56 = charAt23 & 8191;
                    int i57 = i54;
                    int i58 = 13;
                    while (true) {
                        i14 = i57 + 1;
                        charAt4 = str2.charAt(i57);
                        c7 = c2;
                        if (charAt4 < 55296) {
                            break;
                        }
                        i56 |= (charAt4 & 8191) << i58;
                        i58 += 13;
                        i57 = i14;
                        c2 = c7;
                    }
                    charAt23 = i56 | (charAt4 << i58);
                    i4 = i14;
                } else {
                    c7 = c2;
                    i4 = i54;
                }
                int i59 = i4 + 1;
                char charAt24 = str2.charAt(i4);
                int i60 = i59;
                if (charAt24 >= 55296) {
                    char c20 = charAt24 & 8191;
                    int i61 = i60;
                    int i62 = 13;
                    while (true) {
                        i13 = i61 + 1;
                        charAt3 = str2.charAt(i61);
                        z = z2;
                        if (charAt3 < 55296) {
                            break;
                        }
                        c20 |= (charAt3 & 8191) << i62;
                        i62 += 13;
                        i61 = i13;
                        z2 = z;
                    }
                    charAt24 = c20 | (charAt3 << i62);
                    i5 = i13;
                } else {
                    z = z2;
                    i5 = i60;
                }
                char c21 = charAt24 & 255;
                char c22 = c5;
                if ((charAt24 & 1024) != 0) {
                    iArr[i52] = i53;
                    i52++;
                }
                char c23 = c4;
                if (c21 >= '3') {
                    int i63 = i5 + 1;
                    char charAt25 = str2.charAt(i5);
                    char c24 = 55296;
                    if (charAt25 >= 55296) {
                        char c25 = charAt25 & 8191;
                        int i64 = 13;
                        while (true) {
                            i12 = i63 + 1;
                            charAt2 = str2.charAt(i63);
                            if (charAt2 < c24) {
                                break;
                            }
                            c25 |= (charAt2 & 8191) << i64;
                            i64 += 13;
                            i63 = i12;
                            c24 = 55296;
                        }
                        charAt25 = c25 | (charAt2 << i64);
                        i63 = i12;
                    }
                    int i65 = c21 - '3';
                    int i66 = i63;
                    if (i65 == 9 || i65 == 17) {
                        objArr2[((i53 / 3) << 1) + 1] = objArr[i48];
                        i48++;
                    } else if (i65 == 12 && (charAt15 & 1) == 1) {
                        objArr2[((i53 / 3) << 1) + 1] = objArr[i48];
                        i48++;
                    }
                    int i67 = charAt25 << 1;
                    Object obj = objArr[i67];
                    if (obj instanceof Field) {
                        field2 = (Field) obj;
                    } else {
                        field2 = a(cls2, (String) obj);
                        objArr[i67] = field2;
                    }
                    iArr2 = iArr3;
                    i8 = (int) unsafe.objectFieldOffset(field2);
                    int i68 = i67 + 1;
                    Object obj2 = objArr[i68];
                    if (obj2 instanceof Field) {
                        field3 = (Field) obj2;
                    } else {
                        field3 = a(cls2, (String) obj2);
                        objArr[i68] = field3;
                    }
                    i9 = (int) unsafe.objectFieldOffset(field3);
                    r5Var = r5Var2;
                    str = str2;
                    i6 = i66;
                    i7 = 0;
                } else {
                    iArr2 = iArr3;
                    int i69 = i48 + 1;
                    Field a2 = a(cls2, (String) objArr[i48]);
                    if (c21 == 9 || c21 == 17) {
                        r5Var = r5Var2;
                        objArr2[((i53 / 3) << 1) + 1] = a2.getType();
                    } else {
                        if (c21 == 27 || c21 == '1') {
                            r5Var = r5Var2;
                            i11 = i69 + 1;
                            objArr2[((i53 / 3) << 1) + 1] = objArr[i69];
                        } else if (c21 == 12 || c21 == 30 || c21 == ',') {
                            r5Var = r5Var2;
                            if ((charAt15 & 1) == 1) {
                                i11 = i69 + 1;
                                objArr2[((i53 / 3) << 1) + 1] = objArr[i69];
                            }
                        } else if (c21 == '2') {
                            int i70 = c19 + 1;
                            iArr[c19] = i53;
                            int i71 = (i53 / 3) << 1;
                            int i72 = i69 + 1;
                            objArr2[i71] = objArr[i69];
                            if ((charAt24 & 2048) != 0) {
                                i69 = i72 + 1;
                                objArr2[i71 + 1] = objArr[i72];
                                r5Var = r5Var2;
                                c19 = i70;
                            } else {
                                c19 = i70;
                                i69 = i72;
                                r5Var = r5Var2;
                            }
                        } else {
                            r5Var = r5Var2;
                        }
                        str = str2;
                        i69 = i11;
                        i8 = (int) unsafe.objectFieldOffset(a2);
                        if ((charAt15 & 1) == 1 || c21 > 17) {
                            i6 = i5;
                            i10 = 0;
                            i7 = 0;
                        } else {
                            int i73 = i5 + 1;
                            String str3 = str;
                            char charAt26 = str3.charAt(i5);
                            if (charAt26 >= 55296) {
                                char c26 = charAt26 & 8191;
                                int i74 = 13;
                                while (true) {
                                    i6 = i73 + 1;
                                    charAt = str3.charAt(i73);
                                    if (charAt < 55296) {
                                        break;
                                    }
                                    c26 |= (charAt & 8191) << i74;
                                    i74 += 13;
                                    i73 = i6;
                                }
                                charAt26 = c26 | (charAt << i74);
                            } else {
                                i6 = i73;
                            }
                            int i75 = (charAt26 / ' ') + (c8 << 1);
                            Object obj3 = objArr[i75];
                            if (obj3 instanceof Field) {
                                field = (Field) obj3;
                            } else {
                                field = a(cls2, (String) obj3);
                                objArr[i75] = field;
                            }
                            str = str3;
                            i10 = (int) unsafe.objectFieldOffset(field);
                            i7 = charAt26 % ' ';
                        }
                        if (c21 >= 18 && c21 <= '1') {
                            iArr[i51] = i8;
                            i51++;
                        }
                        i48 = i69;
                        i9 = i10;
                    }
                    str = str2;
                    i8 = (int) unsafe.objectFieldOffset(a2);
                    if ((charAt15 & 1) == 1) {
                    }
                    i6 = i5;
                    i10 = 0;
                    i7 = 0;
                    iArr[i51] = i8;
                    i51++;
                    i48 = i69;
                    i9 = i10;
                }
                int i76 = i53 + 1;
                iArr2[i53] = charAt23;
                int i77 = i76 + 1;
                iArr2[i76] = ((charAt24 & 256) != 0 ? 268435456 : 0) | ((charAt24 & 512) != 0 ? 536870912 : 0) | (c21 << 20) | i8;
                i53 = i77 + 1;
                iArr2[i77] = (i7 << 20) | i9;
                str2 = str;
                c5 = c22;
                length = i55;
                r5Var2 = r5Var;
                c2 = c7;
                z2 = z;
                i50 = i6;
                c4 = c23;
                iArr3 = iArr2;
            }
            return new h5(iArr3, objArr2, c4, c5, r5Var2.a, z2, iArr, c2, i49, l5Var, p4Var, g6Var, l3Var, y4Var);
        } else if (((e6) d5Var2) != null) {
            throw new NoSuchMethodError();
        } else {
            throw null;
        }
    }

    public static i6 e(Object obj) {
        x3 x3Var = (x3) obj;
        i6 i6Var = x3Var.zzb;
        if (i6Var != i6.f1866f) {
            return i6Var;
        }
        i6 b2 = i6.b();
        x3Var.zzb = b2;
        return b2;
    }

    public static <T> boolean f(T t2, long j2) {
        return ((Boolean) m6.f(t2, j2)).booleanValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      j.c.a.a.f.e.h5.a(java.lang.Object, long):java.util.List<?>
      j.c.a.a.f.e.h5.a(int, int):int
      j.c.a.a.f.e.h5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.a(java.lang.Object, long, long):void
     arg types: [T, long, long]
     candidates:
      j.c.a.a.f.e.m6.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.a(java.lang.Object, long, java.lang.Object):void
      j.c.a.a.f.e.m6.a(byte[], long, byte):void
      j.c.a.a.f.e.m6.a(java.lang.Object, long, long):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.b(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.b(java.lang.Object, long):double
      j.c.a.a.f.e.h5.b(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.b(java.lang.Object, java.lang.Object):void
      j.c.a.a.f.e.t5.b(java.lang.Object, java.lang.Object):void
      j.c.a.a.f.e.h5.b(java.lang.Object, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void */
    public final void b(T t2, T t3) {
        if (t3 != null) {
            for (int i2 = 0; i2 < this.a.length; i2 += 3) {
                int d2 = d(i2);
                long j2 = (long) (1048575 & d2);
                int i3 = this.a[i2];
                switch ((d2 & 267386880) >>> 20) {
                    case 0:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a(t2, j2, m6.e(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 1:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.d(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 2:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a((Object) t2, j2, m6.b(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 3:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a((Object) t2, j2, m6.b(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 4:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.a(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 5:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a((Object) t2, j2, m6.b(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 6:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.a(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 7:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.c(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 8:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a(t2, j2, m6.f(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 9:
                        a(t2, t3, i2);
                        break;
                    case 10:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a(t2, j2, m6.f(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 11:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.a(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 12:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.a(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 13:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.a(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 14:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a((Object) t2, j2, m6.b(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 15:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.f1878f.a((Object) t2, j2, m6.a(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 16:
                        if (!a((Object) t3, i2)) {
                            break;
                        } else {
                            m6.a((Object) t2, j2, m6.b(t3, j2));
                            b((Object) t2, i2);
                            break;
                        }
                    case 17:
                        a(t2, t3, i2);
                        break;
                    case 18:
                    case 19:
                    case 20:
                    case 21:
                    case 22:
                    case 23:
                    case 24:
                    case 25:
                    case 26:
                    case 27:
                    case 28:
                    case 29:
                    case 30:
                    case 31:
                    case 32:
                    case 33:
                    case 34:
                    case 35:
                    case 36:
                    case 37:
                    case 38:
                    case 39:
                    case 40:
                    case 41:
                    case 42:
                    case 43:
                    case 44:
                    case 45:
                    case 46:
                    case 47:
                    case 48:
                    case 49:
                        this.f1861m.a(t2, t3, j2);
                        break;
                    case 50:
                        u5.a(this.f1864p, t2, t3, j2);
                        break;
                    case 51:
                    case 52:
                    case 53:
                    case 54:
                    case 55:
                    case 56:
                    case 57:
                    case 58:
                    case 59:
                        if (!a(t3, i3, i2)) {
                            break;
                        } else {
                            m6.a(t2, j2, m6.f(t3, j2));
                            b(t2, i3, i2);
                            break;
                        }
                    case 60:
                        b(t2, t3, i2);
                        break;
                    case 61:
                    case 62:
                    case 63:
                    case 64:
                    case 65:
                    case 66:
                    case 67:
                        if (!a(t3, i3, i2)) {
                            break;
                        } else {
                            m6.a(t2, j2, m6.f(t3, j2));
                            b(t2, i3, i2);
                            break;
                        }
                    case 68:
                        b(t2, t3, i2);
                        break;
                }
            }
            if (!this.g) {
                u5.a(this.f1862n, t2, t3);
                if (this.f1856f) {
                    u5.a(this.f1863o, t2, t3);
                    return;
                }
                return;
            }
            return;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.u5.b(int, java.util.List<j.c.a.a.f.e.f5>, j.c.a.a.f.e.t5):int
     arg types: [int, java.util.List<?>, j.c.a.a.f.e.t5]
     candidates:
      j.c.a.a.f.e.u5.b(int, java.util.List<j.c.a.a.f.e.w2>, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.u5.b(int, java.util.List<j.c.a.a.f.e.f5>, j.c.a.a.f.e.t5):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      j.c.a.a.f.e.h5.a(java.lang.Object, long):java.util.List<?>
      j.c.a.a.f.e.h5.a(int, int):int
      j.c.a.a.f.e.h5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean */
    public final int c(T t2) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        T t3 = t2;
        int i12 = 267386880;
        if (this.g) {
            Unsafe unsafe = f1854r;
            int i13 = 0;
            for (int i14 = 0; i14 < this.a.length; i14 += 3) {
                int d2 = d(i14);
                int i15 = (d2 & 267386880) >>> 20;
                int[] iArr = this.a;
                int i16 = iArr[i14];
                long j2 = (long) (d2 & 1048575);
                int i17 = (i15 < r3.zza.zzba || i15 > r3.zzb.zzba) ? 0 : iArr[i14 + 2] & 1048575;
                switch (i15) {
                    case 0:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.d(i16);
                            i13 += i8;
                            break;
                        }
                    case 1:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.b(i16);
                            i13 += i8;
                            break;
                        }
                    case 2:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.a(i16, m6.b(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 3:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.b(i16, m6.b(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 4:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.b(i16, m6.a(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 5:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.i(i16);
                            i13 += i8;
                            break;
                        }
                    case 6:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.l(i16);
                            i13 += i8;
                            break;
                        }
                    case 7:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.e(i16);
                            i13 += i8;
                            break;
                        }
                    case 8:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            Object f2 = m6.f(t3, j2);
                            if (f2 instanceof w2) {
                                i8 = zzek.a(i16, (w2) f2);
                            } else {
                                i8 = zzek.a(i16, (String) f2);
                            }
                            i13 += i8;
                            break;
                        }
                    case 9:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = u5.a(i16, m6.f(t3, j2), a(i14));
                            i13 += i8;
                            break;
                        }
                    case 10:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.a(i16, (w2) m6.f(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 11:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.c(i16, m6.a(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 12:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.e(i16, m6.a(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 13:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.m(i16);
                            i13 += i8;
                            break;
                        }
                    case 14:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.k(i16);
                            i13 += i8;
                            break;
                        }
                    case 15:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.d(i16, m6.a(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 16:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.c(i16, m6.b(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 17:
                        if (!a((Object) t3, i14)) {
                            break;
                        } else {
                            i8 = zzek.a(i16, (f5) m6.f(t3, j2), a(i14));
                            i13 += i8;
                            break;
                        }
                    case 18:
                        i8 = u5.k(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 19:
                        i8 = u5.j(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 20:
                        i8 = u5.b(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 21:
                        i8 = u5.d(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 22:
                        i8 = u5.g(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 23:
                        i8 = u5.k(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 24:
                        i8 = u5.j(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 25:
                        i8 = u5.l(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 26:
                        i8 = u5.a(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 27:
                        i8 = u5.a(i16, a(t3, j2), a(i14));
                        i13 += i8;
                        break;
                    case 28:
                        i8 = u5.c(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 29:
                        i8 = u5.h(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 30:
                        i8 = u5.f(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 31:
                        i8 = u5.j(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 32:
                        i8 = u5.k(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 33:
                        i8 = u5.i(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 34:
                        i8 = u5.e(i16, a(t3, j2));
                        i13 += i8;
                        break;
                    case 35:
                        i10 = u5.i((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 36:
                        i10 = u5.h((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 37:
                        i10 = u5.a((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 38:
                        i10 = u5.b((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 39:
                        i10 = u5.e((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 40:
                        i10 = u5.i((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 41:
                        i10 = u5.h((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 42:
                        i10 = u5.j((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 43:
                        i10 = u5.f((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 44:
                        i10 = u5.d((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 45:
                        i10 = u5.h((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 46:
                        i10 = u5.i((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 47:
                        i10 = u5.g((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 48:
                        i10 = u5.c((List) unsafe.getObject(t3, j2));
                        if (i10 > 0) {
                            if (this.h) {
                                unsafe.putInt(t3, (long) i17, i10);
                            }
                            i11 = zzek.f(i16);
                            i9 = zzek.h(i10);
                            i13 = outline.a(i9, i11, i10, i13);
                            break;
                        } else {
                            break;
                        }
                    case 49:
                        i8 = u5.b(i16, (List<f5>) a(t3, j2), a(i14));
                        i13 += i8;
                        break;
                    case 50:
                        i8 = this.f1864p.a(i16, m6.f(t3, j2), b(i14));
                        i13 += i8;
                        break;
                    case 51:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.d(i16);
                            i13 += i8;
                            break;
                        }
                    case 52:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.b(i16);
                            i13 += i8;
                            break;
                        }
                    case 53:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.a(i16, e(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 54:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.b(i16, e(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 55:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.b(i16, d(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 56:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.i(i16);
                            i13 += i8;
                            break;
                        }
                    case 57:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.l(i16);
                            i13 += i8;
                            break;
                        }
                    case 58:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.e(i16);
                            i13 += i8;
                            break;
                        }
                    case 59:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            Object f3 = m6.f(t3, j2);
                            if (f3 instanceof w2) {
                                i8 = zzek.a(i16, (w2) f3);
                            } else {
                                i8 = zzek.a(i16, (String) f3);
                            }
                            i13 += i8;
                            break;
                        }
                    case 60:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = u5.a(i16, m6.f(t3, j2), a(i14));
                            i13 += i8;
                            break;
                        }
                    case 61:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.a(i16, (w2) m6.f(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 62:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.c(i16, d(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 63:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.e(i16, d(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 64:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.m(i16);
                            i13 += i8;
                            break;
                        }
                    case 65:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.k(i16);
                            i13 += i8;
                            break;
                        }
                    case 66:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.d(i16, d(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 67:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.c(i16, e(t3, j2));
                            i13 += i8;
                            break;
                        }
                    case 68:
                        if (!a(t3, i16, i14)) {
                            break;
                        } else {
                            i8 = zzek.a(i16, (f5) m6.f(t3, j2), a(i14));
                            i13 += i8;
                            break;
                        }
                }
            }
            if (((h6) this.f1862n) != null) {
                return ((x3) t3).zzb.a() + i13;
            }
            throw null;
        }
        Unsafe unsafe2 = f1854r;
        int i18 = -1;
        int i19 = 0;
        int i20 = 0;
        int i21 = 0;
        while (i19 < this.a.length) {
            int d3 = d(i19);
            int[] iArr2 = this.a;
            int i22 = iArr2[i19];
            int i23 = (i12 & d3) >>> 20;
            if (i23 <= 17) {
                i3 = iArr2[i19 + 2];
                int i24 = i3 & 1048575;
                i2 = 1 << (i3 >>> 20);
                if (i24 != i18) {
                    i21 = unsafe2.getInt(t3, (long) i24);
                    i18 = i24;
                }
            } else {
                i3 = (!this.h || i23 < r3.zza.zzba || i23 > r3.zzb.zzba) ? 0 : iArr2[i19 + 2] & 1048575;
                i2 = 0;
            }
            int i25 = d3 & 1048575;
            int i26 = i18;
            long j3 = (long) i25;
            switch (i23) {
                case 0:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.d(i22);
                        i20 += i4;
                        break;
                    }
                case 1:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.b(i22);
                        i20 += i4;
                        break;
                    }
                case 2:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.a(i22, unsafe2.getLong(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 3:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.b(i22, unsafe2.getLong(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 4:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.b(i22, unsafe2.getInt(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 5:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.i(i22);
                        i20 += i4;
                        break;
                    }
                case 6:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.l(i22);
                        i20 += i4;
                        break;
                    }
                case 7:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.e(i22);
                        i20 += i4;
                        break;
                    }
                case 8:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        Object object = unsafe2.getObject(t3, j3);
                        if (object instanceof w2) {
                            i4 = zzek.a(i22, (w2) object);
                        } else {
                            i4 = zzek.a(i22, (String) object);
                        }
                        i20 += i4;
                        break;
                    }
                case 9:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = u5.a(i22, unsafe2.getObject(t3, j3), a(i19));
                        i20 += i4;
                        break;
                    }
                case 10:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.a(i22, (w2) unsafe2.getObject(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 11:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.c(i22, unsafe2.getInt(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 12:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.e(i22, unsafe2.getInt(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 13:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.m(i22);
                        i20 += i4;
                        break;
                    }
                case 14:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.k(i22);
                        i20 += i4;
                        break;
                    }
                case 15:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.d(i22, unsafe2.getInt(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 16:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.c(i22, unsafe2.getLong(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 17:
                    if ((i21 & i2) == 0) {
                        break;
                    } else {
                        i4 = zzek.a(i22, (f5) unsafe2.getObject(t3, j3), a(i19));
                        i20 += i4;
                        break;
                    }
                case 18:
                    i4 = u5.k(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 19:
                    i4 = u5.j(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 20:
                    i4 = u5.b(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 21:
                    i4 = u5.d(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 22:
                    i4 = u5.g(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 23:
                    i4 = u5.k(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 24:
                    i4 = u5.j(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 25:
                    i4 = u5.l(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 26:
                    i4 = u5.a(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 27:
                    i4 = u5.a(i22, (List<?>) ((List) unsafe2.getObject(t3, j3)), a(i19));
                    i20 += i4;
                    break;
                case 28:
                    i4 = u5.c(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 29:
                    i4 = u5.h(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 30:
                    i4 = u5.f(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 31:
                    i4 = u5.j(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 32:
                    i4 = u5.k(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 33:
                    i4 = u5.i(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 34:
                    i4 = u5.e(i22, (List) unsafe2.getObject(t3, j3));
                    i20 += i4;
                    break;
                case 35:
                    i7 = u5.i((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 36:
                    i7 = u5.h((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 37:
                    i7 = u5.a((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 38:
                    i7 = u5.b((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 39:
                    i7 = u5.e((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 40:
                    i7 = u5.i((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 41:
                    i7 = u5.h((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 42:
                    i7 = u5.j((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 43:
                    i7 = u5.f((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 44:
                    i7 = u5.d((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 45:
                    i7 = u5.h((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 46:
                    i7 = u5.i((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 47:
                    i7 = u5.g((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 48:
                    i7 = u5.c((List) unsafe2.getObject(t3, j3));
                    if (i7 > 0) {
                        if (this.h) {
                            unsafe2.putInt(t3, (long) i3, i7);
                        }
                        i6 = zzek.f(i22);
                        i5 = zzek.h(i7);
                        i20 = outline.a(i5, i6, i7, i20);
                        break;
                    } else {
                        break;
                    }
                case 49:
                    i4 = u5.b(i22, (List) unsafe2.getObject(t3, j3), a(i19));
                    i20 += i4;
                    break;
                case 50:
                    i4 = this.f1864p.a(i22, unsafe2.getObject(t3, j3), b(i19));
                    i20 += i4;
                    break;
                case 51:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.d(i22);
                        i20 += i4;
                        break;
                    }
                case 52:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.b(i22);
                        i20 += i4;
                        break;
                    }
                case 53:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.a(i22, e(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 54:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.b(i22, e(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 55:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.b(i22, d(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 56:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.i(i22);
                        i20 += i4;
                        break;
                    }
                case 57:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.l(i22);
                        i20 += i4;
                        break;
                    }
                case 58:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.e(i22);
                        i20 += i4;
                        break;
                    }
                case 59:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        Object object2 = unsafe2.getObject(t3, j3);
                        if (object2 instanceof w2) {
                            i4 = zzek.a(i22, (w2) object2);
                        } else {
                            i4 = zzek.a(i22, (String) object2);
                        }
                        i20 += i4;
                        break;
                    }
                case 60:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = u5.a(i22, unsafe2.getObject(t3, j3), a(i19));
                        i20 += i4;
                        break;
                    }
                case 61:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.a(i22, (w2) unsafe2.getObject(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 62:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.c(i22, d(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 63:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.e(i22, d(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 64:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.m(i22);
                        i20 += i4;
                        break;
                    }
                case 65:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.k(i22);
                        i20 += i4;
                        break;
                    }
                case 66:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.d(i22, d(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 67:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.c(i22, e(t3, j3));
                        i20 += i4;
                        break;
                    }
                case 68:
                    if (!a(t3, i22, i19)) {
                        break;
                    } else {
                        i4 = zzek.a(i22, (f5) unsafe2.getObject(t3, j3), a(i19));
                        i20 += i4;
                        break;
                    }
            }
            i19 += 3;
            i12 = 267386880;
            i18 = i26;
        }
        if (((h6) this.f1862n) != null) {
            int a2 = ((x3) t3).zzb.a() + i20;
            if (!this.f1856f) {
                return a2;
            }
            if (((k3) this.f1863o) != null) {
                m3<Object> m3Var = ((x3.b) t3).zzc;
                int i27 = 0;
                for (int i28 = 0; i28 < m3Var.a.b(); i28++) {
                    Map.Entry<FieldDescriptorType, Object> a3 = m3Var.a.a(i28);
                    i27 += m3.b((o3) a3.getKey(), a3.getValue());
                }
                for (Map.Entry next : m3Var.a.c()) {
                    i27 += m3.b((o3) next.getKey(), next.getValue());
                }
                return a2 + i27;
            }
            throw null;
        }
        throw null;
    }

    public final boolean d(T t2) {
        int i2;
        int i3 = -1;
        int i4 = 0;
        int i5 = 0;
        while (true) {
            boolean z = true;
            if (i4 < this.f1858j) {
                int i6 = this.f1857i[i4];
                int i7 = this.a[i6];
                int d2 = d(i6);
                if (!this.g) {
                    int i8 = this.a[i6 + 2];
                    int i9 = i8 & 1048575;
                    i2 = 1 << (i8 >>> 20);
                    if (i9 != i3) {
                        i5 = f1854r.getInt(t2, (long) i9);
                        i3 = i9;
                    }
                } else {
                    i2 = 0;
                }
                if (((268435456 & d2) != 0) && !a(t2, i6, i5, i2)) {
                    return false;
                }
                int i10 = (267386880 & d2) >>> 20;
                if (i10 != 9 && i10 != 17) {
                    if (i10 != 27) {
                        if (i10 == 60 || i10 == 68) {
                            if (a(t2, i7, i6) && !a(i6).d(m6.f(t2, (long) (d2 & 1048575)))) {
                                return false;
                            }
                        } else if (i10 != 49) {
                            if (i10 == 50 && !this.f1864p.c(m6.f(t2, (long) (d2 & 1048575))).isEmpty()) {
                                this.f1864p.e(b(i6));
                                throw null;
                            }
                        }
                    }
                    List list = (List) m6.f(t2, (long) (d2 & 1048575));
                    if (!list.isEmpty()) {
                        t5 a2 = a(i6);
                        int i11 = 0;
                        while (true) {
                            if (i11 >= list.size()) {
                                break;
                            } else if (!a2.d(list.get(i11))) {
                                z = false;
                                break;
                            } else {
                                i11++;
                            }
                        }
                    }
                    if (!z) {
                        return false;
                    }
                } else if (a(t2, i6, i5, i2) && !a(i6).d(m6.f(t2, (long) (d2 & 1048575)))) {
                    return false;
                }
                i4++;
            } else {
                if (this.f1856f) {
                    if (((k3) this.f1863o) == null) {
                        throw null;
                    } else if (!((x3.b) t2).zzc.b()) {
                        return false;
                    }
                }
                return true;
            }
        }
    }

    public final int e(int i2) {
        return this.a[i2 + 2];
    }

    public static <T> long e(T t2, long j2) {
        return ((Long) m6.f(t2, j2)).longValue();
    }

    public final int d(int i2) {
        return this.a[i2 + 1];
    }

    public static <T> int d(T t2, long j2) {
        return ((Integer) m6.f(t2, j2)).intValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.u5.b(int, java.util.List<java.lang.Float>, j.c.a.a.f.e.c7, boolean):void
     arg types: [int, java.util.List, j.c.a.a.f.e.c7, int]
     candidates:
      j.c.a.a.f.e.u5.b(int, java.util.List<?>, j.c.a.a.f.e.c7, j.c.a.a.f.e.t5):void
      j.c.a.a.f.e.u5.b(int, java.util.List<java.lang.Float>, j.c.a.a.f.e.c7, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.u5.a(int, java.util.List<java.lang.Double>, j.c.a.a.f.e.c7, boolean):void
     arg types: [int, java.util.List, j.c.a.a.f.e.c7, int]
     candidates:
      j.c.a.a.f.e.u5.a(int, int, java.lang.Object, j.c.a.a.f.e.g6):UB
      j.c.a.a.f.e.u5.a(int, java.util.List<?>, j.c.a.a.f.e.c7, j.c.a.a.f.e.t5):void
      j.c.a.a.f.e.u5.a(j.c.a.a.f.e.y4, java.lang.Object, java.lang.Object, long):void
      j.c.a.a.f.e.u5.a(int, java.util.List<java.lang.Double>, j.c.a.a.f.e.c7, boolean):void */
    /* JADX WARNING: Removed duplicated region for block: B:11:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:169:0x04fd  */
    /* JADX WARNING: Removed duplicated region for block: B:171:0x0503  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void b(T r19, j.c.a.a.f.e.c7 r20) {
        /*
            r18 = this;
            r0 = r18
            r1 = r19
            r2 = r20
            boolean r3 = r0.f1856f
            r4 = 0
            if (r3 == 0) goto L_0x002a
            j.c.a.a.f.e.l3<?> r3 = r0.f1863o
            j.c.a.a.f.e.k3 r3 = (j.c.a.a.f.e.k3) r3
            if (r3 == 0) goto L_0x0029
            r3 = r1
            j.c.a.a.f.e.x3$b r3 = (j.c.a.a.f.e.x3.b) r3
            j.c.a.a.f.e.m3<java.lang.Object> r3 = r3.zzc
            j.c.a.a.f.e.x5<FieldDescriptorType, java.lang.Object> r5 = r3.a
            boolean r5 = r5.isEmpty()
            if (r5 != 0) goto L_0x002a
            java.util.Iterator r3 = r3.a()
            java.lang.Object r3 = r3.next()
            java.util.Map$Entry r3 = (java.util.Map.Entry) r3
            goto L_0x002b
        L_0x0029:
            throw r4
        L_0x002a:
            r3 = r4
        L_0x002b:
            r5 = -1
            int[] r6 = r0.a
            int r6 = r6.length
            sun.misc.Unsafe r7 = j.c.a.a.f.e.h5.f1854r
            r9 = 0
            r10 = 0
        L_0x0033:
            if (r9 >= r6) goto L_0x04fb
            int r11 = r0.d(r9)
            int[] r12 = r0.a
            r13 = r12[r9]
            r14 = 267386880(0xff00000, float:2.3665827E-29)
            r14 = r14 & r11
            int r14 = r14 >>> 20
            boolean r15 = r0.g
            r16 = 1048575(0xfffff, float:1.469367E-39)
            r4 = 1
            if (r15 != 0) goto L_0x0063
            r15 = 17
            if (r14 > r15) goto L_0x0063
            int r15 = r9 + 2
            r12 = r12[r15]
            r15 = r12 & r16
            r17 = r9
            if (r15 == r5) goto L_0x005e
            long r8 = (long) r15
            int r10 = r7.getInt(r1, r8)
            r5 = r15
        L_0x005e:
            int r8 = r12 >>> 20
            int r8 = r4 << r8
            goto L_0x0066
        L_0x0063:
            r17 = r9
            r8 = 0
        L_0x0066:
            if (r3 != 0) goto L_0x04f4
            r9 = r11 & r16
            long r11 = (long) r9
            r9 = r17
            switch(r14) {
                case 0: goto L_0x04e0;
                case 1: goto L_0x04d0;
                case 2: goto L_0x04c0;
                case 3: goto L_0x04b0;
                case 4: goto L_0x049e;
                case 5: goto L_0x048e;
                case 6: goto L_0x047e;
                case 7: goto L_0x046d;
                case 8: goto L_0x045f;
                case 9: goto L_0x044a;
                case 10: goto L_0x0437;
                case 11: goto L_0x0426;
                case 12: goto L_0x0413;
                case 13: goto L_0x0402;
                case 14: goto L_0x03f1;
                case 15: goto L_0x03e0;
                case 16: goto L_0x03cf;
                case 17: goto L_0x03ba;
                case 18: goto L_0x03aa;
                case 19: goto L_0x039a;
                case 20: goto L_0x038a;
                case 21: goto L_0x037a;
                case 22: goto L_0x036a;
                case 23: goto L_0x035a;
                case 24: goto L_0x034a;
                case 25: goto L_0x033a;
                case 26: goto L_0x032b;
                case 27: goto L_0x0318;
                case 28: goto L_0x0309;
                case 29: goto L_0x02f9;
                case 30: goto L_0x02e9;
                case 31: goto L_0x02d9;
                case 32: goto L_0x02c9;
                case 33: goto L_0x02b9;
                case 34: goto L_0x02a9;
                case 35: goto L_0x029a;
                case 36: goto L_0x028b;
                case 37: goto L_0x027c;
                case 38: goto L_0x026d;
                case 39: goto L_0x025e;
                case 40: goto L_0x024f;
                case 41: goto L_0x0240;
                case 42: goto L_0x0231;
                case 43: goto L_0x0222;
                case 44: goto L_0x0213;
                case 45: goto L_0x0204;
                case 46: goto L_0x01f5;
                case 47: goto L_0x01e6;
                case 48: goto L_0x01d7;
                case 49: goto L_0x01c4;
                case 50: goto L_0x01bb;
                case 51: goto L_0x01a9;
                case 52: goto L_0x0197;
                case 53: goto L_0x0185;
                case 54: goto L_0x0173;
                case 55: goto L_0x015f;
                case 56: goto L_0x014d;
                case 57: goto L_0x013b;
                case 58: goto L_0x0129;
                case 59: goto L_0x011a;
                case 60: goto L_0x0104;
                case 61: goto L_0x00f0;
                case 62: goto L_0x00df;
                case 63: goto L_0x00cc;
                case 64: goto L_0x00bb;
                case 65: goto L_0x00aa;
                case 66: goto L_0x0099;
                case 67: goto L_0x0088;
                case 68: goto L_0x0073;
                default: goto L_0x0070;
            }
        L_0x0070:
            r14 = 0
            goto L_0x04ef
        L_0x0073:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            java.lang.Object r4 = r7.getObject(r1, r11)
            j.c.a.a.f.e.t5 r8 = r0.a(r9)
            r11 = r2
            j.c.a.a.f.e.g3 r11 = (j.c.a.a.f.e.g3) r11
            r11.b(r13, r4, r8)
            goto L_0x0070
        L_0x0088:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            long r11 = e(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.e(r13, r11)
            goto L_0x0070
        L_0x0099:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            int r4 = d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.d(r13, r4)
            goto L_0x0070
        L_0x00aa:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            long r11 = e(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.b(r13, r11)
            goto L_0x0070
        L_0x00bb:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            int r4 = d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x0070
        L_0x00cc:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            int r4 = d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            com.google.android.gms.internal.measurement.zzek r8 = r8.a
            r8.a(r13, r4)
            goto L_0x0070
        L_0x00df:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            int r4 = d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.c(r13, r4)
            goto L_0x0070
        L_0x00f0:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            java.lang.Object r4 = r7.getObject(r1, r11)
            j.c.a.a.f.e.w2 r4 = (j.c.a.a.f.e.w2) r4
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x0070
        L_0x0104:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            java.lang.Object r4 = r7.getObject(r1, r11)
            j.c.a.a.f.e.t5 r8 = r0.a(r9)
            r11 = r2
            j.c.a.a.f.e.g3 r11 = (j.c.a.a.f.e.g3) r11
            r11.a(r13, r4, r8)
            goto L_0x0070
        L_0x011a:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            java.lang.Object r4 = r7.getObject(r1, r11)
            a(r13, r4, r2)
            goto L_0x0070
        L_0x0129:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            boolean r4 = f(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x0070
        L_0x013b:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            int r4 = d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.b(r13, r4)
            goto L_0x0070
        L_0x014d:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            long r11 = e(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.d(r13, r11)
            goto L_0x0070
        L_0x015f:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            int r4 = d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            com.google.android.gms.internal.measurement.zzek r8 = r8.a
            r8.a(r13, r4)
            goto L_0x0070
        L_0x0173:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            long r11 = e(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.c(r13, r11)
            goto L_0x0070
        L_0x0185:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            long r11 = e(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.a(r13, r11)
            goto L_0x0070
        L_0x0197:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            float r4 = c(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x0070
        L_0x01a9:
            boolean r4 = r0.a(r1, r13, r9)
            if (r4 == 0) goto L_0x0070
            double r11 = b(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.a(r13, r11)
            goto L_0x0070
        L_0x01bb:
            java.lang.Object r4 = r7.getObject(r1, r11)
            r0.a(r2, r13, r4, r9)
            goto L_0x0070
        L_0x01c4:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.t5 r11 = r0.a(r9)
            j.c.a.a.f.e.u5.b(r4, r8, r2, r11)
            goto L_0x0070
        L_0x01d7:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.e(r8, r11, r2, r4)
            goto L_0x0070
        L_0x01e6:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.j(r8, r11, r2, r4)
            goto L_0x0070
        L_0x01f5:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.g(r8, r11, r2, r4)
            goto L_0x0070
        L_0x0204:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.l(r8, r11, r2, r4)
            goto L_0x0070
        L_0x0213:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.m(r8, r11, r2, r4)
            goto L_0x0070
        L_0x0222:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.i(r8, r11, r2, r4)
            goto L_0x0070
        L_0x0231:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.n(r8, r11, r2, r4)
            goto L_0x0070
        L_0x0240:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.k(r8, r11, r2, r4)
            goto L_0x0070
        L_0x024f:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.f(r8, r11, r2, r4)
            goto L_0x0070
        L_0x025e:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.h(r8, r11, r2, r4)
            goto L_0x0070
        L_0x026d:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.d(r8, r11, r2, r4)
            goto L_0x0070
        L_0x027c:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.c(r8, r11, r2, r4)
            goto L_0x0070
        L_0x028b:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.b(r8, r11, r2, r4)
            goto L_0x0070
        L_0x029a:
            int[] r8 = r0.a
            r8 = r8[r9]
            java.lang.Object r11 = r7.getObject(r1, r11)
            java.util.List r11 = (java.util.List) r11
            j.c.a.a.f.e.u5.a(r8, r11, r2, r4)
            goto L_0x0070
        L_0x02a9:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            r13 = 0
            j.c.a.a.f.e.u5.e(r4, r8, r2, r13)
            goto L_0x0070
        L_0x02b9:
            r13 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.j(r4, r8, r2, r13)
            goto L_0x0070
        L_0x02c9:
            r13 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.g(r4, r8, r2, r13)
            goto L_0x0070
        L_0x02d9:
            r13 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.l(r4, r8, r2, r13)
            goto L_0x0070
        L_0x02e9:
            r13 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.m(r4, r8, r2, r13)
            goto L_0x0070
        L_0x02f9:
            r13 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.i(r4, r8, r2, r13)
            goto L_0x0070
        L_0x0309:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.b(r4, r8, r2)
            goto L_0x0070
        L_0x0318:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.t5 r11 = r0.a(r9)
            j.c.a.a.f.e.u5.a(r4, r8, r2, r11)
            goto L_0x0070
        L_0x032b:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.a(r4, r8, r2)
            goto L_0x0070
        L_0x033a:
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            r14 = 0
            j.c.a.a.f.e.u5.n(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x034a:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.k(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x035a:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.f(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x036a:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.h(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x037a:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.d(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x038a:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.c(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x039a:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.b(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x03aa:
            r14 = 0
            int[] r4 = r0.a
            r4 = r4[r9]
            java.lang.Object r8 = r7.getObject(r1, r11)
            java.util.List r8 = (java.util.List) r8
            j.c.a.a.f.e.u5.a(r4, r8, r2, r14)
            goto L_0x04ef
        L_0x03ba:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            java.lang.Object r4 = r7.getObject(r1, r11)
            j.c.a.a.f.e.t5 r8 = r0.a(r9)
            r11 = r2
            j.c.a.a.f.e.g3 r11 = (j.c.a.a.f.e.g3) r11
            r11.b(r13, r4, r8)
            goto L_0x04ef
        L_0x03cf:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            long r11 = r7.getLong(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.e(r13, r11)
            goto L_0x04ef
        L_0x03e0:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            int r4 = r7.getInt(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.d(r13, r4)
            goto L_0x04ef
        L_0x03f1:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            long r11 = r7.getLong(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.b(r13, r11)
            goto L_0x04ef
        L_0x0402:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            int r4 = r7.getInt(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x04ef
        L_0x0413:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            int r4 = r7.getInt(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            com.google.android.gms.internal.measurement.zzek r8 = r8.a
            r8.a(r13, r4)
            goto L_0x04ef
        L_0x0426:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            int r4 = r7.getInt(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.c(r13, r4)
            goto L_0x04ef
        L_0x0437:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            java.lang.Object r4 = r7.getObject(r1, r11)
            j.c.a.a.f.e.w2 r4 = (j.c.a.a.f.e.w2) r4
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x04ef
        L_0x044a:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            java.lang.Object r4 = r7.getObject(r1, r11)
            j.c.a.a.f.e.t5 r8 = r0.a(r9)
            r11 = r2
            j.c.a.a.f.e.g3 r11 = (j.c.a.a.f.e.g3) r11
            r11.a(r13, r4, r8)
            goto L_0x04ef
        L_0x045f:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            java.lang.Object r4 = r7.getObject(r1, r11)
            a(r13, r4, r2)
            goto L_0x04ef
        L_0x046d:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            boolean r4 = j.c.a.a.f.e.m6.c(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x04ef
        L_0x047e:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            int r4 = r7.getInt(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.b(r13, r4)
            goto L_0x04ef
        L_0x048e:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            long r11 = r7.getLong(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.d(r13, r11)
            goto L_0x04ef
        L_0x049e:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            int r4 = r7.getInt(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            com.google.android.gms.internal.measurement.zzek r8 = r8.a
            r8.a(r13, r4)
            goto L_0x04ef
        L_0x04b0:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            long r11 = r7.getLong(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.c(r13, r11)
            goto L_0x04ef
        L_0x04c0:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            long r11 = r7.getLong(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.a(r13, r11)
            goto L_0x04ef
        L_0x04d0:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            float r4 = j.c.a.a.f.e.m6.d(r1, r11)
            r8 = r2
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r13, r4)
            goto L_0x04ef
        L_0x04e0:
            r14 = 0
            r4 = r10 & r8
            if (r4 == 0) goto L_0x04ef
            double r11 = j.c.a.a.f.e.m6.e(r1, r11)
            r4 = r2
            j.c.a.a.f.e.g3 r4 = (j.c.a.a.f.e.g3) r4
            r4.a(r13, r11)
        L_0x04ef:
            int r9 = r9 + 3
            r4 = 0
            goto L_0x0033
        L_0x04f4:
            j.c.a.a.f.e.l3<?> r1 = r0.f1863o
            r1.a(r3)
            r4 = 0
            throw r4
        L_0x04fb:
            if (r3 != 0) goto L_0x0503
            j.c.a.a.f.e.g6<?, ?> r3 = r0.f1862n
            a(r3, r1, r2)
            return
        L_0x0503:
            j.c.a.a.f.e.l3<?> r1 = r0.f1863o
            r1.a(r2, r3)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.h5.b(java.lang.Object, j.c.a.a.f.e.c7):void");
    }

    public static Field a(Class<?> cls, String str) {
        try {
            return cls.getDeclaredField(str);
        } catch (NoSuchFieldException unused) {
            Field[] declaredFields = cls.getDeclaredFields();
            for (Field field : declaredFields) {
                if (str.equals(field.getName())) {
                    return field;
                }
            }
            String name = cls.getName();
            String arrays = Arrays.toString(declaredFields);
            StringBuilder sb = new StringBuilder(outline.a(arrays, name.length() + outline.a(str, 40)));
            sb.append("Field ");
            sb.append(str);
            sb.append(" for ");
            sb.append(name);
            throw new RuntimeException(outline.a(sb, " not found. Known fields are ", arrays));
        }
    }

    public final T a() {
        return this.f1860l.a(this.f1855e);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:14:0x006a, code lost:
        if (j.c.a.a.f.e.u5.a(j.c.a.a.f.e.m6.f(r10, r6), j.c.a.a.f.e.m6.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:18:0x007e, code lost:
        if (j.c.a.a.f.e.m6.b(r10, r6) == j.c.a.a.f.e.m6.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:22:0x0090, code lost:
        if (j.c.a.a.f.e.m6.a(r10, r6) == j.c.a.a.f.e.m6.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:26:0x00a4, code lost:
        if (j.c.a.a.f.e.m6.b(r10, r6) == j.c.a.a.f.e.m6.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:30:0x00b6, code lost:
        if (j.c.a.a.f.e.m6.a(r10, r6) == j.c.a.a.f.e.m6.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:34:0x00c8, code lost:
        if (j.c.a.a.f.e.m6.a(r10, r6) == j.c.a.a.f.e.m6.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x00da, code lost:
        if (j.c.a.a.f.e.m6.a(r10, r6) == j.c.a.a.f.e.m6.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:42:0x00f0, code lost:
        if (j.c.a.a.f.e.u5.a(j.c.a.a.f.e.m6.f(r10, r6), j.c.a.a.f.e.m6.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:46:0x0106, code lost:
        if (j.c.a.a.f.e.u5.a(j.c.a.a.f.e.m6.f(r10, r6), j.c.a.a.f.e.m6.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:50:0x011c, code lost:
        if (j.c.a.a.f.e.u5.a(j.c.a.a.f.e.m6.f(r10, r6), j.c.a.a.f.e.m6.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:54:0x012e, code lost:
        if (j.c.a.a.f.e.m6.c(r10, r6) == j.c.a.a.f.e.m6.c(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:58:0x0140, code lost:
        if (j.c.a.a.f.e.m6.a(r10, r6) == j.c.a.a.f.e.m6.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:62:0x0154, code lost:
        if (j.c.a.a.f.e.m6.b(r10, r6) == j.c.a.a.f.e.m6.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:66:0x0165, code lost:
        if (j.c.a.a.f.e.m6.a(r10, r6) == j.c.a.a.f.e.m6.a(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:70:0x0178, code lost:
        if (j.c.a.a.f.e.m6.b(r10, r6) == j.c.a.a.f.e.m6.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:74:0x018b, code lost:
        if (j.c.a.a.f.e.m6.b(r10, r6) == j.c.a.a.f.e.m6.b(r11, r6)) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:78:0x01a4, code lost:
        if (java.lang.Float.floatToIntBits(j.c.a.a.f.e.m6.d(r10, r6)) == java.lang.Float.floatToIntBits(j.c.a.a.f.e.m6.d(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:82:0x01bf, code lost:
        if (java.lang.Double.doubleToLongBits(j.c.a.a.f.e.m6.e(r10, r6)) == java.lang.Double.doubleToLongBits(j.c.a.a.f.e.m6.e(r11, r6))) goto L_0x01c2;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:8:0x0038, code lost:
        if (j.c.a.a.f.e.u5.a(j.c.a.a.f.e.m6.f(r10, r6), j.c.a.a.f.e.m6.f(r11, r6)) != false) goto L_0x01c2;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final boolean a(T r10, T r11) {
        /*
            r9 = this;
            int[] r0 = r9.a
            int r0 = r0.length
            r1 = 0
            r2 = 0
        L_0x0005:
            r3 = 1
            if (r2 >= r0) goto L_0x01c9
            int r4 = r9.d(r2)
            r5 = 1048575(0xfffff, float:1.469367E-39)
            r6 = r4 & r5
            long r6 = (long) r6
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r4 = r4 & r8
            int r4 = r4 >>> 20
            switch(r4) {
                case 0: goto L_0x01a7;
                case 1: goto L_0x018e;
                case 2: goto L_0x017b;
                case 3: goto L_0x0168;
                case 4: goto L_0x0157;
                case 5: goto L_0x0144;
                case 6: goto L_0x0132;
                case 7: goto L_0x0120;
                case 8: goto L_0x010a;
                case 9: goto L_0x00f4;
                case 10: goto L_0x00de;
                case 11: goto L_0x00cc;
                case 12: goto L_0x00ba;
                case 13: goto L_0x00a8;
                case 14: goto L_0x0094;
                case 15: goto L_0x0082;
                case 16: goto L_0x006e;
                case 17: goto L_0x0058;
                case 18: goto L_0x004a;
                case 19: goto L_0x004a;
                case 20: goto L_0x004a;
                case 21: goto L_0x004a;
                case 22: goto L_0x004a;
                case 23: goto L_0x004a;
                case 24: goto L_0x004a;
                case 25: goto L_0x004a;
                case 26: goto L_0x004a;
                case 27: goto L_0x004a;
                case 28: goto L_0x004a;
                case 29: goto L_0x004a;
                case 30: goto L_0x004a;
                case 31: goto L_0x004a;
                case 32: goto L_0x004a;
                case 33: goto L_0x004a;
                case 34: goto L_0x004a;
                case 35: goto L_0x004a;
                case 36: goto L_0x004a;
                case 37: goto L_0x004a;
                case 38: goto L_0x004a;
                case 39: goto L_0x004a;
                case 40: goto L_0x004a;
                case 41: goto L_0x004a;
                case 42: goto L_0x004a;
                case 43: goto L_0x004a;
                case 44: goto L_0x004a;
                case 45: goto L_0x004a;
                case 46: goto L_0x004a;
                case 47: goto L_0x004a;
                case 48: goto L_0x004a;
                case 49: goto L_0x004a;
                case 50: goto L_0x003c;
                case 51: goto L_0x001c;
                case 52: goto L_0x001c;
                case 53: goto L_0x001c;
                case 54: goto L_0x001c;
                case 55: goto L_0x001c;
                case 56: goto L_0x001c;
                case 57: goto L_0x001c;
                case 58: goto L_0x001c;
                case 59: goto L_0x001c;
                case 60: goto L_0x001c;
                case 61: goto L_0x001c;
                case 62: goto L_0x001c;
                case 63: goto L_0x001c;
                case 64: goto L_0x001c;
                case 65: goto L_0x001c;
                case 66: goto L_0x001c;
                case 67: goto L_0x001c;
                case 68: goto L_0x001c;
                default: goto L_0x001a;
            }
        L_0x001a:
            goto L_0x01c2
        L_0x001c:
            int r4 = r9.e(r2)
            r4 = r4 & r5
            long r4 = (long) r4
            int r8 = j.c.a.a.f.e.m6.a(r10, r4)
            int r4 = j.c.a.a.f.e.m6.a(r11, r4)
            if (r8 != r4) goto L_0x01c1
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r4 = j.c.a.a.f.e.u5.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x003c:
            java.lang.Object r3 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r3 = j.c.a.a.f.e.u5.a(r3, r4)
            goto L_0x01c2
        L_0x004a:
            java.lang.Object r3 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r3 = j.c.a.a.f.e.u5.a(r3, r4)
            goto L_0x01c2
        L_0x0058:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r4 = j.c.a.a.f.e.u5.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x006e:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = j.c.a.a.f.e.m6.b(r10, r6)
            long r6 = j.c.a.a.f.e.m6.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0082:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = j.c.a.a.f.e.m6.a(r10, r6)
            int r5 = j.c.a.a.f.e.m6.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0094:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = j.c.a.a.f.e.m6.b(r10, r6)
            long r6 = j.c.a.a.f.e.m6.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00a8:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = j.c.a.a.f.e.m6.a(r10, r6)
            int r5 = j.c.a.a.f.e.m6.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00ba:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = j.c.a.a.f.e.m6.a(r10, r6)
            int r5 = j.c.a.a.f.e.m6.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00cc:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = j.c.a.a.f.e.m6.a(r10, r6)
            int r5 = j.c.a.a.f.e.m6.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x00de:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r4 = j.c.a.a.f.e.u5.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x00f4:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r4 = j.c.a.a.f.e.u5.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x010a:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            java.lang.Object r4 = j.c.a.a.f.e.m6.f(r10, r6)
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r11, r6)
            boolean r4 = j.c.a.a.f.e.u5.a(r4, r5)
            if (r4 != 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0120:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            boolean r4 = j.c.a.a.f.e.m6.c(r10, r6)
            boolean r5 = j.c.a.a.f.e.m6.c(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0132:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = j.c.a.a.f.e.m6.a(r10, r6)
            int r5 = j.c.a.a.f.e.m6.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0144:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = j.c.a.a.f.e.m6.b(r10, r6)
            long r6 = j.c.a.a.f.e.m6.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x0157:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            int r4 = j.c.a.a.f.e.m6.a(r10, r6)
            int r5 = j.c.a.a.f.e.m6.a(r11, r6)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x0168:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = j.c.a.a.f.e.m6.b(r10, r6)
            long r6 = j.c.a.a.f.e.m6.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x017b:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            long r4 = j.c.a.a.f.e.m6.b(r10, r6)
            long r6 = j.c.a.a.f.e.m6.b(r11, r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
            goto L_0x01c1
        L_0x018e:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            float r4 = j.c.a.a.f.e.m6.d(r10, r6)
            int r4 = java.lang.Float.floatToIntBits(r4)
            float r5 = j.c.a.a.f.e.m6.d(r11, r6)
            int r5 = java.lang.Float.floatToIntBits(r5)
            if (r4 == r5) goto L_0x01c2
            goto L_0x01c1
        L_0x01a7:
            boolean r4 = r9.c(r10, r11, r2)
            if (r4 == 0) goto L_0x01c1
            double r4 = j.c.a.a.f.e.m6.e(r10, r6)
            long r4 = java.lang.Double.doubleToLongBits(r4)
            double r6 = j.c.a.a.f.e.m6.e(r11, r6)
            long r6 = java.lang.Double.doubleToLongBits(r6)
            int r8 = (r4 > r6 ? 1 : (r4 == r6 ? 0 : -1))
            if (r8 == 0) goto L_0x01c2
        L_0x01c1:
            r3 = 0
        L_0x01c2:
            if (r3 != 0) goto L_0x01c5
            return r1
        L_0x01c5:
            int r2 = r2 + 3
            goto L_0x0005
        L_0x01c9:
            j.c.a.a.f.e.g6<?, ?> r0 = r9.f1862n
            r2 = r0
            j.c.a.a.f.e.h6 r2 = (j.c.a.a.f.e.h6) r2
            r4 = 0
            if (r2 == 0) goto L_0x0206
            r2 = r10
            j.c.a.a.f.e.x3 r2 = (j.c.a.a.f.e.x3) r2
            j.c.a.a.f.e.i6 r2 = r2.zzb
            j.c.a.a.f.e.h6 r0 = (j.c.a.a.f.e.h6) r0
            if (r0 == 0) goto L_0x0205
            r0 = r11
            j.c.a.a.f.e.x3 r0 = (j.c.a.a.f.e.x3) r0
            j.c.a.a.f.e.i6 r0 = r0.zzb
            boolean r0 = r2.equals(r0)
            if (r0 != 0) goto L_0x01e6
            return r1
        L_0x01e6:
            boolean r0 = r9.f1856f
            if (r0 == 0) goto L_0x0204
            j.c.a.a.f.e.l3<?> r0 = r9.f1863o
            r1 = r0
            j.c.a.a.f.e.k3 r1 = (j.c.a.a.f.e.k3) r1
            if (r1 == 0) goto L_0x0203
            j.c.a.a.f.e.x3$b r10 = (j.c.a.a.f.e.x3.b) r10
            j.c.a.a.f.e.m3<java.lang.Object> r10 = r10.zzc
            j.c.a.a.f.e.k3 r0 = (j.c.a.a.f.e.k3) r0
            if (r0 == 0) goto L_0x0202
            j.c.a.a.f.e.x3$b r11 = (j.c.a.a.f.e.x3.b) r11
            j.c.a.a.f.e.m3<java.lang.Object> r11 = r11.zzc
            boolean r10 = r10.equals(r11)
            return r10
        L_0x0202:
            throw r4
        L_0x0203:
            throw r4
        L_0x0204:
            return r3
        L_0x0205:
            throw r4
        L_0x0206:
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.h5.a(java.lang.Object, java.lang.Object):boolean");
    }

    public final int a(T t2) {
        int i2;
        int i3;
        int length = this.a.length;
        int i4 = 0;
        for (int i5 = 0; i5 < length; i5 += 3) {
            int d2 = d(i5);
            int i6 = this.a[i5];
            long j2 = (long) (1048575 & d2);
            int i7 = 37;
            switch ((d2 & 267386880) >>> 20) {
                case 0:
                    i3 = i4 * 53;
                    i2 = y3.a(Double.doubleToLongBits(m6.e(t2, j2)));
                    i4 = i2 + i3;
                    break;
                case 1:
                    i3 = i4 * 53;
                    i2 = Float.floatToIntBits(m6.d(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 2:
                    i3 = i4 * 53;
                    i2 = y3.a(m6.b(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 3:
                    i3 = i4 * 53;
                    i2 = y3.a(m6.b(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 4:
                    i3 = i4 * 53;
                    i2 = m6.a(t2, j2);
                    i4 = i2 + i3;
                    break;
                case 5:
                    i3 = i4 * 53;
                    i2 = y3.a(m6.b(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 6:
                    i3 = i4 * 53;
                    i2 = m6.a(t2, j2);
                    i4 = i2 + i3;
                    break;
                case 7:
                    i3 = i4 * 53;
                    i2 = y3.a(m6.c(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 8:
                    i3 = i4 * 53;
                    i2 = ((String) m6.f(t2, j2)).hashCode();
                    i4 = i2 + i3;
                    break;
                case 9:
                    Object f2 = m6.f(t2, j2);
                    if (f2 != null) {
                        i7 = f2.hashCode();
                    }
                    i4 = (i4 * 53) + i7;
                    break;
                case 10:
                    i3 = i4 * 53;
                    i2 = m6.f(t2, j2).hashCode();
                    i4 = i2 + i3;
                    break;
                case 11:
                    i3 = i4 * 53;
                    i2 = m6.a(t2, j2);
                    i4 = i2 + i3;
                    break;
                case 12:
                    i3 = i4 * 53;
                    i2 = m6.a(t2, j2);
                    i4 = i2 + i3;
                    break;
                case 13:
                    i3 = i4 * 53;
                    i2 = m6.a(t2, j2);
                    i4 = i2 + i3;
                    break;
                case 14:
                    i3 = i4 * 53;
                    i2 = y3.a(m6.b(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 15:
                    i3 = i4 * 53;
                    i2 = m6.a(t2, j2);
                    i4 = i2 + i3;
                    break;
                case 16:
                    i3 = i4 * 53;
                    i2 = y3.a(m6.b(t2, j2));
                    i4 = i2 + i3;
                    break;
                case 17:
                    Object f3 = m6.f(t2, j2);
                    if (f3 != null) {
                        i7 = f3.hashCode();
                    }
                    i4 = (i4 * 53) + i7;
                    break;
                case 18:
                case 19:
                case 20:
                case 21:
                case 22:
                case 23:
                case 24:
                case 25:
                case 26:
                case 27:
                case 28:
                case 29:
                case 30:
                case 31:
                case 32:
                case 33:
                case 34:
                case 35:
                case 36:
                case 37:
                case 38:
                case 39:
                case 40:
                case 41:
                case 42:
                case 43:
                case 44:
                case 45:
                case 46:
                case 47:
                case 48:
                case 49:
                    i3 = i4 * 53;
                    i2 = m6.f(t2, j2).hashCode();
                    i4 = i2 + i3;
                    break;
                case 50:
                    i3 = i4 * 53;
                    i2 = m6.f(t2, j2).hashCode();
                    i4 = i2 + i3;
                    break;
                case 51:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(Double.doubleToLongBits(b(t2, j2)));
                        i4 = i2 + i3;
                        break;
                    }
                case 52:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = Float.floatToIntBits(c(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 53:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(e(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 54:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(e(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 55:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t2, j2);
                        i4 = i2 + i3;
                        break;
                    }
                case 56:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(e(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 57:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t2, j2);
                        i4 = i2 + i3;
                        break;
                    }
                case 58:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(f(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 59:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = ((String) m6.f(t2, j2)).hashCode();
                        i4 = i2 + i3;
                        break;
                    }
                case 60:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = m6.f(t2, j2).hashCode();
                        i4 = i2 + i3;
                        break;
                    }
                case 61:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = m6.f(t2, j2).hashCode();
                        i4 = i2 + i3;
                        break;
                    }
                case 62:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t2, j2);
                        i4 = i2 + i3;
                        break;
                    }
                case 63:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t2, j2);
                        i4 = i2 + i3;
                        break;
                    }
                case 64:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t2, j2);
                        i4 = i2 + i3;
                        break;
                    }
                case 65:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(e(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 66:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = d(t2, j2);
                        i4 = i2 + i3;
                        break;
                    }
                case 67:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = y3.a(e(t2, j2));
                        i4 = i2 + i3;
                        break;
                    }
                case 68:
                    if (!a(t2, i6, i5)) {
                        break;
                    } else {
                        i3 = i4 * 53;
                        i2 = m6.f(t2, j2).hashCode();
                        i4 = i2 + i3;
                        break;
                    }
            }
        }
        int i8 = i4 * 53;
        if (((h6) this.f1862n) != null) {
            int hashCode = ((x3) t2).zzb.hashCode() + i8;
            if (!this.f1856f) {
                return hashCode;
            }
            int i9 = hashCode * 53;
            if (((k3) this.f1863o) != null) {
                return i9 + ((x3.b) t2).zzc.hashCode();
            }
            throw null;
        }
        throw null;
    }

    public static List<?> a(Object obj, long j2) {
        return (List) m6.f(obj, j2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.u5.b(int, java.util.List<java.lang.Float>, j.c.a.a.f.e.c7, boolean):void
     arg types: [int, java.util.List, j.c.a.a.f.e.c7, int]
     candidates:
      j.c.a.a.f.e.u5.b(int, java.util.List<?>, j.c.a.a.f.e.c7, j.c.a.a.f.e.t5):void
      j.c.a.a.f.e.u5.b(int, java.util.List<java.lang.Float>, j.c.a.a.f.e.c7, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.u5.a(int, java.util.List<java.lang.Double>, j.c.a.a.f.e.c7, boolean):void
     arg types: [int, java.util.List, j.c.a.a.f.e.c7, int]
     candidates:
      j.c.a.a.f.e.u5.a(int, int, java.lang.Object, j.c.a.a.f.e.g6):UB
      j.c.a.a.f.e.u5.a(int, java.util.List<?>, j.c.a.a.f.e.c7, j.c.a.a.f.e.t5):void
      j.c.a.a.f.e.u5.a(j.c.a.a.f.e.y4, java.lang.Object, java.lang.Object, long):void
      j.c.a.a.f.e.u5.a(int, java.util.List<java.lang.Double>, j.c.a.a.f.e.c7, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      j.c.a.a.f.e.h5.a(java.lang.Object, long):java.util.List<?>
      j.c.a.a.f.e.h5.a(int, int):int
      j.c.a.a.f.e.h5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.b(java.lang.Object, j.c.a.a.f.e.c7):void
     arg types: [T, j.c.a.a.f.e.c7]
     candidates:
      j.c.a.a.f.e.h5.b(java.lang.Object, long):double
      j.c.a.a.f.e.h5.b(java.lang.Object, int):void
      j.c.a.a.f.e.h5.b(java.lang.Object, java.lang.Object):void
      j.c.a.a.f.e.t5.b(java.lang.Object, java.lang.Object):void
      j.c.a.a.f.e.h5.b(java.lang.Object, j.c.a.a.f.e.c7):void */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0035  */
    /* JADX WARNING: Removed duplicated region for block: B:164:0x052b  */
    /* JADX WARNING: Removed duplicated region for block: B:166:0x0531  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(T r12, j.c.a.a.f.e.c7 r13) {
        /*
            r11 = this;
            r0 = r13
            j.c.a.a.f.e.g3 r0 = (j.c.a.a.f.e.g3) r0
            r1 = 0
            if (r0 == 0) goto L_0x053b
            boolean r0 = r11.g
            if (r0 == 0) goto L_0x0537
            boolean r0 = r11.f1856f
            if (r0 == 0) goto L_0x002d
            j.c.a.a.f.e.l3<?> r0 = r11.f1863o
            j.c.a.a.f.e.k3 r0 = (j.c.a.a.f.e.k3) r0
            if (r0 == 0) goto L_0x002c
            r0 = r12
            j.c.a.a.f.e.x3$b r0 = (j.c.a.a.f.e.x3.b) r0
            j.c.a.a.f.e.m3<java.lang.Object> r0 = r0.zzc
            j.c.a.a.f.e.x5<FieldDescriptorType, java.lang.Object> r2 = r0.a
            boolean r2 = r2.isEmpty()
            if (r2 != 0) goto L_0x002d
            java.util.Iterator r0 = r0.a()
            java.lang.Object r0 = r0.next()
            java.util.Map$Entry r0 = (java.util.Map.Entry) r0
            goto L_0x002e
        L_0x002c:
            throw r1
        L_0x002d:
            r0 = r1
        L_0x002e:
            int[] r2 = r11.a
            int r2 = r2.length
            r3 = 0
            r4 = 0
        L_0x0033:
            if (r4 >= r2) goto L_0x0529
            int r5 = r11.d(r4)
            int[] r6 = r11.a
            r7 = r6[r4]
            if (r0 != 0) goto L_0x0523
            r8 = 267386880(0xff00000, float:2.3665827E-29)
            r8 = r8 & r5
            int r8 = r8 >>> 20
            r9 = 1
            r10 = 1048575(0xfffff, float:1.469367E-39)
            switch(r8) {
                case 0: goto L_0x050d;
                case 1: goto L_0x04fa;
                case 2: goto L_0x04e7;
                case 3: goto L_0x04d4;
                case 4: goto L_0x04bf;
                case 5: goto L_0x04ac;
                case 6: goto L_0x0498;
                case 7: goto L_0x0484;
                case 8: goto L_0x0473;
                case 9: goto L_0x045b;
                case 10: goto L_0x0445;
                case 11: goto L_0x0431;
                case 12: goto L_0x041b;
                case 13: goto L_0x0407;
                case 14: goto L_0x03f3;
                case 15: goto L_0x03df;
                case 16: goto L_0x03cb;
                case 17: goto L_0x03b3;
                case 18: goto L_0x03a4;
                case 19: goto L_0x0395;
                case 20: goto L_0x0386;
                case 21: goto L_0x0377;
                case 22: goto L_0x0368;
                case 23: goto L_0x0359;
                case 24: goto L_0x034a;
                case 25: goto L_0x033b;
                case 26: goto L_0x032c;
                case 27: goto L_0x0319;
                case 28: goto L_0x030a;
                case 29: goto L_0x02fb;
                case 30: goto L_0x02ec;
                case 31: goto L_0x02dd;
                case 32: goto L_0x02ce;
                case 33: goto L_0x02bf;
                case 34: goto L_0x02b0;
                case 35: goto L_0x02a1;
                case 36: goto L_0x0292;
                case 37: goto L_0x0283;
                case 38: goto L_0x0274;
                case 39: goto L_0x0265;
                case 40: goto L_0x0256;
                case 41: goto L_0x0247;
                case 42: goto L_0x0238;
                case 43: goto L_0x0229;
                case 44: goto L_0x021a;
                case 45: goto L_0x020b;
                case 46: goto L_0x01fc;
                case 47: goto L_0x01ed;
                case 48: goto L_0x01de;
                case 49: goto L_0x01cb;
                case 50: goto L_0x01c0;
                case 51: goto L_0x01ac;
                case 52: goto L_0x0198;
                case 53: goto L_0x0184;
                case 54: goto L_0x0170;
                case 55: goto L_0x015a;
                case 56: goto L_0x0146;
                case 57: goto L_0x0132;
                case 58: goto L_0x011e;
                case 59: goto L_0x010d;
                case 60: goto L_0x00f5;
                case 61: goto L_0x00df;
                case 62: goto L_0x00cb;
                case 63: goto L_0x00b5;
                case 64: goto L_0x00a1;
                case 65: goto L_0x008d;
                case 66: goto L_0x0079;
                case 67: goto L_0x0065;
                case 68: goto L_0x004d;
                default: goto L_0x004b;
            }
        L_0x004b:
            goto L_0x051f
        L_0x004d:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            j.c.a.a.f.e.t5 r6 = r11.a(r4)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.b(r7, r5, r6)
            goto L_0x051f
        L_0x0065:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = e(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.e(r7, r5)
            goto L_0x051f
        L_0x0079:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.d(r7, r5)
            goto L_0x051f
        L_0x008d:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = e(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.b(r7, r5)
            goto L_0x051f
        L_0x00a1:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x00b5:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            com.google.android.gms.internal.measurement.zzek r6 = r6.a
            r6.a(r7, r5)
            goto L_0x051f
        L_0x00cb:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.c(r7, r5)
            goto L_0x051f
        L_0x00df:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            j.c.a.a.f.e.w2 r5 = (j.c.a.a.f.e.w2) r5
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x00f5:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            j.c.a.a.f.e.t5 r6 = r11.a(r4)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r7, r5, r6)
            goto L_0x051f
        L_0x010d:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            a(r7, r5, r13)
            goto L_0x051f
        L_0x011e:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            boolean r5 = f(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x0132:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.b(r7, r5)
            goto L_0x051f
        L_0x0146:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = e(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.d(r7, r5)
            goto L_0x051f
        L_0x015a:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            com.google.android.gms.internal.measurement.zzek r6 = r6.a
            r6.a(r7, r5)
            goto L_0x051f
        L_0x0170:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = e(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.c(r7, r5)
            goto L_0x051f
        L_0x0184:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = e(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r7, r5)
            goto L_0x051f
        L_0x0198:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            float r5 = c(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x01ac:
            boolean r6 = r11.a(r12, r7, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            double r5 = b(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r7, r5)
            goto L_0x051f
        L_0x01c0:
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            r11.a(r13, r7, r5, r4)
            goto L_0x051f
        L_0x01cb:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.t5 r7 = r11.a(r4)
            j.c.a.a.f.e.u5.b(r6, r5, r13, r7)
            goto L_0x051f
        L_0x01de:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.e(r6, r5, r13, r9)
            goto L_0x051f
        L_0x01ed:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.j(r6, r5, r13, r9)
            goto L_0x051f
        L_0x01fc:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.g(r6, r5, r13, r9)
            goto L_0x051f
        L_0x020b:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.l(r6, r5, r13, r9)
            goto L_0x051f
        L_0x021a:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.m(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0229:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.i(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0238:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.n(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0247:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.k(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0256:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.f(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0265:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.h(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0274:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.d(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0283:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.c(r6, r5, r13, r9)
            goto L_0x051f
        L_0x0292:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.b(r6, r5, r13, r9)
            goto L_0x051f
        L_0x02a1:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.a(r6, r5, r13, r9)
            goto L_0x051f
        L_0x02b0:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.e(r6, r5, r13, r3)
            goto L_0x051f
        L_0x02bf:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.j(r6, r5, r13, r3)
            goto L_0x051f
        L_0x02ce:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.g(r6, r5, r13, r3)
            goto L_0x051f
        L_0x02dd:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.l(r6, r5, r13, r3)
            goto L_0x051f
        L_0x02ec:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.m(r6, r5, r13, r3)
            goto L_0x051f
        L_0x02fb:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.i(r6, r5, r13, r3)
            goto L_0x051f
        L_0x030a:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.b(r6, r5, r13)
            goto L_0x051f
        L_0x0319:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.t5 r7 = r11.a(r4)
            j.c.a.a.f.e.u5.a(r6, r5, r13, r7)
            goto L_0x051f
        L_0x032c:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.a(r6, r5, r13)
            goto L_0x051f
        L_0x033b:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.n(r6, r5, r13, r3)
            goto L_0x051f
        L_0x034a:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.k(r6, r5, r13, r3)
            goto L_0x051f
        L_0x0359:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.f(r6, r5, r13, r3)
            goto L_0x051f
        L_0x0368:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.h(r6, r5, r13, r3)
            goto L_0x051f
        L_0x0377:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.d(r6, r5, r13, r3)
            goto L_0x051f
        L_0x0386:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.c(r6, r5, r13, r3)
            goto L_0x051f
        L_0x0395:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.b(r6, r5, r13, r3)
            goto L_0x051f
        L_0x03a4:
            r6 = r6[r4]
            r5 = r5 & r10
            long r7 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r7)
            java.util.List r5 = (java.util.List) r5
            j.c.a.a.f.e.u5.a(r6, r5, r13, r3)
            goto L_0x051f
        L_0x03b3:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            j.c.a.a.f.e.t5 r6 = r11.a(r4)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.b(r7, r5, r6)
            goto L_0x051f
        L_0x03cb:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = j.c.a.a.f.e.m6.b(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.e(r7, r5)
            goto L_0x051f
        L_0x03df:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = j.c.a.a.f.e.m6.a(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.d(r7, r5)
            goto L_0x051f
        L_0x03f3:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = j.c.a.a.f.e.m6.b(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.b(r7, r5)
            goto L_0x051f
        L_0x0407:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = j.c.a.a.f.e.m6.a(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x041b:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = j.c.a.a.f.e.m6.a(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            com.google.android.gms.internal.measurement.zzek r6 = r6.a
            r6.a(r7, r5)
            goto L_0x051f
        L_0x0431:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = j.c.a.a.f.e.m6.a(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.c(r7, r5)
            goto L_0x051f
        L_0x0445:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            j.c.a.a.f.e.w2 r5 = (j.c.a.a.f.e.w2) r5
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x045b:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            j.c.a.a.f.e.t5 r6 = r11.a(r4)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r7, r5, r6)
            goto L_0x051f
        L_0x0473:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            java.lang.Object r5 = j.c.a.a.f.e.m6.f(r12, r5)
            a(r7, r5, r13)
            goto L_0x051f
        L_0x0484:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            boolean r5 = j.c.a.a.f.e.m6.c(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x0498:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = j.c.a.a.f.e.m6.a(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.b(r7, r5)
            goto L_0x051f
        L_0x04ac:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = j.c.a.a.f.e.m6.b(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.d(r7, r5)
            goto L_0x051f
        L_0x04bf:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            int r5 = j.c.a.a.f.e.m6.a(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            com.google.android.gms.internal.measurement.zzek r6 = r6.a
            r6.a(r7, r5)
            goto L_0x051f
        L_0x04d4:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = j.c.a.a.f.e.m6.b(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.c(r7, r5)
            goto L_0x051f
        L_0x04e7:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            long r5 = j.c.a.a.f.e.m6.b(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r7, r5)
            goto L_0x051f
        L_0x04fa:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            float r5 = j.c.a.a.f.e.m6.d(r12, r5)
            r6 = r13
            j.c.a.a.f.e.g3 r6 = (j.c.a.a.f.e.g3) r6
            r6.a(r7, r5)
            goto L_0x051f
        L_0x050d:
            boolean r6 = r11.a(r12, r4)
            if (r6 == 0) goto L_0x051f
            r5 = r5 & r10
            long r5 = (long) r5
            double r5 = j.c.a.a.f.e.m6.e(r12, r5)
            r8 = r13
            j.c.a.a.f.e.g3 r8 = (j.c.a.a.f.e.g3) r8
            r8.a(r7, r5)
        L_0x051f:
            int r4 = r4 + 3
            goto L_0x0033
        L_0x0523:
            j.c.a.a.f.e.l3<?> r12 = r11.f1863o
            r12.a(r0)
            throw r1
        L_0x0529:
            if (r0 != 0) goto L_0x0531
            j.c.a.a.f.e.g6<?, ?> r0 = r11.f1862n
            a(r0, r12, r13)
            return
        L_0x0531:
            j.c.a.a.f.e.l3<?> r12 = r11.f1863o
            r12.a(r13, r0)
            throw r1
        L_0x0537:
            r11.b(r12, r13)
            return
        L_0x053b:
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.h5.a(java.lang.Object, j.c.a.a.f.e.c7):void");
    }

    public final Object b(int i2) {
        return this.b[(i2 / 3) << 1];
    }

    public final void b(Object obj) {
        int i2;
        int i3 = this.f1858j;
        while (true) {
            i2 = this.f1859k;
            if (i3 >= i2) {
                break;
            }
            long d2 = (long) (d(this.f1857i[i3]) & 1048575);
            Object f2 = m6.f(obj, d2);
            if (f2 != null) {
                m6.a(obj, d2, this.f1864p.d(f2));
            }
            i3++;
        }
        int length = this.f1857i.length;
        while (i2 < length) {
            this.f1861m.a(obj, (long) this.f1857i[i2]);
            i2++;
        }
        if (((h6) this.f1862n) != null) {
            ((x3) obj).zzb.f1867e = false;
            if (!this.f1856f) {
                return;
            }
            if (((k3) this.f1863o) != null) {
                m3<Object> m3Var = ((x3.b) obj).zzc;
                if (!m3Var.b) {
                    m3Var.a.a();
                    m3Var.b = true;
                    return;
                }
                return;
            }
            throw null;
        }
        throw null;
    }

    public final void b(T t2, T t3, int i2) {
        int[] iArr = this.a;
        int i3 = iArr[i2 + 1];
        int i4 = iArr[i2];
        long j2 = (long) (i3 & 1048575);
        if (a(t3, i4, i2)) {
            Object f2 = m6.f(t2, j2);
            Object f3 = m6.f(t3, j2);
            if (f2 != null && f3 != null) {
                m6.a(t2, j2, y3.a(f2, f3));
                b(t2, i4, i2);
            } else if (f3 != null) {
                m6.a(t2, j2, f3);
                b(t2, i4, i2);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void */
    public final void b(T t2, int i2, int i3) {
        m6.f1878f.a((Object) t2, (long) (this.a[i3 + 2] & 1048575), i2);
    }

    public static <T> double b(T t2, long j2) {
        return ((Double) m6.f(t2, j2)).doubleValue();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
     arg types: [T, long, int]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void */
    public final void b(T t2, int i2) {
        if (!this.g) {
            int i3 = this.a[i2 + 2];
            long j2 = (long) (i3 & 1048575);
            m6.f1878f.a((Object) t2, j2, m6.a(t2, j2) | (1 << (i3 >>> 20)));
        }
    }

    public final d4 c(int i2) {
        return (d4) this.b[((i2 / 3) << 1) + 1];
    }

    public static <T> float c(T t2, long j2) {
        return ((Float) m6.f(t2, j2)).floatValue();
    }

    public final boolean c(T t2, T t3, int i2) {
        return a(t2, i2) == a(t3, i2);
    }

    public final <K, V> void a(c7 c7Var, int i2, Object obj, int i3) {
        if (obj != null) {
            this.f1864p.e(this.b[(i3 / 3) << 1]);
            Map<?, ?> c2 = this.f1864p.c(obj);
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                Iterator<Map.Entry<?, ?>> it = c2.entrySet().iterator();
                if (it.hasNext()) {
                    Map.Entry next = it.next();
                    ((zzek.a) g3Var.a).b((i2 << 3) | 2);
                    next.getKey();
                    next.getValue();
                    throw null;
                }
                return;
            }
            throw null;
        }
    }

    public static <UT, UB> void a(g6<UT, UB> g6Var, T t2, c7 c7Var) {
        if (((h6) g6Var) != null) {
            ((x3) t2).zzb.a(c7Var);
            return;
        }
        throw null;
    }

    /*  JADX ERROR: JadxOverflowException in pass: RegionMakerVisitor
        jadx.core.utils.exceptions.JadxOverflowException: Regions count limit reached
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    /* JADX WARNING: Removed duplicated region for block: B:243:0x0426 A[SYNTHETIC] */
    /* JADX WARNING: Removed duplicated region for block: B:91:0x01ef  */
    public final int a(T r17, byte[] r18, int r19, int r20, int r21, int r22, int r23, int r24, long r25, int r27, long r28, j.c.a.a.f.e.s2 r30) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            r3 = r18
            r4 = r19
            r5 = r20
            r2 = r21
            r6 = r23
            r8 = r24
            r9 = r28
            r7 = r30
            sun.misc.Unsafe r11 = j.c.a.a.f.e.h5.f1854r
            java.lang.Object r11 = r11.getObject(r1, r9)
            j.c.a.a.f.e.e4 r11 = (j.c.a.a.f.e.e4) r11
            boolean r12 = r11.a()
            r13 = 1
            if (r12 != 0) goto L_0x0036
            int r12 = r11.size()
            if (r12 != 0) goto L_0x002c
            r12 = 10
            goto L_0x002d
        L_0x002c:
            int r12 = r12 << r13
        L_0x002d:
            j.c.a.a.f.e.e4 r11 = r11.a(r12)
            sun.misc.Unsafe r12 = j.c.a.a.f.e.h5.f1854r
            r12.putObject(r1, r9, r11)
        L_0x0036:
            r9 = 5
            r10 = 3
            r14 = 0
            r12 = 2
            switch(r27) {
                case 18: goto L_0x03e8;
                case 19: goto L_0x03aa;
                case 20: goto L_0x0369;
                case 21: goto L_0x0369;
                case 22: goto L_0x034f;
                case 23: goto L_0x0310;
                case 24: goto L_0x02d1;
                case 25: goto L_0x027a;
                case 26: goto L_0x01c7;
                case 27: goto L_0x01ad;
                case 28: goto L_0x0155;
                case 29: goto L_0x034f;
                case 30: goto L_0x0119;
                case 31: goto L_0x02d1;
                case 32: goto L_0x0310;
                case 33: goto L_0x00cc;
                case 34: goto L_0x007f;
                case 35: goto L_0x03e8;
                case 36: goto L_0x03aa;
                case 37: goto L_0x0369;
                case 38: goto L_0x0369;
                case 39: goto L_0x034f;
                case 40: goto L_0x0310;
                case 41: goto L_0x02d1;
                case 42: goto L_0x027a;
                case 43: goto L_0x034f;
                case 44: goto L_0x0119;
                case 45: goto L_0x02d1;
                case 46: goto L_0x0310;
                case 47: goto L_0x00cc;
                case 48: goto L_0x007f;
                case 49: goto L_0x0040;
                default: goto L_0x003e;
            }
        L_0x003e:
            goto L_0x0426
        L_0x0040:
            if (r6 != r10) goto L_0x0426
            j.c.a.a.f.e.t5 r1 = r0.a(r8)
            r6 = r2 & -8
            r6 = r6 | 4
            r22 = r1
            r23 = r18
            r24 = r19
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = j.c.a.a.c.n.c.a(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.c
            r11.add(r8)
        L_0x005f:
            if (r4 >= r5) goto L_0x0426
            int r8 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r9 = r7.a
            if (r2 != r9) goto L_0x0426
            r22 = r1
            r23 = r18
            r24 = r8
            r25 = r20
            r26 = r6
            r27 = r30
            int r4 = j.c.a.a.c.n.c.a(r22, r23, r24, r25, r26, r27)
            java.lang.Object r8 = r7.c
            r11.add(r8)
            goto L_0x005f
        L_0x007f:
            if (r6 != r12) goto L_0x00a3
            j.c.a.a.f.e.u4 r11 = (j.c.a.a.f.e.u4) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x008a:
            if (r1 >= r2) goto L_0x009a
            int r1 = j.c.a.a.c.n.c.b(r3, r1, r7)
            long r4 = r7.b
            long r4 = j.c.a.a.f.e.e3.a(r4)
            r11.a(r4)
            goto L_0x008a
        L_0x009a:
            if (r1 != r2) goto L_0x009e
            goto L_0x0427
        L_0x009e:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x00a3:
            if (r6 != 0) goto L_0x0426
            j.c.a.a.f.e.u4 r11 = (j.c.a.a.f.e.u4) r11
            int r1 = j.c.a.a.c.n.c.b(r3, r4, r7)
            long r8 = r7.b
            long r8 = j.c.a.a.f.e.e3.a(r8)
            r11.a(r8)
        L_0x00b4:
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            int r1 = j.c.a.a.c.n.c.b(r3, r4, r7)
            long r8 = r7.b
            long r8 = j.c.a.a.f.e.e3.a(r8)
            r11.a(r8)
            goto L_0x00b4
        L_0x00cc:
            if (r6 != r12) goto L_0x00f0
            j.c.a.a.f.e.z3 r11 = (j.c.a.a.f.e.z3) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x00d7:
            if (r1 >= r2) goto L_0x00e7
            int r1 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r4 = r7.a
            int r4 = j.c.a.a.f.e.e3.a(r4)
            r11.d(r4)
            goto L_0x00d7
        L_0x00e7:
            if (r1 != r2) goto L_0x00eb
            goto L_0x0427
        L_0x00eb:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x00f0:
            if (r6 != 0) goto L_0x0426
            j.c.a.a.f.e.z3 r11 = (j.c.a.a.f.e.z3) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = j.c.a.a.f.e.e3.a(r4)
            r11.d(r4)
        L_0x0101:
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = j.c.a.a.f.e.e3.a(r4)
            r11.d(r4)
            goto L_0x0101
        L_0x0119:
            if (r6 != r12) goto L_0x0120
            int r2 = j.c.a.a.c.n.c.a(r3, r4, r11, r7)
            goto L_0x0131
        L_0x0120:
            if (r6 != 0) goto L_0x0426
            r2 = r21
            r3 = r18
            r4 = r19
            r5 = r20
            r6 = r11
            r7 = r30
            int r2 = j.c.a.a.c.n.c.a(r2, r3, r4, r5, r6, r7)
        L_0x0131:
            j.c.a.a.f.e.x3 r1 = (j.c.a.a.f.e.x3) r1
            j.c.a.a.f.e.i6 r3 = r1.zzb
            j.c.a.a.f.e.i6 r4 = j.c.a.a.f.e.i6.f1866f
            if (r3 != r4) goto L_0x013a
            r3 = 0
        L_0x013a:
            java.lang.Object[] r4 = r0.b
            int r5 = r8 / 3
            int r5 = r5 << r13
            int r5 = r5 + r13
            r4 = r4[r5]
            j.c.a.a.f.e.d4 r4 = (j.c.a.a.f.e.d4) r4
            j.c.a.a.f.e.g6<?, ?> r5 = r0.f1862n
            r6 = r22
            java.lang.Object r3 = j.c.a.a.f.e.u5.a(r6, r11, r4, r3, r5)
            j.c.a.a.f.e.i6 r3 = (j.c.a.a.f.e.i6) r3
            if (r3 == 0) goto L_0x0152
            r1.zzb = r3
        L_0x0152:
            r1 = r2
            goto L_0x0427
        L_0x0155:
            if (r6 != r12) goto L_0x0426
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r4 = r7.a
            if (r4 < 0) goto L_0x01a8
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x01a3
            if (r4 != 0) goto L_0x016b
            j.c.a.a.f.e.w2 r4 = j.c.a.a.f.e.w2.c
            r11.add(r4)
            goto L_0x0173
        L_0x016b:
            j.c.a.a.f.e.w2 r6 = j.c.a.a.f.e.w2.a(r3, r1, r4)
            r11.add(r6)
        L_0x0172:
            int r1 = r1 + r4
        L_0x0173:
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r4 = r7.a
            if (r4 < 0) goto L_0x019e
            int r6 = r3.length
            int r6 = r6 - r1
            if (r4 > r6) goto L_0x0199
            if (r4 != 0) goto L_0x0191
            j.c.a.a.f.e.w2 r4 = j.c.a.a.f.e.w2.c
            r11.add(r4)
            goto L_0x0173
        L_0x0191:
            j.c.a.a.f.e.w2 r6 = j.c.a.a.f.e.w2.a(r3, r1, r4)
            r11.add(r6)
            goto L_0x0172
        L_0x0199:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x019e:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.b()
            throw r1
        L_0x01a3:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x01a8:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.b()
            throw r1
        L_0x01ad:
            if (r6 != r12) goto L_0x0426
            j.c.a.a.f.e.t5 r1 = r0.a(r8)
            r22 = r1
            r23 = r21
            r24 = r18
            r25 = r19
            r26 = r20
            r27 = r11
            r28 = r30
            int r1 = j.c.a.a.c.n.c.a(r22, r23, r24, r25, r26, r27, r28)
            goto L_0x0427
        L_0x01c7:
            if (r6 != r12) goto L_0x0426
            r8 = 536870912(0x20000000, double:2.652494739E-315)
            long r8 = r25 & r8
            java.lang.String r1 = ""
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 != 0) goto L_0x021a
            int r4 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0215
            if (r6 != 0) goto L_0x01e2
            r11.add(r1)
            goto L_0x01ed
        L_0x01e2:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = j.c.a.a.f.e.y3.a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
        L_0x01ec:
            int r4 = r4 + r6
        L_0x01ed:
            if (r4 >= r5) goto L_0x0426
            int r6 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0426
            int r4 = j.c.a.a.c.n.c.a(r3, r6, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0210
            if (r6 != 0) goto L_0x0205
            r11.add(r1)
            goto L_0x01ed
        L_0x0205:
            java.lang.String r8 = new java.lang.String
            java.nio.charset.Charset r9 = j.c.a.a.f.e.y3.a
            r8.<init>(r3, r4, r6, r9)
            r11.add(r8)
            goto L_0x01ec
        L_0x0210:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.b()
            throw r1
        L_0x0215:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.b()
            throw r1
        L_0x021a:
            int r4 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x0275
            if (r6 != 0) goto L_0x0228
            r11.add(r1)
            goto L_0x023b
        L_0x0228:
            int r8 = r4 + r6
            boolean r9 = j.c.a.a.f.e.p6.a(r3, r4, r8)
            if (r9 == 0) goto L_0x0270
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = j.c.a.a.f.e.y3.a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
        L_0x023a:
            r4 = r8
        L_0x023b:
            if (r4 >= r5) goto L_0x0426
            int r6 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0426
            int r4 = j.c.a.a.c.n.c.a(r3, r6, r7)
            int r6 = r7.a
            if (r6 < 0) goto L_0x026b
            if (r6 != 0) goto L_0x0253
            r11.add(r1)
            goto L_0x023b
        L_0x0253:
            int r8 = r4 + r6
            boolean r9 = j.c.a.a.f.e.p6.a(r3, r4, r8)
            if (r9 == 0) goto L_0x0266
            java.lang.String r9 = new java.lang.String
            java.nio.charset.Charset r10 = j.c.a.a.f.e.y3.a
            r9.<init>(r3, r4, r6, r10)
            r11.add(r9)
            goto L_0x023a
        L_0x0266:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.f()
            throw r1
        L_0x026b:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.b()
            throw r1
        L_0x0270:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.f()
            throw r1
        L_0x0275:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.b()
            throw r1
        L_0x027a:
            r1 = 0
            if (r6 != r12) goto L_0x02a2
            j.c.a.a.f.e.u2 r11 = (j.c.a.a.f.e.u2) r11
            int r2 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r4 = r7.a
            int r4 = r4 + r2
        L_0x0286:
            if (r2 >= r4) goto L_0x0299
            int r2 = j.c.a.a.c.n.c.b(r3, r2, r7)
            long r5 = r7.b
            int r8 = (r5 > r14 ? 1 : (r5 == r14 ? 0 : -1))
            if (r8 == 0) goto L_0x0294
            r5 = 1
            goto L_0x0295
        L_0x0294:
            r5 = 0
        L_0x0295:
            r11.a(r5)
            goto L_0x0286
        L_0x0299:
            if (r2 != r4) goto L_0x029d
            goto L_0x0152
        L_0x029d:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x02a2:
            if (r6 != 0) goto L_0x0426
            j.c.a.a.f.e.u2 r11 = (j.c.a.a.f.e.u2) r11
            int r4 = j.c.a.a.c.n.c.b(r3, r4, r7)
            long r8 = r7.b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02b2
            r6 = 1
            goto L_0x02b3
        L_0x02b2:
            r6 = 0
        L_0x02b3:
            r11.a(r6)
        L_0x02b6:
            if (r4 >= r5) goto L_0x0426
            int r6 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r8 = r7.a
            if (r2 != r8) goto L_0x0426
            int r4 = j.c.a.a.c.n.c.b(r3, r6, r7)
            long r8 = r7.b
            int r6 = (r8 > r14 ? 1 : (r8 == r14 ? 0 : -1))
            if (r6 == 0) goto L_0x02cc
            r6 = 1
            goto L_0x02cd
        L_0x02cc:
            r6 = 0
        L_0x02cd:
            r11.a(r6)
            goto L_0x02b6
        L_0x02d1:
            if (r6 != r12) goto L_0x02f1
            j.c.a.a.f.e.z3 r11 = (j.c.a.a.f.e.z3) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x02dc:
            if (r1 >= r2) goto L_0x02e8
            int r4 = j.c.a.a.c.n.c.b(r3, r1)
            r11.d(r4)
            int r1 = r1 + 4
            goto L_0x02dc
        L_0x02e8:
            if (r1 != r2) goto L_0x02ec
            goto L_0x0427
        L_0x02ec:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x02f1:
            if (r6 != r9) goto L_0x0426
            j.c.a.a.f.e.z3 r11 = (j.c.a.a.f.e.z3) r11
            int r1 = j.c.a.a.c.n.c.b(r18, r19)
            r11.d(r1)
        L_0x02fc:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            int r1 = j.c.a.a.c.n.c.b(r3, r4)
            r11.d(r1)
            goto L_0x02fc
        L_0x0310:
            if (r6 != r12) goto L_0x0330
            j.c.a.a.f.e.u4 r11 = (j.c.a.a.f.e.u4) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x031b:
            if (r1 >= r2) goto L_0x0327
            long r4 = j.c.a.a.c.n.c.c(r3, r1)
            r11.a(r4)
            int r1 = r1 + 8
            goto L_0x031b
        L_0x0327:
            if (r1 != r2) goto L_0x032b
            goto L_0x0427
        L_0x032b:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x0330:
            if (r6 != r13) goto L_0x0426
            j.c.a.a.f.e.u4 r11 = (j.c.a.a.f.e.u4) r11
            long r8 = j.c.a.a.c.n.c.c(r18, r19)
            r11.a(r8)
        L_0x033b:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            long r8 = j.c.a.a.c.n.c.c(r3, r4)
            r11.a(r8)
            goto L_0x033b
        L_0x034f:
            if (r6 != r12) goto L_0x0357
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r11, r7)
            goto L_0x0427
        L_0x0357:
            if (r6 != 0) goto L_0x0426
            r22 = r18
            r23 = r19
            r24 = r20
            r25 = r11
            r26 = r30
            int r1 = j.c.a.a.c.n.c.a(r21, r22, r23, r24, r25, r26)
            goto L_0x0427
        L_0x0369:
            if (r6 != r12) goto L_0x0389
            j.c.a.a.f.e.u4 r11 = (j.c.a.a.f.e.u4) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x0374:
            if (r1 >= r2) goto L_0x0380
            int r1 = j.c.a.a.c.n.c.b(r3, r1, r7)
            long r4 = r7.b
            r11.a(r4)
            goto L_0x0374
        L_0x0380:
            if (r1 != r2) goto L_0x0384
            goto L_0x0427
        L_0x0384:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x0389:
            if (r6 != 0) goto L_0x0426
            j.c.a.a.f.e.u4 r11 = (j.c.a.a.f.e.u4) r11
            int r1 = j.c.a.a.c.n.c.b(r3, r4, r7)
            long r8 = r7.b
            r11.a(r8)
        L_0x0396:
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            int r1 = j.c.a.a.c.n.c.b(r3, r4, r7)
            long r8 = r7.b
            r11.a(r8)
            goto L_0x0396
        L_0x03aa:
            if (r6 != r12) goto L_0x03c9
            j.c.a.a.f.e.s3 r11 = (j.c.a.a.f.e.s3) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x03b5:
            if (r1 >= r2) goto L_0x03c1
            float r4 = j.c.a.a.c.n.c.e(r3, r1)
            r11.a(r4)
            int r1 = r1 + 4
            goto L_0x03b5
        L_0x03c1:
            if (r1 != r2) goto L_0x03c4
            goto L_0x0427
        L_0x03c4:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x03c9:
            if (r6 != r9) goto L_0x0426
            j.c.a.a.f.e.s3 r11 = (j.c.a.a.f.e.s3) r11
            float r1 = j.c.a.a.c.n.c.e(r18, r19)
            r11.a(r1)
        L_0x03d4:
            int r1 = r4 + 4
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            float r1 = j.c.a.a.c.n.c.e(r3, r4)
            r11.a(r1)
            goto L_0x03d4
        L_0x03e8:
            if (r6 != r12) goto L_0x0407
            j.c.a.a.f.e.i3 r11 = (j.c.a.a.f.e.i3) r11
            int r1 = j.c.a.a.c.n.c.a(r3, r4, r7)
            int r2 = r7.a
            int r2 = r2 + r1
        L_0x03f3:
            if (r1 >= r2) goto L_0x03ff
            double r4 = j.c.a.a.c.n.c.d(r3, r1)
            r11.a(r4)
            int r1 = r1 + 8
            goto L_0x03f3
        L_0x03ff:
            if (r1 != r2) goto L_0x0402
            goto L_0x0427
        L_0x0402:
            com.google.android.gms.internal.measurement.zzfn r1 = com.google.android.gms.internal.measurement.zzfn.a()
            throw r1
        L_0x0407:
            if (r6 != r13) goto L_0x0426
            j.c.a.a.f.e.i3 r11 = (j.c.a.a.f.e.i3) r11
            double r8 = j.c.a.a.c.n.c.d(r18, r19)
            r11.a(r8)
        L_0x0412:
            int r1 = r4 + 8
            if (r1 >= r5) goto L_0x0427
            int r4 = j.c.a.a.c.n.c.a(r3, r1, r7)
            int r6 = r7.a
            if (r2 != r6) goto L_0x0427
            double r8 = j.c.a.a.c.n.c.d(r3, r4)
            r11.a(r8)
            goto L_0x0412
        L_0x0426:
            r1 = r4
        L_0x0427:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.h5.a(java.lang.Object, byte[], int, int, int, int, int, int, long, int, long, j.c.a.a.f.e.s2):int");
    }

    public final <K, V> int a(T t2, byte[] bArr, int i2, int i3, int i4, long j2, s2 s2Var) {
        Unsafe unsafe = f1854r;
        Object obj = this.b[(i4 / 3) << 1];
        Object object = unsafe.getObject(t2, j2);
        if (this.f1864p.b(object)) {
            Object f2 = this.f1864p.f(obj);
            this.f1864p.a(f2, object);
            unsafe.putObject(t2, j2, f2);
            object = f2;
        }
        this.f1864p.e(obj);
        this.f1864p.a(object);
        int a2 = c.a(bArr, i2, s2Var);
        int i5 = s2Var.a;
        if (i5 < 0 || i5 > i3 - a2) {
            throw zzfn.a();
        }
        throw null;
    }

    public final int a(T t2, byte[] bArr, int i2, int i3, int i4, int i5, int i6, int i7, int i8, long j2, int i9, s2 s2Var) {
        int i10;
        T t3 = t2;
        byte[] bArr2 = bArr;
        int i11 = i2;
        int i12 = i4;
        int i13 = i5;
        int i14 = i6;
        long j3 = j2;
        int i15 = i9;
        s2 s2Var2 = s2Var;
        Unsafe unsafe = f1854r;
        long j4 = (long) (this.a[i15 + 2] & 1048575);
        boolean z = true;
        switch (i8) {
            case 51:
                if (i14 == 1) {
                    unsafe.putObject(t3, j3, Double.valueOf(c.d(bArr, i2)));
                    i10 = i11 + 8;
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 52:
                if (i14 == 5) {
                    unsafe.putObject(t3, j3, Float.valueOf(c.e(bArr, i2)));
                    i10 = i11 + 4;
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 53:
            case 54:
                if (i14 == 0) {
                    i10 = c.b(bArr2, i11, s2Var2);
                    unsafe.putObject(t3, j3, Long.valueOf(s2Var2.b));
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 55:
            case 62:
                if (i14 == 0) {
                    i10 = c.a(bArr2, i11, s2Var2);
                    unsafe.putObject(t3, j3, Integer.valueOf(s2Var2.a));
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 56:
            case 65:
                if (i14 == 1) {
                    unsafe.putObject(t3, j3, Long.valueOf(c.c(bArr, i2)));
                    i10 = i11 + 8;
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 57:
            case 64:
                if (i14 == 5) {
                    unsafe.putObject(t3, j3, Integer.valueOf(c.b(bArr, i2)));
                    i10 = i11 + 4;
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 58:
                if (i14 == 0) {
                    i10 = c.b(bArr2, i11, s2Var2);
                    if (s2Var2.b == 0) {
                        z = false;
                    }
                    unsafe.putObject(t3, j3, Boolean.valueOf(z));
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 59:
                if (i14 == 2) {
                    int a2 = c.a(bArr2, i11, s2Var2);
                    int i16 = s2Var2.a;
                    if (i16 == 0) {
                        unsafe.putObject(t3, j3, "");
                    } else if ((i7 & 536870912) == 0 || p6.a(bArr2, a2, a2 + i16)) {
                        unsafe.putObject(t3, j3, new String(bArr2, a2, i16, y3.a));
                        a2 += i16;
                    } else {
                        throw zzfn.f();
                    }
                    unsafe.putInt(t3, j4, i13);
                    return a2;
                }
                return i11;
            case 60:
                if (i14 == 2) {
                    int a3 = c.a(a(i15), bArr2, i11, i3, s2Var2);
                    Object object = unsafe.getInt(t3, j4) == i13 ? unsafe.getObject(t3, j3) : null;
                    if (object == null) {
                        unsafe.putObject(t3, j3, s2Var2.c);
                    } else {
                        unsafe.putObject(t3, j3, y3.a(object, s2Var2.c));
                    }
                    unsafe.putInt(t3, j4, i13);
                    return a3;
                }
                return i11;
            case 61:
                if (i14 == 2) {
                    i10 = c.e(bArr2, i11, s2Var2);
                    unsafe.putObject(t3, j3, s2Var2.c);
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 63:
                if (i14 == 0) {
                    int a4 = c.a(bArr2, i11, s2Var2);
                    int i17 = s2Var2.a;
                    d4 d4Var = (d4) this.b[((i15 / 3) << 1) + 1];
                    if (d4Var == null || d4Var.a(i17)) {
                        unsafe.putObject(t3, j3, Integer.valueOf(i17));
                        i10 = a4;
                        unsafe.putInt(t3, j4, i13);
                        return i10;
                    }
                    e(t2).a(i12, Long.valueOf((long) i17));
                    return a4;
                }
                return i11;
            case 66:
                if (i14 == 0) {
                    i10 = c.a(bArr2, i11, s2Var2);
                    unsafe.putObject(t3, j3, Integer.valueOf(e3.a(s2Var2.a)));
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 67:
                if (i14 == 0) {
                    i10 = c.b(bArr2, i11, s2Var2);
                    unsafe.putObject(t3, j3, Long.valueOf(e3.a(s2Var2.b)));
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            case 68:
                if (i14 == 3) {
                    i10 = c.a(a(i15), bArr, i2, i3, (i12 & -8) | 4, s2Var);
                    Object object2 = unsafe.getInt(t3, j4) == i13 ? unsafe.getObject(t3, j3) : null;
                    if (object2 == null) {
                        unsafe.putObject(t3, j3, s2Var2.c);
                    } else {
                        unsafe.putObject(t3, j3, y3.a(object2, s2Var2.c));
                    }
                    unsafe.putInt(t3, j4, i13);
                    return i10;
                }
                return i11;
            default:
                return i11;
        }
    }

    public final t5 a(int i2) {
        int i3 = (i2 / 3) << 1;
        Object[] objArr = this.b;
        t5 t5Var = (t5) objArr[i3];
        if (t5Var != null) {
            return t5Var;
        }
        t5 a2 = o5.c.a((Class) objArr[i3 + 1]);
        this.b[i3] = a2;
        return a2;
    }

    /* JADX WARN: Type inference failed for: r37v0, types: [int] */
    /* JADX WARN: Type inference failed for: r10v29, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:143:0x046c A[ADDED_TO_REGION] */
    /* JADX WARNING: Removed duplicated region for block: B:147:0x0480  */
    /* JADX WARNING: Removed duplicated region for block: B:154:0x04bc  */
    /* JADX WARNING: Removed duplicated region for block: B:22:0x005b  */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0067  */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final int a(T r33, byte[] r34, int r35, int r36, int r37, j.c.a.a.f.e.s2 r38) {
        /*
            r32 = this;
            r15 = r32
            r14 = r34
            r12 = r36
            r13 = r38
            sun.misc.Unsafe r11 = j.c.a.a.f.e.h5.f1854r
            r6 = r33
            r0 = r35
            r1 = r37
            r4 = r13
            r5 = r15
            r2 = -1
            r3 = 0
            r7 = -1
            r8 = 0
            r16 = 0
        L_0x0018:
            r17 = 1048575(0xfffff, float:1.469367E-39)
            r18 = 0
            if (r0 >= r12) goto L_0x0501
            int r10 = r0 + 1
            byte r0 = r14[r0]
            if (r0 >= 0) goto L_0x0030
            int r0 = j.c.a.a.c.n.c.a(r0, r14, r10, r4)
            int r10 = r4.a
            r31 = r10
            r10 = r0
            r0 = r31
        L_0x0030:
            int r9 = r0 >>> 3
            r13 = r0 & 7
            r16 = r0
            r0 = 3
            if (r9 <= r2) goto L_0x0049
            int r3 = r3 / r0
            int r2 = r5.c
            if (r9 < r2) goto L_0x0047
            int r2 = r5.d
            if (r9 > r2) goto L_0x0047
            int r2 = r5.a(r9, r3)
            goto L_0x0058
        L_0x0047:
            r3 = 0
            goto L_0x0057
        L_0x0049:
            int r2 = r5.c
            if (r9 < r2) goto L_0x0047
            int r2 = r5.d
            if (r9 > r2) goto L_0x0047
            r3 = 0
            int r2 = r5.a(r9, r3)
            goto L_0x0058
        L_0x0057:
            r2 = -1
        L_0x0058:
            r3 = -1
            if (r2 != r3) goto L_0x0067
            r21 = r9
            r15 = r16
            r3 = 0
            r4 = 1
            r16 = 0
            r20 = -1
            goto L_0x0342
        L_0x0067:
            int[] r1 = r5.a
            int r3 = r2 + 1
            r3 = r1[r3]
            r21 = 267386880(0xff00000, float:2.3665827E-29)
            r21 = r3 & r21
            int r0 = r21 >>> 20
            r21 = r4
            r4 = r3 & r17
            long r14 = (long) r4
            r4 = 17
            r23 = r3
            if (r0 > r4) goto L_0x034f
            int r4 = r2 + 2
            r1 = r1[r4]
            int r4 = r1 >>> 20
            r24 = 1
            int r25 = r24 << r4
            r1 = r1 & r17
            if (r1 == r7) goto L_0x0099
            r4 = -1
            if (r7 == r4) goto L_0x0093
            long r3 = (long) r7
            r11.putInt(r6, r3, r8)
        L_0x0093:
            long r3 = (long) r1
            int r8 = r11.getInt(r6, r3)
            r7 = r1
        L_0x0099:
            r1 = 5
            switch(r0) {
                case 0: goto L_0x0315;
                case 1: goto L_0x02f4;
                case 2: goto L_0x02cc;
                case 3: goto L_0x02cc;
                case 4: goto L_0x02ad;
                case 5: goto L_0x0289;
                case 6: goto L_0x0258;
                case 7: goto L_0x022e;
                case 8: goto L_0x0205;
                case 9: goto L_0x01cf;
                case 10: goto L_0x01ab;
                case 11: goto L_0x02ad;
                case 12: goto L_0x016b;
                case 13: goto L_0x0258;
                case 14: goto L_0x0289;
                case 15: goto L_0x0149;
                case 16: goto L_0x0106;
                case 17: goto L_0x00ad;
                default: goto L_0x009d;
            }
        L_0x009d:
            r14 = r34
            r5 = r2
            r19 = r7
            r21 = r9
            r15 = r16
            r4 = 1
            r16 = 0
            r20 = -1
            goto L_0x033d
        L_0x00ad:
            r0 = 3
            if (r13 != r0) goto L_0x00f6
            int r0 = r9 << 3
            r4 = r0 | 4
            j.c.a.a.f.e.t5 r0 = r5.a(r2)
            r13 = r16
            r1 = r34
            r3 = r2
            r2 = r10
            r10 = r3
            r16 = 0
            r3 = r36
            r19 = r7
            r7 = r21
            r20 = -1
            r21 = r9
            r9 = r5
            r5 = r38
            int r0 = j.c.a.a.c.n.c.a(r0, r1, r2, r3, r4, r5)
            r1 = r8 & r25
            if (r1 != 0) goto L_0x00dc
            java.lang.Object r1 = r7.c
            r11.putObject(r6, r14, r1)
            goto L_0x00e9
        L_0x00dc:
            java.lang.Object r1 = r11.getObject(r6, r14)
            java.lang.Object r2 = r7.c
            java.lang.Object r1 = j.c.a.a.f.e.y3.a(r1, r2)
            r11.putObject(r6, r14, r1)
        L_0x00e9:
            r1 = r8 | r25
            r12 = r38
            r8 = r1
            r4 = r7
            r5 = r9
            r24 = r10
            r30 = r11
            r10 = r13
            goto L_0x013a
        L_0x00f6:
            r19 = r7
            r21 = r9
            r13 = r16
            r16 = 0
            r20 = -1
            r14 = r34
            r5 = r2
            r15 = r13
            goto L_0x0313
        L_0x0106:
            r4 = r2
            r19 = r7
            r7 = r21
            r20 = -1
            r21 = r9
            r9 = r5
            r5 = r16
            r16 = 0
            if (r13 != 0) goto L_0x0144
            r2 = r14
            r14 = r34
            int r10 = j.c.a.a.c.n.c.b(r14, r10, r7)
            long r0 = r7.b
            long r17 = j.c.a.a.f.e.e3.a(r0)
            r0 = r11
            r1 = r33
            r13 = r4
            r15 = r5
            r4 = r17
            r0.putLong(r1, r2, r4)
            r0 = r8 | r25
            r12 = r38
            r8 = r0
            r4 = r7
            r5 = r9
            r0 = r10
            r30 = r11
            r24 = r13
            r10 = r15
        L_0x013a:
            r7 = r19
            r9 = r21
            r20 = 0
            r11 = r32
            goto L_0x04f0
        L_0x0144:
            r14 = r34
            r15 = r5
            goto L_0x0286
        L_0x0149:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != 0) goto L_0x0286
            int r0 = j.c.a.a.c.n.c.a(r14, r10, r7)
            int r1 = r7.a
            int r1 = j.c.a.a.f.e.e3.a(r1)
            r11.putInt(r6, r2, r1)
            goto L_0x01c8
        L_0x016b:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != 0) goto L_0x0286
            int r0 = j.c.a.a.c.n.c.a(r14, r10, r7)
            int r1 = r7.a
            j.c.a.a.f.e.d4 r5 = r9.c(r4)
            if (r5 == 0) goto L_0x01a2
            boolean r5 = r5.a(r1)
            if (r5 == 0) goto L_0x0191
            goto L_0x01a2
        L_0x0191:
            j.c.a.a.f.e.i6 r2 = e(r33)
            r35 = r0
            long r0 = (long) r1
            java.lang.Long r0 = java.lang.Long.valueOf(r0)
            r2.a(r15, r0)
        L_0x019f:
            r0 = r35
            goto L_0x01cb
        L_0x01a2:
            r35 = r0
            r11.putInt(r6, r2, r1)
            r0 = r8 | r25
            r8 = r0
            goto L_0x019f
        L_0x01ab:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r0 = 2
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != r0) goto L_0x0286
            int r0 = j.c.a.a.c.n.c.e(r14, r10, r7)
            java.lang.Object r1 = r7.c
            r11.putObject(r6, r2, r1)
        L_0x01c8:
            r1 = r8 | r25
            r8 = r1
        L_0x01cb:
            r24 = r4
            goto L_0x033a
        L_0x01cf:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r0 = 2
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != r0) goto L_0x0286
            j.c.a.a.f.e.t5 r0 = r9.a(r4)
            int r0 = j.c.a.a.c.n.c.a(r0, r14, r10, r12, r7)
            r1 = r8 & r25
            if (r1 != 0) goto L_0x01f6
            java.lang.Object r1 = r7.c
            r11.putObject(r6, r2, r1)
            goto L_0x0274
        L_0x01f6:
            java.lang.Object r1 = r11.getObject(r6, r2)
            java.lang.Object r5 = r7.c
            java.lang.Object r1 = j.c.a.a.f.e.y3.a(r1, r5)
            r11.putObject(r6, r2, r1)
            goto L_0x0274
        L_0x0205:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r0 = 2
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != r0) goto L_0x0286
            r0 = 536870912(0x20000000, float:1.0842022E-19)
            r0 = r23 & r0
            if (r0 != 0) goto L_0x0224
            int r0 = j.c.a.a.c.n.c.c(r14, r10, r7)
            goto L_0x0228
        L_0x0224:
            int r0 = j.c.a.a.c.n.c.d(r14, r10, r7)
        L_0x0228:
            java.lang.Object r1 = r7.c
            r11.putObject(r6, r2, r1)
            goto L_0x0274
        L_0x022e:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != 0) goto L_0x0286
            int r0 = j.c.a.a.c.n.c.b(r14, r10, r7)
            r5 = r0
            long r0 = r7.b
            r17 = 0
            int r10 = (r0 > r17 ? 1 : (r0 == r17 ? 0 : -1))
            if (r10 == 0) goto L_0x0250
            r10 = 1
            goto L_0x0251
        L_0x0250:
            r10 = 0
        L_0x0251:
            j.c.a.a.f.e.m6$d r0 = j.c.a.a.f.e.m6.f1878f
            r0.a(r6, r2, r10)
            r0 = r5
            goto L_0x0274
        L_0x0258:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != r1) goto L_0x0286
            int r0 = j.c.a.a.c.n.c.b(r14, r10)
            r11.putInt(r6, r2, r0)
            int r0 = r10 + 4
        L_0x0274:
            r1 = r8 | r25
            r8 = r1
            r1 = r37
            r13 = r38
            r3 = r4
            r4 = r7
            r5 = r9
            r16 = r15
            r7 = r19
            r2 = r21
            goto L_0x03ac
        L_0x0286:
            r5 = r4
            goto L_0x0313
        L_0x0289:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r0 = 1
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != r0) goto L_0x0286
            long r17 = j.c.a.a.c.n.c.c(r14, r10)
            r0 = r11
            r1 = r33
            r13 = r4
            r4 = r17
            r0.putLong(r1, r2, r4)
            r5 = r13
            goto L_0x0330
        L_0x02ad:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != 0) goto L_0x0286
            int r0 = j.c.a.a.c.n.c.a(r14, r10, r7)
            int r1 = r7.a
            r11.putInt(r6, r2, r1)
            r5 = r4
            goto L_0x0332
        L_0x02cc:
            r4 = r2
            r19 = r7
            r2 = r14
            r15 = r16
            r7 = r21
            r16 = 0
            r20 = -1
            r14 = r34
            r21 = r9
            r9 = r5
            if (r13 != 0) goto L_0x0286
            int r10 = j.c.a.a.c.n.c.b(r14, r10, r7)
            long r0 = r7.b
            r17 = r0
            r0 = r11
            r1 = r33
            r13 = r4
            r4 = r17
            r0.putLong(r1, r2, r4)
            r0 = r8 | r25
            r5 = r13
            goto L_0x0336
        L_0x02f4:
            r19 = r7
            r7 = r21
            r20 = -1
            r21 = r9
            r9 = r5
            r5 = r2
            r2 = r14
            r15 = r16
            r16 = 0
            r14 = r34
            if (r13 != r1) goto L_0x0313
            float r0 = j.c.a.a.c.n.c.e(r14, r10)
            j.c.a.a.f.e.m6$d r1 = j.c.a.a.f.e.m6.f1878f
            r1.a(r6, r2, r0)
            int r0 = r10 + 4
            goto L_0x0332
        L_0x0313:
            r4 = 1
            goto L_0x033d
        L_0x0315:
            r19 = r7
            r7 = r21
            r4 = 1
            r20 = -1
            r21 = r9
            r9 = r5
            r5 = r2
            r2 = r14
            r15 = r16
            r16 = 0
            r14 = r34
            if (r13 != r4) goto L_0x033d
            double r0 = j.c.a.a.c.n.c.d(r14, r10)
            j.c.a.a.f.e.m6.a(r6, r2, r0)
        L_0x0330:
            int r0 = r10 + 8
        L_0x0332:
            r1 = r8 | r25
            r10 = r0
            r0 = r1
        L_0x0336:
            r8 = r0
            r24 = r5
            r0 = r10
        L_0x033a:
            r10 = r6
            goto L_0x039d
        L_0x033d:
            r1 = r37
            r3 = r5
            r7 = r19
        L_0x0342:
            r6 = r1
            r24 = r3
            r2 = r10
            r30 = r11
            r10 = r15
            r20 = 0
            r29 = 1
            goto L_0x046a
        L_0x034f:
            r22 = r7
            r7 = r21
            r4 = 1
            r20 = -1
            r21 = r9
            r9 = r5
            r5 = r2
            r2 = r14
            r15 = r16
            r16 = 0
            r14 = r34
            r1 = 27
            if (r0 != r1) goto L_0x03bf
            r1 = 2
            if (r13 != r1) goto L_0x03b0
            java.lang.Object r0 = r11.getObject(r6, r2)
            j.c.a.a.f.e.e4 r0 = (j.c.a.a.f.e.e4) r0
            boolean r1 = r0.a()
            if (r1 != 0) goto L_0x0386
            int r1 = r0.size()
            if (r1 != 0) goto L_0x037d
            r1 = 10
            goto L_0x037f
        L_0x037d:
            int r1 = r1 << 1
        L_0x037f:
            j.c.a.a.f.e.e4 r0 = r0.a(r1)
            r11.putObject(r6, r2, r0)
        L_0x0386:
            r13 = r0
            j.c.a.a.f.e.t5 r0 = r9.a(r5)
            r1 = r15
            r2 = r34
            r3 = r10
            r4 = r36
            r24 = r5
            r5 = r13
            r10 = r6
            r6 = r38
            int r0 = j.c.a.a.c.n.c.a(r0, r1, r2, r3, r4, r5, r6)
            r19 = r22
        L_0x039d:
            r1 = r37
            r13 = r38
            r4 = r7
            r5 = r9
            r6 = r10
            r16 = r15
            r7 = r19
            r2 = r21
            r3 = r24
        L_0x03ac:
            r15 = r32
            goto L_0x0018
        L_0x03b0:
            r24 = r5
            r23 = r8
            r30 = r11
            r16 = r15
            r20 = 0
            r29 = 1
            r15 = r10
            goto L_0x0430
        L_0x03bf:
            r24 = r5
            r1 = 49
            if (r0 > r1) goto L_0x0416
            r5 = r23
            long r6 = (long) r5
            r9 = r0
            r0 = r32
            r1 = r33
            r25 = r2
            r2 = r34
            r3 = r10
            r19 = 1
            r4 = r36
            r5 = r15
            r27 = r6
            r6 = r21
            r7 = r13
            r23 = r8
            r8 = r24
            r35 = r9
            r13 = r10
            r20 = 0
            r29 = 1
            r9 = r27
            r30 = r11
            r11 = r35
            r16 = r15
            r15 = r13
            r12 = r25
            r14 = r38
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x03fc
            goto L_0x0460
        L_0x03fc:
            r5 = r32
            r15 = r5
            r6 = r33
            r14 = r34
            r12 = r36
            r1 = r37
            r4 = r38
            r13 = r4
            r2 = r21
            r7 = r22
            r8 = r23
            r3 = r24
            r11 = r30
            goto L_0x0018
        L_0x0416:
            r35 = r0
            r25 = r2
            r30 = r11
            r16 = r15
            r5 = r23
            r20 = 0
            r29 = 1
            r23 = r8
            r15 = r10
            r0 = 50
            r9 = r35
            if (r9 != r0) goto L_0x0445
            r0 = 2
            if (r13 == r0) goto L_0x0432
        L_0x0430:
            r10 = r15
            goto L_0x0461
        L_0x0432:
            r0 = r32
            r1 = r33
            r2 = r34
            r3 = r15
            r4 = r36
            r5 = r24
            r6 = r25
            r8 = r38
            r0.a(r1, r2, r3, r4, r5, r6, r8)
            throw r18
        L_0x0445:
            r0 = r32
            r1 = r33
            r2 = r34
            r8 = r5
            r3 = r15
            r4 = r36
            r5 = r16
            r6 = r21
            r7 = r13
            r10 = r25
            r12 = r24
            r13 = r38
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x04e0
        L_0x0460:
            r10 = r0
        L_0x0461:
            r6 = r37
            r2 = r10
            r10 = r16
            r7 = r22
            r8 = r23
        L_0x046a:
            if (r10 != r6) goto L_0x047a
            if (r6 != 0) goto L_0x046f
            goto L_0x047a
        L_0x046f:
            r5 = r32
            r3 = -1
            r11 = r5
            r0 = r2
            r1 = r6
            r2 = r10
            r10 = r33
            goto L_0x050f
        L_0x047a:
            r11 = r32
            boolean r0 = r11.f1856f
            if (r0 == 0) goto L_0x04bc
            r12 = r38
            j.c.a.a.f.e.j3 r0 = r12.d
            j.c.a.a.f.e.j3 r1 = j.c.a.a.f.e.j3.a()
            if (r0 == r1) goto L_0x04be
            j.c.a.a.f.e.f5 r0 = r11.f1855e
            j.c.a.a.f.e.j3 r1 = r12.d
            java.util.Map<j.c.a.a.f.e.j3$a, j.c.a.a.f.e.x3$e<?, ?>> r1 = r1.a
            j.c.a.a.f.e.j3$a r3 = new j.c.a.a.f.e.j3$a
            r9 = r21
            r3.<init>(r0, r9)
            java.lang.Object r0 = r1.get(r3)
            j.c.a.a.f.e.x3$e r0 = (j.c.a.a.f.e.x3.e) r0
            if (r0 != 0) goto L_0x04af
            j.c.a.a.f.e.i6 r4 = e(r33)
            r0 = r10
            r1 = r34
            r3 = r36
            r5 = r38
            int r0 = j.c.a.a.c.n.c.a(r0, r1, r2, r3, r4, r5)
            goto L_0x04cf
        L_0x04af:
            r0 = r33
            j.c.a.a.f.e.x3$b r0 = (j.c.a.a.f.e.x3.b) r0
            r0.a()
            java.lang.NoSuchMethodError r0 = new java.lang.NoSuchMethodError
            r0.<init>()
            throw r0
        L_0x04bc:
            r12 = r38
        L_0x04be:
            r9 = r21
            j.c.a.a.f.e.i6 r4 = e(r33)
            r0 = r10
            r1 = r34
            r3 = r36
            r5 = r38
            int r0 = j.c.a.a.c.n.c.a(r0, r1, r2, r3, r4, r5)
        L_0x04cf:
            r14 = r34
            r1 = r6
            r2 = r9
            r16 = r10
            r5 = r11
            r15 = r5
            r4 = r12
            r13 = r4
            r3 = r24
            r11 = r30
            r6 = r33
            goto L_0x04fd
        L_0x04e0:
            r11 = r32
            r12 = r38
            r10 = r16
            r9 = r21
            r6 = r33
            r5 = r11
            r4 = r12
            r7 = r22
            r8 = r23
        L_0x04f0:
            r14 = r34
            r1 = r37
            r2 = r9
            r16 = r10
            r15 = r11
            r13 = r12
            r3 = r24
            r11 = r30
        L_0x04fd:
            r12 = r36
            goto L_0x0018
        L_0x0501:
            r9 = r5
            r10 = r6
            r22 = r7
            r23 = r8
            r30 = r11
            r11 = r15
            r29 = 1
            r2 = r16
            r3 = -1
        L_0x050f:
            if (r7 == r3) goto L_0x0517
            long r3 = (long) r7
            r6 = r30
            r6.putInt(r10, r3, r8)
        L_0x0517:
            int r3 = r5.f1858j
        L_0x0519:
            int r4 = r5.f1859k
            if (r3 >= r4) goto L_0x0589
            int[] r4 = r5.f1857i
            r4 = r4[r3]
            j.c.a.a.f.e.g6<?, ?> r6 = r5.f1862n
            int[] r7 = r5.a
            r8 = r7[r4]
            int r8 = r4 + 1
            r7 = r7[r8]
            r7 = r7 & r17
            long r7 = (long) r7
            java.lang.Object r7 = j.c.a.a.f.e.m6.f(r10, r7)
            if (r7 != 0) goto L_0x0535
            goto L_0x0586
        L_0x0535:
            java.lang.Object[] r8 = r5.b
            int r4 = r4 / 3
            int r4 = r4 << 1
            int r9 = r4 + 1
            r8 = r8[r9]
            j.c.a.a.f.e.d4 r8 = (j.c.a.a.f.e.d4) r8
            if (r8 != 0) goto L_0x0544
            goto L_0x0586
        L_0x0544:
            j.c.a.a.f.e.y4 r9 = r5.f1864p
            java.util.Map r7 = r9.a(r7)
            j.c.a.a.f.e.y4 r9 = r5.f1864p
            java.lang.Object[] r12 = r5.b
            r4 = r12[r4]
            r9.e(r4)
            java.util.Set r4 = r7.entrySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x055b:
            boolean r7 = r4.hasNext()
            if (r7 == 0) goto L_0x0586
            java.lang.Object r7 = r4.next()
            java.util.Map$Entry r7 = (java.util.Map.Entry) r7
            java.lang.Object r9 = r7.getValue()
            java.lang.Integer r9 = (java.lang.Integer) r9
            int r9 = r9.intValue()
            boolean r9 = r8.a(r9)
            if (r9 != 0) goto L_0x055b
            j.c.a.a.f.e.h6 r6 = (j.c.a.a.f.e.h6) r6
            if (r6 == 0) goto L_0x0585
            j.c.a.a.f.e.i6.b()
            r7.getKey()
            r7.getValue()
            throw r18
        L_0x0585:
            throw r18
        L_0x0586:
            int r3 = r3 + 1
            goto L_0x0519
        L_0x0589:
            if (r1 != 0) goto L_0x0595
            r3 = r36
            if (r0 != r3) goto L_0x0590
            goto L_0x059b
        L_0x0590:
            com.google.android.gms.internal.measurement.zzfn r0 = com.google.android.gms.internal.measurement.zzfn.e()
            throw r0
        L_0x0595:
            r3 = r36
            if (r0 > r3) goto L_0x059c
            if (r2 != r1) goto L_0x059c
        L_0x059b:
            return r0
        L_0x059c:
            com.google.android.gms.internal.measurement.zzfn r0 = com.google.android.gms.internal.measurement.zzfn.e()
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.h5.a(java.lang.Object, byte[], int, int, int, j.c.a.a.f.e.s2):int");
    }

    /* JADX WARN: Type inference failed for: r3v18, types: [int] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
     arg types: [T, long, boolean]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void
     arg types: [T, long, float]
     candidates:
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, byte):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, double):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, int):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, long):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, boolean):void
      j.c.a.a.f.e.m6.d.a(java.lang.Object, long, float):void */
    /* JADX WARNING: Code restructure failed: missing block: B:105:0x0267, code lost:
        if (r0 == r15) goto L_0x0269;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:106:0x0269, code lost:
        r18 = r0;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:96:0x021f, code lost:
        if (r0 == r15) goto L_0x0269;
     */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:23:0x0051  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(T r27, byte[] r28, int r29, int r30, j.c.a.a.f.e.s2 r31) {
        /*
            r26 = this;
            r15 = r26
            boolean r0 = r15.g
            if (r0 == 0) goto L_0x029b
            sun.misc.Unsafe r14 = j.c.a.a.f.e.h5.f1854r
            r12 = -1
            r13 = 0
            r11 = r27
            r8 = r28
            r0 = r29
            r7 = r30
            r9 = r31
            r10 = r15
            r1 = -1
            r2 = 0
        L_0x0017:
            if (r0 >= r7) goto L_0x0293
            int r3 = r0 + 1
            byte r0 = r8[r0]
            if (r0 >= 0) goto L_0x0028
            int r0 = j.c.a.a.c.n.c.a(r0, r8, r3, r9)
            int r3 = r9.a
            r17 = r3
            goto L_0x002b
        L_0x0028:
            r17 = r0
            r0 = r3
        L_0x002b:
            int r5 = r17 >>> 3
            r6 = r17 & 7
            if (r5 <= r1) goto L_0x0040
            int r2 = r2 / 3
            int r1 = r10.c
            if (r5 < r1) goto L_0x004e
            int r1 = r10.d
            if (r5 > r1) goto L_0x004e
            int r1 = r10.a(r5, r2)
            goto L_0x004c
        L_0x0040:
            int r1 = r10.c
            if (r5 < r1) goto L_0x004e
            int r1 = r10.d
            if (r5 > r1) goto L_0x004e
            int r1 = r10.a(r5, r13)
        L_0x004c:
            r3 = r1
            goto L_0x004f
        L_0x004e:
            r3 = -1
        L_0x004f:
            if (r3 != r12) goto L_0x005d
            r18 = r0
            r25 = r5
            r24 = r14
            r22 = -1
            r23 = 0
            goto L_0x026d
        L_0x005d:
            int[] r1 = r10.a
            int r2 = r3 + 1
            r4 = r1[r2]
            r1 = 267386880(0xff00000, float:2.3665827E-29)
            r1 = r1 & r4
            int r2 = r1 >>> 20
            r1 = 1048575(0xfffff, float:1.469367E-39)
            r1 = r1 & r4
            long r12 = (long) r1
            r1 = 17
            r16 = r3
            r3 = 2
            if (r2 > r1) goto L_0x01a8
            r1 = 1
            switch(r2) {
                case 0: goto L_0x0190;
                case 1: goto L_0x017d;
                case 2: goto L_0x0163;
                case 3: goto L_0x0163;
                case 4: goto L_0x0153;
                case 5: goto L_0x013d;
                case 6: goto L_0x012e;
                case 7: goto L_0x0113;
                case 8: goto L_0x00f8;
                case 9: goto L_0x00d2;
                case 10: goto L_0x00c1;
                case 11: goto L_0x0153;
                case 12: goto L_0x00b0;
                case 13: goto L_0x012e;
                case 14: goto L_0x013d;
                case 15: goto L_0x009b;
                case 16: goto L_0x0079;
                default: goto L_0x0078;
            }
        L_0x0078:
            goto L_0x0095
        L_0x0079:
            if (r6 != 0) goto L_0x0095
            int r0 = j.c.a.a.c.n.c.b(r8, r0, r9)
            long r1 = r9.b
            long r17 = j.c.a.a.f.e.e3.a(r1)
            r1 = r14
            r2 = r27
            r6 = r16
            r3 = r12
            r25 = r5
            r12 = r6
            r5 = r17
            r1.putLong(r2, r3, r5)
            goto L_0x017b
        L_0x0095:
            r25 = r5
            r5 = r16
            goto L_0x01eb
        L_0x009b:
            r25 = r5
            r5 = r16
            if (r6 != 0) goto L_0x01eb
            int r0 = j.c.a.a.c.n.c.a(r8, r0, r9)
            int r1 = r9.a
            int r1 = j.c.a.a.f.e.e3.a(r1)
            r14.putInt(r11, r12, r1)
            goto L_0x019f
        L_0x00b0:
            r25 = r5
            r5 = r16
            if (r6 != 0) goto L_0x01eb
            int r0 = j.c.a.a.c.n.c.a(r8, r0, r9)
            int r1 = r9.a
            r14.putInt(r11, r12, r1)
            goto L_0x019f
        L_0x00c1:
            r25 = r5
            r5 = r16
            if (r6 != r3) goto L_0x01eb
            int r0 = j.c.a.a.c.n.c.e(r8, r0, r9)
            java.lang.Object r1 = r9.c
            r14.putObject(r11, r12, r1)
            goto L_0x019f
        L_0x00d2:
            r25 = r5
            r5 = r16
            if (r6 != r3) goto L_0x01eb
            j.c.a.a.f.e.t5 r1 = r10.a(r5)
            int r0 = j.c.a.a.c.n.c.a(r1, r8, r0, r7, r9)
            java.lang.Object r1 = r14.getObject(r11, r12)
            if (r1 != 0) goto L_0x00ed
            java.lang.Object r1 = r9.c
            r14.putObject(r11, r12, r1)
            goto L_0x019f
        L_0x00ed:
            java.lang.Object r2 = r9.c
            java.lang.Object r1 = j.c.a.a.f.e.y3.a(r1, r2)
            r14.putObject(r11, r12, r1)
            goto L_0x019f
        L_0x00f8:
            r25 = r5
            r5 = r16
            if (r6 != r3) goto L_0x01eb
            r1 = 536870912(0x20000000, float:1.0842022E-19)
            r1 = r1 & r4
            if (r1 != 0) goto L_0x0108
            int r0 = j.c.a.a.c.n.c.c(r8, r0, r9)
            goto L_0x010c
        L_0x0108:
            int r0 = j.c.a.a.c.n.c.d(r8, r0, r9)
        L_0x010c:
            java.lang.Object r1 = r9.c
            r14.putObject(r11, r12, r1)
            goto L_0x019f
        L_0x0113:
            r25 = r5
            r5 = r16
            if (r6 != 0) goto L_0x01eb
            int r0 = j.c.a.a.c.n.c.b(r8, r0, r9)
            long r2 = r9.b
            r16 = 0
            int r4 = (r2 > r16 ? 1 : (r2 == r16 ? 0 : -1))
            if (r4 == 0) goto L_0x0126
            goto L_0x0127
        L_0x0126:
            r1 = 0
        L_0x0127:
            j.c.a.a.f.e.m6$d r2 = j.c.a.a.f.e.m6.f1878f
            r2.a(r11, r12, r1)
            goto L_0x019f
        L_0x012e:
            r25 = r5
            r5 = r16
            r1 = 5
            if (r6 != r1) goto L_0x01eb
            int r1 = j.c.a.a.c.n.c.b(r8, r0)
            r14.putInt(r11, r12, r1)
            goto L_0x018d
        L_0x013d:
            r25 = r5
            r5 = r16
            if (r6 != r1) goto L_0x01eb
            long r16 = j.c.a.a.c.n.c.c(r8, r0)
            r1 = r14
            r2 = r27
            r3 = r12
            r12 = r5
            r5 = r16
            r1.putLong(r2, r3, r5)
            r5 = r12
            goto L_0x019d
        L_0x0153:
            r25 = r5
            r5 = r16
            if (r6 != 0) goto L_0x01eb
            int r0 = j.c.a.a.c.n.c.a(r8, r0, r9)
            int r1 = r9.a
            r14.putInt(r11, r12, r1)
            goto L_0x019f
        L_0x0163:
            r25 = r5
            r5 = r16
            if (r6 != 0) goto L_0x01eb
            int r0 = j.c.a.a.c.n.c.b(r8, r0, r9)
            long r3 = r9.b
            r1 = r14
            r2 = r27
            r16 = r3
            r3 = r12
            r12 = r5
            r5 = r16
            r1.putLong(r2, r3, r5)
        L_0x017b:
            r5 = r12
            goto L_0x019f
        L_0x017d:
            r25 = r5
            r5 = r16
            r1 = 5
            if (r6 != r1) goto L_0x01eb
            float r1 = j.c.a.a.c.n.c.e(r8, r0)
            j.c.a.a.f.e.m6$d r2 = j.c.a.a.f.e.m6.f1878f
            r2.a(r11, r12, r1)
        L_0x018d:
            int r0 = r0 + 4
            goto L_0x019f
        L_0x0190:
            r25 = r5
            r5 = r16
            if (r6 != r1) goto L_0x01eb
            double r1 = j.c.a.a.c.n.c.d(r8, r0)
            j.c.a.a.f.e.m6.a(r11, r12, r1)
        L_0x019d:
            int r0 = r0 + 8
        L_0x019f:
            r2 = r5
            r24 = r14
            r22 = -1
            r23 = 0
            goto L_0x0289
        L_0x01a8:
            r25 = r5
            r5 = r16
            r1 = 27
            if (r2 != r1) goto L_0x01f5
            if (r6 != r3) goto L_0x01eb
            java.lang.Object r1 = r14.getObject(r11, r12)
            j.c.a.a.f.e.e4 r1 = (j.c.a.a.f.e.e4) r1
            boolean r2 = r1.a()
            if (r2 != 0) goto L_0x01d0
            int r2 = r1.size()
            if (r2 != 0) goto L_0x01c7
            r2 = 10
            goto L_0x01c9
        L_0x01c7:
            int r2 = r2 << 1
        L_0x01c9:
            j.c.a.a.f.e.e4 r1 = r1.a(r2)
            r14.putObject(r11, r12, r1)
        L_0x01d0:
            r21 = r1
            j.c.a.a.f.e.t5 r16 = r10.a(r5)
            r18 = r28
            r19 = r0
            r20 = r30
            r22 = r31
            int r0 = j.c.a.a.c.n.c.a(r16, r17, r18, r19, r20, r21, r22)
            r3 = r5
            r24 = r14
            r22 = -1
            r23 = 0
            goto L_0x0288
        L_0x01eb:
            r15 = r0
            r16 = r5
            r24 = r14
            r22 = -1
            r23 = 0
            goto L_0x023a
        L_0x01f5:
            r1 = 49
            if (r2 > r1) goto L_0x0226
            long r9 = (long) r4
            r11 = r0
            r0 = r26
            r1 = r27
            r8 = r2
            r2 = r28
            r3 = r11
            r4 = r30
            r16 = r5
            r5 = r17
            r7 = r6
            r6 = r25
            r29 = r8
            r8 = r16
            r15 = r11
            r11 = r29
            r22 = -1
            r23 = 0
            r24 = r14
            r14 = r31
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r11, r12, r14)
            if (r0 != r15) goto L_0x0222
            goto L_0x0269
        L_0x0222:
            r3 = r16
            goto L_0x027e
        L_0x0226:
            r15 = r0
            r29 = r2
            r16 = r5
            r7 = r6
            r24 = r14
            r22 = -1
            r23 = 0
            r0 = 50
            r9 = r29
            if (r9 != r0) goto L_0x0250
            if (r7 == r3) goto L_0x023d
        L_0x023a:
            r18 = r15
            goto L_0x026b
        L_0x023d:
            r0 = r26
            r1 = r27
            r2 = r28
            r3 = r15
            r4 = r30
            r5 = r16
            r6 = r12
            r8 = r31
            r0.a(r1, r2, r3, r4, r5, r6, r8)
            r0 = 0
            throw r0
        L_0x0250:
            r0 = r26
            r1 = r27
            r2 = r28
            r3 = r15
            r8 = r4
            r4 = r30
            r5 = r17
            r6 = r25
            r10 = r12
            r12 = r16
            r13 = r31
            int r0 = r0.a(r1, r2, r3, r4, r5, r6, r7, r8, r9, r10, r12, r13)
            if (r0 != r15) goto L_0x0222
        L_0x0269:
            r18 = r0
        L_0x026b:
            r13 = r16
        L_0x026d:
            j.c.a.a.f.e.i6 r20 = e(r27)
            r16 = r17
            r17 = r28
            r19 = r30
            r21 = r31
            int r0 = j.c.a.a.c.n.c.a(r16, r17, r18, r19, r20, r21)
            r3 = r13
        L_0x027e:
            r10 = r26
            r11 = r27
            r8 = r28
            r7 = r30
            r9 = r31
        L_0x0288:
            r2 = r3
        L_0x0289:
            r12 = -1
            r13 = 0
            r15 = r26
            r14 = r24
            r1 = r25
            goto L_0x0017
        L_0x0293:
            if (r0 != r7) goto L_0x0296
            return
        L_0x0296:
            com.google.android.gms.internal.measurement.zzfn r0 = com.google.android.gms.internal.measurement.zzfn.e()
            throw r0
        L_0x029b:
            r5 = 0
            r0 = r26
            r1 = r27
            r2 = r28
            r3 = r29
            r4 = r30
            r6 = r31
            r0.a(r1, r2, r3, r4, r5, r6)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.h5.a(java.lang.Object, byte[], int, int, j.c.a.a.f.e.s2):void");
    }

    public static void a(int i2, Object obj, c7 c7Var) {
        if (obj instanceof String) {
            zzek.a aVar = (zzek.a) ((g3) c7Var).a;
            aVar.b((i2 << 3) | 2);
            aVar.b((String) obj);
            return;
        }
        zzek.a aVar2 = (zzek.a) ((g3) c7Var).a;
        aVar2.b((i2 << 3) | 2);
        aVar2.b((w2) obj);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      j.c.a.a.f.e.h5.a(java.lang.Object, long):java.util.List<?>
      j.c.a.a.f.e.h5.a(int, int):int
      j.c.a.a.f.e.h5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.b(java.lang.Object, int):void
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.b(java.lang.Object, long):double
      j.c.a.a.f.e.h5.b(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.b(java.lang.Object, java.lang.Object):void
      j.c.a.a.f.e.t5.b(java.lang.Object, java.lang.Object):void
      j.c.a.a.f.e.h5.b(java.lang.Object, int):void */
    public final void a(T t2, T t3, int i2) {
        long j2 = (long) (this.a[i2 + 1] & 1048575);
        if (a((Object) t3, i2)) {
            Object f2 = m6.f(t2, j2);
            Object f3 = m6.f(t3, j2);
            if (f2 != null && f3 != null) {
                m6.a(t2, j2, y3.a(f2, f3));
                b((Object) t2, i2);
            } else if (f3 != null) {
                m6.a(t2, j2, f3);
                b((Object) t2, i2);
            }
        }
    }

    public final boolean a(T t2, int i2, int i3) {
        return m6.a(t2, (long) (this.a[i3 + 2] & 1048575)) == i2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean
     arg types: [T, int]
     candidates:
      j.c.a.a.f.e.h5.a(java.lang.Class<?>, java.lang.String):java.lang.reflect.Field
      j.c.a.a.f.e.h5.a(java.lang.Object, long):java.util.List<?>
      j.c.a.a.f.e.h5.a(int, int):int
      j.c.a.a.f.e.h5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.h5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.h5.a(java.lang.Object, int):boolean */
    public final boolean a(T t2, int i2, int i3, int i4) {
        if (this.g) {
            return a((Object) t2, i2);
        }
        return (i3 & i4) != 0;
    }

    public final boolean a(T t2, int i2) {
        if (this.g) {
            int i3 = this.a[i2 + 1];
            long j2 = (long) (i3 & 1048575);
            switch ((i3 & 267386880) >>> 20) {
                case 0:
                    return m6.e(t2, j2) != 0.0d;
                case 1:
                    return m6.d(t2, j2) != 0.0f;
                case 2:
                    return m6.b(t2, j2) != 0;
                case 3:
                    return m6.b(t2, j2) != 0;
                case 4:
                    return m6.a(t2, j2) != 0;
                case 5:
                    return m6.b(t2, j2) != 0;
                case 6:
                    return m6.a(t2, j2) != 0;
                case 7:
                    return m6.c(t2, j2);
                case 8:
                    Object f2 = m6.f(t2, j2);
                    if (f2 instanceof String) {
                        return !((String) f2).isEmpty();
                    }
                    if (f2 instanceof w2) {
                        return !w2.c.equals(f2);
                    }
                    throw new IllegalArgumentException();
                case 9:
                    return m6.f(t2, j2) != null;
                case 10:
                    return !w2.c.equals(m6.f(t2, j2));
                case 11:
                    return m6.a(t2, j2) != 0;
                case 12:
                    return m6.a(t2, j2) != 0;
                case 13:
                    return m6.a(t2, j2) != 0;
                case 14:
                    return m6.b(t2, j2) != 0;
                case 15:
                    return m6.a(t2, j2) != 0;
                case 16:
                    return m6.b(t2, j2) != 0;
                case 17:
                    return m6.f(t2, j2) != null;
                default:
                    throw new IllegalArgumentException();
            }
        } else {
            int i4 = this.a[i2 + 2];
            return (m6.a(t2, (long) (i4 & 1048575)) & (1 << (i4 >>> 20))) != 0;
        }
    }

    public final int a(int i2, int i3) {
        int length = (this.a.length / 3) - 1;
        while (i3 <= length) {
            int i4 = (length + i3) >>> 1;
            int i5 = i4 * 3;
            int i6 = this.a[i5];
            if (i2 == i6) {
                return i5;
            }
            if (i2 < i6) {
                length = i4 - 1;
            } else {
                i3 = i4 + 1;
            }
        }
        return -1;
    }
}
