package j.c.a.a.g.a;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class j5 implements Callable<List<z8>> {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ s4 c;

    public j5(s4 s4Var, d9 d9Var) {
        this.c = s4Var;
        this.b = d9Var;
    }

    public final /* synthetic */ Object call() {
        this.c.a.o();
        return this.c.a.e().a(this.b.b);
    }
}
