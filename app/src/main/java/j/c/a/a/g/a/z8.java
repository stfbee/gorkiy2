package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class z8 {
    public final String a;
    public final String b;
    public final String c;
    public final long d;

    /* renamed from: e  reason: collision with root package name */
    public final Object f2143e;

    public z8(String str, String str2, String str3, long j2, Object obj) {
        ResourcesFlusher.b(str);
        ResourcesFlusher.b(str3);
        ResourcesFlusher.b(obj);
        this.a = str;
        this.b = str2;
        this.c = str3;
        this.d = j2;
        this.f2143e = obj;
    }
}
