package j.c.a.a.g.a;

import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import java.util.ArrayList;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class e4 {
    public long A;
    public long B;
    public String C;
    public boolean D;
    public long E;
    public long F;
    public final r4 a;
    public final String b;
    public String c;
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public String f1970e;

    /* renamed from: f  reason: collision with root package name */
    public String f1971f;
    public long g;
    public long h;

    /* renamed from: i  reason: collision with root package name */
    public long f1972i;

    /* renamed from: j  reason: collision with root package name */
    public String f1973j;

    /* renamed from: k  reason: collision with root package name */
    public long f1974k;

    /* renamed from: l  reason: collision with root package name */
    public String f1975l;

    /* renamed from: m  reason: collision with root package name */
    public long f1976m;

    /* renamed from: n  reason: collision with root package name */
    public long f1977n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f1978o;

    /* renamed from: p  reason: collision with root package name */
    public long f1979p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1980q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f1981r;

    /* renamed from: s  reason: collision with root package name */
    public String f1982s;

    /* renamed from: t  reason: collision with root package name */
    public Boolean f1983t;
    public long u;
    public List<String> v;
    public long w;
    public long x;
    public long y;
    public long z;

    public e4(r4 r4Var, String str) {
        ResourcesFlusher.b(r4Var);
        ResourcesFlusher.b(str);
        this.a = r4Var;
        this.b = str;
        r4Var.i().d();
    }

    public final void a(String str) {
        this.a.i().d();
        this.D |= !y8.d(this.c, str);
        this.c = str;
    }

    public final void b(String str) {
        this.a.i().d();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.D |= !y8.d(this.d, str);
        this.d = str;
    }

    public final void c(String str) {
        this.a.i().d();
        if (TextUtils.isEmpty(str)) {
            str = null;
        }
        this.D |= !y8.d(this.f1982s, str);
        this.f1982s = str;
    }

    public final void d(String str) {
        this.a.i().d();
        this.D |= !y8.d(this.f1970e, str);
        this.f1970e = str;
    }

    public final void e(String str) {
        this.a.i().d();
        this.D |= !y8.d(this.f1971f, str);
        this.f1971f = str;
    }

    public final void f(String str) {
        this.a.i().d();
        this.D |= !y8.d(this.f1973j, str);
        this.f1973j = str;
    }

    public final String g() {
        this.a.i().d();
        return this.b;
    }

    public final String h() {
        this.a.i().d();
        return this.c;
    }

    public final String i() {
        this.a.i().d();
        return this.d;
    }

    public final String j() {
        this.a.i().d();
        return this.f1982s;
    }

    public final String k() {
        this.a.i().d();
        return this.f1971f;
    }

    public final long l() {
        this.a.i().d();
        return this.h;
    }

    public final long m() {
        this.a.i().d();
        return this.f1972i;
    }

    public final String n() {
        this.a.i().d();
        return this.f1973j;
    }

    public final long o() {
        this.a.i().d();
        return this.f1974k;
    }

    public final String p() {
        this.a.i().d();
        return this.f1975l;
    }

    public final long q() {
        this.a.i().d();
        return this.f1976m;
    }

    public final long r() {
        this.a.i().d();
        return this.f1977n;
    }

    public final long s() {
        this.a.i().d();
        return this.u;
    }

    public final boolean t() {
        this.a.i().d();
        return this.f1978o;
    }

    public final long u() {
        this.a.i().d();
        return this.g;
    }

    public final long v() {
        this.a.i().d();
        return this.E;
    }

    public final long w() {
        this.a.i().d();
        return this.F;
    }

    public final void x() {
        this.a.i().d();
        long j2 = this.g + 1;
        if (j2 > 2147483647L) {
            this.a.a().f2047i.a("Bundle index overflow. appId", n3.a(this.b));
            j2 = 0;
        }
        this.D = true;
        this.g = j2;
    }

    public final void g(String str) {
        this.a.i().d();
        this.D |= !y8.d(this.f1975l, str);
        this.f1975l = str;
    }

    public final void h(long j2) {
        this.a.i().d();
        this.D |= this.E != j2;
        this.E = j2;
    }

    public final void i(long j2) {
        this.a.i().d();
        this.D |= this.F != j2;
        this.F = j2;
    }

    public final void j(long j2) {
        this.a.i().d();
        this.D |= this.f1979p != j2;
        this.f1979p = j2;
    }

    public final void a(long j2) {
        this.a.i().d();
        this.D |= this.h != j2;
        this.h = j2;
    }

    public final void d(long j2) {
        this.a.i().d();
        this.D |= this.f1976m != j2;
        this.f1976m = j2;
    }

    public final void e(long j2) {
        this.a.i().d();
        this.D |= this.f1977n != j2;
        this.f1977n = j2;
    }

    public final void f(long j2) {
        this.a.i().d();
        this.D |= this.u != j2;
        this.u = j2;
    }

    public final void b(long j2) {
        this.a.i().d();
        this.D |= this.f1972i != j2;
        this.f1972i = j2;
    }

    public final void c(long j2) {
        this.a.i().d();
        this.D |= this.f1974k != j2;
        this.f1974k = j2;
    }

    public final void g(long j2) {
        boolean z2 = true;
        ResourcesFlusher.a(j2 >= 0);
        this.a.i().d();
        boolean z3 = this.D;
        if (this.g == j2) {
            z2 = false;
        }
        this.D = z2 | z3;
        this.g = j2;
    }

    public final void h(String str) {
        this.a.i().d();
        this.D |= !y8.d(this.C, str);
        this.C = str;
    }

    public final void a(boolean z2) {
        this.a.i().d();
        this.D |= this.f1978o != z2;
        this.f1978o = z2;
    }

    public final boolean d() {
        this.a.i().d();
        return this.f1981r;
    }

    public final Boolean e() {
        this.a.i().d();
        return this.f1983t;
    }

    public final List<String> f() {
        this.a.i().d();
        return this.v;
    }

    public final long b() {
        this.a.i().d();
        return this.f1979p;
    }

    public final boolean c() {
        this.a.i().d();
        return this.f1980q;
    }

    public final String a() {
        this.a.i().d();
        String str = this.C;
        h((String) null);
        return str;
    }

    public final void b(boolean z2) {
        this.a.i().d();
        this.D = this.f1980q != z2;
        this.f1980q = z2;
    }

    public final void c(boolean z2) {
        this.a.i().d();
        this.D = this.f1981r != z2;
        this.f1981r = z2;
    }

    public final void a(List<String> list) {
        this.a.i().d();
        List<String> list2 = this.v;
        if (!((list2 == null && list == null) ? true : list2 == null ? false : list2.equals(list))) {
            this.D = true;
            this.v = list != null ? new ArrayList(list) : null;
        }
    }
}
