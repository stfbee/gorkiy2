package j.c.a.a.f.e;

import android.os.IBinder;
import android.os.IInterface;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class y2 extends a implements t3 {
    public static t3 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
        if (queryLocalInterface instanceof t3) {
            return (t3) queryLocalInterface;
        }
        return new s4(iBinder);
    }
}
