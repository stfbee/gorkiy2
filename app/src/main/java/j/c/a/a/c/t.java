package j.c.a.a.c;

import android.os.IBinder;
import android.os.IInterface;
import android.os.Parcel;
import android.os.RemoteException;
import android.util.Log;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.b0;
import j.c.a.a.c.l.c0;
import j.c.a.a.d.a;
import j.c.a.a.f.c.b;
import j.c.a.a.f.c.c;
import java.io.UnsupportedEncodingException;
import java.util.Arrays;

public abstract class t extends b implements b0 {
    public int a;

    public t(byte[] bArr) {
        super("com.google.android.gms.common.internal.ICertData");
        ResourcesFlusher.a(bArr.length == 25);
        this.a = Arrays.hashCode(bArr);
    }

    public static b0 a(IBinder iBinder) {
        if (iBinder == null) {
            return null;
        }
        IInterface queryLocalInterface = iBinder.queryLocalInterface("com.google.android.gms.common.internal.ICertData");
        if (queryLocalInterface instanceof b0) {
            return (b0) queryLocalInterface;
        }
        return new c0(iBinder);
    }

    public final a b() {
        return new j.c.a.a.d.b(g());
    }

    public final int c() {
        return this.a;
    }

    public boolean equals(Object obj) {
        a b;
        if (obj != null && (obj instanceof b0)) {
            try {
                b0 b0Var = (b0) obj;
                if (b0Var.c() == this.a && (b = b0Var.b()) != null) {
                    return Arrays.equals(g(), (byte[]) j.c.a.a.d.b.a(b));
                }
                return false;
            } catch (RemoteException e2) {
                Log.e("GoogleCertificates", "Failed to get Google certificates from remote", e2);
            }
        }
        return false;
    }

    public abstract byte[] g();

    public int hashCode() {
        return this.a;
    }

    public final boolean a(int i2, Parcel parcel, Parcel parcel2, int i3) {
        if (i2 == 1) {
            a b = b();
            parcel2.writeNoException();
            c.a(parcel2, b);
            return true;
        } else if (i2 != 2) {
            return false;
        } else {
            int c = c();
            parcel2.writeNoException();
            parcel2.writeInt(c);
            return true;
        }
    }

    public static byte[] a(String str) {
        try {
            return str.getBytes("ISO-8859-1");
        } catch (UnsupportedEncodingException e2) {
            throw new AssertionError(e2);
        }
    }
}
