package j.c.a.a.f.e;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class r4 extends p4 {
    public static final Class<?> c = Collections.unmodifiableList(Collections.emptyList()).getClass();

    public /* synthetic */ r4(o4 o4Var) {
        super(null);
    }

    public final void a(Object obj, long j2) {
        Object obj2;
        List list = (List) m6.f(obj, j2);
        if (list instanceof m4) {
            obj2 = ((m4) list).j();
        } else if (!c.isAssignableFrom(list.getClass())) {
            if (!(list instanceof p5) || !(list instanceof e4)) {
                obj2 = Collections.unmodifiableList(list);
            } else {
                e4 e4Var = (e4) list;
                if (e4Var.a()) {
                    e4Var.o();
                    return;
                }
                return;
            }
        } else {
            return;
        }
        m6.a(obj, j2, obj2);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public final <E> void a(Object obj, Object obj2, long j2) {
        ArrayList arrayList;
        List list = (List) m6.f(obj2, j2);
        int size = list.size();
        List list2 = (List) m6.f(obj, j2);
        if (list2.isEmpty()) {
            if (list2 instanceof m4) {
                list2 = new n4(size);
            } else if (!(list2 instanceof p5) || !(list2 instanceof e4)) {
                list2 = new ArrayList(size);
            } else {
                list2 = ((e4) list2).a(size);
            }
            m6.a(obj, j2, list2);
        } else {
            if (c.isAssignableFrom(list2.getClass())) {
                ArrayList arrayList2 = new ArrayList(list2.size() + size);
                arrayList2.addAll(list2);
                m6.a(obj, j2, arrayList2);
                arrayList = arrayList2;
            } else if (list2 instanceof k6) {
                n4 n4Var = new n4(list2.size() + size);
                n4Var.addAll(n4Var.size(), (k6) list2);
                m6.a(obj, j2, n4Var);
                arrayList = n4Var;
            } else if ((list2 instanceof p5) && (list2 instanceof e4)) {
                e4 e4Var = (e4) list2;
                if (!e4Var.a()) {
                    list2 = e4Var.a(list2.size() + size);
                    m6.a(obj, j2, list2);
                }
            }
            list2 = arrayList;
        }
        int size2 = list2.size();
        int size3 = list.size();
        if (size2 > 0 && size3 > 0) {
            list2.addAll(list);
        }
        if (size2 > 0) {
            list = list2;
        }
        m6.a(obj, j2, list);
    }
}
