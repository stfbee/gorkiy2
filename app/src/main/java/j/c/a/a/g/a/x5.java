package j.c.a.a.g.a;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.text.TextUtils;
import com.crashlytics.android.core.CrashlyticsController;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.c.k.e.c;
import j.c.a.a.c.n.b;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.CopyOnWriteArraySet;
import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class x5 extends e5 {
    public p6 c;
    public s5 d;

    /* renamed from: e  reason: collision with root package name */
    public final Set<v5> f2125e = new CopyOnWriteArraySet();

    /* renamed from: f  reason: collision with root package name */
    public boolean f2126f;
    public final AtomicReference<String> g = new AtomicReference<>();
    public boolean h = true;

    public x5(r4 r4Var) {
        super(r4Var);
    }

    public final void A() {
        d();
        b();
        w();
        if (this.a.e()) {
            r4 r4Var = this.a;
            i9 i9Var = r4Var.g;
            g3 t2 = r4Var.t();
            super.w();
            if (i9Var.d(t2.c, k.C0)) {
                i9 i9Var2 = this.a.g;
                h9 h9Var = i9Var2.a.f2086f;
                Boolean b = i9Var2.b("google_analytics_deferred_deep_link_enabled");
                if (b != null && b.booleanValue()) {
                    a().f2051m.a("Deferred Deep Link feature enabled.");
                    l4 i2 = i();
                    w5 w5Var = new w5(this);
                    i2.o();
                    ResourcesFlusher.b(w5Var);
                    i2.a((p4<?>) new p4(i2, w5Var, "Task exception on worker thread"));
                }
            }
            z6 r2 = r();
            r2.d();
            super.w();
            d9 a = r2.a(true);
            boolean a2 = r2.a.g.a(k.D0);
            if (a2) {
                r2.t().a(3, new byte[0]);
            }
            r2.a(new i7(r2, a, a2));
            this.h = false;
            w3 l2 = l();
            l2.d();
            String string = l2.v().getString("previous_os_version", null);
            l2.e().o();
            String str = Build.VERSION.RELEASE;
            if (!TextUtils.isEmpty(str) && !str.equals(string)) {
                SharedPreferences.Editor edit = l2.v().edit();
                edit.putString("previous_os_version", str);
                edit.apply();
            }
            if (!TextUtils.isEmpty(string)) {
                e().o();
                if (!string.equals(Build.VERSION.RELEASE)) {
                    Bundle bundle = new Bundle();
                    bundle.putString("_po", string);
                    a("auto", "_ou", bundle);
                }
            }
        }
    }

    public final String B() {
        String str = this.a.b;
        if (str != null) {
            return str;
        }
        try {
            return c.a();
        } catch (IllegalStateException e2) {
            this.a.a().f2046f.a("getGoogleAppId failed with exception", e2);
            return null;
        }
    }

    public final void C() {
        i9 i9Var = this.a.g;
        g3 q2 = q();
        super.w();
        if (i9Var.d(q2.c, k.j0)) {
            d();
            String a = l().f2123s.a();
            if (a != null) {
                if (!"unset".equals(a)) {
                    Long valueOf = Long.valueOf("true".equals(a) ? 1 : 0);
                    if (((b) this.a.f2092n) != null) {
                        a("app", "_npa", valueOf, System.currentTimeMillis());
                    } else {
                        throw null;
                    }
                } else if (((b) this.a.f2092n) != null) {
                    a("app", "_npa", (Object) null, System.currentTimeMillis());
                } else {
                    throw null;
                }
            }
        }
        if (!this.a.c() || !this.h) {
            a().f2051m.a("Updating Scion state (FE)");
            z6 r2 = r();
            r2.d();
            super.w();
            r2.a(new m7(r2, r2.a(true)));
            return;
        }
        a().f2051m.a("Recording app launch after enabling measurement for the first time (FE)");
        A();
    }

    public final void a(String str, String str2, Bundle bundle) {
        if (((b) this.a.f2092n) != null) {
            a(str, str2, bundle, true, true, System.currentTimeMillis());
            return;
        }
        throw null;
    }

    public final void b(String str, String str2, Bundle bundle) {
        b();
        d();
        if (((b) this.a.f2092n) != null) {
            a(str, str2, System.currentTimeMillis(), bundle);
            return;
        }
        throw null;
    }

    public final boolean y() {
        return false;
    }

    public final void z() {
        if (this.a.a.getApplicationContext() instanceof Application) {
            ((Application) this.a.a.getApplicationContext()).unregisterActivityLifecycleCallbacks(this.c);
        }
    }

    public final void a(String str, String str2, Object obj, boolean z) {
        if (((b) this.a.f2092n) != null) {
            a(str, str2, obj, z, System.currentTimeMillis());
            return;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.String, java.lang.Class, java.lang.Object):T
     arg types: [android.os.Bundle, java.lang.String, java.lang.Class<java.lang.Long>, long]
     candidates:
      j.c.a.a.c.n.c.a(float, float, float, float):float
      j.c.a.a.c.n.c.a(int, byte[], int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.e4<?>, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.b.w.CircularRevealWidget, float, float, float):android.animation.Animator
      j.c.a.a.c.n.c.a(byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(java.lang.StringBuilder, int, java.lang.String, java.lang.Object):void
      j.c.a.a.c.n.c.a(java.nio.ByteBuffer, java.nio.ByteBuffer, java.nio.ByteBuffer, int):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.String, java.lang.Class, java.lang.Object):T */
    public final void b(Bundle bundle, long j2) {
        Class<Long> cls = Long.class;
        Class<String> cls2 = String.class;
        ResourcesFlusher.b(bundle);
        j.c.a.a.c.n.c.a(bundle, "app_id", cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "origin", cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, DefaultAppMeasurementEventListenerRegistrar.NAME, cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "value", Object.class, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "trigger_event_name", cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "trigger_timeout", (Class) cls, (Object) 0L);
        j.c.a.a.c.n.c.a(bundle, "timed_out_event_name", cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "timed_out_event_params", Bundle.class, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "triggered_event_name", cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "triggered_event_params", Bundle.class, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "time_to_live", (Class) cls, (Object) 0L);
        j.c.a.a.c.n.c.a(bundle, "expired_event_name", cls2, (Object) null);
        j.c.a.a.c.n.c.a(bundle, "expired_event_params", Bundle.class, (Object) null);
        ResourcesFlusher.b(bundle.getString(DefaultAppMeasurementEventListenerRegistrar.NAME));
        ResourcesFlusher.b(bundle.getString("origin"));
        ResourcesFlusher.b(bundle.get("value"));
        bundle.putLong("creation_timestamp", j2);
        String string = bundle.getString(DefaultAppMeasurementEventListenerRegistrar.NAME);
        Object obj = bundle.get("value");
        if (k().b(string) != 0) {
            a().f2046f.a("Invalid conditional user property name", g().c(string));
        } else if (k().b(string, obj) != 0) {
            a().f2046f.a("Invalid conditional user property value", g().c(string), obj);
        } else {
            Object c2 = k().c(string, obj);
            if (c2 == null) {
                a().f2046f.a("Unable to normalize conditional user property value", g().c(string), obj);
                return;
            }
            j.c.a.a.c.n.c.a(bundle, c2);
            long j3 = bundle.getLong("trigger_timeout");
            if (TextUtils.isEmpty(bundle.getString("trigger_event_name")) || (j3 <= 15552000000L && j3 >= 1)) {
                long j4 = bundle.getLong("time_to_live");
                if (j4 > 15552000000L || j4 < 1) {
                    a().f2046f.a("Invalid conditional user property time to live", g().c(string), Long.valueOf(j4));
                    return;
                }
                l4 i2 = i();
                d6 d6Var = new d6(this, bundle);
                i2.o();
                ResourcesFlusher.b(d6Var);
                i2.a((p4<?>) new p4(i2, d6Var, "Task exception on worker thread"));
                return;
            }
            a().f2046f.a("Invalid conditional user property timeout", g().c(string), Long.valueOf(j3));
        }
    }

    public final void a(String str, String str2, String str3, Bundle bundle) {
        if (((b) this.a.f2092n) != null) {
            long currentTimeMillis = System.currentTimeMillis();
            ResourcesFlusher.b(str2);
            Bundle bundle2 = new Bundle();
            if (str != null) {
                bundle2.putString("app_id", str);
            }
            bundle2.putString(DefaultAppMeasurementEventListenerRegistrar.NAME, str2);
            bundle2.putLong("creation_timestamp", currentTimeMillis);
            if (str3 != null) {
                bundle2.putString("expired_event_name", str3);
                bundle2.putBundle("expired_event_params", bundle);
            }
            l4 i2 = i();
            g6 g6Var = new g6(this, bundle2);
            i2.o();
            ResourcesFlusher.b(g6Var);
            i2.a((p4<?>) new p4(i2, g6Var, "Task exception on worker thread"));
            return;
        }
        throw null;
    }

    public final void a(boolean z) {
        w();
        b();
        l4 i2 = i();
        o6 o6Var = new o6(this, z);
        i2.o();
        ResourcesFlusher.b(o6Var);
        i2.a((p4<?>) new p4(i2, o6Var, "Task exception on worker thread"));
    }

    public final void a(String str, String str2, long j2, Bundle bundle) {
        b();
        d();
        a(str, str2, j2, bundle, true, this.d == null || y8.f(str2), false, null);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.f8.a(long, boolean):void
     arg types: [long, int]
     candidates:
      j.c.a.a.g.a.f8.a(boolean, boolean):boolean
      j.c.a.a.g.a.f8.a(long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
     arg types: [j.c.a.a.g.a.w6, android.os.Bundle, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.f8.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      j.c.a.a.g.a.f8.a(long, boolean):void
      j.c.a.a.g.a.f8.a(boolean, boolean):boolean */
    public final void a(String str, String str2, long j2, Bundle bundle, boolean z, boolean z2, boolean z3, String str3) {
        String str4;
        String str5;
        String str6;
        String str7;
        boolean z4;
        Bundle[] bundleArr;
        String str8;
        w6 w6Var;
        int i2;
        ArrayList arrayList;
        long j3;
        Bundle bundle2;
        Class<?> cls;
        List<String> list;
        String str9 = str;
        String str10 = str2;
        long j4 = j2;
        Bundle bundle3 = bundle;
        String str11 = str3;
        ResourcesFlusher.b(str);
        if (!this.a.g.d(str11, k.o0)) {
            ResourcesFlusher.b(str2);
        }
        ResourcesFlusher.b(bundle);
        d();
        w();
        if (!this.a.c()) {
            a().f2051m.a("Event not sent since app measurement is disabled");
            return;
        }
        i9 i9Var = this.a.g;
        g3 q2 = q();
        super.w();
        if (!i9Var.d(q2.c, k.v0) || (list = q().f1994j) == null || list.contains(str10)) {
            int i3 = 0;
            if (!this.f2126f) {
                this.f2126f = true;
                try {
                    if (!this.a.f2085e) {
                        cls = Class.forName("com.google.android.gms.tagmanager.TagManagerService", true, this.a.a.getClassLoader());
                    } else {
                        cls = Class.forName("com.google.android.gms.tagmanager.TagManagerService");
                    }
                    try {
                        cls.getDeclaredMethod("initialize", Context.class).invoke(null, this.a.a);
                    } catch (Exception e2) {
                        a().f2047i.a("Failed to invoke Tag Manager's initialize() method", e2);
                    }
                } catch (ClassNotFoundException unused) {
                    a().f2050l.a("Tag Manager is not found and thus will not be used");
                }
            }
            i9 i9Var2 = this.a.g;
            g3 q3 = q();
            super.w();
            if (i9Var2.d(q3.c, k.E0) && "_cmp".equals(str10) && bundle3.containsKey("gclid")) {
                String string = bundle3.getString("gclid");
                if (((b) this.a.f2092n) != null) {
                    a("auto", "_lgclid", string, System.currentTimeMillis());
                } else {
                    throw null;
                }
            }
            if (z3) {
                h9 h9Var = this.a.f2086f;
                if (!"_iap".equals(str10)) {
                    y8 n2 = this.a.n();
                    int i4 = 2;
                    if (n2.a("event", str10)) {
                        if (!n2.a("event", r5.a, str10)) {
                            i4 = 13;
                        } else if (n2.a("event", 40, str10)) {
                            i4 = 0;
                        }
                    }
                    if (i4 != 0) {
                        a().h.a("Invalid public event name. Event will not be logged (FE)", g().a(str10));
                        this.a.n();
                        String a = y8.a(str10, 40, true);
                        if (str10 != null) {
                            i3 = str2.length();
                        }
                        this.a.n().a(i4, "_ev", a, i3);
                        return;
                    }
                }
            }
            h9 h9Var2 = this.a.f2086f;
            w6 z5 = s().z();
            if (z5 != null && !bundle3.containsKey("_sc")) {
                z5.d = true;
            }
            y6.a(z5, bundle3, z && z3);
            boolean equals = "am".equals(str9);
            boolean f2 = y8.f(str2);
            if (z && this.d != null && !f2 && !equals) {
                a().f2051m.a("Passing event to registered event handler (FE)", g().a(str10), g().a(bundle3));
                AppMeasurementDynamiteService.b bVar = (AppMeasurementDynamiteService.b) this.d;
                if (bVar != null) {
                    try {
                        bVar.a.a(str, str2, bundle, j2);
                    } catch (RemoteException e3) {
                        AppMeasurementDynamiteService.this.a.a().f2047i.a("Event interceptor threw exception", e3);
                    }
                } else {
                    throw null;
                }
            } else if (this.a.e()) {
                int a2 = k().a(str10);
                if (a2 != 0) {
                    a().h.a("Invalid event name. Event will not be logged (FE)", g().a(str10));
                    k();
                    String a3 = y8.a(str10, 40, true);
                    if (str10 != null) {
                        i3 = str2.length();
                    }
                    this.a.n().a(str3, a2, "_ev", a3, i3);
                    return;
                }
                List unmodifiableList = Collections.unmodifiableList(Arrays.asList("_o", "_sn", "_sc", "_si"));
                String str12 = str11;
                String str13 = "_o";
                long j5 = j4;
                String str14 = str10;
                Bundle a4 = k().a(str3, str2, bundle, unmodifiableList, z3, true);
                w6 w6Var2 = (a4 == null || !a4.containsKey("_sc") || !a4.containsKey("_si")) ? null : new w6(a4.getString("_sn"), a4.getString("_sc"), Long.valueOf(a4.getLong("_si")).longValue());
                w6 w6Var3 = w6Var2 == null ? z5 : w6Var2;
                boolean h2 = this.a.g.h(str12);
                String str15 = CrashlyticsController.FIREBASE_APPLICATION_EXCEPTION;
                if (h2) {
                    h9 h9Var3 = this.a.f2086f;
                    if (s().z() != null && str15.equals(str14)) {
                        long z6 = u().z();
                        if (z6 > 0) {
                            k().a(a4, z6);
                        }
                    }
                }
                ArrayList arrayList2 = new ArrayList();
                arrayList2.add(a4);
                long nextLong = k().u().nextLong();
                i9 i9Var3 = this.a.g;
                g3 q4 = q();
                super.w();
                if (!i9Var3.d(q4.c, k.c0) || l().v.a() <= 0 || !l().a(j5) || !l().y.a()) {
                    str4 = str13;
                } else {
                    a().f2052n.a("Current session is expired, remove the session number and Id");
                    i9 i9Var4 = this.a.g;
                    g3 q5 = q();
                    super.w();
                    if (!i9Var4.d(q5.c, k.Y)) {
                        str4 = str13;
                    } else if (((b) this.a.f2092n) != null) {
                        str4 = str13;
                        a("auto", "_sid", (Object) null, System.currentTimeMillis());
                    } else {
                        throw null;
                    }
                    i9 i9Var5 = this.a.g;
                    g3 q6 = q();
                    super.w();
                    if (i9Var5.d(q6.c, k.Z)) {
                        if (((b) this.a.f2092n) != null) {
                            a("auto", "_sno", (Object) null, System.currentTimeMillis());
                        } else {
                            throw null;
                        }
                    }
                }
                i9 i9Var6 = this.a.g;
                g3 q7 = q();
                super.w();
                String str16 = q7.c;
                if (i9Var6 != null) {
                    if (i9Var6.d(str16, k.b0) && a4.getLong("extend_session", 0) == 1) {
                        a().f2052n.a("EXTEND_SESSION param attached: initiate a new session or extend the current active session");
                        r4 r4Var = this.a;
                        r4.a((e5) r4Var.f2089k);
                        r4Var.f2089k.a(j2, true);
                    }
                    String[] strArr = (String[]) a4.keySet().toArray(new String[bundle.size()]);
                    Arrays.sort(strArr);
                    int length = strArr.length;
                    int i5 = 0;
                    int i6 = 0;
                    while (true) {
                        str5 = "_eid";
                        if (i5 >= length) {
                            break;
                        }
                        String str17 = strArr[i5];
                        String[] strArr2 = strArr;
                        Object obj = a4.get(str17);
                        k();
                        int i7 = length;
                        if (obj instanceof Bundle) {
                            bundleArr = new Bundle[]{(Bundle) obj};
                        } else if (obj instanceof Parcelable[]) {
                            Parcelable[] parcelableArr = (Parcelable[]) obj;
                            bundleArr = (Bundle[]) Arrays.copyOf(parcelableArr, parcelableArr.length, Bundle[].class);
                        } else if (obj instanceof ArrayList) {
                            ArrayList arrayList3 = (ArrayList) obj;
                            bundleArr = (Bundle[]) arrayList3.toArray(new Bundle[arrayList3.size()]);
                        } else {
                            bundleArr = null;
                        }
                        if (bundleArr != null) {
                            a4.putInt(str17, bundleArr.length);
                            int i8 = 0;
                            while (i8 < bundleArr.length) {
                                Bundle bundle4 = bundleArr[i8];
                                y6.a(w6Var3, bundle4, true);
                                String str18 = str5;
                                int i9 = i8;
                                long j6 = nextLong;
                                Bundle bundle5 = bundle4;
                                ArrayList arrayList4 = arrayList2;
                                Bundle a5 = k().a(str3, "_ep", bundle5, unmodifiableList, z3, false);
                                a5.putString("_en", str14);
                                a5.putLong(str18, j6);
                                a5.putString("_gn", str17);
                                a5.putInt("_ll", bundleArr.length);
                                int i10 = i9;
                                a5.putInt("_i", i10);
                                arrayList4.add(a5);
                                int i11 = i10 + 1;
                                a4 = a4;
                                nextLong = j6;
                                arrayList2 = arrayList4;
                                i8 = i11;
                                i5 = i5;
                                w6Var3 = w6Var3;
                                i6 = i6;
                                str15 = str15;
                                str5 = str18;
                            }
                            w6Var = w6Var3;
                            i2 = i5;
                            j3 = nextLong;
                            arrayList = arrayList2;
                            str8 = str15;
                            bundle2 = a4;
                            i6 += bundleArr.length;
                        } else {
                            w6Var = w6Var3;
                            i2 = i5;
                            j3 = nextLong;
                            arrayList = arrayList2;
                            str8 = str15;
                            bundle2 = a4;
                        }
                        a4 = bundle2;
                        nextLong = j3;
                        arrayList2 = arrayList;
                        length = i7;
                        w6Var3 = w6Var;
                        str15 = str8;
                        i5 = i2 + 1;
                        strArr = strArr2;
                    }
                    String str19 = str5;
                    int i12 = i6;
                    long j7 = nextLong;
                    ArrayList arrayList5 = arrayList2;
                    String str20 = str15;
                    Bundle bundle6 = a4;
                    if (i12 != 0) {
                        bundle6.putLong(str19, j7);
                        bundle6.putInt("_epc", i12);
                    }
                    int i13 = 0;
                    while (i13 < arrayList5.size()) {
                        Bundle bundle7 = (Bundle) arrayList5.get(i13);
                        if (i13 != 0) {
                            str7 = "_ep";
                            str6 = str14;
                        } else {
                            str7 = str14;
                            str6 = str7;
                        }
                        String str21 = str4;
                        bundle7.putString(str21, str);
                        if (z2) {
                            bundle7 = k().a(bundle7);
                        }
                        Bundle bundle8 = bundle7;
                        a().f2051m.a("Logging event (FE)", g().a(str6), g().a(bundle8));
                        ArrayList arrayList6 = arrayList5;
                        i iVar = new i(str7, new h(bundle8), str, j2);
                        z6 r2 = r();
                        if (r2 != null) {
                            ResourcesFlusher.b(iVar);
                            r2.d();
                            super.w();
                            r2.C();
                            j3 t2 = r2.t();
                            if (t2 != null) {
                                Parcel obtain = Parcel.obtain();
                                iVar.writeToParcel(obtain, 0);
                                byte[] marshall = obtain.marshall();
                                obtain.recycle();
                                if (marshall.length > 131072) {
                                    t2.a().f2047i.a("Event is too long for local database. Sending event directly to service");
                                    z4 = false;
                                } else {
                                    z4 = t2.a(0, marshall);
                                }
                                boolean z7 = z4;
                                Bundle bundle9 = bundle8;
                                String str22 = str21;
                                String str23 = str6;
                                r2.a(new l7(r2, true, z7, iVar, r2.a(true), str3));
                                if (!equals) {
                                    for (v5 onEvent : this.f2125e) {
                                        onEvent.onEvent(str, str2, new Bundle(bundle9), j2);
                                        bundle9 = bundle9;
                                        str23 = str23;
                                    }
                                }
                                i13++;
                                str14 = str23;
                                arrayList5 = arrayList6;
                                str4 = str22;
                            } else {
                                throw null;
                            }
                        } else {
                            throw null;
                        }
                    }
                    String str24 = str14;
                    h9 h9Var4 = this.a.f2086f;
                    if (s().z() != null && str20.equals(str24)) {
                        u().a(true, true);
                        return;
                    }
                    return;
                }
                throw null;
            }
        } else {
            a().f2051m.a("Dropping non-safelisted event. event name, origin", str10, str9);
        }
    }

    public final void a(String str, String str2, Bundle bundle, boolean z, boolean z2, long j2) {
        b();
        String str3 = str == null ? "app" : str;
        Bundle bundle2 = bundle == null ? new Bundle() : bundle;
        boolean z3 = !z2 || this.d == null || y8.f(str2);
        boolean z4 = !z;
        Bundle bundle3 = new Bundle(bundle2);
        for (String next : bundle3.keySet()) {
            Object obj = bundle3.get(next);
            if (obj instanceof Bundle) {
                bundle3.putBundle(next, new Bundle((Bundle) obj));
            } else if (obj instanceof Parcelable[]) {
                Parcelable[] parcelableArr = (Parcelable[]) obj;
                for (int i2 = 0; i2 < parcelableArr.length; i2++) {
                    if (parcelableArr[i2] instanceof Bundle) {
                        parcelableArr[i2] = new Bundle((Bundle) parcelableArr[i2]);
                    }
                }
            } else if (obj instanceof List) {
                List list = (List) obj;
                for (int i3 = 0; i3 < list.size(); i3++) {
                    Object obj2 = list.get(i3);
                    if (obj2 instanceof Bundle) {
                        list.set(i3, new Bundle((Bundle) obj2));
                    }
                }
            }
        }
        l4 i4 = i();
        y5 y5Var = new y5(this, str3, str2, j2, bundle3, z2, z3, z4, null);
        i4.o();
        ResourcesFlusher.b(y5Var);
        i4.a((p4<?>) new p4(i4, y5Var, "Task exception on worker thread"));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String
     arg types: [java.lang.String, int, int]
     candidates:
      j.c.a.a.g.a.y8.a(int, java.lang.Object, boolean):java.lang.Object
      j.c.a.a.g.a.y8.a(android.os.Bundle, java.lang.String, java.lang.Object):void
      j.c.a.a.g.a.y8.a(java.lang.String, int, java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, java.lang.String[], java.lang.String):boolean
      j.c.a.a.g.a.y8.a(java.lang.String, int, boolean):java.lang.String */
    public final void a(String str, String str2, Object obj, boolean z, long j2) {
        if (str == null) {
            str = "app";
        }
        String str3 = str;
        int i2 = 6;
        int i3 = 0;
        if (z) {
            i2 = k().b(str2);
        } else {
            y8 k2 = k();
            if (k2.a("user property", str2)) {
                if (!k2.a("user property", t5.a, str2)) {
                    i2 = 15;
                } else if (k2.a("user property", 24, str2)) {
                    i2 = 0;
                }
            }
        }
        if (i2 != 0) {
            k();
            String a = y8.a(str2, 24, true);
            if (str2 != null) {
                i3 = str2.length();
            }
            this.a.n().a(i2, "_ev", a, i3);
        } else if (obj != null) {
            int b = k().b(str2, obj);
            if (b != 0) {
                k();
                String a2 = y8.a(str2, 24, true);
                if ((obj instanceof String) || (obj instanceof CharSequence)) {
                    i3 = String.valueOf(obj).length();
                }
                this.a.n().a(b, "_ev", a2, i3);
                return;
            }
            Object c2 = k().c(str2, obj);
            if (c2 != null) {
                a(str3, str2, j2, c2);
            }
        } else {
            a(str3, str2, j2, (Object) null);
        }
    }

    public final void a(String str, String str2, long j2, Object obj) {
        l4 i2 = i();
        a6 a6Var = new a6(this, str, str2, obj, j2);
        i2.o();
        ResourcesFlusher.b(a6Var);
        i2.a((p4<?>) new p4(i2, a6Var, "Task exception on worker thread"));
    }

    /* JADX WARNING: Removed duplicated region for block: B:22:0x0082  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x008e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(java.lang.String r9, java.lang.String r10, java.lang.Object r11, long r12) {
        /*
            r8 = this;
            i.b.k.ResourcesFlusher.b(r9)
            i.b.k.ResourcesFlusher.b(r10)
            r8.d()
            r8.b()
            r8.w()
            j.c.a.a.g.a.r4 r0 = r8.a
            j.c.a.a.g.a.i9 r0 = r0.g
            j.c.a.a.g.a.g3 r1 = r8.q()
            r1.w()
            java.lang.String r1 = r1.c
            j.c.a.a.g.a.b3<java.lang.Boolean> r2 = j.c.a.a.g.a.k.j0
            boolean r0 = r0.d(r1, r2)
            java.lang.String r1 = "_npa"
            if (r0 == 0) goto L_0x0078
            java.lang.String r0 = "allow_personalized_ads"
            boolean r0 = r0.equals(r10)
            if (r0 == 0) goto L_0x0078
            boolean r0 = r11 instanceof java.lang.String
            if (r0 == 0) goto L_0x0068
            r0 = r11
            java.lang.String r0 = (java.lang.String) r0
            boolean r2 = android.text.TextUtils.isEmpty(r0)
            if (r2 != 0) goto L_0x0068
            java.util.Locale r10 = java.util.Locale.ENGLISH
            java.lang.String r10 = r0.toLowerCase(r10)
            java.lang.String r11 = "false"
            boolean r10 = r11.equals(r10)
            r2 = 1
            if (r10 == 0) goto L_0x004d
            r4 = r2
            goto L_0x004f
        L_0x004d:
            r4 = 0
        L_0x004f:
            java.lang.Long r10 = java.lang.Long.valueOf(r4)
            j.c.a.a.g.a.w3 r0 = r8.l()
            j.c.a.a.g.a.d4 r0 = r0.f2123s
            long r4 = r10.longValue()
            int r6 = (r4 > r2 ? 1 : (r4 == r2 ? 0 : -1))
            if (r6 != 0) goto L_0x0063
            java.lang.String r11 = "true"
        L_0x0063:
            r0.a(r11)
            r6 = r10
            goto L_0x0076
        L_0x0068:
            if (r11 != 0) goto L_0x0078
            j.c.a.a.g.a.w3 r10 = r8.l()
            j.c.a.a.g.a.d4 r10 = r10.f2123s
            java.lang.String r0 = "unset"
            r10.a(r0)
            r6 = r11
        L_0x0076:
            r3 = r1
            goto L_0x007a
        L_0x0078:
            r3 = r10
            r6 = r11
        L_0x007a:
            j.c.a.a.g.a.r4 r10 = r8.a
            boolean r10 = r10.c()
            if (r10 != 0) goto L_0x008e
            j.c.a.a.g.a.n3 r9 = r8.a()
            j.c.a.a.g.a.p3 r9 = r9.f2051m
            java.lang.String r10 = "User property not set since app measurement is disabled"
            r9.a(r10)
            return
        L_0x008e:
            j.c.a.a.g.a.r4 r10 = r8.a
            boolean r10 = r10.e()
            if (r10 != 0) goto L_0x0097
            return
        L_0x0097:
            j.c.a.a.g.a.n3 r10 = r8.a()
            j.c.a.a.g.a.p3 r10 = r10.f2051m
            j.c.a.a.g.a.l3 r11 = r8.g()
            java.lang.String r11 = r11.a(r3)
            java.lang.String r0 = "Setting user property (FE)"
            r10.a(r0, r11, r6)
            j.c.a.a.g.a.x8 r10 = new j.c.a.a.g.a.x8
            r2 = r10
            r4 = r12
            r7 = r9
            r2.<init>(r3, r4, r6, r7)
            j.c.a.a.g.a.z6 r9 = r8.r()
            r9.d()
            r9.w()
            r9.C()
            j.c.a.a.g.a.j3 r11 = r9.t()
            if (r11 == 0) goto L_0x00f7
            android.os.Parcel r12 = android.os.Parcel.obtain()
            r13 = 0
            r10.writeToParcel(r12, r13)
            byte[] r0 = r12.marshall()
            r12.recycle()
            int r12 = r0.length
            r1 = 131072(0x20000, float:1.83671E-40)
            r2 = 1
            if (r12 <= r1) goto L_0x00e6
            j.c.a.a.g.a.n3 r11 = r11.a()
            j.c.a.a.g.a.p3 r11 = r11.f2047i
            java.lang.String r12 = "User property too long for local database. Sending directly to service"
            r11.a(r12)
            goto L_0x00ea
        L_0x00e6:
            boolean r13 = r11.a(r2, r0)
        L_0x00ea:
            j.c.a.a.g.a.d9 r11 = r9.a(r2)
            j.c.a.a.g.a.c7 r12 = new j.c.a.a.g.a.c7
            r12.<init>(r9, r13, r10, r11)
            r9.a(r12)
            return
        L_0x00f7:
            r9 = 0
            throw r9
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.x5.a(java.lang.String, java.lang.String, java.lang.Object, long):void");
    }

    public final void a(v5 v5Var) {
        b();
        w();
        ResourcesFlusher.b(v5Var);
        if (!this.f2125e.add(v5Var)) {
            a().f2047i.a("OnEventListener already registered");
        }
    }

    public final void a(Bundle bundle, long j2) {
        ResourcesFlusher.b(bundle);
        b();
        Bundle bundle2 = new Bundle(bundle);
        if (!TextUtils.isEmpty(bundle2.getString("app_id"))) {
            a().f2047i.a("Package name should be null when calling setConditionalUserProperty");
        }
        bundle2.remove("app_id");
        b(bundle2, j2);
    }

    public final ArrayList<Bundle> a(String str, String str2, String str3) {
        if (i().t()) {
            a().f2046f.a("Cannot get conditional user properties from analytics worker thread");
            return new ArrayList<>(0);
        } else if (h9.a()) {
            a().f2046f.a("Cannot get conditional user properties from main thread");
            return new ArrayList<>(0);
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                l4 i2 = this.a.i();
                f6 f6Var = new f6(this, atomicReference, str, str2, str3);
                i2.o();
                ResourcesFlusher.b(f6Var);
                i2.a((p4<?>) new p4(i2, f6Var, "Task exception on worker thread"));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e2) {
                    a().f2047i.a("Interrupted waiting for get conditional user properties", str, e2);
                }
            }
            List list = (List) atomicReference.get();
            if (list != null) {
                return y8.b(list);
            }
            a().f2047i.a("Timed out waiting for get conditional user properties", str);
            return new ArrayList<>();
        }
    }

    public final Map<String, Object> a(String str, String str2, String str3, boolean z) {
        if (i().t()) {
            a().f2046f.a("Cannot get user properties from analytics worker thread");
            return Collections.emptyMap();
        } else if (h9.a()) {
            a().f2046f.a("Cannot get user properties from main thread");
            return Collections.emptyMap();
        } else {
            AtomicReference atomicReference = new AtomicReference();
            synchronized (atomicReference) {
                l4 i2 = this.a.i();
                h6 h6Var = new h6(this, atomicReference, str, str2, str3, z);
                i2.o();
                ResourcesFlusher.b(h6Var);
                i2.a((p4<?>) new p4(i2, h6Var, "Task exception on worker thread"));
                try {
                    atomicReference.wait(5000);
                } catch (InterruptedException e2) {
                    a().f2047i.a("Interrupted waiting for get user properties", e2);
                }
            }
            List<x8> list = (List) atomicReference.get();
            if (list == null) {
                a().f2047i.a("Timed out waiting for handle get user properties");
                return Collections.emptyMap();
            }
            ArrayMap arrayMap = new ArrayMap(list.size());
            for (x8 x8Var : list) {
                arrayMap.put(x8Var.c, x8Var.a());
            }
            return arrayMap;
        }
    }
}
