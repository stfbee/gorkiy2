package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class o1 {
    public static volatile a2<Boolean> a = z1.b;
    public static final Object b = new Object();

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0064  */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x0079  */
    /* JADX WARNING: Removed duplicated region for block: B:39:0x0083  */
    /* JADX WARNING: Removed duplicated region for block: B:43:0x0096  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static boolean a(android.content.Context r4, android.net.Uri r5) {
        /*
            java.lang.String r5 = r5.getAuthority()
            java.lang.String r0 = "com.google.android.gms.phenotype"
            boolean r0 = r0.equals(r5)
            if (r0 == 0) goto L_0x009b
            j.c.a.a.f.e.a2<java.lang.Boolean> r5 = j.c.a.a.f.e.o1.a
            boolean r5 = r5.a()
            if (r5 == 0) goto L_0x0021
            j.c.a.a.f.e.a2<java.lang.Boolean> r4 = j.c.a.a.f.e.o1.a
            java.lang.Object r4 = r4.b()
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            boolean r4 = r4.booleanValue()
            return r4
        L_0x0021:
            java.lang.Object r0 = j.c.a.a.f.e.o1.b
            monitor-enter(r0)
            j.c.a.a.f.e.a2<java.lang.Boolean> r5 = j.c.a.a.f.e.o1.a     // Catch:{ all -> 0x0098 }
            boolean r5 = r5.a()     // Catch:{ all -> 0x0098 }
            if (r5 == 0) goto L_0x003a
            j.c.a.a.f.e.a2<java.lang.Boolean> r4 = j.c.a.a.f.e.o1.a     // Catch:{ all -> 0x0098 }
            java.lang.Object r4 = r4.b()     // Catch:{ all -> 0x0098 }
            java.lang.Boolean r4 = (java.lang.Boolean) r4     // Catch:{ all -> 0x0098 }
            boolean r4 = r4.booleanValue()     // Catch:{ all -> 0x0098 }
            monitor-exit(r0)     // Catch:{ all -> 0x0098 }
            return r4
        L_0x003a:
            java.lang.String r5 = "com.google.android.gms"
            java.lang.String r1 = r4.getPackageName()     // Catch:{ all -> 0x0098 }
            boolean r5 = r5.equals(r1)     // Catch:{ all -> 0x0098 }
            r1 = 1
            r2 = 0
            if (r5 == 0) goto L_0x0049
            goto L_0x005f
        L_0x0049:
            android.content.pm.PackageManager r5 = r4.getPackageManager()     // Catch:{ all -> 0x0098 }
            java.lang.String r3 = "com.google.android.gms.phenotype"
            android.content.pm.ProviderInfo r5 = r5.resolveContentProvider(r3, r2)     // Catch:{ all -> 0x0098 }
            if (r5 == 0) goto L_0x0061
            java.lang.String r3 = "com.google.android.gms"
            java.lang.String r5 = r5.packageName     // Catch:{ all -> 0x0098 }
            boolean r5 = r3.equals(r5)     // Catch:{ all -> 0x0098 }
            if (r5 == 0) goto L_0x0061
        L_0x005f:
            r5 = 1
            goto L_0x0062
        L_0x0061:
            r5 = 0
        L_0x0062:
            if (r5 == 0) goto L_0x007a
            android.content.pm.PackageManager r4 = r4.getPackageManager()     // Catch:{ all -> 0x0098 }
            java.lang.String r5 = "com.google.android.gms"
            android.content.pm.ApplicationInfo r4 = r4.getApplicationInfo(r5, r2)     // Catch:{ NameNotFoundException -> 0x0076 }
            int r4 = r4.flags     // Catch:{ all -> 0x0098 }
            r4 = r4 & 129(0x81, float:1.81E-43)
            if (r4 == 0) goto L_0x0076
            r4 = 1
            goto L_0x0077
        L_0x0076:
            r4 = 0
        L_0x0077:
            if (r4 == 0) goto L_0x007a
            goto L_0x007b
        L_0x007a:
            r1 = 0
        L_0x007b:
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x0098 }
            j.c.a.a.f.e.d2 r5 = new j.c.a.a.f.e.d2     // Catch:{ all -> 0x0098 }
            if (r4 == 0) goto L_0x0096
            r5.<init>(r4)     // Catch:{ all -> 0x0098 }
            j.c.a.a.f.e.o1.a = r5     // Catch:{ all -> 0x0098 }
            monitor-exit(r0)     // Catch:{ all -> 0x0098 }
            j.c.a.a.f.e.a2<java.lang.Boolean> r4 = j.c.a.a.f.e.o1.a
            java.lang.Object r4 = r4.b()
            java.lang.Boolean r4 = (java.lang.Boolean) r4
            boolean r4 = r4.booleanValue()
            return r4
        L_0x0096:
            r4 = 0
            throw r4     // Catch:{ all -> 0x0098 }
        L_0x0098:
            r4 = move-exception
            monitor-exit(r0)     // Catch:{ all -> 0x0098 }
            throw r4
        L_0x009b:
            java.lang.IllegalArgumentException r4 = new java.lang.IllegalArgumentException
            r0 = 91
            int r0 = j.a.a.a.outline.a(r5, r0)
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>(r0)
            r1.append(r5)
            java.lang.String r5 = " is an unsupported authority. Only com.google.android.gms.phenotype authority is supported."
            r1.append(r5)
            java.lang.String r5 = r1.toString()
            r4.<init>(r5)
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.o1.a(android.content.Context, android.net.Uri):boolean");
    }
}
