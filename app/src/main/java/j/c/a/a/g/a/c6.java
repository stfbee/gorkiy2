package j.c.a.a.g.a;

import com.google.android.gms.measurement.internal.AppMeasurementDynamiteService;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk@@17.0.1 */
public final class c6 implements Runnable {
    public final /* synthetic */ fb b;
    public final /* synthetic */ AppMeasurementDynamiteService c;

    public c6(AppMeasurementDynamiteService appMeasurementDynamiteService, fb fbVar) {
        this.c = appMeasurementDynamiteService;
        this.b = fbVar;
    }

    public final void run() {
        z6 r2 = this.c.a.r();
        fb fbVar = this.b;
        r2.d();
        r2.w();
        r2.a(new f7(r2, r2.a(false), fbVar));
    }
}
