package j.c.a.a.g.a;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h7 implements Runnable {
    public final /* synthetic */ w6 b;
    public final /* synthetic */ z6 c;

    public h7(z6 z6Var, w6 w6Var) {
        this.c = z6Var;
        this.b = w6Var;
    }

    public final void run() {
        z6 z6Var = this.c;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Failed to send current screen to service");
            return;
        }
        try {
            if (this.b == null) {
                f3Var.a(0, (String) null, (String) null, z6Var.a.a.getPackageName());
            } else {
                f3Var.a(this.b.c, this.b.a, this.b.b, z6Var.a.a.getPackageName());
            }
            this.c.D();
        } catch (RemoteException e2) {
            this.c.a().f2046f.a("Failed to send current screen to the service", e2);
        }
    }
}
