package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final /* synthetic */ class e9 {
    public static final /* synthetic */ int[] a;
    public static final /* synthetic */ int[] b;

    /* JADX WARNING: Can't wrap try/catch for region: R(24:0|(2:1|2)|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Can't wrap try/catch for region: R(25:0|1|2|3|5|6|7|(2:9|10)|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Can't wrap try/catch for region: R(26:0|1|2|3|5|6|7|9|10|11|13|14|15|17|18|19|20|21|22|23|24|25|26|27|28|30) */
    /* JADX WARNING: Code restructure failed: missing block: B:31:?, code lost:
        return;
     */
    /* JADX WARNING: Failed to process nested try/catch */
    /* JADX WARNING: Missing exception handler attribute for start block: B:19:0x0030 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:21:0x0036 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x003c */
    /* JADX WARNING: Missing exception handler attribute for start block: B:25:0x0042 */
    /* JADX WARNING: Missing exception handler attribute for start block: B:27:0x0049 */
    static {
        /*
            j.c.a.a.f.e.c0$a[] r0 = j.c.a.a.f.e.c0.a.values()
            int r0 = r0.length
            int[] r0 = new int[r0]
            j.c.a.a.g.a.e9.b = r0
            r1 = 1
            j.c.a.a.f.e.c0$a r2 = j.c.a.a.f.e.c0.a.LESS_THAN     // Catch:{ NoSuchFieldError -> 0x000e }
            r0[r1] = r1     // Catch:{ NoSuchFieldError -> 0x000e }
        L_0x000e:
            r0 = 2
            int[] r2 = j.c.a.a.g.a.e9.b     // Catch:{ NoSuchFieldError -> 0x0015 }
            j.c.a.a.f.e.c0$a r3 = j.c.a.a.f.e.c0.a.GREATER_THAN     // Catch:{ NoSuchFieldError -> 0x0015 }
            r2[r0] = r0     // Catch:{ NoSuchFieldError -> 0x0015 }
        L_0x0015:
            r2 = 3
            int[] r3 = j.c.a.a.g.a.e9.b     // Catch:{ NoSuchFieldError -> 0x001c }
            j.c.a.a.f.e.c0$a r4 = j.c.a.a.f.e.c0.a.EQUAL     // Catch:{ NoSuchFieldError -> 0x001c }
            r3[r2] = r2     // Catch:{ NoSuchFieldError -> 0x001c }
        L_0x001c:
            r3 = 4
            int[] r4 = j.c.a.a.g.a.e9.b     // Catch:{ NoSuchFieldError -> 0x0023 }
            j.c.a.a.f.e.c0$a r5 = j.c.a.a.f.e.c0.a.BETWEEN     // Catch:{ NoSuchFieldError -> 0x0023 }
            r4[r3] = r3     // Catch:{ NoSuchFieldError -> 0x0023 }
        L_0x0023:
            j.c.a.a.f.e.e0$b[] r4 = j.c.a.a.f.e.e0.b.values()
            int r4 = r4.length
            int[] r4 = new int[r4]
            j.c.a.a.g.a.e9.a = r4
            j.c.a.a.f.e.e0$b r5 = j.c.a.a.f.e.e0.b.REGEXP     // Catch:{ NoSuchFieldError -> 0x0030 }
            r4[r1] = r1     // Catch:{ NoSuchFieldError -> 0x0030 }
        L_0x0030:
            int[] r1 = j.c.a.a.g.a.e9.a     // Catch:{ NoSuchFieldError -> 0x0036 }
            j.c.a.a.f.e.e0$b r4 = j.c.a.a.f.e.e0.b.BEGINS_WITH     // Catch:{ NoSuchFieldError -> 0x0036 }
            r1[r0] = r0     // Catch:{ NoSuchFieldError -> 0x0036 }
        L_0x0036:
            int[] r0 = j.c.a.a.g.a.e9.a     // Catch:{ NoSuchFieldError -> 0x003c }
            j.c.a.a.f.e.e0$b r1 = j.c.a.a.f.e.e0.b.ENDS_WITH     // Catch:{ NoSuchFieldError -> 0x003c }
            r0[r2] = r2     // Catch:{ NoSuchFieldError -> 0x003c }
        L_0x003c:
            int[] r0 = j.c.a.a.g.a.e9.a     // Catch:{ NoSuchFieldError -> 0x0042 }
            j.c.a.a.f.e.e0$b r1 = j.c.a.a.f.e.e0.b.PARTIAL     // Catch:{ NoSuchFieldError -> 0x0042 }
            r0[r3] = r3     // Catch:{ NoSuchFieldError -> 0x0042 }
        L_0x0042:
            int[] r0 = j.c.a.a.g.a.e9.a     // Catch:{ NoSuchFieldError -> 0x0049 }
            j.c.a.a.f.e.e0$b r1 = j.c.a.a.f.e.e0.b.EXACT     // Catch:{ NoSuchFieldError -> 0x0049 }
            r1 = 5
            r0[r1] = r1     // Catch:{ NoSuchFieldError -> 0x0049 }
        L_0x0049:
            int[] r0 = j.c.a.a.g.a.e9.a     // Catch:{ NoSuchFieldError -> 0x0050 }
            j.c.a.a.f.e.e0$b r1 = j.c.a.a.f.e.e0.b.IN_LIST     // Catch:{ NoSuchFieldError -> 0x0050 }
            r1 = 6
            r0[r1] = r1     // Catch:{ NoSuchFieldError -> 0x0050 }
        L_0x0050:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.g.a.e9.<clinit>():void");
    }
}
