package j.c.a.a.c.l;

import android.content.Context;
import android.content.res.Resources;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.i;
import javax.annotation.Nullable;

public class o {
    public final Resources a;
    public final String b;

    public o(Context context) {
        ResourcesFlusher.b(context);
        Resources resources = context.getResources();
        this.a = resources;
        this.b = resources.getResourcePackageName(i.common_google_play_services_unknown_issue);
    }

    @Nullable
    public String a(String str) {
        int identifier = this.a.getIdentifier(str, "string", this.b);
        if (identifier == 0) {
            return null;
        }
        return this.a.getString(identifier);
    }
}
