package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class a5 {
    public static final y4 a;
    public static final y4 b = new b5();

    static {
        y4 y4Var;
        try {
            y4Var = (y4) Class.forName("com.google.protobuf.MapFieldSchemaFull").getDeclaredConstructor(new Class[0]).newInstance(new Object[0]);
        } catch (Exception unused) {
            y4Var = null;
        }
        a = y4Var;
    }
}
