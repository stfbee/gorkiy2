package j.c.a.a.g.a;

import android.os.RemoteException;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class k7 implements Runnable {
    public final /* synthetic */ i b;
    public final /* synthetic */ String c;
    public final /* synthetic */ fb d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ z6 f2025e;

    public k7(z6 z6Var, i iVar, String str, fb fbVar) {
        this.f2025e = z6Var;
        this.b = iVar;
        this.c = str;
        this.d = fbVar;
    }

    public final void run() {
        byte[] bArr = null;
        try {
            f3 f3Var = this.f2025e.d;
            if (f3Var == null) {
                this.f2025e.a().f2046f.a("Discarding data. Failed to send event to service to bundle");
                return;
            }
            bArr = f3Var.a(this.b, this.c);
            this.f2025e.D();
            this.f2025e.k().a(this.d, bArr);
        } catch (RemoteException e2) {
            this.f2025e.a().f2046f.a("Failed to send event to the service to bundle", e2);
        } finally {
            this.f2025e.k().a(this.d, bArr);
        }
    }
}
