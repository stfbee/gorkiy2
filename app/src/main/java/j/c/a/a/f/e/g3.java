package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class g3 implements c7 {
    public final zzek a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T
     arg types: [com.google.android.gms.internal.measurement.zzek, java.lang.String]
     candidates:
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.Object):java.lang.Object
      j.c.a.a.f.e.y3.a(java.lang.Object, java.lang.String):T */
    public g3(zzek zzek) {
        y3.a((Object) zzek, "output");
        zzek zzek2 = zzek;
        this.a = zzek2;
        zzek2.a = this;
    }

    public final void a(int i2, int i3) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 5);
        aVar.c(i3);
    }

    public final void b(int i2, long j2) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 1);
        aVar.b(j2);
    }

    public final void c(int i2, long j2) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 0);
        aVar.a(j2);
    }

    public final void d(int i2, long j2) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 1);
        aVar.b(j2);
    }

    public final void e(int i2, long j2) {
        zzek zzek = this.a;
        if (zzek != null) {
            long e2 = zzek.e(j2);
            zzek.a aVar = (zzek.a) zzek;
            aVar.b((i2 << 3) | 0);
            aVar.a(e2);
            return;
        }
        throw null;
    }

    public final void c(int i2, int i3) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 0);
        aVar.b(i3);
    }

    public final void d(int i2, int i3) {
        zzek zzek = this.a;
        if (zzek != null) {
            int n2 = zzek.n(i3);
            zzek.a aVar = (zzek.a) zzek;
            aVar.b((i2 << 3) | 0);
            aVar.b(n2);
            return;
        }
        throw null;
    }

    public final void a(int i2, long j2) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 0);
        aVar.a(j2);
    }

    public final void b(int i2, int i3) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 5);
        aVar.c(i3);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
     arg types: [j.c.a.a.f.e.f5, j.c.a.a.f.e.g3]
     candidates:
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void */
    public final void b(int i2, Object obj, t5 t5Var) {
        zzek zzek = this.a;
        int i3 = i2 << 3;
        ((zzek.a) zzek).b(i3 | 3);
        t5Var.a((Object) ((f5) obj), (c7) zzek.a);
        ((zzek.a) zzek).b(i3 | 4);
    }

    public final void a(int i2, float f2) {
        zzek zzek = this.a;
        if (zzek != null) {
            int floatToRawIntBits = Float.floatToRawIntBits(f2);
            zzek.a aVar = (zzek.a) zzek;
            aVar.b((i2 << 3) | 5);
            aVar.c(floatToRawIntBits);
            return;
        }
        throw null;
    }

    public final void a(int i2, double d) {
        zzek zzek = this.a;
        if (zzek != null) {
            long doubleToRawLongBits = Double.doubleToRawLongBits(d);
            zzek.a aVar = (zzek.a) zzek;
            aVar.b((i2 << 3) | 1);
            aVar.b(doubleToRawLongBits);
            return;
        }
        throw null;
    }

    public final void a(int i2, boolean z) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 0);
        aVar.a(z ? (byte) 1 : 0);
    }

    public final void a(int i2, w2 w2Var) {
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 2);
        aVar.b(w2Var);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void
     arg types: [j.c.a.a.f.e.f5, j.c.a.a.f.e.g3]
     candidates:
      j.c.a.a.f.e.t5.a(java.lang.Object, java.lang.Object):boolean
      j.c.a.a.f.e.t5.a(java.lang.Object, j.c.a.a.f.e.c7):void */
    public final void a(int i2, Object obj, t5 t5Var) {
        f5 f5Var = (f5) obj;
        zzek.a aVar = (zzek.a) this.a;
        aVar.b((i2 << 3) | 2);
        n2 n2Var = (n2) f5Var;
        x3 x3Var = (x3) n2Var;
        int i3 = x3Var.zzc;
        if (i3 == -1) {
            i3 = t5Var.c(n2Var);
            x3Var.zzc = i3;
        }
        aVar.b(i3);
        t5Var.a((Object) f5Var, (c7) aVar.a);
    }

    public final void a(int i2, Object obj) {
        if (obj instanceof w2) {
            zzek.a aVar = (zzek.a) this.a;
            aVar.f(1, 3);
            aVar.g(2, i2);
            aVar.b(3, (w2) obj);
            aVar.f(1, 4);
            return;
        }
        zzek.a aVar2 = (zzek.a) this.a;
        aVar2.f(1, 3);
        aVar2.g(2, i2);
        aVar2.f(3, 2);
        aVar2.c((f5) obj);
        aVar2.f(1, 4);
    }
}
