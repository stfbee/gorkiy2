package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class u7 implements r7 {
    public static final p1<Long> A;
    public static final p1<Long> B;
    public static final p1<Long> C;
    public static final p1<Long> D;
    public static final p1<Long> E;
    public static final p1<Long> F;
    public static final p1<Long> G;
    public static final p1<Long> H;
    public static final p1<String> I;
    public static final p1<Long> J;
    public static final p1<Long> a;
    public static final p1<Long> b;
    public static final p1<String> c;
    public static final p1<String> d;

    /* renamed from: e  reason: collision with root package name */
    public static final p1<String> f1910e;

    /* renamed from: f  reason: collision with root package name */
    public static final p1<Long> f1911f;
    public static final p1<Long> g;
    public static final p1<Long> h;

    /* renamed from: i  reason: collision with root package name */
    public static final p1<Long> f1912i;

    /* renamed from: j  reason: collision with root package name */
    public static final p1<Long> f1913j;

    /* renamed from: k  reason: collision with root package name */
    public static final p1<Long> f1914k;

    /* renamed from: l  reason: collision with root package name */
    public static final p1<Long> f1915l;

    /* renamed from: m  reason: collision with root package name */
    public static final p1<Long> f1916m;

    /* renamed from: n  reason: collision with root package name */
    public static final p1<Long> f1917n;

    /* renamed from: o  reason: collision with root package name */
    public static final p1<Long> f1918o;

    /* renamed from: p  reason: collision with root package name */
    public static final p1<Long> f1919p;

    /* renamed from: q  reason: collision with root package name */
    public static final p1<Long> f1920q;

    /* renamed from: r  reason: collision with root package name */
    public static final p1<String> f1921r;

    /* renamed from: s  reason: collision with root package name */
    public static final p1<Long> f1922s;

    /* renamed from: t  reason: collision with root package name */
    public static final p1<Long> f1923t;
    public static final p1<Long> u;
    public static final p1<Long> v;
    public static final p1<Long> w;
    public static final p1<Long> x;
    public static final p1<Long> y;
    public static final p1<Long> z;

    static {
        v1 v1Var = new v1(q1.a("com.google.android.gms.measurement"));
        a = p1.a(v1Var, "measurement.ad_id_cache_time", 10000);
        b = p1.a(v1Var, "measurement.config.cache_time", 86400000);
        c = p1.a(v1Var, "measurement.log_tag", "FA");
        d = p1.a(v1Var, "measurement.config.url_authority", "app-measurement.com");
        f1910e = p1.a(v1Var, "measurement.config.url_scheme", "https");
        f1911f = p1.a(v1Var, "measurement.upload.debug_upload_interval", 1000);
        g = p1.a(v1Var, "measurement.lifetimevalue.max_currency_tracked", 4);
        h = p1.a(v1Var, "measurement.store.max_stored_events_per_app", 100000);
        f1912i = p1.a(v1Var, "measurement.experiment.max_ids", 50);
        f1913j = p1.a(v1Var, "measurement.audience.filter_result_max_count", 200);
        f1914k = p1.a(v1Var, "measurement.alarm_manager.minimum_interval", 60000);
        f1915l = p1.a(v1Var, "measurement.upload.minimum_delay", 500);
        f1916m = p1.a(v1Var, "measurement.monitoring.sample_period_millis", 86400000);
        f1917n = p1.a(v1Var, "measurement.upload.realtime_upload_interval", 10000);
        f1918o = p1.a(v1Var, "measurement.upload.refresh_blacklisted_config_interval", 604800000);
        f1919p = p1.a(v1Var, "measurement.config.cache_time.service", 3600000);
        f1920q = p1.a(v1Var, "measurement.service_client.idle_disconnect_millis", 5000);
        f1921r = p1.a(v1Var, "measurement.log_tag.service", "FA-SVC");
        f1922s = p1.a(v1Var, "measurement.upload.stale_data_deletion_interval", 86400000);
        f1923t = p1.a(v1Var, "measurement.upload.backoff_period", 43200000);
        u = p1.a(v1Var, "measurement.upload.initial_upload_delay_time", 15000);
        v = p1.a(v1Var, "measurement.upload.interval", 3600000);
        w = p1.a(v1Var, "measurement.upload.max_bundle_size", 65536);
        x = p1.a(v1Var, "measurement.upload.max_bundles", 100);
        y = p1.a(v1Var, "measurement.upload.max_conversions_per_day", 500);
        z = p1.a(v1Var, "measurement.upload.max_error_events_per_day", 1000);
        A = p1.a(v1Var, "measurement.upload.max_events_per_bundle", 1000);
        B = p1.a(v1Var, "measurement.upload.max_events_per_day", 100000);
        C = p1.a(v1Var, "measurement.upload.max_public_events_per_day", 50000);
        D = p1.a(v1Var, "measurement.upload.max_queue_time", 2419200000L);
        E = p1.a(v1Var, "measurement.upload.max_realtime_events_per_day", 10);
        F = p1.a(v1Var, "measurement.upload.max_batch_size", 65536);
        G = p1.a(v1Var, "measurement.upload.retry_count", 6);
        H = p1.a(v1Var, "measurement.upload.retry_time", 1800000);
        I = p1.a(v1Var, "measurement.upload.url", "https://app-measurement.com/a");
        J = p1.a(v1Var, "measurement.upload.window_interval", 3600000);
    }

    public final long A() {
        return g.b().longValue();
    }

    public final long B() {
        return B.b().longValue();
    }

    public final long C() {
        return f1912i.b().longValue();
    }

    public final long D() {
        return D.b().longValue();
    }

    public final long E() {
        return x.b().longValue();
    }

    public final long F() {
        return F.b().longValue();
    }

    public final long G() {
        return H.b().longValue();
    }

    public final long H() {
        return f1923t.b().longValue();
    }

    public final long a() {
        return a.b().longValue();
    }

    public final long b() {
        return b.b().longValue();
    }

    public final String c() {
        return c.b();
    }

    public final String d() {
        return d.b();
    }

    public final String e() {
        return f1910e.b();
    }

    public final long f() {
        return f1917n.b().longValue();
    }

    public final long g() {
        return f1911f.b().longValue();
    }

    public final long h() {
        return u.b().longValue();
    }

    public final long i() {
        return f1920q.b().longValue();
    }

    public final long j() {
        return f1916m.b().longValue();
    }

    public final long k() {
        return v.b().longValue();
    }

    public final long l() {
        return J.b().longValue();
    }

    public final long m() {
        return f1913j.b().longValue();
    }

    public final long n() {
        return f1915l.b().longValue();
    }

    public final long o() {
        return A.b().longValue();
    }

    public final long p() {
        return w.b().longValue();
    }

    public final long q() {
        return C.b().longValue();
    }

    public final long r() {
        return y.b().longValue();
    }

    public final long s() {
        return E.b().longValue();
    }

    public final long t() {
        return h.b().longValue();
    }

    public final long u() {
        return f1922s.b().longValue();
    }

    public final long v() {
        return G.b().longValue();
    }

    public final String w() {
        return I.b();
    }

    public final long x() {
        return f1918o.b().longValue();
    }

    public final long y() {
        return f1914k.b().longValue();
    }

    public final long z() {
        return z.b().longValue();
    }
}
