package j.c.a.a.c.l;

import android.content.ComponentName;
import android.content.Context;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.os.Message;
import j.c.a.a.c.l.h;
import j.c.a.a.c.m.a;
import java.util.HashSet;
import java.util.Set;

public final class y implements ServiceConnection {
    public final Set<ServiceConnection> a = new HashSet();
    public int b = 2;
    public boolean c;
    public IBinder d;

    /* renamed from: e  reason: collision with root package name */
    public final h.a f1826e;

    /* renamed from: f  reason: collision with root package name */
    public ComponentName f1827f;
    public final /* synthetic */ x g;

    public y(x xVar, h.a aVar) {
        this.g = xVar;
        this.f1826e = aVar;
    }

    public final void a(String str) {
        this.b = 3;
        x xVar = this.g;
        boolean b2 = xVar.f1825f.b(xVar.d, this.f1826e.a(), this, this.f1826e.d);
        this.c = b2;
        if (b2) {
            Message obtainMessage = this.g.f1824e.obtainMessage(1, this.f1826e);
            x xVar2 = this.g;
            xVar2.f1824e.sendMessageDelayed(obtainMessage, xVar2.h);
            return;
        }
        this.b = 2;
        try {
            a aVar = this.g.f1825f;
            Context context = this.g.d;
            if (aVar != null) {
                context.unbindService(this);
                return;
            }
            throw null;
        } catch (IllegalArgumentException unused) {
        }
    }

    public final void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        synchronized (this.g.c) {
            this.g.f1824e.removeMessages(1, this.f1826e);
            this.d = iBinder;
            this.f1827f = componentName;
            for (ServiceConnection onServiceConnected : this.a) {
                onServiceConnected.onServiceConnected(componentName, iBinder);
            }
            this.b = 1;
        }
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        synchronized (this.g.c) {
            this.g.f1824e.removeMessages(1, this.f1826e);
            this.d = null;
            this.f1827f = componentName;
            for (ServiceConnection onServiceDisconnected : this.a) {
                onServiceDisconnected.onServiceDisconnected(componentName);
            }
            this.b = 2;
        }
    }
}
