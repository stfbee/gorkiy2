package j.c.a.a.f.e;

import android.app.Activity;
import j.c.a.a.d.b;
import j.c.a.a.f.e.pb;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public final class v extends pb.a {

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ Activity f1924f;
    public final /* synthetic */ pb.b g;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public v(pb.b bVar, Activity activity) {
        super(true);
        this.g = bVar;
        this.f1924f = activity;
    }

    public final void a() {
        pb.this.g.onActivityResumed(new b(this.f1924f), super.c);
    }
}
