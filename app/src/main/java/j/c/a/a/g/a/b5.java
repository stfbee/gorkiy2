package j.c.a.a.g.a;

import java.util.List;
import java.util.concurrent.Callable;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class b5 implements Callable<List<z8>> {
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ s4 f1941e;

    public b5(s4 s4Var, String str, String str2, String str3) {
        this.f1941e = s4Var;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public final /* synthetic */ Object call() {
        this.f1941e.a.o();
        return this.f1941e.a.e().a(this.b, this.c, this.d);
    }
}
