package j.c.a.a.g.a;

import i.b.k.ResourcesFlusher;
import java.lang.Thread;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n4 implements Thread.UncaughtExceptionHandler {
    public final String a;
    public final /* synthetic */ l4 b;

    public n4(l4 l4Var, String str) {
        this.b = l4Var;
        ResourcesFlusher.b((Object) str);
        this.a = str;
    }

    public final synchronized void uncaughtException(Thread thread, Throwable th) {
        this.b.a().f2046f.a(this.a, th);
    }
}
