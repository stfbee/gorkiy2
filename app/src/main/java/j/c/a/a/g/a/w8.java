package j.c.a.a.g.a;

import android.os.Parcel;
import android.os.Parcelable;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class w8 implements Parcelable.Creator<x8> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        Parcel parcel2 = parcel;
        int b = ResourcesFlusher.b(parcel);
        String str = null;
        Long l2 = null;
        Float f2 = null;
        String str2 = null;
        String str3 = null;
        Double d = null;
        long j2 = 0;
        int i2 = 0;
        while (parcel.dataPosition() < b) {
            int readInt = parcel.readInt();
            switch (65535 & readInt) {
                case 1:
                    i2 = ResourcesFlusher.f(parcel2, readInt);
                    break;
                case 2:
                    str = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 3:
                    j2 = ResourcesFlusher.g(parcel2, readInt);
                    break;
                case 4:
                    int h = ResourcesFlusher.h(parcel2, readInt);
                    if (h != 0) {
                        ResourcesFlusher.c(parcel2, h, 8);
                        l2 = Long.valueOf(parcel.readLong());
                        break;
                    } else {
                        l2 = null;
                        break;
                    }
                case 5:
                    int h2 = ResourcesFlusher.h(parcel2, readInt);
                    if (h2 != 0) {
                        ResourcesFlusher.c(parcel2, h2, 4);
                        f2 = Float.valueOf(parcel.readFloat());
                        break;
                    } else {
                        f2 = null;
                        break;
                    }
                case 6:
                    str2 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 7:
                    str3 = ResourcesFlusher.b(parcel2, readInt);
                    break;
                case 8:
                    int h3 = ResourcesFlusher.h(parcel2, readInt);
                    if (h3 != 0) {
                        ResourcesFlusher.c(parcel2, h3, 8);
                        d = Double.valueOf(parcel.readDouble());
                        break;
                    } else {
                        d = null;
                        break;
                    }
                default:
                    ResourcesFlusher.i(parcel2, readInt);
                    break;
            }
        }
        ResourcesFlusher.c(parcel2, b);
        return new x8(i2, str, j2, l2, f2, str2, str3, d);
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new x8[i2];
    }
}
