package j.c.a.a.g.a;

import java.util.concurrent.atomic.AtomicReference;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class f6 implements Runnable {
    public final /* synthetic */ AtomicReference b;
    public final /* synthetic */ String c;
    public final /* synthetic */ String d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ String f1987e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ x5 f1988f;

    public f6(x5 x5Var, AtomicReference atomicReference, String str, String str2, String str3) {
        this.f1988f = x5Var;
        this.b = atomicReference;
        this.c = str;
        this.d = str2;
        this.f1987e = str3;
    }

    public final void run() {
        z6 r2 = this.f1988f.a.r();
        AtomicReference atomicReference = this.b;
        String str = this.c;
        String str2 = this.d;
        String str3 = this.f1987e;
        r2.d();
        r2.w();
        r2.a(new n7(r2, atomicReference, str, str2, str3, r2.a(false)));
    }
}
