package j.c.a.a.f.e;

import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import j.c.a.a.f.e.q0;
import j.c.a.a.f.e.x3;
import j.c.a.a.f.e.y0;
import java.util.Collections;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class u0 extends x3<u0, a> implements g5 {
    public static final u0 zzav;
    public static volatile m5<u0> zzaw;
    public int zzaa;
    public String zzab = "";
    public String zzac = "";
    public boolean zzad;
    public e4<o0> zzae;
    public String zzaf;
    public int zzag;
    public int zzah;
    public int zzai;
    public String zzaj;
    public long zzak;
    public long zzal;
    public String zzam;
    public String zzan;
    public int zzao;
    public String zzap;
    public v0 zzaq;
    public c4 zzar;
    public long zzas;
    public long zzat;
    public String zzau;
    public int zzc;
    public int zzd;
    public int zze;
    public e4<q0> zzf;
    public e4<y0> zzg;
    public long zzh;
    public long zzi;
    public long zzj;
    public long zzk;
    public long zzl;
    public String zzm = "";
    public String zzn = "";
    public String zzo = "";
    public String zzp = "";
    public int zzq;
    public String zzr = "";
    public String zzs = "";
    public String zzt = "";
    public long zzu;
    public long zzv;
    public String zzw = "";
    public boolean zzx;
    public String zzy = "";
    public long zzz;

    static {
        u0 u0Var = new u0();
        zzav = u0Var;
        x3.zzd.put(u0.class, u0Var);
    }

    public u0() {
        s5<Object> s5Var = s5.f1905e;
        this.zzf = s5Var;
        this.zzg = s5Var;
        this.zzae = s5Var;
        this.zzaf = "";
        this.zzaj = "";
        this.zzam = "";
        this.zzan = "";
        this.zzap = "";
        this.zzar = z3.f1932e;
        this.zzau = "";
    }

    public static /* synthetic */ void a(u0 u0Var) {
        if (u0Var != null) {
            u0Var.zzf = s5.f1905e;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void b(u0 u0Var) {
        if (u0Var != null) {
            u0Var.zzae = s5.f1905e;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void c(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 256;
            u0Var.zzo = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void d(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 512;
            u0Var.zzp = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void e(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 2048;
            u0Var.zzr = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void f(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= CodedOutputStream.DEFAULT_BUFFER_SIZE;
            u0Var.zzs = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void g(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 8192;
            u0Var.zzt = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void h(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= LogFileManager.MAX_LOG_SIZE;
            u0Var.zzw = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void i(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 262144;
            u0Var.zzy = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void k(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 4194304;
            u0Var.zzac = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void l(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 16777216;
            u0Var.zzaf = str;
            return;
        }
        throw null;
    }

    public static a o() {
        return (a) zzav.g();
    }

    public final void j() {
        if (!this.zzf.a()) {
            this.zzf = x3.a(this.zzf);
        }
    }

    public final void m() {
        if (!this.zzg.a()) {
            this.zzg = x3.a(this.zzg);
        }
    }

    public final String n() {
        return this.zzs;
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<u0, a> implements g5 {
        public a() {
            super(u0.zzav);
        }

        public final a a() {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 1;
            u0Var.zze = 1;
            return this;
        }

        public final a b(int i2) {
            i();
            u0.a((u0) super.c, i2);
            return this;
        }

        public final y0 c(int i2) {
            return ((u0) super.c).zzg.get(i2);
        }

        public final a d(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 32;
            u0Var.zzl = j2;
            return this;
        }

        public final a e(String str) {
            i();
            u0.e((u0) super.c, str);
            return this;
        }

        public final a f(String str) {
            i();
            u0.f((u0) super.c, str);
            return this;
        }

        public final a g(String str) {
            i();
            u0.g((u0) super.c, str);
            return this;
        }

        public final a h(String str) {
            i();
            u0.h((u0) super.c, str);
            return this;
        }

        public final a i(String str) {
            i();
            u0.i((u0) super.c, str);
            return this;
        }

        public final a j(String str) {
            i();
            u0.j((u0) super.c, str);
            return this;
        }

        public final a k(String str) {
            i();
            u0.k((u0) super.c, str);
            return this;
        }

        public final a l(String str) {
            i();
            u0.l((u0) super.c, str);
            return this;
        }

        public final a m(String str) {
            i();
            u0.m((u0) super.c, str);
            return this;
        }

        public final int n() {
            return ((u0) super.c).zzf.size();
        }

        public final a o() {
            i();
            u0.a((u0) super.c);
            return this;
        }

        public final List<y0> p() {
            return Collections.unmodifiableList(((u0) super.c).zzg);
        }

        public final int q() {
            return ((u0) super.c).zzg.size();
        }

        public final long r() {
            return ((u0) super.c).zzi;
        }

        public final long s() {
            return ((u0) super.c).zzj;
        }

        public final a t() {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc &= -17;
            u0Var.zzk = 0;
            return this;
        }

        public final a u() {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc &= -33;
            u0Var.zzl = 0;
            return this;
        }

        public final String v() {
            return ((u0) super.c).zzs;
        }

        public final a w() {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc &= -2097153;
            u0Var.zzab = u0.zzav.zzab;
            return this;
        }

        public final String x() {
            return ((u0) super.c).zzac;
        }

        public final a y() {
            i();
            u0.b((u0) super.c);
            return this;
        }

        public final a z() {
            i();
            u0 u0Var = (u0) super.c;
            throw null;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(u0.zzav);
        }

        public final a b(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 8;
            u0Var.zzj = j2;
            return this;
        }

        public final a c(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 16;
            u0Var.zzk = j2;
            return this;
        }

        public final a e(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 16384;
            u0Var.zzu = j2;
            return this;
        }

        public final a f(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 524288;
            u0Var.zzz = j2;
            return this;
        }

        public final a g(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 536870912;
            u0Var.zzak = j2;
            return this;
        }

        public final a h(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 1073741824;
            u0Var.zzal = j2;
            return this;
        }

        public final a i(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzd |= 16;
            u0Var.zzas = j2;
            return this;
        }

        public final a n(String str) {
            i();
            u0.n((u0) super.c, str);
            return this;
        }

        /* renamed from: a  reason: collision with other method in class */
        public final List<q0> m14a() {
            return Collections.unmodifiableList(((u0) super.c).zzf);
        }

        public final a d(String str) {
            i();
            u0.d((u0) super.c, str);
            return this;
        }

        public final a b(String str) {
            i();
            u0.b((u0) super.c, str);
            return this;
        }

        public final a c(String str) {
            i();
            u0.c((u0) super.c, str);
            return this;
        }

        public final a d(int i2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 1024;
            u0Var.zzq = i2;
            return this;
        }

        public final a e(int i2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 1048576;
            u0Var.zzaa = i2;
            return this;
        }

        public final a f(int i2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 33554432;
            u0Var.zzag = i2;
            return this;
        }

        public final q0 a(int i2) {
            return ((u0) super.c).zzf.get(i2);
        }

        public final a b(Iterable<? extends o0> iterable) {
            i();
            u0 u0Var = (u0) super.c;
            if (!u0Var.zzae.a()) {
                u0Var.zzae = x3.a(u0Var.zzae);
            }
            n2.a(iterable, u0Var.zzae);
            return this;
        }

        public final a c(Iterable<? extends Integer> iterable) {
            i();
            u0 u0Var = (u0) super.c;
            if (!u0Var.zzar.a()) {
                c4 c4Var = u0Var.zzar;
                int size = c4Var.size();
                u0Var.zzar = ((z3) c4Var).a(size == 0 ? 10 : size << 1);
            }
            n2.a(iterable, u0Var.zzar);
            return this;
        }

        public final a a(int i2, q0.a aVar) {
            i();
            u0.a((u0) super.c, i2, aVar);
            return this;
        }

        public final a a(q0.a aVar) {
            i();
            u0.a((u0) super.c, aVar);
            return this;
        }

        public final a a(Iterable<? extends q0> iterable) {
            i();
            u0.a((u0) super.c, iterable);
            return this;
        }

        public final a a(int i2, y0 y0Var) {
            i();
            u0.a((u0) super.c, i2, y0Var);
            return this;
        }

        public final a a(y0 y0Var) {
            i();
            u0.a((u0) super.c, y0Var);
            return this;
        }

        public final a a(y0.a aVar) {
            i();
            u0.a((u0) super.c, aVar);
            return this;
        }

        public final a a(long j2) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 4;
            u0Var.zzi = j2;
            return this;
        }

        public final a a(String str) {
            i();
            u0.a((u0) super.c, str);
            return this;
        }

        public final a a(boolean z) {
            i();
            u0 u0Var = (u0) super.c;
            u0Var.zzc |= 131072;
            u0Var.zzx = z;
            return this;
        }
    }

    public static /* synthetic */ void n(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzd |= 4;
            u0Var.zzap = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(u0 u0Var, int i2, q0.a aVar) {
        u0Var.j();
        u0Var.zzf.set(i2, (q0) ((x3) aVar.m()));
    }

    public static /* synthetic */ void b(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 128;
            u0Var.zzn = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void j(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 2097152;
            u0Var.zzab = str;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void m(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 268435456;
            u0Var.zzaj = str;
            return;
        }
        throw null;
    }

    public final String i() {
        return this.zzac;
    }

    public static /* synthetic */ void a(u0 u0Var, q0.a aVar) {
        u0Var.j();
        u0Var.zzf.add((q0) ((x3) aVar.m()));
    }

    public static /* synthetic */ void a(u0 u0Var, Iterable iterable) {
        u0Var.j();
        n2.a(iterable, u0Var.zzf);
    }

    public static /* synthetic */ void a(u0 u0Var, int i2) {
        u0Var.j();
        u0Var.zzf.remove(i2);
    }

    public static /* synthetic */ void a(u0 u0Var, int i2, y0 y0Var) {
        if (y0Var != null) {
            u0Var.m();
            u0Var.zzg.set(i2, y0Var);
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(u0 u0Var, y0 y0Var) {
        if (y0Var != null) {
            u0Var.m();
            u0Var.zzg.add(y0Var);
            return;
        }
        throw null;
    }

    public static /* synthetic */ void a(u0 u0Var, y0.a aVar) {
        u0Var.m();
        u0Var.zzg.add((y0) ((x3) aVar.m()));
    }

    public static /* synthetic */ void a(u0 u0Var, String str) {
        if (str != null) {
            u0Var.zzc |= 64;
            u0Var.zzm = str;
            return;
        }
        throw null;
    }

    public static u0 a(byte[] bArr, j3 j3Var) {
        return (u0) x3.a(zzav, bArr, j3Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new u0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzav, "\u0001+\u0000\u0002\u00012+\u0000\u0004\u0000\u0001\u0004\u0000\u0002\u001b\u0003\u001b\u0004\u0002\u0001\u0005\u0002\u0002\u0006\u0002\u0003\u0007\u0002\u0005\b\b\u0006\t\b\u0007\n\b\b\u000b\b\t\f\u0004\n\r\b\u000b\u000e\b\f\u0010\b\r\u0011\u0002\u000e\u0012\u0002\u000f\u0013\b\u0010\u0014\u0007\u0011\u0015\b\u0012\u0016\u0002\u0013\u0017\u0004\u0014\u0018\b\u0015\u0019\b\u0016\u001a\u0002\u0004\u001c\u0007\u0017\u001d\u001b\u001e\b\u0018\u001f\u0004\u0019 \u0004\u001a!\u0004\u001b\"\b\u001c#\u0002\u001d$\u0002\u001e%\b\u001f&\b '\u0004!)\b\",\t#-\u001d.\u0002$/\u0002%2\b&", new Object[]{"zzc", "zzd", "zze", "zzf", q0.class, "zzg", y0.class, "zzh", "zzi", "zzj", "zzl", "zzm", "zzn", "zzo", "zzp", "zzq", "zzr", "zzs", "zzt", "zzu", "zzv", "zzw", "zzx", "zzy", "zzz", "zzaa", "zzab", "zzac", "zzk", "zzad", "zzae", o0.class, "zzaf", "zzag", "zzah", "zzai", "zzaj", "zzak", "zzal", "zzam", "zzan", "zzao", "zzap", "zzaq", "zzar", "zzas", "zzat", "zzau"});
            case 4:
                return zzav;
            case 5:
                m5<u0> m5Var = zzaw;
                if (m5Var == null) {
                    synchronized (u0.class) {
                        m5Var = zzaw;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzav);
                            zzaw = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
