package j.c.a.a.g.a;

import j.c.a.a.c.n.b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i8 implements Runnable {
    public final /* synthetic */ long b;
    public final /* synthetic */ f8 c;

    public i8(f8 f8Var, long j2) {
        this.c = f8Var;
        this.b = j2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.f8.a(long, boolean):void
     arg types: [long, int]
     candidates:
      j.c.a.a.g.a.f8.a(boolean, boolean):boolean
      j.c.a.a.g.a.f8.a(long, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(long, long):long}
     arg types: [int, long]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(float, float):float}
      ClspMth{java.lang.Math.max(long, long):long} */
    public final void run() {
        f8 f8Var = this.c;
        long j2 = this.b;
        f8Var.d();
        f8Var.A();
        if (f8Var.a.g.a(k.I0)) {
            f8Var.c.removeCallbacks(f8Var.h);
        }
        i9 i9Var = f8Var.a.g;
        g3 q2 = f8Var.q();
        q2.w();
        if (i9Var.d(q2.c, k.c0)) {
            f8Var.l().y.a(false);
        }
        f8Var.a().f2052n.a("Activity resumed, time", Long.valueOf(j2));
        f8Var.d = j2;
        f8Var.f1989e = j2;
        if (f8Var.a.c()) {
            i9 i9Var2 = f8Var.a.g;
            g3 q3 = f8Var.q();
            q3.w();
            String str = q3.c;
            if (i9Var2 == null) {
                throw null;
            } else if (!i9Var2.d(str, k.a0)) {
                f8Var.f1990f.b();
                f8Var.g.b();
                w3 l2 = f8Var.l();
                if (((b) f8Var.a.f2092n) != null) {
                    if (l2.a(System.currentTimeMillis())) {
                        f8Var.l().f2122r.a(true);
                        f8Var.l().w.a(0);
                    }
                    if (f8Var.l().f2122r.a()) {
                        f8Var.f1990f.a(Math.max(0L, f8Var.l().f2120p.a() - f8Var.l().w.a()));
                    } else {
                        f8Var.g.a(Math.max(0L, 3600000 - f8Var.l().w.a()));
                    }
                } else {
                    throw null;
                }
            } else if (((b) f8Var.a.f2092n) != null) {
                f8Var.a(System.currentTimeMillis(), false);
            } else {
                throw null;
            }
        }
    }
}
