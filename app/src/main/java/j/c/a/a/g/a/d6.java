package j.c.a.a.g.a;

import android.os.Bundle;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d6 implements Runnable {
    public final /* synthetic */ Bundle b;
    public final /* synthetic */ x5 c;

    public d6(x5 x5Var, Bundle bundle) {
        this.c = x5Var;
        this.b = bundle;
    }

    public final void run() {
        x5 x5Var = this.c;
        Bundle bundle = this.b;
        x5Var.d();
        x5Var.w();
        ResourcesFlusher.b(bundle);
        ResourcesFlusher.b(bundle.getString(DefaultAppMeasurementEventListenerRegistrar.NAME));
        ResourcesFlusher.b(bundle.getString("origin"));
        ResourcesFlusher.b(bundle.get("value"));
        if (!x5Var.a.c()) {
            x5Var.a().f2051m.a("Conditional property not sent since collection is disabled");
            return;
        }
        x8 x8Var = new x8(bundle.getString(DefaultAppMeasurementEventListenerRegistrar.NAME), bundle.getLong("triggered_timestamp"), bundle.get("value"), bundle.getString("origin"));
        try {
            i a = x5Var.k().a(bundle.getString("app_id"), bundle.getString("triggered_event_name"), bundle.getBundle("triggered_event_params"), bundle.getString("origin"), 0);
            i a2 = x5Var.k().a(bundle.getString("app_id"), bundle.getString("timed_out_event_name"), bundle.getBundle("timed_out_event_params"), bundle.getString("origin"), 0);
            i a3 = x5Var.k().a(bundle.getString("app_id"), bundle.getString("expired_event_name"), bundle.getBundle("expired_event_params"), bundle.getString("origin"), 0);
            String string = bundle.getString("app_id");
            String string2 = bundle.getString("origin");
            long j2 = bundle.getLong("creation_timestamp");
            String string3 = bundle.getString("trigger_event_name");
            long j3 = bundle.getLong("trigger_timeout");
            long j4 = bundle.getLong("time_to_live");
            g9 g9Var = r3;
            g9 g9Var2 = new g9(string, string2, x8Var, j2, false, string3, a2, j3, a, j4, a3);
            x5Var.r().a(g9Var);
        } catch (IllegalArgumentException unused) {
        }
    }
}
