package j.c.a.a.g.a;

import android.os.RemoteException;
import android.text.TextUtils;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l7 implements Runnable {
    public final /* synthetic */ boolean b;
    public final /* synthetic */ boolean c;
    public final /* synthetic */ i d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ d9 f2035e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ String f2036f;
    public final /* synthetic */ z6 g;

    public l7(z6 z6Var, boolean z, boolean z2, i iVar, d9 d9Var, String str) {
        this.g = z6Var;
        this.b = z;
        this.c = z2;
        this.d = iVar;
        this.f2035e = d9Var;
        this.f2036f = str;
    }

    public final void run() {
        z6 z6Var = this.g;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Discarding data. Failed to send event to service");
            return;
        }
        if (this.b) {
            z6Var.a(f3Var, this.c ? null : this.d, this.f2035e);
        } else {
            try {
                if (TextUtils.isEmpty(this.f2036f)) {
                    f3Var.a(this.d, this.f2035e);
                } else {
                    f3Var.a(this.d, this.f2036f, this.g.a().z());
                }
            } catch (RemoteException e2) {
                this.g.a().f2046f.a("Failed to send event to the service", e2);
            }
        }
        this.g.D();
    }
}
