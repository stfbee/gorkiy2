package j.c.a.a.c.l;

import android.os.IBinder;
import android.os.Parcel;
import j.c.a.a.c.z;
import j.c.a.a.f.c.a;
import j.c.a.a.f.c.c;

public final class f0 extends a implements d0 {
    public f0(IBinder iBinder) {
        super(iBinder, "com.google.android.gms.common.internal.IGoogleCertificatesApi");
    }

    public final boolean a(z zVar, j.c.a.a.d.a aVar) {
        Parcel a = a();
        c.a(a, zVar);
        c.a(a, aVar);
        Parcel a2 = a(5, a);
        boolean z = a2.readInt() != 0;
        a2.recycle();
        return z;
    }
}
