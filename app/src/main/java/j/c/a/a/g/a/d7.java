package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class d7 extends b {

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ z6 f1950e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public d7(z6 z6Var, p5 p5Var) {
        super(p5Var);
        this.f1950e = z6Var;
    }

    public final void a() {
        z6 z6Var = this.f1950e;
        z6Var.d();
        if (z6Var.z()) {
            z6Var.a().f2052n.a("Inactivity, disconnecting from the service");
            z6Var.B();
        }
    }
}
