package j.c.a.a.f.e;

import java.util.Collections;
import java.util.List;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class w5 extends x5<FieldDescriptorType, Object> {
    public w5(int i2) {
        super(i2, null);
    }

    public final void a() {
        if (!super.f1927e) {
            for (int i2 = 0; i2 < b(); i2++) {
                Map.Entry a = a(i2);
                if (((o3) a.getKey()).d()) {
                    a.setValue(Collections.unmodifiableList((List) a.getValue()));
                }
            }
            for (Map.Entry entry : c()) {
                if (((o3) entry.getKey()).d()) {
                    entry.setValue(Collections.unmodifiableList((List) entry.getValue()));
                }
            }
        }
        super.a();
    }
}
