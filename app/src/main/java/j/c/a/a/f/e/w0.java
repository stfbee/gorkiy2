package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;
import java.util.List;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class w0 extends x3<w0, a> implements g5 {
    public static final w0 zzg;
    public static volatile m5<w0> zzh;
    public f4 zzc;
    public f4 zzd;
    public e4<p0> zze;
    public e4<x0> zzf;

    static {
        w0 w0Var = new w0();
        zzg = w0Var;
        x3.zzd.put(w0.class, w0Var);
    }

    public w0() {
        u4 u4Var = u4.f1909e;
        this.zzc = u4Var;
        this.zzd = u4Var;
        s5<Object> s5Var = s5.f1905e;
        this.zze = s5Var;
        this.zzf = s5Var;
    }

    public static /* synthetic */ void a(w0 w0Var) {
        if (w0Var != null) {
            w0Var.zzc = u4.f1909e;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void b(w0 w0Var) {
        if (w0Var != null) {
            w0Var.zzd = u4.f1909e;
            return;
        }
        throw null;
    }

    public static a p() {
        return (a) zzg.g();
    }

    public final int i() {
        return this.zzc.size();
    }

    public final List<Long> j() {
        return this.zzd;
    }

    public final int m() {
        return this.zze.size();
    }

    public final List<x0> n() {
        return this.zzf;
    }

    public final int o() {
        return this.zzf.size();
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class a extends x3.a<w0, a> implements g5 {
        public a() {
            super(w0.zzg);
        }

        public final a a(Iterable<? extends Long> iterable) {
            i();
            w0 w0Var = (w0) super.c;
            if (!w0Var.zzc.a()) {
                w0Var.zzc = x3.a(w0Var.zzc);
            }
            n2.a(iterable, w0Var.zzc);
            return this;
        }

        public final a b(Iterable<? extends Long> iterable) {
            i();
            w0 w0Var = (w0) super.c;
            if (!w0Var.zzd.a()) {
                w0Var.zzd = x3.a(w0Var.zzd);
            }
            n2.a(iterable, w0Var.zzd);
            return this;
        }

        public final a c(Iterable<? extends p0> iterable) {
            i();
            w0 w0Var = (w0) super.c;
            if (!w0Var.zze.a()) {
                w0Var.zze = x3.a(w0Var.zze);
            }
            n2.a(iterable, w0Var.zze);
            return this;
        }

        public final a d(Iterable<? extends x0> iterable) {
            i();
            w0 w0Var = (w0) super.c;
            if (!w0Var.zzf.a()) {
                w0Var.zzf = x3.a(w0Var.zzf);
            }
            n2.a(iterable, w0Var.zzf);
            return this;
        }

        public final a n() {
            i();
            w0.b((w0) super.c);
            return this;
        }

        public /* synthetic */ a(z0 z0Var) {
            super(w0.zzg);
        }

        public final a a() {
            i();
            w0.a((w0) super.c);
            return this;
        }

        public final a b(int i2) {
            i();
            w0 w0Var = (w0) super.c;
            if (!w0Var.zzf.a()) {
                w0Var.zzf = x3.a(w0Var.zzf);
            }
            w0Var.zzf.remove(i2);
            return this;
        }

        public final a a(int i2) {
            i();
            w0 w0Var = (w0) super.c;
            if (!w0Var.zze.a()) {
                w0Var.zze = x3.a(w0Var.zze);
            }
            w0Var.zze.remove(i2);
            return this;
        }
    }

    public final p0 a(int i2) {
        return this.zze.get(i2);
    }

    public final x0 b(int i2) {
        return this.zzf.get(i2);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new w0();
            case 2:
                return new a(null);
            case 3:
                return new r5(zzg, "\u0001\u0004\u0000\u0000\u0001\u0004\u0004\u0000\u0004\u0000\u0001\u0015\u0002\u0015\u0003\u001b\u0004\u001b", new Object[]{"zzc", "zzd", "zze", p0.class, "zzf", x0.class});
            case 4:
                return zzg;
            case 5:
                m5<w0> m5Var = zzh;
                if (m5Var == null) {
                    synchronized (w0.class) {
                        m5Var = zzh;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzg);
                            zzh = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
