package j.c.a.a.f.e;

import j.c.a.a.f.e.x3;

/* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
public final class v0 extends x3<v0, b> implements g5 {
    public static final v0 zzf;
    public static volatile m5<v0> zzg;
    public int zzc;
    public int zzd = 1;
    public e4<r0> zze = s5.f1905e;

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public enum a implements b4 {
        RADS(1),
        PROVISIONING(2);
        
        public static final a4<a> zzc = new b1();
        public final int zzd;

        /* access modifiers changed from: public */
        a(int i2) {
            this.zzd = i2;
        }

        public static d4 b() {
            return a1.a;
        }

        public final int a() {
            return this.zzd;
        }

        public final String toString() {
            return "<" + a.class.getName() + '@' + Integer.toHexString(System.identityHashCode(this)) + " number=" + this.zzd + " name=" + name() + '>';
        }

        public static a a(int i2) {
            if (i2 == 1) {
                return RADS;
            }
            if (i2 != 2) {
                return null;
            }
            return PROVISIONING;
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement@@17.0.1 */
    public static final class b extends x3.a<v0, b> implements g5 {
        public /* synthetic */ b(z0 z0Var) {
            super(v0.zzf);
        }
    }

    static {
        v0 v0Var = new v0();
        zzf = v0Var;
        x3.zzd.put(v0.class, v0Var);
    }

    public final Object a(int i2, Object obj, Object obj2) {
        switch (z0.a[i2 - 1]) {
            case 1:
                return new v0();
            case 2:
                return new b(null);
            case 3:
                return new r5(zzf, "\u0001\u0002\u0000\u0001\u0001\u0002\u0002\u0000\u0001\u0000\u0001\f\u0000\u0002\u001b", new Object[]{"zzc", "zzd", a.b(), "zze", r0.class});
            case 4:
                return zzf;
            case 5:
                m5<v0> m5Var = zzg;
                if (m5Var == null) {
                    synchronized (v0.class) {
                        m5Var = zzg;
                        if (m5Var == null) {
                            m5Var = new x3.c<>(zzf);
                            zzg = m5Var;
                        }
                    }
                }
                return m5Var;
            case 6:
                return (byte) 1;
            case 7:
                return null;
            default:
                throw new UnsupportedOperationException();
        }
    }
}
