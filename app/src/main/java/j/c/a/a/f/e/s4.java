package j.c.a.a.f.e;

import android.os.Bundle;
import android.os.IBinder;
import android.os.Parcel;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class s4 extends q implements t3 {
    public s4(IBinder iBinder) {
        super(iBinder, "com.google.android.finsky.externalreferrer.IGetInstallReferrerService");
    }

    public final Bundle a(Bundle bundle) {
        Parcel g = g();
        c2.a(g, bundle);
        Parcel a = a(1, g);
        Bundle bundle2 = (Bundle) c2.a(a, Bundle.CREATOR);
        a.recycle();
        return bundle2;
    }
}
