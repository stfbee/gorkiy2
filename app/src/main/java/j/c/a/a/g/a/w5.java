package j.c.a.a.g.a;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.TextUtils;
import android.util.Pair;
import i.b.k.ResourcesFlusher;
import java.net.MalformedURLException;
import java.net.URL;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final /* synthetic */ class w5 implements Runnable {
    public final x5 b;

    public w5(x5 x5Var) {
        this.b = x5Var;
    }

    public final void run() {
        NetworkInfo networkInfo;
        URL url;
        x5 x5Var = this.b;
        x5Var.d();
        if (x5Var.l().z.a()) {
            x5Var.a().f2051m.a("Deferred Deep Link already retrieved. Not fetching again.");
            return;
        }
        long a = x5Var.l().A.a();
        x5Var.l().A.a(a + 1);
        if (a >= 5) {
            x5Var.a().f2047i.a("Permanently failed to retrieve Deferred Deep Link. Reached maximum retries.");
            x5Var.l().z.a(true);
            return;
        }
        r4 r4Var = x5Var.a;
        r4Var.i().d();
        r4.a((n5) r4Var.g());
        g3 t2 = r4Var.t();
        t2.w();
        String str = t2.c;
        Pair<String, Boolean> a2 = r4Var.l().a(str);
        if (!r4Var.g.s().booleanValue() || ((Boolean) a2.second).booleanValue() || TextUtils.isEmpty((CharSequence) a2.first)) {
            r4Var.a().f2051m.a("ADID unavailable to retrieve Deferred Deep Link. Skipping");
            return;
        }
        r6 g = r4Var.g();
        g.o();
        try {
            networkInfo = ((ConnectivityManager) g.a.a.getSystemService("connectivity")).getActiveNetworkInfo();
        } catch (SecurityException unused) {
            networkInfo = null;
        }
        if (!(networkInfo != null && networkInfo.isConnected())) {
            r4Var.a().f2047i.a("Network is not available for Deferred Deep Link request. Skipping");
            return;
        }
        y8 n2 = r4Var.n();
        r4Var.t().a.g.o();
        String str2 = (String) a2.first;
        long a3 = r4Var.l().A.a() - 1;
        if (n2 != null) {
            try {
                ResourcesFlusher.b(str2);
                ResourcesFlusher.b(str);
                String format = String.format("https://www.googleadservices.com/pagead/conversion/app/deeplink?id_type=adid&sdk_version=%s&rdid=%s&bundleid=%s&retry=%s", String.format("v%s.%s", 18079L, Integer.valueOf(n2.v())), str2, str, Long.valueOf(a3));
                if (str.equals(n2.a.g.a("debug.deferred.deeplink", ""))) {
                    format = format.concat("&ddl_test=1");
                }
                url = new URL(format);
            } catch (IllegalArgumentException | MalformedURLException e2) {
                n2.a().f2046f.a("Failed to create BOW URL for Deferred Deep Link. exception", e2.getMessage());
                url = null;
            }
            r6 g2 = r4Var.g();
            q4 q4Var = new q4(r4Var);
            g2.d();
            g2.o();
            ResourcesFlusher.b(url);
            ResourcesFlusher.b(q4Var);
            g2.i().b(new t6(g2, str, url, q4Var));
            return;
        }
        throw null;
    }
}
