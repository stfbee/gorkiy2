package j.c.a.a.f.e;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import android.util.Log;
import android.util.Pair;
import i.b.k.ResourcesFlusher;
import j.c.a.a.g.a.v5;
import java.util.List;
import java.util.concurrent.ExecutorService;

/* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
public class pb {
    public static volatile pb h;

    /* renamed from: i  reason: collision with root package name */
    public static Boolean f1896i;

    /* renamed from: j  reason: collision with root package name */
    public static Boolean f1897j;

    /* renamed from: k  reason: collision with root package name */
    public static boolean f1898k;

    /* renamed from: l  reason: collision with root package name */
    public static Boolean f1899l;
    public final String a;
    public final j.c.a.a.c.n.a b;
    public final ExecutorService c;
    public List<Pair<v5, c>> d;

    /* renamed from: e  reason: collision with root package name */
    public int f1900e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1901f;
    public l8 g;

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
    public abstract class a implements Runnable {
        public final long b;
        public final long c;
        public final boolean d;

        public a(boolean z) {
            if (((j.c.a.a.c.n.b) pb.this.b) != null) {
                this.b = System.currentTimeMillis();
                if (((j.c.a.a.c.n.b) pb.this.b) != null) {
                    this.c = SystemClock.elapsedRealtime();
                    this.d = z;
                    return;
                }
                throw null;
            }
            throw null;
        }

        public abstract void a();

        public void b() {
        }

        public void run() {
            if (pb.this.f1901f) {
                b();
                return;
            }
            try {
                a();
            } catch (Exception e2) {
                pb.this.a(e2, false, this.d);
                b();
            }
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
    public class b implements Application.ActivityLifecycleCallbacks {
        public b() {
        }

        public final void onActivityCreated(Activity activity, Bundle bundle) {
            pb pbVar = pb.this;
            pbVar.c.execute(new t(this, activity, bundle));
        }

        public final void onActivityDestroyed(Activity activity) {
            pb pbVar = pb.this;
            pbVar.c.execute(new y(this, activity));
        }

        public final void onActivityPaused(Activity activity) {
            pb pbVar = pb.this;
            pbVar.c.execute(new u(this, activity));
        }

        public final void onActivityResumed(Activity activity) {
            pb pbVar = pb.this;
            pbVar.c.execute(new v(this, activity));
        }

        public final void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
            m9 m9Var = new m9();
            pb pbVar = pb.this;
            pbVar.c.execute(new w(this, activity, m9Var));
            Bundle b2 = m9Var.b(50);
            if (b2 != null) {
                bundle.putAll(b2);
            }
        }

        public final void onActivityStarted(Activity activity) {
            pb pbVar = pb.this;
            pbVar.c.execute(new s(this, activity));
        }

        public final void onActivityStopped(Activity activity) {
            pb pbVar = pb.this;
            pbVar.c.execute(new x(this, activity));
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-sdk-api@@17.0.1 */
    public static class c extends jb {
        public final v5 a;

        public c(v5 v5Var) {
            this.a = v5Var;
        }

        public final void a(String str, String str2, Bundle bundle, long j2) {
            this.a.onEvent(str, str2, bundle, j2);
        }

        public final int a() {
            return System.identityHashCode(this.a);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x003a  */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004a  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x0054  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public pb(android.content.Context r8, java.lang.String r9, java.lang.String r10, java.lang.String r11, android.os.Bundle r12) {
        /*
            r7 = this;
            r7.<init>()
            if (r9 == 0) goto L_0x000f
            boolean r0 = a(r10, r11)
            if (r0 != 0) goto L_0x000c
            goto L_0x000f
        L_0x000c:
            r7.a = r9
            goto L_0x0013
        L_0x000f:
            java.lang.String r9 = "FA"
            r7.a = r9
        L_0x0013:
            j.c.a.a.c.n.b r9 = j.c.a.a.c.n.b.a
            r7.b = r9
            java.util.concurrent.ThreadPoolExecutor r9 = new java.util.concurrent.ThreadPoolExecutor
            r1 = 0
            r2 = 1
            r3 = 30
            java.util.concurrent.TimeUnit r5 = java.util.concurrent.TimeUnit.SECONDS
            java.util.concurrent.LinkedBlockingQueue r6 = new java.util.concurrent.LinkedBlockingQueue
            r6.<init>()
            r0 = r9
            r0.<init>(r1, r2, r3, r5, r6)
            r7.c = r9
            r9 = 0
            r0 = 1
            j.c.a.a.c.k.e.c.a(r8)     // Catch:{ IllegalStateException -> 0x0037 }
            java.lang.String r1 = j.c.a.a.c.k.e.c.a()     // Catch:{ IllegalStateException -> 0x0037 }
            if (r1 == 0) goto L_0x0037
            r1 = 1
            goto L_0x0038
        L_0x0037:
            r1 = 0
        L_0x0038:
            if (r1 == 0) goto L_0x0047
            java.lang.String r1 = "com.google.firebase.analytics.FirebaseAnalytics"
            java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0041 }
            r1 = 1
            goto L_0x0042
        L_0x0041:
            r1 = 0
        L_0x0042:
            if (r1 == 0) goto L_0x0045
            goto L_0x0047
        L_0x0045:
            r1 = 0
            goto L_0x0048
        L_0x0047:
            r1 = 1
        L_0x0048:
            if (r1 != 0) goto L_0x0054
            r7.f1901f = r0
            java.lang.String r8 = r7.a
            java.lang.String r9 = "Disabling data collection. Found google_app_id in strings.xml but Google Analytics for Firebase is missing. Remove this value or add Google Analytics for Firebase to resume data collection."
            android.util.Log.w(r8, r9)
            return
        L_0x0054:
            boolean r1 = a(r10, r11)
            if (r1 != 0) goto L_0x007a
            if (r10 == 0) goto L_0x0068
            if (r11 == 0) goto L_0x0068
            java.lang.String r8 = r7.a
            java.lang.String r9 = "Deferring to Google Analytics for Firebase for event data collection. https://goo.gl/J1sWQy"
            android.util.Log.v(r8, r9)
            r7.f1901f = r0
            return
        L_0x0068:
            if (r10 != 0) goto L_0x006c
            r1 = 1
            goto L_0x006d
        L_0x006c:
            r1 = 0
        L_0x006d:
            if (r11 != 0) goto L_0x0070
            r9 = 1
        L_0x0070:
            r9 = r9 ^ r1
            if (r9 == 0) goto L_0x007a
            java.lang.String r9 = r7.a
            java.lang.String r0 = "Specified origin or custom app id is null. Both parameters will be ignored."
            android.util.Log.w(r9, r0)
        L_0x007a:
            j.c.a.a.f.e.ob r9 = new j.c.a.a.f.e.ob
            r1 = r9
            r2 = r7
            r3 = r10
            r4 = r11
            r5 = r8
            r6 = r12
            r1.<init>(r2, r3, r4, r5, r6)
            java.util.concurrent.ExecutorService r10 = r7.c
            r10.execute(r9)
            android.content.Context r8 = r8.getApplicationContext()
            android.app.Application r8 = (android.app.Application) r8
            if (r8 != 0) goto L_0x009a
            java.lang.String r8 = r7.a
            java.lang.String r9 = "Unable to register lifecycle notifications. Application null."
            android.util.Log.w(r8, r9)
            return
        L_0x009a:
            j.c.a.a.f.e.pb$b r9 = new j.c.a.a.f.e.pb$b
            r9.<init>()
            r8.registerActivityLifecycleCallbacks(r9)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.pb.<init>(android.content.Context, java.lang.String, java.lang.String, java.lang.String, android.os.Bundle):void");
    }

    public static pb a(Context context, String str, String str2, String str3, Bundle bundle) {
        ResourcesFlusher.b(context);
        if (h == null) {
            synchronized (pb.class) {
                if (h == null) {
                    h = new pb(context, str, str2, str3, bundle);
                }
            }
        }
        return h;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0032 A[SYNTHETIC, Splitter:B:24:0x0032] */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x0040  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static void b(android.content.Context r6) {
        /*
            java.lang.Class<j.c.a.a.f.e.pb> r0 = j.c.a.a.f.e.pb.class
            monitor-enter(r0)
            r1 = 0
            java.lang.Boolean r2 = j.c.a.a.f.e.pb.f1896i     // Catch:{ Exception -> 0x0072 }
            if (r2 == 0) goto L_0x000e
            java.lang.Boolean r2 = j.c.a.a.f.e.pb.f1897j     // Catch:{ Exception -> 0x0072 }
            if (r2 == 0) goto L_0x000e
            monitor-exit(r0)     // Catch:{ all -> 0x0070 }
            return
        L_0x000e:
            java.lang.String r2 = "app_measurement_internal_disable_startup_flags"
            i.b.k.ResourcesFlusher.b(r2)     // Catch:{ Exception -> 0x0072 }
            j.c.a.a.c.o.a r3 = j.c.a.a.c.o.b.b(r6)     // Catch:{ NameNotFoundException -> 0x002f }
            java.lang.String r4 = r6.getPackageName()     // Catch:{ NameNotFoundException -> 0x002f }
            r5 = 128(0x80, float:1.794E-43)
            android.content.pm.ApplicationInfo r3 = r3.a(r4, r5)     // Catch:{ NameNotFoundException -> 0x002f }
            if (r3 == 0) goto L_0x002f
            android.os.Bundle r4 = r3.metaData     // Catch:{ NameNotFoundException -> 0x002f }
            if (r4 != 0) goto L_0x0028
            goto L_0x002f
        L_0x0028:
            android.os.Bundle r3 = r3.metaData     // Catch:{ NameNotFoundException -> 0x002f }
            boolean r2 = r3.getBoolean(r2)     // Catch:{ NameNotFoundException -> 0x002f }
            goto L_0x0030
        L_0x002f:
            r2 = 0
        L_0x0030:
            if (r2 == 0) goto L_0x0040
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0072 }
            j.c.a.a.f.e.pb.f1896i = r6     // Catch:{ Exception -> 0x0072 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)     // Catch:{ Exception -> 0x0072 }
            j.c.a.a.f.e.pb.f1897j = r6     // Catch:{ Exception -> 0x0072 }
            monitor-exit(r0)     // Catch:{ all -> 0x0070 }
            return
        L_0x0040:
            java.lang.String r2 = "com.google.android.gms.measurement.prefs"
            android.content.SharedPreferences r6 = r6.getSharedPreferences(r2, r1)     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = "use_dynamite_api"
            boolean r2 = r6.getBoolean(r2, r1)     // Catch:{ Exception -> 0x0072 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0072 }
            j.c.a.a.f.e.pb.f1896i = r2     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = "allow_remote_dynamite"
            boolean r2 = r6.getBoolean(r2, r1)     // Catch:{ Exception -> 0x0072 }
            java.lang.Boolean r2 = java.lang.Boolean.valueOf(r2)     // Catch:{ Exception -> 0x0072 }
            j.c.a.a.f.e.pb.f1897j = r2     // Catch:{ Exception -> 0x0072 }
            android.content.SharedPreferences$Editor r6 = r6.edit()     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = "use_dynamite_api"
            r6.remove(r2)     // Catch:{ Exception -> 0x0072 }
            java.lang.String r2 = "allow_remote_dynamite"
            r6.remove(r2)     // Catch:{ Exception -> 0x0072 }
            r6.apply()     // Catch:{ Exception -> 0x0072 }
            goto L_0x0086
        L_0x0070:
            r6 = move-exception
            goto L_0x0088
        L_0x0072:
            r6 = move-exception
            java.lang.String r2 = "FA"
            java.lang.String r3 = "Exception reading flag from SharedPreferences."
            android.util.Log.e(r2, r3, r6)     // Catch:{ all -> 0x0070 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x0070 }
            j.c.a.a.f.e.pb.f1896i = r6     // Catch:{ all -> 0x0070 }
            java.lang.Boolean r6 = java.lang.Boolean.valueOf(r1)     // Catch:{ all -> 0x0070 }
            j.c.a.a.f.e.pb.f1897j = r6     // Catch:{ all -> 0x0070 }
        L_0x0086:
            monitor-exit(r0)     // Catch:{ all -> 0x0070 }
            return
        L_0x0088:
            monitor-exit(r0)     // Catch:{ all -> 0x0070 }
            throw r6
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.a.f.e.pb.b(android.content.Context):void");
    }

    public final void a(Exception exc, boolean z, boolean z2) {
        this.f1901f |= z;
        if (z) {
            Log.w(this.a, "Data collection startup failed. No data will be collected.", exc);
            return;
        }
        if (z2) {
            this.c.execute(new l(this, "Error with data collection. Data lost.", exc));
        }
        Log.w(this.a, "Error with data collection. Data lost.", exc);
    }

    public static boolean a(String str, String str2) {
        boolean z;
        if (!(str2 == null || str == null)) {
            try {
                Class.forName("com.google.firebase.analytics.FirebaseAnalytics");
                z = true;
            } catch (ClassNotFoundException unused) {
                z = false;
            }
            if (!z) {
                return true;
            }
        }
        return false;
    }

    public static boolean a(Context context) {
        Class<String> cls = String.class;
        b(context);
        synchronized (pb.class) {
            if (!f1898k) {
                try {
                    String str = (String) Class.forName("android.os.SystemProperties").getMethod("get", cls, cls).invoke(null, "measurement.dynamite.enabled", "");
                    if ("true".equals(str)) {
                        f1899l = true;
                    } else if ("false".equals(str)) {
                        f1899l = false;
                    } else {
                        f1899l = null;
                    }
                    f1898k = true;
                } catch (Exception e2) {
                    try {
                        Log.e("FA", "Unable to call SystemProperties.get()", e2);
                        f1899l = null;
                    } finally {
                        f1898k = true;
                    }
                }
            }
        }
        Boolean bool = f1899l;
        if (bool == null) {
            bool = f1896i;
        }
        return bool.booleanValue();
    }
}
