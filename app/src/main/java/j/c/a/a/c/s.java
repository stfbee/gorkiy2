package j.c.a.a.c;

import java.util.concurrent.Callable;

public final /* synthetic */ class s implements Callable {
    public final boolean b;
    public final String c;
    public final t d;

    public s(boolean z, String str, t tVar) {
        this.b = z;
        this.c = str;
        this.d = tVar;
    }

    public final Object call() {
        boolean z = this.b;
        String str = this.c;
        t tVar = this.d;
        boolean z2 = true;
        if (z || !r.b(str, tVar, true, false).a) {
            z2 = false;
        }
        return b0.a(str, tVar, z, z2);
    }
}
