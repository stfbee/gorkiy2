package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzek;
import com.google.android.gms.internal.measurement.zzfn;
import java.util.Arrays;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class i6 {

    /* renamed from: f  reason: collision with root package name */
    public static final i6 f1866f = new i6(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1867e;

    public i6() {
        this(0, new int[8], new Object[8], true);
    }

    public static i6 b() {
        return new i6(0, new int[8], new Object[8], true);
    }

    public final void a(c7 c7Var) {
        if (this.a != 0) {
            if (((g3) c7Var) != null) {
                for (int i2 = 0; i2 < this.a; i2++) {
                    a(this.b[i2], this.c[i2], c7Var);
                }
                return;
            }
            throw null;
        }
    }

    public final boolean equals(Object obj) {
        boolean z;
        boolean z2;
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof i6)) {
            return false;
        }
        i6 i6Var = (i6) obj;
        int i2 = this.a;
        if (i2 == i6Var.a) {
            int[] iArr = this.b;
            int[] iArr2 = i6Var.b;
            int i3 = 0;
            while (true) {
                if (i3 >= i2) {
                    z = true;
                    break;
                } else if (iArr[i3] != iArr2[i3]) {
                    z = false;
                    break;
                } else {
                    i3++;
                }
            }
            if (z) {
                Object[] objArr = this.c;
                Object[] objArr2 = i6Var.c;
                int i4 = this.a;
                int i5 = 0;
                while (true) {
                    if (i5 >= i4) {
                        z2 = true;
                        break;
                    } else if (!objArr[i5].equals(objArr2[i5])) {
                        z2 = false;
                        break;
                    } else {
                        i5++;
                    }
                }
                return z2;
            }
        }
    }

    public final int hashCode() {
        int i2 = this.a;
        int i3 = (i2 + 527) * 31;
        int[] iArr = this.b;
        int i4 = 17;
        int i5 = 17;
        for (int i6 = 0; i6 < i2; i6++) {
            i5 = (i5 * 31) + iArr[i6];
        }
        int i7 = (i3 + i5) * 31;
        Object[] objArr = this.c;
        int i8 = this.a;
        for (int i9 = 0; i9 < i8; i9++) {
            i4 = (i4 * 31) + objArr[i9].hashCode();
        }
        return i7 + i4;
    }

    public i6(int i2, int[] iArr, Object[] objArr, boolean z) {
        this.d = -1;
        this.a = i2;
        this.b = iArr;
        this.c = objArr;
        this.f1867e = z;
    }

    public static void a(int i2, Object obj, c7 c7Var) {
        int i3 = i2 >>> 3;
        int i4 = i2 & 7;
        if (i4 == 0) {
            ((g3) c7Var).a(i3, ((Long) obj).longValue());
        } else if (i4 == 1) {
            ((g3) c7Var).d(i3, ((Long) obj).longValue());
        } else if (i4 == 2) {
            ((g3) c7Var).a(i3, (w2) obj);
        } else if (i4 == 3) {
            g3 g3Var = (g3) c7Var;
            if (g3Var != null) {
                int i5 = i3 << 3;
                ((zzek.a) g3Var.a).b(i5 | 3);
                ((i6) obj).a(c7Var);
                ((zzek.a) g3Var.a).b(i5 | 4);
                return;
            }
            throw null;
        } else if (i4 == 5) {
            ((g3) c7Var).b(i3, ((Integer) obj).intValue());
        } else {
            throw new RuntimeException(zzfn.d());
        }
    }

    public final int a() {
        int i2;
        int i3 = this.d;
        if (i3 != -1) {
            return i3;
        }
        int i4 = 0;
        for (int i5 = 0; i5 < this.a; i5++) {
            int i6 = this.b[i5];
            int i7 = i6 >>> 3;
            int i8 = i6 & 7;
            if (i8 == 0) {
                i2 = zzek.b(i7, ((Long) this.c[i5]).longValue());
            } else if (i8 == 1) {
                ((Long) this.c[i5]).longValue();
                i2 = zzek.i(i7);
            } else if (i8 == 2) {
                i2 = zzek.a(i7, (w2) this.c[i5]);
            } else if (i8 == 3) {
                i4 = ((i6) this.c[i5]).a() + (zzek.f(i7) << 1) + i4;
            } else if (i8 == 5) {
                ((Integer) this.c[i5]).intValue();
                i2 = zzek.l(i7);
            } else {
                throw new IllegalStateException(zzfn.d());
            }
            i4 = i2 + i4;
        }
        this.d = i4;
        return i4;
    }

    public final void a(int i2, Object obj) {
        if (this.f1867e) {
            int i3 = this.a;
            if (i3 == this.b.length) {
                int i4 = this.a + (i3 < 4 ? 8 : i3 >> 1);
                this.b = Arrays.copyOf(this.b, i4);
                this.c = Arrays.copyOf(this.c, i4);
            }
            int[] iArr = this.b;
            int i5 = this.a;
            iArr[i5] = i2;
            this.c[i5] = obj;
            this.a = i5 + 1;
            return;
        }
        throw new UnsupportedOperationException();
    }
}
