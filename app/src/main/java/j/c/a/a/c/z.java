package j.c.a.a.c;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;
import android.os.RemoteException;
import android.util.Log;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.l.p.a;
import j.c.a.a.d.b;
import javax.annotation.Nullable;

public final class z extends a {
    public static final Parcelable.Creator<z> CREATOR = new a0();
    public final String b;
    @Nullable
    public final t c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f1829e;

    public z(String str, @Nullable IBinder iBinder, boolean z, boolean z2) {
        byte[] bArr;
        this.b = str;
        u uVar = null;
        if (iBinder != null) {
            try {
                j.c.a.a.d.a b2 = t.a(iBinder).b();
                if (b2 == null) {
                    bArr = null;
                } else {
                    bArr = (byte[]) b.a(b2);
                }
                if (bArr != null) {
                    uVar = new u(bArr);
                } else {
                    Log.e("GoogleCertificatesQuery", "Could not unwrap certificate");
                }
            } catch (RemoteException e2) {
                Log.e("GoogleCertificatesQuery", "Could not unwrap certificate", e2);
            }
        }
        this.c = uVar;
        this.d = z;
        this.f1829e = z2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
     arg types: [android.os.Parcel, int, java.lang.String, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void
     arg types: [android.os.Parcel, int, j.c.a.a.c.t, int]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int, boolean, int):int
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.util.AttributeSet, android.content.res.Resources$Theme):android.content.res.ColorStateList
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, android.content.res.Resources$Theme, android.util.AttributeSet, int[]):android.content.res.TypedArray
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, org.xmlpull.v1.XmlPullParser, java.lang.String, int):java.lang.String
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Bundle, boolean):void
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, java.lang.String, boolean):void
      i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect, android.graphics.Rect):boolean
      i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int, boolean):boolean
      i.b.k.ResourcesFlusher.a(i.f.a.h.d, i.f.a.h.f, java.util.List<i.f.a.h.f>, boolean):boolean
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.IBinder, boolean):void */
    public final void writeToParcel(Parcel parcel, int i2) {
        int a = ResourcesFlusher.a(parcel);
        ResourcesFlusher.a(parcel, 1, this.b, false);
        t tVar = this.c;
        if (tVar == null) {
            Log.w("GoogleCertificatesQuery", "certificate binder is null");
            tVar = null;
        } else if (tVar == null) {
            throw null;
        }
        ResourcesFlusher.a(parcel, 2, (IBinder) tVar, false);
        ResourcesFlusher.a(parcel, 3, this.d);
        ResourcesFlusher.a(parcel, 4, this.f1829e);
        ResourcesFlusher.k(parcel, a);
    }

    public z(String str, @Nullable t tVar, boolean z, boolean z2) {
        this.b = str;
        this.c = tVar;
        this.d = z;
        this.f1829e = z2;
    }
}
