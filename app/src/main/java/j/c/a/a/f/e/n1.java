package j.c.a.a.f.e;

import android.database.ContentObserver;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class n1 extends ContentObserver {
    public n1() {
        super(null);
    }

    public final void onChange(boolean z) {
        p1.f1894i.incrementAndGet();
    }
}
