package j.c.a.a.g.a;

import android.os.Bundle;
import android.os.RemoteException;
import j.c.a.a.f.e.fb;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class s7 implements Runnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ String c;
    public final /* synthetic */ boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ d9 f2101e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ fb f2102f;
    public final /* synthetic */ z6 g;

    public s7(z6 z6Var, String str, String str2, boolean z, d9 d9Var, fb fbVar) {
        this.g = z6Var;
        this.b = str;
        this.c = str2;
        this.d = z;
        this.f2101e = d9Var;
        this.f2102f = fbVar;
    }

    public final void run() {
        Bundle bundle = new Bundle();
        try {
            f3 f3Var = this.g.d;
            if (f3Var == null) {
                this.g.a().f2046f.a("Failed to get user properties", this.b, this.c);
                return;
            }
            bundle = y8.a(f3Var.a(this.b, this.c, this.d, this.f2101e));
            this.g.D();
            this.g.k().a(this.f2102f, bundle);
        } catch (RemoteException e2) {
            this.g.a().f2046f.a("Failed to get user properties", this.b, e2);
        } finally {
            this.g.k().a(this.f2102f, bundle);
        }
    }
}
