package j.c.a.a.c;

import java.util.concurrent.Callable;

public final class d0 extends b0 {

    /* renamed from: e  reason: collision with root package name */
    public final Callable<String> f1776e;

    public /* synthetic */ d0(Callable callable, c0 c0Var) {
        super(false, null, null);
        this.f1776e = callable;
    }

    public final String a() {
        try {
            return this.f1776e.call();
        } catch (Exception e2) {
            throw new RuntimeException(e2);
        }
    }
}
