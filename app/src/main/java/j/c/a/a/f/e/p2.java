package j.c.a.a.f.e;

import com.google.android.gms.internal.measurement.zzfn;
import j.c.a.a.f.e.n2;
import j.c.a.a.f.e.p2;
import j.c.a.a.f.e.x3;
import java.io.IOException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public abstract class p2<MessageType extends n2<MessageType, BuilderType>, BuilderType extends p2<MessageType, BuilderType>> implements e5 {
    public final BuilderType a(byte[] bArr, j3 j3Var) {
        int length = bArr.length;
        BuilderType buildertype = (x3.a) this;
        buildertype.i();
        try {
            o5.c.a((Object) buildertype.c).a(buildertype.c, bArr, 0, length + 0, new s2(j3Var));
            return buildertype;
        } catch (zzfn e2) {
            throw e2;
        } catch (IndexOutOfBoundsException unused) {
            throw zzfn.a();
        } catch (IOException e3) {
            throw new RuntimeException("Reading from byte array should not throw IOException.", e3);
        }
    }
}
