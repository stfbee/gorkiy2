package j.c.a.a.c.l;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import i.b.k.ResourcesFlusher;
import java.util.Arrays;

public abstract class h {
    public static final Object a = new Object();
    public static h b;

    public static final class a {
        public final String a;
        public final String b;
        public final ComponentName c = null;
        public final int d;

        public a(String str, String str2, int i2) {
            ResourcesFlusher.b(str);
            this.a = str;
            ResourcesFlusher.b(str2);
            this.b = str2;
            this.d = i2;
        }

        public final Intent a() {
            if (this.a != null) {
                return new Intent(this.a).setPackage(this.b);
            }
            return new Intent().setComponent(this.c);
        }

        public final boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return ResourcesFlusher.c(this.a, aVar.a) && ResourcesFlusher.c(this.b, aVar.b) && ResourcesFlusher.c(this.c, aVar.c) && this.d == aVar.d;
        }

        public final int hashCode() {
            return Arrays.hashCode(new Object[]{this.a, this.b, this.c, Integer.valueOf(this.d)});
        }

        public final String toString() {
            String str = this.a;
            return str == null ? this.c.flattenToString() : str;
        }
    }

    public static h a(Context context) {
        synchronized (a) {
            if (b == null) {
                b = new x(context.getApplicationContext());
            }
        }
        return b;
    }

    public abstract boolean a(a aVar, ServiceConnection serviceConnection, String str);

    public abstract void b(a aVar, ServiceConnection serviceConnection, String str);
}
