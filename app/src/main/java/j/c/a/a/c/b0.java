package j.c.a.a.c;

import j.c.a.a.c.n.d;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;
import javax.annotation.CheckReturnValue;
import javax.annotation.Nullable;

@CheckReturnValue
public class b0 {
    public static final b0 d = new b0(true, null, null);
    public final boolean a;
    public final String b;
    public final Throwable c;

    public b0(boolean z, @Nullable String str, @Nullable Throwable th) {
        this.a = z;
        this.b = str;
        this.c = th;
    }

    public static b0 a(Callable<String> callable) {
        return new d0(callable, null);
    }

    public static b0 a(String str) {
        return new b0(false, str, null);
    }

    @Nullable
    public String a() {
        return this.b;
    }

    public static String a(String str, t tVar, boolean z, boolean z2) {
        MessageDigest messageDigest;
        Object[] objArr = new Object[5];
        objArr[0] = z2 ? "debug cert rejected" : "not whitelisted";
        objArr[1] = str;
        int i2 = 0;
        while (true) {
            if (i2 >= 2) {
                messageDigest = null;
                break;
            }
            try {
                messageDigest = MessageDigest.getInstance("SHA-1");
                if (messageDigest != null) {
                    break;
                }
                i2++;
            } catch (NoSuchAlgorithmException unused) {
            }
        }
        byte[] digest = messageDigest.digest(tVar.g());
        char[] cArr = new char[(digest.length << 1)];
        int i3 = 0;
        for (byte b2 : digest) {
            byte b3 = b2 & 255;
            int i4 = i3 + 1;
            char[] cArr2 = d.a;
            cArr[i3] = cArr2[b3 >>> 4];
            i3 = i4 + 1;
            cArr[i4] = cArr2[b3 & 15];
        }
        objArr[2] = new String(cArr);
        objArr[3] = Boolean.valueOf(z);
        objArr[4] = "12451009.false";
        return String.format("%s: pkg=%s, sha1=%s, atk=%s, ver=%s", objArr);
    }
}
