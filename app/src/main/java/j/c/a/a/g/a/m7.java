package j.c.a.a.g.a;

import android.os.RemoteException;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class m7 implements Runnable {
    public final /* synthetic */ d9 b;
    public final /* synthetic */ z6 c;

    public m7(z6 z6Var, d9 d9Var) {
        this.c = z6Var;
        this.b = d9Var;
    }

    public final void run() {
        z6 z6Var = this.c;
        f3 f3Var = z6Var.d;
        if (f3Var == null) {
            z6Var.a().f2046f.a("Failed to send measurementEnabled to service");
            return;
        }
        try {
            f3Var.c(this.b);
            this.c.D();
        } catch (RemoteException e2) {
            this.c.a().f2046f.a("Failed to send measurementEnabled to the service", e2);
        }
    }
}
