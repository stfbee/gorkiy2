package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public enum u3 {
    SCALAR(false),
    VECTOR(true),
    PACKED_VECTOR(true),
    MAP(false);
    
    public final boolean zze;

    /* access modifiers changed from: public */
    u3(boolean z) {
        this.zze = z;
    }
}
