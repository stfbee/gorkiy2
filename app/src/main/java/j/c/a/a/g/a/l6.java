package j.c.a.a.g.a;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class l6 implements Runnable {
    public final /* synthetic */ boolean b;
    public final /* synthetic */ x5 c;

    public l6(x5 x5Var, boolean z) {
        this.c = x5Var;
        this.b = z;
    }

    public final void run() {
        x5 x5Var = this.c;
        boolean z = this.b;
        x5Var.d();
        x5Var.b();
        x5Var.w();
        x5Var.a().f2051m.a("Setting app measurement enabled (FE)", Boolean.valueOf(z));
        x5Var.l().a(z);
        x5Var.C();
    }
}
