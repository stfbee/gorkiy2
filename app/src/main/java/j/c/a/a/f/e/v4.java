package j.c.a.a.f.e;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class v4 implements c5 {
    public c5[] a;

    public v4(c5... c5VarArr) {
        this.a = c5VarArr;
    }

    public final d5 a(Class<?> cls) {
        for (c5 c5Var : this.a) {
            if (c5Var.b(cls)) {
                return c5Var.a(cls);
            }
        }
        String name = cls.getName();
        throw new UnsupportedOperationException(name.length() != 0 ? "No factory is available for message type: ".concat(name) : new String("No factory is available for message type: "));
    }

    public final boolean b(Class<?> cls) {
        for (c5 b : this.a) {
            if (b.b(cls)) {
                return true;
            }
        }
        return false;
    }
}
