package j.c.a.a.g.a;

import android.app.Activity;
import android.os.Bundle;
import android.os.SystemClock;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.c.n.b;
import java.util.Map;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class y6 extends e5 {
    public w6 c;
    public volatile w6 d;

    /* renamed from: e  reason: collision with root package name */
    public w6 f2136e;

    /* renamed from: f  reason: collision with root package name */
    public final Map<Activity, w6> f2137f = new ArrayMap();
    public String g;

    public y6(r4 r4Var) {
        super(r4Var);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void
     arg types: [android.app.Activity, j.c.a.a.g.a.w6, int]
     candidates:
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.w6, android.os.Bundle, boolean):void
      j.c.a.a.g.a.y6.a(j.c.a.a.g.a.y6, j.c.a.a.g.a.w6, boolean):void
      j.c.a.a.g.a.y6.a(android.app.Activity, java.lang.String, java.lang.String):void
      j.c.a.a.g.a.y6.a(android.app.Activity, j.c.a.a.g.a.w6, boolean):void */
    public final void a(Activity activity, String str, String str2) {
        if (this.d == null) {
            a().f2049k.a("setCurrentScreen cannot be called while no activity active");
        } else if (this.f2137f.get(activity) == null) {
            a().f2049k.a("setCurrentScreen must be called with an activity in the activity lifecycle");
        } else {
            if (str2 == null) {
                str2 = a(activity.getClass().getCanonicalName());
            }
            boolean equals = this.d.b.equals(str2);
            boolean d2 = y8.d(this.d.a, str);
            if (equals && d2) {
                a().f2049k.a("setCurrentScreen cannot be called with the same class and name");
            } else if (str != null && (str.length() <= 0 || str.length() > 100)) {
                a().f2049k.a("Invalid screen name length in setCurrentScreen. Length", Integer.valueOf(str.length()));
            } else if (str2 == null || (str2.length() > 0 && str2.length() <= 100)) {
                a().f2052n.a("Setting current screen to name, class", str == null ? "null" : str, str2);
                w6 w6Var = new w6(str, str2, k().t());
                this.f2137f.put(activity, w6Var);
                a(activity, w6Var, true);
            } else {
                a().f2049k.a("Invalid class name length in setCurrentScreen. Length", Integer.valueOf(str2.length()));
            }
        }
    }

    public final boolean y() {
        return false;
    }

    public final w6 z() {
        w();
        d();
        return this.c;
    }

    public final void a(Activity activity, w6 w6Var, boolean z) {
        w6 w6Var2 = this.d == null ? this.f2136e : this.d;
        if (w6Var.b == null) {
            w6Var = new w6(w6Var.a, a(activity.getClass().getCanonicalName()), w6Var.c);
        }
        this.f2136e = this.d;
        this.d = w6Var;
        l4 i2 = i();
        x6 x6Var = new x6(this, z, w6Var2, w6Var);
        i2.o();
        ResourcesFlusher.b(x6Var);
        i2.a((p4<?>) new p4(i2, x6Var, "Task exception on worker thread"));
    }

    public static /* synthetic */ void a(y6 y6Var, w6 w6Var, boolean z) {
        a o2 = y6Var.o();
        if (((b) y6Var.a.f2092n) != null) {
            o2.a(SystemClock.elapsedRealtime());
            if (y6Var.u().a(w6Var.d, z)) {
                w6Var.d = false;
                return;
            }
            return;
        }
        throw null;
    }

    public static void a(w6 w6Var, Bundle bundle, boolean z) {
        if (bundle != null && w6Var != null && (!bundle.containsKey("_sc") || z)) {
            String str = w6Var.a;
            if (str != null) {
                bundle.putString("_sn", str);
            } else {
                bundle.remove("_sn");
            }
            bundle.putString("_sc", w6Var.b);
            bundle.putLong("_si", w6Var.c);
        } else if (bundle != null && w6Var == null && z) {
            bundle.remove("_sn");
            bundle.remove("_sc");
            bundle.remove("_si");
        }
    }

    public final void a(String str, w6 w6Var) {
        d();
        synchronized (this) {
            if (this.g == null || this.g.equals(str) || w6Var != null) {
                this.g = str;
            }
        }
    }

    public static String a(String str) {
        String[] split = str.split("\\.");
        String str2 = split.length > 0 ? split[split.length - 1] : "";
        return str2.length() > 100 ? str2.substring(0, 100) : str2;
    }

    public final w6 a(Activity activity) {
        ResourcesFlusher.b(activity);
        w6 w6Var = this.f2137f.get(activity);
        if (w6Var != null) {
            return w6Var;
        }
        w6 w6Var2 = new w6(null, a(activity.getClass().getCanonicalName()), k().t());
        this.f2137f.put(activity, w6Var2);
        return w6Var2;
    }

    public final void a(Activity activity, Bundle bundle) {
        Bundle bundle2;
        if (bundle != null && (bundle2 = bundle.getBundle("com.google.app_measurement.screen_service")) != null) {
            this.f2137f.put(activity, new w6(bundle2.getString(DefaultAppMeasurementEventListenerRegistrar.NAME), bundle2.getString("referrer_name"), bundle2.getLong("id")));
        }
    }
}
