package j.c.a.a.f.e;

import java.util.AbstractList;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.RandomAccess;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class k6 extends AbstractList<String> implements m4, RandomAccess {
    public final m4 b;

    public k6(m4 m4Var) {
        this.b = m4Var;
    }

    public final void a(w2 w2Var) {
        throw new UnsupportedOperationException();
    }

    public final Object b(int i2) {
        return this.b.b(i2);
    }

    public final /* synthetic */ Object get(int i2) {
        return (String) this.b.get(i2);
    }

    public final Iterator<String> iterator() {
        return new n6(this);
    }

    public final m4 j() {
        return this;
    }

    public final ListIterator<String> listIterator(int i2) {
        return new j6(this, i2);
    }

    public final int size() {
        return this.b.size();
    }

    public final List<?> b() {
        return this.b.b();
    }
}
