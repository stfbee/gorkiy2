package j.c.a.a.i;

import com.google.android.gms.tasks.RuntimeExecutionException;
import java.util.concurrent.CancellationException;

public final class u implements Runnable {
    public final /* synthetic */ e b;
    public final /* synthetic */ t c;

    public u(t tVar, e eVar) {
        this.c = tVar;
        this.b = eVar;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.t]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.t]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult> */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult>
     arg types: [java.util.concurrent.Executor, j.c.a.a.i.t]
     candidates:
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.c):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.d):j.c.a.a.i.e<TResult>
      j.c.a.a.i.e.a(java.util.concurrent.Executor, j.c.a.a.i.b):j.c.a.a.i.e<TResult> */
    public final void run() {
        try {
            e a = this.c.b.a(this.b.b());
            if (a == null) {
                t tVar = this.c;
                tVar.c.a((Exception) new NullPointerException("Continuation returned null"));
                return;
            }
            a.a(g.a, (d) this.c);
            a.a(g.a, (c) this.c);
            a.a(g.a, (b) this.c);
        } catch (RuntimeExecutionException e2) {
            if (e2.getCause() instanceof Exception) {
                this.c.c.a((Exception) e2.getCause());
                return;
            }
            this.c.c.a((Exception) e2);
        } catch (CancellationException unused) {
            this.c.c.e();
        } catch (Exception e3) {
            this.c.c.a(e3);
        }
    }
}
