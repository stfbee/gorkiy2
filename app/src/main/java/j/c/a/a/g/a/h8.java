package j.c.a.a.g.a;

import android.os.SystemClock;
import j.c.a.a.c.n.b;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class h8 extends b {

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ f8 f2006e;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h8(f8 f8Var, p5 p5Var) {
        super(p5Var);
        this.f2006e = f8Var;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.g.a.f8.a(boolean, boolean):boolean
     arg types: [int, int]
     candidates:
      j.c.a.a.g.a.f8.a(long, boolean):void
      j.c.a.a.g.a.f8.a(boolean, boolean):boolean */
    public final void a() {
        f8 f8Var = this.f2006e;
        f8Var.d();
        f8Var.a(false, false);
        a o2 = f8Var.o();
        if (((b) f8Var.a.f2092n) != null) {
            o2.a(SystemClock.elapsedRealtime());
            return;
        }
        throw null;
    }
}
