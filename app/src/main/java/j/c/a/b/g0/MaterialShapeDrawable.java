package j.c.a.b.g0;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Matrix;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.Log;
import i.h.f.ColorUtils;
import i.h.f.j.TintAwareDrawable;
import j.c.a.a.c.n.c;
import j.c.a.b.f0.ShadowRenderer;
import j.c.a.b.g0.ShapeAppearancePathProvider;
import j.c.a.b.g0.ShapePath;
import j.c.a.b.y.ElevationOverlayProvider;
import java.util.BitSet;
import java.util.Objects;

public class MaterialShapeDrawable extends Drawable implements TintAwareDrawable, Shapeable {
    public static final String x = MaterialShapeDrawable.class.getSimpleName();
    public static final Paint y = new Paint(1);
    public b b;
    public final ShapePath.f[] c;
    public final ShapePath.f[] d;

    /* renamed from: e  reason: collision with root package name */
    public final BitSet f2214e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f2215f;
    public final Matrix g;
    public final Path h;

    /* renamed from: i  reason: collision with root package name */
    public final Path f2216i;

    /* renamed from: j  reason: collision with root package name */
    public final RectF f2217j;

    /* renamed from: k  reason: collision with root package name */
    public final RectF f2218k;

    /* renamed from: l  reason: collision with root package name */
    public final Region f2219l;

    /* renamed from: m  reason: collision with root package name */
    public final Region f2220m;

    /* renamed from: n  reason: collision with root package name */
    public ShapeAppearanceModel f2221n;

    /* renamed from: o  reason: collision with root package name */
    public final Paint f2222o;

    /* renamed from: p  reason: collision with root package name */
    public final Paint f2223p;

    /* renamed from: q  reason: collision with root package name */
    public final ShadowRenderer f2224q;

    /* renamed from: r  reason: collision with root package name */
    public final ShapeAppearancePathProvider.a f2225r;

    /* renamed from: s  reason: collision with root package name */
    public final ShapeAppearancePathProvider f2226s;

    /* renamed from: t  reason: collision with root package name */
    public PorterDuffColorFilter f2227t;
    public PorterDuffColorFilter u;
    public final RectF v;
    public boolean w;

    public class a implements ShapeAppearancePathProvider.a {
        public a() {
        }
    }

    public MaterialShapeDrawable() {
        this(new ShapeAppearanceModel());
    }

    public static MaterialShapeDrawable a(Context context, float f2) {
        int a2 = c.a(context, j.c.a.b.b.colorSurface, MaterialShapeDrawable.class.getSimpleName());
        MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable();
        materialShapeDrawable.b.b = new ElevationOverlayProvider(context);
        materialShapeDrawable.j();
        materialShapeDrawable.a(ColorStateList.valueOf(a2));
        b bVar = materialShapeDrawable.b;
        if (bVar.f2236o != f2) {
            bVar.f2236o = f2;
            materialShapeDrawable.j();
        }
        return materialShapeDrawable;
    }

    public void b(ColorStateList colorStateList) {
        b bVar = this.b;
        if (bVar.f2228e != colorStateList) {
            bVar.f2228e = colorStateList;
            onStateChange(getState());
        }
    }

    public int c() {
        b bVar = this.b;
        return (int) (Math.sin(Math.toRadians((double) bVar.f2241t)) * ((double) bVar.f2240s));
    }

    public int d() {
        b bVar = this.b;
        return (int) (Math.cos(Math.toRadians((double) bVar.f2241t)) * ((double) bVar.f2240s));
    }

    /* JADX WARNING: Code restructure failed: missing block: B:31:0x00ed, code lost:
        if ((!h() && !r13.h.isConvex() && android.os.Build.VERSION.SDK_INT < 29) != false) goto L_0x00ef;
     */
    /* JADX WARNING: Removed duplicated region for block: B:35:0x00f6  */
    /* JADX WARNING: Removed duplicated region for block: B:48:0x019a  */
    /* JADX WARNING: Removed duplicated region for block: B:51:0x01b1  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void draw(android.graphics.Canvas r14) {
        /*
            r13 = this;
            android.graphics.Paint r0 = r13.f2222o
            android.graphics.PorterDuffColorFilter r1 = r13.f2227t
            r0.setColorFilter(r1)
            android.graphics.Paint r0 = r13.f2222o
            int r0 = r0.getAlpha()
            android.graphics.Paint r1 = r13.f2222o
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r13.b
            int r2 = r2.f2234m
            int r3 = r2 >>> 7
            int r2 = r2 + r3
            int r2 = r2 * r0
            int r2 = r2 >>> 8
            r1.setAlpha(r2)
            android.graphics.Paint r1 = r13.f2223p
            android.graphics.PorterDuffColorFilter r2 = r13.u
            r1.setColorFilter(r2)
            android.graphics.Paint r1 = r13.f2223p
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r13.b
            float r2 = r2.f2233l
            r1.setStrokeWidth(r2)
            android.graphics.Paint r1 = r13.f2223p
            int r1 = r1.getAlpha()
            android.graphics.Paint r2 = r13.f2223p
            j.c.a.b.g0.MaterialShapeDrawable$b r3 = r13.b
            int r3 = r3.f2234m
            int r4 = r3 >>> 7
            int r3 = r3 + r4
            int r3 = r3 * r1
            int r3 = r3 >>> 8
            r2.setAlpha(r3)
            boolean r2 = r13.f2215f
            r3 = 0
            r4 = 0
            if (r2 == 0) goto L_0x00c8
            float r2 = r13.e()
            float r2 = -r2
            j.c.a.b.g0.MaterialShapeDrawable$b r5 = r13.b
            j.c.a.b.g0.ShapeAppearanceModel r5 = r5.a
            if (r5 == 0) goto L_0x00c7
            j.c.a.b.g0.ShapeAppearanceModel$b r6 = new j.c.a.b.g0.ShapeAppearanceModel$b
            r6.<init>(r5)
            j.c.a.b.g0.CornerSize r7 = r5.f2242e
            boolean r8 = r7 instanceof j.c.a.b.g0.RelativeCornerSize
            if (r8 == 0) goto L_0x0060
            goto L_0x0066
        L_0x0060:
            j.c.a.b.g0.AdjustedCornerSize r8 = new j.c.a.b.g0.AdjustedCornerSize
            r8.<init>(r2, r7)
            r7 = r8
        L_0x0066:
            r6.f2248e = r7
            j.c.a.b.g0.CornerSize r7 = r5.f2243f
            boolean r8 = r7 instanceof j.c.a.b.g0.RelativeCornerSize
            if (r8 == 0) goto L_0x006f
            goto L_0x0075
        L_0x006f:
            j.c.a.b.g0.AdjustedCornerSize r8 = new j.c.a.b.g0.AdjustedCornerSize
            r8.<init>(r2, r7)
            r7 = r8
        L_0x0075:
            r6.f2249f = r7
            j.c.a.b.g0.CornerSize r7 = r5.h
            boolean r8 = r7 instanceof j.c.a.b.g0.RelativeCornerSize
            if (r8 == 0) goto L_0x007e
            goto L_0x0084
        L_0x007e:
            j.c.a.b.g0.AdjustedCornerSize r8 = new j.c.a.b.g0.AdjustedCornerSize
            r8.<init>(r2, r7)
            r7 = r8
        L_0x0084:
            r6.h = r7
            j.c.a.b.g0.CornerSize r5 = r5.g
            boolean r7 = r5 instanceof j.c.a.b.g0.RelativeCornerSize
            if (r7 == 0) goto L_0x008d
            goto L_0x0093
        L_0x008d:
            j.c.a.b.g0.AdjustedCornerSize r7 = new j.c.a.b.g0.AdjustedCornerSize
            r7.<init>(r2, r5)
            r5 = r7
        L_0x0093:
            r6.g = r5
            j.c.a.b.g0.ShapeAppearanceModel r8 = r6.a()
            r13.f2221n = r8
            j.c.a.b.g0.ShapeAppearancePathProvider r7 = r13.f2226s
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r13.b
            float r9 = r2.f2232k
            android.graphics.RectF r2 = r13.f2218k
            android.graphics.RectF r5 = r13.b()
            r2.set(r5)
            float r2 = r13.e()
            android.graphics.RectF r5 = r13.f2218k
            r5.inset(r2, r2)
            android.graphics.RectF r10 = r13.f2218k
            android.graphics.Path r12 = r13.f2216i
            r11 = 0
            r7.a(r8, r9, r10, r11, r12)
            android.graphics.RectF r2 = r13.b()
            android.graphics.Path r5 = r13.h
            r13.a(r2, r5)
            r13.f2215f = r3
            goto L_0x00c8
        L_0x00c7:
            throw r4
        L_0x00c8:
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r13.b
            int r5 = r2.f2238q
            r6 = 2
            r7 = 1
            if (r5 == r7) goto L_0x00f1
            int r2 = r2.f2239r
            if (r2 <= 0) goto L_0x00f1
            if (r5 == r6) goto L_0x00ef
            boolean r2 = r13.h()
            if (r2 != 0) goto L_0x00ec
            android.graphics.Path r2 = r13.h
            boolean r2 = r2.isConvex()
            if (r2 != 0) goto L_0x00ec
            int r2 = android.os.Build.VERSION.SDK_INT
            r5 = 29
            if (r2 >= r5) goto L_0x00ec
            r2 = 1
            goto L_0x00ed
        L_0x00ec:
            r2 = 0
        L_0x00ed:
            if (r2 == 0) goto L_0x00f1
        L_0x00ef:
            r2 = 1
            goto L_0x00f2
        L_0x00f1:
            r2 = 0
        L_0x00f2:
            if (r2 != 0) goto L_0x00f6
            goto L_0x018b
        L_0x00f6:
            r14.save()
            int r2 = r13.c()
            int r5 = r13.d()
            float r2 = (float) r2
            float r5 = (float) r5
            r14.translate(r2, r5)
            boolean r2 = r13.w
            if (r2 != 0) goto L_0x0111
            r13.a(r14)
            r14.restore()
            goto L_0x018b
        L_0x0111:
            android.graphics.RectF r2 = r13.v
            float r2 = r2.width()
            android.graphics.Rect r5 = r13.getBounds()
            int r5 = r5.width()
            float r5 = (float) r5
            float r2 = r2 - r5
            int r2 = (int) r2
            android.graphics.RectF r5 = r13.v
            float r5 = r5.height()
            android.graphics.Rect r8 = r13.getBounds()
            int r8 = r8.height()
            float r8 = (float) r8
            float r5 = r5 - r8
            int r5 = (int) r5
            if (r2 < 0) goto L_0x01db
            if (r5 < 0) goto L_0x01db
            android.graphics.RectF r8 = r13.v
            float r8 = r8.width()
            int r8 = (int) r8
            j.c.a.b.g0.MaterialShapeDrawable$b r9 = r13.b
            int r9 = r9.f2239r
            int r9 = r9 * 2
            int r9 = r9 + r8
            int r9 = r9 + r2
            android.graphics.RectF r8 = r13.v
            float r8 = r8.height()
            int r8 = (int) r8
            j.c.a.b.g0.MaterialShapeDrawable$b r10 = r13.b
            int r10 = r10.f2239r
            int r10 = r10 * 2
            int r10 = r10 + r8
            int r10 = r10 + r5
            android.graphics.Bitmap$Config r6 = android.graphics.Bitmap.Config.ARGB_8888
            android.graphics.Bitmap r6 = android.graphics.Bitmap.createBitmap(r9, r10, r6)
            android.graphics.Canvas r8 = new android.graphics.Canvas
            r8.<init>(r6)
            android.graphics.Rect r9 = r13.getBounds()
            int r9 = r9.left
            j.c.a.b.g0.MaterialShapeDrawable$b r10 = r13.b
            int r10 = r10.f2239r
            int r9 = r9 - r10
            int r9 = r9 - r2
            float r2 = (float) r9
            android.graphics.Rect r9 = r13.getBounds()
            int r9 = r9.top
            j.c.a.b.g0.MaterialShapeDrawable$b r10 = r13.b
            int r10 = r10.f2239r
            int r9 = r9 - r10
            int r9 = r9 - r5
            float r5 = (float) r9
            float r9 = -r2
            float r10 = -r5
            r8.translate(r9, r10)
            r13.a(r8)
            r14.drawBitmap(r6, r2, r5, r4)
            r6.recycle()
            r14.restore()
        L_0x018b:
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r13.b
            android.graphics.Paint$Style r2 = r2.v
            android.graphics.Paint$Style r4 = android.graphics.Paint.Style.FILL_AND_STROKE
            if (r2 == r4) goto L_0x0197
            android.graphics.Paint$Style r4 = android.graphics.Paint.Style.FILL
            if (r2 != r4) goto L_0x0198
        L_0x0197:
            r3 = 1
        L_0x0198:
            if (r3 == 0) goto L_0x01ab
            android.graphics.Paint r6 = r13.f2222o
            android.graphics.Path r7 = r13.h
            j.c.a.b.g0.MaterialShapeDrawable$b r2 = r13.b
            j.c.a.b.g0.ShapeAppearanceModel r8 = r2.a
            android.graphics.RectF r9 = r13.b()
            r4 = r13
            r5 = r14
            r4.a(r5, r6, r7, r8, r9)
        L_0x01ab:
            boolean r2 = r13.g()
            if (r2 == 0) goto L_0x01d0
            android.graphics.Paint r5 = r13.f2223p
            android.graphics.Path r6 = r13.f2216i
            j.c.a.b.g0.ShapeAppearanceModel r7 = r13.f2221n
            android.graphics.RectF r2 = r13.f2218k
            android.graphics.RectF r3 = r13.b()
            r2.set(r3)
            float r2 = r13.e()
            android.graphics.RectF r3 = r13.f2218k
            r3.inset(r2, r2)
            android.graphics.RectF r8 = r13.f2218k
            r3 = r13
            r4 = r14
            r3.a(r4, r5, r6, r7, r8)
        L_0x01d0:
            android.graphics.Paint r14 = r13.f2222o
            r14.setAlpha(r0)
            android.graphics.Paint r14 = r13.f2223p
            r14.setAlpha(r1)
            return
        L_0x01db:
            java.lang.IllegalStateException r14 = new java.lang.IllegalStateException
            java.lang.String r0 = "Invalid shadow bounds. Check that the treatments result in a valid path."
            r14.<init>(r0)
            throw r14
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.g0.MaterialShapeDrawable.draw(android.graphics.Canvas):void");
    }

    public final float e() {
        if (g()) {
            return this.f2223p.getStrokeWidth() / 2.0f;
        }
        return 0.0f;
    }

    public float f() {
        return this.b.a.f2242e.a(b());
    }

    public final boolean g() {
        Paint.Style style = this.b.v;
        return (style == Paint.Style.FILL_AND_STROKE || style == Paint.Style.STROKE) && this.f2223p.getStrokeWidth() > 0.0f;
    }

    public Drawable.ConstantState getConstantState() {
        return this.b;
    }

    public int getOpacity() {
        return -3;
    }

    @TargetApi(21)
    public void getOutline(Outline outline) {
        if (this.b.f2238q != 2) {
            if (h()) {
                outline.setRoundRect(getBounds(), f() * this.b.f2232k);
                return;
            }
            a(b(), this.h);
            if (this.h.isConvex() || Build.VERSION.SDK_INT >= 29) {
                outline.setConvexPath(this.h);
            }
        }
    }

    public boolean getPadding(Rect rect) {
        Rect rect2 = this.b.f2230i;
        if (rect2 == null) {
            return super.getPadding(rect);
        }
        rect.set(rect2);
        return true;
    }

    public Region getTransparentRegion() {
        this.f2219l.set(getBounds());
        a(b(), this.h);
        this.f2220m.setPath(this.h, this.f2219l);
        this.f2219l.op(this.f2220m, Region.Op.DIFFERENCE);
        return this.f2219l;
    }

    public boolean h() {
        return this.b.a.a(b());
    }

    public final boolean i() {
        PorterDuffColorFilter porterDuffColorFilter = this.f2227t;
        PorterDuffColorFilter porterDuffColorFilter2 = this.u;
        b bVar = this.b;
        this.f2227t = a(bVar.g, bVar.h, this.f2222o, true);
        b bVar2 = this.b;
        this.u = a(bVar2.f2229f, bVar2.h, this.f2223p, false);
        b bVar3 = this.b;
        if (bVar3.u) {
            this.f2224q.a(bVar3.g.getColorForState(getState(), 0));
        }
        if (!Objects.equals(porterDuffColorFilter, this.f2227t) || !Objects.equals(porterDuffColorFilter2, this.u)) {
            return true;
        }
        return false;
    }

    public void invalidateSelf() {
        this.f2215f = true;
        super.invalidateSelf();
    }

    public boolean isStateful() {
        ColorStateList colorStateList;
        ColorStateList colorStateList2;
        ColorStateList colorStateList3;
        ColorStateList colorStateList4;
        return super.isStateful() || ((colorStateList = this.b.g) != null && colorStateList.isStateful()) || (((colorStateList2 = this.b.f2229f) != null && colorStateList2.isStateful()) || (((colorStateList3 = this.b.f2228e) != null && colorStateList3.isStateful()) || ((colorStateList4 = this.b.d) != null && colorStateList4.isStateful())));
    }

    public final void j() {
        b bVar = this.b;
        float f2 = bVar.f2236o + bVar.f2237p;
        bVar.f2239r = (int) Math.ceil((double) (0.75f * f2));
        this.b.f2240s = (int) Math.ceil((double) (f2 * 0.25f));
        i();
        super.invalidateSelf();
    }

    public Drawable mutate() {
        this.b = new b(this.b);
        return super;
    }

    public void onBoundsChange(Rect rect) {
        this.f2215f = true;
        super.onBoundsChange(rect);
    }

    public boolean onStateChange(int[] iArr) {
        boolean z = a(iArr) || i();
        if (z) {
            invalidateSelf();
        }
        return z;
    }

    public void setAlpha(int i2) {
        b bVar = this.b;
        if (bVar.f2234m != i2) {
            bVar.f2234m = i2;
            super.invalidateSelf();
        }
    }

    public void setColorFilter(ColorFilter colorFilter) {
        this.b.c = colorFilter;
        super.invalidateSelf();
    }

    public void setShapeAppearanceModel(ShapeAppearanceModel shapeAppearanceModel) {
        this.b.a = shapeAppearanceModel;
        invalidateSelf();
    }

    public void setTint(int i2) {
        setTintList(ColorStateList.valueOf(i2));
    }

    public void setTintList(ColorStateList colorStateList) {
        this.b.g = colorStateList;
        i();
        super.invalidateSelf();
    }

    public void setTintMode(PorterDuff.Mode mode) {
        b bVar = this.b;
        if (bVar.h != mode) {
            bVar.h = mode;
            i();
            super.invalidateSelf();
        }
    }

    public MaterialShapeDrawable(ShapeAppearanceModel shapeAppearanceModel) {
        this(new b(shapeAppearanceModel, null));
    }

    public MaterialShapeDrawable(b bVar) {
        this.c = new ShapePath.f[4];
        this.d = new ShapePath.f[4];
        this.f2214e = new BitSet(8);
        this.g = new Matrix();
        this.h = new Path();
        this.f2216i = new Path();
        this.f2217j = new RectF();
        this.f2218k = new RectF();
        this.f2219l = new Region();
        this.f2220m = new Region();
        this.f2222o = new Paint(1);
        this.f2223p = new Paint(1);
        this.f2224q = new ShadowRenderer();
        this.f2226s = new ShapeAppearancePathProvider();
        this.v = new RectF();
        this.w = true;
        this.b = bVar;
        this.f2223p.setStyle(Paint.Style.STROKE);
        this.f2222o.setStyle(Paint.Style.FILL);
        y.setColor(-1);
        y.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_OUT));
        i();
        a(getState());
        this.f2225r = new a();
    }

    public RectF b() {
        this.f2217j.set(getBounds());
        return this.f2217j;
    }

    public void b(float f2) {
        b bVar = this.b;
        if (bVar.f2232k != f2) {
            bVar.f2232k = f2;
            this.f2215f = true;
            invalidateSelf();
        }
    }

    public void b(int i2) {
        this.f2224q.a(i2);
        this.b.u = false;
        super.invalidateSelf();
    }

    public void a(ColorStateList colorStateList) {
        b bVar = this.b;
        if (bVar.d != colorStateList) {
            bVar.d = colorStateList;
            onStateChange(getState());
        }
    }

    public final void b(RectF rectF, Path path) {
        ShapeAppearancePathProvider shapeAppearancePathProvider = this.f2226s;
        b bVar = this.b;
        ShapeAppearanceModel shapeAppearanceModel = bVar.a;
        float f2 = bVar.f2232k;
        shapeAppearancePathProvider.a(shapeAppearanceModel, f2, rectF, this.f2225r, path);
    }

    public void a(float f2, int i2) {
        this.b.f2233l = f2;
        invalidateSelf();
        b(ColorStateList.valueOf(i2));
    }

    public void a(float f2, ColorStateList colorStateList) {
        this.b.f2233l = f2;
        invalidateSelf();
        b(colorStateList);
    }

    public void a(Context context) {
        this.b.b = new ElevationOverlayProvider(context);
        j();
    }

    public static final class b extends Drawable.ConstantState {
        public ShapeAppearanceModel a;
        public ElevationOverlayProvider b;
        public ColorFilter c;
        public ColorStateList d = null;

        /* renamed from: e  reason: collision with root package name */
        public ColorStateList f2228e = null;

        /* renamed from: f  reason: collision with root package name */
        public ColorStateList f2229f = null;
        public ColorStateList g = null;
        public PorterDuff.Mode h = PorterDuff.Mode.SRC_IN;

        /* renamed from: i  reason: collision with root package name */
        public Rect f2230i = null;

        /* renamed from: j  reason: collision with root package name */
        public float f2231j = 1.0f;

        /* renamed from: k  reason: collision with root package name */
        public float f2232k = 1.0f;

        /* renamed from: l  reason: collision with root package name */
        public float f2233l;

        /* renamed from: m  reason: collision with root package name */
        public int f2234m = 255;

        /* renamed from: n  reason: collision with root package name */
        public float f2235n = 0.0f;

        /* renamed from: o  reason: collision with root package name */
        public float f2236o = 0.0f;

        /* renamed from: p  reason: collision with root package name */
        public float f2237p = 0.0f;

        /* renamed from: q  reason: collision with root package name */
        public int f2238q = 0;

        /* renamed from: r  reason: collision with root package name */
        public int f2239r = 0;

        /* renamed from: s  reason: collision with root package name */
        public int f2240s = 0;

        /* renamed from: t  reason: collision with root package name */
        public int f2241t = 0;
        public boolean u = false;
        public Paint.Style v = Paint.Style.FILL_AND_STROKE;

        public b(ShapeAppearanceModel shapeAppearanceModel, ElevationOverlayProvider elevationOverlayProvider) {
            this.a = shapeAppearanceModel;
            this.b = elevationOverlayProvider;
        }

        public int getChangingConfigurations() {
            return 0;
        }

        public Drawable newDrawable() {
            MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(this);
            materialShapeDrawable.f2215f = true;
            return materialShapeDrawable;
        }

        public b(b bVar) {
            this.a = bVar.a;
            this.b = bVar.b;
            this.f2233l = bVar.f2233l;
            this.c = bVar.c;
            this.d = bVar.d;
            this.f2228e = bVar.f2228e;
            this.h = bVar.h;
            this.g = bVar.g;
            this.f2234m = bVar.f2234m;
            this.f2231j = bVar.f2231j;
            this.f2240s = bVar.f2240s;
            this.f2238q = bVar.f2238q;
            this.u = bVar.u;
            this.f2232k = bVar.f2232k;
            this.f2235n = bVar.f2235n;
            this.f2236o = bVar.f2236o;
            this.f2237p = bVar.f2237p;
            this.f2239r = bVar.f2239r;
            this.f2241t = bVar.f2241t;
            this.f2229f = bVar.f2229f;
            this.v = bVar.v;
            if (bVar.f2230i != null) {
                this.f2230i = new Rect(bVar.f2230i);
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final int a(int i2) {
        b bVar = this.b;
        float f2 = bVar.f2236o + bVar.f2237p + bVar.f2235n;
        ElevationOverlayProvider elevationOverlayProvider = bVar.b;
        if (elevationOverlayProvider == null || !elevationOverlayProvider.a) {
            return i2;
        }
        if (!(ColorUtils.b(i2, 255) == elevationOverlayProvider.c)) {
            return i2;
        }
        float f3 = elevationOverlayProvider.d;
        float f4 = 0.0f;
        if (f3 > 0.0f && f2 > 0.0f) {
            f4 = Math.min(((((float) Math.log1p((double) (f2 / f3))) * 4.5f) + 2.0f) / 100.0f, 1.0f);
        }
        return ColorUtils.b(c.a(ColorUtils.b(i2, 255), elevationOverlayProvider.b, f4), Color.alpha(i2));
    }

    public void a(float f2) {
        b bVar = this.b;
        if (bVar.f2236o != f2) {
            bVar.f2236o = f2;
            j();
        }
    }

    public final void a(Canvas canvas, Paint paint, Path path, ShapeAppearanceModel shapeAppearanceModel, RectF rectF) {
        if (shapeAppearanceModel.a(rectF)) {
            float a2 = shapeAppearanceModel.f2243f.a(rectF) * this.b.f2232k;
            canvas.drawRoundRect(rectF, a2, a2, paint);
            return;
        }
        canvas.drawPath(path, paint);
    }

    public final void a(Canvas canvas) {
        if (this.f2214e.cardinality() > 0) {
            Log.w(x, "Compatibility shadow requested but can't be drawn for all operations in this shape.");
        }
        if (this.b.f2240s != 0) {
            canvas.drawPath(this.h, this.f2224q.a);
        }
        for (int i2 = 0; i2 < 4; i2++) {
            this.c[i2].a(ShapePath.f.a, this.f2224q, this.b.f2239r, canvas);
            this.d[i2].a(ShapePath.f.a, this.f2224q, this.b.f2239r, canvas);
        }
        if (this.w) {
            int c2 = c();
            int d2 = d();
            canvas.translate((float) (-c2), (float) (-d2));
            canvas.drawPath(this.h, y);
            canvas.translate((float) c2, (float) d2);
        }
    }

    public final void a(RectF rectF, Path path) {
        b(rectF, path);
        if (this.b.f2231j != 1.0f) {
            this.g.reset();
            Matrix matrix = this.g;
            float f2 = this.b.f2231j;
            matrix.setScale(f2, f2, rectF.width() / 2.0f, rectF.height() / 2.0f);
            path.transform(this.g);
        }
        path.computeBounds(this.v, true);
    }

    public final PorterDuffColorFilter a(ColorStateList colorStateList, PorterDuff.Mode mode, Paint paint, boolean z) {
        int color;
        int a2;
        if (colorStateList == null || mode == null) {
            return (!z || (a2 = a((color = paint.getColor()))) == color) ? null : new PorterDuffColorFilter(a2, PorterDuff.Mode.SRC_IN);
        }
        int colorForState = colorStateList.getColorForState(getState(), 0);
        if (z) {
            colorForState = a(colorForState);
        }
        return new PorterDuffColorFilter(colorForState, mode);
    }

    public final boolean a(int[] iArr) {
        boolean z;
        int color;
        int colorForState;
        int color2;
        int colorForState2;
        if (this.b.d == null || color2 == (colorForState2 = this.b.d.getColorForState(iArr, (color2 = this.f2222o.getColor())))) {
            z = false;
        } else {
            this.f2222o.setColor(colorForState2);
            z = true;
        }
        if (this.b.f2228e == null || color == (colorForState = this.b.f2228e.getColorForState(iArr, (color = this.f2223p.getColor())))) {
            return z;
        }
        this.f2223p.setColor(colorForState);
        return true;
    }
}
