package j.c.a.b.g0;

import android.graphics.RectF;

public interface CornerSize {
    float a(RectF rectF);
}
