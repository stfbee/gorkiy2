package j.c.a.b.u;

import android.content.res.ColorStateList;
import android.view.View;
import androidx.appcompat.widget.AppCompatCheckBox;
import j.c.a.a.c.n.c;
import j.c.a.b.b;
import j.c.a.b.k;

public class MaterialCheckBox extends AppCompatCheckBox {
    public static final int g = k.Widget_MaterialComponents_CompoundButton_CheckBox;
    public static final int[][] h = {new int[]{16842910, 16842912}, new int[]{16842910, -16842912}, new int[]{-16842910, 16842912}, new int[]{-16842910, -16842912}};

    /* renamed from: e  reason: collision with root package name */
    public ColorStateList f2354e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f2355f;

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MaterialCheckBox(android.content.Context r8, android.util.AttributeSet r9) {
        /*
            r7 = this;
            int r3 = j.c.a.b.b.checkboxStyle
            int r0 = j.c.a.b.u.MaterialCheckBox.g
            android.content.Context r8 = j.c.a.b.m0.a.MaterialThemeOverlay.a(r8, r9, r3, r0)
            r7.<init>(r8, r9, r3)
            android.content.Context r8 = r7.getContext()
            int[] r2 = j.c.a.b.l.MaterialCheckBox
            int r4 = j.c.a.b.u.MaterialCheckBox.g
            r6 = 0
            int[] r5 = new int[r6]
            r0 = r8
            r1 = r9
            android.content.res.TypedArray r9 = j.c.a.b.b0.ThemeEnforcement.b(r0, r1, r2, r3, r4, r5)
            int r0 = j.c.a.b.l.MaterialCheckBox_buttonTint
            boolean r0 = r9.hasValue(r0)
            if (r0 == 0) goto L_0x002d
            int r0 = j.c.a.b.l.MaterialCheckBox_buttonTint
            android.content.res.ColorStateList r8 = j.c.a.a.c.n.c.a(r8, r9, r0)
            r7.setButtonTintList(r8)
        L_0x002d:
            int r8 = j.c.a.b.l.MaterialCheckBox_useMaterialThemeColors
            boolean r8 = r9.getBoolean(r8, r6)
            r7.f2355f = r8
            r9.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.u.MaterialCheckBox.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.view.View, int):int
     arg types: [j.c.a.b.u.MaterialCheckBox, int]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(android.view.View, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(int, int, float):int
     arg types: [int, int, int]
     candidates:
      j.c.a.a.c.n.c.a(float, float, float):float
      j.c.a.a.c.n.c.a(android.content.Context, int, int):int
      j.c.a.a.c.n.c.a(android.content.Context, int, java.lang.String):int
      j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(byte[], int, int):long
      j.c.a.a.c.n.c.a(android.content.Context, android.content.res.TypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.content.Context, i.b.q.TintTypedArray, int):android.content.res.ColorStateList
      j.c.a.a.c.n.c.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList, android.graphics.PorterDuff$Mode):android.graphics.PorterDuffColorFilter
      j.c.a.a.c.n.c.a(j.c.a.a.i.e, long, java.util.concurrent.TimeUnit):TResult
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String[], java.lang.String[]):java.lang.String
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[], byte[]):java.security.interfaces.ECPublicKey
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.f5, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(j.c.e.MessageLite, java.lang.StringBuilder, int):void
      j.c.a.a.c.n.c.a(byte[], long, int):void
      j.c.a.a.c.n.c.a(android.content.Context, int, boolean):boolean
      j.c.a.a.c.n.c.a(l.b.ObservableSource, l.b.Observer, l.b.t.Function):boolean
      j.c.a.a.c.n.c.a(int, int, float):int */
    private ColorStateList getMaterialThemeColorsTintList() {
        if (this.f2354e == null) {
            int[] iArr = new int[h.length];
            int a = c.a((View) this, b.colorControlActivated);
            int a2 = c.a((View) this, b.colorSurface);
            int a3 = c.a((View) this, b.colorOnSurface);
            iArr[0] = c.a(a2, a, 1.0f);
            iArr[1] = c.a(a2, a3, 0.54f);
            iArr[2] = c.a(a2, a3, 0.38f);
            iArr[3] = c.a(a2, a3, 0.38f);
            this.f2354e = new ColorStateList(h, iArr);
        }
        return this.f2354e;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (this.f2355f && getButtonTintList() == null) {
            setUseMaterialThemeColors(true);
        }
    }

    public void setUseMaterialThemeColors(boolean z) {
        this.f2355f = z;
        if (z) {
            setButtonTintList(getMaterialThemeColorsTintList());
        } else {
            setButtonTintList(null);
        }
    }
}
