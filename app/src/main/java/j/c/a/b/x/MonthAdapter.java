package j.c.a.b.x;

import android.widget.BaseAdapter;
import j.c.a.a.c.n.c;
import java.util.Calendar;

public class MonthAdapter extends BaseAdapter {

    /* renamed from: f  reason: collision with root package name */
    public static final int f2365f = c.c().getMaximum(4);
    public final Month b;
    public final DateSelector<?> c;
    public CalendarStyle d;

    /* renamed from: e  reason: collision with root package name */
    public final CalendarConstraints f2366e;

    public MonthAdapter(n nVar, DateSelector<?> dateSelector, a aVar) {
        this.b = nVar;
        this.c = dateSelector;
        this.f2366e = aVar;
    }

    public int a() {
        return this.b.f();
    }

    public int b() {
        return (this.b.f() + this.b.g) - 1;
    }

    public int getCount() {
        return a() + this.b.g;
    }

    public long getItemId(int i2) {
        return (long) (i2 / this.b.f2364f);
    }

    /* JADX WARN: Type inference failed for: r9v29, types: [android.view.View] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x00eb  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View getView(int r8, android.view.View r9, android.view.ViewGroup r10) {
        /*
            r7 = this;
            android.content.Context r0 = r10.getContext()
            j.c.a.b.x.CalendarStyle r1 = r7.d
            if (r1 != 0) goto L_0x000f
            j.c.a.b.x.CalendarStyle r1 = new j.c.a.b.x.CalendarStyle
            r1.<init>(r0)
            r7.d = r1
        L_0x000f:
            r0 = r9
            android.widget.TextView r0 = (android.widget.TextView) r0
            r1 = 0
            if (r9 != 0) goto L_0x0026
            android.content.Context r9 = r10.getContext()
            android.view.LayoutInflater r9 = android.view.LayoutInflater.from(r9)
            int r0 = j.c.a.b.h.mtrl_calendar_day
            android.view.View r9 = r9.inflate(r0, r10, r1)
            r0 = r9
            android.widget.TextView r0 = (android.widget.TextView) r0
        L_0x0026:
            int r9 = r7.a()
            int r9 = r8 - r9
            r10 = 1
            if (r9 < 0) goto L_0x00db
            j.c.a.b.x.Month r2 = r7.b
            int r3 = r2.g
            if (r9 < r3) goto L_0x0037
            goto L_0x00db
        L_0x0037:
            int r9 = r9 + r10
            r0.setTag(r2)
            java.lang.String r2 = java.lang.String.valueOf(r9)
            r0.setText(r2)
            j.c.a.b.x.Month r2 = r7.b
            java.util.Calendar r2 = r2.b
            java.util.Calendar r2 = j.c.a.a.c.n.c.a(r2)
            r3 = 5
            r2.set(r3, r9)
            long r2 = r2.getTimeInMillis()
            j.c.a.b.x.Month r9 = r7.b
            int r9 = r9.f2363e
            j.c.a.b.x.Month r4 = new j.c.a.b.x.Month
            java.util.Calendar r5 = j.c.a.a.c.n.c.b()
            r4.<init>(r5)
            int r4 = r4.f2363e
            java.lang.String r5 = "UTC"
            r6 = 24
            if (r9 != r4) goto L_0x009e
            java.util.Locale r9 = java.util.Locale.getDefault()
            int r4 = android.os.Build.VERSION.SDK_INT
            if (r4 < r6) goto L_0x0086
            java.lang.String r4 = "MMMEd"
            android.icu.text.DateFormat r9 = android.icu.text.DateFormat.getInstanceForSkeleton(r4, r9)
            android.icu.util.TimeZone r4 = android.icu.util.TimeZone.getTimeZone(r5)
            r9.setTimeZone(r4)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r2)
            java.lang.String r9 = r9.format(r4)
            goto L_0x009a
        L_0x0086:
            java.text.DateFormat r9 = java.text.DateFormat.getDateInstance(r1, r9)
            java.util.TimeZone r4 = j.c.a.a.c.n.c.a()
            r9.setTimeZone(r4)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r2)
            java.lang.String r9 = r9.format(r4)
        L_0x009a:
            r0.setContentDescription(r9)
            goto L_0x00d4
        L_0x009e:
            java.util.Locale r9 = java.util.Locale.getDefault()
            int r4 = android.os.Build.VERSION.SDK_INT
            if (r4 < r6) goto L_0x00bd
            java.lang.String r4 = "yMMMEd"
            android.icu.text.DateFormat r9 = android.icu.text.DateFormat.getInstanceForSkeleton(r4, r9)
            android.icu.util.TimeZone r4 = android.icu.util.TimeZone.getTimeZone(r5)
            r9.setTimeZone(r4)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r2)
            java.lang.String r9 = r9.format(r4)
            goto L_0x00d1
        L_0x00bd:
            java.text.DateFormat r9 = java.text.DateFormat.getDateInstance(r1, r9)
            java.util.TimeZone r4 = j.c.a.a.c.n.c.a()
            r9.setTimeZone(r4)
            java.util.Date r4 = new java.util.Date
            r4.<init>(r2)
            java.lang.String r9 = r9.format(r4)
        L_0x00d1:
            r0.setContentDescription(r9)
        L_0x00d4:
            r0.setVisibility(r1)
            r0.setEnabled(r10)
            goto L_0x00e3
        L_0x00db:
            r9 = 8
            r0.setVisibility(r9)
            r0.setEnabled(r1)
        L_0x00e3:
            java.lang.Long r8 = r7.getItem(r8)
            if (r8 != 0) goto L_0x00eb
            goto L_0x016e
        L_0x00eb:
            j.c.a.b.x.CalendarConstraints r9 = r7.f2366e
            j.c.a.b.x.CalendarConstraints$b r9 = r9.f2356e
            long r2 = r8.longValue()
            boolean r9 = r9.e(r2)
            if (r9 == 0) goto L_0x0164
            r0.setEnabled(r10)
            j.c.a.b.x.DateSelector<?> r9 = r7.c
            java.util.Collection r9 = r9.p()
            java.util.Iterator r9 = r9.iterator()
        L_0x0106:
            boolean r10 = r9.hasNext()
            if (r10 == 0) goto L_0x0144
            java.lang.Object r10 = r9.next()
            java.lang.Long r10 = (java.lang.Long) r10
            long r1 = r10.longValue()
            long r3 = r8.longValue()
            java.util.Calendar r10 = j.c.a.a.c.n.c.c()
            r10.setTimeInMillis(r3)
            java.util.Calendar r10 = j.c.a.a.c.n.c.a(r10)
            long r3 = r10.getTimeInMillis()
            java.util.Calendar r10 = j.c.a.a.c.n.c.c()
            r10.setTimeInMillis(r1)
            java.util.Calendar r10 = j.c.a.a.c.n.c.a(r10)
            long r1 = r10.getTimeInMillis()
            int r10 = (r3 > r1 ? 1 : (r3 == r1 ? 0 : -1))
            if (r10 != 0) goto L_0x0106
            j.c.a.b.x.CalendarStyle r8 = r7.d
            j.c.a.b.x.CalendarItemStyle r8 = r8.b
            r8.a(r0)
            goto L_0x016e
        L_0x0144:
            java.util.Calendar r9 = j.c.a.a.c.n.c.b()
            long r9 = r9.getTimeInMillis()
            long r1 = r8.longValue()
            int r8 = (r9 > r1 ? 1 : (r9 == r1 ? 0 : -1))
            if (r8 != 0) goto L_0x015c
            j.c.a.b.x.CalendarStyle r8 = r7.d
            j.c.a.b.x.CalendarItemStyle r8 = r8.c
            r8.a(r0)
            goto L_0x016e
        L_0x015c:
            j.c.a.b.x.CalendarStyle r8 = r7.d
            j.c.a.b.x.CalendarItemStyle r8 = r8.a
            r8.a(r0)
            goto L_0x016e
        L_0x0164:
            r0.setEnabled(r1)
            j.c.a.b.x.CalendarStyle r8 = r7.d
            j.c.a.b.x.CalendarItemStyle r8 = r8.g
            r8.a(r0)
        L_0x016e:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.a.b.x.MonthAdapter.getView(int, android.view.View, android.view.ViewGroup):android.view.View");
    }

    public boolean hasStableIds() {
        return true;
    }

    public Long getItem(int i2) {
        if (i2 < this.b.f() || i2 > b()) {
            return null;
        }
        Month month = this.b;
        Calendar a = c.a(month.b);
        a.set(5, (i2 - month.f()) + 1);
        return Long.valueOf(a.getTimeInMillis());
    }
}
