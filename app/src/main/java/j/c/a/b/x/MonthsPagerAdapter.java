package j.c.a.b.x;

import android.view.View;
import android.widget.AdapterView;
import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.datepicker.MaterialCalendarGridView;
import j.c.a.b.x.MaterialCalendar;
import java.util.Iterator;

public class MonthsPagerAdapter implements AdapterView.OnItemClickListener {
    public final /* synthetic */ MaterialCalendarGridView b;
    public final /* synthetic */ MonthsPagerAdapter0 c;

    public MonthsPagerAdapter(MonthsPagerAdapter0 monthsPagerAdapter0, MaterialCalendarGridView materialCalendarGridView) {
        this.c = monthsPagerAdapter0;
        this.b = materialCalendarGridView;
    }

    public void onItemClick(AdapterView<?> adapterView, View view, int i2, long j2) {
        MonthAdapter adapter = this.b.getAdapter();
        if (i2 >= adapter.a() && i2 <= adapter.b()) {
            MaterialCalendar.f fVar = this.c.f2367e;
            long longValue = this.b.getAdapter().getItem(i2).longValue();
            MaterialCalendar.d dVar = (MaterialCalendar.d) fVar;
            if (MaterialCalendar.this.a0.f2356e.e(longValue)) {
                MaterialCalendar.this.Z.f(longValue);
                Iterator<OnSelectionChangedListener<S>> it = MaterialCalendar.this.X.iterator();
                while (it.hasNext()) {
                    it.next().a(MaterialCalendar.this.Z.i());
                }
                MaterialCalendar.this.f0.getAdapter().a.b();
                RecyclerView recyclerView = MaterialCalendar.this.e0;
                if (recyclerView != null) {
                    recyclerView.getAdapter().a.b();
                }
            }
        }
    }
}
