package j.c.a.b.n;

import android.view.View;
import com.google.android.material.appbar.AppBarLayout;
import i.h.l.x.AccessibilityViewCommand;

/* compiled from: AppBarLayout */
public class AppBarLayout2 implements AccessibilityViewCommand {
    public final /* synthetic */ AppBarLayout a;
    public final /* synthetic */ boolean b;

    public AppBarLayout2(AppBarLayout.BaseBehavior baseBehavior, AppBarLayout appBarLayout, boolean z) {
        this.a = appBarLayout;
        this.b = z;
    }

    public boolean a(View view, AccessibilityViewCommand.a aVar) {
        this.a.setExpanded(this.b);
        return true;
    }
}
