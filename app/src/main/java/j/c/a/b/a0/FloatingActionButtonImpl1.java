package j.c.a.b.a0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.TimeInterpolator;
import android.animation.ValueAnimator;
import android.content.res.ColorStateList;
import android.graphics.Matrix;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.os.Build;
import android.view.View;
import android.view.ViewTreeObserver;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import i.b.k.ResourcesFlusher;
import i.h.l.ViewCompat;
import j.c.a.b.a0.e;
import j.c.a.b.b0.StateListAnimator;
import j.c.a.b.f0.ShadowViewDelegate;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.m.AnimationUtils;
import j.c.a.b.m.ImageMatrixProperty;
import j.c.a.b.m.MatrixEvaluator;
import j.c.a.b.m.MotionSpec;
import java.util.ArrayList;
import java.util.Iterator;

/* compiled from: FloatingActionButtonImpl */
public class FloatingActionButtonImpl1 {
    public static final TimeInterpolator A = AnimationUtils.c;
    public static final int[] B = {16842919, 16842910};
    public static final int[] C = {16843623, 16842908, 16842910};
    public static final int[] D = {16842908, 16842910};
    public static final int[] E = {16843623, 16842910};
    public static final int[] F = {16842910};
    public static final int[] G = new int[0];
    public ShapeAppearanceModel a;
    public MaterialShapeDrawable b;
    public boolean c;
    public boolean d = true;

    /* renamed from: e  reason: collision with root package name */
    public float f2149e;

    /* renamed from: f  reason: collision with root package name */
    public float f2150f;
    public float g;
    public final StateListAnimator h;

    /* renamed from: i  reason: collision with root package name */
    public MotionSpec f2151i;

    /* renamed from: j  reason: collision with root package name */
    public MotionSpec f2152j;

    /* renamed from: k  reason: collision with root package name */
    public Animator f2153k;

    /* renamed from: l  reason: collision with root package name */
    public MotionSpec f2154l;

    /* renamed from: m  reason: collision with root package name */
    public MotionSpec f2155m;

    /* renamed from: n  reason: collision with root package name */
    public float f2156n;

    /* renamed from: o  reason: collision with root package name */
    public float f2157o = 1.0f;

    /* renamed from: p  reason: collision with root package name */
    public int f2158p = 0;

    /* renamed from: q  reason: collision with root package name */
    public ArrayList<Animator.AnimatorListener> f2159q;

    /* renamed from: r  reason: collision with root package name */
    public ArrayList<Animator.AnimatorListener> f2160r;

    /* renamed from: s  reason: collision with root package name */
    public ArrayList<e.e> f2161s;

    /* renamed from: t  reason: collision with root package name */
    public final FloatingActionButton f2162t;
    public final ShadowViewDelegate u;
    public final Rect v = new Rect();
    public final RectF w = new RectF();
    public final RectF x = new RectF();
    public final Matrix y = new Matrix();
    public ViewTreeObserver.OnPreDrawListener z;

    /* compiled from: FloatingActionButtonImpl */
    public class a extends MatrixEvaluator {
        public a() {
        }

        public Object evaluate(float f2, Object obj, Object obj2) {
            FloatingActionButtonImpl1.this.f2157o = f2;
            ((Matrix) obj).getValues(super.a);
            ((Matrix) obj2).getValues(super.b);
            for (int i2 = 0; i2 < 9; i2++) {
                float[] fArr = super.b;
                float f3 = fArr[i2];
                float[] fArr2 = super.a;
                fArr[i2] = ((f3 - fArr2[i2]) * f2) + fArr2[i2];
            }
            super.c.setValues(super.b);
            return super.c;
        }
    }

    /* compiled from: FloatingActionButtonImpl */
    public class b extends h {
        public b(FloatingActionButtonImpl1 floatingActionButtonImpl1) {
            super(null);
        }

        public float a() {
            return 0.0f;
        }
    }

    /* compiled from: FloatingActionButtonImpl */
    public class c extends h {
        public c() {
            super(null);
        }

        public float a() {
            FloatingActionButtonImpl1 floatingActionButtonImpl1 = FloatingActionButtonImpl1.this;
            return floatingActionButtonImpl1.f2149e + floatingActionButtonImpl1.f2150f;
        }
    }

    /* compiled from: FloatingActionButtonImpl */
    public class d extends h {
        public d() {
            super(null);
        }

        public float a() {
            FloatingActionButtonImpl1 floatingActionButtonImpl1 = FloatingActionButtonImpl1.this;
            return floatingActionButtonImpl1.f2149e + floatingActionButtonImpl1.g;
        }
    }

    /* compiled from: FloatingActionButtonImpl */
    public interface e {
        void a();

        void b();
    }

    /* compiled from: FloatingActionButtonImpl */
    public interface f {
    }

    /* compiled from: FloatingActionButtonImpl */
    public class g extends h {
        public g() {
            super(null);
        }

        public float a() {
            return FloatingActionButtonImpl1.this.f2149e;
        }
    }

    /* compiled from: FloatingActionButtonImpl */
    public abstract class h extends AnimatorListenerAdapter implements ValueAnimator.AnimatorUpdateListener {
        public boolean a;
        public float b;
        public float c;

        public /* synthetic */ h(FloatingActionButtonImpl floatingActionButtonImpl) {
        }

        public abstract float a();

        public void onAnimationEnd(Animator animator) {
            if (FloatingActionButtonImpl1.this != null) {
                this.a = false;
                return;
            }
            throw null;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            float f2;
            if (!this.a) {
                MaterialShapeDrawable materialShapeDrawable = FloatingActionButtonImpl1.this.b;
                if (materialShapeDrawable == null) {
                    f2 = 0.0f;
                } else {
                    f2 = materialShapeDrawable.b.f2236o;
                }
                this.b = f2;
                this.c = a();
                this.a = true;
            }
            FloatingActionButtonImpl1 floatingActionButtonImpl1 = FloatingActionButtonImpl1.this;
            valueAnimator.getAnimatedFraction();
            if (floatingActionButtonImpl1 == null) {
                throw null;
            }
        }
    }

    public FloatingActionButtonImpl1(FloatingActionButton floatingActionButton, ShadowViewDelegate shadowViewDelegate) {
        this.f2162t = floatingActionButton;
        this.u = shadowViewDelegate;
        StateListAnimator stateListAnimator = new StateListAnimator();
        this.h = stateListAnimator;
        stateListAnimator.a(B, a(new d()));
        this.h.a(C, a(new c()));
        this.h.a(D, a(new c()));
        this.h.a(E, a(new c()));
        this.h.a(F, a(new g()));
        this.h.a(G, a(new b(this)));
        this.f2156n = this.f2162t.getRotation();
    }

    public float a() {
        throw null;
    }

    public final void a(float f2) {
        this.f2157o = f2;
        Matrix matrix = this.y;
        matrix.reset();
        this.f2162t.getDrawable();
        this.f2162t.setImageMatrix(matrix);
    }

    public void a(float f2, float f3, float f4) {
        throw null;
    }

    public void a(ColorStateList colorStateList) {
        throw null;
    }

    public void a(int[] iArr) {
        throw null;
    }

    public boolean b() {
        if (this.f2162t.getVisibility() == 0) {
            if (this.f2158p == 1) {
                return true;
            }
            return false;
        } else if (this.f2158p != 2) {
            return true;
        } else {
            return false;
        }
    }

    public boolean c() {
        if (this.f2162t.getVisibility() != 0) {
            if (this.f2158p == 2) {
                return true;
            }
            return false;
        } else if (this.f2158p != 1) {
            return true;
        } else {
            return false;
        }
    }

    public void d() {
        throw null;
    }

    public void e() {
        throw null;
    }

    public void f() {
        ArrayList<e.e> arrayList = this.f2161s;
        if (arrayList != null) {
            Iterator<e.e> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().a();
            }
        }
    }

    public void g() {
        ArrayList<e.e> arrayList = this.f2161s;
        if (arrayList != null) {
            Iterator<e.e> it = arrayList.iterator();
            while (it.hasNext()) {
                it.next().b();
            }
        }
    }

    public boolean h() {
        throw null;
    }

    public boolean i() {
        throw null;
    }

    public final boolean j() {
        return ViewCompat.w(this.f2162t) && !this.f2162t.isInEditMode();
    }

    public final boolean k() {
        return !this.c || this.f2162t.getSizeDimension() >= 0;
    }

    public void l() {
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
     arg types: [?[OBJECT, ARRAY], java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T */
    public final void m() {
        Rect rect = this.v;
        a(rect);
        ResourcesFlusher.a((Object) null, (Object) "Didn't initialize content background");
        if (i()) {
            InsetDrawable insetDrawable = new InsetDrawable((Drawable) null, rect.left, rect.top, rect.right, rect.bottom);
            FloatingActionButton.b bVar = (FloatingActionButton.b) this.u;
            if (bVar != null) {
                FloatingActionButtonImpl1.super.setBackgroundDrawable(insetDrawable);
            } else {
                throw null;
            }
        } else if (((FloatingActionButton.b) this.u) == null) {
            throw null;
        }
        ShadowViewDelegate shadowViewDelegate = this.u;
        int i2 = rect.left;
        int i3 = rect.top;
        int i4 = rect.right;
        int i5 = rect.bottom;
        FloatingActionButton.b bVar2 = (FloatingActionButton.b) shadowViewDelegate;
        FloatingActionButton.this.f478l.set(i2, i3, i4, i5);
        FloatingActionButton floatingActionButton = FloatingActionButton.this;
        int i6 = floatingActionButton.f476j;
        floatingActionButton.setPadding(i2 + i6, i3 + i6, i4 + i6, i5 + i6);
    }

    public final AnimatorSet a(MotionSpec motionSpec, float f2, float f3, float f4) {
        ArrayList arrayList = new ArrayList();
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this.f2162t, View.ALPHA, f2);
        motionSpec.a("opacity").a(ofFloat);
        arrayList.add(ofFloat);
        ObjectAnimator ofFloat2 = ObjectAnimator.ofFloat(this.f2162t, View.SCALE_X, f3);
        motionSpec.a("scale").a(ofFloat2);
        if (Build.VERSION.SDK_INT == 26) {
            ofFloat2.setEvaluator(new FloatingActionButtonImpl2(this));
        }
        arrayList.add(ofFloat2);
        ObjectAnimator ofFloat3 = ObjectAnimator.ofFloat(this.f2162t, View.SCALE_Y, f3);
        motionSpec.a("scale").a(ofFloat3);
        if (Build.VERSION.SDK_INT == 26) {
            ofFloat3.setEvaluator(new FloatingActionButtonImpl2(this));
        }
        arrayList.add(ofFloat3);
        this.y.reset();
        this.f2162t.getDrawable();
        ObjectAnimator ofObject = ObjectAnimator.ofObject(this.f2162t, new ImageMatrixProperty(), new a(), new Matrix(this.y));
        motionSpec.a("iconScale").a(ofObject);
        arrayList.add(ofObject);
        AnimatorSet animatorSet = new AnimatorSet();
        j.c.a.a.c.n.c.a(animatorSet, arrayList);
        return animatorSet;
    }

    public void a(Rect rect) {
        int i2 = 0;
        if (this.c) {
            i2 = (0 - this.f2162t.getSizeDimension()) / 2;
        }
        float a2 = this.d ? a() + this.g : 0.0f;
        int max = Math.max(i2, (int) Math.ceil((double) a2));
        int max2 = Math.max(i2, (int) Math.ceil((double) (a2 * 1.5f)));
        rect.set(max, max2, max, max2);
    }

    public final ValueAnimator a(h hVar) {
        ValueAnimator valueAnimator = new ValueAnimator();
        valueAnimator.setInterpolator(A);
        valueAnimator.setDuration(100L);
        valueAnimator.addListener(hVar);
        valueAnimator.addUpdateListener(hVar);
        valueAnimator.setFloatValues(0.0f, 1.0f);
        return valueAnimator;
    }
}
