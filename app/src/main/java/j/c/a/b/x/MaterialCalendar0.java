package j.c.a.b.x;

import android.view.View;
import j.c.a.b.x.MaterialCalendar;

/* compiled from: MaterialCalendar */
public class MaterialCalendar0 implements View.OnClickListener {
    public final /* synthetic */ MaterialCalendar b;

    public MaterialCalendar0(MaterialCalendar materialCalendar) {
        this.b = materialCalendar;
    }

    public void onClick(View view) {
        MaterialCalendar materialCalendar = this.b;
        MaterialCalendar.e eVar = materialCalendar.c0;
        if (eVar == MaterialCalendar.e.YEAR) {
            materialCalendar.a(MaterialCalendar.e.DAY);
        } else if (eVar == MaterialCalendar.e.DAY) {
            materialCalendar.a(MaterialCalendar.e.YEAR);
        }
    }
}
