package j.c.a.b.g0;

import android.graphics.Matrix;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.RectF;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapePath;
import java.util.ArrayList;

public class ShapeAppearancePathProvider {
    public final ShapePath[] a = new ShapePath[4];
    public final Matrix[] b = new Matrix[4];
    public final Matrix[] c = new Matrix[4];
    public final PointF d = new PointF();

    /* renamed from: e  reason: collision with root package name */
    public final Path f2254e = new Path();

    /* renamed from: f  reason: collision with root package name */
    public final Path f2255f = new Path();
    public final ShapePath g = new ShapePath();
    public final float[] h = new float[2];

    /* renamed from: i  reason: collision with root package name */
    public final float[] f2256i = new float[2];

    /* renamed from: j  reason: collision with root package name */
    public boolean f2257j = true;

    public interface a {
    }

    public ShapeAppearancePathProvider() {
        for (int i2 = 0; i2 < 4; i2++) {
            this.a[i2] = new ShapePath();
            this.b[i2] = new Matrix();
            this.c[i2] = new Matrix();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.max(float, float):float}
     arg types: [float, int]
     candidates:
      ClspMth{java.lang.Math.max(double, double):double}
      ClspMth{java.lang.Math.max(int, int):int}
      ClspMth{java.lang.Math.max(long, long):long}
      ClspMth{java.lang.Math.max(float, float):float} */
    public void a(ShapeAppearanceModel shapeAppearanceModel, float f2, RectF rectF, a aVar, Path path) {
        float f3;
        EdgeTreatment edgeTreatment;
        CornerSize cornerSize;
        CornerTreatment cornerTreatment;
        ShapeAppearanceModel shapeAppearanceModel2 = shapeAppearanceModel;
        float f4 = f2;
        RectF rectF2 = rectF;
        Path path2 = path;
        path.rewind();
        this.f2254e.rewind();
        this.f2255f.rewind();
        this.f2255f.addRect(rectF2, Path.Direction.CW);
        char c2 = 0;
        int i2 = 0;
        while (i2 < 4) {
            if (i2 == 1) {
                cornerSize = shapeAppearanceModel2.g;
            } else if (i2 == 2) {
                cornerSize = shapeAppearanceModel2.h;
            } else if (i2 != 3) {
                cornerSize = shapeAppearanceModel2.f2243f;
            } else {
                cornerSize = shapeAppearanceModel2.f2242e;
            }
            if (i2 == 1) {
                cornerTreatment = shapeAppearanceModel2.c;
            } else if (i2 == 2) {
                cornerTreatment = shapeAppearanceModel2.d;
            } else if (i2 != 3) {
                cornerTreatment = shapeAppearanceModel2.b;
            } else {
                cornerTreatment = shapeAppearanceModel2.a;
            }
            ShapePath shapePath = this.a[i2];
            if (cornerTreatment != null) {
                cornerTreatment.a(shapePath, 90.0f, f4, cornerSize.a(rectF2));
                int i3 = i2 + 1;
                float f5 = (float) (i3 * 90);
                this.b[i2].reset();
                PointF pointF = this.d;
                if (i2 == 1) {
                    pointF.set(rectF2.right, rectF2.bottom);
                } else if (i2 == 2) {
                    pointF.set(rectF2.left, rectF2.bottom);
                } else if (i2 != 3) {
                    pointF.set(rectF2.right, rectF2.top);
                } else {
                    pointF.set(rectF2.left, rectF2.top);
                }
                Matrix matrix = this.b[i2];
                PointF pointF2 = this.d;
                matrix.setTranslate(pointF2.x, pointF2.y);
                this.b[i2].preRotate(f5);
                float[] fArr = this.h;
                ShapePath[] shapePathArr = this.a;
                fArr[0] = shapePathArr[i2].c;
                fArr[1] = shapePathArr[i2].d;
                this.b[i2].mapPoints(fArr);
                this.c[i2].reset();
                Matrix matrix2 = this.c[i2];
                float[] fArr2 = this.h;
                matrix2.setTranslate(fArr2[0], fArr2[1]);
                this.c[i2].preRotate(f5);
                i2 = i3;
            } else {
                throw null;
            }
        }
        int i4 = 0;
        while (i4 < 4) {
            float[] fArr3 = this.h;
            ShapePath[] shapePathArr2 = this.a;
            fArr3[c2] = shapePathArr2[i4].a;
            fArr3[1] = shapePathArr2[i4].b;
            this.b[i4].mapPoints(fArr3);
            if (i4 == 0) {
                float[] fArr4 = this.h;
                path2.moveTo(fArr4[c2], fArr4[1]);
            } else {
                float[] fArr5 = this.h;
                path2.lineTo(fArr5[c2], fArr5[1]);
            }
            this.a[i4].a(this.b[i4], path2);
            if (aVar != null) {
                ShapePath shapePath2 = this.a[i4];
                Matrix matrix3 = this.b[i4];
                MaterialShapeDrawable.a aVar2 = (MaterialShapeDrawable.a) aVar;
                MaterialShapeDrawable.this.f2214e.set(i4, shapePath2.f2260i);
                ShapePath.f[] fVarArr = MaterialShapeDrawable.this.c;
                shapePath2.a(shapePath2.f2259f);
                fVarArr[i4] = new ShapePath0(shapePath2, new ArrayList(shapePath2.h), matrix3);
            }
            int i5 = i4 + 1;
            int i6 = i5 % 4;
            float[] fArr6 = this.h;
            ShapePath[] shapePathArr3 = this.a;
            fArr6[c2] = shapePathArr3[i4].c;
            fArr6[1] = shapePathArr3[i4].d;
            this.b[i4].mapPoints(fArr6);
            float[] fArr7 = this.f2256i;
            ShapePath[] shapePathArr4 = this.a;
            fArr7[c2] = shapePathArr4[i6].a;
            fArr7[1] = shapePathArr4[i6].b;
            this.b[i6].mapPoints(fArr7);
            float[] fArr8 = this.h;
            float f6 = fArr8[c2];
            float[] fArr9 = this.f2256i;
            float max = Math.max(((float) Math.hypot((double) (f6 - fArr9[c2]), (double) (fArr8[1] - fArr9[1]))) - 0.001f, 0.0f);
            float[] fArr10 = this.h;
            ShapePath[] shapePathArr5 = this.a;
            fArr10[c2] = shapePathArr5[i4].c;
            fArr10[1] = shapePathArr5[i4].d;
            this.b[i4].mapPoints(fArr10);
            if (i4 == 1 || i4 == 3) {
                f3 = Math.abs(rectF.centerX() - this.h[c2]);
            } else {
                f3 = Math.abs(rectF.centerY() - this.h[1]);
            }
            this.g.a(0.0f, 0.0f, 270.0f, 0.0f);
            if (i4 == 1) {
                edgeTreatment = shapeAppearanceModel2.f2246k;
            } else if (i4 == 2) {
                edgeTreatment = shapeAppearanceModel2.f2247l;
            } else if (i4 != 3) {
                edgeTreatment = shapeAppearanceModel2.f2245j;
            } else {
                edgeTreatment = shapeAppearanceModel2.f2244i;
            }
            edgeTreatment.a(max, f3, f4, this.g);
            Path path3 = new Path();
            this.g.a(this.c[i4], path3);
            if (!this.f2257j || (!a(path3, i4) && !a(path3, i6))) {
                this.g.a(this.c[i4], path2);
            } else {
                path3.op(path3, this.f2255f, Path.Op.DIFFERENCE);
                float[] fArr11 = this.h;
                ShapePath shapePath3 = this.g;
                fArr11[c2] = shapePath3.a;
                fArr11[1] = shapePath3.b;
                this.c[i4].mapPoints(fArr11);
                Path path4 = this.f2254e;
                float[] fArr12 = this.h;
                path4.moveTo(fArr12[c2], fArr12[1]);
                this.g.a(this.c[i4], this.f2254e);
            }
            if (aVar != null) {
                ShapePath shapePath4 = this.g;
                Matrix matrix4 = this.c[i4];
                MaterialShapeDrawable.a aVar3 = (MaterialShapeDrawable.a) aVar;
                MaterialShapeDrawable.this.f2214e.set(i4 + 4, shapePath4.f2260i);
                ShapePath.f[] fVarArr2 = MaterialShapeDrawable.this.d;
                shapePath4.a(shapePath4.f2259f);
                fVarArr2[i4] = new ShapePath0(shapePath4, new ArrayList(shapePath4.h), matrix4);
            }
            i4 = i5;
            c2 = 0;
        }
        path.close();
        this.f2254e.close();
        if (!this.f2254e.isEmpty()) {
            path2.op(this.f2254e, Path.Op.UNION);
        }
    }

    public final boolean a(Path path, int i2) {
        Path path2 = new Path();
        this.a[i2].a(this.b[i2], path2);
        RectF rectF = new RectF();
        path.computeBounds(rectF, true);
        path2.computeBounds(rectF, true);
        path.op(path2, Path.Op.INTERSECT);
        path.computeBounds(rectF, true);
        if (!rectF.isEmpty()) {
            return true;
        }
        if (rectF.width() <= 1.0f || rectF.height() <= 1.0f) {
            return false;
        }
        return true;
    }
}
