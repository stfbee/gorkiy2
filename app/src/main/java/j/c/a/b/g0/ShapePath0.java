package j.c.a.b.g0;

import android.graphics.Canvas;
import android.graphics.Matrix;
import j.c.a.b.f0.ShadowRenderer;
import j.c.a.b.g0.ShapePath;
import java.util.List;

/* compiled from: ShapePath */
public class ShapePath0 extends ShapePath.f {
    public final /* synthetic */ List b;
    public final /* synthetic */ Matrix c;

    public ShapePath0(ShapePath shapePath, List list, Matrix matrix) {
        this.b = list;
        this.c = matrix;
    }

    public void a(Matrix matrix, ShadowRenderer shadowRenderer, int i2, Canvas canvas) {
        for (ShapePath.f a : this.b) {
            a.a(this.c, shadowRenderer, i2, canvas);
        }
    }
}
