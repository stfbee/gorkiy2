package j.c.a.b.w;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

public final class CircularRevealCompat extends AnimatorListenerAdapter {
    public final /* synthetic */ CircularRevealWidget a;

    public CircularRevealCompat(CircularRevealWidget circularRevealWidget) {
        this.a = circularRevealWidget;
    }

    public void onAnimationEnd(Animator animator) {
        this.a.b();
    }

    public void onAnimationStart(Animator animator) {
        this.a.a();
    }
}
