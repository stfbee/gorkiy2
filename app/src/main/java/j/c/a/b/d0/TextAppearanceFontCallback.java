package j.c.a.b.d0;

import android.graphics.Typeface;

public abstract class TextAppearanceFontCallback {
    public abstract void a(int i2);

    public abstract void a(Typeface typeface, boolean z);
}
