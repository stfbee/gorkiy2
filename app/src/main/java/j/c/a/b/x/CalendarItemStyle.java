package j.c.a.b.x;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.widget.TextView;
import i.b.k.ResourcesFlusher;
import i.h.l.ViewCompat;
import j.c.a.a.c.n.c;
import j.c.a.b.g0.AbsoluteCornerSize;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.l;

public final class CalendarItemStyle {
    public final Rect a;
    public final ColorStateList b;
    public final ColorStateList c;
    public final ColorStateList d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2358e;

    /* renamed from: f  reason: collision with root package name */
    public final ShapeAppearanceModel f2359f;

    public CalendarItemStyle(ColorStateList colorStateList, ColorStateList colorStateList2, ColorStateList colorStateList3, int i2, ShapeAppearanceModel shapeAppearanceModel, Rect rect) {
        ResourcesFlusher.a(rect.left);
        ResourcesFlusher.a(rect.top);
        ResourcesFlusher.a(rect.right);
        ResourcesFlusher.a(rect.bottom);
        this.a = rect;
        this.b = colorStateList2;
        this.c = colorStateList;
        this.d = colorStateList3;
        this.f2358e = i2;
        this.f2359f = shapeAppearanceModel;
    }

    public static CalendarItemStyle a(Context context, int i2) {
        if (i2 != 0) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, l.MaterialCalendarItem);
            Rect rect = new Rect(obtainStyledAttributes.getDimensionPixelOffset(l.MaterialCalendarItem_android_insetLeft, 0), obtainStyledAttributes.getDimensionPixelOffset(l.MaterialCalendarItem_android_insetTop, 0), obtainStyledAttributes.getDimensionPixelOffset(l.MaterialCalendarItem_android_insetRight, 0), obtainStyledAttributes.getDimensionPixelOffset(l.MaterialCalendarItem_android_insetBottom, 0));
            ColorStateList a2 = c.a(context, obtainStyledAttributes, l.MaterialCalendarItem_itemFillColor);
            ColorStateList a3 = c.a(context, obtainStyledAttributes, l.MaterialCalendarItem_itemTextColor);
            ColorStateList a4 = c.a(context, obtainStyledAttributes, l.MaterialCalendarItem_itemStrokeColor);
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(l.MaterialCalendarItem_itemStrokeWidth, 0);
            ShapeAppearanceModel a5 = ShapeAppearanceModel.a(context, obtainStyledAttributes.getResourceId(l.MaterialCalendarItem_itemShapeAppearance, 0), obtainStyledAttributes.getResourceId(l.MaterialCalendarItem_itemShapeAppearanceOverlay, 0), new AbsoluteCornerSize((float) 0)).a();
            obtainStyledAttributes.recycle();
            return new CalendarItemStyle(a2, a3, a4, dimensionPixelSize, a5, rect);
        }
        throw new IllegalArgumentException("Cannot create a CalendarItemStyle with a styleResId of 0");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void}
     arg types: [android.graphics.drawable.RippleDrawable, int, int, int, int]
     candidates:
      ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, float, float, float, float):void}
      ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void} */
    public void a(TextView textView) {
        MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable();
        MaterialShapeDrawable materialShapeDrawable2 = new MaterialShapeDrawable();
        materialShapeDrawable.setShapeAppearanceModel(this.f2359f);
        materialShapeDrawable2.setShapeAppearanceModel(this.f2359f);
        materialShapeDrawable.a(this.c);
        materialShapeDrawable.a((float) this.f2358e, this.d);
        textView.setTextColor(this.b);
        RippleDrawable rippleDrawable = new RippleDrawable(this.b.withAlpha(30), materialShapeDrawable, materialShapeDrawable2);
        Rect rect = this.a;
        ViewCompat.a(textView, new InsetDrawable((Drawable) rippleDrawable, rect.left, rect.top, rect.right, rect.bottom));
    }
}
