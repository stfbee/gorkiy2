package j.c.a.b.e0;

import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Build;
import android.util.Log;
import i.h.f.ColorUtils;

public class RippleUtils {
    public static final boolean a = true;
    public static final int[] b = {16842919};
    public static final int[] c = {16843623, 16842908};
    public static final int[] d = {16842908};

    /* renamed from: e  reason: collision with root package name */
    public static final int[] f2202e = {16843623};

    /* renamed from: f  reason: collision with root package name */
    public static final int[] f2203f = {16842913, 16842919};
    public static final int[] g = {16842913, 16843623, 16842908};
    public static final int[] h = {16842913, 16842908};

    /* renamed from: i  reason: collision with root package name */
    public static final int[] f2204i = {16842913, 16843623};

    /* renamed from: j  reason: collision with root package name */
    public static final int[] f2205j = {16842913};

    /* renamed from: k  reason: collision with root package name */
    public static final int[] f2206k = {16842910, 16842919};

    /* renamed from: l  reason: collision with root package name */
    public static final String f2207l = RippleUtils.class.getSimpleName();

    public static ColorStateList a(ColorStateList colorStateList) {
        if (colorStateList == null) {
            return ColorStateList.valueOf(0);
        }
        int i2 = Build.VERSION.SDK_INT;
        if (i2 >= 22 && i2 <= 27 && Color.alpha(colorStateList.getDefaultColor()) == 0 && Color.alpha(colorStateList.getColorForState(f2206k, 0)) != 0) {
            Log.w(f2207l, "Use a non-transparent color for the default color as it will be used to finish ripple animations.");
        }
        return colorStateList;
    }

    public static boolean a(int[] iArr) {
        boolean z = false;
        boolean z2 = false;
        for (int i2 : iArr) {
            if (i2 == 16842910) {
                z = true;
            } else if (i2 == 16842908 || i2 == 16842919 || i2 == 16843623) {
                z2 = true;
            }
        }
        return z && z2;
    }

    public static int a(ColorStateList colorStateList, int[] iArr) {
        int colorForState = colorStateList != null ? colorStateList.getColorForState(iArr, colorStateList.getDefaultColor()) : 0;
        return ColorUtils.b(colorForState, Math.min(Color.alpha(colorForState) * 2, 255));
    }
}
