package j.c.a.b.x;

import android.os.Parcel;
import android.os.Parcelable;
import java.util.Arrays;

public final class CalendarConstraints implements Parcelable {
    public static final Parcelable.Creator<a> CREATOR = new a();
    public final Month b;
    public final Month c;
    public final Month d;

    /* renamed from: e  reason: collision with root package name */
    public final b f2356e;

    /* renamed from: f  reason: collision with root package name */
    public final int f2357f;
    public final int g;

    public static class a implements Parcelable.Creator<a> {
        public Object createFromParcel(Parcel parcel) {
            return new CalendarConstraints((Month) parcel.readParcelable(Month.class.getClassLoader()), (Month) parcel.readParcelable(Month.class.getClassLoader()), (Month) parcel.readParcelable(Month.class.getClassLoader()), (b) parcel.readParcelable(b.class.getClassLoader()), null);
        }

        public Object[] newArray(int i2) {
            return new CalendarConstraints[i2];
        }
    }

    public interface b extends Parcelable {
        boolean e(long j2);
    }

    public /* synthetic */ CalendarConstraints(Month month, Month month2, Month month3, b bVar, a aVar) {
        this.b = month;
        this.c = month2;
        this.d = month3;
        this.f2356e = bVar;
        if (month.b.compareTo(month3.b) > 0) {
            throw new IllegalArgumentException("start Month cannot be after current Month");
        } else if (month3.b.compareTo(month2.b) <= 0) {
            this.g = month.b(month2) + 1;
            this.f2357f = (month2.f2363e - month.f2363e) + 1;
        } else {
            throw new IllegalArgumentException("current Month cannot be after end Month");
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CalendarConstraints)) {
            return false;
        }
        CalendarConstraints calendarConstraints = (CalendarConstraints) obj;
        if (!this.b.equals(calendarConstraints.b) || !this.c.equals(calendarConstraints.c) || !this.d.equals(calendarConstraints.d) || !this.f2356e.equals(calendarConstraints.f2356e)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        return Arrays.hashCode(new Object[]{this.b, this.c, this.d, this.f2356e});
    }

    public void writeToParcel(Parcel parcel, int i2) {
        parcel.writeParcelable(this.b, 0);
        parcel.writeParcelable(this.c, 0);
        parcel.writeParcelable(this.d, 0);
        parcel.writeParcelable(this.f2356e, 0);
    }
}
