package j.c.a.b.n;

import android.animation.ValueAnimator;
import j.c.a.b.g0.MaterialShapeDrawable;

public class AppBarLayout implements ValueAnimator.AnimatorUpdateListener {
    public final /* synthetic */ MaterialShapeDrawable a;

    public AppBarLayout(com.google.android.material.appbar.AppBarLayout appBarLayout, MaterialShapeDrawable materialShapeDrawable) {
        this.a = materialShapeDrawable;
    }

    public void onAnimationUpdate(ValueAnimator valueAnimator) {
        this.a.a(((Float) valueAnimator.getAnimatedValue()).floatValue());
    }
}
