package j.c.a.b.k0;

import android.text.Editable;
import android.text.TextWatcher;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import com.google.android.material.textfield.TextInputLayout;
import i.b.l.a.AppCompatResources;
import j.c.a.b.e;
import j.c.a.b.j;

public class PasswordToggleEndIconDelegate extends EndIconDelegate {
    public final TextWatcher d = new a();

    /* renamed from: e  reason: collision with root package name */
    public final TextInputLayout.f f2290e = new b();

    /* renamed from: f  reason: collision with root package name */
    public final TextInputLayout.g f2291f = new c(this);

    public class a implements TextWatcher {
        public a() {
        }

        public void afterTextChanged(Editable editable) {
        }

        public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
            PasswordToggleEndIconDelegate passwordToggleEndIconDelegate = PasswordToggleEndIconDelegate.this;
            passwordToggleEndIconDelegate.c.setChecked(!PasswordToggleEndIconDelegate.a(passwordToggleEndIconDelegate));
        }

        public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        }
    }

    public class b implements TextInputLayout.f {
        public b() {
        }

        public void a(TextInputLayout textInputLayout) {
            EditText editText = textInputLayout.getEditText();
            textInputLayout.setEndIconVisible(true);
            PasswordToggleEndIconDelegate passwordToggleEndIconDelegate = PasswordToggleEndIconDelegate.this;
            passwordToggleEndIconDelegate.c.setChecked(!PasswordToggleEndIconDelegate.a(passwordToggleEndIconDelegate));
            editText.removeTextChangedListener(PasswordToggleEndIconDelegate.this.d);
            editText.addTextChangedListener(PasswordToggleEndIconDelegate.this.d);
        }
    }

    public class c implements TextInputLayout.g {
        public c(PasswordToggleEndIconDelegate passwordToggleEndIconDelegate) {
        }

        public void a(TextInputLayout textInputLayout, int i2) {
            EditText editText = textInputLayout.getEditText();
            if (editText != null && i2 == 1) {
                editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
            }
        }
    }

    public class d implements View.OnClickListener {
        public d() {
        }

        public void onClick(View view) {
            EditText editText = PasswordToggleEndIconDelegate.this.a.getEditText();
            if (editText != null) {
                int selectionEnd = editText.getSelectionEnd();
                if (PasswordToggleEndIconDelegate.a(PasswordToggleEndIconDelegate.this)) {
                    editText.setTransformationMethod(null);
                } else {
                    editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
                }
                if (selectionEnd >= 0) {
                    editText.setSelection(selectionEnd);
                }
            }
        }
    }

    public PasswordToggleEndIconDelegate(TextInputLayout textInputLayout) {
        super(textInputLayout);
    }

    public void a() {
        super.a.setEndIconDrawable(AppCompatResources.c(super.b, e.design_password_eye));
        TextInputLayout textInputLayout = super.a;
        textInputLayout.setEndIconContentDescription(textInputLayout.getResources().getText(j.password_toggle_content_description));
        super.a.setEndIconOnClickListener(new d());
        super.a.a(this.f2290e);
        super.a.g0.add(this.f2291f);
        EditText editText = super.a.getEditText();
        if (editText != null && (editText.getInputType() == 16 || editText.getInputType() == 128 || editText.getInputType() == 144 || editText.getInputType() == 224)) {
            editText.setTransformationMethod(PasswordTransformationMethod.getInstance());
        }
    }

    public static /* synthetic */ boolean a(PasswordToggleEndIconDelegate passwordToggleEndIconDelegate) {
        EditText editText = super.a.getEditText();
        return editText != null && (editText.getTransformationMethod() instanceof PasswordTransformationMethod);
    }
}
