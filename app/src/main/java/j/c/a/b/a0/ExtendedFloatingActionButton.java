package j.c.a.b.a0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;

public class ExtendedFloatingActionButton extends AnimatorListenerAdapter {
    public boolean a;
    public final /* synthetic */ MotionStrategy b;
    public final /* synthetic */ ExtendedFloatingActionButton.c c = null;

    public ExtendedFloatingActionButton(com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton extendedFloatingActionButton, MotionStrategy motionStrategy) {
        this.b = motionStrategy;
    }

    public void onAnimationCancel(Animator animator) {
        this.a = true;
        this.b.c();
    }

    public void onAnimationEnd(Animator animator) {
        this.b.b();
        if (!this.a) {
            this.b.a(null);
        }
    }

    public void onAnimationStart(Animator animator) {
        this.b.onAnimationStart(animator);
        this.a = false;
    }
}
