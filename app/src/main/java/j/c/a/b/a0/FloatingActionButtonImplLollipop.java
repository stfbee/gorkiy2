package j.c.a.b.a0;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.StateListAnimator;
import android.content.res.ColorStateList;
import android.graphics.Rect;
import android.os.Build;
import android.view.View;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import j.c.a.b.f0.ShadowViewDelegate;
import java.util.ArrayList;

public class FloatingActionButtonImplLollipop extends FloatingActionButtonImpl1 {
    public FloatingActionButtonImplLollipop(FloatingActionButton floatingActionButton, ShadowViewDelegate shadowViewDelegate) {
        super(floatingActionButton, shadowViewDelegate);
    }

    public void a(float f2, float f3, float f4) {
        if (Build.VERSION.SDK_INT == 21) {
            super.f2162t.refreshDrawableState();
        } else {
            StateListAnimator stateListAnimator = new StateListAnimator();
            stateListAnimator.addState(FloatingActionButtonImpl1.B, a(f2, f4));
            stateListAnimator.addState(FloatingActionButtonImpl1.C, a(f2, f3));
            stateListAnimator.addState(FloatingActionButtonImpl1.D, a(f2, f3));
            stateListAnimator.addState(FloatingActionButtonImpl1.E, a(f2, f3));
            AnimatorSet animatorSet = new AnimatorSet();
            ArrayList arrayList = new ArrayList();
            arrayList.add(ObjectAnimator.ofFloat(super.f2162t, "elevation", f2).setDuration(0L));
            int i2 = Build.VERSION.SDK_INT;
            if (i2 >= 22 && i2 <= 24) {
                FloatingActionButton floatingActionButton = super.f2162t;
                arrayList.add(ObjectAnimator.ofFloat(floatingActionButton, View.TRANSLATION_Z, floatingActionButton.getTranslationZ()).setDuration(100L));
            }
            arrayList.add(ObjectAnimator.ofFloat(super.f2162t, View.TRANSLATION_Z, 0.0f).setDuration(100L));
            animatorSet.playSequentially((Animator[]) arrayList.toArray(new Animator[0]));
            animatorSet.setInterpolator(FloatingActionButtonImpl1.A);
            stateListAnimator.addState(FloatingActionButtonImpl1.F, animatorSet);
            stateListAnimator.addState(FloatingActionButtonImpl1.G, a(0.0f, 0.0f));
            super.f2162t.setStateListAnimator(stateListAnimator);
        }
        if (i()) {
            m();
        }
    }

    public void a(ColorStateList colorStateList) {
    }

    public void d() {
    }

    public void e() {
        m();
    }

    public boolean h() {
        return false;
    }

    public boolean i() {
        return FloatingActionButton.this.f477k || !k();
    }

    public void l() {
    }

    public final Animator a(float f2, float f3) {
        AnimatorSet animatorSet = new AnimatorSet();
        animatorSet.play(ObjectAnimator.ofFloat(super.f2162t, "elevation", f2).setDuration(0L)).with(ObjectAnimator.ofFloat(super.f2162t, View.TRANSLATION_Z, f3).setDuration(100L));
        animatorSet.setInterpolator(FloatingActionButtonImpl1.A);
        return animatorSet;
    }

    public float a() {
        return super.f2162t.getElevation();
    }

    public void a(int[] iArr) {
        if (Build.VERSION.SDK_INT != 21) {
            return;
        }
        if (super.f2162t.isEnabled()) {
            super.f2162t.setElevation(super.f2149e);
            if (super.f2162t.isPressed()) {
                super.f2162t.setTranslationZ(super.g);
            } else if (super.f2162t.isFocused() || super.f2162t.isHovered()) {
                super.f2162t.setTranslationZ(super.f2150f);
            } else {
                super.f2162t.setTranslationZ(0.0f);
            }
        } else {
            super.f2162t.setElevation(0.0f);
            super.f2162t.setTranslationZ(0.0f);
        }
    }

    public void a(Rect rect) {
        if (FloatingActionButton.this.f477k) {
            super.a(rect);
        } else if (!k()) {
            int sizeDimension = (0 - super.f2162t.getSizeDimension()) / 2;
            rect.set(sizeDimension, sizeDimension, sizeDimension, sizeDimension);
        } else {
            rect.set(0, 0, 0, 0);
        }
    }
}
