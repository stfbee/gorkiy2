package j.c.a.b.n0;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import com.google.android.material.transformation.FabTransformationBehavior;
import j.c.a.b.w.CircularRevealWidget;

/* compiled from: FabTransformationBehavior */
public class FabTransformationBehavior1 extends AnimatorListenerAdapter {
    public final /* synthetic */ CircularRevealWidget a;

    public FabTransformationBehavior1(FabTransformationBehavior fabTransformationBehavior, CircularRevealWidget circularRevealWidget) {
        this.a = circularRevealWidget;
    }

    public void onAnimationEnd(Animator animator) {
        CircularRevealWidget.e revealInfo = this.a.getRevealInfo();
        revealInfo.c = Float.MAX_VALUE;
        this.a.setRevealInfo(revealInfo);
    }
}
