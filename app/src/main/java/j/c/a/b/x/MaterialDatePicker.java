package j.c.a.b.x;

import android.content.Context;
import android.content.res.TypedArray;
import i.l.a.c;
import j.c.a.b.b;

public final class MaterialDatePicker<S> extends c {
    public static boolean b(Context context) {
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(j.c.a.a.c.n.c.a(context, b.materialCalendarStyle, MaterialCalendar.class.getCanonicalName()), new int[]{16843277});
        boolean z = obtainStyledAttributes.getBoolean(0, false);
        obtainStyledAttributes.recycle();
        return z;
    }
}
