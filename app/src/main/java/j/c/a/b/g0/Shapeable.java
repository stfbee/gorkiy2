package j.c.a.b.g0;

public interface Shapeable {
    void setShapeAppearanceModel(ShapeAppearanceModel shapeAppearanceModel);
}
