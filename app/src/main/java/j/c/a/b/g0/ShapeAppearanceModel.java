package j.c.a.b.g0;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ContextThemeWrapper;
import j.c.a.a.c.n.c;
import j.c.a.b.l;

public class ShapeAppearanceModel {
    public CornerTreatment a;
    public CornerTreatment b;
    public CornerTreatment c;
    public CornerTreatment d;

    /* renamed from: e  reason: collision with root package name */
    public CornerSize f2242e;

    /* renamed from: f  reason: collision with root package name */
    public CornerSize f2243f;
    public CornerSize g;
    public CornerSize h;

    /* renamed from: i  reason: collision with root package name */
    public EdgeTreatment f2244i;

    /* renamed from: j  reason: collision with root package name */
    public EdgeTreatment f2245j;

    /* renamed from: k  reason: collision with root package name */
    public EdgeTreatment f2246k;

    /* renamed from: l  reason: collision with root package name */
    public EdgeTreatment f2247l;

    public static final class b {
        public CornerTreatment a = new RoundedCornerTreatment();
        public CornerTreatment b = new RoundedCornerTreatment();
        public CornerTreatment c = new RoundedCornerTreatment();
        public CornerTreatment d = new RoundedCornerTreatment();

        /* renamed from: e  reason: collision with root package name */
        public CornerSize f2248e = new AbsoluteCornerSize(0.0f);

        /* renamed from: f  reason: collision with root package name */
        public CornerSize f2249f = new AbsoluteCornerSize(0.0f);
        public CornerSize g = new AbsoluteCornerSize(0.0f);
        public CornerSize h = new AbsoluteCornerSize(0.0f);

        /* renamed from: i  reason: collision with root package name */
        public EdgeTreatment f2250i = new EdgeTreatment();

        /* renamed from: j  reason: collision with root package name */
        public EdgeTreatment f2251j = new EdgeTreatment();

        /* renamed from: k  reason: collision with root package name */
        public EdgeTreatment f2252k = new EdgeTreatment();

        /* renamed from: l  reason: collision with root package name */
        public EdgeTreatment f2253l = new EdgeTreatment();

        public b() {
        }

        public b a(float f2) {
            this.h = new AbsoluteCornerSize(f2);
            return this;
        }

        public b b(float f2) {
            this.g = new AbsoluteCornerSize(f2);
            return this;
        }

        public b c(float f2) {
            this.f2248e = new AbsoluteCornerSize(f2);
            return this;
        }

        public b d(float f2) {
            this.f2249f = new AbsoluteCornerSize(f2);
            return this;
        }

        public static float a(CornerTreatment cornerTreatment) {
            if (cornerTreatment instanceof RoundedCornerTreatment) {
                return ((RoundedCornerTreatment) cornerTreatment).a;
            }
            if (cornerTreatment instanceof CutCornerTreatment) {
                return ((CutCornerTreatment) cornerTreatment).a;
            }
            return -1.0f;
        }

        public ShapeAppearanceModel a() {
            return new ShapeAppearanceModel(this, null);
        }

        public b(ShapeAppearanceModel shapeAppearanceModel) {
            this.a = shapeAppearanceModel.a;
            this.b = shapeAppearanceModel.b;
            this.c = shapeAppearanceModel.c;
            this.d = shapeAppearanceModel.d;
            this.f2248e = shapeAppearanceModel.f2242e;
            this.f2249f = shapeAppearanceModel.f2243f;
            this.g = shapeAppearanceModel.g;
            this.h = shapeAppearanceModel.h;
            this.f2250i = shapeAppearanceModel.f2244i;
            this.f2251j = shapeAppearanceModel.f2245j;
            this.f2252k = shapeAppearanceModel.f2246k;
            this.f2253l = shapeAppearanceModel.f2247l;
        }
    }

    public /* synthetic */ ShapeAppearanceModel(b bVar, a aVar) {
        this.a = bVar.a;
        this.b = bVar.b;
        this.c = bVar.c;
        this.d = bVar.d;
        this.f2242e = bVar.f2248e;
        this.f2243f = bVar.f2249f;
        this.g = bVar.g;
        this.h = bVar.h;
        this.f2244i = bVar.f2250i;
        this.f2245j = bVar.f2251j;
        this.f2246k = bVar.f2252k;
        this.f2247l = bVar.f2253l;
    }

    public static b a(Context context, AttributeSet attributeSet, int i2, int i3) {
        AbsoluteCornerSize absoluteCornerSize = new AbsoluteCornerSize((float) 0);
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.MaterialShape, i2, i3);
        int resourceId = obtainStyledAttributes.getResourceId(l.MaterialShape_shapeAppearance, 0);
        int resourceId2 = obtainStyledAttributes.getResourceId(l.MaterialShape_shapeAppearanceOverlay, 0);
        obtainStyledAttributes.recycle();
        return a(context, resourceId, resourceId2, absoluteCornerSize);
    }

    public static b a(Context context, int i2, int i3, CornerSize cornerSize) {
        if (i3 != 0) {
            ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(context, i2);
            i2 = i3;
            context = contextThemeWrapper;
        }
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(i2, l.ShapeAppearance);
        try {
            int i4 = obtainStyledAttributes.getInt(l.ShapeAppearance_cornerFamily, 0);
            int i5 = obtainStyledAttributes.getInt(l.ShapeAppearance_cornerFamilyTopLeft, i4);
            int i6 = obtainStyledAttributes.getInt(l.ShapeAppearance_cornerFamilyTopRight, i4);
            int i7 = obtainStyledAttributes.getInt(l.ShapeAppearance_cornerFamilyBottomRight, i4);
            int i8 = obtainStyledAttributes.getInt(l.ShapeAppearance_cornerFamilyBottomLeft, i4);
            CornerSize a2 = a(obtainStyledAttributes, l.ShapeAppearance_cornerSize, cornerSize);
            CornerSize a3 = a(obtainStyledAttributes, l.ShapeAppearance_cornerSizeTopLeft, a2);
            CornerSize a4 = a(obtainStyledAttributes, l.ShapeAppearance_cornerSizeTopRight, a2);
            CornerSize a5 = a(obtainStyledAttributes, l.ShapeAppearance_cornerSizeBottomRight, a2);
            CornerSize a6 = a(obtainStyledAttributes, l.ShapeAppearance_cornerSizeBottomLeft, a2);
            b bVar = new b();
            CornerTreatment a7 = c.a(i5);
            bVar.a = a7;
            float a8 = b.a(a7);
            if (a8 != -1.0f) {
                bVar.c(a8);
            }
            bVar.f2248e = a3;
            CornerTreatment a9 = c.a(i6);
            bVar.b = a9;
            float a10 = b.a(a9);
            if (a10 != -1.0f) {
                bVar.d(a10);
            }
            bVar.f2249f = a4;
            CornerTreatment a11 = c.a(i7);
            bVar.c = a11;
            float a12 = b.a(a11);
            if (a12 != -1.0f) {
                bVar.b(a12);
            }
            bVar.g = a5;
            CornerTreatment a13 = c.a(i8);
            bVar.d = a13;
            float a14 = b.a(a13);
            if (a14 != -1.0f) {
                bVar.a(a14);
            }
            bVar.h = a6;
            return bVar;
        } finally {
            obtainStyledAttributes.recycle();
        }
    }

    public ShapeAppearanceModel() {
        this.a = new RoundedCornerTreatment();
        this.b = new RoundedCornerTreatment();
        this.c = new RoundedCornerTreatment();
        this.d = new RoundedCornerTreatment();
        this.f2242e = new AbsoluteCornerSize(0.0f);
        this.f2243f = new AbsoluteCornerSize(0.0f);
        this.g = new AbsoluteCornerSize(0.0f);
        this.h = new AbsoluteCornerSize(0.0f);
        this.f2244i = new EdgeTreatment();
        this.f2245j = new EdgeTreatment();
        this.f2246k = new EdgeTreatment();
        this.f2247l = new EdgeTreatment();
    }

    public static CornerSize a(TypedArray typedArray, int i2, CornerSize cornerSize) {
        TypedValue peekValue = typedArray.peekValue(i2);
        if (peekValue == null) {
            return cornerSize;
        }
        int i3 = peekValue.type;
        if (i3 == 5) {
            return new AbsoluteCornerSize((float) TypedValue.complexToDimensionPixelSize(peekValue.data, typedArray.getResources().getDisplayMetrics()));
        }
        return i3 == 6 ? new RelativeCornerSize(peekValue.getFraction(1.0f, 1.0f)) : cornerSize;
    }

    public ShapeAppearanceModel a(float f2) {
        b bVar = new b(this);
        bVar.c(f2);
        bVar.d(f2);
        bVar.b(f2);
        bVar.a(f2);
        return bVar.a();
    }

    public boolean a(RectF rectF) {
        Class<EdgeTreatment> cls = EdgeTreatment.class;
        boolean z = this.f2247l.getClass().equals(cls) && this.f2245j.getClass().equals(cls) && this.f2244i.getClass().equals(cls) && this.f2246k.getClass().equals(cls);
        float a2 = this.f2242e.a(rectF);
        boolean z2 = this.f2243f.a(rectF) == a2 && this.h.a(rectF) == a2 && this.g.a(rectF) == a2;
        boolean z3 = (this.b instanceof RoundedCornerTreatment) && (this.a instanceof RoundedCornerTreatment) && (this.c instanceof RoundedCornerTreatment) && (this.d instanceof RoundedCornerTreatment);
        if (!z || !z2 || !z3) {
            return false;
        }
        return true;
    }
}
