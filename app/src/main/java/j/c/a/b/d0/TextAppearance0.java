package j.c.a.b.d0;

import android.graphics.Typeface;
import android.text.TextPaint;

/* compiled from: TextAppearance */
public class TextAppearance0 extends TextAppearanceFontCallback {
    public final /* synthetic */ TextPaint a;
    public final /* synthetic */ TextAppearanceFontCallback b;
    public final /* synthetic */ TextAppearance c;

    public TextAppearance0(TextAppearance textAppearance, TextPaint textPaint, TextAppearanceFontCallback textAppearanceFontCallback) {
        this.c = textAppearance;
        this.a = textPaint;
        this.b = super;
    }

    public void a(Typeface typeface, boolean z) {
        this.c.a(this.a, typeface);
        this.b.a(typeface, z);
    }

    public void a(int i2) {
        this.b.a(i2);
    }
}
