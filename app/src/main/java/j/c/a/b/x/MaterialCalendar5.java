package j.c.a.b.x;

import androidx.recyclerview.widget.RecyclerView;
import com.google.android.material.button.MaterialButton;

/* compiled from: MaterialCalendar */
public class MaterialCalendar5 extends RecyclerView.t {
    public final /* synthetic */ MonthsPagerAdapter0 a;
    public final /* synthetic */ MaterialButton b;
    public final /* synthetic */ MaterialCalendar c;

    public MaterialCalendar5(MaterialCalendar materialCalendar, MonthsPagerAdapter0 monthsPagerAdapter0, MaterialButton materialButton) {
        this.c = materialCalendar;
        this.a = monthsPagerAdapter0;
        this.b = materialButton;
    }

    public void a(RecyclerView recyclerView, int i2, int i3) {
        int i4;
        if (i2 < 0) {
            i4 = this.c.N().s();
        } else {
            i4 = this.c.N().t();
        }
        this.c.b0 = this.a.b(i4);
        this.b.setText(this.a.c.b.a(i4).c);
    }

    public void a(RecyclerView recyclerView, int i2) {
        if (i2 == 0) {
            recyclerView.announceForAccessibility(this.b.getText());
        }
    }
}
