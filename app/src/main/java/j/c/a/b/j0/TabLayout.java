package j.c.a.b.j0;

import android.view.View;
import com.google.android.material.tabs.TabLayout;

public class TabLayout implements View.OnLayoutChangeListener {
    public final /* synthetic */ View a;
    public final /* synthetic */ TabLayout.i b;

    public TabLayout(TabLayout.i iVar, View view) {
        this.b = iVar;
        this.a = view;
    }

    public void onLayoutChange(View view, int i2, int i3, int i4, int i5, int i6, int i7, int i8, int i9) {
        if (this.a.getVisibility() == 0) {
            this.b.b(this.a);
        }
    }
}
