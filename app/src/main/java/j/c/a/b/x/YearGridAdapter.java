package j.c.a.b.x;

import android.view.View;
import j.c.a.a.c.n.c;
import j.c.a.b.x.MaterialCalendar;
import java.util.Calendar;

public class YearGridAdapter implements View.OnClickListener {
    public final /* synthetic */ int b;
    public final /* synthetic */ YearGridAdapter0 c;

    public YearGridAdapter(YearGridAdapter0 yearGridAdapter0, int i2) {
        this.c = yearGridAdapter0;
        this.b = i2;
    }

    public void onClick(View view) {
        int i2 = this.b;
        int i3 = this.c.c.b0.d;
        Calendar c2 = c.c();
        c2.set(1, i2);
        c2.set(2, i3);
        Month month = new Month(c2);
        CalendarConstraints calendarConstraints = this.c.c.a0;
        if (month.compareTo(calendarConstraints.b) < 0) {
            month = calendarConstraints.b;
        } else if (month.compareTo(calendarConstraints.c) > 0) {
            month = calendarConstraints.c;
        }
        this.c.c.a(month);
        this.c.c.a(MaterialCalendar.e.DAY);
    }
}
