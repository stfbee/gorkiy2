package j.c.b.a.u;

import j.c.b.a.a0.SignatureConfig;
import j.c.b.a.b0.StreamingAeadConfig;
import j.c.b.a.v.DeterministicAeadConfig;
import j.c.b.a.w.HybridConfig;
import j.c.b.a.z.RegistryConfig;

public final class TinkConfig {
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.a(HybridConfig.a);
        RegistryConfig.b bVar = g;
        bVar.a(SignatureConfig.a);
        RegistryConfig.b bVar2 = bVar;
        bVar2.m();
        RegistryConfig.a((RegistryConfig) bVar2.c, "TINK_1_0_0");
        RegistryConfig registryConfig = (RegistryConfig) bVar2.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(HybridConfig.b);
        RegistryConfig.b bVar3 = g2;
        bVar3.a(SignatureConfig.b);
        RegistryConfig.b bVar4 = bVar3;
        bVar4.a(DeterministicAeadConfig.a);
        RegistryConfig.b bVar5 = bVar4;
        bVar5.a(StreamingAeadConfig.a);
        RegistryConfig.b bVar6 = bVar5;
        bVar6.m();
        RegistryConfig.a((RegistryConfig) bVar6.c, "TINK_1_1_0");
        RegistryConfig registryConfig2 = (RegistryConfig) bVar6.k();
        RegistryConfig.b g3 = RegistryConfig.g();
        g3.a(HybridConfig.c);
        RegistryConfig.b bVar7 = g3;
        bVar7.a(SignatureConfig.c);
        RegistryConfig.b bVar8 = bVar7;
        bVar8.a(DeterministicAeadConfig.b);
        RegistryConfig.b bVar9 = bVar8;
        bVar9.a(StreamingAeadConfig.b);
        RegistryConfig.b bVar10 = bVar9;
        bVar10.m();
        RegistryConfig.a((RegistryConfig) bVar10.c, "TINK");
        RegistryConfig registryConfig3 = (RegistryConfig) bVar10.k();
    }

    public static void a() {
        DeterministicAeadConfig.a();
        HybridConfig.a();
        SignatureConfig.a();
    }
}
