package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.EcdsaParams;
import j.c.b.a.z.t0;
import j.c.e.ByteString;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class EcdsaPublicKey extends GeneratedMessageLite<t0, t0.b> implements u0 {

    /* renamed from: i  reason: collision with root package name */
    public static final EcdsaPublicKey f2442i;

    /* renamed from: j  reason: collision with root package name */
    public static volatile Parser<t0> f2443j;

    /* renamed from: e  reason: collision with root package name */
    public int f2444e;

    /* renamed from: f  reason: collision with root package name */
    public EcdsaParams f2445f;
    public ByteString g;
    public ByteString h;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.EcdsaPublicKey.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.EcdsaPublicKey.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.EcdsaPublicKey.a.<clinit>():void");
        }
    }

    static {
        EcdsaPublicKey ecdsaPublicKey = new EcdsaPublicKey();
        f2442i = ecdsaPublicKey;
        super.f();
    }

    public EcdsaPublicKey() {
        ByteString byteString = ByteString.c;
        this.g = byteString;
        this.h = byteString;
    }

    public static /* synthetic */ void a(EcdsaPublicKey ecdsaPublicKey, EcdsaParams ecdsaParams) {
        if (ecdsaParams != null) {
            ecdsaPublicKey.f2445f = ecdsaParams;
            return;
        }
        throw null;
    }

    public static /* synthetic */ void b(EcdsaPublicKey ecdsaPublicKey, ByteString byteString) {
        if (byteString != null) {
            ecdsaPublicKey.h = byteString;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaParams] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        int i4 = this.f2444e;
        if (i4 != 0) {
            i3 = 0 + CodedOutputStream.d(1, i4);
        }
        if (this.f2445f != null) {
            i3 += CodedOutputStream.b(2, (MessageLite) g());
        }
        if (!this.g.isEmpty()) {
            i3 += CodedOutputStream.b(3, this.g);
        }
        if (!this.h.isEmpty()) {
            i3 += CodedOutputStream.b(4, this.h);
        }
        super.d = i3;
        return i3;
    }

    public EcdsaParams g() {
        EcdsaParams ecdsaParams = this.f2445f;
        return ecdsaParams == null ? EcdsaParams.h : ecdsaParams;
    }

    public static final class b extends GeneratedMessageLite.b<t0, t0.b> implements u0 {
        public b() {
            super(EcdsaPublicKey.f2442i);
        }

        public /* synthetic */ b(a aVar) {
            super(EcdsaPublicKey.f2442i);
        }
    }

    public static /* synthetic */ void a(EcdsaPublicKey ecdsaPublicKey, ByteString byteString) {
        if (byteString != null) {
            ecdsaPublicKey.g = byteString;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaParams] */
    public void a(CodedOutputStream codedOutputStream) {
        int i2 = this.f2444e;
        if (i2 != 0) {
            codedOutputStream.b(1, i2);
        }
        if (this.f2445f != null) {
            codedOutputStream.a(2, (MessageLite) g());
        }
        if (!this.g.isEmpty()) {
            codedOutputStream.a(3, this.g);
        }
        if (!this.h.isEmpty()) {
            codedOutputStream.a(4, this.h);
        }
    }

    /* JADX WARN: Type inference failed for: r6v6, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaParams] */
    /* JADX WARN: Type inference failed for: r2v2, types: [j.c.e.MessageLite, j.c.b.a.z.EcdsaParams] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return f2442i;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                EcdsaPublicKey ecdsaPublicKey = (EcdsaPublicKey) obj2;
                this.f2444e = kVar.a(this.f2444e != 0, this.f2444e, ecdsaPublicKey.f2444e != 0, ecdsaPublicKey.f2444e);
                this.f2445f = (EcdsaParams) kVar.a((MessageLite) this.f2445f, (MessageLite) ecdsaPublicKey.f2445f);
                this.g = kVar.a(this.g != ByteString.c, this.g, ecdsaPublicKey.g != ByteString.c, ecdsaPublicKey.g);
                boolean z2 = this.h != ByteString.c;
                ByteString byteString = this.h;
                if (ecdsaPublicKey.h != ByteString.c) {
                    z = true;
                }
                this.h = kVar.a(z2, byteString, z, ecdsaPublicKey.h);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 8) {
                                this.f2444e = codedInputStream.d();
                            } else if (g2 == 18) {
                                EcdsaParams.b bVar = this.f2445f != null ? (EcdsaParams.b) this.f2445f.e() : null;
                                EcdsaParams ecdsaParams = (EcdsaParams) codedInputStream.a(EcdsaParams.h.i(), extensionRegistryLite);
                                this.f2445f = ecdsaParams;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2445f = (EcdsaParams) bVar.l();
                                }
                            } else if (g2 == 26) {
                                this.g = codedInputStream.b();
                            } else if (g2 == 34) {
                                this.h = codedInputStream.b();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new EcdsaPublicKey();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2443j == null) {
                    synchronized (EcdsaPublicKey.class) {
                        if (f2443j == null) {
                            f2443j = new GeneratedMessageLite.c(f2442i);
                        }
                    }
                }
                return f2443j;
            default:
                throw new UnsupportedOperationException();
        }
        return f2442i;
    }
}
