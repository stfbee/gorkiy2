package j.c.b.a.c0;

import j.c.a.a.c.n.c;
import j.c.b.a.c0.EngineWrapper;
import j.c.b.a.c0.z;
import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.KeyPairGenerator;
import java.security.MessageDigest;
import java.security.Provider;
import java.security.Security;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;
import javax.crypto.Cipher;
import javax.crypto.Mac;

public final class EngineFactory<T_WRAPPER extends EngineWrapper<T_ENGINE>, T_ENGINE> {
    public static final Logger d = Logger.getLogger(EngineFactory.class.getName());

    /* renamed from: e  reason: collision with root package name */
    public static final List<Provider> f2381e;

    /* renamed from: f  reason: collision with root package name */
    public static final EngineFactory<z.a, Cipher> f2382f = new EngineFactory<>(new EngineWrapper.a());
    public static final EngineFactory<z.e, Mac> g = new EngineFactory<>(new EngineWrapper.e());
    public static final EngineFactory<z.f, MessageDigest> h = new EngineFactory<>(new EngineWrapper.f());

    /* renamed from: i  reason: collision with root package name */
    public static final EngineFactory<z.d, KeyPairGenerator> f2383i = new EngineFactory<>(new EngineWrapper.d());

    /* renamed from: j  reason: collision with root package name */
    public static final EngineFactory<z.c, KeyFactory> f2384j = new EngineFactory<>(new EngineWrapper.c());
    public T_WRAPPER a;
    public List<Provider> b = f2381e;
    public boolean c = true;

    static {
        if (c.d()) {
            String[] strArr = {"GmsCore_OpenSSL", "AndroidOpenSSL"};
            ArrayList arrayList = new ArrayList();
            for (int i2 = 0; i2 < 2; i2++) {
                String str = strArr[i2];
                Provider provider = Security.getProvider(str);
                if (provider != null) {
                    arrayList.add(provider);
                } else {
                    d.info(String.format("Provider %s not available", str));
                }
            }
            f2381e = arrayList;
        } else {
            f2381e = new ArrayList();
        }
        new EngineFactory(new EngineWrapper.g());
        new EngineFactory(new EngineWrapper.b());
    }

    public EngineFactory(T_WRAPPER t_wrapper) {
        this.a = t_wrapper;
    }

    public T_ENGINE a(String str) {
        boolean z;
        for (Provider next : this.b) {
            try {
                this.a.a(str, next);
                z = true;
                continue;
            } catch (Exception e2) {
                e2.printStackTrace();
                z = false;
                continue;
            }
            if (z) {
                return this.a.a(str, next);
            }
        }
        if (this.c) {
            return this.a.a(str, null);
        }
        throw new GeneralSecurityException("No good Provider found.");
    }
}
