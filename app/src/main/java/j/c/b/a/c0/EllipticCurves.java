package j.c.b.a.c0;

public enum EllipticCurves {
    NIST_P256,
    NIST_P384,
    NIST_P521
}
