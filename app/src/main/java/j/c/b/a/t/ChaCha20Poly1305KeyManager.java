package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.Aead;
import j.c.b.a.KeyManager;
import j.c.b.a.a;
import j.c.b.a.c0.ChaCha20Poly1305;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.ChaCha20Poly1305Key;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class ChaCha20Poly1305KeyManager implements KeyManager<a> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.ChaCha20Poly1305Key, j.c.e.MessageLite] */
    public MessageLite b(MessageLite messageLite) {
        return c();
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.b.a.z.ChaCha20Poly1305Key, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.ChaCha20Poly1305Key, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((ChaCha20Poly1305Key) GeneratedMessageLite.a((GeneratedMessageLite) ChaCha20Poly1305Key.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("invalid ChaCha20Poly1305 key", e2);
        }
    }

    public Aead a(MessageLite messageLite) {
        if (messageLite instanceof ChaCha20Poly1305Key) {
            ChaCha20Poly1305Key chaCha20Poly1305Key = (ChaCha20Poly1305Key) messageLite;
            Validators.a(chaCha20Poly1305Key.f2432e, 0);
            if (chaCha20Poly1305Key.f2433f.size() == 32) {
                return new ChaCha20Poly1305(chaCha20Poly1305Key.f2433f.d());
            }
            throw new GeneralSecurityException("invalid ChaCha20Poly1305Key: incorrect key length");
        }
        throw new GeneralSecurityException("expected ChaCha20Poly1305Key proto");
    }

    public KeyData b(ByteString byteString) {
        ChaCha20Poly1305Key c = c();
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key");
        ByteString a = c.a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }

    public final ChaCha20Poly1305Key c() {
        ChaCha20Poly1305Key.b bVar = (ChaCha20Poly1305Key.b) ChaCha20Poly1305Key.g.e();
        bVar.m();
        ((ChaCha20Poly1305Key) bVar.c).f2432e = 0;
        ByteString a = ByteString.a(Random.a(32));
        bVar.m();
        ChaCha20Poly1305Key.a((ChaCha20Poly1305Key) bVar.c, a);
        return (ChaCha20Poly1305Key) bVar.k();
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.ChaCha20Poly1305Key, j.c.e.MessageLite] */
    public MessageLite a(ByteString byteString) {
        return c();
    }

    public boolean a(String str) {
        return "type.googleapis.com/google.crypto.tink.ChaCha20Poly1305Key".equals(str);
    }
}
