package j.c.b.a.w;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.f;
import java.security.GeneralSecurityException;

public class HybridEncryptCatalogue implements Catalogue<f> {
    public KeyManager<f> a(String str, String str2, int i2) {
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 1420614889 && lowerCase.equals("hybridencrypt")) ? (char) 0 : 65535) == 0) {
            if (str.hashCode() == 396454335 && str.equals("type.googleapis.com/google.crypto.tink.EciesAeadHkdfPublicKey")) {
                c = 0;
            }
            if (c == 0) {
                EciesAeadHkdfPublicKeyManager eciesAeadHkdfPublicKeyManager = new EciesAeadHkdfPublicKeyManager();
                if (eciesAeadHkdfPublicKeyManager.b() >= i2) {
                    return eciesAeadHkdfPublicKeyManager;
                }
                throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
            }
            throw new GeneralSecurityException(String.format("No support for primitive 'HybridEncrypt' with key type '%s'.", str));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
