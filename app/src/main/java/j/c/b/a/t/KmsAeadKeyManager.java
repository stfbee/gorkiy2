package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.Aead;
import j.c.b.a.KeyManager;
import j.c.b.a.KmsClients;
import j.c.b.a.a;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.KeyData;
import j.c.b.a.z.KmsAeadKey;
import j.c.b.a.z.KmsAeadKeyFormat;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class KmsAeadKeyManager implements KeyManager<a> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.KmsAeadKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r3v4, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof KmsAeadKeyFormat) {
            KmsAeadKey.b bVar = (KmsAeadKey.b) KmsAeadKey.g.e();
            bVar.m();
            KmsAeadKey.a(bVar.c, (KmsAeadKeyFormat) messageLite);
            bVar.m();
            bVar.c.f2502e = 0;
            return bVar.k();
        }
        throw new GeneralSecurityException("expected KmsAeadKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.KmsAeadKey] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.KmsAeadKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((KmsAeadKey) GeneratedMessageLite.a((GeneratedMessageLite) KmsAeadKey.g, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected KmsAeadKey proto", e2);
        }
    }

    public Aead a(MessageLite messageLite) {
        if (messageLite instanceof KmsAeadKey) {
            KmsAeadKey kmsAeadKey = (KmsAeadKey) messageLite;
            Validators.a(kmsAeadKey.f2502e, 0);
            String str = kmsAeadKey.g().f2505e;
            return KmsClients.a(str).b(str);
        }
        throw new GeneralSecurityException("expected KmsAeadKey proto");
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.KmsAeadKey");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.KmsAeadKey");
        ByteString a = ((KmsAeadKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.REMOTE);
        return (KeyData) g.k();
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.KmsAeadKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.KmsAeadKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((KmsAeadKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) KmsAeadKeyFormat.f2504f, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized KmsAeadKeyFormat proto", e2);
        }
    }
}
