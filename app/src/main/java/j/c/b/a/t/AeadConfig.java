package j.c.b.a.t;

import j.c.a.a.c.n.c;
import j.c.b.a.Registry;
import j.c.b.a.y.MacConfig;
import j.c.b.a.z.RegistryConfig;
import java.security.GeneralSecurityException;

public final class AeadConfig {
    @Deprecated
    public static final RegistryConfig a;
    public static final RegistryConfig b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      j.c.a.a.c.n.c.a(int, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(j.c.a.a.f.e.t5, byte[], int, int, j.c.a.a.f.e.s2):int
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String):java.security.spec.ECParameterSpec
      j.c.a.a.c.n.c.a(byte, byte, byte, char[], int):void
      j.c.a.a.c.n.c.a(byte[], int, byte[], int, int):byte[]
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String, java.lang.String, int, boolean):j.c.b.a.z.KeyTypeEntry */
    static {
        RegistryConfig.b g = RegistryConfig.g();
        g.a(MacConfig.a);
        RegistryConfig.b bVar = g;
        bVar.a(c.a("TinkAead", "Aead", "AesCtrHmacAeadKey", 0, true));
        bVar.a(c.a("TinkAead", "Aead", "AesEaxKey", 0, true));
        bVar.a(c.a("TinkAead", "Aead", "AesGcmKey", 0, true));
        bVar.a(c.a("TinkAead", "Aead", "ChaCha20Poly1305Key", 0, true));
        bVar.a(c.a("TinkAead", "Aead", "KmsAeadKey", 0, true));
        bVar.a(c.a("TinkAead", "Aead", "KmsEnvelopeAeadKey", 0, true));
        bVar.m();
        RegistryConfig.a((RegistryConfig) bVar.c, "TINK_AEAD_1_0_0");
        a = (RegistryConfig) bVar.k();
        RegistryConfig.b g2 = RegistryConfig.g();
        g2.a(a);
        RegistryConfig.b bVar2 = g2;
        bVar2.m();
        RegistryConfig.a((RegistryConfig) bVar2.c, "TINK_AEAD_1_1_0");
        RegistryConfig registryConfig = (RegistryConfig) bVar2.k();
        RegistryConfig.b g3 = RegistryConfig.g();
        g3.a(MacConfig.b);
        RegistryConfig.b bVar3 = g3;
        bVar3.a(c.a("TinkAead", "Aead", "AesCtrHmacAeadKey", 0, true));
        bVar3.a(c.a("TinkAead", "Aead", "AesEaxKey", 0, true));
        bVar3.a(c.a("TinkAead", "Aead", "AesGcmKey", 0, true));
        bVar3.a(c.a("TinkAead", "Aead", "ChaCha20Poly1305Key", 0, true));
        bVar3.a(c.a("TinkAead", "Aead", "KmsAeadKey", 0, true));
        bVar3.a(c.a("TinkAead", "Aead", "KmsEnvelopeAeadKey", 0, true));
        bVar3.m();
        RegistryConfig.a((RegistryConfig) bVar3.c, "TINK_AEAD");
        b = (RegistryConfig) bVar3.k();
        try {
            MacConfig.a();
            Registry.a("TinkAead", new AeadCatalogue());
            c.a(b);
        } catch (GeneralSecurityException e2) {
            throw new ExceptionInInitializerError(e2);
        }
    }

    public static void a() {
        MacConfig.a();
        Registry.a("TinkAead", new AeadCatalogue());
        c.a(b);
    }
}
