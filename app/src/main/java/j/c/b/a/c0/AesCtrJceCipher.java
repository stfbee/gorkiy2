package j.c.b.a.c0;

import j.a.a.a.outline;
import java.security.GeneralSecurityException;
import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

public final class AesCtrJceCipher implements IndCpaCipher {
    public final SecretKeySpec a;
    public final int b;
    public final int c;

    public AesCtrJceCipher(byte[] bArr, int i2) {
        Validators.a(bArr.length);
        this.a = new SecretKeySpec(bArr, "AES");
        int blockSize = EngineFactory.f2382f.a("AES/CTR/NoPadding").getBlockSize();
        this.c = blockSize;
        if (i2 < 12 || i2 > blockSize) {
            throw new GeneralSecurityException("invalid IV size");
        }
        this.b = i2;
    }

    public byte[] a(byte[] bArr) {
        int length = bArr.length;
        int i2 = this.b;
        if (length <= Integer.MAX_VALUE - i2) {
            byte[] bArr2 = new byte[(bArr.length + i2)];
            byte[] a2 = Random.a(i2);
            System.arraycopy(a2, 0, bArr2, 0, this.b);
            a(bArr, 0, bArr.length, bArr2, this.b, a2, true);
            return bArr2;
        }
        StringBuilder a3 = outline.a("plaintext length can not exceed ");
        a3.append(Integer.MAX_VALUE - this.b);
        throw new GeneralSecurityException(a3.toString());
    }

    public byte[] b(byte[] bArr) {
        int length = bArr.length;
        int i2 = this.b;
        if (length >= i2) {
            byte[] bArr2 = new byte[i2];
            System.arraycopy(bArr, 0, bArr2, 0, i2);
            int length2 = bArr.length;
            int i3 = this.b;
            byte[] bArr3 = new byte[(length2 - i3)];
            a(bArr, i3, bArr.length - i3, bArr3, 0, bArr2, false);
            return bArr3;
        }
        throw new GeneralSecurityException("ciphertext too short");
    }

    public final void a(byte[] bArr, int i2, int i3, byte[] bArr2, int i4, byte[] bArr3, boolean z) {
        Cipher a2 = EngineFactory.f2382f.a("AES/CTR/NoPadding");
        byte[] bArr4 = new byte[this.c];
        System.arraycopy(bArr3, 0, bArr4, 0, this.b);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(bArr4);
        if (z) {
            a2.init(1, this.a, ivParameterSpec);
        } else {
            a2.init(2, this.a, ivParameterSpec);
        }
        if (a2.doFinal(bArr, i2, i3, bArr2, i4) != i3) {
            throw new GeneralSecurityException("stored output's length does not match input's length");
        }
    }
}
