package j.c.b.a;

import j.c.b.a.z.KeyData;
import j.c.b.a.z.KeyStatusType;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.Keyset;
import j.c.b.a.z.OutputPrefixType;
import j.c.b.a.z.z1;
import java.security.SecureRandom;
import java.util.Collections;
import javax.annotation.concurrent.GuardedBy;

public final class KeysetManager {
    @GuardedBy("this")
    public final Keyset.b a;

    public KeysetManager(Keyset.b bVar) {
        this.a = bVar;
    }

    public static int c() {
        SecureRandom secureRandom = new SecureRandom();
        byte[] bArr = new byte[4];
        byte b = 0;
        while (b == 0) {
            secureRandom.nextBytes(bArr);
            b = ((bArr[0] & Byte.MAX_VALUE) << 24) | ((bArr[1] & 255) << 16) | ((bArr[2] & 255) << 8) | (bArr[3] & 255);
        }
        return b;
    }

    @GuardedBy("this")
    public synchronized KeysetHandle a() {
        return KeysetHandle.a((Keyset) this.a.k());
    }

    @GuardedBy("this")
    public synchronized KeysetManager b(KeyTemplate keyTemplate) {
        Keyset.c a2 = a(keyTemplate);
        Keyset.b bVar = this.a;
        bVar.m();
        Keyset.a((Keyset) bVar.c, a2);
        int i2 = a2.g;
        bVar.m();
        ((Keyset) bVar.c).f2490f = i2;
        return this;
    }

    @GuardedBy("this")
    public final synchronized Keyset.c a(KeyTemplate keyTemplate) {
        Keyset.c.a aVar;
        KeyData b = Registry.b((z1) keyTemplate);
        int b2 = b();
        OutputPrefixType a2 = OutputPrefixType.a(keyTemplate.g);
        if (a2 == null) {
            a2 = OutputPrefixType.UNRECOGNIZED;
        }
        if (a2 == OutputPrefixType.UNKNOWN_PREFIX) {
            a2 = OutputPrefixType.TINK;
        }
        aVar = (Keyset.c.a) Keyset.c.f2491i.e();
        aVar.m();
        Keyset.c.a((Keyset.c) aVar.c, b);
        aVar.m();
        ((Keyset.c) aVar.c).g = b2;
        KeyStatusType keyStatusType = KeyStatusType.ENABLED;
        aVar.m();
        Keyset.c.a((Keyset.c) aVar.c, keyStatusType);
        aVar.m();
        Keyset.c.a((Keyset.c) aVar.c, a2);
        return (Keyset.c) aVar.k();
    }

    @GuardedBy("this")
    public final synchronized int b() {
        int c;
        c = c();
        for (Keyset.c cVar : Collections.unmodifiableList(((Keyset) this.a.c).g)) {
            if (cVar.g == c) {
                c = c();
            }
        }
        return c;
    }
}
