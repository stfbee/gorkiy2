package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.KeysetInfo;
import j.c.b.a.z.n1;
import j.c.e.ByteString;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class EncryptedKeyset extends GeneratedMessageLite<n1, n1.b> implements o1 {
    public static final EncryptedKeyset g;
    public static volatile Parser<n1> h;

    /* renamed from: e  reason: collision with root package name */
    public ByteString f2468e = ByteString.c;

    /* renamed from: f  reason: collision with root package name */
    public KeysetInfo f2469f;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.EncryptedKeyset.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.EncryptedKeyset.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.EncryptedKeyset.a.<clinit>():void");
        }
    }

    static {
        EncryptedKeyset encryptedKeyset = new EncryptedKeyset();
        g = encryptedKeyset;
        super.f();
    }

    public static /* synthetic */ void a(EncryptedKeyset encryptedKeyset, ByteString byteString) {
        if (byteString != null) {
            encryptedKeyset.f2468e = byteString;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v3, types: [j.c.e.MessageLite, j.c.b.a.z.KeysetInfo] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (!this.f2468e.isEmpty()) {
            i3 = 0 + CodedOutputStream.b(2, this.f2468e);
        }
        ? r1 = this.f2469f;
        if (r1 != 0) {
            i3 += CodedOutputStream.b(3, (MessageLite) r1);
        }
        super.d = i3;
        return i3;
    }

    public static final class b extends GeneratedMessageLite.b<n1, n1.b> implements o1 {
        public b() {
            super(EncryptedKeyset.g);
        }

        public /* synthetic */ b(a aVar) {
            super(EncryptedKeyset.g);
        }
    }

    public static /* synthetic */ void a(EncryptedKeyset encryptedKeyset, KeysetInfo keysetInfo) {
        if (keysetInfo != null) {
            encryptedKeyset.f2469f = keysetInfo;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [j.c.e.MessageLite, j.c.b.a.z.KeysetInfo] */
    public void a(CodedOutputStream codedOutputStream) {
        if (!this.f2468e.isEmpty()) {
            codedOutputStream.a(2, this.f2468e);
        }
        ? r0 = this.f2469f;
        if (r0 != 0) {
            codedOutputStream.a(3, (MessageLite) r0);
        }
    }

    /* JADX WARN: Type inference failed for: r6v6, types: [j.c.e.MessageLite, j.c.b.a.z.KeysetInfo] */
    /* JADX WARN: Type inference failed for: r8v2, types: [j.c.e.MessageLite, j.c.b.a.z.KeysetInfo] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return g;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                EncryptedKeyset encryptedKeyset = (EncryptedKeyset) obj2;
                boolean z2 = this.f2468e != ByteString.c;
                ByteString byteString = this.f2468e;
                if (encryptedKeyset.f2468e != ByteString.c) {
                    z = true;
                }
                this.f2468e = kVar.a(z2, byteString, z, encryptedKeyset.f2468e);
                this.f2469f = (KeysetInfo) kVar.a((MessageLite) this.f2469f, (MessageLite) encryptedKeyset.f2469f);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 18) {
                                this.f2468e = codedInputStream.b();
                            } else if (g2 == 26) {
                                KeysetInfo.b bVar = this.f2469f != null ? (KeysetInfo.b) this.f2469f.e() : null;
                                KeysetInfo keysetInfo = (KeysetInfo) codedInputStream.a(KeysetInfo.h.i(), extensionRegistryLite);
                                this.f2469f = keysetInfo;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2469f = (KeysetInfo) bVar.l();
                                }
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new EncryptedKeyset();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (h == null) {
                    synchronized (EncryptedKeyset.class) {
                        if (h == null) {
                            h = new GeneratedMessageLite.c(g);
                        }
                    }
                }
                return h;
            default:
                throw new UnsupportedOperationException();
        }
        return g;
    }
}
