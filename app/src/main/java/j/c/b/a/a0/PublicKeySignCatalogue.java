package j.c.b.a.a0;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.p;
import java.security.GeneralSecurityException;

public class PublicKeySignCatalogue implements Catalogue<p> {
    public KeyManager<p> a(String str, String str2, int i2) {
        KeyManager<p> keyManager;
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == -1213945325 && lowerCase.equals("publickeysign")) ? (char) 0 : 65535) == 0) {
            int hashCode = str.hashCode();
            if (hashCode != -1470419991) {
                if (hashCode == -359160126 && str.equals("type.googleapis.com/google.crypto.tink.EcdsaPrivateKey")) {
                    c = 0;
                }
            } else if (str.equals("type.googleapis.com/google.crypto.tink.Ed25519PrivateKey")) {
                c = 1;
            }
            if (c == 0) {
                keyManager = new EcdsaSignKeyManager();
            } else if (c == 1) {
                keyManager = new Ed25519PrivateKeyManager();
            } else {
                throw new GeneralSecurityException(String.format("No support for primitive 'PublicKeySign' with key type '%s'.", str));
            }
            if (keyManager.b() >= i2) {
                return keyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
