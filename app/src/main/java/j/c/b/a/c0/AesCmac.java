package j.c.b.a.c0;

import j.c.a.a.c.n.c;
import j.c.b.a.Mac;
import java.security.GeneralSecurityException;
import java.security.InvalidAlgorithmParameterException;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

public final class AesCmac implements Mac {
    public final SecretKey a;
    public final int b;
    public byte[] c;
    public byte[] d;

    public AesCmac(byte[] bArr, int i2) {
        Validators.a(bArr.length);
        if (i2 < 10) {
            throw new InvalidAlgorithmParameterException("tag size too small, min is 10 bytes");
        } else if (i2 <= 16) {
            this.a = new SecretKeySpec(bArr, "AES");
            this.b = i2;
            Cipher a2 = EngineFactory.f2382f.a("AES/ECB/NoPadding");
            a2.init(1, this.a);
            byte[] b2 = c.b(a2.doFinal(new byte[16]));
            this.c = b2;
            this.d = c.b(b2);
        } else {
            throw new InvalidAlgorithmParameterException("tag size too large, max is 16 bytes");
        }
    }

    public byte[] a(byte[] bArr) {
        byte[] bArr2;
        Cipher a2 = EngineFactory.f2382f.a("AES/ECB/NoPadding");
        boolean z = true;
        a2.init(1, this.a);
        int max = Math.max(1, (int) Math.ceil(((double) bArr.length) / 16.0d));
        if (max * 16 != bArr.length) {
            z = false;
        }
        if (z) {
            bArr2 = c.a(bArr, (max - 1) * 16, this.c, 0, 16);
        } else {
            bArr2 = c.c(c.a(Arrays.copyOfRange(bArr, (max - 1) * 16, bArr.length)), this.d);
        }
        byte[] bArr3 = new byte[16];
        for (int i2 = 0; i2 < max - 1; i2++) {
            bArr3 = a2.doFinal(c.a(bArr3, 0, bArr, i2 * 16, 16));
        }
        byte[] c2 = c.c(bArr2, bArr3);
        byte[] bArr4 = new byte[this.b];
        System.arraycopy(a2.doFinal(c2), 0, bArr4, 0, this.b);
        return bArr4;
    }

    public void a(byte[] bArr, byte[] bArr2) {
        if (!c.b(bArr, a(bArr2))) {
            throw new GeneralSecurityException("invalid MAC");
        }
    }
}
