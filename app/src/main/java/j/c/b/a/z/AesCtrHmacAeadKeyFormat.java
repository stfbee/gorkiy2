package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.AesCtrKeyFormat;
import j.c.b.a.z.HmacKeyFormat;
import j.c.b.a.z.b;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class AesCtrHmacAeadKeyFormat extends GeneratedMessageLite<b, b.b> implements c {
    public static final AesCtrHmacAeadKeyFormat g;
    public static volatile Parser<b> h;

    /* renamed from: e  reason: collision with root package name */
    public AesCtrKeyFormat f2391e;

    /* renamed from: f  reason: collision with root package name */
    public HmacKeyFormat f2392f;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.AesCtrHmacAeadKeyFormat.a.<clinit>():void");
        }
    }

    static {
        AesCtrHmacAeadKeyFormat aesCtrHmacAeadKeyFormat = new AesCtrHmacAeadKeyFormat();
        g = aesCtrHmacAeadKeyFormat;
        super.f();
    }

    public static /* synthetic */ void a(AesCtrHmacAeadKeyFormat aesCtrHmacAeadKeyFormat, AesCtrKeyFormat aesCtrKeyFormat) {
        if (aesCtrKeyFormat != null) {
            aesCtrHmacAeadKeyFormat.f2391e = aesCtrKeyFormat;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [j.c.b.a.z.HmacKeyFormat, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r2v1, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrKeyFormat] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        if (this.f2391e != null) {
            i3 = 0 + CodedOutputStream.b(1, (MessageLite) g());
        }
        if (this.f2392f != null) {
            i3 += CodedOutputStream.b(2, (MessageLite) h());
        }
        super.d = i3;
        return i3;
    }

    public AesCtrKeyFormat g() {
        AesCtrKeyFormat aesCtrKeyFormat = this.f2391e;
        return aesCtrKeyFormat == null ? AesCtrKeyFormat.g : aesCtrKeyFormat;
    }

    public HmacKeyFormat h() {
        HmacKeyFormat hmacKeyFormat = this.f2392f;
        return hmacKeyFormat == null ? HmacKeyFormat.g : hmacKeyFormat;
    }

    public static final class b extends GeneratedMessageLite.b<b, b.b> implements c {
        public b() {
            super(AesCtrHmacAeadKeyFormat.g);
        }

        public /* synthetic */ b(a aVar) {
            super(AesCtrHmacAeadKeyFormat.g);
        }
    }

    public static /* synthetic */ void a(AesCtrHmacAeadKeyFormat aesCtrHmacAeadKeyFormat, HmacKeyFormat hmacKeyFormat) {
        if (hmacKeyFormat != null) {
            aesCtrHmacAeadKeyFormat.f2392f = hmacKeyFormat;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [j.c.b.a.z.HmacKeyFormat, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrKeyFormat] */
    public void a(CodedOutputStream codedOutputStream) {
        if (this.f2391e != null) {
            codedOutputStream.a(1, (MessageLite) g());
        }
        if (this.f2392f != null) {
            codedOutputStream.a(2, (MessageLite) h());
        }
    }

    /* JADX WARN: Type inference failed for: r4v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrKeyFormat] */
    /* JADX WARN: Type inference failed for: r0v1, types: [j.c.e.MessageLite, j.c.b.a.z.AesCtrKeyFormat] */
    /* JADX WARN: Type inference failed for: r4v6, types: [j.c.b.a.z.HmacKeyFormat, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r6v2, types: [j.c.b.a.z.HmacKeyFormat, j.c.e.MessageLite] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        switch (jVar.ordinal()) {
            case 0:
                return g;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                AesCtrHmacAeadKeyFormat aesCtrHmacAeadKeyFormat = (AesCtrHmacAeadKeyFormat) obj2;
                this.f2391e = (AesCtrKeyFormat) kVar.a((MessageLite) this.f2391e, (MessageLite) aesCtrHmacAeadKeyFormat.f2391e);
                this.f2392f = (HmacKeyFormat) kVar.a((MessageLite) this.f2392f, (MessageLite) aesCtrHmacAeadKeyFormat.f2392f);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                boolean z = false;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 10) {
                                AesCtrKeyFormat.b bVar = this.f2391e != null ? (AesCtrKeyFormat.b) this.f2391e.e() : null;
                                AesCtrKeyFormat aesCtrKeyFormat = (AesCtrKeyFormat) codedInputStream.a(AesCtrKeyFormat.g.i(), extensionRegistryLite);
                                this.f2391e = aesCtrKeyFormat;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2391e = (AesCtrKeyFormat) bVar.l();
                                }
                            } else if (g2 == 18) {
                                HmacKeyFormat.b bVar2 = this.f2392f != null ? (HmacKeyFormat.b) this.f2392f.e() : null;
                                HmacKeyFormat hmacKeyFormat = (HmacKeyFormat) codedInputStream.a(HmacKeyFormat.g.i(), extensionRegistryLite);
                                this.f2392f = hmacKeyFormat;
                                if (bVar2 != null) {
                                    bVar2.a(super);
                                    this.f2392f = (HmacKeyFormat) bVar2.l();
                                }
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AesCtrHmacAeadKeyFormat();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (h == null) {
                    synchronized (AesCtrHmacAeadKeyFormat.class) {
                        if (h == null) {
                            h = new GeneratedMessageLite.c(g);
                        }
                    }
                }
                return h;
            default:
                throw new UnsupportedOperationException();
        }
        return g;
    }
}
