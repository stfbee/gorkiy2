package j.c.b.a;

import j.a.a.a.outline;
import j.c.b.a.PrimitiveSet;
import j.c.b.a.z.KeyData;
import j.c.b.a.z.KeyStatusType;
import j.c.b.a.z.Keyset;
import j.c.b.a.z.w1;
import j.c.b.a.z.z1;
import j.c.e.ByteString;
import j.c.e.MessageLite;
import j.c.e.o;
import java.nio.ByteBuffer;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.logging.Logger;

public final class Registry {
    public static final Logger a = Logger.getLogger(Registry.class.getName());
    public static final ConcurrentMap<String, g> b = new ConcurrentHashMap();
    public static final ConcurrentMap<String, Boolean> c = new ConcurrentHashMap();
    public static final ConcurrentMap<String, b> d = new ConcurrentHashMap();

    public static synchronized <P> o a(z1 z1Var) {
        MessageLite a2;
        synchronized (Registry.class) {
            KeyManager b2 = b(z1Var.f2481e);
            if (c.get(z1Var.f2481e).booleanValue()) {
                a2 = b2.a(z1Var.f2482f);
            } else {
                throw new GeneralSecurityException("newKey-operation not permitted for key type " + z1Var.f2481e);
            }
        }
        return a2;
    }

    public static synchronized <P> w1 b(z1 z1Var) {
        KeyData b2;
        synchronized (Registry.class) {
            KeyManager b3 = b(z1Var.f2481e);
            if (c.get(z1Var.f2481e).booleanValue()) {
                b2 = b3.b(z1Var.f2482f);
            } else {
                throw new GeneralSecurityException("newKey-operation not permitted for key type " + z1Var.f2481e);
            }
        }
        return b2;
    }

    public static <P> PrimitiveSet<P> a(h hVar, KeyManager keyManager) {
        Object obj;
        byte[] bArr;
        Util.b(hVar.a);
        PrimitiveSet<P> primitiveSet = new PrimitiveSet<>();
        for (Keyset.c next : hVar.a.g) {
            if (next.k() == KeyStatusType.ENABLED) {
                if (keyManager == null || !keyManager.a(next.g().f2478e)) {
                    String str = next.g().f2478e;
                    obj = b(str).c(next.g().f2479f);
                } else {
                    obj = keyManager.c(next.g().f2479f);
                }
                int ordinal = next.h().ordinal();
                if (ordinal != 1) {
                    if (ordinal != 2) {
                        if (ordinal == 3) {
                            bArr = CryptoFormat.a;
                        } else if (ordinal != 4) {
                            throw new GeneralSecurityException("unknown output prefix type");
                        }
                    }
                    bArr = ByteBuffer.allocate(5).put((byte) 0).putInt(next.g).array();
                } else {
                    bArr = ByteBuffer.allocate(5).put((byte) 1).putInt(next.g).array();
                }
                PrimitiveSet.a<P> aVar = new PrimitiveSet.a<>(obj, bArr, next.k(), next.h());
                ArrayList arrayList = new ArrayList();
                arrayList.add(aVar);
                String str2 = new String(aVar.a(), PrimitiveSet.c);
                List put = primitiveSet.a.put(str2, Collections.unmodifiableList(arrayList));
                if (put != null) {
                    ArrayList arrayList2 = new ArrayList();
                    arrayList2.addAll(put);
                    arrayList2.add(aVar);
                    primitiveSet.a.put(str2, Collections.unmodifiableList(arrayList2));
                }
                if (next.g == hVar.a.f2490f) {
                    primitiveSet.b = aVar;
                }
            }
        }
        return primitiveSet;
    }

    public static <P> KeyManager<P> b(String str) {
        KeyManager<P> keyManager = b.get(str);
        if (keyManager != null) {
            return keyManager;
        }
        throw new GeneralSecurityException(outline.a("No key manager found for key type: ", str, ".  Check the configuration of the registry."));
    }

    public static synchronized <P> void a(String str, Catalogue catalogue) {
        synchronized (Registry.class) {
            if (str == null) {
                throw new IllegalArgumentException("catalogueName must be non-null.");
            } else if (catalogue != null) {
                if (d.containsKey(str.toLowerCase())) {
                    if (!catalogue.getClass().equals(d.get(str.toLowerCase()).getClass())) {
                        Logger logger = a;
                        logger.warning("Attempted overwrite of a catalogueName catalogue for name " + str);
                        throw new GeneralSecurityException("catalogue for name " + str + " has been already registered");
                    }
                }
                d.put(str.toLowerCase(), catalogue);
            } else {
                throw new IllegalArgumentException("catalogue must be non-null.");
            }
        }
    }

    public static <P> Catalogue<P> a(String str) {
        if (str != null) {
            Catalogue<P> catalogue = d.get(str.toLowerCase());
            if (catalogue != null) {
                return catalogue;
            }
            String format = String.format("no catalogue found for %s. ", str);
            if (str.toLowerCase().startsWith("tinkaead")) {
                format = outline.a(format, "Maybe call AeadConfig.register().");
            }
            if (str.toLowerCase().startsWith("tinkdeterministicaead")) {
                format = outline.a(format, "Maybe call DeterministicAeadConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkstreamingaead")) {
                format = outline.a(format, "Maybe call StreamingAeadConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkhybriddecrypt") || str.toLowerCase().startsWith("tinkhybridencrypt")) {
                format = outline.a(format, "Maybe call HybridConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkmac")) {
                format = outline.a(format, "Maybe call MacConfig.register().");
            } else if (str.toLowerCase().startsWith("tinkpublickeysign") || str.toLowerCase().startsWith("tinkpublickeyverify")) {
                format = outline.a(format, "Maybe call SignatureConfig.register().");
            } else if (str.toLowerCase().startsWith("tink")) {
                format = outline.a(format, "Maybe call TinkConfig.register().");
            }
            throw new GeneralSecurityException(format);
        }
        throw new IllegalArgumentException("catalogueName must be non-null.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.b.a.Registry.a(j.c.b.a.KeyManager, boolean):void
     arg types: [j.c.b.a.KeyManager, int]
     candidates:
      j.c.b.a.Registry.a(j.c.b.a.h, j.c.b.a.KeyManager):j.c.b.a.PrimitiveSet<P>
      j.c.b.a.Registry.a(java.lang.String, j.c.e.o):j.c.e.o
      j.c.b.a.Registry.a(java.lang.String, byte[]):P
      j.c.b.a.Registry.a(java.lang.String, j.c.b.a.Catalogue):void
      j.c.b.a.Registry.a(j.c.b.a.KeyManager, boolean):void */
    public static synchronized <P> void a(KeyManager keyManager) {
        synchronized (Registry.class) {
            a(keyManager, true);
        }
    }

    public static synchronized <P> void a(KeyManager keyManager, boolean z) {
        synchronized (Registry.class) {
            if (keyManager != null) {
                String a2 = keyManager.a();
                if (b.containsKey(a2)) {
                    KeyManager b2 = b(a2);
                    boolean booleanValue = c.get(a2).booleanValue();
                    if (keyManager.getClass().equals(b2.getClass())) {
                        if (!booleanValue) {
                            if (!z) {
                            }
                        }
                    }
                    Logger logger = a;
                    logger.warning("Attempted overwrite of a registered key manager for key type " + a2);
                    throw new GeneralSecurityException(String.format("typeUrl (%s) is already registered with %s, cannot be re-registered with %s", a2, b2.getClass().getName(), keyManager.getClass().getName()));
                }
                b.put(a2, keyManager);
                c.put(a2, Boolean.valueOf(z));
            } else {
                throw new IllegalArgumentException("key manager must be non-null.");
            }
        }
    }

    public static synchronized <P> o a(String str, o oVar) {
        MessageLite b2;
        synchronized (Registry.class) {
            KeyManager b3 = b(str);
            if (c.get(str).booleanValue()) {
                b2 = b3.b((MessageLite) oVar);
            } else {
                throw new GeneralSecurityException("newKey-operation not permitted for key type " + str);
            }
        }
        return b2;
    }

    public static <P> P a(String str, byte[] bArr) {
        return b(str).c(ByteString.a(bArr));
    }
}
