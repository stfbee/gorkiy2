package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.AesCtrHmacStreamingParams;
import j.c.b.a.z.e;
import j.c.e.ByteString;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class AesCtrHmacStreamingKey extends GeneratedMessageLite<e, e.b> implements h {
    public static final AesCtrHmacStreamingKey h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<e> f2393i;

    /* renamed from: e  reason: collision with root package name */
    public int f2394e;

    /* renamed from: f  reason: collision with root package name */
    public AesCtrHmacStreamingParams f2395f;
    public ByteString g = ByteString.c;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.AesCtrHmacStreamingKey.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.AesCtrHmacStreamingKey.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.AesCtrHmacStreamingKey.a.<clinit>():void");
        }
    }

    static {
        AesCtrHmacStreamingKey aesCtrHmacStreamingKey = new AesCtrHmacStreamingKey();
        h = aesCtrHmacStreamingKey;
        super.f();
    }

    public static /* synthetic */ void a(AesCtrHmacStreamingKey aesCtrHmacStreamingKey, AesCtrHmacStreamingParams aesCtrHmacStreamingParams) {
        if (aesCtrHmacStreamingParams != null) {
            aesCtrHmacStreamingKey.f2395f = aesCtrHmacStreamingParams;
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v1, types: [j.c.b.a.z.AesCtrHmacStreamingParams, j.c.e.MessageLite] */
    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        int i4 = this.f2394e;
        if (i4 != 0) {
            i3 = 0 + CodedOutputStream.d(1, i4);
        }
        if (this.f2395f != null) {
            i3 += CodedOutputStream.b(2, (MessageLite) g());
        }
        if (!this.g.isEmpty()) {
            i3 += CodedOutputStream.b(3, this.g);
        }
        super.d = i3;
        return i3;
    }

    public AesCtrHmacStreamingParams g() {
        AesCtrHmacStreamingParams aesCtrHmacStreamingParams = this.f2395f;
        return aesCtrHmacStreamingParams == null ? AesCtrHmacStreamingParams.f2398i : aesCtrHmacStreamingParams;
    }

    public static final class b extends GeneratedMessageLite.b<e, e.b> implements h {
        public b() {
            super(AesCtrHmacStreamingKey.h);
        }

        public /* synthetic */ b(a aVar) {
            super(AesCtrHmacStreamingKey.h);
        }
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [j.c.b.a.z.AesCtrHmacStreamingParams, j.c.e.MessageLite] */
    public void a(CodedOutputStream codedOutputStream) {
        int i2 = this.f2394e;
        if (i2 != 0) {
            codedOutputStream.b(1, i2);
        }
        if (this.f2395f != null) {
            codedOutputStream.a(2, (MessageLite) g());
        }
        if (!this.g.isEmpty()) {
            codedOutputStream.a(3, this.g);
        }
    }

    /* JADX WARN: Type inference failed for: r6v6, types: [j.c.b.a.z.AesCtrHmacStreamingParams, j.c.e.MessageLite] */
    /* JADX WARN: Type inference failed for: r2v2, types: [j.c.b.a.z.AesCtrHmacStreamingParams, j.c.e.MessageLite] */
    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                AesCtrHmacStreamingKey aesCtrHmacStreamingKey = (AesCtrHmacStreamingKey) obj2;
                this.f2394e = kVar.a(this.f2394e != 0, this.f2394e, aesCtrHmacStreamingKey.f2394e != 0, aesCtrHmacStreamingKey.f2394e);
                this.f2395f = (AesCtrHmacStreamingParams) kVar.a((MessageLite) this.f2395f, (MessageLite) aesCtrHmacStreamingKey.f2395f);
                boolean z2 = this.g != ByteString.c;
                ByteString byteString = this.g;
                if (aesCtrHmacStreamingKey.g != ByteString.c) {
                    z = true;
                }
                this.g = kVar.a(z2, byteString, z, aesCtrHmacStreamingKey.g);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 8) {
                                this.f2394e = codedInputStream.d();
                            } else if (g2 == 18) {
                                AesCtrHmacStreamingParams.b bVar = this.f2395f != null ? (AesCtrHmacStreamingParams.b) this.f2395f.e() : null;
                                AesCtrHmacStreamingParams aesCtrHmacStreamingParams = (AesCtrHmacStreamingParams) codedInputStream.a(AesCtrHmacStreamingParams.f2398i.i(), extensionRegistryLite);
                                this.f2395f = aesCtrHmacStreamingParams;
                                if (bVar != null) {
                                    bVar.a(super);
                                    this.f2395f = (AesCtrHmacStreamingParams) bVar.l();
                                }
                            } else if (g2 == 26) {
                                this.g = codedInputStream.b();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AesCtrHmacStreamingKey();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2393i == null) {
                    synchronized (AesCtrHmacStreamingKey.class) {
                        if (f2393i == null) {
                            f2393i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2393i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
