package j.c.b.a.z;

import com.google.protobuf.CodedOutputStream;
import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.z.a0;
import j.c.e.CodedInputStream;
import j.c.e.ExtensionRegistryLite;
import j.c.e.GeneratedMessageLite;
import j.c.e.Parser;
import java.io.IOException;

public final class AesGcmHkdfStreamingParams extends GeneratedMessageLite<a0, a0.b> implements b0 {
    public static final AesGcmHkdfStreamingParams h;

    /* renamed from: i  reason: collision with root package name */
    public static volatile Parser<a0> f2421i;

    /* renamed from: e  reason: collision with root package name */
    public int f2422e;

    /* renamed from: f  reason: collision with root package name */
    public int f2423f;
    public int g;

    public static /* synthetic */ class a {
        public static final /* synthetic */ int[] a;

        /* JADX WARNING: Can't wrap try/catch for region: R(20:0|(2:1|2)|3|5|6|7|8|9|11|12|13|14|15|(2:17|18)|19|21|22|23|24|26) */
        /* JADX WARNING: Can't wrap try/catch for region: R(22:0|1|2|3|5|6|7|8|9|11|12|13|14|15|17|18|19|21|22|23|24|26) */
        /* JADX WARNING: Code restructure failed: missing block: B:27:?, code lost:
            return;
         */
        /* JADX WARNING: Failed to process nested try/catch */
        /* JADX WARNING: Missing exception handler attribute for start block: B:13:0x0025 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:23:0x0039 */
        /* JADX WARNING: Missing exception handler attribute for start block: B:7:0x0017 */
        static {
            /*
                j.c.e.GeneratedMessageLite$j[] r0 = j.c.e.GeneratedMessageLite.j.values()
                int r0 = r0.length
                int[] r0 = new int[r0]
                j.c.b.a.z.AesGcmHkdfStreamingParams.a.a = r0
                r1 = 1
                r2 = 4
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.NEW_MUTABLE_INSTANCE     // Catch:{ NoSuchFieldError -> 0x000f }
                r0[r2] = r1     // Catch:{ NoSuchFieldError -> 0x000f }
            L_0x000f:
                r0 = 2
                int[] r3 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x0017 }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.IS_INITIALIZED     // Catch:{ NoSuchFieldError -> 0x0017 }
                r4 = 0
                r3[r4] = r0     // Catch:{ NoSuchFieldError -> 0x0017 }
            L_0x0017:
                int[] r3 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x001e }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.MAKE_IMMUTABLE     // Catch:{ NoSuchFieldError -> 0x001e }
                r4 = 3
                r3[r4] = r4     // Catch:{ NoSuchFieldError -> 0x001e }
            L_0x001e:
                r3 = 5
                int[] r4 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x0025 }
                j.c.e.GeneratedMessageLite$j r5 = j.c.e.GeneratedMessageLite.j.NEW_BUILDER     // Catch:{ NoSuchFieldError -> 0x0025 }
                r4[r3] = r2     // Catch:{ NoSuchFieldError -> 0x0025 }
            L_0x0025:
                int[] r2 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x002b }
                j.c.e.GeneratedMessageLite$j r4 = j.c.e.GeneratedMessageLite.j.VISIT     // Catch:{ NoSuchFieldError -> 0x002b }
                r2[r1] = r3     // Catch:{ NoSuchFieldError -> 0x002b }
            L_0x002b:
                r1 = 6
                int[] r2 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x0032 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.MERGE_FROM_STREAM     // Catch:{ NoSuchFieldError -> 0x0032 }
                r2[r0] = r1     // Catch:{ NoSuchFieldError -> 0x0032 }
            L_0x0032:
                r0 = 7
                int[] r2 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x0039 }
                j.c.e.GeneratedMessageLite$j r3 = j.c.e.GeneratedMessageLite.j.GET_DEFAULT_INSTANCE     // Catch:{ NoSuchFieldError -> 0x0039 }
                r2[r1] = r0     // Catch:{ NoSuchFieldError -> 0x0039 }
            L_0x0039:
                int[] r1 = j.c.b.a.z.AesGcmHkdfStreamingParams.a.a     // Catch:{ NoSuchFieldError -> 0x0041 }
                j.c.e.GeneratedMessageLite$j r2 = j.c.e.GeneratedMessageLite.j.GET_PARSER     // Catch:{ NoSuchFieldError -> 0x0041 }
                r2 = 8
                r1[r0] = r2     // Catch:{ NoSuchFieldError -> 0x0041 }
            L_0x0041:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: j.c.b.a.z.AesGcmHkdfStreamingParams.a.<clinit>():void");
        }
    }

    static {
        AesGcmHkdfStreamingParams aesGcmHkdfStreamingParams = new AesGcmHkdfStreamingParams();
        h = aesGcmHkdfStreamingParams;
        super.f();
    }

    public void a(CodedOutputStream codedOutputStream) {
        int i2 = this.f2422e;
        if (i2 != 0) {
            codedOutputStream.b(1, i2);
        }
        int i3 = this.f2423f;
        if (i3 != 0) {
            codedOutputStream.b(2, i3);
        }
        int i4 = this.g;
        if (i4 != HashType.UNKNOWN_HASH.value) {
            codedOutputStream.a(3, i4);
        }
    }

    public int c() {
        int i2 = super.d;
        if (i2 != -1) {
            return i2;
        }
        int i3 = 0;
        int i4 = this.f2422e;
        if (i4 != 0) {
            i3 = 0 + CodedOutputStream.d(1, i4);
        }
        int i5 = this.f2423f;
        if (i5 != 0) {
            i3 += CodedOutputStream.d(2, i5);
        }
        int i6 = this.g;
        if (i6 != HashType.UNKNOWN_HASH.value) {
            i3 += CodedOutputStream.c(3, i6);
        }
        super.d = i3;
        return i3;
    }

    public static final class b extends GeneratedMessageLite.b<a0, a0.b> implements b0 {
        public b() {
            super(AesGcmHkdfStreamingParams.h);
        }

        public /* synthetic */ b(a aVar) {
            super(AesGcmHkdfStreamingParams.h);
        }
    }

    /* JADX INFO: Can't fix incorrect switch cases order, some code will duplicate */
    public final Object a(GeneratedMessageLite.j jVar, Object obj, Object obj2) {
        boolean z = false;
        switch (jVar.ordinal()) {
            case 0:
                return h;
            case 1:
                GeneratedMessageLite.k kVar = (GeneratedMessageLite.k) obj;
                AesGcmHkdfStreamingParams aesGcmHkdfStreamingParams = (AesGcmHkdfStreamingParams) obj2;
                this.f2422e = kVar.a(this.f2422e != 0, this.f2422e, aesGcmHkdfStreamingParams.f2422e != 0, aesGcmHkdfStreamingParams.f2422e);
                this.f2423f = kVar.a(this.f2423f != 0, this.f2423f, aesGcmHkdfStreamingParams.f2423f != 0, aesGcmHkdfStreamingParams.f2423f);
                boolean z2 = this.g != 0;
                int i2 = this.g;
                if (aesGcmHkdfStreamingParams.g != 0) {
                    z = true;
                }
                this.g = kVar.a(z2, i2, z, aesGcmHkdfStreamingParams.g);
                return this;
            case 2:
                CodedInputStream codedInputStream = (CodedInputStream) obj;
                ExtensionRegistryLite extensionRegistryLite = (ExtensionRegistryLite) obj2;
                while (!z) {
                    try {
                        int g2 = codedInputStream.g();
                        if (g2 != 0) {
                            if (g2 == 8) {
                                this.f2422e = codedInputStream.d();
                            } else if (g2 == 16) {
                                this.f2423f = codedInputStream.d();
                            } else if (g2 == 24) {
                                this.g = codedInputStream.d();
                            } else if (!codedInputStream.e(g2)) {
                            }
                        }
                        z = true;
                    } catch (InvalidProtocolBufferException e2) {
                        throw new RuntimeException(e2);
                    } catch (IOException e3) {
                        throw new RuntimeException(new InvalidProtocolBufferException(e3.getMessage()));
                    }
                }
                break;
            case 3:
                return null;
            case 4:
                return new AesGcmHkdfStreamingParams();
            case 5:
                return new b(null);
            case 6:
                break;
            case 7:
                if (f2421i == null) {
                    synchronized (AesGcmHkdfStreamingParams.class) {
                        if (f2421i == null) {
                            f2421i = new GeneratedMessageLite.c(h);
                        }
                    }
                }
                return f2421i;
            default:
                throw new UnsupportedOperationException();
        }
        return h;
    }
}
