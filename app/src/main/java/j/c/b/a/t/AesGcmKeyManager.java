package j.c.b.a.t;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.b.a.Aead;
import j.c.b.a.KeyManager;
import j.c.b.a.a;
import j.c.b.a.c0.AesGcmJce;
import j.c.b.a.c0.Random;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.AesGcmKey;
import j.c.b.a.z.AesGcmKeyFormat;
import j.c.b.a.z.KeyData;
import j.c.e.ByteString;
import j.c.e.GeneratedMessageLite;
import j.c.e.MessageLite;
import j.c.e.f;
import java.security.GeneralSecurityException;

public class AesGcmKeyManager implements KeyManager<a> {
    public String a() {
        return "type.googleapis.com/google.crypto.tink.AesGcmKey";
    }

    public int b() {
        return 0;
    }

    /* JADX WARN: Type inference failed for: r3v7, types: [j.c.e.MessageLite, j.c.e.GeneratedMessageLite] */
    public MessageLite b(MessageLite messageLite) {
        if (messageLite instanceof AesGcmKeyFormat) {
            AesGcmKeyFormat aesGcmKeyFormat = (AesGcmKeyFormat) messageLite;
            Validators.a(aesGcmKeyFormat.f2427e);
            AesGcmKey.b bVar = (AesGcmKey.b) AesGcmKey.g.e();
            ByteString a = ByteString.a(Random.a(aesGcmKeyFormat.f2427e));
            bVar.m();
            AesGcmKey.a(bVar.c, a);
            bVar.m();
            bVar.c.f2424e = 0;
            return bVar.k();
        }
        throw new GeneralSecurityException("expected AesGcmKeyFormat proto");
    }

    /* JADX WARN: Type inference failed for: r2v3, types: [j.c.b.a.z.AesGcmKey, j.c.e.MessageLite] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesGcmKey, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public Object c(ByteString byteString) {
        try {
            return a((MessageLite) ((AesGcmKey) GeneratedMessageLite.a((GeneratedMessageLite) AesGcmKey.g, (f) byteString)));
        } catch (InvalidProtocolBufferException unused) {
            throw new GeneralSecurityException("expected AesGcmKey proto");
        }
    }

    public Aead a(MessageLite messageLite) {
        if (messageLite instanceof AesGcmKey) {
            AesGcmKey aesGcmKey = (AesGcmKey) messageLite;
            Validators.a(aesGcmKey.f2424e, 0);
            Validators.a(aesGcmKey.f2425f.size());
            return new AesGcmJce(aesGcmKey.f2425f.d());
        }
        throw new GeneralSecurityException("expected AesGcmKey proto");
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [j.c.e.MessageLite, j.c.b.a.z.AesGcmKeyFormat] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T
     arg types: [j.c.b.a.z.AesGcmKeyFormat, j.c.e.ByteString]
     candidates:
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, byte[]):T
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite$j, java.lang.Object):java.lang.Object
      j.c.e.GeneratedMessageLite.a(j.c.e.k$k, j.c.e.GeneratedMessageLite):void
      j.c.e.GeneratedMessageLite.a(j.c.e.GeneratedMessageLite, j.c.e.f):T */
    public MessageLite a(ByteString byteString) {
        try {
            return b((MessageLite) ((AesGcmKeyFormat) GeneratedMessageLite.a((GeneratedMessageLite) AesGcmKeyFormat.f2426f, (f) byteString)));
        } catch (InvalidProtocolBufferException e2) {
            throw new GeneralSecurityException("expected serialized AesGcmKeyFormat proto", e2);
        }
    }

    public boolean a(String str) {
        return str.equals("type.googleapis.com/google.crypto.tink.AesGcmKey");
    }

    public KeyData b(ByteString byteString) {
        KeyData.b g = KeyData.g();
        g.m();
        KeyData.a((KeyData) g.c, "type.googleapis.com/google.crypto.tink.AesGcmKey");
        ByteString a = ((AesGcmKey) a(byteString)).a();
        g.m();
        KeyData.a((KeyData) g.c, a);
        g.a(KeyData.c.SYMMETRIC);
        return (KeyData) g.k();
    }
}
