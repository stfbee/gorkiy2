package j.c.b.a.a0;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.q;
import java.security.GeneralSecurityException;

public class PublicKeyVerifyCatalogue implements Catalogue<q> {
    public KeyManager<q> a(String str, String str2, int i2) {
        KeyManager<q> keyManager;
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 1712166735 && lowerCase.equals("publickeyverify")) ? (char) 0 : 65535) == 0) {
            int hashCode = str.hashCode();
            if (hashCode != 248906128) {
                if (hashCode == 1737079689 && str.equals("type.googleapis.com/google.crypto.tink.Ed25519PublicKey")) {
                    c = 1;
                }
            } else if (str.equals("type.googleapis.com/google.crypto.tink.EcdsaPublicKey")) {
                c = 0;
            }
            if (c == 0) {
                keyManager = new EcdsaVerifyKeyManager();
            } else if (c == 1) {
                keyManager = new Ed25519PublicKeyManager();
            } else {
                throw new GeneralSecurityException(String.format("No support for primitive 'PublicKeyVerify' with key type '%s'.", str));
            }
            if (keyManager.b() >= i2) {
                return keyManager;
            }
            throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
