package j.c.b.a.x.a;

import android.content.Context;
import android.os.Build;
import android.security.keystore.KeyGenParameterSpec;
import android.util.Log;
import com.google.protobuf.InvalidProtocolBufferException;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import j.c.b.a.Aead;
import j.c.b.a.KeysetHandle;
import j.c.b.a.KeysetManager;
import j.c.b.a.KeysetWriter;
import j.c.b.a.c0.Validators;
import j.c.b.a.z.KeyTemplate;
import j.c.b.a.z.Keyset;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import javax.annotation.concurrent.GuardedBy;
import javax.crypto.KeyGenerator;

public final class AndroidKeysetManager {
    public final SharedPrefKeysetReader a;
    public final KeysetWriter b;
    public final boolean c;
    public final Aead d;

    /* renamed from: e  reason: collision with root package name */
    public final KeyTemplate f2385e;
    @GuardedBy("this")

    /* renamed from: f  reason: collision with root package name */
    public KeysetManager f2386f;

    public static final class b {
        public SharedPrefKeysetReader a = null;
        public KeysetWriter b = null;
        public String c = null;
        public boolean d = true;

        /* renamed from: e  reason: collision with root package name */
        public KeyTemplate f2387e = null;

        public b a(Context context, String str, String str2) {
            if (context == null) {
                throw new IllegalArgumentException("need an Android context");
            } else if (str != null) {
                this.a = new SharedPrefKeysetReader(context, str, str2);
                this.b = new SharedPrefKeysetWriter(context, str, str2);
                return this;
            } else {
                throw new IllegalArgumentException("need a keyset name");
            }
        }
    }

    public /* synthetic */ AndroidKeysetManager(b bVar, a aVar) {
        KeysetManager keysetManager;
        SharedPrefKeysetReader sharedPrefKeysetReader = bVar.a;
        this.a = sharedPrefKeysetReader;
        if (sharedPrefKeysetReader != null) {
            KeysetWriter keysetWriter = bVar.b;
            this.b = keysetWriter;
            if (keysetWriter != null) {
                boolean z = bVar.d;
                this.c = z;
                if (!z || bVar.c != null) {
                    if (c()) {
                        String str = bVar.c;
                        String a2 = Validators.a("android-keystore://", str);
                        KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
                        instance.load(null);
                        if (!instance.containsAlias(a2)) {
                            String a3 = Validators.a("android-keystore://", str);
                            KeyGenerator instance2 = KeyGenerator.getInstance("AES", "AndroidKeyStore");
                            instance2.init(new KeyGenParameterSpec.Builder(a3, 3).setKeySize(256).setBlockModes("GCM").setEncryptionPaddings("NoPadding").build());
                            instance2.generateKey();
                        }
                        if (Build.VERSION.SDK_INT >= 23) {
                            try {
                                this.d = new AndroidKeystoreAesGcm(Validators.a("android-keystore://", str));
                            } catch (IOException e2) {
                                throw new GeneralSecurityException(e2);
                            }
                        } else {
                            throw new GeneralSecurityException("needs Android Keystore on Android M or newer");
                        }
                    } else {
                        this.d = null;
                    }
                    this.f2385e = bVar.f2387e;
                    try {
                        keysetManager = b();
                    } catch (IOException e3) {
                        StringBuilder a4 = outline.a("cannot read keyset: ");
                        a4.append(e3.toString());
                        Log.i("j.c.b.a.x.a.a", a4.toString());
                        if (this.f2385e != null) {
                            keysetManager = new KeysetManager((Keyset.b) Keyset.h.e());
                            keysetManager.b(this.f2385e);
                            try {
                                if (c()) {
                                    keysetManager.a().a(this.b, this.d);
                                } else {
                                    KeysetHandle a5 = keysetManager.a();
                                    SharedPrefKeysetWriter sharedPrefKeysetWriter = (SharedPrefKeysetWriter) this.b;
                                    sharedPrefKeysetWriter.a.putString(sharedPrefKeysetWriter.b, c.c(a5.a.b())).apply();
                                }
                            } catch (IOException e4) {
                                throw new GeneralSecurityException(e4);
                            }
                        } else {
                            throw new GeneralSecurityException("cannot obtain keyset handle");
                        }
                    }
                    this.f2386f = keysetManager;
                    return;
                }
                throw new IllegalArgumentException("need a master key URI, please set it with Builder#masterKeyUri");
            }
            throw new IllegalArgumentException("need to specify where to write the keyset to with Builder#withSharedPref");
        }
        throw new IllegalArgumentException("need to specify where to read the keyset from with Builder#withSharedPref");
    }

    @GuardedBy("this")
    public synchronized KeysetHandle a() {
        return this.f2386f.a();
    }

    public final KeysetManager b() {
        if (c()) {
            try {
                return new KeysetManager((Keyset.b) KeysetHandle.a(this.a, this.d).a.e());
            } catch (InvalidProtocolBufferException | GeneralSecurityException e2) {
                StringBuilder a2 = outline.a("cannot decrypt keyset: ");
                a2.append(e2.toString());
                Log.i("j.c.b.a.x.a.a", a2.toString());
            }
        }
        KeysetHandle a3 = KeysetHandle.a(Keyset.a(this.a.a()));
        if (c()) {
            a3.a(this.b, this.d);
        }
        return new KeysetManager((Keyset.b) a3.a.e());
    }

    public final boolean c() {
        return this.c && Build.VERSION.SDK_INT >= 23;
    }
}
