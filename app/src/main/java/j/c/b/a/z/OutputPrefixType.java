package j.c.b.a.z;

import j.c.e.Internal;
import j.c.e.l;

public enum OutputPrefixType implements l.a {
    UNKNOWN_PREFIX(0),
    TINK(1),
    LEGACY(2),
    RAW(3),
    CRUNCHY(4),
    UNRECOGNIZED(-1);
    
    public static final int CRUNCHY_VALUE = 4;
    public static final int LEGACY_VALUE = 2;
    public static final int RAW_VALUE = 3;
    public static final int TINK_VALUE = 1;
    public static final int UNKNOWN_PREFIX_VALUE = 0;
    public static final Internal.b<p2> internalValueMap = new a();
    public final int value;

    public class a implements Internal.b<p2> {
    }

    /* access modifiers changed from: public */
    OutputPrefixType(int i2) {
        this.value = i2;
    }

    public static OutputPrefixType a(int i2) {
        if (i2 == 0) {
            return UNKNOWN_PREFIX;
        }
        if (i2 == 1) {
            return TINK;
        }
        if (i2 == 2) {
            return LEGACY;
        }
        if (i2 == 3) {
            return RAW;
        }
        if (i2 != 4) {
            return null;
        }
        return CRUNCHY;
    }
}
