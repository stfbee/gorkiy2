package j.c.b.a.v;

import j.c.b.a.Catalogue;
import j.c.b.a.KeyManager;
import j.c.b.a.d;
import java.security.GeneralSecurityException;

public class DeterministicAeadCatalogue implements Catalogue<d> {
    public KeyManager<d> a(String str, String str2, int i2) {
        String lowerCase = str2.toLowerCase();
        char c = 65535;
        if (((lowerCase.hashCode() == 109397593 && lowerCase.equals("deterministicaead")) ? (char) 0 : 65535) == 0) {
            if (str.hashCode() == -2089717248 && str.equals("type.googleapis.com/google.crypto.tink.AesSivKey")) {
                c = 0;
            }
            if (c == 0) {
                AesSivKeyManager aesSivKeyManager = new AesSivKeyManager();
                if (aesSivKeyManager.b() >= i2) {
                    return aesSivKeyManager;
                }
                throw new GeneralSecurityException(String.format("No key manager for key type '%s' with version at least %d.", str, Integer.valueOf(i2)));
            }
            throw new GeneralSecurityException(String.format("No support for primitive 'DeterministicAead' with key type '%s'.", str));
        }
        throw new GeneralSecurityException(String.format("No support for primitive '%s'.", str2));
    }
}
