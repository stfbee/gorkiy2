package j.c.b.a.z;

import j.c.e.Internal;
import j.c.e.l;

public enum EllipticCurveType implements l.a {
    UNKNOWN_CURVE(0),
    NIST_P256(2),
    NIST_P384(3),
    NIST_P521(4),
    UNRECOGNIZED(-1);
    
    public static final int NIST_P256_VALUE = 2;
    public static final int NIST_P384_VALUE = 3;
    public static final int NIST_P521_VALUE = 4;
    public static final int UNKNOWN_CURVE_VALUE = 0;
    public static final Internal.b<m1> internalValueMap = new a();
    public final int value;

    public class a implements Internal.b<m1> {
    }

    /* access modifiers changed from: public */
    EllipticCurveType(int i2) {
        this.value = i2;
    }

    public static EllipticCurveType a(int i2) {
        if (i2 == 0) {
            return UNKNOWN_CURVE;
        }
        if (i2 == 2) {
            return NIST_P256;
        }
        if (i2 == 3) {
            return NIST_P384;
        }
        if (i2 != 4) {
            return null;
        }
        return NIST_P521;
    }
}
