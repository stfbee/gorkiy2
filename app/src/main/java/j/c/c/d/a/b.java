package j.c.c.d.a;

import android.content.Context;
import android.os.Bundle;
import com.google.android.gms.measurement.AppMeasurement;
import com.google.firebase.FirebaseApp;
import i.b.k.ResourcesFlusher;
import j.c.c.a;
import j.c.c.f.d;
import java.util.concurrent.ConcurrentHashMap;

/* compiled from: com.google.android.gms:play-services-measurement-api@@17.0.1 */
public class b implements a {
    public static volatile a b;
    public final AppMeasurement a;

    public b(AppMeasurement appMeasurement) {
        ResourcesFlusher.b(appMeasurement);
        this.a = appMeasurement;
        new ConcurrentHashMap();
    }

    public static a a(FirebaseApp firebaseApp, Context context, d dVar) {
        ResourcesFlusher.b(firebaseApp);
        ResourcesFlusher.b(context);
        ResourcesFlusher.b(dVar);
        ResourcesFlusher.b(context.getApplicationContext());
        if (b == null) {
            synchronized (b.class) {
                if (b == null) {
                    Bundle bundle = new Bundle(1);
                    firebaseApp.a();
                    if ("[DEFAULT]".equals(firebaseApp.b)) {
                        dVar.a(a.class, e.a, d.a);
                        bundle.putBoolean("dataCollectionDefaultEnabled", firebaseApp.isDataCollectionDefaultEnabled());
                    }
                    b = new b(AppMeasurement.a(context, bundle));
                }
            }
        }
        return b;
    }
}
