package j.c.c.e;

import java.util.HashSet;
import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class l {
    public final d<?> a;
    public final Set<l> b = new HashSet();
    public final Set<l> c = new HashSet();

    public l(d<?> dVar) {
        this.a = dVar;
    }

    public boolean a() {
        return this.c.isEmpty();
    }
}
