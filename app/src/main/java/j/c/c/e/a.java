package j.c.c.e;

import java.util.Set;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public abstract class a {
    public <T> T a(Class cls) {
        j.c.c.h.a b = b(cls);
        if (b == null) {
            return null;
        }
        return b.get();
    }

    public abstract <T> j.c.c.h.a<T> b(Class<T> cls);

    public <T> Set<T> c(Class<T> cls) {
        return d(cls).get();
    }

    public abstract <T> j.c.c.h.a<Set<T>> d(Class<T> cls);
}
