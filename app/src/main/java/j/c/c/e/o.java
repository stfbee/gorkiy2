package j.c.c.e;

import j.c.c.f.a;
import j.c.c.f.b;
import java.util.Map;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public final /* synthetic */ class o implements Runnable {
    public final Map.Entry b;
    public final a c;

    public o(Map.Entry entry, a aVar) {
        this.b = entry;
        this.c = aVar;
    }

    public void run() {
        Map.Entry entry = this.b;
        ((b) entry.getKey()).a(this.c);
    }
}
