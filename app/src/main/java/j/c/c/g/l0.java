package j.c.c.g;

import com.google.firebase.iid.FirebaseInstanceId;
import j.c.a.a.c.n.c;
import j.c.a.a.i.a;
import j.c.a.a.i.e;

public final /* synthetic */ class l0 implements a {
    public final FirebaseInstanceId a;
    public final String b;
    public final String c;

    public l0(FirebaseInstanceId firebaseInstanceId, String str, String str2) {
        this.a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
    }

    public final Object a(e eVar) {
        FirebaseInstanceId firebaseInstanceId = this.a;
        String str = this.b;
        String str2 = this.c;
        if (firebaseInstanceId != null) {
            String g = FirebaseInstanceId.g();
            y a2 = FirebaseInstanceId.f556j.a("", str, str2);
            if (!firebaseInstanceId.d.b() && !firebaseInstanceId.a(a2)) {
                return c.b(new t0(g, a2.a));
            }
            return firebaseInstanceId.f558e.a(str, str2, new k0(firebaseInstanceId, g, y.a(a2), str, str2));
        }
        throw null;
    }
}
