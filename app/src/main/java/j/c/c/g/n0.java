package j.c.c.g;

import com.google.firebase.iid.FirebaseInstanceId;
import j.c.c.f.a;
import j.c.c.f.b;

public final /* synthetic */ class n0 implements b {
    public final FirebaseInstanceId.a a;

    public n0(FirebaseInstanceId.a aVar) {
        this.a = aVar;
    }

    public final void a(a aVar) {
        FirebaseInstanceId.a aVar2 = this.a;
        synchronized (aVar2) {
            if (aVar2.a()) {
                FirebaseInstanceId.this.b();
            }
        }
    }
}
