package j.c.c.g;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;
import android.util.Log;
import j.c.a.a.c.n.g.a;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;

public final class f0 implements ServiceConnection {
    public final Context a;
    public final Intent b;
    public final ScheduledExecutorService c;
    public final Queue<c0> d = new ArrayDeque();

    /* renamed from: e  reason: collision with root package name */
    public d0 f2523e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f2524f = false;

    public f0(Context context, String str) {
        ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = new ScheduledThreadPoolExecutor(0, new a("Firebase-FirebaseInstanceIdServiceConnection"));
        this.a = context.getApplicationContext();
        this.b = new Intent(str).setPackage(this.a.getPackageName());
        this.c = scheduledThreadPoolExecutor;
    }

    public final synchronized void a(Intent intent, BroadcastReceiver.PendingResult pendingResult) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            Log.d("EnhancedIntentService", "new intent queued in the bind-strategy delivery");
        }
        this.d.add(new c0(intent, pendingResult, this.c));
        a();
    }

    public final void b() {
        while (!this.d.isEmpty()) {
            this.d.poll().a();
        }
    }

    public final synchronized void onServiceConnected(ComponentName componentName, IBinder iBinder) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(valueOf.length() + 20);
            sb.append("onServiceConnected: ");
            sb.append(valueOf);
            Log.d("EnhancedIntentService", sb.toString());
        }
        this.f2524f = false;
        if (!(iBinder instanceof d0)) {
            String valueOf2 = String.valueOf(iBinder);
            StringBuilder sb2 = new StringBuilder(valueOf2.length() + 28);
            sb2.append("Invalid service connection: ");
            sb2.append(valueOf2);
            Log.e("EnhancedIntentService", sb2.toString());
            b();
            return;
        }
        this.f2523e = (d0) iBinder;
        a();
    }

    public final void onServiceDisconnected(ComponentName componentName) {
        if (Log.isLoggable("EnhancedIntentService", 3)) {
            String valueOf = String.valueOf(componentName);
            StringBuilder sb = new StringBuilder(valueOf.length() + 23);
            sb.append("onServiceDisconnected: ");
            sb.append(valueOf);
            Log.d("EnhancedIntentService", sb.toString());
        }
        a();
    }

    /* JADX WARNING: Code restructure failed: missing block: B:54:0x00ca, code lost:
        return;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final synchronized void a() {
        /*
            r6 = this;
            monitor-enter(r6)
            java.lang.String r0 = "EnhancedIntentService"
            r1 = 3
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x0011
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "flush queue called"
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x00cd }
        L_0x0011:
            java.util.Queue<j.c.c.g.c0> r0 = r6.d     // Catch:{ all -> 0x00cd }
            boolean r0 = r0.isEmpty()     // Catch:{ all -> 0x00cd }
            if (r0 != 0) goto L_0x00cb
            java.lang.String r0 = "EnhancedIntentService"
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x0028
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "found intent to be delivered"
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x00cd }
        L_0x0028:
            j.c.c.g.d0 r0 = r6.f2523e     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x0073
            j.c.c.g.d0 r0 = r6.f2523e     // Catch:{ all -> 0x00cd }
            boolean r0 = r0.isBinderAlive()     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x0073
            java.lang.String r0 = "EnhancedIntentService"
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00cd }
            if (r0 == 0) goto L_0x0043
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "binder is alive, sending the intent."
            android.util.Log.d(r0, r2)     // Catch:{ all -> 0x00cd }
        L_0x0043:
            java.util.Queue<j.c.c.g.c0> r0 = r6.d     // Catch:{ all -> 0x00cd }
            java.lang.Object r0 = r0.poll()     // Catch:{ all -> 0x00cd }
            j.c.c.g.c0 r0 = (j.c.c.g.c0) r0     // Catch:{ all -> 0x00cd }
            j.c.c.g.d0 r2 = r6.f2523e     // Catch:{ all -> 0x00cd }
            r3 = 0
            if (r2 == 0) goto L_0x0072
            int r2 = android.os.Binder.getCallingUid()     // Catch:{ all -> 0x00cd }
            int r4 = android.os.Process.myUid()     // Catch:{ all -> 0x00cd }
            if (r2 != r4) goto L_0x006a
            java.lang.String r2 = "EnhancedIntentService"
            boolean r1 = android.util.Log.isLoggable(r2, r1)     // Catch:{ all -> 0x00cd }
            if (r1 == 0) goto L_0x0067
            java.lang.String r1 = "service received new intent via bind strategy"
            android.util.Log.d(r2, r1)     // Catch:{ all -> 0x00cd }
        L_0x0067:
            android.content.Intent r0 = r0.a     // Catch:{ all -> 0x00cd }
            throw r3     // Catch:{ all -> 0x00cd }
        L_0x006a:
            java.lang.SecurityException r0 = new java.lang.SecurityException     // Catch:{ all -> 0x00cd }
            java.lang.String r1 = "Binding only allowed within app"
            r0.<init>(r1)     // Catch:{ all -> 0x00cd }
            throw r0     // Catch:{ all -> 0x00cd }
        L_0x0072:
            throw r3     // Catch:{ all -> 0x00cd }
        L_0x0073:
            java.lang.String r0 = "EnhancedIntentService"
            boolean r0 = android.util.Log.isLoggable(r0, r1)     // Catch:{ all -> 0x00cd }
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x009c
            java.lang.String r0 = "EnhancedIntentService"
            boolean r3 = r6.f2524f     // Catch:{ all -> 0x00cd }
            if (r3 != 0) goto L_0x0085
            r3 = 1
            goto L_0x0086
        L_0x0085:
            r3 = 0
        L_0x0086:
            r4 = 39
            java.lang.StringBuilder r5 = new java.lang.StringBuilder     // Catch:{ all -> 0x00cd }
            r5.<init>(r4)     // Catch:{ all -> 0x00cd }
            java.lang.String r4 = "binder is dead. start connection? "
            r5.append(r4)     // Catch:{ all -> 0x00cd }
            r5.append(r3)     // Catch:{ all -> 0x00cd }
            java.lang.String r3 = r5.toString()     // Catch:{ all -> 0x00cd }
            android.util.Log.d(r0, r3)     // Catch:{ all -> 0x00cd }
        L_0x009c:
            boolean r0 = r6.f2524f     // Catch:{ all -> 0x00cd }
            if (r0 != 0) goto L_0x00c9
            r6.f2524f = r2     // Catch:{ all -> 0x00cd }
            j.c.a.a.c.m.a r0 = j.c.a.a.c.m.a.a()     // Catch:{ SecurityException -> 0x00bc }
            android.content.Context r2 = r6.a     // Catch:{ SecurityException -> 0x00bc }
            android.content.Intent r3 = r6.b     // Catch:{ SecurityException -> 0x00bc }
            r4 = 65
            boolean r0 = r0.a(r2, r3, r6, r4)     // Catch:{ SecurityException -> 0x00bc }
            if (r0 == 0) goto L_0x00b4
            monitor-exit(r6)
            return
        L_0x00b4:
            java.lang.String r0 = "EnhancedIntentService"
            java.lang.String r2 = "binding to the service failed"
            android.util.Log.e(r0, r2)     // Catch:{ SecurityException -> 0x00bc }
            goto L_0x00c4
        L_0x00bc:
            r0 = move-exception
            java.lang.String r2 = "EnhancedIntentService"
            java.lang.String r3 = "Exception while binding the service"
            android.util.Log.e(r2, r3, r0)     // Catch:{ all -> 0x00cd }
        L_0x00c4:
            r6.f2524f = r1     // Catch:{ all -> 0x00cd }
            r6.b()     // Catch:{ all -> 0x00cd }
        L_0x00c9:
            monitor-exit(r6)
            return
        L_0x00cb:
            monitor-exit(r6)
            return
        L_0x00cd:
            r0 = move-exception
            monitor-exit(r6)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.g.f0.a():void");
    }
}
