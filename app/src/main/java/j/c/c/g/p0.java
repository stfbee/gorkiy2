package j.c.c.g;

import android.os.Build;
import android.os.Bundle;
import com.google.firebase.FirebaseApp;
import j.c.a.a.i.e;
import j.c.a.a.i.j;
import j.c.a.a.i.y;
import j.c.c.j.f;
import java.util.concurrent.Executor;

public final class p0 implements b {
    public final FirebaseApp a;
    public final o b;
    public final u c;
    public final Executor d;

    /* renamed from: e  reason: collision with root package name */
    public final f f2529e;

    public p0(FirebaseApp firebaseApp, o oVar, Executor executor, f fVar) {
        firebaseApp.a();
        u uVar = new u(firebaseApp.a, oVar);
        this.a = firebaseApp;
        this.b = oVar;
        this.c = uVar;
        this.d = executor;
        this.f2529e = fVar;
    }

    public final <T> e<Void> a(e<T> eVar) {
        Executor executor = e0.a;
        r0 r0Var = new r0();
        y yVar = (y) eVar;
        if (yVar != null) {
            y yVar2 = new y();
            yVar.b.a(new j(executor, r0Var, yVar2));
            yVar.f();
            return yVar2;
        }
        throw null;
    }

    public final e<Void> b(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    public final boolean b() {
        return false;
    }

    public final e<String> b(e<Bundle> eVar) {
        Executor executor = this.d;
        q0 q0Var = new q0(this);
        y yVar = (y) eVar;
        if (yVar != null) {
            y yVar2 = new y();
            yVar.b.a(new j(executor, q0Var, yVar2));
            yVar.f();
            return yVar2;
        }
        throw null;
    }

    public final boolean a() {
        return this.b.a() != 0;
    }

    public final e<String> a(String str, String str2, String str3, String str4) {
        return b(a(str, str3, str4, new Bundle()));
    }

    public final e<Void> a(String str, String str2, String str3) {
        Bundle bundle = new Bundle();
        String valueOf = String.valueOf(str3);
        bundle.putString("gcm.topic", valueOf.length() != 0 ? "/topics/".concat(valueOf) : new String("/topics/"));
        bundle.putString("delete", "1");
        String valueOf2 = String.valueOf(str3);
        return a(b(a(str, str2, valueOf2.length() != 0 ? "/topics/".concat(valueOf2) : new String("/topics/"), bundle)));
    }

    public final e<Bundle> a(String str, String str2, String str3, Bundle bundle) {
        bundle.putString("scope", str3);
        bundle.putString("sender", str2);
        bundle.putString("subtype", str2);
        bundle.putString("appid", str);
        FirebaseApp firebaseApp = this.a;
        firebaseApp.a();
        bundle.putString("gmp_app_id", firebaseApp.c.b);
        bundle.putString("gmsv", Integer.toString(this.b.d()));
        bundle.putString("osv", Integer.toString(Build.VERSION.SDK_INT));
        bundle.putString("app_ver", this.b.b());
        bundle.putString("app_ver_name", this.b.c());
        bundle.putString("cliv", "fiid-12451000");
        bundle.putString("Firebase-Client", this.f2529e.a());
        j.c.a.a.i.f fVar = new j.c.a.a.i.f();
        this.d.execute(new o0(this, bundle, fVar));
        return fVar.a;
    }
}
