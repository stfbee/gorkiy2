package j.c.c.g;

import android.content.Context;
import android.util.Log;
import j.c.a.a.f.d.a;
import j.c.a.a.f.d.b;
import j.c.a.a.i.e;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import javax.annotation.concurrent.GuardedBy;

public final class f {
    @GuardedBy("MessengerIpcClient.class")

    /* renamed from: e  reason: collision with root package name */
    public static f f2522e;
    public final Context a;
    public final ScheduledExecutorService b;
    @GuardedBy("this")
    public h c = new h(this, null);
    @GuardedBy("this")
    public int d = 1;

    public f(Context context, ScheduledExecutorService scheduledExecutorService) {
        this.b = scheduledExecutorService;
        this.a = context.getApplicationContext();
    }

    public static synchronized f a(Context context) {
        f fVar;
        synchronized (f.class) {
            if (f2522e == null) {
                b bVar = a.a;
                f2522e = new f(context, Executors.unconfigurableScheduledExecutorService(Executors.newScheduledThreadPool(1, new j.c.a.a.c.n.g.a("MessengerIpcClient"))));
            }
            fVar = f2522e;
        }
        return fVar;
    }

    public final synchronized <T> e<T> a(m<T> mVar) {
        if (Log.isLoggable("MessengerIpcClient", 3)) {
            String valueOf = String.valueOf(mVar);
            StringBuilder sb = new StringBuilder(valueOf.length() + 9);
            sb.append("Queueing ");
            sb.append(valueOf);
            Log.d("MessengerIpcClient", sb.toString());
        }
        if (!this.c.a(mVar)) {
            h hVar = new h(this, null);
            this.c = hVar;
            hVar.a(mVar);
        }
        return mVar.b.a;
    }

    public final synchronized int a() {
        int i2;
        i2 = this.d;
        this.d = i2 + 1;
        return i2;
    }
}
