package j.c.c.g;

import com.google.firebase.iid.FirebaseInstanceId;
import j.c.a.a.c.n.c;
import j.c.a.a.i.e;

public final /* synthetic */ class m0 {
    public final FirebaseInstanceId a;
    public final String b;
    public final String c;
    public final String d;

    public m0(FirebaseInstanceId firebaseInstanceId, String str, String str2, String str3) {
        this.a = firebaseInstanceId;
        this.b = str;
        this.c = str2;
        this.d = str3;
    }

    public final e a(Object obj) {
        FirebaseInstanceId firebaseInstanceId = this.a;
        String str = this.b;
        String str2 = this.c;
        String str3 = this.d;
        String str4 = (String) obj;
        FirebaseInstanceId.f556j.a("", str, str2, str4, firebaseInstanceId.c.b());
        return c.b(new t0(str3, str4));
    }
}
