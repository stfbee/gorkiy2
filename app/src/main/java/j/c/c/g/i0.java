package j.c.c.g;

import android.os.IBinder;
import android.os.Parcel;
import android.os.Parcelable;

public final class i0 implements Parcelable.Creator<j0> {
    public final /* synthetic */ Object createFromParcel(Parcel parcel) {
        IBinder readStrongBinder = parcel.readStrongBinder();
        if (readStrongBinder != null) {
            return new j0(readStrongBinder);
        }
        return null;
    }

    public final /* synthetic */ Object[] newArray(int i2) {
        return new j0[i2];
    }
}
