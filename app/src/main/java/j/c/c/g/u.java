package j.c.c.g;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.os.Message;
import android.os.Messenger;
import android.os.Parcelable;
import android.util.Log;
import com.crashlytics.android.core.CrashlyticsController;
import com.google.firebase.iid.zzam;
import i.e.SimpleArrayMap;
import j.c.a.a.c.n.c;
import j.c.a.a.i.f;
import j.c.c.g.j0;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.concurrent.GuardedBy;

public final class u {
    public static int g;
    public static PendingIntent h;
    @GuardedBy("responseCallbacks")
    public final SimpleArrayMap<String, f<Bundle>> a = new SimpleArrayMap<>();
    public final Context b;
    public final o c;
    public Messenger d;

    /* renamed from: e  reason: collision with root package name */
    public Messenger f2530e;

    /* renamed from: f  reason: collision with root package name */
    public j0 f2531f;

    public u(Context context, o oVar) {
        this.b = context;
        this.c = oVar;
        this.d = new Messenger(new t(this, Looper.getMainLooper()));
    }

    public final void a(Message message) {
        if (message != null) {
            Object obj = message.obj;
            if (obj instanceof Intent) {
                Intent intent = (Intent) obj;
                intent.setExtrasClassLoader(new j0.a());
                if (intent.hasExtra("google.messenger")) {
                    Parcelable parcelableExtra = intent.getParcelableExtra("google.messenger");
                    if (parcelableExtra instanceof j0) {
                        this.f2531f = (j0) parcelableExtra;
                    }
                    if (parcelableExtra instanceof Messenger) {
                        this.f2530e = (Messenger) parcelableExtra;
                    }
                }
                Intent intent2 = (Intent) message.obj;
                String action = intent2.getAction();
                if ("com.google.android.c2dm.intent.REGISTRATION".equals(action)) {
                    String stringExtra = intent2.getStringExtra("registration_id");
                    if (stringExtra == null) {
                        stringExtra = intent2.getStringExtra("unregistered");
                    }
                    if (stringExtra == null) {
                        String stringExtra2 = intent2.getStringExtra(CrashlyticsController.EVENT_TYPE_LOGGED);
                        if (stringExtra2 == null) {
                            String valueOf = String.valueOf(intent2.getExtras());
                            StringBuilder sb = new StringBuilder(valueOf.length() + 49);
                            sb.append("Unexpected response, no error or registration id ");
                            sb.append(valueOf);
                            Log.w("FirebaseInstanceId", sb.toString());
                            return;
                        }
                        if (Log.isLoggable("FirebaseInstanceId", 3)) {
                            Log.d("FirebaseInstanceId", stringExtra2.length() != 0 ? "Received InstanceID error ".concat(stringExtra2) : new String("Received InstanceID error "));
                        }
                        if (stringExtra2.startsWith("|")) {
                            String[] split = stringExtra2.split("\\|");
                            if (split.length <= 2 || !"ID".equals(split[1])) {
                                Log.w("FirebaseInstanceId", stringExtra2.length() != 0 ? "Unexpected structured response ".concat(stringExtra2) : new String("Unexpected structured response "));
                                return;
                            }
                            String str = split[2];
                            String str2 = split[3];
                            if (str2.startsWith(":")) {
                                str2 = str2.substring(1);
                            }
                            a(str, intent2.putExtra(CrashlyticsController.EVENT_TYPE_LOGGED, str2).getExtras());
                            return;
                        }
                        synchronized (this.a) {
                            for (int i2 = 0; i2 < this.a.d; i2++) {
                                a(this.a.c(i2), intent2.getExtras());
                            }
                        }
                        return;
                    }
                    Matcher matcher = Pattern.compile("\\|ID\\|([^|]+)\\|:?+(.*)").matcher(stringExtra);
                    if (matcher.matches()) {
                        String group = matcher.group(1);
                        String group2 = matcher.group(2);
                        Bundle extras = intent2.getExtras();
                        extras.putString("registration_id", group2);
                        a(group, extras);
                        return;
                    } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                        Log.d("FirebaseInstanceId", stringExtra.length() != 0 ? "Unexpected response string: ".concat(stringExtra) : new String("Unexpected response string: "));
                        return;
                    } else {
                        return;
                    }
                } else if (Log.isLoggable("FirebaseInstanceId", 3)) {
                    String valueOf2 = String.valueOf(action);
                    Log.d("FirebaseInstanceId", valueOf2.length() != 0 ? "Unexpected response action: ".concat(valueOf2) : new String("Unexpected response action: "));
                    return;
                } else {
                    return;
                }
            }
        }
        Log.w("FirebaseInstanceId", "Dropping invalid message");
    }

    public final Bundle b(Bundle bundle) {
        Bundle c2 = c(bundle);
        if (c2 == null || !c2.containsKey("google.messenger")) {
            return c2;
        }
        Bundle c3 = c(bundle);
        if (c3 == null || !c3.containsKey("google.messenger")) {
            return c3;
        }
        return null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:36:0x00e9 A[SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final android.os.Bundle c(android.os.Bundle r8) {
        /*
            r7 = this;
            java.lang.String r0 = a()
            j.c.a.a.i.f r1 = new j.c.a.a.i.f
            r1.<init>()
            i.e.SimpleArrayMap<java.lang.String, j.c.a.a.i.f<android.os.Bundle>> r2 = r7.a
            monitor-enter(r2)
            i.e.SimpleArrayMap<java.lang.String, j.c.a.a.i.f<android.os.Bundle>> r3 = r7.a     // Catch:{ all -> 0x0120 }
            r3.put(r0, r1)     // Catch:{ all -> 0x0120 }
            monitor-exit(r2)     // Catch:{ all -> 0x0120 }
            j.c.c.g.o r2 = r7.c
            int r2 = r2.a()
            if (r2 == 0) goto L_0x0118
            android.content.Intent r2 = new android.content.Intent
            r2.<init>()
            java.lang.String r3 = "com.google.android.gms"
            r2.setPackage(r3)
            j.c.c.g.o r3 = r7.c
            int r3 = r3.a()
            r4 = 2
            if (r3 != r4) goto L_0x0033
            java.lang.String r3 = "com.google.iid.TOKEN_REQUEST"
            r2.setAction(r3)
            goto L_0x0038
        L_0x0033:
            java.lang.String r3 = "com.google.android.c2dm.intent.REGISTER"
            r2.setAction(r3)
        L_0x0038:
            r2.putExtras(r8)
            android.content.Context r8 = r7.b
            a(r8, r2)
            java.lang.String r8 = java.lang.String.valueOf(r0)
            int r8 = r8.length()
            int r8 = r8 + 5
            java.lang.StringBuilder r3 = new java.lang.StringBuilder
            r3.<init>(r8)
            java.lang.String r8 = "|ID|"
            r3.append(r8)
            r3.append(r0)
            java.lang.String r8 = "|"
            r3.append(r8)
            java.lang.String r8 = r3.toString()
            java.lang.String r3 = "kid"
            r2.putExtra(r3, r8)
            r8 = 3
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r3 = android.util.Log.isLoggable(r3, r8)
            if (r3 == 0) goto L_0x0092
            android.os.Bundle r3 = r2.getExtras()
            java.lang.String r3 = java.lang.String.valueOf(r3)
            int r5 = r3.length()
            int r5 = r5 + 8
            java.lang.StringBuilder r6 = new java.lang.StringBuilder
            r6.<init>(r5)
            java.lang.String r5 = "Sending "
            r6.append(r5)
            r6.append(r3)
            java.lang.String r3 = r6.toString()
            java.lang.String r5 = "FirebaseInstanceId"
            android.util.Log.d(r5, r3)
        L_0x0092:
            android.os.Messenger r3 = r7.d
            java.lang.String r5 = "google.messenger"
            r2.putExtra(r5, r3)
            android.os.Messenger r3 = r7.f2530e
            if (r3 != 0) goto L_0x00a1
            j.c.c.g.j0 r3 = r7.f2531f
            if (r3 == 0) goto L_0x00c7
        L_0x00a1:
            android.os.Message r3 = android.os.Message.obtain()
            r3.obj = r2
            android.os.Messenger r5 = r7.f2530e     // Catch:{ RemoteException -> 0x00b7 }
            if (r5 == 0) goto L_0x00b1
            android.os.Messenger r5 = r7.f2530e     // Catch:{ RemoteException -> 0x00b7 }
            r5.send(r3)     // Catch:{ RemoteException -> 0x00b7 }
            goto L_0x00da
        L_0x00b1:
            j.c.c.g.j0 r5 = r7.f2531f     // Catch:{ RemoteException -> 0x00b7 }
            r5.a(r3)     // Catch:{ RemoteException -> 0x00b7 }
            goto L_0x00da
        L_0x00b7:
            java.lang.String r3 = "FirebaseInstanceId"
            boolean r8 = android.util.Log.isLoggable(r3, r8)
            if (r8 == 0) goto L_0x00c7
            java.lang.String r8 = "FirebaseInstanceId"
            java.lang.String r3 = "Messenger failed, fallback to startService"
            android.util.Log.d(r8, r3)
        L_0x00c7:
            j.c.c.g.o r8 = r7.c
            int r8 = r8.a()
            if (r8 != r4) goto L_0x00d5
            android.content.Context r8 = r7.b
            r8.sendBroadcast(r2)
            goto L_0x00da
        L_0x00d5:
            android.content.Context r8 = r7.b
            r8.startService(r2)
        L_0x00da:
            j.c.a.a.i.y<TResult> r8 = r1.a     // Catch:{ InterruptedException | TimeoutException -> 0x00fc, ExecutionException -> 0x00f5 }
            r1 = 30000(0x7530, double:1.4822E-319)
            java.util.concurrent.TimeUnit r3 = java.util.concurrent.TimeUnit.MILLISECONDS     // Catch:{ InterruptedException | TimeoutException -> 0x00fc, ExecutionException -> 0x00f5 }
            java.lang.Object r8 = j.c.a.a.c.n.c.a(r8, r1, r3)     // Catch:{ InterruptedException | TimeoutException -> 0x00fc, ExecutionException -> 0x00f5 }
            android.os.Bundle r8 = (android.os.Bundle) r8     // Catch:{ InterruptedException | TimeoutException -> 0x00fc, ExecutionException -> 0x00f5 }
            i.e.SimpleArrayMap<java.lang.String, j.c.a.a.i.f<android.os.Bundle>> r1 = r7.a
            monitor-enter(r1)
            i.e.SimpleArrayMap<java.lang.String, j.c.a.a.i.f<android.os.Bundle>> r2 = r7.a     // Catch:{ all -> 0x00f0 }
            r2.remove(r0)     // Catch:{ all -> 0x00f0 }
            monitor-exit(r1)     // Catch:{ all -> 0x00f0 }
            return r8
        L_0x00f0:
            r8 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x00f0 }
            throw r8
        L_0x00f3:
            r8 = move-exception
            goto L_0x010b
        L_0x00f5:
            r8 = move-exception
            java.io.IOException r1 = new java.io.IOException     // Catch:{ all -> 0x00f3 }
            r1.<init>(r8)     // Catch:{ all -> 0x00f3 }
            throw r1     // Catch:{ all -> 0x00f3 }
        L_0x00fc:
            java.lang.String r8 = "FirebaseInstanceId"
            java.lang.String r1 = "No response"
            android.util.Log.w(r8, r1)     // Catch:{ all -> 0x00f3 }
            java.io.IOException r8 = new java.io.IOException     // Catch:{ all -> 0x00f3 }
            java.lang.String r1 = "TIMEOUT"
            r8.<init>(r1)     // Catch:{ all -> 0x00f3 }
            throw r8     // Catch:{ all -> 0x00f3 }
        L_0x010b:
            i.e.SimpleArrayMap<java.lang.String, j.c.a.a.i.f<android.os.Bundle>> r1 = r7.a
            monitor-enter(r1)
            i.e.SimpleArrayMap<java.lang.String, j.c.a.a.i.f<android.os.Bundle>> r2 = r7.a     // Catch:{ all -> 0x0115 }
            r2.remove(r0)     // Catch:{ all -> 0x0115 }
            monitor-exit(r1)     // Catch:{ all -> 0x0115 }
            throw r8
        L_0x0115:
            r8 = move-exception
            monitor-exit(r1)     // Catch:{ all -> 0x0115 }
            throw r8
        L_0x0118:
            java.io.IOException r8 = new java.io.IOException
            java.lang.String r0 = "MISSING_INSTANCEID_SERVICE"
            r8.<init>(r0)
            throw r8
        L_0x0120:
            r8 = move-exception
            monitor-exit(r2)     // Catch:{ all -> 0x0120 }
            throw r8
        */
        throw new UnsupportedOperationException("Method not decompiled: j.c.c.g.u.c(android.os.Bundle):android.os.Bundle");
    }

    public static synchronized void a(Context context, Intent intent) {
        synchronized (u.class) {
            if (h == null) {
                Intent intent2 = new Intent();
                intent2.setPackage("com.google.example.invalidpackage");
                h = PendingIntent.getBroadcast(context, 0, intent2, 0);
            }
            intent.putExtra("app", h);
        }
    }

    public final void a(String str, Bundle bundle) {
        synchronized (this.a) {
            f remove = this.a.remove(str);
            if (remove == null) {
                String valueOf = String.valueOf(str);
                Log.w("FirebaseInstanceId", valueOf.length() != 0 ? "Missing callback for ".concat(valueOf) : new String("Missing callback for "));
                return;
            }
            remove.a.a(bundle);
        }
    }

    public final Bundle a(Bundle bundle) {
        if (this.c.d() < 12000000) {
            return b(bundle);
        }
        f a2 = f.a(this.b);
        try {
            return (Bundle) c.a(a2.a(new n(a2.a(), bundle)));
        } catch (InterruptedException | ExecutionException e2) {
            if (Log.isLoggable("FirebaseInstanceId", 3)) {
                String valueOf = String.valueOf(e2);
                StringBuilder sb = new StringBuilder(valueOf.length() + 22);
                sb.append("Error making request: ");
                sb.append(valueOf);
                Log.d("FirebaseInstanceId", sb.toString());
            }
            if (!(e2.getCause() instanceof zzam) || ((zzam) e2.getCause()).b != 4) {
                return null;
            }
            return b(bundle);
        }
    }

    public static synchronized String a() {
        String num;
        synchronized (u.class) {
            int i2 = g;
            g = i2 + 1;
            num = Integer.toString(i2);
        }
        return num;
    }
}
