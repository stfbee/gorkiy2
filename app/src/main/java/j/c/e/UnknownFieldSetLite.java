package j.c.e;

import java.util.Arrays;

public final class UnknownFieldSetLite {
    public static final UnknownFieldSetLite d = new UnknownFieldSetLite(0, new int[0], new Object[0], false);
    public int a;
    public int[] b;
    public Object[] c;

    public UnknownFieldSetLite() {
        this(0, new int[8], new Object[8], true);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || !(obj instanceof UnknownFieldSetLite)) {
            return false;
        }
        UnknownFieldSetLite unknownFieldSetLite = (UnknownFieldSetLite) obj;
        return this.a == unknownFieldSetLite.a && Arrays.equals(this.b, unknownFieldSetLite.b) && Arrays.deepEquals(this.c, unknownFieldSetLite.c);
    }

    public int hashCode() {
        int hashCode = Arrays.hashCode(this.b);
        return Arrays.deepHashCode(this.c) + ((hashCode + ((527 + this.a) * 31)) * 31);
    }

    public UnknownFieldSetLite(int i2, int[] iArr, Object[] objArr, boolean z) {
        this.a = i2;
        this.b = iArr;
        this.c = objArr;
    }
}
