package j.c.e;

import com.google.protobuf.InvalidProtocolBufferException;
import j.c.e.l;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.util.List;
import java.util.RandomAccess;

public final class Internal {
    public static final Charset a = Charset.forName("UTF-8");
    public static final byte[] b;

    public interface a {
    }

    public interface b<T extends l.a> {
    }

    public interface c<E> extends List<E>, RandomAccess {
    }

    static {
        Charset.forName("ISO-8859-1");
        byte[] bArr = new byte[0];
        b = bArr;
        ByteBuffer.wrap(bArr);
        byte[] bArr2 = b;
        int length = bArr2.length;
        try {
            new CodedInputStream(bArr2, 0, length, false).b(length);
        } catch (InvalidProtocolBufferException e2) {
            throw new IllegalArgumentException(e2);
        }
    }

    public static int a(int i2, byte[] bArr, int i3, int i4) {
        for (int i5 = i3; i5 < i3 + i4; i5++) {
            i2 = (i2 * 31) + bArr[i5];
        }
        return i2;
    }

    public static int a(boolean z) {
        return z ? 1231 : 1237;
    }
}
