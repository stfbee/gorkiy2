package j.c.e;

import com.google.protobuf.CodedOutputStream;

public interface MessageLite extends MessageLiteOrBuilder {

    public interface a extends MessageLiteOrBuilder, Cloneable {
    }

    void a(CodedOutputStream codedOutputStream);

    byte[] b();

    int c();

    a e();

    Parser<? extends o> i();
}
