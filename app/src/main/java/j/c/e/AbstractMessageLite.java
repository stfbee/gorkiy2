package j.c.e;

import com.google.protobuf.CodedOutputStream;
import j.a.a.a.outline;
import j.c.e.AbstractMessageLite;
import j.c.e.AbstractMessageLite.a;
import j.c.e.ByteString;
import j.c.e.o;
import java.io.IOException;

public abstract class AbstractMessageLite<MessageType extends AbstractMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> implements o {
    public int b = 0;

    public static abstract class a<MessageType extends AbstractMessageLite<MessageType, BuilderType>, BuilderType extends a<MessageType, BuilderType>> implements o.a {
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [j.c.e.MessageLite, j.c.e.AbstractMessageLite] */
    public ByteString a() {
        try {
            ByteString.e d = ByteString.d(c());
            a(d.a);
            if (d.a.a() == 0) {
                return new ByteString.g(d.b);
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e2) {
            throw new RuntimeException(a("ByteString"), e2);
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [j.c.e.MessageLite, j.c.e.AbstractMessageLite] */
    public byte[] b() {
        try {
            byte[] bArr = new byte[c()];
            CodedOutputStream a2 = CodedOutputStream.a(bArr);
            a(a2);
            if (a2.a() == 0) {
                return bArr;
            }
            throw new IllegalStateException("Did not write as much data as expected.");
        } catch (IOException e2) {
            throw new RuntimeException(a("byte array"), e2);
        }
    }

    public final String a(String str) {
        StringBuilder a2 = outline.a("Serializing ");
        a2.append(getClass().getName());
        a2.append(" to a ");
        a2.append(str);
        a2.append(" threw an IOException (should never happen).");
        return a2.toString();
    }
}
