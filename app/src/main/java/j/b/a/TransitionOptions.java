package j.b.a;

import j.b.a.j;
import j.b.a.q.i.NoTransition;
import j.b.a.q.i.TransitionFactory;

public abstract class TransitionOptions<CHILD extends j<CHILD, TranscodeType>, TranscodeType> implements Cloneable {
    public TransitionFactory<? super TranscodeType> b = NoTransition.b;

    public final CHILD clone() {
        try {
            return (TransitionOptions) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }

    /* renamed from: clone  reason: collision with other method in class */
    public Object m11clone() {
        try {
            return (TransitionOptions) super.clone();
        } catch (CloneNotSupportedException e2) {
            throw new RuntimeException(e2);
        }
    }
}
