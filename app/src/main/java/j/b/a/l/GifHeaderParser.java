package j.b.a.l;

import android.util.Log;
import java.nio.BufferUnderflowException;
import java.nio.ByteBuffer;

public class GifHeaderParser {
    public final byte[] a = new byte[256];
    public ByteBuffer b;
    public GifHeader c;
    public int d = 0;

    public final int[] a(int i2) {
        byte[] bArr = new byte[(i2 * 3)];
        int[] iArr = null;
        try {
            this.b.get(bArr);
            iArr = new int[256];
            int i3 = 0;
            int i4 = 0;
            while (i3 < i2) {
                int i5 = i4 + 1;
                int i6 = i5 + 1;
                int i7 = i6 + 1;
                int i8 = i3 + 1;
                iArr[i3] = ((bArr[i4] & 255) << 16) | -16777216 | ((bArr[i5] & 255) << 8) | (bArr[i6] & 255);
                i4 = i7;
                i3 = i8;
            }
        } catch (BufferUnderflowException e2) {
            if (Log.isLoggable("GifHeaderParser", 3)) {
                Log.d("GifHeaderParser", "Format Error Reading Color Table", e2);
            }
            this.c.b = 1;
        }
        return iArr;
    }

    public GifHeader b() {
        if (this.b == null) {
            throw new IllegalStateException("You must call setData() before parseHeader()");
        } else if (a()) {
            return this.c;
        } else {
            StringBuilder sb = new StringBuilder();
            for (int i2 = 0; i2 < 6; i2++) {
                sb.append((char) c());
            }
            if (!sb.toString().startsWith("GIF")) {
                this.c.b = 1;
            } else {
                this.c.f1577f = e();
                this.c.g = e();
                int c2 = c();
                this.c.h = (c2 & 128) != 0;
                this.c.f1578i = (int) Math.pow(2.0d, (double) ((c2 & 7) + 1));
                this.c.f1579j = c();
                this.c.f1580k = c();
                if (this.c.h && !a()) {
                    GifHeader gifHeader = this.c;
                    gifHeader.a = a(gifHeader.f1578i);
                    GifHeader gifHeader2 = this.c;
                    gifHeader2.f1581l = gifHeader2.a[gifHeader2.f1579j];
                }
            }
            if (!a()) {
                boolean z = false;
                while (!z && !a() && this.c.c <= Integer.MAX_VALUE) {
                    int c3 = c();
                    if (c3 == 33) {
                        int c4 = c();
                        if (c4 == 1) {
                            f();
                        } else if (c4 == 249) {
                            this.c.d = new GifFrame();
                            c();
                            int c5 = c();
                            GifFrame gifFrame = this.c.d;
                            int i3 = (c5 & 28) >> 2;
                            gifFrame.g = i3;
                            if (i3 == 0) {
                                gifFrame.g = 1;
                            }
                            this.c.d.f1572f = (c5 & 1) != 0;
                            int e2 = e();
                            if (e2 < 2) {
                                e2 = 10;
                            }
                            GifFrame gifFrame2 = this.c.d;
                            gifFrame2.f1573i = e2 * 10;
                            gifFrame2.h = c();
                            c();
                        } else if (c4 == 254) {
                            f();
                        } else if (c4 != 255) {
                            f();
                        } else {
                            d();
                            StringBuilder sb2 = new StringBuilder();
                            for (int i4 = 0; i4 < 11; i4++) {
                                sb2.append((char) this.a[i4]);
                            }
                            if (sb2.toString().equals("NETSCAPE2.0")) {
                                do {
                                    d();
                                    byte[] bArr = this.a;
                                    if (bArr[0] == 1) {
                                        this.c.f1582m = ((bArr[2] & 255) << 8) | (bArr[1] & 255);
                                    }
                                    if (this.d <= 0) {
                                        break;
                                    }
                                } while (!a());
                            } else {
                                f();
                            }
                        }
                    } else if (c3 == 44) {
                        GifHeader gifHeader3 = this.c;
                        if (gifHeader3.d == null) {
                            gifHeader3.d = new GifFrame();
                        }
                        this.c.d.a = e();
                        this.c.d.b = e();
                        this.c.d.c = e();
                        this.c.d.d = e();
                        int c6 = c();
                        boolean z2 = (c6 & 128) != 0;
                        int pow = (int) Math.pow(2.0d, (double) ((c6 & 7) + 1));
                        this.c.d.f1571e = (c6 & 64) != 0;
                        if (z2) {
                            this.c.d.f1575k = a(pow);
                        } else {
                            this.c.d.f1575k = null;
                        }
                        this.c.d.f1574j = this.b.position();
                        c();
                        f();
                        if (!a()) {
                            GifHeader gifHeader4 = this.c;
                            gifHeader4.c++;
                            gifHeader4.f1576e.add(gifHeader4.d);
                        }
                    } else if (c3 != 59) {
                        this.c.b = 1;
                    } else {
                        z = true;
                    }
                }
                GifHeader gifHeader5 = this.c;
                if (gifHeader5.c < 0) {
                    gifHeader5.b = 1;
                }
            }
            return this.c;
        }
    }

    public final int c() {
        try {
            return this.b.get() & 255;
        } catch (Exception unused) {
            this.c.b = 1;
            return 0;
        }
    }

    public final void d() {
        int c2 = c();
        this.d = c2;
        if (c2 > 0) {
            int i2 = 0;
            int i3 = 0;
            while (i2 < this.d) {
                try {
                    i3 = this.d - i2;
                    this.b.get(this.a, i2, i3);
                    i2 += i3;
                } catch (Exception e2) {
                    if (Log.isLoggable("GifHeaderParser", 3)) {
                        Log.d("GifHeaderParser", "Error Reading Block n: " + i2 + " count: " + i3 + " blockSize: " + this.d, e2);
                    }
                    this.c.b = 1;
                    return;
                }
            }
        }
    }

    public final int e() {
        return this.b.getShort();
    }

    public final void f() {
        int c2;
        do {
            c2 = c();
            this.b.position(Math.min(this.b.position() + c2, this.b.limit()));
        } while (c2 > 0);
    }

    public final boolean a() {
        return this.c.b != 0;
    }
}
