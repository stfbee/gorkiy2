package j.b.a.p;

import i.e.ArrayMap;
import j.b.a.s.MultiClassKey;
import j.b.a.s.i;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

public class ModelToResourceClassCache {
    public final AtomicReference<i> a = new AtomicReference<>();
    public final ArrayMap<i, List<Class<?>>> b = new ArrayMap<>();

    public List<Class<?>> a(Class<?> cls, Class<?> cls2, Class<?> cls3) {
        List<Class<?>> orDefault;
        MultiClassKey andSet = this.a.getAndSet(null);
        if (andSet == null) {
            andSet = new MultiClassKey(cls, cls2, cls3);
        } else {
            andSet.a = cls;
            andSet.b = cls2;
            andSet.c = cls3;
        }
        synchronized (this.b) {
            orDefault = this.b.getOrDefault(andSet, null);
        }
        this.a.set(andSet);
        return orDefault;
    }

    public void a(Class<?> cls, Class<?> cls2, Class<?> cls3, List<Class<?>> list) {
        synchronized (this.b) {
            this.b.put(new MultiClassKey(cls, cls2, cls3), list);
        }
    }
}
