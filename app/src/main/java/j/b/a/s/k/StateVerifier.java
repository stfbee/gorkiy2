package j.b.a.s.k;

public abstract class StateVerifier {

    public static class b extends StateVerifier {
        public volatile boolean a;

        public b() {
            super(null);
        }

        public void a() {
            if (this.a) {
                throw new IllegalStateException("Already released");
            }
        }
    }

    public /* synthetic */ StateVerifier(a aVar) {
    }

    public abstract void a();
}
