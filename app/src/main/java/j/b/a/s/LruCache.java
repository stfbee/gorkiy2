package j.b.a.s;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<T, Y> {
    public final Map<T, Y> a = new LinkedHashMap(100, 0.75f, true);
    public long b;
    public long c;

    public LruCache(long j2) {
        this.b = j2;
    }

    public synchronized long a() {
        return this.b;
    }

    public void a(Object obj, Object obj2) {
    }

    public int b(Y y) {
        return 1;
    }

    public synchronized Y b(T t2, Y y) {
        long b2 = (long) b(y);
        if (b2 >= this.b) {
            a(t2, y);
            return null;
        }
        if (y != null) {
            this.c += b2;
        }
        Y put = this.a.put(t2, y);
        if (put != null) {
            this.c -= (long) b(put);
            if (!put.equals(y)) {
                a(t2, put);
            }
        }
        a(this.b);
        return put;
    }

    public synchronized Y c(T t2) {
        Y remove;
        remove = this.a.remove(t2);
        if (remove != null) {
            this.c -= (long) b(remove);
        }
        return remove;
    }

    public synchronized Y a(Object obj) {
        return this.a.get(obj);
    }

    public synchronized void a(long j2) {
        while (this.c > j2) {
            Iterator<Map.Entry<T, Y>> it = this.a.entrySet().iterator();
            Map.Entry next = it.next();
            Object value = next.getValue();
            this.c -= (long) b(value);
            Object key = next.getKey();
            it.remove();
            a(key, value);
        }
    }
}
