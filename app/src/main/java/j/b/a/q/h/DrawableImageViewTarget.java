package j.b.a.q.h;

import android.graphics.drawable.Drawable;
import android.widget.ImageView;

public class DrawableImageViewTarget extends ImageViewTarget<Drawable> {
    public DrawableImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    public void a(Object obj) {
        ((ImageView) this.b).setImageDrawable((Drawable) obj);
    }
}
