package j.b.a.q.h;

import android.graphics.drawable.Animatable;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import j.b.a.q.i.Transition;

public abstract class ImageViewTarget<Z> extends ViewTarget<ImageView, Z> {

    /* renamed from: e  reason: collision with root package name */
    public Animatable f1772e;

    public ImageViewTarget(ImageView imageView) {
        super(imageView);
    }

    public void a(Drawable drawable) {
        b((Object) null);
        ((ImageView) super.b).setImageDrawable(drawable);
    }

    public abstract void a(Z z);

    public void b(Drawable drawable) {
        b((Object) null);
        ((ImageView) super.b).setImageDrawable(drawable);
    }

    public void c() {
        Animatable animatable = this.f1772e;
        if (animatable != null) {
            animatable.start();
        }
    }

    public void a(Z z, Transition<? super Z> transition) {
        b((Object) z);
    }

    public final void b(Z z) {
        a((Object) z);
        if (z instanceof Animatable) {
            Animatable animatable = (Animatable) z;
            this.f1772e = animatable;
            animatable.start();
            return;
        }
        this.f1772e = null;
    }

    public void c(Drawable drawable) {
        super.c.a();
        Animatable animatable = this.f1772e;
        if (animatable != null) {
            animatable.stop();
        }
        b((Object) null);
        ((ImageView) super.b).setImageDrawable(drawable);
    }

    public void a() {
        Animatable animatable = this.f1772e;
        if (animatable != null) {
            animatable.stop();
        }
    }
}
