package j.b.a.m.o.c;

import android.graphics.Bitmap;
import android.os.ParcelFileDescriptor;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.o.c.ImageReader;
import j.b.a.m.o.c.l;

public final class ParcelFileDescriptorBitmapDecoder implements ResourceDecoder<ParcelFileDescriptor, Bitmap> {
    public final Downsampler a;

    public ParcelFileDescriptorBitmapDecoder(Downsampler downsampler) {
        this.a = downsampler;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.b.a.m.o.c.Downsampler.a(j.b.a.m.o.c.r, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap>
     arg types: [j.b.a.m.o.c.ImageReader$b, int, int, j.b.a.m.Options, j.b.a.m.o.c.Downsampler$b]
     candidates:
      j.b.a.m.o.c.Downsampler.a(java.lang.IllegalArgumentException, int, int, java.lang.String, android.graphics.BitmapFactory$Options):java.io.IOException
      j.b.a.m.o.c.Downsampler.a(java.io.InputStream, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap>
      j.b.a.m.o.c.Downsampler.a(j.b.a.m.o.c.r, int, int, j.b.a.m.g, j.b.a.m.o.c.l$b):j.b.a.m.m.Resource<android.graphics.Bitmap> */
    public Resource a(Object obj, int i2, int i3, Options options) {
        Downsampler downsampler = this.a;
        return downsampler.a((r) new ImageReader.b((ParcelFileDescriptor) obj, downsampler.d, downsampler.c), i2, i3, (g) options, (l.b) Downsampler.f1708k);
    }

    public boolean a(Object obj, Options options) {
        ParcelFileDescriptor parcelFileDescriptor = (ParcelFileDescriptor) obj;
        if (this.a != null) {
            return true;
        }
        throw null;
    }
}
