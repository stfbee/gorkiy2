package j.b.a.m.n;

import android.util.Log;
import j.b.a.m.Encoder;
import j.b.a.m.Options;
import j.b.a.s.ByteBufferUtil;
import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;

public class ByteBufferEncoder implements Encoder<ByteBuffer> {
    public boolean a(Object obj, File file, Options options) {
        try {
            ByteBufferUtil.a((ByteBuffer) obj, file);
            return true;
        } catch (IOException e2) {
            if (Log.isLoggable("ByteBufferEncoder", 3)) {
                Log.d("ByteBufferEncoder", "Failed to write data", e2);
            }
            return false;
        }
    }
}
