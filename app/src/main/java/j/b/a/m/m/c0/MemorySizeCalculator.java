package j.b.a.m.m.c0;

import android.app.ActivityManager;
import android.content.Context;
import android.os.Build;
import android.text.format.Formatter;
import android.util.DisplayMetrics;
import android.util.Log;
import com.crashlytics.android.answers.SessionEvent;
import j.a.a.a.outline;

public final class MemorySizeCalculator {
    public final int a;
    public final int b;
    public final Context c;
    public final int d;

    public static final class a {

        /* renamed from: i  reason: collision with root package name */
        public static final int f1684i = (Build.VERSION.SDK_INT < 26 ? 4 : 1);
        public final Context a;
        public ActivityManager b;
        public c c;
        public float d = 2.0f;

        /* renamed from: e  reason: collision with root package name */
        public float f1685e = ((float) f1684i);

        /* renamed from: f  reason: collision with root package name */
        public float f1686f = 0.4f;
        public float g = 0.33f;
        public int h = 4194304;

        public a(Context context) {
            this.a = context;
            this.b = (ActivityManager) context.getSystemService(SessionEvent.ACTIVITY_KEY);
            this.c = new b(context.getResources().getDisplayMetrics());
            if (Build.VERSION.SDK_INT >= 26 && this.b.isLowRamDevice()) {
                this.f1685e = 0.0f;
            }
        }
    }

    public static final class b implements c {
        public final DisplayMetrics a;

        public b(DisplayMetrics displayMetrics) {
            this.a = displayMetrics;
        }
    }

    public interface c {
    }

    public MemorySizeCalculator(a aVar) {
        int i2;
        this.c = aVar.a;
        if (aVar.b.isLowRamDevice()) {
            i2 = aVar.h / 2;
        } else {
            i2 = aVar.h;
        }
        this.d = i2;
        ActivityManager activityManager = aVar.b;
        float f2 = aVar.f1686f;
        int round = Math.round(((float) (activityManager.getMemoryClass() * 1024 * 1024)) * (activityManager.isLowRamDevice() ? aVar.g : f2));
        c cVar = aVar.c;
        float f3 = (float) (((b) cVar).a.widthPixels * ((b) cVar).a.heightPixels * 4);
        int round2 = Math.round(aVar.f1685e * f3);
        int round3 = Math.round(f3 * aVar.d);
        int i3 = round - this.d;
        int i4 = round3 + round2;
        if (i4 <= i3) {
            this.b = round3;
            this.a = round2;
        } else {
            float f4 = (float) i3;
            float f5 = aVar.f1685e;
            float f6 = aVar.d;
            float f7 = f4 / (f5 + f6);
            this.b = Math.round(f6 * f7);
            this.a = Math.round(f7 * aVar.f1685e);
        }
        if (Log.isLoggable("MemorySizeCalculator", 3)) {
            StringBuilder a2 = outline.a("Calculation complete, Calculated memory cache size: ");
            a2.append(a(this.b));
            a2.append(", pool size: ");
            a2.append(a(this.a));
            a2.append(", byte array size: ");
            a2.append(a(this.d));
            a2.append(", memory class limited? ");
            a2.append(i4 > round);
            a2.append(", max size: ");
            a2.append(a(round));
            a2.append(", memoryClass: ");
            a2.append(aVar.b.getMemoryClass());
            a2.append(", isLowMemoryDevice: ");
            a2.append(aVar.b.isLowRamDevice());
            Log.d("MemorySizeCalculator", a2.toString());
        }
    }

    public final String a(int i2) {
        return Formatter.formatFileSize(this.c, (long) i2);
    }
}
