package j.b.a.m.o.c;

import j.b.a.m.Option;

public abstract class DownsampleStrategy {
    public static final DownsampleStrategy a = new c();
    public static final DownsampleStrategy b = new a();
    public static final DownsampleStrategy c = new b();
    public static final DownsampleStrategy d = new d();

    /* renamed from: e  reason: collision with root package name */
    public static final DownsampleStrategy f1703e;

    /* renamed from: f  reason: collision with root package name */
    public static final Option<k> f1704f;
    public static final boolean g = true;

    public static class a extends DownsampleStrategy {
        public e a(int i2, int i3, int i4, int i5) {
            if (b(i2, i3, i4, i5) == 1.0f) {
                return e.QUALITY;
            }
            return DownsampleStrategy.a.a(i2, i3, i4, i5);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: ClspMth{java.lang.Math.min(float, float):float}
         arg types: [int, float]
         candidates:
          ClspMth{java.lang.Math.min(double, double):double}
          ClspMth{java.lang.Math.min(long, long):long}
          ClspMth{java.lang.Math.min(int, int):int}
          ClspMth{java.lang.Math.min(float, float):float} */
        public float b(int i2, int i3, int i4, int i5) {
            return Math.min(1.0f, DownsampleStrategy.a.b(i2, i3, i4, i5));
        }
    }

    public static class b extends DownsampleStrategy {
        public e a(int i2, int i3, int i4, int i5) {
            return e.QUALITY;
        }

        public float b(int i2, int i3, int i4, int i5) {
            return Math.max(((float) i4) / ((float) i2), ((float) i5) / ((float) i3));
        }
    }

    public static class c extends DownsampleStrategy {
        public e a(int i2, int i3, int i4, int i5) {
            if (DownsampleStrategy.g) {
                return e.QUALITY;
            }
            return e.MEMORY;
        }

        public float b(int i2, int i3, int i4, int i5) {
            if (DownsampleStrategy.g) {
                return Math.min(((float) i4) / ((float) i2), ((float) i5) / ((float) i3));
            }
            int max = Math.max(i3 / i5, i2 / i4);
            if (max == 0) {
                return 1.0f;
            }
            return 1.0f / ((float) Integer.highestOneBit(max));
        }
    }

    public static class d extends DownsampleStrategy {
        public e a(int i2, int i3, int i4, int i5) {
            return e.QUALITY;
        }

        public float b(int i2, int i3, int i4, int i5) {
            return 1.0f;
        }
    }

    public enum e {
        MEMORY,
        QUALITY
    }

    static {
        DownsampleStrategy downsampleStrategy = c;
        f1703e = downsampleStrategy;
        f1704f = Option.a("com.bumptech.glide.load.resource.bitmap.Downsampler.DownsampleStrategy", downsampleStrategy);
    }

    public abstract e a(int i2, int i3, int i4, int i5);

    public abstract float b(int i2, int i3, int i4, int i5);
}
