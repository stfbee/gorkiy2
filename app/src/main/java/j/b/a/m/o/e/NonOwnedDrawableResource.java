package j.b.a.m.o.e;

import android.graphics.drawable.Drawable;

public final class NonOwnedDrawableResource extends DrawableResource<Drawable> {
    public NonOwnedDrawableResource(Drawable drawable) {
        super(drawable);
    }

    public int b() {
        return Math.max(1, super.b.getIntrinsicHeight() * super.b.getIntrinsicWidth() * 4);
    }

    public Class<Drawable> c() {
        return super.b.getClass();
    }

    public void d() {
    }
}
