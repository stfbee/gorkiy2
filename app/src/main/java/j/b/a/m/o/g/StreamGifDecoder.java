package j.b.a.m.o.g;

import android.util.Log;
import com.bumptech.glide.load.ImageHeaderParser;
import i.b.k.ResourcesFlusher;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.b;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.List;

public class StreamGifDecoder implements ResourceDecoder<InputStream, c> {
    public final List<ImageHeaderParser> a;
    public final ResourceDecoder<ByteBuffer, c> b;
    public final ArrayPool c;

    public StreamGifDecoder(List<ImageHeaderParser> list, ResourceDecoder<ByteBuffer, c> resourceDecoder, b bVar) {
        this.a = list;
        this.b = resourceDecoder;
        this.c = bVar;
    }

    public Resource a(Object obj, int i2, int i3, Options options) {
        byte[] bArr;
        InputStream inputStream = (InputStream) obj;
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream(16384);
        try {
            byte[] bArr2 = new byte[16384];
            while (true) {
                int read = inputStream.read(bArr2);
                if (read == -1) {
                    break;
                }
                byteArrayOutputStream.write(bArr2, 0, read);
            }
            byteArrayOutputStream.flush();
            bArr = byteArrayOutputStream.toByteArray();
        } catch (IOException e2) {
            if (Log.isLoggable("StreamGifDecoder", 5)) {
                Log.w("StreamGifDecoder", "Error reading data from stream", e2);
            }
            bArr = null;
        }
        if (bArr == null) {
            return null;
        }
        return this.b.a(ByteBuffer.wrap(bArr), i2, i3, options);
    }

    public boolean a(Object obj, Options options) {
        return !((Boolean) options.a(GifOptions.b)).booleanValue() && ResourcesFlusher.b(this.a, (InputStream) obj, this.c) == ImageHeaderParser.ImageType.GIF;
    }
}
