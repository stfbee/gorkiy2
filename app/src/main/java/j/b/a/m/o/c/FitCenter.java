package j.b.a.m.o.c;

import android.graphics.Bitmap;
import j.b.a.m.Key;
import j.b.a.m.m.b0.BitmapPool;
import java.security.MessageDigest;

public class FitCenter extends BitmapTransformation {
    public static final byte[] b = "com.bumptech.glide.load.resource.bitmap.FitCenter".getBytes(Key.a);

    public Bitmap a(BitmapPool bitmapPool, Bitmap bitmap, int i2, int i3) {
        return TransformationUtils.c(bitmapPool, bitmap, i2, i3);
    }

    public boolean equals(Object obj) {
        return obj instanceof FitCenter;
    }

    public int hashCode() {
        return 1572326941;
    }

    public void a(MessageDigest messageDigest) {
        messageDigest.update(b);
    }
}
