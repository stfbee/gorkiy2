package j.b.a.m.l;

import android.content.ContentResolver;
import android.content.res.AssetFileDescriptor;
import android.net.Uri;
import j.a.a.a.outline;
import java.io.FileNotFoundException;

public final class AssetFileDescriptorLocalUriFetcher extends LocalUriFetcher<AssetFileDescriptor> {
    public AssetFileDescriptorLocalUriFetcher(ContentResolver contentResolver, Uri uri) {
        super(contentResolver, uri);
    }

    public void a(Object obj) {
        ((AssetFileDescriptor) obj).close();
    }

    public Object a(Uri uri, ContentResolver contentResolver) {
        AssetFileDescriptor openAssetFileDescriptor = contentResolver.openAssetFileDescriptor(uri, "r");
        if (openAssetFileDescriptor != null) {
            return openAssetFileDescriptor;
        }
        throw new FileNotFoundException(outline.a("FileDescriptor is null for: ", uri));
    }

    public Class<AssetFileDescriptor> a() {
        return AssetFileDescriptor.class;
    }
}
