package j.b.a.m.n;

import com.bumptech.glide.load.engine.GlideException;
import i.b.k.ResourcesFlusher;
import i.h.k.Pools;
import j.a.a.a.outline;
import j.b.a.Priority;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.Key;
import j.b.a.m.g;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MultiModelLoader<Model, Data> implements ModelLoader<Model, Data> {
    public final List<ModelLoader<Model, Data>> a;
    public final Pools<List<Throwable>> b;

    public MultiModelLoader(List<ModelLoader<Model, Data>> list, Pools<List<Throwable>> pools) {
        this.a = list;
        this.b = pools;
    }

    public ModelLoader.a<Data> a(Model model, int i2, int i3, g gVar) {
        ModelLoader.a a2;
        int size = this.a.size();
        ArrayList arrayList = new ArrayList(size);
        Key key = null;
        for (int i4 = 0; i4 < size; i4++) {
            ModelLoader modelLoader = this.a.get(i4);
            if (modelLoader.a(model) && (a2 = modelLoader.a(model, i2, i3, gVar)) != null) {
                key = a2.a;
                arrayList.add(a2.c);
            }
        }
        if (arrayList.isEmpty() || key == null) {
            return null;
        }
        return new ModelLoader.a<>(key, new a(arrayList, this.b));
    }

    public String toString() {
        StringBuilder a2 = outline.a("MultiModelLoader{modelLoaders=");
        a2.append(Arrays.toString(this.a.toArray()));
        a2.append('}');
        return a2.toString();
    }

    public static class a<Data> implements DataFetcher<Data>, DataFetcher.a<Data> {
        public final List<DataFetcher<Data>> b;
        public final Pools<List<Throwable>> c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public Priority f1691e;

        /* renamed from: f  reason: collision with root package name */
        public DataFetcher.a<? super Data> f1692f;
        public List<Throwable> g;
        public boolean h;

        public a(List<DataFetcher<Data>> list, Pools<List<Throwable>> pools) {
            this.c = pools;
            if (!list.isEmpty()) {
                this.b = list;
                this.d = 0;
                return;
            }
            throw new IllegalArgumentException("Must not be empty.");
        }

        public void a(f fVar, DataFetcher.a<? super Data> aVar) {
            this.f1691e = fVar;
            this.f1692f = aVar;
            this.g = this.c.a();
            this.b.get(this.d).a(fVar, this);
            if (this.h) {
                cancel();
            }
        }

        public void b() {
            List<Throwable> list = this.g;
            if (list != null) {
                this.c.a(list);
            }
            this.g = null;
            for (DataFetcher<Data> b2 : this.b) {
                b2.b();
            }
        }

        public DataSource c() {
            return this.b.get(0).c();
        }

        public void cancel() {
            this.h = true;
            for (DataFetcher<Data> cancel : this.b) {
                cancel.cancel();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [java.util.List<java.lang.Throwable>, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        public final void d() {
            if (!this.h) {
                if (this.d < this.b.size() - 1) {
                    this.d++;
                    a(this.f1691e, this.f1692f);
                    return;
                }
                ResourcesFlusher.a((Object) this.g, "Argument must not be null");
                this.f1692f.a((Exception) new GlideException("Fetch failed", new ArrayList(this.g)));
            }
        }

        public Class<Data> a() {
            return this.b.get(0).a();
        }

        public void a(Data data) {
            if (data != null) {
                this.f1692f.a((Object) data);
            } else {
                d();
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [java.util.List<java.lang.Throwable>, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        public void a(Exception exc) {
            List<Throwable> list = this.g;
            ResourcesFlusher.a((Object) list, "Argument must not be null");
            list.add(exc);
            d();
        }
    }

    public boolean a(Model model) {
        for (ModelLoader<Model, Data> a2 : this.a) {
            if (a2.a(model)) {
                return true;
            }
        }
        return false;
    }
}
