package j.b.a.m.m;

import j.a.a.a.outline;
import j.b.a.m.Key;
import j.b.a.m.Options;
import j.b.a.m.Transformation;
import j.b.a.m.e;
import j.b.a.m.g;
import j.b.a.m.m.b0.ArrayPool;
import j.b.a.m.m.b0.b;
import j.b.a.s.LruCache;
import j.b.a.s.Util;
import java.nio.ByteBuffer;
import java.security.MessageDigest;

public final class ResourceCacheKey implements Key {

    /* renamed from: j  reason: collision with root package name */
    public static final LruCache<Class<?>, byte[]> f1669j = new LruCache<>(50);
    public final ArrayPool b;
    public final Key c;
    public final Key d;

    /* renamed from: e  reason: collision with root package name */
    public final int f1670e;

    /* renamed from: f  reason: collision with root package name */
    public final int f1671f;
    public final Class<?> g;
    public final Options h;

    /* renamed from: i  reason: collision with root package name */
    public final Transformation<?> f1672i;

    public ResourceCacheKey(b bVar, e eVar, e eVar2, int i2, int i3, Transformation<?> transformation, Class<?> cls, g gVar) {
        this.b = bVar;
        this.c = eVar;
        this.d = eVar2;
        this.f1670e = i2;
        this.f1671f = i3;
        this.f1672i = transformation;
        this.g = cls;
        this.h = gVar;
    }

    /* JADX WARN: Type inference failed for: r1v6, types: [j.b.a.m.Transformation<?>, j.b.a.m.Key] */
    public void a(MessageDigest messageDigest) {
        byte[] bArr = (byte[]) this.b.a(8, byte[].class);
        ByteBuffer.wrap(bArr).putInt(this.f1670e).putInt(this.f1671f).array();
        this.d.a(messageDigest);
        this.c.a(messageDigest);
        messageDigest.update(bArr);
        ? r1 = this.f1672i;
        if (r1 != 0) {
            r1.a(messageDigest);
        }
        this.h.a(messageDigest);
        byte[] a = f1669j.a(this.g);
        if (a == null) {
            a = this.g.getName().getBytes(Key.a);
            f1669j.b(this.g, a);
        }
        messageDigest.update(a);
        this.b.put(bArr);
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof ResourceCacheKey)) {
            return false;
        }
        ResourceCacheKey resourceCacheKey = (ResourceCacheKey) obj;
        if (this.f1671f != resourceCacheKey.f1671f || this.f1670e != resourceCacheKey.f1670e || !Util.b(this.f1672i, resourceCacheKey.f1672i) || !this.g.equals(resourceCacheKey.g) || !this.c.equals(resourceCacheKey.c) || !this.d.equals(resourceCacheKey.d) || !this.h.equals(resourceCacheKey.h)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = ((((this.d.hashCode() + (this.c.hashCode() * 31)) * 31) + this.f1670e) * 31) + this.f1671f;
        Transformation<?> transformation = this.f1672i;
        if (transformation != null) {
            hashCode = (hashCode * 31) + transformation.hashCode();
        }
        int hashCode2 = this.g.hashCode();
        return this.h.hashCode() + ((hashCode2 + (hashCode * 31)) * 31);
    }

    public String toString() {
        StringBuilder a = outline.a("ResourceCacheKey{sourceKey=");
        a.append(this.c);
        a.append(", signature=");
        a.append(this.d);
        a.append(", width=");
        a.append(this.f1670e);
        a.append(", height=");
        a.append(this.f1671f);
        a.append(", decodedResourceClass=");
        a.append(this.g);
        a.append(", transformation='");
        a.append(this.f1672i);
        a.append('\'');
        a.append(", options=");
        a.append(this.h);
        a.append('}');
        return a.toString();
    }
}
