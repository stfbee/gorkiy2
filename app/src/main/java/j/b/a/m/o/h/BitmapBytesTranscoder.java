package j.b.a.m.o.h;

import android.graphics.Bitmap;
import j.b.a.m.g;
import j.b.a.m.m.Resource;
import j.b.a.m.o.d.BytesResource;
import java.io.ByteArrayOutputStream;

public class BitmapBytesTranscoder implements ResourceTranscoder<Bitmap, byte[]> {
    public final Bitmap.CompressFormat a = Bitmap.CompressFormat.JPEG;
    public final int b = 100;

    public Resource<byte[]> a(Resource<Bitmap> resource, g gVar) {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        resource.get().compress(this.a, this.b, byteArrayOutputStream);
        resource.d();
        return new BytesResource(byteArrayOutputStream.toByteArray());
    }
}
