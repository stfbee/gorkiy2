package j.b.a.m.n.y;

import j.b.a.m.Option;
import j.b.a.m.Options;
import j.b.a.m.l.HttpUrlFetcher;
import j.b.a.m.n.GlideUrl;
import j.b.a.m.n.ModelCache;
import j.b.a.m.n.ModelLoader;
import j.b.a.m.n.ModelLoaderFactory;
import j.b.a.m.n.g;
import j.b.a.m.n.r;
import java.io.InputStream;

public class HttpGlideUrlLoader implements ModelLoader<g, InputStream> {
    public static final Option<Integer> b = Option.a("com.bumptech.glide.load.model.stream.HttpGlideUrlLoader.Timeout", 2500);
    public final ModelCache<g, g> a;

    public static class a implements ModelLoaderFactory<g, InputStream> {
        public final ModelCache<g, g> a = new ModelCache<>(500);

        public ModelLoader<g, InputStream> a(r rVar) {
            return new HttpGlideUrlLoader(this.a);
        }
    }

    public HttpGlideUrlLoader(ModelCache<g, g> modelCache) {
        this.a = modelCache;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        GlideUrl glideUrl = (GlideUrl) obj;
        ModelCache<g, g> modelCache = this.a;
        if (modelCache != null) {
            ModelCache.b a2 = ModelCache.b.a(glideUrl, 0, 0);
            B a3 = modelCache.a.a(a2);
            a2.a();
            GlideUrl glideUrl2 = (GlideUrl) a3;
            if (glideUrl2 == null) {
                ModelCache<g, g> modelCache2 = this.a;
                if (modelCache2 != null) {
                    modelCache2.a.b(ModelCache.b.a(glideUrl, 0, 0), glideUrl);
                } else {
                    throw null;
                }
            } else {
                glideUrl = glideUrl2;
            }
        }
        return new ModelLoader.a(glideUrl, new HttpUrlFetcher(glideUrl, ((Integer) options.a(b)).intValue()));
    }

    public boolean a(Object obj) {
        GlideUrl glideUrl = (GlideUrl) obj;
        return true;
    }
}
