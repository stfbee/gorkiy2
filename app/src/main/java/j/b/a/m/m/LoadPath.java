package j.b.a.m.m;

import com.bumptech.glide.load.engine.GlideException;
import i.b.k.ResourcesFlusher;
import i.h.k.Pools;
import j.a.a.a.outline;
import j.b.a.m.g;
import j.b.a.m.l.DataRewinder;
import j.b.a.m.m.DecodePath;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class LoadPath<Data, ResourceType, Transcode> {
    public final Pools<List<Throwable>> a;
    public final List<? extends DecodePath<Data, ResourceType, Transcode>> b;
    public final String c;

    public LoadPath(Class<Data> cls, Class<ResourceType> cls2, Class<Transcode> cls3, List<DecodePath<Data, ResourceType, Transcode>> list, Pools<List<Throwable>> pools) {
        this.a = pools;
        if (!list.isEmpty()) {
            this.b = list;
            StringBuilder a2 = outline.a("Failed LoadPath{");
            a2.append(cls.getSimpleName());
            a2.append("->");
            a2.append(cls2.getSimpleName());
            a2.append("->");
            a2.append(cls3.getSimpleName());
            a2.append("}");
            this.c = a2.toString();
            return;
        }
        throw new IllegalArgumentException("Must not be empty.");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.List<java.lang.Throwable>, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public Resource<Transcode> a(DataRewinder<Data> dataRewinder, g gVar, int i2, int i3, DecodePath.a<ResourceType> aVar) {
        Resource<Transcode> resource;
        List<Throwable> a2 = this.a.a();
        ResourcesFlusher.a((Object) a2, "Argument must not be null");
        List list = a2;
        try {
            int size = this.b.size();
            resource = null;
            for (int i4 = 0; i4 < size; i4++) {
                resource = ((DecodePath) this.b.get(i4)).a(dataRewinder, i2, i3, gVar, aVar);
                if (resource != null) {
                    break;
                }
            }
        } catch (GlideException e2) {
            list.add(e2);
        } catch (Throwable th) {
            this.a.a(list);
            throw th;
        }
        if (resource != null) {
            this.a.a(list);
            return resource;
        }
        throw new GlideException(this.c, new ArrayList(list));
    }

    public String toString() {
        StringBuilder a2 = outline.a("LoadPath{decodePaths=");
        a2.append(Arrays.toString(this.b.toArray()));
        a2.append('}');
        return a2.toString();
    }
}
