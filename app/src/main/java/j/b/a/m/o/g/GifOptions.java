package j.b.a.m.o.g;

import j.b.a.m.DecodeFormat;
import j.b.a.m.Option;
import j.b.a.m.b;

public final class GifOptions {
    public static final Option<b> a = Option.a("com.bumptech.glide.load.resource.gif.GifOptions.DecodeFormat", DecodeFormat.DEFAULT);
    public static final Option<Boolean> b = Option.a("com.bumptech.glide.load.resource.gif.GifOptions.DisableAnimation", false);
}
