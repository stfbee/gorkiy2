package j.b.a.m.o.c;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.ParcelFileDescriptor;
import com.bumptech.glide.load.ImageHeaderParser;
import i.b.k.ResourcesFlusher;
import j.b.a.m.l.InputStreamRewinder;
import j.b.a.m.l.ParcelFileDescriptorRewinder;
import j.b.a.m.m.b0.ArrayPool;
import java.io.InputStream;
import java.util.List;

public interface ImageReader {

    public static final class a implements ImageReader {
        public final InputStreamRewinder a;
        public final ArrayPool b;
        public final List<ImageHeaderParser> c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [j.b.a.m.m.b0.b, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        public a(InputStream inputStream, List<ImageHeaderParser> list, j.b.a.m.m.b0.b bVar) {
            ResourcesFlusher.a((Object) bVar, "Argument must not be null");
            this.b = bVar;
            ResourcesFlusher.a((Object) list, "Argument must not be null");
            this.c = list;
            this.a = new InputStreamRewinder(inputStream, bVar);
        }

        public Bitmap a(BitmapFactory.Options options) {
            return BitmapFactory.decodeStream(this.a.a(), null, options);
        }

        public void b() {
            this.a.a.a();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.b):int
         arg types: [java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.ArrayPool]
         candidates:
          i.b.k.ResourcesFlusher.a(int, int, int):int
          i.b.k.ResourcesFlusher.a(android.content.Context, int, int):int
          i.b.k.ResourcesFlusher.a(java.lang.Object, android.util.Property, android.graphics.Path):android.animation.ObjectAnimator
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, org.xmlpull.v1.XmlPullParser, android.content.res.Resources$Theme):android.content.res.ColorStateList
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int, android.os.Parcelable$Creator):T
          i.b.k.ResourcesFlusher.a(android.view.inputmethod.InputConnection, android.view.inputmethod.EditorInfo, android.view.View):android.view.inputmethod.InputConnection
          i.b.k.ResourcesFlusher.a(android.content.res.TypedArray, int, int):java.lang.String
          i.b.k.ResourcesFlusher.a(android.content.Context, android.os.CancellationSignal, android.net.Uri):java.nio.ByteBuffer
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int, int):void
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int, long):void
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int, boolean):void
          i.b.k.ResourcesFlusher.a(android.os.Parcel, android.os.Parcelable, int):void
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetContainer, i.f.a.LinearSystem, int):void
          i.b.k.ResourcesFlusher.a(java.util.List<i.f.a.h.f>, int, int):void
          i.b.k.ResourcesFlusher.a(int, android.graphics.Rect, android.graphics.Rect):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.Rect, android.graphics.Rect, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, android.content.res.Resources, int):boolean
          i.b.k.ResourcesFlusher.a(float[], int, int):float[]
          i.b.k.ResourcesFlusher.a(java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.b):int */
        public int c() {
            return ResourcesFlusher.a(this.c, this.a.a(), (j.b.a.m.m.b0.b) this.b);
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.b(java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.b):com.bumptech.glide.load.ImageHeaderParser$ImageType
         arg types: [java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.ArrayPool]
         candidates:
          i.b.k.ResourcesFlusher.b(int, android.graphics.Rect, android.graphics.Rect):int
          i.b.k.ResourcesFlusher.b(android.os.Parcel, int, int):void
          i.b.k.ResourcesFlusher.b(android.content.res.TypedArray, int, int):java.lang.CharSequence[]
          i.b.k.ResourcesFlusher.b(android.os.Parcel, int, android.os.Parcelable$Creator):T[]
          i.b.k.ResourcesFlusher.b(java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.io.InputStream, j.b.a.m.m.b0.b):com.bumptech.glide.load.ImageHeaderParser$ImageType */
        public ImageHeaderParser.ImageType a() {
            return ResourcesFlusher.b(this.c, this.a.a(), (j.b.a.m.m.b0.b) this.b);
        }
    }

    Bitmap a(BitmapFactory.Options options);

    ImageHeaderParser.ImageType a();

    void b();

    int c();

    public static final class b implements ImageReader {
        public final ArrayPool a;
        public final List<ImageHeaderParser> b;
        public final ParcelFileDescriptorRewinder c;

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [j.b.a.m.m.b0.b, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
         arg types: [java.util.List<com.bumptech.glide.load.ImageHeaderParser>, java.lang.String]
         candidates:
          i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
          i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
          i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
          i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
          i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
          i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
          i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
          i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
          i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
          i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
          i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
          i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
          i.b.k.ResourcesFlusher.a(int, int):boolean
          i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
          i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
          i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
          i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
          i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
        public b(ParcelFileDescriptor parcelFileDescriptor, List<ImageHeaderParser> list, j.b.a.m.m.b0.b bVar) {
            ResourcesFlusher.a((Object) bVar, "Argument must not be null");
            this.a = bVar;
            ResourcesFlusher.a((Object) list, "Argument must not be null");
            this.b = list;
            this.c = new ParcelFileDescriptorRewinder(parcelFileDescriptor);
        }

        public Bitmap a(BitmapFactory.Options options) {
            return BitmapFactory.decodeFileDescriptor(this.c.a().getFileDescriptor(), null, options);
        }

        public void b() {
        }

        /* JADX WARNING: Removed duplicated region for block: B:19:0x0040 A[SYNTHETIC, Splitter:B:19:0x0040] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int c() {
            /*
                r11 = this;
                java.util.List<com.bumptech.glide.load.ImageHeaderParser> r0 = r11.b
                j.b.a.m.l.ParcelFileDescriptorRewinder r1 = r11.c
                j.b.a.m.m.b0.ArrayPool r2 = r11.a
                int r3 = r0.size()
                r4 = 0
            L_0x000b:
                r5 = -1
                if (r4 >= r3) goto L_0x0047
                java.lang.Object r6 = r0.get(r4)
                com.bumptech.glide.load.ImageHeaderParser r6 = (com.bumptech.glide.load.ImageHeaderParser) r6
                r7 = 0
                j.b.a.m.o.c.RecyclableBufferedInputStream r8 = new j.b.a.m.o.c.RecyclableBufferedInputStream     // Catch:{ all -> 0x003d }
                java.io.FileInputStream r9 = new java.io.FileInputStream     // Catch:{ all -> 0x003d }
                android.os.ParcelFileDescriptor r10 = r1.a()     // Catch:{ all -> 0x003d }
                java.io.FileDescriptor r10 = r10.getFileDescriptor()     // Catch:{ all -> 0x003d }
                r9.<init>(r10)     // Catch:{ all -> 0x003d }
                r8.<init>(r9, r2)     // Catch:{ all -> 0x003d }
                int r6 = r6.a(r8, r2)     // Catch:{ all -> 0x003a }
                r8.close()     // Catch:{ IOException -> 0x002f }
                goto L_0x0030
            L_0x002f:
            L_0x0030:
                r1.a()
                if (r6 == r5) goto L_0x0037
                r5 = r6
                goto L_0x0047
            L_0x0037:
                int r4 = r4 + 1
                goto L_0x000b
            L_0x003a:
                r0 = move-exception
                r7 = r8
                goto L_0x003e
            L_0x003d:
                r0 = move-exception
            L_0x003e:
                if (r7 == 0) goto L_0x0043
                r7.close()     // Catch:{ IOException -> 0x0043 }
            L_0x0043:
                r1.a()
                throw r0
            L_0x0047:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.o.c.ImageReader.b.c():int");
        }

        /* JADX WARNING: Removed duplicated region for block: B:17:0x0040 A[SYNTHETIC, Splitter:B:17:0x0040] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public com.bumptech.glide.load.ImageHeaderParser.ImageType a() {
            /*
                r10 = this;
                java.util.List<com.bumptech.glide.load.ImageHeaderParser> r0 = r10.b
                j.b.a.m.l.ParcelFileDescriptorRewinder r1 = r10.c
                j.b.a.m.m.b0.ArrayPool r2 = r10.a
                int r3 = r0.size()
                r4 = 0
            L_0x000b:
                if (r4 >= r3) goto L_0x0047
                java.lang.Object r5 = r0.get(r4)
                com.bumptech.glide.load.ImageHeaderParser r5 = (com.bumptech.glide.load.ImageHeaderParser) r5
                r6 = 0
                j.b.a.m.o.c.RecyclableBufferedInputStream r7 = new j.b.a.m.o.c.RecyclableBufferedInputStream     // Catch:{ all -> 0x003d }
                java.io.FileInputStream r8 = new java.io.FileInputStream     // Catch:{ all -> 0x003d }
                android.os.ParcelFileDescriptor r9 = r1.a()     // Catch:{ all -> 0x003d }
                java.io.FileDescriptor r9 = r9.getFileDescriptor()     // Catch:{ all -> 0x003d }
                r8.<init>(r9)     // Catch:{ all -> 0x003d }
                r7.<init>(r8, r2)     // Catch:{ all -> 0x003d }
                com.bumptech.glide.load.ImageHeaderParser$ImageType r5 = r5.a(r7)     // Catch:{ all -> 0x003a }
                r7.close()     // Catch:{ IOException -> 0x002e }
                goto L_0x002f
            L_0x002e:
            L_0x002f:
                r1.a()
                com.bumptech.glide.load.ImageHeaderParser$ImageType r6 = com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN
                if (r5 == r6) goto L_0x0037
                goto L_0x0049
            L_0x0037:
                int r4 = r4 + 1
                goto L_0x000b
            L_0x003a:
                r0 = move-exception
                r6 = r7
                goto L_0x003e
            L_0x003d:
                r0 = move-exception
            L_0x003e:
                if (r6 == 0) goto L_0x0043
                r6.close()     // Catch:{ IOException -> 0x0043 }
            L_0x0043:
                r1.a()
                throw r0
            L_0x0047:
                com.bumptech.glide.load.ImageHeaderParser$ImageType r5 = com.bumptech.glide.load.ImageHeaderParser.ImageType.UNKNOWN
            L_0x0049:
                return r5
            */
            throw new UnsupportedOperationException("Method not decompiled: j.b.a.m.o.c.ImageReader.b.a():com.bumptech.glide.load.ImageHeaderParser$ImageType");
        }
    }
}
