package j.b.a.m.o.f;

import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import java.io.File;

public class FileDecoder implements ResourceDecoder<File, File> {
    public Resource a(Object obj, int i2, int i3, Options options) {
        return new FileResource((File) obj);
    }

    public boolean a(Object obj, Options options) {
        File file = (File) obj;
        return true;
    }
}
