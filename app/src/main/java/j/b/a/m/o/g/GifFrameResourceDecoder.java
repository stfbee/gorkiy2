package j.b.a.m.o.g;

import android.graphics.Bitmap;
import j.b.a.l.GifDecoder;
import j.b.a.l.a;
import j.b.a.m.Options;
import j.b.a.m.ResourceDecoder;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import j.b.a.m.o.c.BitmapResource;

public final class GifFrameResourceDecoder implements ResourceDecoder<a, Bitmap> {
    public final BitmapPool a;

    public GifFrameResourceDecoder(BitmapPool bitmapPool) {
        this.a = bitmapPool;
    }

    public Resource a(Object obj, int i2, int i3, Options options) {
        return BitmapResource.a(((GifDecoder) obj).c(), this.a);
    }

    public boolean a(Object obj, Options options) {
        GifDecoder gifDecoder = (GifDecoder) obj;
        return true;
    }
}
