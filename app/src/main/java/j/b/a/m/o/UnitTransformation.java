package j.b.a.m.o;

import android.content.Context;
import j.b.a.m.Transformation;
import j.b.a.m.m.Resource;
import java.security.MessageDigest;

public final class UnitTransformation<T> implements Transformation<T> {
    public static final Transformation<?> b = new UnitTransformation();

    public Resource<T> a(Context context, Resource<T> resource, int i2, int i3) {
        return resource;
    }

    public void a(MessageDigest messageDigest) {
    }
}
