package j.b.a.m.o.c;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import j.b.a.Glide;
import j.b.a.m.Transformation;
import j.b.a.m.m.Resource;
import j.b.a.m.m.b0.BitmapPool;
import java.security.MessageDigest;

public class DrawableTransformation implements Transformation<Drawable> {
    public final Transformation<Bitmap> b;
    public final boolean c;

    public DrawableTransformation(Transformation<Bitmap> transformation, boolean z) {
        this.b = transformation;
        this.c = z;
    }

    public Resource<Drawable> a(Context context, Resource<Drawable> resource, int i2, int i3) {
        BitmapPool bitmapPool = Glide.a(context).b;
        Drawable drawable = resource.get();
        Resource<Bitmap> a = DrawableToBitmapConverter.a(bitmapPool, drawable, i2, i3);
        if (a != null) {
            Resource<Bitmap> a2 = this.b.a(context, a, i2, i3);
            if (!a2.equals(a)) {
                return LazyBitmapDrawableResource.a(context.getResources(), a2);
            }
            a2.d();
            return resource;
        } else if (!this.c) {
            return resource;
        } else {
            throw new IllegalArgumentException("Unable to convert " + drawable + " to a Bitmap");
        }
    }

    public boolean equals(Object obj) {
        if (obj instanceof DrawableTransformation) {
            return this.b.equals(((DrawableTransformation) obj).b);
        }
        return false;
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [j.b.a.m.Transformation<android.graphics.Bitmap>, j.b.a.m.Key] */
    public void a(MessageDigest messageDigest) {
        this.b.a(messageDigest);
    }
}
