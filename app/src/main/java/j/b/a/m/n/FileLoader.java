package j.b.a.m.n;

import android.os.ParcelFileDescriptor;
import android.util.Log;
import j.b.a.f;
import j.b.a.m.DataSource;
import j.b.a.m.Options;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.n.ModelLoader;
import j.b.a.r.ObjectKey;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FileLoader<Data> implements ModelLoader<File, Data> {
    public final d<Data> a;

    public static class a<Data> implements ModelLoaderFactory<File, Data> {
        public final d<Data> a;

        public a(d<Data> dVar) {
            this.a = dVar;
        }

        public final ModelLoader<File, Data> a(r rVar) {
            return new FileLoader(this.a);
        }
    }

    public static class b extends a<ParcelFileDescriptor> {
        public b() {
            super(new a());
        }

        public class a implements d<ParcelFileDescriptor> {
            public void a(Object obj) {
                ((ParcelFileDescriptor) obj).close();
            }

            public Object a(File file) {
                return ParcelFileDescriptor.open(file, 268435456);
            }

            public Class<ParcelFileDescriptor> a() {
                return ParcelFileDescriptor.class;
            }
        }
    }

    public interface d<Data> {
        Class<Data> a();

        Data a(File file);

        void a(Data data);
    }

    public static class e extends a<InputStream> {
        public e() {
            super(new a());
        }

        public class a implements d<InputStream> {
            public void a(Object obj) {
                ((InputStream) obj).close();
            }

            public Object a(File file) {
                return new FileInputStream(file);
            }

            public Class<InputStream> a() {
                return InputStream.class;
            }
        }
    }

    public FileLoader(d<Data> dVar) {
        this.a = dVar;
    }

    public ModelLoader.a a(Object obj, int i2, int i3, Options options) {
        File file = (File) obj;
        return new ModelLoader.a(new ObjectKey(file), new c(file, this.a));
    }

    public boolean a(Object obj) {
        File file = (File) obj;
        return true;
    }

    public static final class c<Data> implements DataFetcher<Data> {
        public final File b;
        public final d<Data> c;
        public Data d;

        public c(File file, d<Data> dVar) {
            this.b = file;
            this.c = dVar;
        }

        public void a(f fVar, DataFetcher.a<? super Data> aVar) {
            try {
                Data a = this.c.a(this.b);
                this.d = a;
                aVar.a((Object) a);
            } catch (FileNotFoundException e2) {
                if (Log.isLoggable("FileLoader", 3)) {
                    Log.d("FileLoader", "Failed to open file", e2);
                }
                aVar.a((Exception) e2);
            }
        }

        public void b() {
            Data data = this.d;
            if (data != null) {
                try {
                    this.c.a((Object) data);
                } catch (IOException unused) {
                }
            }
        }

        public DataSource c() {
            return DataSource.LOCAL;
        }

        public void cancel() {
        }

        public Class<Data> a() {
            return this.c.a();
        }
    }
}
