package j.b.a.m;

import android.text.TextUtils;
import i.b.k.ResourcesFlusher;
import j.a.a.a.outline;
import java.security.MessageDigest;

public final class Option<T> {

    /* renamed from: e  reason: collision with root package name */
    public static final b<Object> f1597e = new a();
    public final T a;
    public final b<T> b;
    public final String c;
    public volatile byte[] d;

    public class a implements b<Object> {
        public void a(byte[] bArr, Object obj, MessageDigest messageDigest) {
        }
    }

    public interface b<T> {
        void a(byte[] bArr, Object obj, MessageDigest messageDigest);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
     arg types: [j.b.a.m.Option$b<T>, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T */
    public Option(String str, T t2, b<T> bVar) {
        if (!TextUtils.isEmpty(str)) {
            this.c = str;
            this.a = t2;
            ResourcesFlusher.a((Object) bVar, "Argument must not be null");
            this.b = bVar;
            return;
        }
        throw new IllegalArgumentException("Must not be null or empty");
    }

    public static <T> Option<T> a(String str, T t2) {
        return new Option<>(str, t2, f1597e);
    }

    public boolean equals(Object obj) {
        if (obj instanceof Option) {
            return this.c.equals(((Option) obj).c);
        }
        return false;
    }

    public int hashCode() {
        return this.c.hashCode();
    }

    public String toString() {
        StringBuilder a2 = outline.a("Option{key='");
        a2.append(this.c);
        a2.append('\'');
        a2.append('}');
        return a2.toString();
    }
}
