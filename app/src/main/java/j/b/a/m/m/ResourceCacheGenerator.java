package j.b.a.m.m;

import com.bumptech.glide.Registry;
import j.a.a.a.outline;
import j.b.a.m.DataSource;
import j.b.a.m.Key;
import j.b.a.m.Transformation;
import j.b.a.m.e;
import j.b.a.m.l.DataFetcher;
import j.b.a.m.m.DataFetcherGenerator;
import j.b.a.m.m.g;
import j.b.a.m.n.ModelLoader;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class ResourceCacheGenerator implements g, DataFetcher.a<Object> {
    public final DataFetcherGenerator.a b;
    public final DecodeHelper<?> c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f1664e = -1;

    /* renamed from: f  reason: collision with root package name */
    public Key f1665f;
    public List<ModelLoader<File, ?>> g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public volatile ModelLoader.a<?> f1666i;

    /* renamed from: j  reason: collision with root package name */
    public File f1667j;

    /* renamed from: k  reason: collision with root package name */
    public ResourceCacheKey f1668k;

    public ResourceCacheGenerator(DecodeHelper<?> decodeHelper, g.a aVar) {
        this.c = decodeHelper;
        this.b = aVar;
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    public boolean a() {
        List<e> a = this.c.a();
        if (a.isEmpty()) {
            return false;
        }
        DecodeHelper<?> decodeHelper = this.c;
        Registry registry = decodeHelper.c.b;
        Class<?> cls = decodeHelper.d.getClass();
        Class<?> cls2 = decodeHelper.g;
        Class<Transcode> cls3 = decodeHelper.f1614k;
        List<Class<?>> a2 = registry.h.a(cls, cls2, cls3);
        ArrayList arrayList = a2;
        if (a2 == null) {
            ArrayList arrayList2 = new ArrayList();
            for (Class<?> b2 : registry.a.a(cls)) {
                Iterator it = ((ArrayList) registry.c.b(b2, cls2)).iterator();
                while (it.hasNext()) {
                    Class cls4 = (Class) it.next();
                    if (!((ArrayList) registry.f369f.b(cls4, cls3)).isEmpty() && !arrayList2.contains(cls4)) {
                        arrayList2.add(cls4);
                    }
                }
            }
            registry.h.a(cls, cls2, cls3, Collections.unmodifiableList(arrayList2));
            arrayList = arrayList2;
        }
        if (!arrayList.isEmpty()) {
            while (true) {
                List<ModelLoader<File, ?>> list = this.g;
                if (list != null) {
                    if (this.h < list.size()) {
                        this.f1666i = null;
                        boolean z = false;
                        while (!z) {
                            if (!(this.h < this.g.size())) {
                                break;
                            }
                            List<ModelLoader<File, ?>> list2 = this.g;
                            int i2 = this.h;
                            this.h = i2 + 1;
                            File file = this.f1667j;
                            DecodeHelper<?> decodeHelper2 = this.c;
                            this.f1666i = list2.get(i2).a(file, decodeHelper2.f1610e, decodeHelper2.f1611f, decodeHelper2.f1612i);
                            if (this.f1666i != null && this.c.c(this.f1666i.c.a())) {
                                this.f1666i.c.a(this.c.f1618o, this);
                                z = true;
                            }
                        }
                        return z;
                    }
                }
                int i3 = this.f1664e + 1;
                this.f1664e = i3;
                if (i3 >= arrayList.size()) {
                    int i4 = this.d + 1;
                    this.d = i4;
                    if (i4 >= a.size()) {
                        return false;
                    }
                    this.f1664e = 0;
                }
                Key key = a.get(this.d);
                Class cls5 = arrayList.get(this.f1664e);
                Transformation<Z> b3 = this.c.b(cls5);
                DecodeHelper<?> decodeHelper3 = this.c;
                this.f1668k = new ResourceCacheKey(decodeHelper3.c.a, key, decodeHelper3.f1617n, decodeHelper3.f1610e, decodeHelper3.f1611f, b3, cls5, decodeHelper3.f1612i);
                File a3 = this.c.b().a(this.f1668k);
                this.f1667j = a3;
                if (a3 != null) {
                    this.f1665f = key;
                    this.g = this.c.c.b.a(a3);
                    this.h = 0;
                }
            }
        } else if (File.class.equals(this.c.f1614k)) {
            return false;
        } else {
            StringBuilder a4 = outline.a("Failed to find any load path from ");
            a4.append(this.c.d.getClass());
            a4.append(" to ");
            a4.append(this.c.f1614k);
            throw new IllegalStateException(a4.toString());
        }
    }

    public void cancel() {
        ModelLoader.a<?> aVar = this.f1666i;
        if (aVar != null) {
            aVar.c.cancel();
        }
    }

    public void a(Object obj) {
        this.b.a(this.f1665f, obj, this.f1666i.c, DataSource.RESOURCE_DISK_CACHE, this.f1668k);
    }

    public void a(Exception exc) {
        this.b.a(this.f1668k, exc, this.f1666i.c, DataSource.RESOURCE_DISK_CACHE);
    }
}
