package j.b.a.n;

import j.b.a.q.h.Target;
import j.b.a.s.Util;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.Set;
import java.util.WeakHashMap;

public final class TargetTracker implements LifecycleListener {
    public final Set<Target<?>> b = Collections.newSetFromMap(new WeakHashMap());

    /* JADX WARN: Type inference failed for: r1v2, types: [j.b.a.q.h.Target, j.b.a.n.LifecycleListener] */
    public void a() {
        Iterator it = ((ArrayList) Util.a(this.b)).iterator();
        while (it.hasNext()) {
            ((Target) it.next()).a();
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [j.b.a.q.h.Target, j.b.a.n.LifecycleListener] */
    public void c() {
        Iterator it = ((ArrayList) Util.a(this.b)).iterator();
        while (it.hasNext()) {
            ((Target) it.next()).c();
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [j.b.a.q.h.Target, j.b.a.n.LifecycleListener] */
    public void d() {
        Iterator it = ((ArrayList) Util.a(this.b)).iterator();
        while (it.hasNext()) {
            ((Target) it.next()).d();
        }
    }
}
