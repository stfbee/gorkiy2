package j.b.a.n;

import android.content.Context;
import android.util.Log;
import androidx.fragment.app.Fragment;
import i.l.a.FragmentManager;
import i.l.a.FragmentManagerImpl;
import j.b.a.Glide;
import j.b.a.RequestManager;
import java.util.HashSet;
import java.util.Set;

public class SupportRequestManagerFragment extends Fragment {
    public final ActivityFragmentLifecycle X;
    public final RequestManagerTreeNode Y = new a();
    public final Set<o> Z = new HashSet();
    public SupportRequestManagerFragment a0;
    public RequestManager b0;
    public Fragment c0;

    public class a implements RequestManagerTreeNode {
        public a() {
        }

        public String toString() {
            return super.toString() + "{fragment=" + SupportRequestManagerFragment.this + "}";
        }
    }

    public SupportRequestManagerFragment() {
        ActivityFragmentLifecycle activityFragmentLifecycle = new ActivityFragmentLifecycle();
        this.X = activityFragmentLifecycle;
    }

    public void B() {
        super.F = true;
        this.X.a();
        O();
    }

    public void D() {
        super.F = true;
        this.c0 = null;
        O();
    }

    public void G() {
        super.F = true;
        this.X.b();
    }

    public void H() {
        super.F = true;
        this.X.c();
    }

    public final Fragment N() {
        Fragment fragment = super.v;
        return fragment != null ? super : this.c0;
    }

    public final void O() {
        SupportRequestManagerFragment supportRequestManagerFragment = this.a0;
        if (supportRequestManagerFragment != null) {
            supportRequestManagerFragment.Z.remove(this);
            this.a0 = null;
        }
    }

    public final void a(Context context, FragmentManager fragmentManager) {
        O();
        RequestManagerRetriever requestManagerRetriever = Glide.a(context).g;
        if (requestManagerRetriever != null) {
            SupportRequestManagerFragment a2 = requestManagerRetriever.a(fragmentManager, (Fragment) null, RequestManagerRetriever.d(context));
            this.a0 = a2;
            if (!equals(a2)) {
                this.a0.Z.add(this);
                return;
            }
            return;
        }
        throw null;
    }

    public String toString() {
        return super.toString() + "{parent=" + N() + "}";
    }

    public void a(Context context) {
        super.a(context);
        Fragment fragment = this;
        while (true) {
            Fragment fragment2 = super.v;
            if (fragment2 == null) {
                break;
            }
            fragment = fragment2;
        }
        FragmentManagerImpl fragmentManagerImpl = super.f232s;
        if (fragmentManagerImpl != null) {
            try {
                a(p(), fragmentManagerImpl);
            } catch (IllegalStateException e2) {
                if (Log.isLoggable("SupportRMFragment", 5)) {
                    Log.w("SupportRMFragment", "Unable to register fragment with root", e2);
                }
            }
        } else if (Log.isLoggable("SupportRMFragment", 5)) {
            Log.w("SupportRMFragment", "Unable to register fragment with root, ancestor detached");
        }
    }
}
