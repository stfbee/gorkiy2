package j.b.a.r;

import j.b.a.m.Key;
import java.security.MessageDigest;

public final class EmptySignature implements Key {
    public static final EmptySignature b = new EmptySignature();

    public void a(MessageDigest messageDigest) {
    }

    public String toString() {
        return "EmptySignature";
    }
}
