package j.f.a.b;

import com.crashlytics.android.core.SessionProtobufHelper;
import com.redmadrobot.inputmask.helper.Compiler;
import j.a.a.a.outline;
import j.f.a.c.CaretString;
import j.f.a.c.Next;
import j.f.a.c.State;
import j.f.a.c.c;
import j.f.a.c.e.EOLState;
import j.f.a.c.e.FixedState;
import j.f.a.c.e.FreeState;
import j.f.a.c.e.OptionalValueState;
import j.f.a.c.e.ValueState;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import l.a.a.a.o.d.EventsFilesManager;
import n.i._Arrays;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: Mask.kt */
public class Mask {
    public static final Map<String, c> c = new HashMap();
    public static final Mask d = null;
    public final State a;
    public final List<c> b;

    /* compiled from: Mask.kt */
    public static final class a {
        public final CaretString a;
        public final String b;
        public final int c;
        public final boolean d;

        public a(CaretString caretString, String str, int i2, boolean z) {
            if (caretString == null) {
                Intrinsics.a("formattedText");
                throw null;
            } else if (str != null) {
                this.a = caretString;
                this.b = str;
                this.c = i2;
                this.d = z;
            } else {
                Intrinsics.a("extractedValue");
                throw null;
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof a) {
                    a aVar = (a) obj;
                    if (Intrinsics.a(this.a, aVar.a) && Intrinsics.a((Object) this.b, (Object) aVar.b)) {
                        if (this.c == aVar.c) {
                            if (this.d == aVar.d) {
                                return true;
                            }
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public int hashCode() {
            CaretString caretString = this.a;
            int i2 = 0;
            int hashCode = (caretString != null ? caretString.hashCode() : 0) * 31;
            String str = this.b;
            if (str != null) {
                i2 = str.hashCode();
            }
            int i3 = (((hashCode + i2) * 31) + this.c) * 31;
            boolean z = this.d;
            if (z) {
                z = true;
            }
            return i3 + (z ? 1 : 0);
        }

        public String toString() {
            StringBuilder a2 = outline.a("Result(formattedText=");
            a2.append(this.a);
            a2.append(", extractedValue=");
            a2.append(this.b);
            a2.append(", affinity=");
            a2.append(this.c);
            a2.append(", complete=");
            return outline.a(a2, this.d, ")");
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [char[], java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, boolean, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, boolean, int):java.util.List<java.lang.String>
      n.r.Indent.a(java.lang.CharSequence, char, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
     arg types: [java.lang.String, java.lang.String, java.lang.String, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String */
    public Mask(String str, List<c> list) {
        Iterator it;
        String str2;
        String str3;
        String str4;
        Compiler compiler;
        String str5;
        boolean z;
        int i2;
        List<c> list2 = list;
        if (str == null) {
            Intrinsics.a("format");
            throw null;
        } else if (list2 != null) {
            this.b = list2;
            Compiler compiler2 = new Compiler(list2);
            char[] charArray = str.toCharArray();
            String str6 = "(this as java.lang.String).toCharArray()";
            Intrinsics.a((Object) charArray, str6);
            int length = charArray.length;
            int i3 = 0;
            boolean z2 = false;
            boolean z3 = false;
            boolean z4 = false;
            while (true) {
                char c2 = '\\';
                if (i3 < length) {
                    char c3 = charArray[i3];
                    if ('\\' == c3) {
                        z2 = !z2;
                    } else {
                        if ('[' == c3) {
                            if (!z3) {
                                z3 = !z2;
                            } else {
                                throw new Compiler.FormatError();
                            }
                        }
                        if (']' == c3 && !z2) {
                            z3 = false;
                        }
                        if ('{' == c3) {
                            if (!z4) {
                                z4 = !z2;
                            } else {
                                throw new Compiler.FormatError();
                            }
                        }
                        if ('}' == c3 && !z2) {
                            z4 = false;
                        }
                        z2 = false;
                    }
                    i3++;
                } else {
                    ArrayList arrayList = new ArrayList();
                    char[] charArray2 = str.toCharArray();
                    Intrinsics.a((Object) charArray2, str6);
                    int length2 = charArray2.length;
                    int i4 = 0;
                    boolean z5 = false;
                    String str7 = "";
                    while (i4 < length2) {
                        char c4 = charArray2[i4];
                        if (c2 != c4 || z5) {
                            if (('[' == c4 || '{' == c4) && !z5) {
                                if (str7.length() > 0) {
                                    arrayList.add(str7);
                                }
                                str7 = "";
                            }
                            String a2 = outline.a(str7, c4);
                            if ((']' == c4 || '}' == c4) && !z5) {
                                arrayList.add(a2);
                                a2 = "";
                            }
                            z5 = false;
                            str7 = a2;
                        } else {
                            str7 = outline.a(str7, c4);
                            z5 = true;
                        }
                        i4++;
                        c2 = '\\';
                    }
                    if (!(str7.length() == 0)) {
                        arrayList.add(str7);
                    }
                    ArrayList arrayList2 = new ArrayList();
                    Iterator it2 = arrayList.iterator();
                    while (it2.hasNext()) {
                        String str8 = (String) it2.next();
                        Iterator it3 = it2;
                        if (Indent.b(str8, "[", false, 2)) {
                            int length3 = str8.length();
                            int i5 = 0;
                            String str9 = "";
                            while (true) {
                                if (i5 >= length3) {
                                    compiler = compiler2;
                                    str4 = str6;
                                    break;
                                }
                                int i6 = length3;
                                char charAt = str8.charAt(i5);
                                if (charAt == '[') {
                                    str5 = outline.a(str9, charAt);
                                    compiler = compiler2;
                                    str4 = str6;
                                } else {
                                    if (charAt == ']') {
                                        compiler = compiler2;
                                        str4 = str6;
                                        if (!Indent.a(str9, "\\", false, 2)) {
                                            arrayList2.add(str9 + charAt);
                                            break;
                                        }
                                    } else {
                                        compiler = compiler2;
                                        str4 = str6;
                                    }
                                    if (charAt == '0' || charAt == '9') {
                                        i2 = 2;
                                        z = false;
                                        if (Indent.a((CharSequence) str9, (CharSequence) "A", false, 2) || Indent.a((CharSequence) str9, (CharSequence) "a", false, 2) || Indent.a((CharSequence) str9, (CharSequence) "-", false, 2) || Indent.a((CharSequence) str9, (CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, false, 2)) {
                                            arrayList2.add(str9 + "]");
                                            StringBuilder sb = new StringBuilder();
                                            sb.append('[');
                                            sb.append(charAt);
                                            str5 = sb.toString();
                                        }
                                    } else {
                                        i2 = 2;
                                        z = false;
                                    }
                                    if ((charAt == 'A' || charAt == 'a') && (Indent.a((CharSequence) str9, (CharSequence) SessionProtobufHelper.SIGNAL_DEFAULT, z, i2) || Indent.a((CharSequence) str9, (CharSequence) "9", z, i2) || Indent.a((CharSequence) str9, (CharSequence) "-", z, i2) || Indent.a((CharSequence) str9, (CharSequence) EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, z, i2))) {
                                        arrayList2.add(str9 + "]");
                                        StringBuilder sb2 = new StringBuilder();
                                        sb2.append('[');
                                        sb2.append(charAt);
                                        str5 = sb2.toString();
                                    } else if ((charAt == '-' || charAt == '_') && (Indent.a((CharSequence) str9, (CharSequence) SessionProtobufHelper.SIGNAL_DEFAULT, z, i2) || Indent.a((CharSequence) str9, (CharSequence) "9", z, i2) || Indent.a((CharSequence) str9, (CharSequence) "A", z, i2) || Indent.a((CharSequence) str9, (CharSequence) "a", z, i2))) {
                                        arrayList2.add(str9 + "]");
                                        StringBuilder sb3 = new StringBuilder();
                                        sb3.append('[');
                                        sb3.append(charAt);
                                        str5 = sb3.toString();
                                    } else {
                                        str5 = outline.a(str9, charAt);
                                    }
                                }
                                str9 = str5;
                                i5++;
                                length3 = i6;
                                compiler2 = compiler;
                                str6 = str4;
                            }
                        } else {
                            compiler = compiler2;
                            str4 = str6;
                            arrayList2.add(str8);
                        }
                        it2 = it3;
                        compiler2 = compiler;
                        str6 = str4;
                    }
                    Compiler compiler3 = compiler2;
                    String str10 = str6;
                    ArrayList arrayList3 = new ArrayList();
                    Iterator it4 = arrayList2.iterator();
                    while (it4.hasNext()) {
                        String str11 = (String) it4.next();
                        if (Indent.b(str11, "[", false, 2)) {
                            it = it4;
                            if (Indent.a((CharSequence) str11, (CharSequence) SessionProtobufHelper.SIGNAL_DEFAULT, false, 2) || Indent.a((CharSequence) str11, (CharSequence) "9", false, 2)) {
                                str2 = str10;
                                StringBuilder a3 = outline.a("[");
                                String a4 = Indent.a(Indent.a(str11, "[", "", false, 4), "]", "", false, 4);
                                if (a4 != null) {
                                    char[] charArray3 = a4.toCharArray();
                                    Intrinsics.a((Object) charArray3, str2);
                                    str3 = outline.a(a3, _Arrays.a(j.c.a.a.c.n.c.b(charArray3), "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Functions0) null, 62), "]");
                                    arrayList3.add(str3);
                                    it4 = it;
                                    str10 = str2;
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            } else if (Indent.a((CharSequence) str11, (CharSequence) "a", false, 2) || Indent.a((CharSequence) str11, (CharSequence) "A", false, 2)) {
                                str2 = str10;
                                StringBuilder a5 = outline.a("[");
                                String a6 = Indent.a(Indent.a(str11, "[", "", false, 4), "]", "", false, 4);
                                if (a6 != null) {
                                    char[] charArray4 = a6.toCharArray();
                                    Intrinsics.a((Object) charArray4, str2);
                                    str11 = outline.a(a5, _Arrays.a(j.c.a.a.c.n.c.b(charArray4), "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Functions0) null, 62), "]");
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            } else {
                                StringBuilder a7 = outline.a("[");
                                String a8 = Indent.a(Indent.a(Indent.a(Indent.a(str11, "[", "", false, 4), "]", "", false, 4), EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, "A", false, 4), "-", "a", false, 4);
                                if (a8 != null) {
                                    char[] charArray5 = a8.toCharArray();
                                    str2 = str10;
                                    Intrinsics.a((Object) charArray5, str2);
                                    str3 = Indent.a(Indent.a(outline.a(a7, _Arrays.a(j.c.a.a.c.n.c.b(charArray5), "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Functions0) null, 62), "]"), "A", EventsFilesManager.ROLL_OVER_FILE_NAME_SEPARATOR, false, 4), "a", "-", false, 4);
                                    arrayList3.add(str3);
                                    it4 = it;
                                    str10 = str2;
                                } else {
                                    throw new TypeCastException("null cannot be cast to non-null type java.lang.String");
                                }
                            }
                        } else {
                            it = it4;
                            str2 = str10;
                        }
                        str3 = str11;
                        arrayList3.add(str3);
                        it4 = it;
                        str10 = str2;
                    }
                    this.a = compiler3.a(_Arrays.a(arrayList3, "", (CharSequence) null, (CharSequence) null, 0, (CharSequence) null, (Functions0) null, 62), false, false, null);
                    return;
                }
            }
        } else {
            Intrinsics.a("customNotations");
            throw null;
        }
    }

    public a a(CaretString caretString, boolean z) {
        if (caretString != null) {
            CaretStringIterator a2 = a(caretString);
            int i2 = 0;
            int i3 = caretString.b;
            State state = this.a;
            boolean b2 = a2.b();
            boolean a3 = a2.a();
            Character c2 = a2.c();
            String str = "";
            String str2 = str;
            while (c2 != null) {
                Next a4 = state.a(c2.charValue());
                if (a4 != null) {
                    state = a4.a;
                    StringBuilder a5 = outline.a(str);
                    Character ch = a4.b;
                    if (ch == null) {
                        ch = "";
                    }
                    a5.append(ch);
                    str = a5.toString();
                    StringBuilder a6 = outline.a(str2);
                    Character ch2 = a4.d;
                    if (ch2 == null) {
                        ch2 = "";
                    }
                    a6.append(ch2);
                    str2 = a6.toString();
                    if (a4.c) {
                        b2 = a2.b();
                        a3 = a2.a();
                        c2 = a2.c();
                        i2++;
                    } else if (b2 && a4.b != null) {
                        i3++;
                    }
                } else {
                    if (a3) {
                        i3--;
                    }
                    b2 = a2.b();
                    a3 = a2.a();
                    c2 = a2.c();
                }
                i2--;
            }
            while (z && b2) {
                Next a7 = state.a();
                if (a7 == null) {
                    break;
                }
                state = a7.a;
                StringBuilder a8 = outline.a(str);
                Character ch3 = a7.b;
                if (ch3 == null) {
                    ch3 = "";
                }
                a8.append(ch3);
                str = a8.toString();
                StringBuilder a9 = outline.a(str2);
                Character ch4 = a7.d;
                if (ch4 == null) {
                    ch4 = "";
                }
                a9.append(ch4);
                str2 = a9.toString();
                if (a7.b != null) {
                    i3++;
                }
            }
            return new a(new CaretString(str, i3, caretString.c), str2, i2, a(state));
        }
        Intrinsics.a("text");
        throw null;
    }

    public final int b() {
        State state = this.a;
        int i2 = 0;
        while (state != null && !(state instanceof EOLState)) {
            if ((state instanceof FixedState) || (state instanceof ValueState) || (state instanceof OptionalValueState)) {
                i2++;
            }
            state = state.a;
        }
        return i2;
    }

    public CaretStringIterator a(CaretString caretString) {
        if (caretString != null) {
            return new CaretStringIterator(caretString, 0, 2);
        }
        Intrinsics.a("text");
        throw null;
    }

    public final int a() {
        State state = this.a;
        int i2 = 0;
        while (state != null && !(state instanceof EOLState)) {
            if ((state instanceof FixedState) || (state instanceof FreeState) || (state instanceof ValueState) || (state instanceof OptionalValueState)) {
                i2++;
            }
            state = state.a;
        }
        return i2;
    }

    public final boolean a(State state) {
        if (state instanceof EOLState) {
            return true;
        }
        if (state instanceof ValueState) {
            return ((ValueState) state).b instanceof ValueState.a.c;
        }
        if (state instanceof FixedState) {
            return false;
        }
        return a(state.b());
    }
}
