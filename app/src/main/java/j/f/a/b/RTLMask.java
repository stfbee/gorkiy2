package j.f.a.b;

import j.f.a.b.Mask;
import j.f.a.c.CaretString;
import j.f.a.c.c;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: RTLMask.kt */
public final class RTLMask extends Mask {

    /* renamed from: e  reason: collision with root package name */
    public static final Map<String, e> f2611e = new HashMap();

    /* renamed from: f  reason: collision with root package name */
    public static final RTLMask f2612f = null;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public RTLMask(String str, List<c> list) {
        super(j.c.a.a.c.n.c.a(str), list);
        if (str == null) {
            Intrinsics.a("format");
            throw null;
        } else if (list != null) {
        } else {
            Intrinsics.a("customNotations");
            throw null;
        }
    }

    public Mask.a a(CaretString caretString, boolean z) {
        if (caretString != null) {
            Mask.a a = super.a(caretString.a(), z);
            CaretString a2 = a.a.a();
            String str = a.b;
            if (str != null) {
                return new Mask.a(a2, Collections.a((CharSequence) str).toString(), a.c, a.d);
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
        }
        Intrinsics.a("text");
        throw null;
    }

    public CaretStringIterator a(CaretString caretString) {
        if (caretString != null) {
            return new RTLCaretStringIterator(caretString);
        }
        Intrinsics.a("text");
        throw null;
    }
}
