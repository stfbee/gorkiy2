package j.f.a.c;

import j.a.a.a.outline;
import kotlin.TypeCastException;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: CaretString.kt */
public final class CaretString {
    public final String a;
    public final int b;
    public final a c;

    /* compiled from: CaretString.kt */
    public enum a {
        FORWARD,
        BACKWARD
    }

    public CaretString(String str, int i2, a aVar) {
        if (str == null) {
            Intrinsics.a("string");
            throw null;
        } else if (aVar != null) {
            this.a = str;
            this.b = i2;
            this.c = aVar;
        } else {
            Intrinsics.a("caretGravity");
            throw null;
        }
    }

    public final CaretString a() {
        String str = this.a;
        if (str != null) {
            return new CaretString(Collections.a((CharSequence) str).toString(), this.a.length() - this.b, this.c);
        }
        throw new TypeCastException("null cannot be cast to non-null type kotlin.CharSequence");
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    public boolean equals(Object obj) {
        if (this != obj) {
            if (obj instanceof CaretString) {
                CaretString caretString = (CaretString) obj;
                if (Intrinsics.a((Object) this.a, (Object) caretString.a)) {
                    if (!(this.b == caretString.b) || !Intrinsics.a(this.c, caretString.c)) {
                        return false;
                    }
                }
            }
            return false;
        }
        return true;
    }

    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (((str != null ? str.hashCode() : 0) * 31) + this.b) * 31;
        a aVar = this.c;
        if (aVar != null) {
            i2 = aVar.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("CaretString(string=");
        a2.append(this.a);
        a2.append(", caretPosition=");
        a2.append(this.b);
        a2.append(", caretGravity=");
        a2.append(this.c);
        a2.append(")");
        return a2.toString();
    }
}
