package j.f.a;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import androidx.recyclerview.widget.RecyclerView;
import j.a.a.a.outline;
import j.f.a.b.AffinityCalculationStrategy;
import j.f.a.b.Mask;
import j.f.a.b.RTLMask;
import j.f.a.c.CaretString;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;
import kotlin.NoWhenBranchMatchedException;
import n.i.Collections;
import n.i.Collections2;
import n.n.c.Intrinsics;

/* compiled from: MaskedTextChangedListener.kt */
public class MaskedTextChangedListener implements TextWatcher, View.OnFocusChangeListener {
    public String b;
    public int c;
    public final WeakReference<EditText> d;

    /* renamed from: e  reason: collision with root package name */
    public String f2605e;

    /* renamed from: f  reason: collision with root package name */
    public List<String> f2606f;
    public List<j.f.a.c.c> g;
    public AffinityCalculationStrategy h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f2607i;

    /* renamed from: j  reason: collision with root package name */
    public TextWatcher f2608j;

    /* renamed from: k  reason: collision with root package name */
    public a f2609k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f2610l;

    /* compiled from: MaskedTextChangedListener.kt */
    public interface a {
        void a(boolean z, String str, String str2);
    }

    /* compiled from: Comparisons.kt */
    public static final class b<T> implements Comparator<T> {
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
         arg types: [java.lang.Integer, java.lang.Integer]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(java.lang.Object, java.lang.String):T
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int */
        public final int compare(T t2, T t3) {
            return Collections.a((Comparable) Integer.valueOf(((c) t3).b), (Comparable) Integer.valueOf(((c) t2).b));
        }
    }

    /* compiled from: MaskedTextChangedListener.kt */
    public static final class c {
        public final Mask a;
        public final int b;

        public c(Mask mask, int i2) {
            if (mask != null) {
                this.a = mask;
                this.b = i2;
                return;
            }
            Intrinsics.a("mask");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                if (obj instanceof c) {
                    c cVar = (c) obj;
                    if (Intrinsics.a(this.a, cVar.a)) {
                        if (this.b == cVar.b) {
                            return true;
                        }
                    }
                }
                return false;
            }
            return true;
        }

        public int hashCode() {
            Mask mask = this.a;
            return ((mask != null ? mask.hashCode() : 0) * 31) + this.b;
        }

        public String toString() {
            StringBuilder a2 = outline.a("MaskAffinity(mask=");
            a2.append(this.a);
            a2.append(", affinity=");
            a2.append(this.b);
            a2.append(")");
            return a2.toString();
        }
    }

    public MaskedTextChangedListener(String str, EditText editText, a aVar) {
        if (str == null) {
            Intrinsics.a("format");
            throw null;
        } else if (editText != null) {
            Collections2 collections2 = Collections2.b;
            AffinityCalculationStrategy affinityCalculationStrategy = AffinityCalculationStrategy.WHOLE_STRING;
            if (affinityCalculationStrategy != null) {
                this.f2605e = str;
                this.f2606f = collections2;
                this.g = collections2;
                this.h = affinityCalculationStrategy;
                this.f2607i = true;
                this.f2608j = null;
                this.f2609k = aVar;
                this.f2610l = false;
                this.b = "";
                this.d = new WeakReference<>(editText);
                return;
            }
            Intrinsics.a("affinityCalculationStrategy");
            throw null;
        } else {
            Intrinsics.a("field");
            throw null;
        }
    }

    public final Mask a() {
        return a(this.f2605e, this.g);
    }

    public void afterTextChanged(Editable editable) {
        EditText editText = this.d.get();
        if (editText != null) {
            editText.removeTextChangedListener(this);
        }
        if (editable != null) {
            editable.replace(0, editable.length(), this.b);
        }
        EditText editText2 = this.d.get();
        if (editText2 != null) {
            editText2.setSelection(this.c);
        }
        EditText editText3 = this.d.get();
        if (editText3 != null) {
            editText3.addTextChangedListener(this);
        }
        TextWatcher textWatcher = this.f2608j;
        if (textWatcher != null) {
            textWatcher.afterTextChanged(editable);
        }
    }

    public void beforeTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        TextWatcher textWatcher = this.f2608j;
        if (textWatcher != null) {
            textWatcher.beforeTextChanged(charSequence, i2, i3, i4);
        }
    }

    public void onFocusChange(View view, boolean z) {
        String str;
        if (this.f2607i && z) {
            EditText editText = this.d.get();
            Editable editable = null;
            Editable text = editText != null ? editText.getText() : null;
            if (text != null) {
                if (text.length() == 0) {
                    str = "";
                } else {
                    EditText editText2 = this.d.get();
                    if (editText2 != null) {
                        editable = editText2.getText();
                    }
                    str = String.valueOf(editable);
                }
                CaretString caretString = new CaretString(str, str.length(), CaretString.a.FORWARD);
                Mask.a a2 = a(caretString, this.f2607i).a(caretString, this.f2607i);
                CaretString caretString2 = a2.a;
                this.b = caretString2.a;
                this.c = caretString2.b;
                EditText editText3 = this.d.get();
                if (editText3 != null) {
                    editText3.setText(this.b);
                }
                EditText editText4 = this.d.get();
                if (editText4 != null) {
                    editText4.setSelection(a2.a.b);
                }
                a aVar = this.f2609k;
                if (aVar != null) {
                    aVar.a(a2.d, a2.b, this.b);
                    return;
                }
                return;
            }
            Intrinsics.a();
            throw null;
        }
    }

    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        if (charSequence != null) {
            boolean z = false;
            boolean z2 = i3 > 0 && i4 == 0;
            if (!z2) {
                i2 += i4;
            }
            CaretString caretString = new CaretString(charSequence.toString(), i2, z2 ? CaretString.a.BACKWARD : CaretString.a.FORWARD);
            if (!z2) {
                z = this.f2607i;
            }
            Mask.a a2 = a(caretString, z).a(caretString, z);
            CaretString caretString2 = a2.a;
            String str = caretString2.a;
            this.b = str;
            this.c = caretString2.b;
            a aVar = this.f2609k;
            if (aVar != null) {
                aVar.a(a2.d, a2.b, str);
                return;
            }
            return;
        }
        Intrinsics.a("text");
        throw null;
    }

    public final Mask a(CaretString caretString, boolean z) {
        if (this.f2606f.isEmpty()) {
            return a();
        }
        int a2 = a(a(), caretString, z);
        ArrayList arrayList = new ArrayList();
        for (String a3 : this.f2606f) {
            Mask a4 = a(a3, this.g);
            arrayList.add(new c(a4, a(a4, caretString, z)));
        }
        if (arrayList.size() > 1) {
            b bVar = new b();
            if (arrayList.size() > 1) {
                java.util.Collections.sort(arrayList, bVar);
            }
        }
        int i2 = -1;
        Iterator it = arrayList.iterator();
        int i3 = 0;
        while (true) {
            if (!it.hasNext()) {
                break;
            } else if (a2 >= ((c) it.next()).b) {
                i2 = i3;
                break;
            } else {
                i3++;
            }
        }
        if (i2 >= 0) {
            arrayList.add(i2, new c(a(), a2));
        } else {
            arrayList.add(new c(a(), a2));
        }
        if (!arrayList.isEmpty()) {
            return ((c) arrayList.get(0)).a;
        }
        throw new NoSuchElementException("List is empty.");
    }

    public final j.f.a.b.c a(String str, List<j.f.a.c.c> list) {
        if (this.f2610l) {
            RTLMask rTLMask = RTLMask.f2612f;
            if (str == null) {
                Intrinsics.a("format");
                throw null;
            } else if (list != null) {
                RTLMask rTLMask2 = RTLMask.f2611e.get(j.c.a.a.c.n.c.a(str));
                if (rTLMask2 != null) {
                    return rTLMask2;
                }
                RTLMask rTLMask3 = new RTLMask(str, list);
                RTLMask.f2611e.put(j.c.a.a.c.n.c.a(str), rTLMask3);
                return rTLMask3;
            } else {
                Intrinsics.a("customNotations");
                throw null;
            }
        } else {
            Mask mask = Mask.d;
            if (str == null) {
                Intrinsics.a("format");
                throw null;
            } else if (list != null) {
                Mask mask2 = Mask.c.get(str);
                if (mask2 != null) {
                    return mask2;
                }
                Mask mask3 = new Mask(str, list);
                Mask.c.put(str, mask3);
                return mask3;
            } else {
                Intrinsics.a("customNotations");
                throw null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final int a(Mask mask, CaretString caretString, boolean z) {
        String str;
        int i2;
        int i3;
        AffinityCalculationStrategy affinityCalculationStrategy = this.h;
        if (mask == null) {
            Intrinsics.a("mask");
            throw null;
        } else if (caretString != null) {
            int ordinal = affinityCalculationStrategy.ordinal();
            if (ordinal == 0) {
                return mask.a(caretString, z).c;
            }
            boolean z2 = true;
            if (ordinal != 1) {
                if (ordinal != 2) {
                    if (ordinal == 3) {
                        i2 = mask.a(caretString, z).b.length();
                        if (i2 > mask.b()) {
                            return RecyclerView.UNDEFINED_DURATION;
                        }
                        i3 = mask.b();
                    } else {
                        throw new NoWhenBranchMatchedException();
                    }
                } else if (caretString.a.length() > mask.a()) {
                    return RecyclerView.UNDEFINED_DURATION;
                } else {
                    i2 = caretString.a.length();
                    i3 = mask.a();
                }
                return i2 - i3;
            }
            String str2 = mask.a(caretString, z).a.a;
            String str3 = caretString.a;
            if (!(str2.length() == 0)) {
                if (str3.length() != 0) {
                    z2 = false;
                }
                if (!z2) {
                    int i4 = 0;
                    while (true) {
                        if (i4 < str2.length() && i4 < str3.length()) {
                            if (str2.charAt(i4) != str3.charAt(i4)) {
                                str = str2.substring(0, i4);
                                Intrinsics.a((Object) str, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                                break;
                            }
                            i4++;
                        } else {
                            str = str2.substring(0, i4);
                            Intrinsics.a((Object) str, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                        }
                    }
                    str = str2.substring(0, i4);
                    Intrinsics.a((Object) str, "(this as java.lang.Strin…ing(startIndex, endIndex)");
                    return str.length();
                }
            }
            str = "";
            return str.length();
        } else {
            Intrinsics.a("text");
            throw null;
        }
    }
}
