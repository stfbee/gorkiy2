package i.j.b;

import android.content.Context;
import android.util.Log;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.animation.Interpolator;
import android.widget.OverScroller;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import i.h.l.ViewCompat;
import j.a.a.a.outline;
import java.util.Arrays;

public class ViewDragHelper {
    public static final Interpolator v = new a();
    public int a;
    public int b;
    public int c = -1;
    public float[] d;

    /* renamed from: e  reason: collision with root package name */
    public float[] f1245e;

    /* renamed from: f  reason: collision with root package name */
    public float[] f1246f;
    public float[] g;
    public int[] h;

    /* renamed from: i  reason: collision with root package name */
    public int[] f1247i;

    /* renamed from: j  reason: collision with root package name */
    public int[] f1248j;

    /* renamed from: k  reason: collision with root package name */
    public int f1249k;

    /* renamed from: l  reason: collision with root package name */
    public VelocityTracker f1250l;

    /* renamed from: m  reason: collision with root package name */
    public float f1251m;

    /* renamed from: n  reason: collision with root package name */
    public float f1252n;

    /* renamed from: o  reason: collision with root package name */
    public int f1253o;

    /* renamed from: p  reason: collision with root package name */
    public OverScroller f1254p;

    /* renamed from: q  reason: collision with root package name */
    public final c f1255q;

    /* renamed from: r  reason: collision with root package name */
    public View f1256r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f1257s;

    /* renamed from: t  reason: collision with root package name */
    public final ViewGroup f1258t;
    public final Runnable u = new b();

    public static class a implements Interpolator {
        public float getInterpolation(float f2) {
            float f3 = f2 - 1.0f;
            return (f3 * f3 * f3 * f3 * f3) + 1.0f;
        }
    }

    public class b implements Runnable {
        public b() {
        }

        public void run() {
            ViewDragHelper.this.c(0);
        }
    }

    public static abstract class c {
        public int a(View view) {
            return 0;
        }

        public abstract int a(View view, int i2, int i3);

        public abstract void a(int i2);

        public abstract void a(View view, float f2, float f3);

        public void a(View view, int i2) {
        }

        public abstract void a(View view, int i2, int i3, int i4, int i5);

        public int b(View view) {
            return 0;
        }

        public abstract int b(View view, int i2, int i3);

        public abstract boolean b(View view, int i2);
    }

    public ViewDragHelper(Context context, ViewGroup viewGroup, c cVar) {
        if (viewGroup == null) {
            throw new IllegalArgumentException("Parent view may not be null");
        } else if (cVar != null) {
            this.f1258t = viewGroup;
            this.f1255q = cVar;
            ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
            this.f1253o = (int) ((context.getResources().getDisplayMetrics().density * 20.0f) + 0.5f);
            this.b = viewConfiguration.getScaledTouchSlop();
            this.f1251m = (float) viewConfiguration.getScaledMaximumFlingVelocity();
            this.f1252n = (float) viewConfiguration.getScaledMinimumFlingVelocity();
            this.f1254p = new OverScroller(context, v);
        } else {
            throw new IllegalArgumentException("Callback may not be null");
        }
    }

    public void a(View view, int i2) {
        if (view.getParent() == this.f1258t) {
            this.f1256r = view;
            this.c = i2;
            this.f1255q.a(view, i2);
            c(1);
            return;
        }
        StringBuilder a2 = outline.a("captureChildView: parameter must be a descendant of the ViewDragHelper's tracked parent view (");
        a2.append(this.f1258t);
        a2.append(")");
        throw new IllegalArgumentException(a2.toString());
    }

    public boolean b(int i2, int i3) {
        if (this.f1257s) {
            return a(i2, i3, (int) this.f1250l.getXVelocity(this.c), (int) this.f1250l.getYVelocity(this.c));
        }
        throw new IllegalStateException("Cannot settleCapturedViewAt outside of a call to Callback#onViewReleased");
    }

    public void c(int i2) {
        this.f1258t.removeCallbacks(this.u);
        if (this.a != i2) {
            this.a = i2;
            this.f1255q.a(i2);
            if (this.a == 0) {
                this.f1256r = null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.util.Arrays.fill(float[], float):void}
     arg types: [float[], int]
     candidates:
      ClspMth{java.util.Arrays.fill(double[], double):void}
      ClspMth{java.util.Arrays.fill(byte[], byte):void}
      ClspMth{java.util.Arrays.fill(long[], long):void}
      ClspMth{java.util.Arrays.fill(boolean[], boolean):void}
      ClspMth{java.util.Arrays.fill(char[], char):void}
      ClspMth{java.util.Arrays.fill(short[], short):void}
      ClspMth{java.util.Arrays.fill(java.lang.Object[], java.lang.Object):void}
      ClspMth{java.util.Arrays.fill(int[], int):void}
      ClspMth{java.util.Arrays.fill(float[], float):void} */
    public void a() {
        this.c = -1;
        float[] fArr = this.d;
        if (fArr != null) {
            Arrays.fill(fArr, 0.0f);
            Arrays.fill(this.f1245e, 0.0f);
            Arrays.fill(this.f1246f, 0.0f);
            Arrays.fill(this.g, 0.0f);
            Arrays.fill(this.h, 0);
            Arrays.fill(this.f1247i, 0);
            Arrays.fill(this.f1248j, 0);
            this.f1249k = 0;
        }
        VelocityTracker velocityTracker = this.f1250l;
        if (velocityTracker != null) {
            velocityTracker.recycle();
            this.f1250l = null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{java.lang.Math.min(float, float):float}
     arg types: [int, float]
     candidates:
      ClspMth{java.lang.Math.min(double, double):double}
      ClspMth{java.lang.Math.min(long, long):long}
      ClspMth{java.lang.Math.min(int, int):int}
      ClspMth{java.lang.Math.min(float, float):float} */
    public final int b(int i2, int i3, int i4) {
        int i5;
        if (i2 == 0) {
            return 0;
        }
        int width = this.f1258t.getWidth();
        float f2 = (float) (width / 2);
        float sin = (((float) Math.sin((double) ((Math.min(1.0f, ((float) Math.abs(i2)) / ((float) width)) - 0.5f) * 0.47123894f))) * f2) + f2;
        int abs = Math.abs(i3);
        if (abs > 0) {
            i5 = Math.round(Math.abs(sin / ((float) abs)) * 1000.0f) * 4;
        } else {
            i5 = (int) (((((float) Math.abs(i2)) / ((float) i4)) + 1.0f) * 256.0f);
        }
        return Math.min(i5, 600);
    }

    /* JADX WARNING: Code restructure failed: missing block: B:47:0x00db, code lost:
        if (r12 != r11) goto L_0x00e4;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean c(android.view.MotionEvent r17) {
        /*
            r16 = this;
            r0 = r16
            r1 = r17
            int r2 = r17.getActionMasked()
            int r3 = r17.getActionIndex()
            if (r2 != 0) goto L_0x0011
            r16.a()
        L_0x0011:
            android.view.VelocityTracker r4 = r0.f1250l
            if (r4 != 0) goto L_0x001b
            android.view.VelocityTracker r4 = android.view.VelocityTracker.obtain()
            r0.f1250l = r4
        L_0x001b:
            android.view.VelocityTracker r4 = r0.f1250l
            r4.addMovement(r1)
            r4 = 0
            r5 = 2
            r6 = 0
            r7 = 1
            if (r2 == 0) goto L_0x0103
            if (r2 == r7) goto L_0x00fe
            if (r2 == r5) goto L_0x006e
            r8 = 3
            if (r2 == r8) goto L_0x00fe
            r8 = 5
            if (r2 == r8) goto L_0x003e
            r4 = 6
            if (r2 == r4) goto L_0x0036
        L_0x0033:
            r2 = 0
            goto L_0x0132
        L_0x0036:
            int r1 = r1.getPointerId(r3)
            r0.a(r1)
            goto L_0x0033
        L_0x003e:
            int r2 = r1.getPointerId(r3)
            float r8 = r1.getX(r3)
            float r1 = r1.getY(r3)
            r0.b(r8, r1, r2)
            int r3 = r0.a
            if (r3 != 0) goto L_0x005e
            int[] r1 = r0.h
            r1 = r1[r2]
            r1 = r1 & r6
            if (r1 == 0) goto L_0x0033
            i.j.b.ViewDragHelper$c r1 = r0.f1255q
            if (r1 == 0) goto L_0x005d
            goto L_0x0033
        L_0x005d:
            throw r4
        L_0x005e:
            if (r3 != r5) goto L_0x0033
            int r3 = (int) r8
            int r1 = (int) r1
            android.view.View r1 = r0.a(r3, r1)
            android.view.View r3 = r0.f1256r
            if (r1 != r3) goto L_0x0033
            r0.b(r1, r2)
            goto L_0x0033
        L_0x006e:
            float[] r2 = r0.d
            if (r2 == 0) goto L_0x0033
            float[] r2 = r0.f1245e
            if (r2 != 0) goto L_0x0077
            goto L_0x0033
        L_0x0077:
            int r2 = r17.getPointerCount()
            r3 = 0
        L_0x007c:
            if (r3 >= r2) goto L_0x00f9
            int r4 = r1.getPointerId(r3)
            boolean r5 = r0.b(r4)
            if (r5 != 0) goto L_0x008a
            goto L_0x00f5
        L_0x008a:
            float r5 = r1.getX(r3)
            float r8 = r1.getY(r3)
            float[] r9 = r0.d
            r9 = r9[r4]
            float r9 = r5 - r9
            float[] r10 = r0.f1245e
            r10 = r10[r4]
            float r10 = r8 - r10
            int r5 = (int) r5
            int r8 = (int) r8
            android.view.View r5 = r0.a(r5, r8)
            if (r5 == 0) goto L_0x00ae
            boolean r8 = r0.a(r5, r9, r10)
            if (r8 == 0) goto L_0x00ae
            r8 = 1
            goto L_0x00af
        L_0x00ae:
            r8 = 0
        L_0x00af:
            if (r8 == 0) goto L_0x00e4
            int r11 = r5.getLeft()
            int r12 = (int) r9
            int r13 = r11 + r12
            i.j.b.ViewDragHelper$c r14 = r0.f1255q
            int r12 = r14.a(r5, r13, r12)
            int r13 = r5.getTop()
            int r14 = (int) r10
            int r15 = r13 + r14
            i.j.b.ViewDragHelper$c r6 = r0.f1255q
            int r6 = r6.b(r5, r15, r14)
            i.j.b.ViewDragHelper$c r14 = r0.f1255q
            int r14 = r14.a(r5)
            i.j.b.ViewDragHelper$c r15 = r0.f1255q
            int r15 = r15.b(r5)
            if (r14 == 0) goto L_0x00dd
            if (r14 <= 0) goto L_0x00e4
            if (r12 != r11) goto L_0x00e4
        L_0x00dd:
            if (r15 == 0) goto L_0x00f9
            if (r15 <= 0) goto L_0x00e4
            if (r6 != r13) goto L_0x00e4
            goto L_0x00f9
        L_0x00e4:
            r0.a(r9, r10, r4)
            int r6 = r0.a
            if (r6 != r7) goto L_0x00ec
            goto L_0x00f9
        L_0x00ec:
            if (r8 == 0) goto L_0x00f5
            boolean r4 = r0.b(r5, r4)
            if (r4 == 0) goto L_0x00f5
            goto L_0x00f9
        L_0x00f5:
            int r3 = r3 + 1
            r6 = 0
            goto L_0x007c
        L_0x00f9:
            r16.b(r17)
            goto L_0x0033
        L_0x00fe:
            r16.a()
            goto L_0x0033
        L_0x0103:
            float r2 = r17.getX()
            float r3 = r17.getY()
            r6 = 0
            int r1 = r1.getPointerId(r6)
            r0.b(r2, r3, r1)
            int r2 = (int) r2
            int r3 = (int) r3
            android.view.View r2 = r0.a(r2, r3)
            android.view.View r3 = r0.f1256r
            if (r2 != r3) goto L_0x0124
            int r3 = r0.a
            if (r3 != r5) goto L_0x0124
            r0.b(r2, r1)
        L_0x0124:
            int[] r2 = r0.h
            r1 = r2[r1]
            r2 = 0
            r1 = r1 & r2
            if (r1 == 0) goto L_0x0132
            i.j.b.ViewDragHelper$c r1 = r0.f1255q
            if (r1 == 0) goto L_0x0131
            goto L_0x0132
        L_0x0131:
            throw r4
        L_0x0132:
            int r1 = r0.a
            if (r1 != r7) goto L_0x0138
            r6 = 1
            goto L_0x0139
        L_0x0138:
            r6 = 0
        L_0x0139:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: i.j.b.ViewDragHelper.c(android.view.MotionEvent):boolean");
    }

    public final void b(float f2, float f3, int i2) {
        float[] fArr = this.d;
        int i3 = 0;
        if (fArr == null || fArr.length <= i2) {
            int i4 = i2 + 1;
            float[] fArr2 = new float[i4];
            float[] fArr3 = new float[i4];
            float[] fArr4 = new float[i4];
            float[] fArr5 = new float[i4];
            int[] iArr = new int[i4];
            int[] iArr2 = new int[i4];
            int[] iArr3 = new int[i4];
            float[] fArr6 = this.d;
            if (fArr6 != null) {
                System.arraycopy(fArr6, 0, fArr2, 0, fArr6.length);
                float[] fArr7 = this.f1245e;
                System.arraycopy(fArr7, 0, fArr3, 0, fArr7.length);
                float[] fArr8 = this.f1246f;
                System.arraycopy(fArr8, 0, fArr4, 0, fArr8.length);
                float[] fArr9 = this.g;
                System.arraycopy(fArr9, 0, fArr5, 0, fArr9.length);
                int[] iArr4 = this.h;
                System.arraycopy(iArr4, 0, iArr, 0, iArr4.length);
                int[] iArr5 = this.f1247i;
                System.arraycopy(iArr5, 0, iArr2, 0, iArr5.length);
                int[] iArr6 = this.f1248j;
                System.arraycopy(iArr6, 0, iArr3, 0, iArr6.length);
            }
            this.d = fArr2;
            this.f1245e = fArr3;
            this.f1246f = fArr4;
            this.g = fArr5;
            this.h = iArr;
            this.f1247i = iArr2;
            this.f1248j = iArr3;
        }
        float[] fArr10 = this.d;
        this.f1246f[i2] = f2;
        fArr10[i2] = f2;
        float[] fArr11 = this.f1245e;
        this.g[i2] = f3;
        fArr11[i2] = f3;
        int[] iArr7 = this.h;
        int i5 = (int) f2;
        int i6 = (int) f3;
        if (i5 < this.f1258t.getLeft() + this.f1253o) {
            i3 = 1;
        }
        if (i6 < this.f1258t.getTop() + this.f1253o) {
            i3 |= 4;
        }
        if (i5 > this.f1258t.getRight() - this.f1253o) {
            i3 |= 2;
        }
        if (i6 > this.f1258t.getBottom() - this.f1253o) {
            i3 |= 8;
        }
        iArr7[i2] = i3;
        this.f1249k |= 1 << i2;
    }

    public final boolean a(int i2, int i3, int i4, int i5) {
        float f2;
        float f3;
        float f4;
        float f5;
        int left = this.f1256r.getLeft();
        int top = this.f1256r.getTop();
        int i6 = i2 - left;
        int i7 = i3 - top;
        if (i6 == 0 && i7 == 0) {
            this.f1254p.abortAnimation();
            c(0);
            return false;
        }
        View view = this.f1256r;
        int a2 = a(i4, (int) this.f1252n, (int) this.f1251m);
        int a3 = a(i5, (int) this.f1252n, (int) this.f1251m);
        int abs = Math.abs(i6);
        int abs2 = Math.abs(i7);
        int abs3 = Math.abs(a2);
        int abs4 = Math.abs(a3);
        int i8 = abs3 + abs4;
        int i9 = abs + abs2;
        if (a2 != 0) {
            f2 = (float) abs3;
            f3 = (float) i8;
        } else {
            f2 = (float) abs;
            f3 = (float) i9;
        }
        float f6 = f2 / f3;
        if (a3 != 0) {
            f5 = (float) abs4;
            f4 = (float) i8;
        } else {
            f5 = (float) abs2;
            f4 = (float) i9;
        }
        int b2 = b(i6, a2, this.f1255q.a(view));
        float b3 = ((float) b(i7, a3, this.f1255q.b(view))) * (f5 / f4);
        this.f1254p.startScroll(left, top, i6, i7, (int) (b3 + (((float) b2) * f6)));
        c(2);
        return true;
    }

    public final int a(int i2, int i3, int i4) {
        int abs = Math.abs(i2);
        if (abs < i3) {
            return 0;
        }
        if (abs > i4) {
            return i2 > 0 ? i4 : -i4;
        }
        return i2;
    }

    public final float a(float f2, float f3, float f4) {
        float abs = Math.abs(f2);
        if (abs < f3) {
            return 0.0f;
        }
        if (abs > f4) {
            return f2 > 0.0f ? f4 : -f4;
        }
        return f2;
    }

    public boolean a(boolean z) {
        if (this.a == 2) {
            boolean computeScrollOffset = this.f1254p.computeScrollOffset();
            int currX = this.f1254p.getCurrX();
            int currY = this.f1254p.getCurrY();
            int left = currX - this.f1256r.getLeft();
            int top = currY - this.f1256r.getTop();
            if (left != 0) {
                ViewCompat.d(this.f1256r, left);
            }
            if (top != 0) {
                ViewCompat.e(this.f1256r, top);
            }
            if (!(left == 0 && top == 0)) {
                this.f1255q.a(this.f1256r, currX, currY, left, top);
            }
            if (computeScrollOffset && currX == this.f1254p.getFinalX() && currY == this.f1254p.getFinalY()) {
                this.f1254p.abortAnimation();
                computeScrollOffset = false;
            }
            if (!computeScrollOffset) {
                if (z) {
                    this.f1258t.post(this.u);
                } else {
                    c(0);
                }
            }
        }
        if (this.a == 2) {
            return true;
        }
        return false;
    }

    public final void b(MotionEvent motionEvent) {
        int pointerCount = motionEvent.getPointerCount();
        for (int i2 = 0; i2 < pointerCount; i2++) {
            int pointerId = motionEvent.getPointerId(i2);
            if (b(pointerId)) {
                float x = motionEvent.getX(i2);
                float y = motionEvent.getY(i2);
                this.f1246f[pointerId] = x;
                this.g[pointerId] = y;
            }
        }
    }

    public final void a(float f2, float f3) {
        this.f1257s = true;
        this.f1255q.a(this.f1256r, f2, f3);
        this.f1257s = false;
        if (this.a == 1) {
            c(0);
        }
    }

    public final boolean b(int i2) {
        if ((this.f1249k & (1 << i2)) != 0) {
            return true;
        }
        Log.e("ViewDragHelper", "Ignoring pointerId=" + i2 + " because ACTION_DOWN was not received " + "for this pointer before ACTION_MOVE. It likely happened because " + " ViewDragHelper did not receive all the events in the event stream.");
        return false;
    }

    public final void a(int i2) {
        if (this.d != null) {
            boolean z = true;
            int i3 = 1 << i2;
            if ((this.f1249k & i3) == 0) {
                z = false;
            }
            if (z) {
                this.d[i2] = 0.0f;
                this.f1245e[i2] = 0.0f;
                this.f1246f[i2] = 0.0f;
                this.g[i2] = 0.0f;
                this.h[i2] = 0;
                this.f1247i[i2] = 0;
                this.f1248j[i2] = 0;
                this.f1249k &= ~i3;
            }
        }
    }

    public boolean b(View view, int i2) {
        if (view == this.f1256r && this.c == i2) {
            return true;
        }
        if (view == null || !this.f1255q.b(view, i2)) {
            return false;
        }
        this.c = i2;
        a(view, i2);
        return true;
    }

    public final void b() {
        this.f1250l.computeCurrentVelocity(AnswersRetryFilesSender.BACKOFF_MS, this.f1251m);
        a(a(this.f1250l.getXVelocity(this.c), this.f1252n, this.f1251m), a(this.f1250l.getYVelocity(this.c), this.f1252n, this.f1251m));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.j.b.ViewDragHelper.a(float, float):void
     arg types: [int, int]
     candidates:
      i.j.b.ViewDragHelper.a(int, int):android.view.View
      i.j.b.ViewDragHelper.a(android.view.View, int):void
      i.j.b.ViewDragHelper.a(float, float):void */
    public void a(MotionEvent motionEvent) {
        int i2;
        int actionMasked = motionEvent.getActionMasked();
        int actionIndex = motionEvent.getActionIndex();
        if (actionMasked == 0) {
            a();
        }
        if (this.f1250l == null) {
            this.f1250l = VelocityTracker.obtain();
        }
        this.f1250l.addMovement(motionEvent);
        int i3 = 0;
        if (actionMasked == 0) {
            float x = motionEvent.getX();
            float y = motionEvent.getY();
            int pointerId = motionEvent.getPointerId(0);
            View a2 = a((int) x, (int) y);
            b(x, y, pointerId);
            b(a2, pointerId);
            if ((this.h[pointerId] & 0) != 0 && this.f1255q == null) {
                throw null;
            }
        } else if (actionMasked == 1) {
            if (this.a == 1) {
                b();
            }
            a();
        } else if (actionMasked != 2) {
            if (actionMasked == 3) {
                if (this.a == 1) {
                    a(0.0f, 0.0f);
                }
                a();
            } else if (actionMasked == 5) {
                int pointerId2 = motionEvent.getPointerId(actionIndex);
                float x2 = motionEvent.getX(actionIndex);
                float y2 = motionEvent.getY(actionIndex);
                b(x2, y2, pointerId2);
                if (this.a == 0) {
                    b(a((int) x2, (int) y2), pointerId2);
                    if ((this.h[pointerId2] & 0) != 0 && this.f1255q == null) {
                        throw null;
                    }
                    return;
                }
                int i4 = (int) x2;
                int i5 = (int) y2;
                View view = this.f1256r;
                if (view != null && i4 >= view.getLeft() && i4 < view.getRight() && i5 >= view.getTop() && i5 < view.getBottom()) {
                    i3 = 1;
                }
                if (i3 != 0) {
                    b(this.f1256r, pointerId2);
                }
            } else if (actionMasked == 6) {
                int pointerId3 = motionEvent.getPointerId(actionIndex);
                if (this.a == 1 && pointerId3 == this.c) {
                    int pointerCount = motionEvent.getPointerCount();
                    while (true) {
                        if (i3 >= pointerCount) {
                            i2 = -1;
                            break;
                        }
                        int pointerId4 = motionEvent.getPointerId(i3);
                        if (pointerId4 != this.c) {
                            View a3 = a((int) motionEvent.getX(i3), (int) motionEvent.getY(i3));
                            View view2 = this.f1256r;
                            if (a3 == view2 && b(view2, pointerId4)) {
                                i2 = this.c;
                                break;
                            }
                        }
                        i3++;
                    }
                    if (i2 == -1) {
                        b();
                    }
                }
                a(pointerId3);
            }
        } else if (this.a != 1) {
            int pointerCount2 = motionEvent.getPointerCount();
            while (i3 < pointerCount2) {
                int pointerId5 = motionEvent.getPointerId(i3);
                if (b(pointerId5)) {
                    float x3 = motionEvent.getX(i3);
                    float y3 = motionEvent.getY(i3);
                    float f2 = x3 - this.d[pointerId5];
                    float f3 = y3 - this.f1245e[pointerId5];
                    a(f2, f3, pointerId5);
                    if (this.a != 1) {
                        View a4 = a((int) x3, (int) y3);
                        if (a(a4, f2, f3) && b(a4, pointerId5)) {
                            break;
                        }
                    } else {
                        break;
                    }
                }
                i3++;
            }
            b(motionEvent);
        } else if (b(this.c)) {
            int findPointerIndex = motionEvent.findPointerIndex(this.c);
            float x4 = motionEvent.getX(findPointerIndex);
            float y4 = motionEvent.getY(findPointerIndex);
            float[] fArr = this.f1246f;
            int i6 = this.c;
            int i7 = (int) (x4 - fArr[i6]);
            int i8 = (int) (y4 - this.g[i6]);
            int left = this.f1256r.getLeft() + i7;
            int top = this.f1256r.getTop() + i8;
            int left2 = this.f1256r.getLeft();
            int top2 = this.f1256r.getTop();
            if (i7 != 0) {
                left = this.f1255q.a(this.f1256r, left, i7);
                ViewCompat.d(this.f1256r, left - left2);
            }
            int i9 = left;
            if (i8 != 0) {
                top = this.f1255q.b(this.f1256r, top, i8);
                ViewCompat.e(this.f1256r, top - top2);
            }
            int i10 = top;
            if (!(i7 == 0 && i8 == 0)) {
                this.f1255q.a(this.f1256r, i9, i10, i9 - left2, i10 - top2);
            }
            b(motionEvent);
        }
    }

    public final void a(float f2, float f3, int i2) {
        int i3 = 1;
        if (!a(f2, f3, i2, 1)) {
            i3 = 0;
        }
        if (a(f3, f2, i2, 4)) {
            i3 |= 4;
        }
        if (a(f2, f3, i2, 2)) {
            i3 |= 2;
        }
        if (a(f3, f2, i2, 8)) {
            i3 |= 8;
        }
        if (i3 != 0) {
            int[] iArr = this.f1247i;
            iArr[i2] = iArr[i2] | i3;
            if (this.f1255q == null) {
                throw null;
            }
        }
    }

    public final boolean a(float f2, float f3, int i2, int i3) {
        float abs = Math.abs(f2);
        float abs2 = Math.abs(f3);
        if ((this.h[i2] & i3) != i3 || (0 & i3) == 0 || (this.f1248j[i2] & i3) == i3 || (this.f1247i[i2] & i3) == i3) {
            return false;
        }
        int i4 = this.b;
        if (abs <= ((float) i4) && abs2 <= ((float) i4)) {
            return false;
        }
        if (abs < abs2 * 0.5f && this.f1255q == null) {
            throw null;
        } else if ((this.f1247i[i2] & i3) != 0 || abs <= ((float) this.b)) {
            return false;
        } else {
            return true;
        }
    }

    public final boolean a(View view, float f2, float f3) {
        if (view == null) {
            return false;
        }
        boolean z = this.f1255q.a(view) > 0;
        boolean z2 = this.f1255q.b(view) > 0;
        if (z && z2) {
            float f4 = f3 * f3;
            int i2 = this.b;
            if (f4 + (f2 * f2) > ((float) (i2 * i2))) {
                return true;
            }
            return false;
        } else if (z) {
            if (Math.abs(f2) > ((float) this.b)) {
                return true;
            }
            return false;
        } else if (!z2 || Math.abs(f3) <= ((float) this.b)) {
            return false;
        } else {
            return true;
        }
    }

    public View a(int i2, int i3) {
        int childCount = this.f1258t.getChildCount();
        while (true) {
            childCount--;
            if (childCount < 0) {
                return null;
            }
            ViewGroup viewGroup = this.f1258t;
            if (this.f1255q != null) {
                View childAt = viewGroup.getChildAt(childCount);
                if (i2 >= childAt.getLeft() && i2 < childAt.getRight() && i3 >= childAt.getTop() && i3 < childAt.getBottom()) {
                    return childAt;
                }
            } else {
                throw null;
            }
        }
    }
}
