package i.u.a;

import android.security.keystore.KeyGenParameterSpec;

public final class MasterKeys {
    public static final KeyGenParameterSpec a = new KeyGenParameterSpec.Builder("_androidx_security_master_key_", 3).setBlockModes("GCM").setEncryptionPaddings("NoPadding").setKeySize(256).build();
}
