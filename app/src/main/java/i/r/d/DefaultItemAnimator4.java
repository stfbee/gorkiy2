package i.r.d;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: DefaultItemAnimator */
public class DefaultItemAnimator4 extends AnimatorListenerAdapter {
    public final /* synthetic */ RecyclerView.d0 a;
    public final /* synthetic */ int b;
    public final /* synthetic */ View c;
    public final /* synthetic */ int d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ ViewPropertyAnimator f1366e;

    /* renamed from: f  reason: collision with root package name */
    public final /* synthetic */ DefaultItemAnimator7 f1367f;

    public DefaultItemAnimator4(DefaultItemAnimator7 defaultItemAnimator7, RecyclerView.d0 d0Var, int i2, View view, int i3, ViewPropertyAnimator viewPropertyAnimator) {
        this.f1367f = defaultItemAnimator7;
        this.a = d0Var;
        this.b = i2;
        this.c = view;
        this.d = i3;
        this.f1366e = viewPropertyAnimator;
    }

    public void onAnimationCancel(Animator animator) {
        if (this.b != 0) {
            this.c.setTranslationX(0.0f);
        }
        if (this.d != 0) {
            this.c.setTranslationY(0.0f);
        }
    }

    public void onAnimationEnd(Animator animator) {
        this.f1366e.setListener(null);
        this.f1367f.a(this.a);
        this.f1367f.f1376p.remove(this.a);
        this.f1367f.d();
    }

    public void onAnimationStart(Animator animator) {
        if (this.f1367f == null) {
            throw null;
        }
    }
}
