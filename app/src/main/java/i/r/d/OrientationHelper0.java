package i.r.d;

import android.view.View;
import androidx.recyclerview.widget.RecyclerView;

/* compiled from: OrientationHelper */
public final class OrientationHelper0 extends OrientationHelper {
    public OrientationHelper0(RecyclerView.o oVar) {
        super(oVar, null);
    }

    public int a() {
        return super.a.f326q;
    }

    public int b() {
        RecyclerView.o oVar = super.a;
        return oVar.f326q - oVar.k();
    }

    public int c(View view) {
        RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
        return super.a.e(view) + pVar.topMargin + pVar.bottomMargin;
    }

    public int d(View view) {
        return super.a.d(view) - ((RecyclerView.p) view.getLayoutParams()).leftMargin;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
    public int e(View view) {
        super.a.a(view, true, super.c);
        return super.c.right;
    }

    public int f() {
        return super.a.j();
    }

    public int g() {
        RecyclerView.o oVar = super.a;
        return (oVar.f326q - oVar.j()) - super.a.k();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
     arg types: [android.view.View, int, android.graphics.Rect]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void */
    public int f(View view) {
        super.a.a(view, true, super.c);
        return super.c.left;
    }

    public void a(int i2) {
        super.a.d(i2);
    }

    public int c() {
        return super.a.k();
    }

    public int d() {
        return super.a.f324o;
    }

    public int e() {
        return super.a.f325p;
    }

    public int a(View view) {
        return super.a.g(view) + ((RecyclerView.p) view.getLayoutParams()).rightMargin;
    }

    public int b(View view) {
        RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
        return super.a.f(view) + pVar.leftMargin + pVar.rightMargin;
    }
}
