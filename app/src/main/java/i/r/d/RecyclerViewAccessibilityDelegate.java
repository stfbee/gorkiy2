package i.r.d;

import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.view.accessibility.AccessibilityEvent;
import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.core.CodedOutputStream;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.h.l.x.AccessibilityNodeProviderCompat;
import java.util.Map;
import java.util.WeakHashMap;

public class RecyclerViewAccessibilityDelegate extends AccessibilityDelegateCompat {
    public final RecyclerView d;

    /* renamed from: e  reason: collision with root package name */
    public final a f1411e;

    public RecyclerViewAccessibilityDelegate(RecyclerView recyclerView) {
        this.d = recyclerView;
        a aVar = this.f1411e;
        if (aVar != null) {
            this.f1411e = aVar;
        } else {
            this.f1411e = new a(this);
        }
    }

    public boolean a() {
        return this.d.hasPendingAdapterUpdates();
    }

    public void b(View view, AccessibilityEvent accessibilityEvent) {
        super.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
        if ((view instanceof RecyclerView) && !a()) {
            RecyclerView recyclerView = (RecyclerView) view;
            if (recyclerView.getLayoutManager() != null) {
                recyclerView.getLayoutManager().a(accessibilityEvent);
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:29:0x0088 A[ADDED_TO_REGION] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(android.view.View r9, int r10, android.os.Bundle r11) {
        /*
            r8 = this;
            boolean r9 = super.a(r9, r10, r11)
            r11 = 1
            if (r9 == 0) goto L_0x0008
            return r11
        L_0x0008:
            boolean r9 = r8.a()
            r0 = 0
            if (r9 != 0) goto L_0x0096
            androidx.recyclerview.widget.RecyclerView r9 = r8.d
            androidx.recyclerview.widget.RecyclerView$o r9 = r9.getLayoutManager()
            if (r9 == 0) goto L_0x0096
            androidx.recyclerview.widget.RecyclerView r9 = r8.d
            androidx.recyclerview.widget.RecyclerView$o r9 = r9.getLayoutManager()
            androidx.recyclerview.widget.RecyclerView r1 = r9.b
            androidx.recyclerview.widget.RecyclerView$v r2 = r1.mRecycler
            r2 = 4096(0x1000, float:5.74E-42)
            if (r10 == r2) goto L_0x0058
            r2 = 8192(0x2000, float:1.14794E-41)
            if (r10 == r2) goto L_0x002c
            r3 = 0
            r4 = 0
            goto L_0x0086
        L_0x002c:
            r10 = -1
            boolean r1 = r1.canScrollVertically(r10)
            if (r1 == 0) goto L_0x0041
            int r1 = r9.f327r
            int r2 = r9.l()
            int r1 = r1 - r2
            int r2 = r9.i()
            int r1 = r1 - r2
            int r1 = -r1
            goto L_0x0042
        L_0x0041:
            r1 = 0
        L_0x0042:
            androidx.recyclerview.widget.RecyclerView r2 = r9.b
            boolean r10 = r2.canScrollHorizontally(r10)
            if (r10 == 0) goto L_0x0084
            int r10 = r9.f326q
            int r2 = r9.j()
            int r10 = r10 - r2
            int r2 = r9.k()
            int r10 = r10 - r2
            int r10 = -r10
            goto L_0x0081
        L_0x0058:
            boolean r10 = r1.canScrollVertically(r11)
            if (r10 == 0) goto L_0x006c
            int r10 = r9.f327r
            int r1 = r9.l()
            int r10 = r10 - r1
            int r1 = r9.i()
            int r10 = r10 - r1
            r1 = r10
            goto L_0x006d
        L_0x006c:
            r1 = 0
        L_0x006d:
            androidx.recyclerview.widget.RecyclerView r10 = r9.b
            boolean r10 = r10.canScrollHorizontally(r11)
            if (r10 == 0) goto L_0x0084
            int r10 = r9.f326q
            int r2 = r9.j()
            int r10 = r10 - r2
            int r2 = r9.k()
            int r10 = r10 - r2
        L_0x0081:
            r3 = r10
            r4 = r1
            goto L_0x0086
        L_0x0084:
            r4 = r1
            r3 = 0
        L_0x0086:
            if (r4 != 0) goto L_0x008c
            if (r3 != 0) goto L_0x008c
            r11 = 0
            goto L_0x0095
        L_0x008c:
            androidx.recyclerview.widget.RecyclerView r2 = r9.b
            r5 = 0
            r6 = -2147483648(0xffffffff80000000, float:-0.0)
            r7 = 1
            r2.smoothScrollBy(r3, r4, r5, r6, r7)
        L_0x0095:
            return r11
        L_0x0096:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.r.d.RecyclerViewAccessibilityDelegate.a(android.view.View, int, android.os.Bundle):boolean");
    }

    public static class a extends AccessibilityDelegateCompat {
        public final RecyclerViewAccessibilityDelegate d;

        /* renamed from: e  reason: collision with root package name */
        public Map<View, i.h.l.a> f1412e = new WeakHashMap();

        public a(RecyclerViewAccessibilityDelegate recyclerViewAccessibilityDelegate) {
            this.d = recyclerViewAccessibilityDelegate;
        }

        public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            if (this.d.a() || this.d.d.getLayoutManager() == null) {
                super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
                return;
            }
            this.d.d.getLayoutManager().a(view, accessibilityNodeInfoCompat);
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                super.a(view, accessibilityNodeInfoCompat);
            } else {
                super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
            }
        }

        public void b(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                super.b(view, accessibilityEvent);
            } else {
                super.a.onInitializeAccessibilityEvent(view, accessibilityEvent);
            }
        }

        public void c(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                super.c(view, accessibilityEvent);
            } else {
                super.a.onPopulateAccessibilityEvent(view, accessibilityEvent);
            }
        }

        public void d(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                super.d(view, accessibilityEvent);
            } else {
                super.a.sendAccessibilityEventUnchecked(view, accessibilityEvent);
            }
        }

        public boolean a(View view, int i2, Bundle bundle) {
            if (this.d.a() || this.d.d.getLayoutManager() == null) {
                return super.a(view, i2, bundle);
            }
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                if (super.a(view, i2, bundle)) {
                    return true;
                }
            } else if (super.a(view, i2, bundle)) {
                return true;
            }
            RecyclerView.v vVar = this.d.d.getLayoutManager().b.mRecycler;
            return false;
        }

        public void a(View view, int i2) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                super.a(view, i2);
            } else {
                super.a.sendAccessibilityEvent(view, i2);
            }
        }

        public boolean a(View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                return super.a(view, accessibilityEvent);
            }
            return super.a.dispatchPopulateAccessibilityEvent(view, accessibilityEvent);
        }

        public boolean a(ViewGroup viewGroup, View view, AccessibilityEvent accessibilityEvent) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(viewGroup);
            if (accessibilityDelegateCompat != null) {
                return super.a(viewGroup, view, accessibilityEvent);
            }
            return super.a.onRequestSendAccessibilityEvent(viewGroup, view, accessibilityEvent);
        }

        public AccessibilityNodeProviderCompat a(View view) {
            AccessibilityDelegateCompat accessibilityDelegateCompat = this.f1412e.get(view);
            if (accessibilityDelegateCompat != null) {
                return super.a(view);
            }
            return super.a(view);
        }
    }

    public void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        super.a.onInitializeAccessibilityNodeInfo(view, accessibilityNodeInfoCompat.a);
        if (!a() && this.d.getLayoutManager() != null) {
            RecyclerView.o layoutManager = this.d.getLayoutManager();
            RecyclerView recyclerView = layoutManager.b;
            RecyclerView.v vVar = recyclerView.mRecycler;
            RecyclerView.a0 a0Var = recyclerView.mState;
            if (recyclerView.canScrollVertically(-1) || layoutManager.b.canScrollHorizontally(-1)) {
                accessibilityNodeInfoCompat.a.addAction(8192);
                accessibilityNodeInfoCompat.a.setScrollable(true);
            }
            if (layoutManager.b.canScrollVertically(1) || layoutManager.b.canScrollHorizontally(1)) {
                accessibilityNodeInfoCompat.a.addAction((int) CodedOutputStream.DEFAULT_BUFFER_SIZE);
                accessibilityNodeInfoCompat.a.setScrollable(true);
            }
            accessibilityNodeInfoCompat.a(AccessibilityNodeInfoCompat.b.a(layoutManager.b(vVar, a0Var), layoutManager.a(vVar, a0Var), false, 0));
        }
    }
}
