package i.r.d;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.view.View;
import android.view.ViewPropertyAnimator;
import androidx.recyclerview.widget.RecyclerView;
import i.r.d.DefaultItemAnimator7;

/* compiled from: DefaultItemAnimator */
public class DefaultItemAnimator5 extends AnimatorListenerAdapter {
    public final /* synthetic */ DefaultItemAnimator7.a a;
    public final /* synthetic */ ViewPropertyAnimator b;
    public final /* synthetic */ View c;
    public final /* synthetic */ DefaultItemAnimator7 d;

    public DefaultItemAnimator5(DefaultItemAnimator7 defaultItemAnimator7, DefaultItemAnimator7.a aVar, ViewPropertyAnimator viewPropertyAnimator, View view) {
        this.d = defaultItemAnimator7;
        this.a = aVar;
        this.b = viewPropertyAnimator;
        this.c = view;
    }

    public void onAnimationEnd(Animator animator) {
        this.b.setListener(null);
        this.c.setAlpha(1.0f);
        this.c.setTranslationX(0.0f);
        this.c.setTranslationY(0.0f);
        this.d.a(this.a.a);
        this.d.f1378r.remove(this.a.a);
        this.d.d();
    }

    public void onAnimationStart(Animator animator) {
        DefaultItemAnimator7 defaultItemAnimator7 = this.d;
        RecyclerView.d0 d0Var = this.a.a;
        if (defaultItemAnimator7 == null) {
            throw null;
        }
    }
}
