package i.p.a;

import i.o.ViewModelStoreOwner;
import i.o.k;
import i.o.y;
import java.io.FileDescriptor;
import java.io.PrintWriter;

public abstract class LoaderManager {
    public static <T extends k & y> a a(T t2) {
        return new LoaderManagerImpl(t2, ((ViewModelStoreOwner) t2).f());
    }

    @Deprecated
    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);
}
