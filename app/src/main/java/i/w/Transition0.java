package i.w;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import i.e.ArrayMap;

/* compiled from: Transition */
public class Transition0 extends AnimatorListenerAdapter {
    public final /* synthetic */ ArrayMap a;
    public final /* synthetic */ Transition b;

    public Transition0(Transition transition, ArrayMap arrayMap) {
        this.b = transition;
        this.a = arrayMap;
    }

    public void onAnimationEnd(Animator animator) {
        this.a.remove(animator);
        this.b.x.remove(animator);
    }

    public void onAnimationStart(Animator animator) {
        this.b.x.add(animator);
    }
}
