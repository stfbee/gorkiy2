package i.w;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.animation.PropertyValuesHolder;
import android.animation.TypeConverter;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.util.Property;
import android.view.View;
import android.view.ViewGroup;
import i.b.k.ResourcesFlusher;
import i.h.l.ViewCompat;
import i.w.b;
import java.util.Map;

public class ChangeBounds extends Transition {
    public static final String[] M = {"android:changeBounds:bounds", "android:changeBounds:clip", "android:changeBounds:parent", "android:changeBounds:windowX", "android:changeBounds:windowY"};
    public static final Property<Drawable, PointF> N = new b(PointF.class, "boundsOrigin");
    public static final Property<b.k, PointF> O = new c(PointF.class, "topLeft");
    public static final Property<b.k, PointF> P = new d(PointF.class, "bottomRight");
    public static final Property<View, PointF> Q = new e(PointF.class, "bottomRight");
    public static final Property<View, PointF> R = new f(PointF.class, "topLeft");
    public static final Property<View, PointF> S = new g(PointF.class, "position");
    public static RectEvaluator T = new RectEvaluator();
    public int[] J = new int[2];
    public boolean K = false;
    public boolean L = false;

    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ ViewGroup a;
        public final /* synthetic */ BitmapDrawable b;
        public final /* synthetic */ View c;
        public final /* synthetic */ float d;

        public a(ChangeBounds changeBounds, ViewGroup viewGroup, BitmapDrawable bitmapDrawable, View view, float f2) {
            this.a = viewGroup;
            this.b = bitmapDrawable;
            this.c = view;
            this.d = f2;
        }

        public void onAnimationEnd(Animator animator) {
            ViewOverlayImpl a2 = ViewUtils.a(this.a);
            ((ViewOverlayApi18) a2).a.remove(this.b);
            ViewUtils.a.a(this.c, this.d);
        }
    }

    public static class b extends Property<Drawable, PointF> {
        public Rect a = new Rect();

        public b(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            ((Drawable) obj).copyBounds(this.a);
            Rect rect = this.a;
            return new PointF((float) rect.left, (float) rect.top);
        }

        public void set(Object obj, Object obj2) {
            Drawable drawable = (Drawable) obj;
            PointF pointF = (PointF) obj2;
            drawable.copyBounds(this.a);
            this.a.offsetTo(Math.round(pointF.x), Math.round(pointF.y));
            drawable.setBounds(this.a);
        }
    }

    public static class c extends Property<b.k, PointF> {
        public c(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            k kVar = (k) obj;
            return null;
        }

        public void set(Object obj, Object obj2) {
            k kVar = (k) obj;
            PointF pointF = (PointF) obj2;
            if (kVar != null) {
                kVar.a = Math.round(pointF.x);
                int round = Math.round(pointF.y);
                kVar.b = round;
                int i2 = kVar.f1449f + 1;
                kVar.f1449f = i2;
                if (i2 == kVar.g) {
                    ViewUtils.a(kVar.f1448e, kVar.a, round, kVar.c, kVar.d);
                    kVar.f1449f = 0;
                    kVar.g = 0;
                    return;
                }
                return;
            }
            throw null;
        }
    }

    public static class d extends Property<b.k, PointF> {
        public d(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            k kVar = (k) obj;
            return null;
        }

        public void set(Object obj, Object obj2) {
            k kVar = (k) obj;
            PointF pointF = (PointF) obj2;
            if (kVar != null) {
                kVar.c = Math.round(pointF.x);
                int round = Math.round(pointF.y);
                kVar.d = round;
                int i2 = kVar.g + 1;
                kVar.g = i2;
                if (kVar.f1449f == i2) {
                    ViewUtils.a(kVar.f1448e, kVar.a, kVar.b, kVar.c, round);
                    kVar.f1449f = 0;
                    kVar.g = 0;
                    return;
                }
                return;
            }
            throw null;
        }
    }

    public static class e extends Property<View, PointF> {
        public e(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            View view = (View) obj;
            return null;
        }

        public void set(Object obj, Object obj2) {
            View view = (View) obj;
            PointF pointF = (PointF) obj2;
            ViewUtils.a(view, view.getLeft(), view.getTop(), Math.round(pointF.x), Math.round(pointF.y));
        }
    }

    public static class f extends Property<View, PointF> {
        public f(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            View view = (View) obj;
            return null;
        }

        public void set(Object obj, Object obj2) {
            View view = (View) obj;
            PointF pointF = (PointF) obj2;
            ViewUtils.a(view, Math.round(pointF.x), Math.round(pointF.y), view.getRight(), view.getBottom());
        }
    }

    public static class g extends Property<View, PointF> {
        public g(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            View view = (View) obj;
            return null;
        }

        public void set(Object obj, Object obj2) {
            View view = (View) obj;
            PointF pointF = (PointF) obj2;
            int round = Math.round(pointF.x);
            int round2 = Math.round(pointF.y);
            ViewUtils.a(view, round, round2, view.getWidth() + round, view.getHeight() + round2);
        }
    }

    public class h extends AnimatorListenerAdapter {
        public final /* synthetic */ k a;
        public k mViewBounds = this.a;

        public h(ChangeBounds changeBounds, k kVar) {
            this.a = kVar;
        }
    }

    public class i extends AnimatorListenerAdapter {
        public boolean a;
        public final /* synthetic */ View b;
        public final /* synthetic */ Rect c;
        public final /* synthetic */ int d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ int f1446e;

        /* renamed from: f  reason: collision with root package name */
        public final /* synthetic */ int f1447f;
        public final /* synthetic */ int g;

        public i(ChangeBounds changeBounds, View view, Rect rect, int i2, int i3, int i4, int i5) {
            this.b = view;
            this.c = rect;
            this.d = i2;
            this.f1446e = i3;
            this.f1447f = i4;
            this.g = i5;
        }

        public void onAnimationCancel(Animator animator) {
            this.a = true;
        }

        public void onAnimationEnd(Animator animator) {
            if (!this.a) {
                ViewCompat.a(this.b, this.c);
                ViewUtils.a(this.b, this.d, this.f1446e, this.f1447f, this.g);
            }
        }
    }

    public class j extends TransitionListenerAdapter {
        public boolean a = false;
        public final /* synthetic */ ViewGroup b;

        public j(ChangeBounds changeBounds, ViewGroup viewGroup) {
            this.b = viewGroup;
        }

        public void a(Transition transition) {
            ViewGroupUtils.a(this.b, false);
        }

        public void b(Transition transition) {
            ViewGroupUtils.a(this.b, true);
        }

        public void d(Transition transition) {
            ViewGroupUtils.a(this.b, false);
            this.a = true;
        }

        public void e(Transition transition) {
            if (!this.a) {
                ViewGroupUtils.a(this.b, false);
            }
            transition.b(this);
        }
    }

    public static class k {
        public int a;
        public int b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public View f1448e;

        /* renamed from: f  reason: collision with root package name */
        public int f1449f;
        public int g;

        public k(View view) {
            this.f1448e = view;
        }
    }

    public void a(TransitionValues transitionValues) {
        d(transitionValues);
    }

    public String[] c() {
        return M;
    }

    public final void d(TransitionValues transitionValues) {
        View view = transitionValues.b;
        if (ViewCompat.w(view) || view.getWidth() != 0 || view.getHeight() != 0) {
            transitionValues.a.put("android:changeBounds:bounds", new Rect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom()));
            transitionValues.a.put("android:changeBounds:parent", transitionValues.b.getParent());
            if (this.L) {
                transitionValues.b.getLocationInWindow(this.J);
                transitionValues.a.put("android:changeBounds:windowX", Integer.valueOf(this.J[0]));
                transitionValues.a.put("android:changeBounds:windowY", Integer.valueOf(this.J[1]));
            }
            if (this.K) {
                transitionValues.a.put("android:changeBounds:clip", view.getClipBounds());
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.w.ViewUtilsBase.a(android.view.View, float):void
     arg types: [android.view.View, int]
     candidates:
      i.w.ViewUtilsBase.a(android.view.View, int):void
      i.w.ViewUtilsBase.a(android.view.View, android.graphics.Matrix):void
      i.w.ViewUtilsBase.a(android.view.View, float):void */
    public Animator a(ViewGroup viewGroup, TransitionValues transitionValues, TransitionValues transitionValues2) {
        int i2;
        View view;
        Animator animator;
        ObjectAnimator objectAnimator;
        int i3;
        Rect rect;
        ObjectAnimator objectAnimator2;
        TransitionValues b2;
        TransitionValues transitionValues3 = transitionValues;
        TransitionValues transitionValues4 = transitionValues2;
        if (transitionValues3 == null || transitionValues4 == null) {
            return null;
        }
        Map<String, Object> map = transitionValues3.a;
        Map<String, Object> map2 = transitionValues4.a;
        ViewGroup viewGroup2 = (ViewGroup) map.get("android:changeBounds:parent");
        ViewGroup viewGroup3 = (ViewGroup) map2.get("android:changeBounds:parent");
        if (viewGroup2 == null || viewGroup3 == null) {
            return null;
        }
        View view2 = transitionValues4.b;
        if (!this.L || ((b2 = b(viewGroup2, true)) != null ? viewGroup3 == b2.b : viewGroup2 == viewGroup3)) {
            Rect rect2 = (Rect) transitionValues3.a.get("android:changeBounds:bounds");
            Rect rect3 = (Rect) transitionValues4.a.get("android:changeBounds:bounds");
            int i4 = rect2.left;
            int i5 = rect3.left;
            int i6 = rect2.top;
            int i7 = rect3.top;
            int i8 = rect2.right;
            int i9 = rect3.right;
            int i10 = rect2.bottom;
            int i11 = rect3.bottom;
            int i12 = i8 - i4;
            int i13 = i10 - i6;
            int i14 = i9 - i5;
            int i15 = i11 - i7;
            View view3 = view2;
            Rect rect4 = (Rect) transitionValues3.a.get("android:changeBounds:clip");
            Rect rect5 = (Rect) transitionValues4.a.get("android:changeBounds:clip");
            if ((i12 == 0 || i13 == 0) && (i14 == 0 || i15 == 0)) {
                i2 = 0;
            } else {
                i2 = (i4 == i5 && i6 == i7) ? 0 : 1;
                if (!(i8 == i9 && i10 == i11)) {
                    i2++;
                }
            }
            if ((rect4 != null && !rect4.equals(rect5)) || (rect4 == null && rect5 != null)) {
                i2++;
            }
            if (i2 <= 0) {
                return null;
            }
            Rect rect6 = rect5;
            Rect rect7 = rect4;
            if (!this.K) {
                view = view3;
                ViewUtils.a(view, i4, i6, i8, i10);
                if (i2 == 2) {
                    if (i12 == i14 && i13 == i15) {
                        animator = ResourcesFlusher.a(view, S, super.F.a((float) i4, (float) i6, (float) i5, (float) i7));
                    } else {
                        k kVar = new k(view);
                        ObjectAnimator a2 = ResourcesFlusher.a(kVar, O, super.F.a((float) i4, (float) i6, (float) i5, (float) i7));
                        ObjectAnimator a3 = ResourcesFlusher.a(kVar, P, super.F.a((float) i8, (float) i10, (float) i9, (float) i11));
                        AnimatorSet animatorSet = new AnimatorSet();
                        animatorSet.playTogether(a2, a3);
                        animatorSet.addListener(new h(this, kVar));
                        animator = animatorSet;
                    }
                } else if (i4 == i5 && i6 == i7) {
                    animator = ResourcesFlusher.a(view, Q, super.F.a((float) i8, (float) i10, (float) i9, (float) i11));
                } else {
                    animator = ResourcesFlusher.a(view, R, super.F.a((float) i4, (float) i6, (float) i5, (float) i7));
                }
            } else {
                view = view3;
                ViewUtils.a(view, i4, i6, Math.max(i12, i14) + i4, Math.max(i13, i15) + i6);
                if (i4 == i5 && i6 == i7) {
                    objectAnimator = null;
                } else {
                    objectAnimator = ResourcesFlusher.a(view, S, super.F.a((float) i4, (float) i6, (float) i5, (float) i7));
                }
                if (rect7 == null) {
                    i3 = 0;
                    rect = new Rect(0, 0, i12, i13);
                } else {
                    i3 = 0;
                    rect = rect7;
                }
                Rect rect8 = rect6 == null ? new Rect(i3, i3, i14, i15) : rect6;
                if (!rect.equals(rect8)) {
                    ViewCompat.a(view, rect);
                    RectEvaluator rectEvaluator = T;
                    Object[] objArr = new Object[2];
                    objArr[i3] = rect;
                    objArr[1] = rect8;
                    ObjectAnimator ofObject = ObjectAnimator.ofObject(view, "clipBounds", rectEvaluator, objArr);
                    ofObject.addListener(new i(this, view, rect6, i5, i7, i9, i11));
                    objectAnimator2 = ofObject;
                } else {
                    objectAnimator2 = null;
                }
                animator = TransitionUtils.a(objectAnimator, objectAnimator2);
            }
            if (view.getParent() instanceof ViewGroup) {
                ViewGroup viewGroup4 = (ViewGroup) view.getParent();
                ViewGroupUtils.a(viewGroup4, true);
                a(new j(this, viewGroup4));
            }
            return animator;
        }
        int intValue = ((Integer) transitionValues3.a.get("android:changeBounds:windowX")).intValue();
        int intValue2 = ((Integer) transitionValues3.a.get("android:changeBounds:windowY")).intValue();
        int intValue3 = ((Integer) transitionValues4.a.get("android:changeBounds:windowX")).intValue();
        int intValue4 = ((Integer) transitionValues4.a.get("android:changeBounds:windowY")).intValue();
        if (intValue == intValue3 && intValue2 == intValue4) {
            return null;
        }
        viewGroup.getLocationInWindow(this.J);
        Bitmap createBitmap = Bitmap.createBitmap(view2.getWidth(), view2.getHeight(), Bitmap.Config.ARGB_8888);
        view2.draw(new Canvas(createBitmap));
        BitmapDrawable bitmapDrawable = new BitmapDrawable(createBitmap);
        float b3 = ViewUtils.b(view2);
        ViewUtils.a.a(view2, 0.0f);
        viewGroup.getOverlay().add(bitmapDrawable);
        PathMotion pathMotion = super.F;
        int[] iArr = this.J;
        ObjectAnimator ofPropertyValuesHolder = ObjectAnimator.ofPropertyValuesHolder(bitmapDrawable, PropertyValuesHolder.ofObject(N, (TypeConverter) null, pathMotion.a((float) (intValue - iArr[0]), (float) (intValue2 - iArr[1]), (float) (intValue3 - iArr[0]), (float) (intValue4 - iArr[1]))));
        ofPropertyValuesHolder.addListener(new a(this, viewGroup, bitmapDrawable, view2, b3));
        return ofPropertyValuesHolder;
    }

    public void c(TransitionValues transitionValues) {
        d(transitionValues);
    }
}
