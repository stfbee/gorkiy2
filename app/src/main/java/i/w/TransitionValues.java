package i.w;

import android.view.View;
import j.a.a.a.outline;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class TransitionValues {
    public final Map<String, Object> a = new HashMap();
    public View b;
    public final ArrayList<i> c = new ArrayList<>();

    @Deprecated
    public TransitionValues() {
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof TransitionValues)) {
            return false;
        }
        TransitionValues transitionValues = (TransitionValues) obj;
        return this.b == transitionValues.b && this.a.equals(transitionValues.a);
    }

    public int hashCode() {
        return this.a.hashCode() + (this.b.hashCode() * 31);
    }

    public String toString() {
        StringBuilder a2 = outline.a("TransitionValues@");
        a2.append(Integer.toHexString(hashCode()));
        a2.append(":\n");
        StringBuilder b2 = outline.b(a2.toString(), "    view = ");
        b2.append(this.b);
        b2.append("\n");
        String a3 = outline.a(b2.toString(), "    values:");
        for (String next : this.a.keySet()) {
            a3 = a3 + "    " + next + ": " + this.a.get(next) + "\n";
        }
        return a3;
    }

    public TransitionValues(View view) {
        this.b = view;
    }
}
