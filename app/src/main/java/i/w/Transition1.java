package i.w;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;

/* compiled from: Transition */
public class Transition1 extends AnimatorListenerAdapter {
    public final /* synthetic */ Transition a;

    public Transition1(Transition transition) {
        this.a = transition;
    }

    public void onAnimationEnd(Animator animator) {
        this.a.b();
        animator.removeListener(this);
    }
}
