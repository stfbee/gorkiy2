package i.w;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.view.View;
import android.view.ViewGroup;
import i.h.l.ViewCompat;

public class Fade extends Visibility {

    public class a extends TransitionListenerAdapter {
        public final /* synthetic */ View a;

        public a(Fade fade, View view) {
            this.a = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.w.ViewUtilsBase.a(android.view.View, float):void
         arg types: [android.view.View, int]
         candidates:
          i.w.ViewUtilsBase.a(android.view.View, int):void
          i.w.ViewUtilsBase.a(android.view.View, android.graphics.Matrix):void
          i.w.ViewUtilsBase.a(android.view.View, float):void */
        public void e(Transition transition) {
            ViewUtils.a.a(this.a, 1.0f);
            ViewUtils.a.a(this.a);
            transition.b(this);
        }
    }

    public static class b extends AnimatorListenerAdapter {
        public final View a;
        public boolean b = false;

        public b(View view) {
            this.a = view;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: i.w.ViewUtilsBase.a(android.view.View, float):void
         arg types: [android.view.View, int]
         candidates:
          i.w.ViewUtilsBase.a(android.view.View, int):void
          i.w.ViewUtilsBase.a(android.view.View, android.graphics.Matrix):void
          i.w.ViewUtilsBase.a(android.view.View, float):void */
        public void onAnimationEnd(Animator animator) {
            ViewUtils.a.a(this.a, 1.0f);
            if (this.b) {
                this.a.setLayerType(0, null);
            }
        }

        public void onAnimationStart(Animator animator) {
            if (ViewCompat.t(this.a) && this.a.getLayerType() == 0) {
                this.b = true;
                this.a.setLayerType(2, null);
            }
        }
    }

    public Fade(int i2) {
        if ((i2 & -4) == 0) {
            super.J = i2;
            return;
        }
        throw new IllegalArgumentException("Only MODE_IN and MODE_OUT flags are allowed");
    }

    public final Animator a(View view, float f2, float f3) {
        if (f2 == f3) {
            return null;
        }
        ViewUtils.a.a(view, f2);
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(view, ViewUtils.b, f3);
        ofFloat.addListener(new b(view));
        a(new a(this, view));
        return ofFloat;
    }

    public void c(TransitionValues transitionValues) {
        d(transitionValues);
        transitionValues.a.put("android:fade:transitionAlpha", Float.valueOf(ViewUtils.b(transitionValues.b)));
    }

    public Animator a(ViewGroup viewGroup, View view, TransitionValues transitionValues, TransitionValues transitionValues2) {
        Float f2;
        ViewUtils.a.c(view);
        return a(view, (transitionValues == null || (f2 = (Float) transitionValues.a.get("android:fade:transitionAlpha")) == null) ? 1.0f : f2.floatValue(), 0.0f);
    }
}
