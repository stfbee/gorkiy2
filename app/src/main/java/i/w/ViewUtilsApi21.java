package i.w;

import android.annotation.SuppressLint;
import android.graphics.Matrix;
import android.view.View;

public class ViewUtilsApi21 extends ViewUtilsApi19 {

    /* renamed from: f  reason: collision with root package name */
    public static boolean f1468f = true;
    public static boolean g = true;

    @SuppressLint({"NewApi"})
    public void a(View view, Matrix matrix) {
        if (f1468f) {
            try {
                view.transformMatrixToGlobal(matrix);
            } catch (NoSuchMethodError unused) {
                f1468f = false;
            }
        }
    }

    @SuppressLint({"NewApi"})
    public void b(View view, Matrix matrix) {
        if (g) {
            try {
                view.transformMatrixToLocal(matrix);
            } catch (NoSuchMethodError unused) {
                g = false;
            }
        }
    }
}
