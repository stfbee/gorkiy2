package i.e;

import java.util.ConcurrentModificationException;
import java.util.Map;

public class SimpleArrayMap<K, V> {

    /* renamed from: e  reason: collision with root package name */
    public static Object[] f1070e;

    /* renamed from: f  reason: collision with root package name */
    public static int f1071f;
    public static Object[] g;
    public static int h;
    public int[] b;
    public Object[] c;
    public int d;

    public SimpleArrayMap() {
        this.b = ContainerHelpers.a;
        this.c = ContainerHelpers.c;
        this.d = 0;
    }

    public int a(Object obj, int i2) {
        int i3 = this.d;
        if (i3 == 0) {
            return -1;
        }
        try {
            int a = ContainerHelpers.a(this.b, i3, i2);
            if (a < 0 || obj.equals(this.c[a << 1])) {
                return a;
            }
            int i4 = a + 1;
            while (i4 < i3 && this.b[i4] == i2) {
                if (obj.equals(this.c[i4 << 1])) {
                    return i4;
                }
                i4++;
            }
            int i5 = a - 1;
            while (i5 >= 0 && this.b[i5] == i2) {
                if (obj.equals(this.c[i5 << 1])) {
                    return i5;
                }
                i5--;
            }
            return ~i4;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    public void b(int i2) {
        int i3 = this.d;
        int[] iArr = this.b;
        if (iArr.length < i2) {
            Object[] objArr = this.c;
            a(i2);
            if (this.d > 0) {
                System.arraycopy(iArr, 0, this.b, 0, i3);
                System.arraycopy(objArr, 0, this.c, 0, i3 << 1);
            }
            a(iArr, objArr, i3);
        }
        if (this.d != i3) {
            throw new ConcurrentModificationException();
        }
    }

    public K c(int i2) {
        return this.c[i2 << 1];
    }

    public void clear() {
        int i2 = this.d;
        if (i2 > 0) {
            int[] iArr = this.b;
            Object[] objArr = this.c;
            this.b = ContainerHelpers.a;
            this.c = ContainerHelpers.c;
            this.d = 0;
            a(iArr, objArr, i2);
        }
        if (this.d > 0) {
            throw new ConcurrentModificationException();
        }
    }

    public boolean containsKey(Object obj) {
        return a(obj) >= 0;
    }

    public boolean containsValue(Object obj) {
        return b(obj) >= 0;
    }

    public V d(int i2) {
        V[] vArr = this.c;
        int i3 = i2 << 1;
        V v = vArr[i3 + 1];
        int i4 = this.d;
        int i5 = 0;
        if (i4 <= 1) {
            a(this.b, vArr, i4);
            this.b = ContainerHelpers.a;
            this.c = ContainerHelpers.c;
        } else {
            int i6 = i4 - 1;
            int[] iArr = this.b;
            int i7 = 8;
            if (iArr.length <= 8 || i4 >= iArr.length / 3) {
                if (i2 < i6) {
                    int[] iArr2 = this.b;
                    int i8 = i2 + 1;
                    int i9 = i6 - i2;
                    System.arraycopy(iArr2, i8, iArr2, i2, i9);
                    Object[] objArr = this.c;
                    System.arraycopy(objArr, i8 << 1, objArr, i3, i9 << 1);
                }
                Object[] objArr2 = this.c;
                int i10 = i6 << 1;
                objArr2[i10] = null;
                objArr2[i10 + 1] = null;
            } else {
                if (i4 > 8) {
                    i7 = i4 + (i4 >> 1);
                }
                int[] iArr3 = this.b;
                Object[] objArr3 = this.c;
                a(i7);
                if (i4 == this.d) {
                    if (i2 > 0) {
                        System.arraycopy(iArr3, 0, this.b, 0, i2);
                        System.arraycopy(objArr3, 0, this.c, 0, i3);
                    }
                    if (i2 < i6) {
                        int i11 = i2 + 1;
                        int i12 = i6 - i2;
                        System.arraycopy(iArr3, i11, this.b, i2, i12);
                        System.arraycopy(objArr3, i11 << 1, this.c, i3, i12 << 1);
                    }
                } else {
                    throw new ConcurrentModificationException();
                }
            }
            i5 = i6;
        }
        if (i4 == this.d) {
            this.d = i5;
            return v;
        }
        throw new ConcurrentModificationException();
    }

    public V e(int i2) {
        return this.c[(i2 << 1) + 1];
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj instanceof SimpleArrayMap) {
            SimpleArrayMap simpleArrayMap = (SimpleArrayMap) obj;
            if (this.d != simpleArrayMap.d) {
                return false;
            }
            int i2 = 0;
            while (i2 < this.d) {
                try {
                    Object c2 = c(i2);
                    Object e2 = e(i2);
                    Object obj2 = simpleArrayMap.get(c2);
                    if (e2 == null) {
                        if (obj2 != null || !simpleArrayMap.containsKey(c2)) {
                            return false;
                        }
                    } else if (!e2.equals(obj2)) {
                        return false;
                    }
                    i2++;
                } catch (ClassCastException | NullPointerException unused) {
                    return false;
                }
            }
            return true;
        }
        if (obj instanceof Map) {
            Map map = (Map) obj;
            if (this.d != map.size()) {
                return false;
            }
            int i3 = 0;
            while (i3 < this.d) {
                try {
                    Object c3 = c(i3);
                    Object e3 = e(i3);
                    Object obj3 = map.get(c3);
                    if (e3 == null) {
                        if (obj3 != null || !map.containsKey(c3)) {
                            return false;
                        }
                    } else if (!e3.equals(obj3)) {
                        return false;
                    }
                    i3++;
                } catch (ClassCastException | NullPointerException unused2) {
                }
            }
            return true;
        }
        return false;
    }

    public V get(Object obj) {
        return getOrDefault(obj, null);
    }

    public V getOrDefault(Object obj, V v) {
        int a = a(obj);
        return a >= 0 ? this.c[(a << 1) + 1] : v;
    }

    public int hashCode() {
        int[] iArr = this.b;
        Object[] objArr = this.c;
        int i2 = this.d;
        int i3 = 1;
        int i4 = 0;
        int i5 = 0;
        while (i4 < i2) {
            Object obj = objArr[i3];
            i5 += (obj == null ? 0 : obj.hashCode()) ^ iArr[i4];
            i4++;
            i3 += 2;
        }
        return i5;
    }

    public boolean isEmpty() {
        return this.d <= 0;
    }

    public V put(K k2, V v) {
        int i2;
        int i3;
        int i4 = this.d;
        if (k2 == null) {
            i3 = a();
            i2 = 0;
        } else {
            int hashCode = k2.hashCode();
            i2 = hashCode;
            i3 = a(k2, hashCode);
        }
        if (i3 >= 0) {
            int i5 = (i3 << 1) + 1;
            V[] vArr = this.c;
            V v2 = vArr[i5];
            vArr[i5] = v;
            return v2;
        }
        int i6 = ~i3;
        if (i4 >= this.b.length) {
            int i7 = 4;
            if (i4 >= 8) {
                i7 = (i4 >> 1) + i4;
            } else if (i4 >= 4) {
                i7 = 8;
            }
            int[] iArr = this.b;
            Object[] objArr = this.c;
            a(i7);
            if (i4 == this.d) {
                int[] iArr2 = this.b;
                if (iArr2.length > 0) {
                    System.arraycopy(iArr, 0, iArr2, 0, iArr.length);
                    System.arraycopy(objArr, 0, this.c, 0, objArr.length);
                }
                a(iArr, objArr, i4);
            } else {
                throw new ConcurrentModificationException();
            }
        }
        if (i6 < i4) {
            int[] iArr3 = this.b;
            int i8 = i6 + 1;
            System.arraycopy(iArr3, i6, iArr3, i8, i4 - i6);
            Object[] objArr2 = this.c;
            System.arraycopy(objArr2, i6 << 1, objArr2, i8 << 1, (this.d - i6) << 1);
        }
        int i9 = this.d;
        if (i4 == i9) {
            int[] iArr4 = this.b;
            if (i6 < iArr4.length) {
                iArr4[i6] = i2;
                Object[] objArr3 = this.c;
                int i10 = i6 << 1;
                objArr3[i10] = k2;
                objArr3[i10 + 1] = v;
                this.d = i9 + 1;
                return null;
            }
        }
        throw new ConcurrentModificationException();
    }

    public V putIfAbsent(K k2, V v) {
        V orDefault = getOrDefault(k2, null);
        return orDefault == null ? put(k2, v) : orDefault;
    }

    public V remove(Object obj) {
        int a = a(obj);
        if (a >= 0) {
            return d(a);
        }
        return null;
    }

    public V replace(K k2, V v) {
        int a = a((Object) k2);
        if (a >= 0) {
            return a(a, v);
        }
        return null;
    }

    public int size() {
        return this.d;
    }

    public String toString() {
        if (isEmpty()) {
            return "{}";
        }
        StringBuilder sb = new StringBuilder(this.d * 28);
        sb.append('{');
        for (int i2 = 0; i2 < this.d; i2++) {
            if (i2 > 0) {
                sb.append(", ");
            }
            Object c2 = c(i2);
            if (c2 != this) {
                sb.append(c2);
            } else {
                sb.append("(this Map)");
            }
            sb.append('=');
            Object e2 = e(i2);
            if (e2 != this) {
                sb.append(e2);
            } else {
                sb.append("(this Map)");
            }
        }
        sb.append('}');
        return sb.toString();
    }

    public boolean remove(Object obj, Object obj2) {
        int a = a(obj);
        if (a < 0) {
            return false;
        }
        Object e2 = e(a);
        if (obj2 != e2 && (obj2 == null || !obj2.equals(e2))) {
            return false;
        }
        d(a);
        return true;
    }

    public boolean replace(K k2, V v, V v2) {
        int a = a((Object) k2);
        if (a < 0) {
            return false;
        }
        V e2 = e(a);
        if (e2 != v && (v == null || !v.equals(e2))) {
            return false;
        }
        a(a, v2);
        return true;
    }

    public SimpleArrayMap(int i2) {
        if (i2 == 0) {
            this.b = ContainerHelpers.a;
            this.c = ContainerHelpers.c;
        } else {
            a(i2);
        }
        this.d = 0;
    }

    public int a() {
        int i2 = this.d;
        if (i2 == 0) {
            return -1;
        }
        try {
            int a = ContainerHelpers.a(this.b, i2, 0);
            if (a < 0 || this.c[a << 1] == null) {
                return a;
            }
            int i3 = a + 1;
            while (i3 < i2 && this.b[i3] == 0) {
                if (this.c[i3 << 1] == null) {
                    return i3;
                }
                i3++;
            }
            int i4 = a - 1;
            while (i4 >= 0 && this.b[i4] == 0) {
                if (this.c[i4 << 1] == null) {
                    return i4;
                }
                i4--;
            }
            return ~i3;
        } catch (ArrayIndexOutOfBoundsException unused) {
            throw new ConcurrentModificationException();
        }
    }

    public int b(Object obj) {
        int i2 = this.d * 2;
        Object[] objArr = this.c;
        if (obj == null) {
            for (int i3 = 1; i3 < i2; i3 += 2) {
                if (objArr[i3] == null) {
                    return i3 >> 1;
                }
            }
            return -1;
        }
        for (int i4 = 1; i4 < i2; i4 += 2) {
            if (obj.equals(objArr[i4])) {
                return i4 >> 1;
            }
        }
        return -1;
    }

    public final void a(int i2) {
        Class<SimpleArrayMap> cls = SimpleArrayMap.class;
        if (i2 == 8) {
            synchronized (cls) {
                if (g != null) {
                    Object[] objArr = g;
                    this.c = objArr;
                    g = (Object[]) objArr[0];
                    this.b = (int[]) objArr[1];
                    objArr[1] = null;
                    objArr[0] = null;
                    h--;
                    return;
                }
            }
        } else if (i2 == 4) {
            synchronized (cls) {
                if (f1070e != null) {
                    Object[] objArr2 = f1070e;
                    this.c = objArr2;
                    f1070e = (Object[]) objArr2[0];
                    this.b = (int[]) objArr2[1];
                    objArr2[1] = null;
                    objArr2[0] = null;
                    f1071f--;
                    return;
                }
            }
        }
        this.b = new int[i2];
        this.c = new Object[(i2 << 1)];
    }

    public static void a(int[] iArr, Object[] objArr, int i2) {
        Class<SimpleArrayMap> cls = SimpleArrayMap.class;
        if (iArr.length == 8) {
            synchronized (cls) {
                if (h < 10) {
                    objArr[0] = g;
                    objArr[1] = iArr;
                    for (int i3 = (i2 << 1) - 1; i3 >= 2; i3--) {
                        objArr[i3] = null;
                    }
                    g = objArr;
                    h++;
                }
            }
        } else if (iArr.length == 4) {
            synchronized (cls) {
                if (f1071f < 10) {
                    objArr[0] = f1070e;
                    objArr[1] = iArr;
                    for (int i4 = (i2 << 1) - 1; i4 >= 2; i4--) {
                        objArr[i4] = null;
                    }
                    f1070e = objArr;
                    f1071f++;
                }
            }
        }
    }

    public int a(Object obj) {
        return obj == null ? a() : a(obj, obj.hashCode());
    }

    public V a(int i2, Object obj) {
        int i3 = (i2 << 1) + 1;
        Object[] objArr = this.c;
        Object obj2 = objArr[i3];
        objArr[i3] = obj;
        return obj2;
    }

    public void a(SimpleArrayMap simpleArrayMap) {
        int i2 = simpleArrayMap.d;
        b(this.d + i2);
        if (this.d != 0) {
            for (int i3 = 0; i3 < i2; i3++) {
                put(simpleArrayMap.c(i3), simpleArrayMap.e(i3));
            }
        } else if (i2 > 0) {
            System.arraycopy(simpleArrayMap.b, 0, this.b, 0, i2);
            System.arraycopy(simpleArrayMap.c, 0, this.c, 0, i2 << 1);
            this.d = i2;
        }
    }
}
