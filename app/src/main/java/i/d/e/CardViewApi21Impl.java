package i.d.e;

import android.graphics.Rect;
import androidx.cardview.widget.CardView;

public class CardViewApi21Impl implements CardViewImpl {
    public void a(CardViewDelegate cardViewDelegate, float f2) {
        RoundRectDrawable a = a(cardViewDelegate);
        CardView.a aVar = (CardView.a) cardViewDelegate;
        boolean useCompatPadding = CardView.this.getUseCompatPadding();
        boolean a2 = aVar.a();
        if (!(f2 == a.f1052e && a.f1053f == useCompatPadding && a.g == a2)) {
            a.f1052e = f2;
            a.f1053f = useCompatPadding;
            a.g = a2;
            a.a((Rect) null);
            a.invalidateSelf();
        }
        d(cardViewDelegate);
    }

    public float b(CardViewDelegate cardViewDelegate) {
        return a(cardViewDelegate).f1052e;
    }

    public float c(CardViewDelegate cardViewDelegate) {
        return a(cardViewDelegate).a;
    }

    public void d(CardViewDelegate cardViewDelegate) {
        CardView.a aVar = (CardView.a) cardViewDelegate;
        if (!CardView.this.getUseCompatPadding()) {
            aVar.a(0, 0, 0, 0);
            return;
        }
        float f2 = a(cardViewDelegate).f1052e;
        float f3 = a(cardViewDelegate).a;
        int ceil = (int) Math.ceil((double) RoundRectDrawableWithShadow.a(f2, f3, aVar.a()));
        int ceil2 = (int) Math.ceil((double) RoundRectDrawableWithShadow.b(f2, f3, aVar.a()));
        aVar.a(ceil, ceil2, ceil, ceil2);
    }

    public final RoundRectDrawable a(CardViewDelegate cardViewDelegate) {
        return (RoundRectDrawable) ((CardView.a) cardViewDelegate).a;
    }
}
