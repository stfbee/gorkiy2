package i.g.d;

import i.e.SimpleArrayMap;
import i.h.k.Pools;
import i.h.k.Pools0;
import java.util.ArrayList;
import java.util.HashSet;

public final class DirectedAcyclicGraph<T> {
    public final Pools<ArrayList<T>> a = new Pools0(10);
    public final SimpleArrayMap<T, ArrayList<T>> b = new SimpleArrayMap<>();
    public final ArrayList<T> c = new ArrayList<>();
    public final HashSet<T> d = new HashSet<>();

    public void a(T t2) {
        if (!(this.b.a(t2) >= 0)) {
            this.b.put(t2, null);
        }
    }

    public final void a(T t2, ArrayList<T> arrayList, HashSet<T> hashSet) {
        if (!arrayList.contains(t2)) {
            if (!hashSet.contains(t2)) {
                hashSet.add(t2);
                ArrayList orDefault = this.b.getOrDefault(t2, null);
                if (orDefault != null) {
                    int size = orDefault.size();
                    for (int i2 = 0; i2 < size; i2++) {
                        a(orDefault.get(i2), arrayList, hashSet);
                    }
                }
                hashSet.remove(t2);
                arrayList.add(t2);
                return;
            }
            throw new RuntimeException("This graph contains cyclic dependencies");
        }
    }
}
