package i.b.k;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;
import i.b.k.AppCompatDelegateImpl;
import i.b.p.ActionMode;
import i.b.p.SupportMenuInflater;
import i.b.q.AppCompatDrawableManager;
import i.b.q.VectorEnabledTintResources;
import i.h.d.ActivityCompat;
import i.h.d.TaskStackBuilder;
import i.h.e.ContextCompat;
import i.l.a.FragmentActivity;
import java.util.ArrayList;

public class AppCompatActivity extends FragmentActivity implements AppCompatCallback, TaskStackBuilder {

    /* renamed from: o  reason: collision with root package name */
    public AppCompatDelegate f736o;

    /* renamed from: p  reason: collision with root package name */
    public Resources f737p;

    public ActionMode a(ActionMode.a aVar) {
        return null;
    }

    public void a(Toolbar toolbar) {
        CharSequence charSequence;
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        if (appCompatDelegateImpl.d instanceof Activity) {
            appCompatDelegateImpl.j();
            ActionBar actionBar = appCompatDelegateImpl.f740i;
            if (!(actionBar instanceof WindowDecorActionBar)) {
                appCompatDelegateImpl.f741j = null;
                if (actionBar != null) {
                    actionBar.f();
                }
                if (toolbar != null) {
                    Object obj = appCompatDelegateImpl.d;
                    if (obj instanceof Activity) {
                        charSequence = ((Activity) obj).getTitle();
                    } else {
                        charSequence = appCompatDelegateImpl.f742k;
                    }
                    ToolbarActionBar toolbarActionBar = new ToolbarActionBar(toolbar, charSequence, appCompatDelegateImpl.g);
                    appCompatDelegateImpl.f740i = toolbarActionBar;
                    appCompatDelegateImpl.f739f.setCallback(toolbarActionBar.c);
                } else {
                    appCompatDelegateImpl.f740i = null;
                    appCompatDelegateImpl.f739f.setCallback(appCompatDelegateImpl.g);
                }
                appCompatDelegateImpl.b();
                return;
            }
            throw new IllegalStateException("This Activity already has an action bar supplied by the window decor. Do not request Window.FEATURE_SUPPORT_ACTION_BAR and set windowActionBar to false in your theme to use a Toolbar instead.");
        }
    }

    public void a(ActionMode actionMode) {
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        m().a(view, layoutParams);
    }

    public void attachBaseContext(Context context) {
        super.attachBaseContext(context);
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        appCompatDelegateImpl.a(false);
        appCompatDelegateImpl.J = true;
    }

    public void b(ActionMode actionMode) {
    }

    public void closeOptionsMenu() {
        ActionBar n2 = n();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (n2 == null || !n2.a()) {
            super.closeOptionsMenu();
        }
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        ActionBar n2 = n();
        if (keyCode != 82 || n2 == null || !n2.a(keyEvent)) {
            return super.dispatchKeyEvent(keyEvent);
        }
        return true;
    }

    public Intent e() {
        return ResourcesFlusher.a((Activity) this);
    }

    public <T extends View> T findViewById(int i2) {
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        appCompatDelegateImpl.f();
        return appCompatDelegateImpl.f739f.findViewById(i2);
    }

    public MenuInflater getMenuInflater() {
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        if (appCompatDelegateImpl.f741j == null) {
            appCompatDelegateImpl.j();
            ActionBar actionBar = appCompatDelegateImpl.f740i;
            appCompatDelegateImpl.f741j = new SupportMenuInflater(actionBar != null ? actionBar.d() : appCompatDelegateImpl.f738e);
        }
        return appCompatDelegateImpl.f741j;
    }

    public Resources getResources() {
        if (this.f737p == null) {
            boolean z = VectorEnabledTintResources.a;
        }
        Resources resources = this.f737p;
        return resources == null ? super.getResources() : resources;
    }

    public void invalidateOptionsMenu() {
        m().b();
    }

    public void l() {
        m().b();
    }

    public AppCompatDelegate m() {
        if (this.f736o == null) {
            this.f736o = AppCompatDelegate.a(this, this);
        }
        return this.f736o;
    }

    public ActionBar n() {
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        appCompatDelegateImpl.j();
        return appCompatDelegateImpl.f740i;
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        if (this.f737p != null) {
            this.f737p.updateConfiguration(configuration, super.getResources().getDisplayMetrics());
        }
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        if (appCompatDelegateImpl.A && appCompatDelegateImpl.u) {
            appCompatDelegateImpl.j();
            ActionBar actionBar = appCompatDelegateImpl.f740i;
            if (actionBar != null) {
                actionBar.a(configuration);
            }
        }
        AppCompatDrawableManager.a().a(appCompatDelegateImpl.f738e);
        appCompatDelegateImpl.a(false);
    }

    public void onContentChanged() {
    }

    public void onCreate(Bundle bundle) {
        AppCompatDelegate m2 = m();
        m2.a();
        m2.a(bundle);
        super.onCreate(bundle);
    }

    public void onDestroy() {
        super.onDestroy();
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        if (appCompatDelegateImpl != null) {
            AppCompatDelegate.b(appCompatDelegateImpl);
            if (appCompatDelegateImpl.T) {
                appCompatDelegateImpl.f739f.getDecorView().removeCallbacks(appCompatDelegateImpl.V);
            }
            appCompatDelegateImpl.L = false;
            appCompatDelegateImpl.M = true;
            ActionBar actionBar = appCompatDelegateImpl.f740i;
            if (actionBar != null) {
                actionBar.f();
            }
            AppCompatDelegateImpl.g gVar = appCompatDelegateImpl.R;
            if (gVar != null) {
                gVar.a();
            }
            AppCompatDelegateImpl.g gVar2 = appCompatDelegateImpl.S;
            if (gVar2 != null) {
                gVar2.a();
                return;
            }
            return;
        }
        throw null;
    }

    public boolean onKeyDown(int i2, KeyEvent keyEvent) {
        Window window;
        if (Build.VERSION.SDK_INT < 26 && !keyEvent.isCtrlPressed() && !KeyEvent.metaStateHasNoModifiers(keyEvent.getMetaState()) && keyEvent.getRepeatCount() == 0 && !KeyEvent.isModifierKey(keyEvent.getKeyCode()) && (window = getWindow()) != null && window.getDecorView() != null && window.getDecorView().dispatchKeyShortcutEvent(keyEvent)) {
            return true;
        }
        return super.onKeyDown(i2, keyEvent);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
     arg types: [i.b.k.AppCompatActivity, android.content.ComponentName]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent */
    public final boolean onMenuItemSelected(int i2, MenuItem menuItem) {
        Intent a;
        if (super.onMenuItemSelected(i2, menuItem)) {
            return true;
        }
        ActionBar n2 = n();
        if (menuItem.getItemId() != 16908332 || n2 == null || (n2.c() & 4) == 0 || (a = ResourcesFlusher.a((Activity) this)) == null) {
            return false;
        }
        if (shouldUpRecreateTask(a)) {
            ArrayList arrayList = new ArrayList();
            Intent e2 = e();
            if (e2 == null) {
                e2 = ResourcesFlusher.a((Activity) this);
            }
            if (e2 != null) {
                ComponentName component = e2.getComponent();
                if (component == null) {
                    component = e2.resolveActivity(getPackageManager());
                }
                int size = arrayList.size();
                try {
                    Intent a2 = ResourcesFlusher.a((Context) this, component);
                    while (a2 != null) {
                        arrayList.add(size, a2);
                        a2 = ResourcesFlusher.a((Context) this, a2.getComponent());
                    }
                    arrayList.add(e2);
                } catch (PackageManager.NameNotFoundException e3) {
                    Log.e("TaskStackBuilder", "Bad ComponentName while traversing activity parent metadata");
                    throw new IllegalArgumentException(e3);
                }
            }
            if (!arrayList.isEmpty()) {
                Intent[] intentArr = (Intent[]) arrayList.toArray(new Intent[arrayList.size()]);
                intentArr[0] = new Intent(intentArr[0]).addFlags(268484608);
                ContextCompat.a(this, intentArr, null);
                try {
                    ActivityCompat.a((Activity) this);
                    return true;
                } catch (IllegalStateException unused) {
                    finish();
                    return true;
                }
            } else {
                throw new IllegalStateException("No intents added to TaskStackBuilder; cannot startActivities");
            }
        } else {
            navigateUpTo(a);
            return true;
        }
    }

    public boolean onMenuOpened(int i2, Menu menu) {
        return super.onMenuOpened(i2, menu);
    }

    public void onPanelClosed(int i2, Menu menu) {
        super.onPanelClosed(i2, menu);
    }

    public void onPostCreate(Bundle bundle) {
        super.onPostCreate(bundle);
        ((AppCompatDelegateImpl) m()).f();
    }

    public void onPostResume() {
        super.onPostResume();
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        appCompatDelegateImpl.j();
        ActionBar actionBar = appCompatDelegateImpl.f740i;
        if (actionBar != null) {
            actionBar.d(true);
        }
    }

    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        if (appCompatDelegateImpl.N != -100) {
            AppCompatDelegateImpl.a0.put(appCompatDelegateImpl.d.getClass(), Integer.valueOf(appCompatDelegateImpl.N));
        }
    }

    public void onStart() {
        super.onStart();
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) m();
        appCompatDelegateImpl.L = true;
        appCompatDelegateImpl.d();
        AppCompatDelegate.a(appCompatDelegateImpl);
    }

    public void onStop() {
        super.onStop();
        m().c();
    }

    public void onTitleChanged(CharSequence charSequence, int i2) {
        super.onTitleChanged(charSequence, i2);
        m().a(charSequence);
    }

    public void openOptionsMenu() {
        ActionBar n2 = n();
        if (!getWindow().hasFeature(0)) {
            return;
        }
        if (n2 == null || !n2.g()) {
            super.openOptionsMenu();
        }
    }

    public void setContentView(int i2) {
        m().b(i2);
    }

    public void setTheme(int i2) {
        super.setTheme(i2);
        ((AppCompatDelegateImpl) m()).O = i2;
    }

    public void setContentView(View view) {
        m().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        m().b(view, layoutParams);
    }
}
