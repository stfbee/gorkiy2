package i.b.k;

import android.view.View;
import android.view.WindowInsets;
import i.h.l.OnApplyWindowInsetsListener;
import i.h.l.ViewCompat;
import i.h.l.WindowInsetsCompat;

/* compiled from: AppCompatDelegateImpl */
public class AppCompatDelegateImpl1 implements OnApplyWindowInsetsListener {
    public final /* synthetic */ AppCompatDelegateImpl a;

    public AppCompatDelegateImpl1(AppCompatDelegateImpl appCompatDelegateImpl) {
        this.a = appCompatDelegateImpl;
    }

    public WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
        int d = windowInsetsCompat.d();
        int f2 = this.a.f(d);
        if (d != f2) {
            windowInsetsCompat = new WindowInsetsCompat(((WindowInsets) windowInsetsCompat.a).replaceSystemWindowInsets(windowInsetsCompat.b(), f2, windowInsetsCompat.c(), windowInsetsCompat.a()));
        }
        return ViewCompat.a(view, windowInsetsCompat);
    }
}
