package i.b.k;

import android.app.Dialog;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import i.b.p.ActionMode;
import i.h.l.KeyEventDispatcher;

public class AppCompatDialog extends Dialog implements AppCompatCallback {
    public AppCompatDelegate b;
    public final KeyEventDispatcher.a c;

    public class a implements KeyEventDispatcher.a {
        public a() {
        }

        public boolean a(KeyEvent keyEvent) {
            return AppCompatDialog.this.a(keyEvent);
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public AppCompatDialog(android.content.Context r5, int r6) {
        /*
            r4 = this;
            r0 = 1
            if (r6 != 0) goto L_0x0014
            android.util.TypedValue r1 = new android.util.TypedValue
            r1.<init>()
            android.content.res.Resources$Theme r2 = r5.getTheme()
            int r3 = i.b.a.dialogTheme
            r2.resolveAttribute(r3, r1, r0)
            int r1 = r1.resourceId
            goto L_0x0015
        L_0x0014:
            r1 = r6
        L_0x0015:
            r4.<init>(r5, r1)
            i.b.k.AppCompatDialog$a r1 = new i.b.k.AppCompatDialog$a
            r1.<init>()
            r4.c = r1
            i.b.k.AppCompatDelegate r1 = r4.a()
            if (r6 != 0) goto L_0x0035
            android.util.TypedValue r6 = new android.util.TypedValue
            r6.<init>()
            android.content.res.Resources$Theme r5 = r5.getTheme()
            int r2 = i.b.a.dialogTheme
            r5.resolveAttribute(r2, r6, r0)
            int r6 = r6.resourceId
        L_0x0035:
            r5 = r1
            i.b.k.AppCompatDelegateImpl r5 = (i.b.k.AppCompatDelegateImpl) r5
            r5.O = r6
            r5 = 0
            r1.a(r5)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.k.AppCompatDialog.<init>(android.content.Context, int):void");
    }

    public ActionMode a(ActionMode.a aVar) {
        return null;
    }

    public void a(ActionMode actionMode) {
    }

    public boolean a(int i2) {
        return a().a(i2);
    }

    public void addContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().a(view, layoutParams);
    }

    public void b(ActionMode actionMode) {
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        return KeyEventDispatcher.a(this.c, getWindow().getDecorView(), this, keyEvent);
    }

    public <T extends View> T findViewById(int i2) {
        AppCompatDelegateImpl appCompatDelegateImpl = (AppCompatDelegateImpl) a();
        appCompatDelegateImpl.f();
        return appCompatDelegateImpl.f739f.findViewById(i2);
    }

    public void invalidateOptionsMenu() {
        a().b();
    }

    public void onCreate(Bundle bundle) {
        a().a();
        super.onCreate(bundle);
        a().a(bundle);
    }

    public void onStop() {
        super.onStop();
        a().c();
    }

    public void setContentView(int i2) {
        a().b(i2);
    }

    public void setTitle(CharSequence charSequence) {
        super.setTitle(charSequence);
        a().a(charSequence);
    }

    public AppCompatDelegate a() {
        if (this.b == null) {
            this.b = AppCompatDelegate.a(super, this);
        }
        return this.b;
    }

    public void setContentView(View view) {
        a().a(view);
    }

    public void setContentView(View view, ViewGroup.LayoutParams layoutParams) {
        a().b(view, layoutParams);
    }

    public void setTitle(int i2) {
        super.setTitle(i2);
        a().a(getContext().getString(i2));
    }

    public boolean a(KeyEvent keyEvent) {
        return super.dispatchKeyEvent(keyEvent);
    }
}
