package i.b.k;

import android.view.View;
import androidx.appcompat.app.AlertController;

/* compiled from: AlertController */
public class AlertController1 implements Runnable {
    public final /* synthetic */ View b;
    public final /* synthetic */ View c;
    public final /* synthetic */ AlertController d;

    public AlertController1(AlertController alertController, View view, View view2) {
        this.d = alertController;
        this.b = view;
        this.c = view2;
    }

    public void run() {
        AlertController.a(this.d.g, this.b, this.c);
    }
}
