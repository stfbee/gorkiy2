package i.b.k;

import android.content.Context;
import android.content.res.Configuration;
import android.view.KeyCharacterMap;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import androidx.appcompat.widget.Toolbar;
import i.b.k.a;
import i.b.p.WindowCallbackWrapper;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuPresenter;
import i.b.q.DecorToolbar;
import i.b.q.ToolbarWidgetWrapper0;
import i.h.l.ViewCompat;
import java.util.ArrayList;

public class ToolbarActionBar extends ActionBar {
    public DecorToolbar a;
    public boolean b;
    public Window.Callback c;
    public boolean d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f778e;

    /* renamed from: f  reason: collision with root package name */
    public ArrayList<a.b> f779f = new ArrayList<>();
    public final Runnable g = new a();
    public final Toolbar.f h = new b();

    public class a implements Runnable {
        public a() {
        }

        public void run() {
            ToolbarActionBar toolbarActionBar = ToolbarActionBar.this;
            Menu h = toolbarActionBar.h();
            MenuBuilder menuBuilder = h instanceof MenuBuilder ? (MenuBuilder) h : null;
            if (menuBuilder != null) {
                menuBuilder.j();
            }
            try {
                h.clear();
                if (!toolbarActionBar.c.onCreatePanelMenu(0, h) || !toolbarActionBar.c.onPreparePanel(0, null, h)) {
                    h.clear();
                }
            } finally {
                if (menuBuilder != null) {
                    menuBuilder.i();
                }
            }
        }
    }

    public class b implements Toolbar.f {
        public b() {
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            return ToolbarActionBar.this.c.onMenuItemSelected(0, menuItem);
        }
    }

    public final class d implements MenuBuilder.a {
        public d() {
        }

        public void a(MenuBuilder menuBuilder) {
            ToolbarActionBar toolbarActionBar = ToolbarActionBar.this;
            if (toolbarActionBar.c == null) {
                return;
            }
            if (toolbarActionBar.a.b()) {
                ToolbarActionBar.this.c.onPanelClosed(108, menuBuilder);
            } else if (ToolbarActionBar.this.c.onPreparePanel(0, null, menuBuilder)) {
                ToolbarActionBar.this.c.onMenuOpened(108, menuBuilder);
            }
        }

        public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
            return false;
        }
    }

    public class e extends WindowCallbackWrapper {
        public e(Window.Callback callback) {
            super(callback);
        }

        public View onCreatePanelView(int i2) {
            if (i2 == 0) {
                return new View(ToolbarActionBar.this.a.c());
            }
            return super.b.onCreatePanelView(i2);
        }

        public boolean onPreparePanel(int i2, View view, Menu menu) {
            boolean onPreparePanel = super.b.onPreparePanel(i2, view, menu);
            if (onPreparePanel) {
                ToolbarActionBar toolbarActionBar = ToolbarActionBar.this;
                if (!toolbarActionBar.b) {
                    toolbarActionBar.a.f();
                    ToolbarActionBar.this.b = true;
                }
            }
            return onPreparePanel;
        }
    }

    public ToolbarActionBar(Toolbar toolbar, CharSequence charSequence, Window.Callback callback) {
        this.a = new ToolbarWidgetWrapper0(toolbar, false);
        e eVar = new e(callback);
        this.c = eVar;
        this.a.setWindowCallback(eVar);
        toolbar.setOnMenuItemClickListener(this.h);
        this.a.setWindowTitle(charSequence);
    }

    public void a(Configuration configuration) {
    }

    public void a(CharSequence charSequence) {
        this.a.setWindowTitle(charSequence);
    }

    public void b(boolean z) {
    }

    public boolean b() {
        if (!this.a.n()) {
            return false;
        }
        this.a.collapseActionView();
        return true;
    }

    public void c(boolean z) {
        this.a.c(((z ? 4 : 0) & 4) | (-5 & this.a.i()));
    }

    public Context d() {
        return this.a.c();
    }

    public void d(boolean z) {
    }

    public boolean e() {
        this.a.k().removeCallbacks(this.g);
        ViewCompat.a(this.a.k(), this.g);
        return true;
    }

    public void f() {
        this.a.k().removeCallbacks(this.g);
    }

    public boolean g() {
        return this.a.e();
    }

    public final Menu h() {
        if (!this.d) {
            this.a.a(new c(), new d());
            this.d = true;
        }
        return this.a.j();
    }

    public final class c implements MenuPresenter.a {
        public boolean b;

        public c() {
        }

        public boolean a(MenuBuilder menuBuilder) {
            Window.Callback callback = ToolbarActionBar.this.c;
            if (callback == null) {
                return false;
            }
            callback.onMenuOpened(108, menuBuilder);
            return true;
        }

        public void a(MenuBuilder menuBuilder, boolean z) {
            if (!this.b) {
                this.b = true;
                ToolbarActionBar.this.a.h();
                Window.Callback callback = ToolbarActionBar.this.c;
                if (callback != null) {
                    callback.onPanelClosed(108, menuBuilder);
                }
                this.b = false;
            }
        }
    }

    public boolean a() {
        return this.a.d();
    }

    public boolean a(KeyEvent keyEvent) {
        if (keyEvent.getAction() == 1) {
            this.a.e();
        }
        return true;
    }

    public int c() {
        return this.a.i();
    }

    public boolean a(int i2, KeyEvent keyEvent) {
        Menu h2 = h();
        if (h2 == null) {
            return false;
        }
        boolean z = true;
        if (KeyCharacterMap.load(keyEvent != null ? keyEvent.getDeviceId() : -1).getKeyboardType() == 1) {
            z = false;
        }
        h2.setQwertyMode(z);
        return h2.performShortcut(i2, keyEvent, 0);
    }

    public void a(boolean z) {
        if (z != this.f778e) {
            this.f778e = z;
            int size = this.f779f.size();
            for (int i2 = 0; i2 < size; i2++) {
                this.f779f.get(i2).a(z);
            }
        }
    }
}
