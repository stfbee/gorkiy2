package i.b.k;

import android.view.View;

public class AlertController implements Runnable {
    public final /* synthetic */ View b;
    public final /* synthetic */ View c;
    public final /* synthetic */ androidx.appcompat.app.AlertController d;

    public AlertController(androidx.appcompat.app.AlertController alertController, View view, View view2) {
        this.d = alertController;
        this.b = view;
        this.c = view2;
    }

    public void run() {
        androidx.appcompat.app.AlertController.a(this.d.A, this.b, this.c);
    }
}
