package i.b.q;

import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.util.AttributeSet;
import android.view.View;
import i.b.j;
import i.h.l.ViewCompat;

public class AppCompatBackgroundHelper {
    public final View a;
    public final AppCompatDrawableManager b;
    public int c = -1;
    public TintInfo d;

    /* renamed from: e  reason: collision with root package name */
    public TintInfo f949e;

    /* renamed from: f  reason: collision with root package name */
    public TintInfo f950f;

    public AppCompatBackgroundHelper(View view) {
        this.a = view;
        this.b = AppCompatDrawableManager.a();
    }

    public void a(AttributeSet attributeSet, int i2) {
        TintTypedArray a2 = TintTypedArray.a(this.a.getContext(), attributeSet, j.ViewBackgroundHelper, i2, 0);
        try {
            if (a2.f(j.ViewBackgroundHelper_android_background)) {
                this.c = a2.f(j.ViewBackgroundHelper_android_background, -1);
                ColorStateList b2 = this.b.b(this.a.getContext(), this.c);
                if (b2 != null) {
                    a(b2);
                }
            }
            if (a2.f(j.ViewBackgroundHelper_backgroundTint)) {
                ViewCompat.a(this.a, a2.a(j.ViewBackgroundHelper_backgroundTint));
            }
            if (a2.f(j.ViewBackgroundHelper_backgroundTintMode)) {
                ViewCompat.a(this.a, DrawableUtils.a(a2.d(j.ViewBackgroundHelper_backgroundTintMode, -1), null));
            }
        } finally {
            a2.b.recycle();
        }
    }

    public void b(ColorStateList colorStateList) {
        if (this.f949e == null) {
            this.f949e = new TintInfo();
        }
        TintInfo tintInfo = this.f949e;
        tintInfo.a = colorStateList;
        tintInfo.d = true;
        a();
    }

    public PorterDuff.Mode c() {
        TintInfo tintInfo = this.f949e;
        if (tintInfo != null) {
            return tintInfo.b;
        }
        return null;
    }

    public void d() {
        this.c = -1;
        a((ColorStateList) null);
        a();
    }

    public ColorStateList b() {
        TintInfo tintInfo = this.f949e;
        if (tintInfo != null) {
            return tintInfo.a;
        }
        return null;
    }

    public void a(int i2) {
        this.c = i2;
        AppCompatDrawableManager appCompatDrawableManager = this.b;
        a(appCompatDrawableManager != null ? appCompatDrawableManager.b(this.a.getContext(), i2) : null);
        a();
    }

    public void a(PorterDuff.Mode mode) {
        if (this.f949e == null) {
            this.f949e = new TintInfo();
        }
        TintInfo tintInfo = this.f949e;
        tintInfo.b = mode;
        tintInfo.c = true;
        a();
    }

    public void a() {
        Drawable background = this.a.getBackground();
        if (background != null) {
            int i2 = Build.VERSION.SDK_INT;
            boolean z = true;
            if (i2 <= 21 ? i2 == 21 : this.d != null) {
                if (this.f950f == null) {
                    this.f950f = new TintInfo();
                }
                TintInfo tintInfo = this.f950f;
                tintInfo.a = null;
                tintInfo.d = false;
                tintInfo.b = null;
                tintInfo.c = false;
                ColorStateList d2 = ViewCompat.d(this.a);
                if (d2 != null) {
                    tintInfo.d = true;
                    tintInfo.a = d2;
                }
                PorterDuff.Mode backgroundTintMode = this.a.getBackgroundTintMode();
                if (backgroundTintMode != null) {
                    tintInfo.c = true;
                    tintInfo.b = backgroundTintMode;
                }
                if (tintInfo.d || tintInfo.c) {
                    AppCompatDrawableManager.a(background, tintInfo, this.a.getDrawableState());
                } else {
                    z = false;
                }
                if (z) {
                    return;
                }
            }
            TintInfo tintInfo2 = this.f949e;
            if (tintInfo2 != null) {
                AppCompatDrawableManager.a(background, tintInfo2, this.a.getDrawableState());
                return;
            }
            TintInfo tintInfo3 = this.d;
            if (tintInfo3 != null) {
                AppCompatDrawableManager.a(background, tintInfo3, this.a.getDrawableState());
            }
        }
    }

    public void a(ColorStateList colorStateList) {
        if (colorStateList != null) {
            if (this.d == null) {
                this.d = new TintInfo();
            }
            TintInfo tintInfo = this.d;
            tintInfo.a = colorStateList;
            tintInfo.d = true;
        } else {
            this.d = null;
        }
        a();
    }
}
