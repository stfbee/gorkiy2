package i.b.q;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Outline;
import android.graphics.drawable.Drawable;
import androidx.appcompat.widget.ActionBarContainer;

public class ActionBarBackgroundDrawable extends Drawable {
    public final ActionBarContainer a;

    public ActionBarBackgroundDrawable(ActionBarContainer actionBarContainer) {
        this.a = actionBarContainer;
    }

    public void draw(Canvas canvas) {
        ActionBarContainer actionBarContainer = this.a;
        if (actionBarContainer.f58i) {
            Drawable drawable = actionBarContainer.h;
            if (drawable != null) {
                super.draw(canvas);
                return;
            }
            return;
        }
        Drawable drawable2 = actionBarContainer.f57f;
        if (drawable2 != null) {
            super.draw(canvas);
        }
        ActionBarContainer actionBarContainer2 = this.a;
        Drawable drawable3 = actionBarContainer2.g;
        if (drawable3 != null && actionBarContainer2.f59j) {
            super.draw(canvas);
        }
    }

    public int getOpacity() {
        return 0;
    }

    public void getOutline(Outline outline) {
        ActionBarContainer actionBarContainer = this.a;
        if (actionBarContainer.f58i) {
            Drawable drawable = actionBarContainer.h;
            if (drawable != null) {
                super.getOutline(outline);
                return;
            }
            return;
        }
        Drawable drawable2 = actionBarContainer.f57f;
        if (drawable2 != null) {
            super.getOutline(outline);
        }
    }

    public void setAlpha(int i2) {
    }

    public void setColorFilter(ColorFilter colorFilter) {
    }
}
