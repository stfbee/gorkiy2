package i.b.q;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.view.Menu;
import android.view.ViewGroup;
import android.view.Window;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuPresenter;
import i.h.l.ViewPropertyAnimatorCompat;

public interface DecorToolbar {
    ViewPropertyAnimatorCompat a(int i2, long j2);

    void a(int i2);

    void a(Menu menu, MenuPresenter.a aVar);

    void a(MenuPresenter.a aVar, MenuBuilder.a aVar2);

    void a(ScrollingTabContainerView scrollingTabContainerView);

    void a(boolean z);

    boolean a();

    void b(int i2);

    void b(boolean z);

    boolean b();

    Context c();

    void c(int i2);

    void collapseActionView();

    boolean d();

    boolean e();

    void f();

    boolean g();

    CharSequence getTitle();

    void h();

    int i();

    Menu j();

    ViewGroup k();

    int l();

    void m();

    boolean n();

    void o();

    void setIcon(int i2);

    void setIcon(Drawable drawable);

    void setWindowCallback(Window.Callback callback);

    void setWindowTitle(CharSequence charSequence);
}
