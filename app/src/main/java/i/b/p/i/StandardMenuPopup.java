package i.b.p.i;

import android.content.Context;
import android.content.res.Resources;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import i.b.d;
import i.b.g;
import i.b.p.i.MenuPresenter;
import i.b.q.MenuPopupWindow;

public final class StandardMenuPopup extends MenuPopup implements PopupWindow.OnDismissListener, AdapterView.OnItemClickListener, MenuPresenter, View.OnKeyListener {
    public static final int w = g.abc_popup_menu_item_layout;
    public final Context c;
    public final MenuBuilder d;

    /* renamed from: e  reason: collision with root package name */
    public final MenuAdapter f917e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f918f;
    public final int g;
    public final int h;

    /* renamed from: i  reason: collision with root package name */
    public final int f919i;

    /* renamed from: j  reason: collision with root package name */
    public final MenuPopupWindow f920j;

    /* renamed from: k  reason: collision with root package name */
    public final ViewTreeObserver.OnGlobalLayoutListener f921k = new a();

    /* renamed from: l  reason: collision with root package name */
    public final View.OnAttachStateChangeListener f922l = new b();

    /* renamed from: m  reason: collision with root package name */
    public PopupWindow.OnDismissListener f923m;

    /* renamed from: n  reason: collision with root package name */
    public View f924n;

    /* renamed from: o  reason: collision with root package name */
    public View f925o;

    /* renamed from: p  reason: collision with root package name */
    public MenuPresenter.a f926p;

    /* renamed from: q  reason: collision with root package name */
    public ViewTreeObserver f927q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f928r;

    /* renamed from: s  reason: collision with root package name */
    public boolean f929s;

    /* renamed from: t  reason: collision with root package name */
    public int f930t;
    public int u = 0;
    public boolean v;

    public class a implements ViewTreeObserver.OnGlobalLayoutListener {
        public a() {
        }

        public void onGlobalLayout() {
            if (StandardMenuPopup.this.b()) {
                StandardMenuPopup standardMenuPopup = StandardMenuPopup.this;
                if (!standardMenuPopup.f920j.B) {
                    View view = standardMenuPopup.f925o;
                    if (view == null || !view.isShown()) {
                        StandardMenuPopup.this.dismiss();
                    } else {
                        StandardMenuPopup.this.f920j.a();
                    }
                }
            }
        }
    }

    public class b implements View.OnAttachStateChangeListener {
        public b() {
        }

        public void onViewAttachedToWindow(View view) {
        }

        public void onViewDetachedFromWindow(View view) {
            ViewTreeObserver viewTreeObserver = StandardMenuPopup.this.f927q;
            if (viewTreeObserver != null) {
                if (!viewTreeObserver.isAlive()) {
                    StandardMenuPopup.this.f927q = view.getViewTreeObserver();
                }
                StandardMenuPopup standardMenuPopup = StandardMenuPopup.this;
                standardMenuPopup.f927q.removeGlobalOnLayoutListener(standardMenuPopup.f921k);
            }
            view.removeOnAttachStateChangeListener(this);
        }
    }

    public StandardMenuPopup(Context context, MenuBuilder menuBuilder, View view, int i2, int i3, boolean z) {
        this.c = context;
        this.d = menuBuilder;
        this.f918f = z;
        this.f917e = new MenuAdapter(menuBuilder, LayoutInflater.from(context), this.f918f, w);
        this.h = i2;
        this.f919i = i3;
        Resources resources = context.getResources();
        this.g = Math.max(resources.getDisplayMetrics().widthPixels / 2, resources.getDimensionPixelSize(d.abc_config_prefDialogWidth));
        this.f924n = view;
        this.f920j = new MenuPopupWindow(this.c, null, this.h, this.f919i);
        menuBuilder.a(this, context);
    }

    public void a(int i2) {
        this.u = i2;
    }

    public void a(MenuBuilder menuBuilder) {
    }

    public void b(boolean z) {
        this.f917e.d = z;
    }

    public void c(int i2) {
        MenuPopupWindow menuPopupWindow = this.f920j;
        menuPopupWindow.h = i2;
        menuPopupWindow.f996j = true;
    }

    public boolean c() {
        return false;
    }

    public void dismiss() {
        if (b()) {
            this.f920j.dismiss();
        }
    }

    public ListView g() {
        return this.f920j.d;
    }

    public void onDismiss() {
        this.f928r = true;
        this.d.a(true);
        ViewTreeObserver viewTreeObserver = this.f927q;
        if (viewTreeObserver != null) {
            if (!viewTreeObserver.isAlive()) {
                this.f927q = this.f925o.getViewTreeObserver();
            }
            this.f927q.removeGlobalOnLayoutListener(this.f921k);
            this.f927q = null;
        }
        this.f925o.removeOnAttachStateChangeListener(this.f922l);
        PopupWindow.OnDismissListener onDismissListener = this.f923m;
        if (onDismissListener != null) {
            onDismissListener.onDismiss();
        }
    }

    public boolean onKey(View view, int i2, KeyEvent keyEvent) {
        if (keyEvent.getAction() != 1 || i2 != 82) {
            return false;
        }
        dismiss();
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, i.b.q.DropDownListView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x00c8 A[RETURN] */
    /* JADX WARNING: Removed duplicated region for block: B:34:0x00c9  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a() {
        /*
            r7 = this;
            boolean r0 = r7.b()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x000b
        L_0x0008:
            r1 = 1
            goto L_0x00c6
        L_0x000b:
            boolean r0 = r7.f928r
            if (r0 != 0) goto L_0x00c6
            android.view.View r0 = r7.f924n
            if (r0 != 0) goto L_0x0015
            goto L_0x00c6
        L_0x0015:
            r7.f925o = r0
            i.b.q.MenuPopupWindow r0 = r7.f920j
            android.widget.PopupWindow r0 = r0.C
            r0.setOnDismissListener(r7)
            i.b.q.MenuPopupWindow r0 = r7.f920j
            r0.f1006t = r7
            r0.a(r2)
            android.view.View r0 = r7.f925o
            android.view.ViewTreeObserver r3 = r7.f927q
            if (r3 != 0) goto L_0x002d
            r3 = 1
            goto L_0x002e
        L_0x002d:
            r3 = 0
        L_0x002e:
            android.view.ViewTreeObserver r4 = r0.getViewTreeObserver()
            r7.f927q = r4
            if (r3 == 0) goto L_0x003b
            android.view.ViewTreeObserver$OnGlobalLayoutListener r3 = r7.f921k
            r4.addOnGlobalLayoutListener(r3)
        L_0x003b:
            android.view.View$OnAttachStateChangeListener r3 = r7.f922l
            r0.addOnAttachStateChangeListener(r3)
            i.b.q.MenuPopupWindow r3 = r7.f920j
            r3.f1005s = r0
            int r0 = r7.u
            r3.f999m = r0
            boolean r0 = r7.f929s
            r3 = 0
            if (r0 != 0) goto L_0x005b
            i.b.p.i.MenuAdapter r0 = r7.f917e
            android.content.Context r4 = r7.c
            int r5 = r7.g
            int r0 = i.b.p.i.MenuPopup.a(r0, r3, r4, r5)
            r7.f930t = r0
            r7.f929s = r2
        L_0x005b:
            i.b.q.MenuPopupWindow r0 = r7.f920j
            int r4 = r7.f930t
            r0.d(r4)
            i.b.q.MenuPopupWindow r0 = r7.f920j
            r4 = 2
            android.widget.PopupWindow r0 = r0.C
            r0.setInputMethodMode(r4)
            i.b.q.MenuPopupWindow r0 = r7.f920j
            android.graphics.Rect r4 = r7.b
            if (r0 == 0) goto L_0x00c5
            if (r4 == 0) goto L_0x0078
            android.graphics.Rect r5 = new android.graphics.Rect
            r5.<init>(r4)
            goto L_0x0079
        L_0x0078:
            r5 = r3
        L_0x0079:
            r0.A = r5
            i.b.q.MenuPopupWindow r0 = r7.f920j
            r0.a()
            i.b.q.MenuPopupWindow r0 = r7.f920j
            i.b.q.DropDownListView r0 = r0.d
            r0.setOnKeyListener(r7)
            boolean r4 = r7.v
            if (r4 == 0) goto L_0x00b7
            i.b.p.i.MenuBuilder r4 = r7.d
            java.lang.CharSequence r4 = r4.f888m
            if (r4 == 0) goto L_0x00b7
            android.content.Context r4 = r7.c
            android.view.LayoutInflater r4 = android.view.LayoutInflater.from(r4)
            int r5 = i.b.g.abc_popup_menu_header_item_layout
            android.view.View r4 = r4.inflate(r5, r0, r1)
            android.widget.FrameLayout r4 = (android.widget.FrameLayout) r4
            r5 = 16908310(0x1020016, float:2.387729E-38)
            android.view.View r5 = r4.findViewById(r5)
            android.widget.TextView r5 = (android.widget.TextView) r5
            if (r5 == 0) goto L_0x00b1
            i.b.p.i.MenuBuilder r6 = r7.d
            java.lang.CharSequence r6 = r6.f888m
            r5.setText(r6)
        L_0x00b1:
            r4.setEnabled(r1)
            r0.addHeaderView(r4, r3, r1)
        L_0x00b7:
            i.b.q.MenuPopupWindow r0 = r7.f920j
            i.b.p.i.MenuAdapter r1 = r7.f917e
            r0.a(r1)
            i.b.q.MenuPopupWindow r0 = r7.f920j
            r0.a()
            goto L_0x0008
        L_0x00c5:
            throw r3
        L_0x00c6:
            if (r1 == 0) goto L_0x00c9
            return
        L_0x00c9:
            java.lang.IllegalStateException r0 = new java.lang.IllegalStateException
            java.lang.String r1 = "StandardMenuPopup cannot be used without an anchor"
            r0.<init>(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.p.i.StandardMenuPopup.a():void");
    }

    public boolean b() {
        return !this.f928r && this.f920j.b();
    }

    public void b(int i2) {
        this.f920j.g = i2;
    }

    public void c(boolean z) {
        this.v = z;
    }

    public void a(boolean z) {
        this.f929s = false;
        MenuAdapter menuAdapter = this.f917e;
        if (menuAdapter != null) {
            menuAdapter.notifyDataSetChanged();
        }
    }

    public void a(MenuPresenter.a aVar) {
        this.f926p = aVar;
    }

    /* JADX WARNING: Removed duplicated region for block: B:20:0x006e  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean a(i.b.p.i.SubMenuBuilder r10) {
        /*
            r9 = this;
            boolean r0 = r10.hasVisibleItems()
            r1 = 0
            if (r0 == 0) goto L_0x0076
            i.b.p.i.MenuPopupHelper r0 = new i.b.p.i.MenuPopupHelper
            android.content.Context r3 = r9.c
            android.view.View r5 = r9.f925o
            boolean r6 = r9.f918f
            int r7 = r9.h
            int r8 = r9.f919i
            r2 = r0
            r4 = r10
            r2.<init>(r3, r4, r5, r6, r7, r8)
            i.b.p.i.MenuPresenter$a r2 = r9.f926p
            r0.a(r2)
            boolean r2 = i.b.p.i.MenuPopup.b(r10)
            r0.h = r2
            i.b.p.i.MenuPopup r3 = r0.f914j
            if (r3 == 0) goto L_0x002a
            r3.b(r2)
        L_0x002a:
            android.widget.PopupWindow$OnDismissListener r2 = r9.f923m
            r0.f915k = r2
            r2 = 0
            r9.f923m = r2
            i.b.p.i.MenuBuilder r2 = r9.d
            r2.a(r1)
            i.b.q.MenuPopupWindow r2 = r9.f920j
            int r3 = r2.g
            boolean r4 = r2.f996j
            if (r4 != 0) goto L_0x0040
            r2 = 0
            goto L_0x0042
        L_0x0040:
            int r2 = r2.h
        L_0x0042:
            int r4 = r9.u
            android.view.View r5 = r9.f924n
            int r5 = i.h.l.ViewCompat.k(r5)
            int r4 = android.view.Gravity.getAbsoluteGravity(r4, r5)
            r4 = r4 & 7
            r5 = 5
            if (r4 != r5) goto L_0x005a
            android.view.View r4 = r9.f924n
            int r4 = r4.getWidth()
            int r3 = r3 + r4
        L_0x005a:
            boolean r4 = r0.b()
            r5 = 1
            if (r4 == 0) goto L_0x0062
            goto L_0x006b
        L_0x0062:
            android.view.View r4 = r0.f912f
            if (r4 != 0) goto L_0x0068
            r0 = 0
            goto L_0x006c
        L_0x0068:
            r0.a(r3, r2, r5, r5)
        L_0x006b:
            r0 = 1
        L_0x006c:
            if (r0 == 0) goto L_0x0076
            i.b.p.i.MenuPresenter$a r0 = r9.f926p
            if (r0 == 0) goto L_0x0075
            r0.a(r10)
        L_0x0075:
            return r5
        L_0x0076:
            return r1
        */
        throw new UnsupportedOperationException("Method not decompiled: i.b.p.i.StandardMenuPopup.a(i.b.p.i.SubMenuBuilder):boolean");
    }

    public void a(MenuBuilder menuBuilder, boolean z) {
        if (menuBuilder == this.d) {
            dismiss();
            MenuPresenter.a aVar = this.f926p;
            if (aVar != null) {
                aVar.a(menuBuilder, z);
            }
        }
    }

    public void a(View view) {
        this.f924n = view;
    }

    public void a(PopupWindow.OnDismissListener onDismissListener) {
        this.f923m = onDismissListener;
    }
}
