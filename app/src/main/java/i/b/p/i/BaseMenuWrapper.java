package i.b.p.i;

import android.content.Context;
import android.view.MenuItem;
import android.view.SubMenu;
import i.e.ArrayMap;
import i.h.g.a.SupportMenuItem;
import i.h.g.a.SupportSubMenu;
import i.h.g.a.b;
import i.h.g.a.c;
import java.util.Map;

public abstract class BaseMenuWrapper {
    public final Context a;
    public Map<b, MenuItem> b;
    public Map<c, SubMenu> c;

    public BaseMenuWrapper(Context context) {
        this.a = context;
    }

    public final MenuItem a(MenuItem menuItem) {
        if (!(menuItem instanceof SupportMenuItem)) {
            return menuItem;
        }
        SupportMenuItem supportMenuItem = (SupportMenuItem) menuItem;
        if (this.b == null) {
            this.b = new ArrayMap();
        }
        MenuItem menuItem2 = this.b.get(menuItem);
        if (menuItem2 != null) {
            return menuItem2;
        }
        MenuItemWrapperICS menuItemWrapperICS = new MenuItemWrapperICS(this.a, supportMenuItem);
        this.b.put(supportMenuItem, menuItemWrapperICS);
        return menuItemWrapperICS;
    }

    public final SubMenu a(SubMenu subMenu) {
        if (!(subMenu instanceof SupportSubMenu)) {
            return subMenu;
        }
        SupportSubMenu supportSubMenu = (SupportSubMenu) subMenu;
        if (this.c == null) {
            this.c = new ArrayMap();
        }
        SubMenu subMenu2 = this.c.get(supportSubMenu);
        if (subMenu2 != null) {
            return subMenu2;
        }
        SubMenuWrapperICS subMenuWrapperICS = new SubMenuWrapperICS(this.a, supportSubMenu);
        this.c.put(supportSubMenu, subMenuWrapperICS);
        return subMenuWrapperICS;
    }
}
