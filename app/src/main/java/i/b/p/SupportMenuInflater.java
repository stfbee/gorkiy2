package i.b.p;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.content.res.XmlResourceParser;
import android.graphics.PorterDuff;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.util.Xml;
import android.view.InflateException;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import com.crashlytics.android.core.CodedOutputStream;
import i.b.j;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuItemWrapperICS;
import i.b.q.DrawableUtils;
import i.b.q.TintTypedArray;
import i.h.g.a.SupportMenu;
import i.h.g.a.SupportMenuItem;
import i.h.l.ActionProvider;
import j.a.a.a.outline;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Method;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

public class SupportMenuInflater extends MenuInflater {

    /* renamed from: e  reason: collision with root package name */
    public static final Class<?>[] f827e;

    /* renamed from: f  reason: collision with root package name */
    public static final Class<?>[] f828f;
    public final Object[] a;
    public final Object[] b;
    public Context c;
    public Object d;

    public static class a implements MenuItem.OnMenuItemClickListener {
        public static final Class<?>[] c = {MenuItem.class};
        public Object a;
        public Method b;

        public a(Object obj, String str) {
            this.a = obj;
            Class<?> cls = obj.getClass();
            try {
                this.b = cls.getMethod(str, c);
            } catch (Exception e2) {
                InflateException inflateException = new InflateException("Couldn't resolve menu item onClick handler " + str + " in class " + cls.getName());
                inflateException.initCause(e2);
                throw inflateException;
            }
        }

        public boolean onMenuItemClick(MenuItem menuItem) {
            try {
                if (this.b.getReturnType() == Boolean.TYPE) {
                    return ((Boolean) this.b.invoke(this.a, menuItem)).booleanValue();
                }
                this.b.invoke(this.a, menuItem);
                return true;
            } catch (Exception e2) {
                throw new RuntimeException(e2);
            }
        }
    }

    static {
        Class<?>[] clsArr = {Context.class};
        f827e = clsArr;
        f828f = clsArr;
    }

    public SupportMenuInflater(Context context) {
        super(context);
        this.c = context;
        Object[] objArr = {context};
        this.a = objArr;
        this.b = objArr;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.TintTypedArray.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      i.b.q.TintTypedArray.a(int, float):float
      i.b.q.TintTypedArray.a(int, int):int
      i.b.q.TintTypedArray.a(int, boolean):boolean */
    public final void a(XmlPullParser xmlPullParser, AttributeSet attributeSet, Menu menu) {
        char c2;
        char c3;
        AttributeSet attributeSet2 = attributeSet;
        b bVar = new b(menu);
        int eventType = xmlPullParser.getEventType();
        while (true) {
            if (eventType == 2) {
                String name = xmlPullParser.getName();
                if (name.equals("menu")) {
                    eventType = xmlPullParser.next();
                } else {
                    throw new RuntimeException(outline.a("Expecting menu, got ", name));
                }
            } else {
                eventType = xmlPullParser.next();
                if (eventType == 1) {
                    break;
                }
            }
        }
        boolean z = false;
        boolean z2 = false;
        String str = null;
        while (!z) {
            if (eventType != 1) {
                if (eventType != 2) {
                    if (eventType == 3) {
                        String name2 = xmlPullParser.getName();
                        if (z2 && name2.equals(str)) {
                            str = null;
                            z2 = false;
                            eventType = xmlPullParser.next();
                        } else if (name2.equals("group")) {
                            bVar.b = 0;
                            bVar.c = 0;
                            bVar.d = 0;
                            bVar.f829e = 0;
                            bVar.f830f = true;
                            bVar.g = true;
                        } else if (name2.equals("item")) {
                            if (!bVar.h) {
                                ActionProvider actionProvider = bVar.A;
                                if (actionProvider == null || !((MenuItemWrapperICS.a) actionProvider).b.hasSubMenu()) {
                                    bVar.h = true;
                                    bVar.a(bVar.a.add(bVar.b, bVar.f831i, bVar.f832j, bVar.f833k));
                                } else {
                                    bVar.a();
                                }
                            }
                        } else if (name2.equals("menu")) {
                            z = true;
                            eventType = xmlPullParser.next();
                        }
                    }
                } else if (!z2) {
                    String name3 = xmlPullParser.getName();
                    if (name3.equals("group")) {
                        TypedArray obtainStyledAttributes = SupportMenuInflater.this.c.obtainStyledAttributes(attributeSet2, j.MenuGroup);
                        bVar.b = obtainStyledAttributes.getResourceId(j.MenuGroup_android_id, 0);
                        bVar.c = obtainStyledAttributes.getInt(j.MenuGroup_android_menuCategory, 0);
                        bVar.d = obtainStyledAttributes.getInt(j.MenuGroup_android_orderInCategory, 0);
                        bVar.f829e = obtainStyledAttributes.getInt(j.MenuGroup_android_checkableBehavior, 0);
                        bVar.f830f = obtainStyledAttributes.getBoolean(j.MenuGroup_android_visible, true);
                        bVar.g = obtainStyledAttributes.getBoolean(j.MenuGroup_android_enabled, true);
                        obtainStyledAttributes.recycle();
                    } else if (name3.equals("item")) {
                        TintTypedArray a2 = TintTypedArray.a(SupportMenuInflater.this.c, attributeSet2, j.MenuItem);
                        bVar.f831i = a2.f(j.MenuItem_android_id, 0);
                        bVar.f832j = (a2.d(j.MenuItem_android_menuCategory, bVar.c) & -65536) | (a2.d(j.MenuItem_android_orderInCategory, bVar.d) & 65535);
                        bVar.f833k = a2.e(j.MenuItem_android_title);
                        bVar.f834l = a2.e(j.MenuItem_android_titleCondensed);
                        bVar.f835m = a2.f(j.MenuItem_android_icon, 0);
                        String d2 = a2.d(j.MenuItem_android_alphabeticShortcut);
                        if (d2 == null) {
                            c2 = 0;
                        } else {
                            c2 = d2.charAt(0);
                        }
                        bVar.f836n = c2;
                        bVar.f837o = a2.d(j.MenuItem_alphabeticModifiers, CodedOutputStream.DEFAULT_BUFFER_SIZE);
                        String d3 = a2.d(j.MenuItem_android_numericShortcut);
                        if (d3 == null) {
                            c3 = 0;
                        } else {
                            c3 = d3.charAt(0);
                        }
                        bVar.f838p = c3;
                        bVar.f839q = a2.d(j.MenuItem_numericModifiers, CodedOutputStream.DEFAULT_BUFFER_SIZE);
                        if (a2.f(j.MenuItem_android_checkable)) {
                            bVar.f840r = a2.a(j.MenuItem_android_checkable, false) ? 1 : 0;
                        } else {
                            bVar.f840r = bVar.f829e;
                        }
                        bVar.f841s = a2.a(j.MenuItem_android_checked, false);
                        bVar.f842t = a2.a(j.MenuItem_android_visible, bVar.f830f);
                        bVar.u = a2.a(j.MenuItem_android_enabled, bVar.g);
                        bVar.v = a2.d(j.MenuItem_showAsAction, -1);
                        bVar.z = a2.d(j.MenuItem_android_onClick);
                        bVar.w = a2.f(j.MenuItem_actionLayout, 0);
                        bVar.x = a2.d(j.MenuItem_actionViewClass);
                        String d4 = a2.d(j.MenuItem_actionProviderClass);
                        bVar.y = d4;
                        boolean z3 = d4 != null;
                        if (z3 && bVar.w == 0 && bVar.x == null) {
                            bVar.A = (ActionProvider) bVar.a(bVar.y, f828f, SupportMenuInflater.this.b);
                        } else {
                            if (z3) {
                                Log.w("SupportMenuInflater", "Ignoring attribute 'actionProviderClass'. Action view already specified.");
                            }
                            bVar.A = null;
                        }
                        bVar.B = a2.e(j.MenuItem_contentDescription);
                        bVar.C = a2.e(j.MenuItem_tooltipText);
                        if (a2.f(j.MenuItem_iconTintMode)) {
                            bVar.E = DrawableUtils.a(a2.d(j.MenuItem_iconTintMode, -1), bVar.E);
                        } else {
                            bVar.E = null;
                        }
                        if (a2.f(j.MenuItem_iconTint)) {
                            bVar.D = a2.a(j.MenuItem_iconTint);
                        } else {
                            bVar.D = null;
                        }
                        a2.b.recycle();
                        bVar.h = false;
                    } else {
                        if (name3.equals("menu")) {
                            a(xmlPullParser, attributeSet2, bVar.a());
                        } else {
                            z2 = true;
                            str = name3;
                        }
                        eventType = xmlPullParser.next();
                    }
                }
                eventType = xmlPullParser.next();
            } else {
                throw new RuntimeException("Unexpected end of document");
            }
        }
    }

    public void inflate(int i2, Menu menu) {
        if (!(menu instanceof SupportMenu)) {
            super.inflate(i2, menu);
            return;
        }
        XmlResourceParser xmlResourceParser = null;
        try {
            xmlResourceParser = this.c.getResources().getLayout(i2);
            a(xmlResourceParser, Xml.asAttributeSet(xmlResourceParser), menu);
            xmlResourceParser.close();
        } catch (XmlPullParserException e2) {
            throw new InflateException("Error inflating menu XML", e2);
        } catch (IOException e3) {
            throw new InflateException("Error inflating menu XML", e3);
        } catch (Throwable th) {
            if (xmlResourceParser != null) {
                xmlResourceParser.close();
            }
            throw th;
        }
    }

    public class b {
        public ActionProvider A;
        public CharSequence B;
        public CharSequence C;
        public ColorStateList D = null;
        public PorterDuff.Mode E = null;
        public Menu a;
        public int b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f829e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f830f;
        public boolean g;
        public boolean h;

        /* renamed from: i  reason: collision with root package name */
        public int f831i;

        /* renamed from: j  reason: collision with root package name */
        public int f832j;

        /* renamed from: k  reason: collision with root package name */
        public CharSequence f833k;

        /* renamed from: l  reason: collision with root package name */
        public CharSequence f834l;

        /* renamed from: m  reason: collision with root package name */
        public int f835m;

        /* renamed from: n  reason: collision with root package name */
        public char f836n;

        /* renamed from: o  reason: collision with root package name */
        public int f837o;

        /* renamed from: p  reason: collision with root package name */
        public char f838p;

        /* renamed from: q  reason: collision with root package name */
        public int f839q;

        /* renamed from: r  reason: collision with root package name */
        public int f840r;

        /* renamed from: s  reason: collision with root package name */
        public boolean f841s;

        /* renamed from: t  reason: collision with root package name */
        public boolean f842t;
        public boolean u;
        public int v;
        public int w;
        public String x;
        public String y;
        public String z;

        public b(Menu menu) {
            this.a = menu;
            this.b = 0;
            this.c = 0;
            this.d = 0;
            this.f829e = 0;
            this.f830f = true;
            this.g = true;
        }

        public final void a(MenuItem menuItem) {
            boolean z2 = false;
            menuItem.setChecked(this.f841s).setVisible(this.f842t).setEnabled(this.u).setCheckable(this.f840r >= 1).setTitleCondensed(this.f834l).setIcon(this.f835m);
            int i2 = this.v;
            if (i2 >= 0) {
                menuItem.setShowAsAction(i2);
            }
            if (this.z != null) {
                if (!SupportMenuInflater.this.c.isRestricted()) {
                    SupportMenuInflater supportMenuInflater = SupportMenuInflater.this;
                    if (supportMenuInflater.d == null) {
                        supportMenuInflater.d = supportMenuInflater.a(supportMenuInflater.c);
                    }
                    menuItem.setOnMenuItemClickListener(new a(supportMenuInflater.d, this.z));
                } else {
                    throw new IllegalStateException("The android:onClick attribute cannot be used within a restricted context");
                }
            }
            boolean z3 = menuItem instanceof MenuItemImpl;
            if (z3) {
                MenuItemImpl menuItemImpl = (MenuItemImpl) menuItem;
            }
            if (this.f840r >= 2) {
                if (z3) {
                    MenuItemImpl menuItemImpl2 = (MenuItemImpl) menuItem;
                    menuItemImpl2.x = (menuItemImpl2.x & -5) | 4;
                } else if (menuItem instanceof MenuItemWrapperICS) {
                    MenuItemWrapperICS menuItemWrapperICS = (MenuItemWrapperICS) menuItem;
                    try {
                        if (menuItemWrapperICS.f910e == null) {
                            menuItemWrapperICS.f910e = menuItemWrapperICS.d.getClass().getDeclaredMethod("setExclusiveCheckable", Boolean.TYPE);
                        }
                        menuItemWrapperICS.f910e.invoke(menuItemWrapperICS.d, true);
                    } catch (Exception e2) {
                        Log.w("MenuItemWrapper", "Error while calling setExclusiveCheckable", e2);
                    }
                }
            }
            String str = this.x;
            if (str != null) {
                menuItem.setActionView((View) a(str, SupportMenuInflater.f827e, SupportMenuInflater.this.a));
                z2 = true;
            }
            int i3 = this.w;
            if (i3 > 0) {
                if (!z2) {
                    menuItem.setActionView(i3);
                } else {
                    Log.w("SupportMenuInflater", "Ignoring attribute 'itemActionViewLayout'. Action view already specified.");
                }
            }
            ActionProvider actionProvider = this.A;
            if (actionProvider != null) {
                if (menuItem instanceof SupportMenuItem) {
                    ((SupportMenuItem) menuItem).a(actionProvider);
                } else {
                    Log.w("MenuItemCompat", "setActionProvider: item does not implement SupportMenuItem; ignoring");
                }
            }
            CharSequence charSequence = this.B;
            boolean z4 = menuItem instanceof SupportMenuItem;
            if (z4) {
                ((SupportMenuItem) menuItem).setContentDescription(charSequence);
            } else if (Build.VERSION.SDK_INT >= 26) {
                menuItem.setContentDescription(charSequence);
            }
            CharSequence charSequence2 = this.C;
            if (z4) {
                ((SupportMenuItem) menuItem).setTooltipText(charSequence2);
            } else if (Build.VERSION.SDK_INT >= 26) {
                menuItem.setTooltipText(charSequence2);
            }
            char c2 = this.f836n;
            int i4 = this.f837o;
            if (z4) {
                ((SupportMenuItem) menuItem).setAlphabeticShortcut(c2, i4);
            } else if (Build.VERSION.SDK_INT >= 26) {
                menuItem.setAlphabeticShortcut(c2, i4);
            }
            char c3 = this.f838p;
            int i5 = this.f839q;
            if (z4) {
                ((SupportMenuItem) menuItem).setNumericShortcut(c3, i5);
            } else if (Build.VERSION.SDK_INT >= 26) {
                menuItem.setNumericShortcut(c3, i5);
            }
            PorterDuff.Mode mode = this.E;
            if (mode != null) {
                if (z4) {
                    ((SupportMenuItem) menuItem).setIconTintMode(mode);
                } else if (Build.VERSION.SDK_INT >= 26) {
                    menuItem.setIconTintMode(mode);
                }
            }
            ColorStateList colorStateList = this.D;
            if (colorStateList == null) {
                return;
            }
            if (z4) {
                ((SupportMenuItem) menuItem).setIconTintList(colorStateList);
            } else if (Build.VERSION.SDK_INT >= 26) {
                menuItem.setIconTintList(colorStateList);
            }
        }

        public SubMenu a() {
            this.h = true;
            SubMenu addSubMenu = this.a.addSubMenu(this.b, this.f831i, this.f832j, this.f833k);
            a(addSubMenu.getItem());
            return addSubMenu;
        }

        public final <T> T a(String str, Class<?>[] clsArr, Object[] objArr) {
            try {
                Constructor<?> constructor = Class.forName(str, false, SupportMenuInflater.this.c.getClassLoader()).getConstructor(clsArr);
                constructor.setAccessible(true);
                return constructor.newInstance(objArr);
            } catch (Exception e2) {
                Log.w("SupportMenuInflater", "Cannot instantiate class: " + str, e2);
                return null;
            }
        }
    }

    public final Object a(Object obj) {
        return (!(obj instanceof Activity) && (obj instanceof ContextWrapper)) ? a(((ContextWrapper) obj).getBaseContext()) : obj;
    }
}
