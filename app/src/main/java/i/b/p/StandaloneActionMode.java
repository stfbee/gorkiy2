package i.b.p;

import android.content.Context;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import androidx.appcompat.widget.ActionBarContextView;
import i.b.p.ActionMode;
import i.b.p.i.MenuBuilder;
import i.b.q.ActionMenuPresenter;
import java.lang.ref.WeakReference;

public class StandaloneActionMode extends ActionMode implements MenuBuilder.a {
    public Context d;

    /* renamed from: e  reason: collision with root package name */
    public ActionBarContextView f824e;

    /* renamed from: f  reason: collision with root package name */
    public ActionMode.a f825f;
    public WeakReference<View> g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public MenuBuilder f826i;

    public StandaloneActionMode(Context context, ActionBarContextView actionBarContextView, ActionMode.a aVar, boolean z) {
        this.d = context;
        this.f824e = actionBarContextView;
        this.f825f = aVar;
        MenuBuilder menuBuilder = new MenuBuilder(actionBarContextView.getContext());
        menuBuilder.f887l = 1;
        this.f826i = menuBuilder;
        menuBuilder.f882e = this;
    }

    public void a(CharSequence charSequence) {
        this.f824e.setSubtitle(charSequence);
    }

    public void b(CharSequence charSequence) {
        this.f824e.setTitle(charSequence);
    }

    public Menu c() {
        return this.f826i;
    }

    public MenuInflater d() {
        return new SupportMenuInflater(this.f824e.getContext());
    }

    public CharSequence e() {
        return this.f824e.getSubtitle();
    }

    public CharSequence f() {
        return this.f824e.getTitle();
    }

    public void g() {
        this.f825f.a(super, this.f826i);
    }

    public boolean h() {
        return this.f824e.f70s;
    }

    public void a(int i2) {
        this.f824e.setSubtitle(this.d.getString(i2));
    }

    public void b(int i2) {
        this.f824e.setTitle(this.d.getString(i2));
    }

    public void a(View view) {
        this.f824e.setCustomView(view);
        this.g = view != null ? new WeakReference<>(view) : null;
    }

    public View b() {
        WeakReference<View> weakReference = this.g;
        if (weakReference != null) {
            return weakReference.get();
        }
        return null;
    }

    public void a() {
        if (!this.h) {
            this.h = true;
            this.f824e.sendAccessibilityEvent(32);
            this.f825f.a(super);
        }
    }

    public void a(boolean z) {
        super.c = z;
        this.f824e.setTitleOptional(z);
    }

    public boolean a(MenuBuilder menuBuilder, MenuItem menuItem) {
        return this.f825f.a(super, menuItem);
    }

    public void a(MenuBuilder menuBuilder) {
        g();
        ActionMenuPresenter actionMenuPresenter = this.f824e.f932e;
        if (actionMenuPresenter != null) {
            actionMenuPresenter.f();
        }
    }
}
