package i.l.a;

import androidx.fragment.app.Fragment;
import java.io.FileDescriptor;
import java.io.PrintWriter;
import java.util.List;

public abstract class FragmentManager {
    public static final FragmentFactory c = new FragmentFactory();
    public FragmentFactory b = null;

    public interface a {
        String a();
    }

    public abstract Fragment a(int i2);

    public abstract Fragment a(String str);

    public abstract List<Fragment> a();

    public abstract void a(String str, FileDescriptor fileDescriptor, PrintWriter printWriter, String[] strArr);

    public abstract boolean b();
}
