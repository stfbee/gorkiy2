package i.l.a;

import androidx.fragment.app.Fragment;
import i.l.a.r;
import i.o.Lifecycle;
import j.a.a.a.outline;
import java.lang.reflect.Modifier;
import java.util.ArrayList;

public abstract class FragmentTransaction {
    public ArrayList<r.a> a = new ArrayList<>();
    public int b;
    public int c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f1324e;

    /* renamed from: f  reason: collision with root package name */
    public int f1325f;
    public int g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public boolean f1326i = true;

    /* renamed from: j  reason: collision with root package name */
    public String f1327j;

    /* renamed from: k  reason: collision with root package name */
    public int f1328k;

    /* renamed from: l  reason: collision with root package name */
    public CharSequence f1329l;

    /* renamed from: m  reason: collision with root package name */
    public int f1330m;

    /* renamed from: n  reason: collision with root package name */
    public CharSequence f1331n;

    /* renamed from: o  reason: collision with root package name */
    public ArrayList<String> f1332o;

    /* renamed from: p  reason: collision with root package name */
    public ArrayList<String> f1333p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1334q = false;

    /* renamed from: r  reason: collision with root package name */
    public ArrayList<Runnable> f1335r;

    public static final class a {
        public int a;
        public Fragment b;
        public int c;
        public int d;

        /* renamed from: e  reason: collision with root package name */
        public int f1336e;

        /* renamed from: f  reason: collision with root package name */
        public int f1337f;
        public Lifecycle.b g;
        public Lifecycle.b h;

        public a() {
        }

        public a(int i2, Fragment fragment) {
            this.a = i2;
            this.b = fragment;
            Lifecycle.b bVar = Lifecycle.b.RESUMED;
            this.g = bVar;
            this.h = bVar;
        }
    }

    public void a(a aVar) {
        this.a.add(aVar);
        aVar.c = this.b;
        aVar.d = this.c;
        aVar.f1336e = this.d;
        aVar.f1337f = this.f1324e;
    }

    public abstract int b();

    public void a(int i2, Fragment fragment, String str, int i3) {
        Class<?> cls = fragment.getClass();
        int modifiers = cls.getModifiers();
        if (cls.isAnonymousClass() || !Modifier.isPublic(modifiers) || (cls.isMemberClass() && !Modifier.isStatic(modifiers))) {
            StringBuilder a2 = outline.a("Fragment ");
            a2.append(cls.getCanonicalName());
            a2.append(" must be a public static class to be  properly recreated from instance state.");
            throw new IllegalStateException(a2.toString());
        }
        if (str != null) {
            String str2 = fragment.y;
            if (str2 == null || str.equals(str2)) {
                fragment.y = str;
            } else {
                throw new IllegalStateException("Can't change tag of fragment " + fragment + ": was " + fragment.y + " now " + str);
            }
        }
        if (i2 != 0) {
            if (i2 != -1) {
                int i4 = fragment.w;
                if (i4 == 0 || i4 == i2) {
                    fragment.w = i2;
                    fragment.x = i2;
                } else {
                    throw new IllegalStateException("Can't change container ID of fragment " + fragment + ": was " + fragment.w + " now " + i2);
                }
            } else {
                throw new IllegalArgumentException("Can't add fragment " + fragment + " with tag " + str + " to container view with no id");
            }
        }
        a(new a(i3, fragment));
    }

    public FragmentTransaction a(int i2, Fragment fragment) {
        if (i2 != 0) {
            a(i2, fragment, null, 2);
            return this;
        }
        throw new IllegalArgumentException("Must use non-zero containerViewId");
    }

    public FragmentTransaction a(String str) {
        if (this.f1326i) {
            this.h = true;
            this.f1327j = str;
            return this;
        }
        throw new IllegalStateException("This FragmentTransaction is not allowed to be added to the back stack.");
    }
}
