package i.l.a;

import android.util.Log;
import androidx.fragment.app.Fragment;
import i.o.ViewModel;
import i.o.ViewModelProvider;
import i.o.v;
import i.o.x;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;

public class FragmentManagerViewModel extends ViewModel {
    public static final ViewModelProvider.b h = new a();
    public final HashSet<Fragment> b = new HashSet<>();
    public final HashMap<String, p> c = new HashMap<>();
    public final HashMap<String, x> d = new HashMap<>();

    /* renamed from: e  reason: collision with root package name */
    public final boolean f1313e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1314f = false;
    public boolean g = false;

    public static class a implements ViewModelProvider.b {
        public <T extends v> T a(Class<T> cls) {
            return new FragmentManagerViewModel(true);
        }
    }

    public FragmentManagerViewModel(boolean z) {
        this.f1313e = z;
    }

    public boolean a(Fragment fragment) {
        if (!this.b.contains(fragment)) {
            return true;
        }
        if (this.f1313e) {
            return this.f1314f;
        }
        return !this.g;
    }

    public void b() {
        if (FragmentManagerImpl.H) {
            Log.d("FragmentManager", "onCleared called for " + this);
        }
        this.f1314f = true;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null || FragmentManagerViewModel.class != obj.getClass()) {
            return false;
        }
        FragmentManagerViewModel fragmentManagerViewModel = (FragmentManagerViewModel) obj;
        if (!this.b.equals(fragmentManagerViewModel.b) || !this.c.equals(fragmentManagerViewModel.c) || !this.d.equals(fragmentManagerViewModel.d)) {
            return false;
        }
        return true;
    }

    public int hashCode() {
        int hashCode = this.c.hashCode();
        return this.d.hashCode() + ((hashCode + (this.b.hashCode() * 31)) * 31);
    }

    public String toString() {
        StringBuilder sb = new StringBuilder("FragmentManagerViewModel{");
        sb.append(Integer.toHexString(System.identityHashCode(this)));
        sb.append("} Fragments (");
        Iterator<Fragment> it = this.b.iterator();
        while (it.hasNext()) {
            sb.append(it.next());
            if (it.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") Child Non Config (");
        Iterator<String> it2 = this.c.keySet().iterator();
        while (it2.hasNext()) {
            sb.append(it2.next());
            if (it2.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(") ViewModelStores (");
        Iterator<String> it3 = this.d.keySet().iterator();
        while (it3.hasNext()) {
            sb.append(it3.next());
            if (it3.hasNext()) {
                sb.append(", ");
            }
        }
        sb.append(')');
        return sb.toString();
    }
}
