package i.f.a.h;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class ConstraintWidgetGroup {
    public List<d> a;
    public int b = -1;
    public int c = -1;
    public boolean d = false;

    /* renamed from: e  reason: collision with root package name */
    public final int[] f1123e = {-1, -1};

    /* renamed from: f  reason: collision with root package name */
    public List<d> f1124f = new ArrayList();
    public List<d> g = new ArrayList();
    public HashSet<d> h = new HashSet<>();

    /* renamed from: i  reason: collision with root package name */
    public HashSet<d> f1125i = new HashSet<>();

    /* renamed from: j  reason: collision with root package name */
    public List<d> f1126j = new ArrayList();

    /* renamed from: k  reason: collision with root package name */
    public List<d> f1127k = new ArrayList();

    public ConstraintWidgetGroup(List<d> list) {
        this.a = list;
    }

    public void a(ConstraintWidget constraintWidget, int i2) {
        if (i2 == 0) {
            this.h.add(constraintWidget);
        } else if (i2 == 1) {
            this.f1125i.add(constraintWidget);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f.a.h.ConstraintWidgetGroup.a(java.util.ArrayList<i.f.a.h.d>, i.f.a.h.d):void
     arg types: [java.util.ArrayList<i.f.a.h.d>, i.f.a.h.ConstraintWidget]
     candidates:
      i.f.a.h.ConstraintWidgetGroup.a(i.f.a.h.ConstraintWidget, int):void
      i.f.a.h.ConstraintWidgetGroup.a(java.util.ArrayList<i.f.a.h.d>, i.f.a.h.d):void */
    public final void a(ArrayList<d> arrayList, d dVar) {
        ConstraintWidget constraintWidget;
        if (!dVar.d0) {
            arrayList.add(dVar);
            dVar.d0 = true;
            if (!dVar.j()) {
                if (dVar instanceof Helper) {
                    Helper helper = (Helper) dVar;
                    int i2 = helper.l0;
                    for (int i3 = 0; i3 < i2; i3++) {
                        a(arrayList, (d) helper.k0[i3]);
                    }
                }
                for (ConstraintAnchor constraintAnchor : dVar.A) {
                    ConstraintAnchor constraintAnchor2 = constraintAnchor.d;
                    if (!(constraintAnchor2 == null || (constraintWidget = constraintAnchor2.b) == dVar.D)) {
                        a(arrayList, (d) constraintWidget);
                    }
                }
            }
        }
    }

    public ConstraintWidgetGroup(List<d> list, boolean z) {
        this.a = list;
        this.d = z;
    }

    /* JADX WARNING: Removed duplicated region for block: B:24:0x0048  */
    /* JADX WARNING: Removed duplicated region for block: B:25:0x0050  */
    /* JADX WARNING: Removed duplicated region for block: B:28:0x006b  */
    /* JADX WARNING: Removed duplicated region for block: B:33:0x0087  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public final void a(i.f.a.h.ConstraintWidget r7) {
        /*
            r6 = this;
            boolean r0 = r7.b0
            if (r0 == 0) goto L_0x00dd
            boolean r0 = r7.j()
            if (r0 == 0) goto L_0x000b
            return
        L_0x000b:
            i.f.a.h.ConstraintAnchor r0 = r7.u
            i.f.a.h.ConstraintAnchor r0 = r0.d
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0015
            r0 = 1
            goto L_0x0016
        L_0x0015:
            r0 = 0
        L_0x0016:
            if (r0 == 0) goto L_0x001d
            i.f.a.h.ConstraintAnchor r3 = r7.u
            i.f.a.h.ConstraintAnchor r3 = r3.d
            goto L_0x0021
        L_0x001d:
            i.f.a.h.ConstraintAnchor r3 = r7.f1121s
            i.f.a.h.ConstraintAnchor r3 = r3.d
        L_0x0021:
            if (r3 == 0) goto L_0x0045
            i.f.a.h.ConstraintWidget r4 = r3.b
            boolean r5 = r4.c0
            if (r5 != 0) goto L_0x002c
            r6.a(r4)
        L_0x002c:
            i.f.a.h.ConstraintAnchor$c r4 = r3.c
            i.f.a.h.ConstraintAnchor$c r5 = i.f.a.h.ConstraintAnchor.c.RIGHT
            if (r4 != r5) goto L_0x003c
            i.f.a.h.ConstraintWidget r3 = r3.b
            int r4 = r3.I
            int r3 = r3.i()
            int r3 = r3 + r4
            goto L_0x0046
        L_0x003c:
            i.f.a.h.ConstraintAnchor$c r5 = i.f.a.h.ConstraintAnchor.c.LEFT
            if (r4 != r5) goto L_0x0045
            i.f.a.h.ConstraintWidget r3 = r3.b
            int r3 = r3.I
            goto L_0x0046
        L_0x0045:
            r3 = 0
        L_0x0046:
            if (r0 == 0) goto L_0x0050
            i.f.a.h.ConstraintAnchor r0 = r7.u
            int r0 = r0.a()
            int r3 = r3 - r0
            goto L_0x005c
        L_0x0050:
            i.f.a.h.ConstraintAnchor r0 = r7.f1121s
            int r0 = r0.a()
            int r4 = r7.i()
            int r4 = r4 + r0
            int r3 = r3 + r4
        L_0x005c:
            int r0 = r7.i()
            int r0 = r3 - r0
            r7.a(r0, r3)
            i.f.a.h.ConstraintAnchor r0 = r7.w
            i.f.a.h.ConstraintAnchor r0 = r0.d
            if (r0 == 0) goto L_0x0087
            i.f.a.h.ConstraintWidget r1 = r0.b
            boolean r3 = r1.c0
            if (r3 != 0) goto L_0x0074
            r6.a(r1)
        L_0x0074:
            i.f.a.h.ConstraintWidget r0 = r0.b
            int r1 = r0.J
            int r0 = r0.Q
            int r1 = r1 + r0
            int r0 = r7.Q
            int r1 = r1 - r0
            int r0 = r7.F
            int r0 = r0 + r1
            r7.c(r1, r0)
            r7.c0 = r2
            return
        L_0x0087:
            i.f.a.h.ConstraintAnchor r0 = r7.v
            i.f.a.h.ConstraintAnchor r0 = r0.d
            if (r0 == 0) goto L_0x008e
            r1 = 1
        L_0x008e:
            if (r1 == 0) goto L_0x0095
            i.f.a.h.ConstraintAnchor r0 = r7.v
            i.f.a.h.ConstraintAnchor r0 = r0.d
            goto L_0x0099
        L_0x0095:
            i.f.a.h.ConstraintAnchor r0 = r7.f1122t
            i.f.a.h.ConstraintAnchor r0 = r0.d
        L_0x0099:
            if (r0 == 0) goto L_0x00bc
            i.f.a.h.ConstraintWidget r4 = r0.b
            boolean r5 = r4.c0
            if (r5 != 0) goto L_0x00a4
            r6.a(r4)
        L_0x00a4:
            i.f.a.h.ConstraintAnchor$c r4 = r0.c
            i.f.a.h.ConstraintAnchor$c r5 = i.f.a.h.ConstraintAnchor.c.BOTTOM
            if (r4 != r5) goto L_0x00b4
            i.f.a.h.ConstraintWidget r0 = r0.b
            int r3 = r0.J
            int r0 = r0.d()
            int r3 = r3 + r0
            goto L_0x00bc
        L_0x00b4:
            i.f.a.h.ConstraintAnchor$c r5 = i.f.a.h.ConstraintAnchor.c.TOP
            if (r4 != r5) goto L_0x00bc
            i.f.a.h.ConstraintWidget r0 = r0.b
            int r3 = r0.J
        L_0x00bc:
            if (r1 == 0) goto L_0x00c6
            i.f.a.h.ConstraintAnchor r0 = r7.v
            int r0 = r0.a()
            int r3 = r3 - r0
            goto L_0x00d2
        L_0x00c6:
            i.f.a.h.ConstraintAnchor r0 = r7.f1122t
            int r0 = r0.a()
            int r1 = r7.d()
            int r1 = r1 + r0
            int r3 = r3 + r1
        L_0x00d2:
            int r0 = r7.d()
            int r0 = r3 - r0
            r7.c(r0, r3)
            r7.c0 = r2
        L_0x00dd:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: i.f.a.h.ConstraintWidgetGroup.a(i.f.a.h.ConstraintWidget):void");
    }
}
