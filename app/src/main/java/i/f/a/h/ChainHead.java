package i.f.a.h;

import java.util.ArrayList;

public class ChainHead {
    public ConstraintWidget a;
    public ConstraintWidget b;
    public ConstraintWidget c;
    public ConstraintWidget d;

    /* renamed from: e  reason: collision with root package name */
    public ConstraintWidget f1095e;

    /* renamed from: f  reason: collision with root package name */
    public ConstraintWidget f1096f;
    public ConstraintWidget g;
    public ArrayList<d> h;

    /* renamed from: i  reason: collision with root package name */
    public int f1097i;

    /* renamed from: j  reason: collision with root package name */
    public int f1098j;

    /* renamed from: k  reason: collision with root package name */
    public float f1099k = 0.0f;

    /* renamed from: l  reason: collision with root package name */
    public int f1100l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f1101m = false;

    /* renamed from: n  reason: collision with root package name */
    public boolean f1102n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f1103o;

    /* renamed from: p  reason: collision with root package name */
    public boolean f1104p;

    /* renamed from: q  reason: collision with root package name */
    public boolean f1105q;

    public ChainHead(ConstraintWidget constraintWidget, int i2, boolean z) {
        this.a = constraintWidget;
        this.f1100l = i2;
        this.f1101m = z;
    }
}
