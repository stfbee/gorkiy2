package i.f.a.h;

public class ResolutionDimension extends ResolutionNode {
    public float c = 0.0f;

    public void a(int i2) {
        if (super.b == 0 || this.c != ((float) i2)) {
            this.c = (float) i2;
            if (super.b == 1) {
                b();
            }
            a();
        }
    }
}
