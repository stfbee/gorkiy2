package i.f.a;

import i.f.a.SolverVariable;
import j.a.a.a.outline;
import java.util.Arrays;

public class ArrayLinkedVariables {
    public int a = 0;
    public final ArrayRow b;
    public final Cache c;
    public int d = 8;

    /* renamed from: e  reason: collision with root package name */
    public SolverVariable f1074e = null;

    /* renamed from: f  reason: collision with root package name */
    public int[] f1075f = new int[8];
    public int[] g = new int[8];
    public float[] h = new float[8];

    /* renamed from: i  reason: collision with root package name */
    public int f1076i = -1;

    /* renamed from: j  reason: collision with root package name */
    public int f1077j = -1;

    /* renamed from: k  reason: collision with root package name */
    public boolean f1078k = false;

    public ArrayLinkedVariables(ArrayRow arrayRow, Cache cache) {
        this.b = arrayRow;
        this.c = cache;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float
     arg types: [i.f.a.SolverVariable, int]
     candidates:
      i.f.a.ArrayLinkedVariables.a(boolean[], i.f.a.SolverVariable):i.f.a.SolverVariable
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, float):void
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float */
    public final void a(SolverVariable solverVariable, float f2) {
        if (f2 == 0.0f) {
            a(solverVariable, true);
            return;
        }
        int i2 = this.f1076i;
        if (i2 == -1) {
            this.f1076i = 0;
            this.h[0] = f2;
            this.f1075f[0] = solverVariable.b;
            this.g[0] = -1;
            solverVariable.f1094j++;
            solverVariable.a(this.b);
            this.a++;
            if (!this.f1078k) {
                int i3 = this.f1077j + 1;
                this.f1077j = i3;
                int[] iArr = this.f1075f;
                if (i3 >= iArr.length) {
                    this.f1078k = true;
                    this.f1077j = iArr.length - 1;
                    return;
                }
                return;
            }
            return;
        }
        int i4 = 0;
        int i5 = -1;
        while (i2 != -1 && i4 < this.a) {
            int[] iArr2 = this.f1075f;
            int i6 = iArr2[i2];
            int i7 = solverVariable.b;
            if (i6 == i7) {
                this.h[i2] = f2;
                return;
            }
            if (iArr2[i2] < i7) {
                i5 = i2;
            }
            i2 = this.g[i2];
            i4++;
        }
        int i8 = this.f1077j;
        int i9 = i8 + 1;
        if (this.f1078k) {
            int[] iArr3 = this.f1075f;
            if (iArr3[i8] != -1) {
                i8 = iArr3.length;
            }
        } else {
            i8 = i9;
        }
        int[] iArr4 = this.f1075f;
        if (i8 >= iArr4.length && this.a < iArr4.length) {
            int i10 = 0;
            while (true) {
                int[] iArr5 = this.f1075f;
                if (i10 >= iArr5.length) {
                    break;
                } else if (iArr5[i10] == -1) {
                    i8 = i10;
                    break;
                } else {
                    i10++;
                }
            }
        }
        int[] iArr6 = this.f1075f;
        if (i8 >= iArr6.length) {
            i8 = iArr6.length;
            int i11 = this.d * 2;
            this.d = i11;
            this.f1078k = false;
            this.f1077j = i8 - 1;
            this.h = Arrays.copyOf(this.h, i11);
            this.f1075f = Arrays.copyOf(this.f1075f, this.d);
            this.g = Arrays.copyOf(this.g, this.d);
        }
        this.f1075f[i8] = solverVariable.b;
        this.h[i8] = f2;
        if (i5 != -1) {
            int[] iArr7 = this.g;
            iArr7[i8] = iArr7[i5];
            iArr7[i5] = i8;
        } else {
            this.g[i8] = this.f1076i;
            this.f1076i = i8;
        }
        solverVariable.f1094j++;
        solverVariable.a(this.b);
        this.a++;
        if (!this.f1078k) {
            this.f1077j++;
        }
        if (this.a >= this.f1075f.length) {
            this.f1078k = true;
        }
        int i12 = this.f1077j;
        int[] iArr8 = this.f1075f;
        if (i12 >= iArr8.length) {
            this.f1078k = true;
            this.f1077j = iArr8.length - 1;
        }
    }

    public final boolean b(SolverVariable solverVariable) {
        return solverVariable.f1094j <= 1;
    }

    public String toString() {
        int i2 = this.f1076i;
        String str = "";
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            StringBuilder a2 = outline.a(outline.a(str, " -> "));
            a2.append(this.h[i2]);
            a2.append(" : ");
            StringBuilder a3 = outline.a(a2.toString());
            a3.append(this.c.c[this.f1075f[i2]]);
            str = a3.toString();
            i2 = this.g[i2];
            i3++;
        }
        return str;
    }

    public final float b(int i2) {
        int i3 = this.f1076i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.a) {
            if (i4 == i2) {
                return this.h[i3];
            }
            i3 = this.g[i3];
            i4++;
        }
        return 0.0f;
    }

    public final void a(SolverVariable solverVariable, float f2, boolean z) {
        if (f2 != 0.0f) {
            int i2 = this.f1076i;
            if (i2 == -1) {
                this.f1076i = 0;
                this.h[0] = f2;
                this.f1075f[0] = solverVariable.b;
                this.g[0] = -1;
                solverVariable.f1094j++;
                solverVariable.a(this.b);
                this.a++;
                if (!this.f1078k) {
                    int i3 = this.f1077j + 1;
                    this.f1077j = i3;
                    int[] iArr = this.f1075f;
                    if (i3 >= iArr.length) {
                        this.f1078k = true;
                        this.f1077j = iArr.length - 1;
                        return;
                    }
                    return;
                }
                return;
            }
            int i4 = 0;
            int i5 = -1;
            while (i2 != -1 && i4 < this.a) {
                int[] iArr2 = this.f1075f;
                int i6 = iArr2[i2];
                int i7 = solverVariable.b;
                if (i6 == i7) {
                    float[] fArr = this.h;
                    fArr[i2] = fArr[i2] + f2;
                    if (fArr[i2] == 0.0f) {
                        if (i2 == this.f1076i) {
                            this.f1076i = this.g[i2];
                        } else {
                            int[] iArr3 = this.g;
                            iArr3[i5] = iArr3[i2];
                        }
                        if (z) {
                            solverVariable.b(this.b);
                        }
                        if (this.f1078k) {
                            this.f1077j = i2;
                        }
                        solverVariable.f1094j--;
                        this.a--;
                        return;
                    }
                    return;
                }
                if (iArr2[i2] < i7) {
                    i5 = i2;
                }
                i2 = this.g[i2];
                i4++;
            }
            int i8 = this.f1077j;
            int i9 = i8 + 1;
            if (this.f1078k) {
                int[] iArr4 = this.f1075f;
                if (iArr4[i8] != -1) {
                    i8 = iArr4.length;
                }
            } else {
                i8 = i9;
            }
            int[] iArr5 = this.f1075f;
            if (i8 >= iArr5.length && this.a < iArr5.length) {
                int i10 = 0;
                while (true) {
                    int[] iArr6 = this.f1075f;
                    if (i10 >= iArr6.length) {
                        break;
                    } else if (iArr6[i10] == -1) {
                        i8 = i10;
                        break;
                    } else {
                        i10++;
                    }
                }
            }
            int[] iArr7 = this.f1075f;
            if (i8 >= iArr7.length) {
                i8 = iArr7.length;
                int i11 = this.d * 2;
                this.d = i11;
                this.f1078k = false;
                this.f1077j = i8 - 1;
                this.h = Arrays.copyOf(this.h, i11);
                this.f1075f = Arrays.copyOf(this.f1075f, this.d);
                this.g = Arrays.copyOf(this.g, this.d);
            }
            this.f1075f[i8] = solverVariable.b;
            this.h[i8] = f2;
            if (i5 != -1) {
                int[] iArr8 = this.g;
                iArr8[i8] = iArr8[i5];
                iArr8[i5] = i8;
            } else {
                this.g[i8] = this.f1076i;
                this.f1076i = i8;
            }
            solverVariable.f1094j++;
            solverVariable.a(this.b);
            this.a++;
            if (!this.f1078k) {
                this.f1077j++;
            }
            int i12 = this.f1077j;
            int[] iArr9 = this.f1075f;
            if (i12 >= iArr9.length) {
                this.f1078k = true;
                this.f1077j = iArr9.length - 1;
            }
        }
    }

    public final float a(SolverVariable solverVariable, boolean z) {
        if (this.f1074e == solverVariable) {
            this.f1074e = null;
        }
        int i2 = this.f1076i;
        if (i2 == -1) {
            return 0.0f;
        }
        int i3 = 0;
        int i4 = -1;
        while (i2 != -1 && i3 < this.a) {
            if (this.f1075f[i2] == solverVariable.b) {
                if (i2 == this.f1076i) {
                    this.f1076i = this.g[i2];
                } else {
                    int[] iArr = this.g;
                    iArr[i4] = iArr[i2];
                }
                if (z) {
                    solverVariable.b(this.b);
                }
                solverVariable.f1094j--;
                this.a--;
                this.f1075f[i2] = -1;
                if (this.f1078k) {
                    this.f1077j = i2;
                }
                return this.h[i2];
            }
            i3++;
            i4 = i2;
            i2 = this.g[i2];
        }
        return 0.0f;
    }

    public final void a() {
        int i2 = this.f1076i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            SolverVariable solverVariable = this.c.c[this.f1075f[i2]];
            if (solverVariable != null) {
                solverVariable.b(this.b);
            }
            i2 = this.g[i2];
            i3++;
        }
        this.f1076i = -1;
        this.f1077j = -1;
        this.f1078k = false;
        this.a = 0;
    }

    public SolverVariable a(boolean[] zArr, SolverVariable solverVariable) {
        SolverVariable.a aVar;
        int i2 = this.f1076i;
        int i3 = 0;
        SolverVariable solverVariable2 = null;
        float f2 = 0.0f;
        while (i2 != -1 && i3 < this.a) {
            if (this.h[i2] < 0.0f) {
                SolverVariable solverVariable3 = this.c.c[this.f1075f[i2]];
                if ((zArr == null || !zArr[solverVariable3.b]) && solverVariable3 != solverVariable && ((aVar = solverVariable3.g) == SolverVariable.a.SLACK || aVar == SolverVariable.a.ERROR)) {
                    float f3 = this.h[i2];
                    if (f3 < f2) {
                        solverVariable2 = solverVariable3;
                        f2 = f3;
                    }
                }
            }
            i2 = this.g[i2];
            i3++;
        }
        return solverVariable2;
    }

    public final SolverVariable a(int i2) {
        int i3 = this.f1076i;
        int i4 = 0;
        while (i3 != -1 && i4 < this.a) {
            if (i4 == i2) {
                return this.c.c[this.f1075f[i3]];
            }
            i3 = this.g[i3];
            i4++;
        }
        return null;
    }

    public final float a(SolverVariable solverVariable) {
        int i2 = this.f1076i;
        int i3 = 0;
        while (i2 != -1 && i3 < this.a) {
            if (this.f1075f[i2] == solverVariable.b) {
                return this.h[i2];
            }
            i2 = this.g[i2];
            i3++;
        }
        return 0.0f;
    }
}
