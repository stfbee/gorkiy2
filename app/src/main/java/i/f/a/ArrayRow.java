package i.f.a;

import i.f.a.LinearSystem;

public class ArrayRow implements LinearSystem.a {
    public SolverVariable a = null;
    public float b = 0.0f;
    public boolean c;
    public final ArrayLinkedVariables d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1079e = false;

    public ArrayRow(Cache cache) {
        this.d = new ArrayLinkedVariables(this, cache);
    }

    public ArrayRow a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, int i2) {
        boolean z = false;
        if (i2 != 0) {
            if (i2 < 0) {
                i2 *= -1;
                z = true;
            }
            this.b = (float) i2;
        }
        if (!z) {
            this.d.a(solverVariable, -1.0f);
            this.d.a(solverVariable2, 1.0f);
            this.d.a(solverVariable3, 1.0f);
        } else {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable3, -1.0f);
        }
        return this;
    }

    public ArrayRow b(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, int i2) {
        boolean z = false;
        if (i2 != 0) {
            if (i2 < 0) {
                i2 *= -1;
                z = true;
            }
            this.b = (float) i2;
        }
        if (!z) {
            this.d.a(solverVariable, -1.0f);
            this.d.a(solverVariable2, 1.0f);
            this.d.a(solverVariable3, -1.0f);
        } else {
            this.d.a(solverVariable, 1.0f);
            this.d.a(solverVariable2, -1.0f);
            this.d.a(solverVariable3, 1.0f);
        }
        return this;
    }

    /* JADX WARNING: Removed duplicated region for block: B:25:0x0078  */
    /* JADX WARNING: Removed duplicated region for block: B:26:0x007d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.String toString() {
        /*
            r9 = this;
            i.f.a.SolverVariable r0 = r9.a
            if (r0 != 0) goto L_0x0007
            java.lang.String r0 = "0"
            goto L_0x0016
        L_0x0007:
            java.lang.String r0 = ""
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            i.f.a.SolverVariable r1 = r9.a
            r0.append(r1)
            java.lang.String r0 = r0.toString()
        L_0x0016:
            java.lang.String r1 = " = "
            java.lang.String r0 = j.a.a.a.outline.a(r0, r1)
            float r1 = r9.b
            r2 = 0
            r3 = 0
            int r1 = (r1 > r2 ? 1 : (r1 == r2 ? 0 : -1))
            if (r1 == 0) goto L_0x0033
            java.lang.StringBuilder r0 = j.a.a.a.outline.a(r0)
            float r1 = r9.b
            r0.append(r1)
            java.lang.String r0 = r0.toString()
            r1 = 1
            goto L_0x0034
        L_0x0033:
            r1 = 0
        L_0x0034:
            i.f.a.ArrayLinkedVariables r4 = r9.d
            int r4 = r4.a
        L_0x0038:
            if (r3 >= r4) goto L_0x0098
            i.f.a.ArrayLinkedVariables r5 = r9.d
            i.f.a.SolverVariable r5 = r5.a(r3)
            if (r5 != 0) goto L_0x0043
            goto L_0x0095
        L_0x0043:
            i.f.a.ArrayLinkedVariables r6 = r9.d
            float r6 = r6.b(r3)
            int r7 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r7 != 0) goto L_0x004e
            goto L_0x0095
        L_0x004e:
            java.lang.String r5 = r5.toString()
            r8 = -1082130432(0xffffffffbf800000, float:-1.0)
            if (r1 != 0) goto L_0x0061
            int r1 = (r6 > r2 ? 1 : (r6 == r2 ? 0 : -1))
            if (r1 >= 0) goto L_0x0072
            java.lang.String r1 = "- "
            java.lang.String r0 = j.a.a.a.outline.a(r0, r1)
            goto L_0x0070
        L_0x0061:
            if (r7 <= 0) goto L_0x006a
            java.lang.String r1 = " + "
            java.lang.String r0 = j.a.a.a.outline.a(r0, r1)
            goto L_0x0072
        L_0x006a:
            java.lang.String r1 = " - "
            java.lang.String r0 = j.a.a.a.outline.a(r0, r1)
        L_0x0070:
            float r6 = r6 * r8
        L_0x0072:
            r1 = 1065353216(0x3f800000, float:1.0)
            int r1 = (r6 > r1 ? 1 : (r6 == r1 ? 0 : -1))
            if (r1 != 0) goto L_0x007d
            java.lang.String r0 = j.a.a.a.outline.a(r0, r5)
            goto L_0x0094
        L_0x007d:
            java.lang.StringBuilder r1 = new java.lang.StringBuilder
            r1.<init>()
            r1.append(r0)
            r1.append(r6)
            java.lang.String r0 = " "
            r1.append(r0)
            r1.append(r5)
            java.lang.String r0 = r1.toString()
        L_0x0094:
            r1 = 1
        L_0x0095:
            int r3 = r3 + 1
            goto L_0x0038
        L_0x0098:
            if (r1 != 0) goto L_0x00a0
            java.lang.String r1 = "0.0"
            java.lang.String r0 = j.a.a.a.outline.a(r0, r1)
        L_0x00a0:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: i.f.a.ArrayRow.toString():java.lang.String");
    }

    public ArrayRow a(LinearSystem linearSystem, int i2) {
        this.d.a(linearSystem.a(i2, "ep"), 1.0f);
        this.d.a(linearSystem.a(i2, "em"), -1.0f);
        return this;
    }

    public ArrayRow b(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f2) {
        this.d.a(solverVariable3, 0.5f);
        this.d.a(solverVariable4, 0.5f);
        this.d.a(solverVariable, -0.5f);
        this.d.a(solverVariable2, -0.5f);
        this.b = -f2;
        return this;
    }

    public ArrayRow a(SolverVariable solverVariable, SolverVariable solverVariable2, SolverVariable solverVariable3, SolverVariable solverVariable4, float f2) {
        this.d.a(solverVariable, -1.0f);
        this.d.a(solverVariable2, 1.0f);
        this.d.a(solverVariable3, f2);
        this.d.a(solverVariable4, -f2);
        return this;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float
     arg types: [i.f.a.SolverVariable, int]
     candidates:
      i.f.a.ArrayLinkedVariables.a(boolean[], i.f.a.SolverVariable):i.f.a.SolverVariable
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, float):void
      i.f.a.ArrayLinkedVariables.a(i.f.a.SolverVariable, boolean):float */
    public void b(SolverVariable solverVariable) {
        SolverVariable solverVariable2 = this.a;
        if (solverVariable2 != null) {
            this.d.a(solverVariable2, -1.0f);
            this.a = null;
        }
        float a2 = this.d.a(solverVariable, true) * -1.0f;
        this.a = solverVariable;
        if (a2 != 1.0f) {
            this.b /= a2;
            ArrayLinkedVariables arrayLinkedVariables = this.d;
            int i2 = arrayLinkedVariables.f1076i;
            int i3 = 0;
            while (i2 != -1 && i3 < arrayLinkedVariables.a) {
                float[] fArr = arrayLinkedVariables.h;
                fArr[i2] = fArr[i2] / a2;
                i2 = arrayLinkedVariables.g[i2];
                i3++;
            }
        }
    }

    public void a(SolverVariable solverVariable) {
        int i2 = solverVariable.d;
        float f2 = 1.0f;
        if (i2 != 1) {
            if (i2 == 2) {
                f2 = 1000.0f;
            } else if (i2 == 3) {
                f2 = 1000000.0f;
            } else if (i2 == 4) {
                f2 = 1.0E9f;
            } else if (i2 == 5) {
                f2 = 1.0E12f;
            }
        }
        this.d.a(solverVariable, f2);
    }
}
