package i.v.a.e;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import i.v.a.SimpleSQLiteQuery;
import i.v.a.SupportSQLiteDatabase;

/* compiled from: FrameworkSQLiteDatabase */
public class FrameworkSQLiteDatabase0 implements SupportSQLiteDatabase {
    public static final String[] c = new String[0];
    public final SQLiteDatabase b;

    public FrameworkSQLiteDatabase0(SQLiteDatabase sQLiteDatabase) {
        this.b = sQLiteDatabase;
    }

    public Cursor a(SimpleSQLiteQuery simpleSQLiteQuery) {
        return this.b.rawQueryWithFactory(new FrameworkSQLiteDatabase(this, simpleSQLiteQuery), simpleSQLiteQuery.a, c, null);
    }

    public Cursor b(String str) {
        return a(new SimpleSQLiteQuery(str));
    }

    public void close() {
        this.b.close();
    }

    public String a() {
        return this.b.getPath();
    }
}
