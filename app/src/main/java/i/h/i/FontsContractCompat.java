package i.h.i;

import android.content.ContentUris;
import android.content.Context;
import android.content.pm.PackageManager;
import android.content.pm.ProviderInfo;
import android.content.pm.Signature;
import android.content.res.Resources;
import android.database.Cursor;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.os.Handler;
import i.b.k.ResourcesFlusher;
import i.e.LruCache;
import i.e.SimpleArrayMap;
import i.h.e.b.ResourcesCompat;
import i.h.f.TypefaceCompat;
import i.h.i.SelfDestructiveThread;
import i.h.i.b;
import j.a.a.a.outline;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import l.a.a.a.o.b.AbstractSpiCall;

public class FontsContractCompat {
    public static final LruCache<String, Typeface> a = new LruCache<>(16);
    public static final SelfDestructiveThread b = new SelfDestructiveThread("fonts", 10, AbstractSpiCall.DEFAULT_TIMEOUT);
    public static final Object c = new Object();
    public static final SimpleArrayMap<String, ArrayList<SelfDestructiveThread.c<b.g>>> d = new SimpleArrayMap<>();

    /* renamed from: e  reason: collision with root package name */
    public static final Comparator<byte[]> f1190e = new d();

    public static class a implements Callable<b.g> {
        public final /* synthetic */ Context b;
        public final /* synthetic */ FontRequest c;
        public final /* synthetic */ int d;

        /* renamed from: e  reason: collision with root package name */
        public final /* synthetic */ String f1191e;

        public a(Context context, FontRequest fontRequest, int i2, String str) {
            this.b = context;
            this.c = fontRequest;
            this.d = i2;
            this.f1191e = str;
        }

        public Object call() {
            g a = FontsContractCompat.a(this.b, this.c, this.d);
            Typeface typeface = a.a;
            if (typeface != null) {
                FontsContractCompat.a.a(this.f1191e, typeface);
            }
            return a;
        }
    }

    public static class b implements SelfDestructiveThread.c<b.g> {
        public final /* synthetic */ ResourcesCompat a;
        public final /* synthetic */ Handler b;

        public b(ResourcesCompat resourcesCompat, Handler handler) {
            this.a = resourcesCompat;
            this.b = handler;
        }

        public void a(Object obj) {
            g gVar = (g) obj;
            if (gVar == null) {
                this.a.a(1, this.b);
                return;
            }
            int i2 = gVar.b;
            if (i2 == 0) {
                this.a.a(gVar.a, this.b);
            } else {
                this.a.a(i2, this.b);
            }
        }
    }

    public static class c implements SelfDestructiveThread.c<b.g> {
        public final /* synthetic */ String a;

        public c(String str) {
            this.a = str;
        }

        /* JADX WARNING: Code restructure failed: missing block: B:11:0x001e, code lost:
            if (r0 >= r1.size()) goto L_0x002c;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:12:0x0020, code lost:
            ((i.h.i.SelfDestructiveThread.c) r1.get(r0)).a(r5);
            r0 = r0 + 1;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:13:0x002c, code lost:
            return;
         */
        /* JADX WARNING: Code restructure failed: missing block: B:9:0x0019, code lost:
            r0 = 0;
         */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public void a(i.h.i.FontsContractCompat.g r5) {
            /*
                r4 = this;
                java.lang.Object r0 = i.h.i.FontsContractCompat.c
                monitor-enter(r0)
                i.e.SimpleArrayMap<java.lang.String, java.util.ArrayList<i.h.i.SelfDestructiveThread$c<i.h.i.b$g>>> r1 = i.h.i.FontsContractCompat.d     // Catch:{ all -> 0x002d }
                java.lang.String r2 = r4.a     // Catch:{ all -> 0x002d }
                java.lang.Object r1 = r1.get(r2)     // Catch:{ all -> 0x002d }
                java.util.ArrayList r1 = (java.util.ArrayList) r1     // Catch:{ all -> 0x002d }
                if (r1 != 0) goto L_0x0011
                monitor-exit(r0)     // Catch:{ all -> 0x002d }
                return
            L_0x0011:
                i.e.SimpleArrayMap<java.lang.String, java.util.ArrayList<i.h.i.SelfDestructiveThread$c<i.h.i.b$g>>> r2 = i.h.i.FontsContractCompat.d     // Catch:{ all -> 0x002d }
                java.lang.String r3 = r4.a     // Catch:{ all -> 0x002d }
                r2.remove(r3)     // Catch:{ all -> 0x002d }
                monitor-exit(r0)     // Catch:{ all -> 0x002d }
                r0 = 0
            L_0x001a:
                int r2 = r1.size()
                if (r0 >= r2) goto L_0x002c
                java.lang.Object r2 = r1.get(r0)
                i.h.i.SelfDestructiveThread$c r2 = (i.h.i.SelfDestructiveThread.c) r2
                r2.a(r5)
                int r0 = r0 + 1
                goto L_0x001a
            L_0x002c:
                return
            L_0x002d:
                r5 = move-exception
                monitor-exit(r0)     // Catch:{ all -> 0x002d }
                throw r5
            */
            throw new UnsupportedOperationException("Method not decompiled: i.h.i.FontsContractCompat.c.a(i.h.i.FontsContractCompat$g):void");
        }
    }

    public static class d implements Comparator<byte[]> {
        /* JADX INFO: additional move instructions added (1) to help type inference */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v2, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v5, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v5, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r5v6, resolved type: byte} */
        /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r6v6, resolved type: byte} */
        /* JADX WARNING: Multi-variable type inference failed */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int compare(java.lang.Object r5, java.lang.Object r6) {
            /*
                r4 = this;
                byte[] r5 = (byte[]) r5
                byte[] r6 = (byte[]) r6
                int r0 = r5.length
                int r1 = r6.length
                r2 = 0
                if (r0 == r1) goto L_0x000c
                int r5 = r5.length
                int r6 = r6.length
                goto L_0x001a
            L_0x000c:
                r0 = 0
            L_0x000d:
                int r1 = r5.length
                if (r0 >= r1) goto L_0x0020
                byte r1 = r5[r0]
                byte r3 = r6[r0]
                if (r1 == r3) goto L_0x001d
                byte r5 = r5[r0]
                byte r6 = r6[r0]
            L_0x001a:
                int r2 = r5 - r6
                goto L_0x0020
            L_0x001d:
                int r0 = r0 + 1
                goto L_0x000d
            L_0x0020:
                return r2
            */
            throw new UnsupportedOperationException("Method not decompiled: i.h.i.FontsContractCompat.d.compare(java.lang.Object, java.lang.Object):int");
        }
    }

    public static class e {
        public final int a;
        public final f[] b;

        public e(int i2, f[] fVarArr) {
            this.a = i2;
            this.b = fVarArr;
        }
    }

    public static class f {
        public final Uri a;
        public final int b;
        public final int c;
        public final boolean d;

        /* renamed from: e  reason: collision with root package name */
        public final int f1192e;

        public f(Uri uri, int i2, int i3, boolean z, int i4) {
            if (uri != null) {
                this.a = uri;
                this.b = i2;
                this.c = i3;
                this.d = z;
                this.f1192e = i4;
                return;
            }
            throw null;
        }
    }

    public static final class g {
        public final Typeface a;
        public final int b;

        public g(Typeface typeface, int i2) {
            this.a = typeface;
            this.b = i2;
        }
    }

    public static g a(Context context, FontRequest fontRequest, int i2) {
        try {
            e a2 = a(context, (CancellationSignal) null, fontRequest);
            int i3 = a2.a;
            int i4 = -3;
            if (i3 == 0) {
                Typeface a3 = TypefaceCompat.a.a(context, (CancellationSignal) null, a2.b, i2);
                if (a3 != null) {
                    i4 = 0;
                }
                return new g(a3, i4);
            }
            if (i3 == 1) {
                i4 = -2;
            }
            return new g(null, i4);
        } catch (PackageManager.NameNotFoundException unused) {
            return new g(null, -1);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:32:0x0070, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:36:0x0081, code lost:
        r3 = i.h.i.FontsContractCompat.b;
        r4 = new i.h.i.FontsContractCompat.c(r0);
     */
    /* JADX WARNING: Code restructure failed: missing block: B:37:0x0088, code lost:
        if (r3 == null) goto L_0x0098;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:38:0x008a, code lost:
        r3.b(new i.h.i.SelfDestructiveThread0(r3, r1, new android.os.Handler(), r4));
     */
    /* JADX WARNING: Code restructure failed: missing block: B:39:0x0097, code lost:
        return null;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:40:0x0098, code lost:
        throw null;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static android.graphics.Typeface a(android.content.Context r2, i.h.i.FontRequest r3, i.h.e.b.ResourcesCompat r4, android.os.Handler r5, boolean r6, int r7, int r8) {
        /*
            java.lang.StringBuilder r0 = new java.lang.StringBuilder
            r0.<init>()
            java.lang.String r1 = r3.f1189f
            r0.append(r1)
            java.lang.String r1 = "-"
            r0.append(r1)
            r0.append(r8)
            java.lang.String r0 = r0.toString()
            i.e.LruCache<java.lang.String, android.graphics.Typeface> r1 = i.h.i.FontsContractCompat.a
            java.lang.Object r1 = r1.a(r0)
            android.graphics.Typeface r1 = (android.graphics.Typeface) r1
            if (r1 == 0) goto L_0x0026
            if (r4 == 0) goto L_0x0025
            r4.a(r1)
        L_0x0025:
            return r1
        L_0x0026:
            if (r6 == 0) goto L_0x0041
            r1 = -1
            if (r7 != r1) goto L_0x0041
            i.h.i.FontsContractCompat$g r2 = a(r2, r3, r8)
            if (r4 == 0) goto L_0x003e
            int r3 = r2.b
            if (r3 != 0) goto L_0x003b
            android.graphics.Typeface r3 = r2.a
            r4.a(r3, r5)
            goto L_0x003e
        L_0x003b:
            r4.a(r3, r5)
        L_0x003e:
            android.graphics.Typeface r2 = r2.a
            return r2
        L_0x0041:
            i.h.i.FontsContractCompat$a r1 = new i.h.i.FontsContractCompat$a
            r1.<init>(r2, r3, r8, r0)
            r2 = 0
            if (r6 == 0) goto L_0x0054
            i.h.i.SelfDestructiveThread r3 = i.h.i.FontsContractCompat.b     // Catch:{ InterruptedException -> 0x0053 }
            java.lang.Object r3 = r3.a(r1, r7)     // Catch:{ InterruptedException -> 0x0053 }
            i.h.i.FontsContractCompat$g r3 = (i.h.i.FontsContractCompat.g) r3     // Catch:{ InterruptedException -> 0x0053 }
            android.graphics.Typeface r2 = r3.a     // Catch:{ InterruptedException -> 0x0053 }
        L_0x0053:
            return r2
        L_0x0054:
            if (r4 != 0) goto L_0x0058
            r3 = r2
            goto L_0x005d
        L_0x0058:
            i.h.i.FontsContractCompat$b r3 = new i.h.i.FontsContractCompat$b
            r3.<init>(r4, r5)
        L_0x005d:
            java.lang.Object r4 = i.h.i.FontsContractCompat.c
            monitor-enter(r4)
            i.e.SimpleArrayMap<java.lang.String, java.util.ArrayList<i.h.i.SelfDestructiveThread$c<i.h.i.b$g>>> r5 = i.h.i.FontsContractCompat.d     // Catch:{ all -> 0x0099 }
            java.lang.Object r5 = r5.getOrDefault(r0, r2)     // Catch:{ all -> 0x0099 }
            java.util.ArrayList r5 = (java.util.ArrayList) r5     // Catch:{ all -> 0x0099 }
            if (r5 == 0) goto L_0x0071
            if (r3 == 0) goto L_0x006f
            r5.add(r3)     // Catch:{ all -> 0x0099 }
        L_0x006f:
            monitor-exit(r4)     // Catch:{ all -> 0x0099 }
            return r2
        L_0x0071:
            if (r3 == 0) goto L_0x0080
            java.util.ArrayList r5 = new java.util.ArrayList     // Catch:{ all -> 0x0099 }
            r5.<init>()     // Catch:{ all -> 0x0099 }
            r5.add(r3)     // Catch:{ all -> 0x0099 }
            i.e.SimpleArrayMap<java.lang.String, java.util.ArrayList<i.h.i.SelfDestructiveThread$c<i.h.i.b$g>>> r3 = i.h.i.FontsContractCompat.d     // Catch:{ all -> 0x0099 }
            r3.put(r0, r5)     // Catch:{ all -> 0x0099 }
        L_0x0080:
            monitor-exit(r4)     // Catch:{ all -> 0x0099 }
            i.h.i.SelfDestructiveThread r3 = i.h.i.FontsContractCompat.b
            i.h.i.FontsContractCompat$c r4 = new i.h.i.FontsContractCompat$c
            r4.<init>(r0)
            if (r3 == 0) goto L_0x0098
            android.os.Handler r5 = new android.os.Handler
            r5.<init>()
            i.h.i.SelfDestructiveThread0 r6 = new i.h.i.SelfDestructiveThread0
            r6.<init>(r3, r1, r5, r4)
            r3.b(r6)
            return r2
        L_0x0098:
            throw r2
        L_0x0099:
            r2 = move-exception
            monitor-exit(r4)     // Catch:{ all -> 0x0099 }
            throw r2
        */
        throw new UnsupportedOperationException("Method not decompiled: i.h.i.FontsContractCompat.a(android.content.Context, i.h.i.FontRequest, i.h.e.b.ResourcesCompat, android.os.Handler, boolean, int, int):android.graphics.Typeface");
    }

    public static Map<Uri, ByteBuffer> a(Context context, b.f[] fVarArr, CancellationSignal cancellationSignal) {
        HashMap hashMap = new HashMap();
        for (b.f fVar : fVarArr) {
            if (fVar.f1192e == 0) {
                Uri uri = fVar.a;
                if (!hashMap.containsKey(uri)) {
                    hashMap.put(uri, ResourcesFlusher.a(context, cancellationSignal, uri));
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    /* JADX WARN: Type inference failed for: r7v2, types: [i.h.i.FontsContractCompat$f[], android.database.Cursor] */
    public static e a(Context context, CancellationSignal cancellationSignal, FontRequest fontRequest) {
        Cursor cursor;
        Uri uri;
        boolean z;
        FontRequest fontRequest2 = fontRequest;
        PackageManager packageManager = context.getPackageManager();
        Resources resources = context.getResources();
        String str = fontRequest2.a;
        ProviderInfo resolveContentProvider = packageManager.resolveContentProvider(str, 0);
        if (resolveContentProvider == null) {
            throw new PackageManager.NameNotFoundException(outline.a("No package found for authority: ", str));
        } else if (resolveContentProvider.packageName.equals(fontRequest2.b)) {
            Signature[] signatureArr = packageManager.getPackageInfo(resolveContentProvider.packageName, 64).signatures;
            ArrayList arrayList = new ArrayList();
            for (Signature byteArray : signatureArr) {
                arrayList.add(byteArray.toByteArray());
            }
            Collections.sort(arrayList, f1190e);
            List<List<byte[]>> list = fontRequest2.d;
            if (list == null) {
                list = ResourcesFlusher.a(resources, fontRequest2.f1188e);
            }
            int i2 = 0;
            while (true) {
                cursor = 0;
                if (i2 >= list.size()) {
                    resolveContentProvider = cursor;
                    break;
                }
                ArrayList arrayList2 = new ArrayList(list.get(i2));
                Collections.sort(arrayList2, f1190e);
                if (arrayList.size() == arrayList2.size()) {
                    int i3 = 0;
                    while (true) {
                        if (i3 >= arrayList.size()) {
                            z = true;
                            break;
                        } else if (!Arrays.equals((byte[]) arrayList.get(i3), (byte[]) arrayList2.get(i3))) {
                            break;
                        } else {
                            i3++;
                        }
                    }
                }
                z = false;
                if (z) {
                    break;
                }
                i2++;
            }
            if (resolveContentProvider == null) {
                return new e(1, cursor);
            }
            String str2 = resolveContentProvider.authority;
            ArrayList arrayList3 = new ArrayList();
            Uri build = new Uri.Builder().scheme("content").authority(str2).build();
            Uri build2 = new Uri.Builder().scheme("content").authority(str2).appendPath("file").build();
            try {
                cursor = context.getContentResolver().query(build, new String[]{"_id", "file_id", "font_ttc_index", "font_variation_settings", "font_weight", "font_italic", "result_code"}, "query = ?", new String[]{fontRequest2.c}, null, cancellationSignal);
                if (cursor != null && cursor.getCount() > 0) {
                    int columnIndex = cursor.getColumnIndex("result_code");
                    arrayList3 = new ArrayList();
                    int columnIndex2 = cursor.getColumnIndex("_id");
                    int columnIndex3 = cursor.getColumnIndex("file_id");
                    int columnIndex4 = cursor.getColumnIndex("font_ttc_index");
                    int columnIndex5 = cursor.getColumnIndex("font_weight");
                    int columnIndex6 = cursor.getColumnIndex("font_italic");
                    while (cursor.moveToNext()) {
                        int i4 = columnIndex != -1 ? cursor.getInt(columnIndex) : 0;
                        int i5 = columnIndex4 != -1 ? cursor.getInt(columnIndex4) : 0;
                        if (columnIndex3 == -1) {
                            uri = ContentUris.withAppendedId(build, cursor.getLong(columnIndex2));
                        } else {
                            uri = ContentUris.withAppendedId(build2, cursor.getLong(columnIndex3));
                        }
                        arrayList3.add(new f(uri, i5, columnIndex5 != -1 ? cursor.getInt(columnIndex5) : 400, columnIndex6 != -1 && cursor.getInt(columnIndex6) == 1, i4));
                    }
                }
                return new e(0, (f[]) arrayList3.toArray(new f[0]));
            } finally {
                if (cursor != 0) {
                    cursor.close();
                }
            }
        } else {
            throw new PackageManager.NameNotFoundException("Found content provider " + str + ", but package was not " + fontRequest2.b);
        }
    }
}
