package i.h.k;

/* compiled from: Pools */
public class Pools1<T> extends Pools0<T> {
    public final Object c = new Object();

    public Pools1(int i2) {
        super(i2);
    }

    public T a() {
        T a;
        synchronized (this.c) {
            a = super.a();
        }
        return a;
    }

    public boolean a(T t2) {
        boolean a;
        synchronized (this.c) {
            a = super.a(t2);
        }
        return a;
    }
}
