package i.h.f;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.CancellationSignal;
import android.util.Log;
import i.b.k.ResourcesFlusher;
import i.e.SimpleArrayMap;
import i.h.e.b.FontResourcesParserCompat0;
import i.h.e.b.FontResourcesParserCompat1;
import i.h.i.FontsContractCompat;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Array;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.ByteBuffer;
import java.nio.MappedByteBuffer;
import java.nio.channels.FileChannel;
import java.util.List;

public class TypefaceCompatApi24Impl extends TypefaceCompatBaseImpl {
    public static final Class<?> b;
    public static final Constructor<?> c;
    public static final Method d;

    /* renamed from: e  reason: collision with root package name */
    public static final Method f1179e;

    static {
        Method method;
        Method method2;
        Class<?> cls;
        Constructor<?> constructor = null;
        try {
            cls = Class.forName("android.graphics.FontFamily");
            Constructor<?> constructor2 = cls.getConstructor(new Class[0]);
            method = cls.getMethod("addFontWeightStyle", ByteBuffer.class, Integer.TYPE, List.class, Integer.TYPE, Boolean.TYPE);
            method2 = Typeface.class.getMethod("createFromFamiliesWithDefault", Array.newInstance(cls, 1).getClass());
            constructor = constructor2;
        } catch (ClassNotFoundException | NoSuchMethodException e2) {
            Log.e("TypefaceCompatApi24Impl", e2.getClass().getName(), e2);
            cls = null;
            method2 = null;
            method = null;
        }
        c = constructor;
        b = cls;
        d = method;
        f1179e = method2;
    }

    public Typeface a(Context context, FontResourcesParserCompat1 fontResourcesParserCompat1, Resources resources, int i2) {
        Object obj;
        MappedByteBuffer mappedByteBuffer;
        Throwable th;
        try {
            obj = c.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            obj = null;
        }
        if (obj == null) {
            return null;
        }
        for (FontResourcesParserCompat0 fontResourcesParserCompat0 : fontResourcesParserCompat1.a) {
            int i3 = fontResourcesParserCompat0.f1176f;
            File a = ResourcesFlusher.a(context);
            Resources resources2 = resources;
            if (a != null) {
                try {
                    if (ResourcesFlusher.a(a, resources2, i3)) {
                        try {
                            FileInputStream fileInputStream = new FileInputStream(a);
                            try {
                                FileChannel channel = fileInputStream.getChannel();
                                mappedByteBuffer = channel.map(FileChannel.MapMode.READ_ONLY, 0, channel.size());
                                fileInputStream.close();
                                a.delete();
                                if (mappedByteBuffer != null || !a(obj, mappedByteBuffer, fontResourcesParserCompat0.f1175e, fontResourcesParserCompat0.b, fontResourcesParserCompat0.c)) {
                                    return null;
                                }
                            } catch (Throwable th2) {
                                Throwable th3 = th2;
                                fileInputStream.close();
                                throw th3;
                            }
                        } catch (IOException unused2) {
                            mappedByteBuffer = null;
                        } catch (Throwable th4) {
                            th.addSuppressed(th4);
                        }
                    }
                } finally {
                    a.delete();
                }
            }
            mappedByteBuffer = null;
            if (mappedByteBuffer != null) {
                return null;
            }
        }
        return a(obj);
    }

    public Typeface a(Context context, CancellationSignal cancellationSignal, FontsContractCompat.f[] fVarArr, int i2) {
        Object obj;
        try {
            obj = c.newInstance(new Object[0]);
        } catch (IllegalAccessException | InstantiationException | InvocationTargetException unused) {
            obj = null;
        }
        if (obj == null) {
            return null;
        }
        SimpleArrayMap simpleArrayMap = new SimpleArrayMap();
        for (FontsContractCompat.f fVar : fVarArr) {
            Uri uri = fVar.a;
            ByteBuffer byteBuffer = (ByteBuffer) simpleArrayMap.get(uri);
            if (byteBuffer == null) {
                byteBuffer = ResourcesFlusher.a(context, cancellationSignal, uri);
                simpleArrayMap.put(uri, byteBuffer);
            }
            if (byteBuffer == null || !a(obj, byteBuffer, fVar.b, fVar.c, fVar.d)) {
                return null;
            }
        }
        Typeface a = a(obj);
        if (a == null) {
            return null;
        }
        return Typeface.create(a, i2);
    }

    public static boolean a(Object obj, ByteBuffer byteBuffer, int i2, int i3, boolean z) {
        try {
            return ((Boolean) d.invoke(obj, byteBuffer, Integer.valueOf(i2), null, Integer.valueOf(i3), Boolean.valueOf(z))).booleanValue();
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return false;
        }
    }

    public static Typeface a(Object obj) {
        try {
            Object newInstance = Array.newInstance(b, 1);
            Array.set(newInstance, 0, obj);
            return (Typeface) f1179e.invoke(null, newInstance);
        } catch (IllegalAccessException | InvocationTargetException unused) {
            return null;
        }
    }
}
