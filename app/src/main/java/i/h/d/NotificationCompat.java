package i.h.d;

import android.app.PendingIntent;
import android.graphics.drawable.Icon;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import androidx.core.graphics.drawable.IconCompat;
import java.lang.reflect.InvocationTargetException;

public class NotificationCompat {
    public final Bundle a;
    public IconCompat b;
    public final RemoteInput[] c;
    public final RemoteInput[] d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f1157e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f1158f;
    public final int g;
    public final boolean h;
    @Deprecated

    /* renamed from: i  reason: collision with root package name */
    public int f1159i;

    /* renamed from: j  reason: collision with root package name */
    public CharSequence f1160j;

    /* renamed from: k  reason: collision with root package name */
    public PendingIntent f1161k;

    public NotificationCompat(int i2, CharSequence charSequence, PendingIntent pendingIntent) {
        int i3;
        IconCompat a2 = i2 == 0 ? null : IconCompat.a(null, "", i2);
        Bundle bundle = new Bundle();
        this.f1158f = true;
        this.b = a2;
        if (a2 != null) {
            int i4 = -1;
            if (a2.a != -1 || (i3 = Build.VERSION.SDK_INT) < 23) {
                i4 = a2.a;
            } else {
                Icon icon = (Icon) a2.b;
                if (i3 >= 28) {
                    i4 = icon.getType();
                } else {
                    try {
                        i4 = ((Integer) icon.getClass().getMethod("getType", new Class[0]).invoke(icon, new Object[0])).intValue();
                    } catch (IllegalAccessException e2) {
                        Log.e("IconCompat", "Unable to get icon type " + icon, e2);
                    } catch (InvocationTargetException e3) {
                        Log.e("IconCompat", "Unable to get icon type " + icon, e3);
                    } catch (NoSuchMethodException e4) {
                        Log.e("IconCompat", "Unable to get icon type " + icon, e4);
                    }
                }
            }
            if (i4 == 2) {
                this.f1159i = a2.a();
            }
        }
        this.f1160j = NotificationCompat0.a(charSequence);
        this.f1161k = pendingIntent;
        this.a = bundle;
        this.c = null;
        this.d = null;
        this.f1157e = true;
        this.g = 0;
        this.f1158f = true;
        this.h = false;
    }
}
