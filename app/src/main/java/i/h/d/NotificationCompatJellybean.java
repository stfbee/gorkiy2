package i.h.d;

import android.os.Bundle;

public class NotificationCompatJellybean {
    public static final Object a = new Object();

    public static Bundle a(NotificationCompat notificationCompat) {
        Bundle bundle;
        Bundle bundle2 = new Bundle();
        bundle2.putInt("icon", notificationCompat.f1159i);
        bundle2.putCharSequence("title", notificationCompat.f1160j);
        bundle2.putParcelable("actionIntent", notificationCompat.f1161k);
        if (notificationCompat.a != null) {
            bundle = new Bundle(notificationCompat.a);
        } else {
            bundle = new Bundle();
        }
        bundle.putBoolean("android.support.allowGeneratedReplies", notificationCompat.f1157e);
        bundle2.putBundle("extras", bundle);
        bundle2.putParcelableArray("remoteInputs", a(notificationCompat.c));
        bundle2.putBoolean("showsUserInterface", notificationCompat.f1158f);
        bundle2.putInt("semanticAction", notificationCompat.g);
        return bundle2;
    }

    public static Bundle[] a(RemoteInput[] remoteInputArr) {
        if (remoteInputArr == null) {
            return null;
        }
        Bundle[] bundleArr = new Bundle[remoteInputArr.length];
        if (remoteInputArr.length <= 0) {
            return bundleArr;
        }
        RemoteInput remoteInput = remoteInputArr[0];
        new Bundle();
        throw null;
    }
}
