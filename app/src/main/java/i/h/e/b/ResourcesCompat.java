package i.h.e.b;

import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;

public abstract class ResourcesCompat {

    public class a implements Runnable {
        public final /* synthetic */ Typeface b;

        public a(Typeface typeface) {
            this.b = typeface;
        }

        public void run() {
            ResourcesCompat.this.a(this.b);
        }
    }

    public class b implements Runnable {
        public final /* synthetic */ int b;

        public b(int i2) {
            this.b = i2;
        }

        public void run() {
            ResourcesCompat.this.a(this.b);
        }
    }

    public abstract void a(int i2);

    public abstract void a(Typeface typeface);

    public final void a(Typeface typeface, Handler handler) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        handler.post(new a(typeface));
    }

    public final void a(int i2, Handler handler) {
        if (handler == null) {
            handler = new Handler(Looper.getMainLooper());
        }
        handler.post(new b(i2));
    }
}
