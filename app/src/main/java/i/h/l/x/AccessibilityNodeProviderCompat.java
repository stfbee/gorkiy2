package i.h.l.x;

import android.os.Bundle;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityNodeProvider;
import java.util.List;

public class AccessibilityNodeProviderCompat {
    public final Object a;

    public static class a extends AccessibilityNodeProvider {
        public final AccessibilityNodeProviderCompat a;

        public a(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            this.a = accessibilityNodeProviderCompat;
        }

        public AccessibilityNodeInfo createAccessibilityNodeInfo(int i2) {
            AccessibilityNodeInfoCompat a2 = this.a.a(i2);
            if (a2 == null) {
                return null;
            }
            return a2.a;
        }

        public List<AccessibilityNodeInfo> findAccessibilityNodeInfosByText(String str, int i2) {
            if (this.a != null) {
                return null;
            }
            throw null;
        }

        public boolean performAction(int i2, int i3, Bundle bundle) {
            return this.a.a(i2, i3, bundle);
        }
    }

    public static class b extends a {
        public b(AccessibilityNodeProviderCompat accessibilityNodeProviderCompat) {
            super(accessibilityNodeProviderCompat);
        }

        public AccessibilityNodeInfo findFocus(int i2) {
            AccessibilityNodeInfoCompat b = super.a.b(i2);
            if (b == null) {
                return null;
            }
            return b.a;
        }
    }

    public AccessibilityNodeProviderCompat() {
        this.a = new b(this);
    }

    public AccessibilityNodeInfoCompat a(int i2) {
        return null;
    }

    public boolean a(int i2, int i3, Bundle bundle) {
        return false;
    }

    public AccessibilityNodeInfoCompat b(int i2) {
        return null;
    }

    public AccessibilityNodeProviderCompat(Object obj) {
        this.a = obj;
    }
}
