package i.h.l;

import android.text.TextUtils;
import android.view.View;
import i.h.l.ViewCompat;

/* compiled from: ViewCompat */
public final class ViewCompat1 extends ViewCompat.b<CharSequence> {
    public ViewCompat1(int i2, Class cls, int i3, int i4) {
        super(i2, cls, i3, i4);
    }

    public void a(View view, Object obj) {
        view.setAccessibilityPaneTitle((CharSequence) obj);
    }

    public boolean a(Object obj, Object obj2) {
        return !TextUtils.equals((CharSequence) obj, (CharSequence) obj2);
    }

    public Object a(View view) {
        return view.getAccessibilityPaneTitle();
    }
}
