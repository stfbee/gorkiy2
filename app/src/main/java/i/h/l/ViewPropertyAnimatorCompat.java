package i.h.l;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.View;
import i.b.k.WindowDecorActionBar;
import java.lang.ref.WeakReference;

public final class ViewPropertyAnimatorCompat {
    public WeakReference<View> a;
    public Runnable b = null;
    public Runnable c = null;
    public int d = -1;

    public class a extends AnimatorListenerAdapter {
        public final /* synthetic */ ViewPropertyAnimatorListener a;
        public final /* synthetic */ View b;

        public a(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, ViewPropertyAnimatorListener viewPropertyAnimatorListener, View view) {
            this.a = viewPropertyAnimatorListener;
            this.b = view;
        }

        public void onAnimationCancel(Animator animator) {
            this.a.c(this.b);
        }

        public void onAnimationEnd(Animator animator) {
            this.a.a(this.b);
        }

        public void onAnimationStart(Animator animator) {
            this.a.b(this.b);
        }
    }

    public class b implements ValueAnimator.AnimatorUpdateListener {
        public final /* synthetic */ ViewPropertyAnimatorUpdateListener a;
        public final /* synthetic */ View b;

        public b(ViewPropertyAnimatorCompat viewPropertyAnimatorCompat, ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener, View view) {
            this.a = viewPropertyAnimatorUpdateListener;
            this.b = view;
        }

        public void onAnimationUpdate(ValueAnimator valueAnimator) {
            ((View) WindowDecorActionBar.this.d.getParent()).invalidate();
        }
    }

    public ViewPropertyAnimatorCompat(View view) {
        this.a = new WeakReference<>(view);
    }

    public ViewPropertyAnimatorCompat a(long j2) {
        View view = this.a.get();
        if (view != null) {
            view.animate().setDuration(j2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat b(float f2) {
        View view = this.a.get();
        if (view != null) {
            view.animate().translationY(f2);
        }
        return this;
    }

    public ViewPropertyAnimatorCompat a(float f2) {
        View view = this.a.get();
        if (view != null) {
            view.animate().alpha(f2);
        }
        return this;
    }

    public void a() {
        View view = this.a.get();
        if (view != null) {
            view.animate().cancel();
        }
    }

    public ViewPropertyAnimatorCompat a(ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        View view = this.a.get();
        if (view != null) {
            a(view, viewPropertyAnimatorListener);
        }
        return this;
    }

    public final void a(View view, ViewPropertyAnimatorListener viewPropertyAnimatorListener) {
        if (viewPropertyAnimatorListener != null) {
            view.animate().setListener(new a(this, viewPropertyAnimatorListener, view));
        } else {
            view.animate().setListener(null);
        }
    }

    public ViewPropertyAnimatorCompat a(ViewPropertyAnimatorUpdateListener viewPropertyAnimatorUpdateListener) {
        View view = this.a.get();
        if (view != null) {
            b bVar = null;
            if (viewPropertyAnimatorUpdateListener != null) {
                bVar = new b(this, viewPropertyAnimatorUpdateListener, view);
            }
            view.animate().setUpdateListener(bVar);
        }
        return this;
    }
}
