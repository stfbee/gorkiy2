package i.h.l;

import android.annotation.SuppressLint;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.util.SparseArray;
import android.view.Display;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import android.view.WindowInsets;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityManager;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.h.l.x.AccessibilityViewCommand;
import java.lang.ref.WeakReference;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.WeakHashMap;
import java.util.concurrent.atomic.AtomicInteger;

public class ViewCompat {
    public static WeakHashMap<View, s> a = null;
    public static Field b;
    public static boolean c = false;
    public static ThreadLocal<Rect> d;

    public static class a implements View.OnApplyWindowInsetsListener {
        public final /* synthetic */ OnApplyWindowInsetsListener a;

        public a(OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
            this.a = onApplyWindowInsetsListener;
        }

        public WindowInsets onApplyWindowInsets(View view, WindowInsets windowInsets) {
            return (WindowInsets) this.a.a(view, WindowInsetsCompat.a(windowInsets)).a;
        }
    }

    public interface c {
        boolean a(View view, KeyEvent keyEvent);
    }

    static {
        new AtomicInteger(1);
        new WeakHashMap();
    }

    public static void A(View view) {
        view.postInvalidateOnAnimation();
    }

    public static void B(View view) {
        view.requestApplyInsets();
    }

    public static void C(View view) {
        view.stopNestedScroll();
    }

    public static void D(View view) {
        float translationY = view.getTranslationY();
        view.setTranslationY(1.0f + translationY);
        view.setTranslationY(translationY);
    }

    public static Rect a() {
        if (d == null) {
            d = new ThreadLocal<>();
        }
        Rect rect = d.get();
        if (rect == null) {
            rect = new Rect();
            d.set(rect);
        }
        rect.setEmpty();
        return rect;
    }

    public static AccessibilityDelegateCompat b(View view) {
        View.AccessibilityDelegate c2 = c(view);
        if (c2 == null) {
            return null;
        }
        if (c2 instanceof AccessibilityDelegateCompat.a) {
            return ((AccessibilityDelegateCompat.a) c2).a;
        }
        return new AccessibilityDelegateCompat(c2);
    }

    public static View.AccessibilityDelegate c(View view) {
        if (Build.VERSION.SDK_INT >= 29) {
            return view.getAccessibilityDelegate();
        }
        if (c) {
            return null;
        }
        if (b == null) {
            try {
                Field declaredField = View.class.getDeclaredField("mAccessibilityDelegate");
                b = declaredField;
                declaredField.setAccessible(true);
            } catch (Throwable unused) {
                c = true;
                return null;
            }
        }
        try {
            Object obj = b.get(view);
            if (obj instanceof View.AccessibilityDelegate) {
                return (View.AccessibilityDelegate) obj;
            }
            return null;
        } catch (Throwable unused2) {
            c = true;
            return null;
        }
    }

    public static ColorStateList d(View view) {
        return view.getBackgroundTintList();
    }

    public static void e(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.offsetTopAndBottom(i2);
            return;
        }
        Rect a2 = a();
        boolean z = false;
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            a2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
            z = !a2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
        b(view, i2);
        if (z && a2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
            ((View) parent).invalidate(a2);
        }
    }

    public static void f(View view, int i2) {
        a(i2, view);
        c(view, 0);
    }

    public static void g(View view, int i2) {
        view.setAccessibilityLiveRegion(i2);
    }

    public static void h(View view, int i2) {
        view.setImportantForAccessibility(i2);
    }

    public static int i(View view) {
        return view.getImportantForAccessibility();
    }

    @SuppressLint({"InlinedApi"})
    public static int j(View view) {
        if (Build.VERSION.SDK_INT >= 26) {
            return view.getImportantForAutofill();
        }
        return 0;
    }

    public static int k(View view) {
        return view.getLayoutDirection();
    }

    public static int l(View view) {
        return view.getMinimumHeight();
    }

    public static int m(View view) {
        return view.getMinimumWidth();
    }

    public static int n(View view) {
        return view.getPaddingEnd();
    }

    public static int o(View view) {
        return view.getPaddingStart();
    }

    public static String p(View view) {
        return view.getTransitionName();
    }

    public static int q(View view) {
        return view.getWindowSystemUiVisibility();
    }

    public static float r(View view) {
        return view.getZ();
    }

    public static boolean s(View view) {
        return view.hasOnClickListeners();
    }

    public static boolean t(View view) {
        return view.hasOverlappingRendering();
    }

    public static boolean u(View view) {
        return view.hasTransientState();
    }

    public static boolean v(View view) {
        return view.isAttachedToWindow();
    }

    public static boolean w(View view) {
        return view.isLaidOut();
    }

    public static boolean x(View view) {
        return view.isNestedScrollingEnabled();
    }

    public static boolean y(View view) {
        return view.isPaddingRelative();
    }

    public static boolean z(View view) {
        Boolean bool = (Boolean) new ViewCompat0(i.h.b.tag_screen_reader_focusable, Boolean.class, 28).b(view);
        if (bool == null) {
            return false;
        }
        return bool.booleanValue();
    }

    public static void d(View view, int i2) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.offsetLeftAndRight(i2);
            return;
        }
        Rect a2 = a();
        boolean z = false;
        ViewParent parent = view.getParent();
        if (parent instanceof View) {
            View view2 = (View) parent;
            a2.set(view2.getLeft(), view2.getTop(), view2.getRight(), view2.getBottom());
            z = !a2.intersects(view.getLeft(), view.getTop(), view.getRight(), view.getBottom());
        }
        a(view, i2);
        if (z && a2.intersect(view.getLeft(), view.getTop(), view.getRight(), view.getBottom())) {
            ((View) parent).invalidate(a2);
        }
    }

    public static float g(View view) {
        return view.getElevation();
    }

    public static boolean h(View view) {
        return view.getFitsSystemWindows();
    }

    public static Display f(View view) {
        return view.getDisplay();
    }

    public static abstract class b<T> {
        public final int a;
        public final Class<T> b;
        public final int c;

        public b(int i2, Class<T> cls, int i3) {
            this.a = i2;
            this.b = cls;
            this.c = i3;
        }

        public abstract T a(View view);

        public abstract void a(View view, T t2);

        public boolean a(Boolean bool, Boolean bool2) {
            boolean z;
            boolean booleanValue = bool == null ? false : bool.booleanValue();
            if (bool2 == null) {
                z = false;
            } else {
                z = bool2.booleanValue();
            }
            if (booleanValue == z) {
                return true;
            }
            return false;
        }

        public abstract boolean a(T t2, T t3);

        public T b(View view) {
            if (Build.VERSION.SDK_INT >= this.c) {
                return a(view);
            }
            T tag = view.getTag(this.a);
            if (this.b.isInstance(tag)) {
                return tag;
            }
            return null;
        }

        public b(int i2, Class<T> cls, int i3, int i4) {
            this.a = i2;
            this.b = cls;
            this.c = i4;
        }
    }

    public static class d {
        public static final ArrayList<WeakReference<View>> d = new ArrayList<>();
        public WeakHashMap<View, Boolean> a = null;
        public SparseArray<WeakReference<View>> b = null;
        public WeakReference<KeyEvent> c = null;

        public static d a(View view) {
            d dVar = (d) view.getTag(i.h.b.tag_unhandled_key_event_manager);
            if (dVar != null) {
                return dVar;
            }
            d dVar2 = new d();
            view.setTag(i.h.b.tag_unhandled_key_event_manager, dVar2);
            return dVar2;
        }

        public final boolean b(View view, KeyEvent keyEvent) {
            ArrayList arrayList = (ArrayList) view.getTag(i.h.b.tag_unhandled_key_listeners);
            if (arrayList == null) {
                return false;
            }
            for (int size = arrayList.size() - 1; size >= 0; size--) {
                if (((c) arrayList.get(size)).a(view, keyEvent)) {
                    return true;
                }
            }
            return false;
        }

        public final View a(View view, KeyEvent keyEvent) {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null && weakHashMap.containsKey(view)) {
                if (view instanceof ViewGroup) {
                    ViewGroup viewGroup = (ViewGroup) view;
                    for (int childCount = viewGroup.getChildCount() - 1; childCount >= 0; childCount--) {
                        View a2 = a(viewGroup.getChildAt(childCount), keyEvent);
                        if (a2 != null) {
                            return a2;
                        }
                    }
                }
                if (b(view, keyEvent)) {
                    return view;
                }
            }
            return null;
        }

        public final void a() {
            WeakHashMap<View, Boolean> weakHashMap = this.a;
            if (weakHashMap != null) {
                weakHashMap.clear();
            }
            if (!d.isEmpty()) {
                synchronized (d) {
                    if (this.a == null) {
                        this.a = new WeakHashMap<>();
                    }
                    for (int size = d.size() - 1; size >= 0; size--) {
                        View view = (View) d.get(size).get();
                        if (view == null) {
                            d.remove(size);
                        } else {
                            this.a.put(view, Boolean.TRUE);
                            for (ViewParent parent = view.getParent(); parent instanceof View; parent = parent.getParent()) {
                                this.a.put((View) parent, Boolean.TRUE);
                            }
                        }
                    }
                }
            }
        }
    }

    public static void b(View view, boolean z) {
        view.setHasTransientState(z);
    }

    public static void b(View view, int i2) {
        view.offsetTopAndBottom(i2);
        if (view.getVisibility() == 0) {
            float translationY = view.getTranslationY();
            view.setTranslationY(1.0f + translationY);
            view.setTranslationY(translationY);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                D((View) parent);
            }
        }
    }

    public static WindowInsetsCompat a(View view, WindowInsetsCompat windowInsetsCompat) {
        WindowInsets windowInsets = (WindowInsets) windowInsetsCompat.a;
        WindowInsets onApplyWindowInsets = view.onApplyWindowInsets(windowInsets);
        if (!onApplyWindowInsets.equals(windowInsets)) {
            windowInsets = new WindowInsets(onApplyWindowInsets);
        }
        return WindowInsetsCompat.a(windowInsets);
    }

    public static void a(View view, AccessibilityDelegateCompat accessibilityDelegateCompat) {
        View.AccessibilityDelegate accessibilityDelegate;
        if (accessibilityDelegateCompat == null && (c(view) instanceof AccessibilityDelegateCompat.a)) {
            accessibilityDelegateCompat = new AccessibilityDelegateCompat();
        }
        if (accessibilityDelegateCompat == null) {
            accessibilityDelegate = null;
        } else {
            accessibilityDelegate = accessibilityDelegateCompat.b;
        }
        view.setAccessibilityDelegate(accessibilityDelegate);
    }

    public static void c(View view, int i2) {
        if (((AccessibilityManager) view.getContext().getSystemService("accessibility")).isEnabled()) {
            boolean z = ((CharSequence) new ViewCompat1(i.h.b.tag_accessibility_pane_title, CharSequence.class, 8, 28).b(view)) != null;
            if (view.getAccessibilityLiveRegion() != 0 || (z && view.getVisibility() == 0)) {
                AccessibilityEvent obtain = AccessibilityEvent.obtain();
                obtain.setEventType(z ? 32 : 2048);
                obtain.setContentChangeTypes(i2);
                view.sendAccessibilityEventUnchecked(obtain);
            } else if (view.getParent() != null) {
                try {
                    view.getParent().notifySubtreeAccessibilityStateChanged(view, view, i2);
                } catch (AbstractMethodError e2) {
                    Log.e("ViewCompat", view.getParent().getClass().getSimpleName() + " does not fully implement ViewParent", e2);
                }
            }
        }
    }

    public static boolean b(View view, KeyEvent keyEvent) {
        int indexOfKey;
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        d a2 = d.a(view);
        WeakReference<KeyEvent> weakReference = a2.c;
        if (weakReference != null && weakReference.get() == keyEvent) {
            return false;
        }
        a2.c = new WeakReference<>(keyEvent);
        WeakReference weakReference2 = null;
        if (a2.b == null) {
            a2.b = new SparseArray<>();
        }
        SparseArray<WeakReference<View>> sparseArray = a2.b;
        if (keyEvent.getAction() == 1 && (indexOfKey = sparseArray.indexOfKey(keyEvent.getKeyCode())) >= 0) {
            weakReference2 = sparseArray.valueAt(indexOfKey);
            sparseArray.removeAt(indexOfKey);
        }
        if (weakReference2 == null) {
            weakReference2 = sparseArray.get(keyEvent.getKeyCode());
        }
        if (weakReference2 == null) {
            return false;
        }
        View view2 = (View) weakReference2.get();
        if (view2 != null && v(view2)) {
            a2.b(view2, keyEvent);
        }
        return true;
    }

    public static void a(View view, Runnable runnable) {
        view.postOnAnimation(runnable);
    }

    public static Rect e(View view) {
        return view.getClipBounds();
    }

    public static void a(View view, Runnable runnable, long j2) {
        view.postOnAnimationDelayed(runnable, j2);
    }

    public static boolean a(View view, int i2, Bundle bundle) {
        return view.performAccessibilityAction(i2, bundle);
    }

    public static void a(View view, AccessibilityNodeInfoCompat.a aVar, CharSequence charSequence, AccessibilityViewCommand accessibilityViewCommand) {
        if (accessibilityViewCommand == null && charSequence == null) {
            f(view, aVar.a());
            return;
        }
        AccessibilityNodeInfoCompat.a aVar2 = new AccessibilityNodeInfoCompat.a(null, aVar.b, charSequence, accessibilityViewCommand, aVar.c);
        AccessibilityDelegateCompat b2 = b(view);
        if (b2 == null) {
            b2 = new AccessibilityDelegateCompat();
        }
        a(view, b2);
        a(aVar2.a(), view);
        ArrayList arrayList = (ArrayList) view.getTag(i.h.b.tag_accessibility_actions);
        if (arrayList == null) {
            arrayList = new ArrayList();
            view.setTag(i.h.b.tag_accessibility_actions, arrayList);
        }
        arrayList.add(aVar2);
        c(view, 0);
    }

    public static void a(int i2, View view) {
        ArrayList arrayList = (ArrayList) view.getTag(i.h.b.tag_accessibility_actions);
        if (arrayList == null) {
            arrayList = new ArrayList();
            view.setTag(i.h.b.tag_accessibility_actions, arrayList);
        }
        for (int i3 = 0; i3 < arrayList.size(); i3++) {
            if (((AccessibilityNodeInfoCompat.a) arrayList.get(i3)).a() == i2) {
                arrayList.remove(i3);
                return;
            }
        }
    }

    public static void a(View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        view.onInitializeAccessibilityNodeInfo(accessibilityNodeInfoCompat.a);
    }

    public static void a(View view, int i2, int i3, int i4, int i5) {
        view.setPaddingRelative(i2, i3, i4, i5);
    }

    public static ViewPropertyAnimatorCompat a(View view) {
        if (a == null) {
            a = new WeakHashMap<>();
        }
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat = a.get(view);
        if (viewPropertyAnimatorCompat != null) {
            return viewPropertyAnimatorCompat;
        }
        ViewPropertyAnimatorCompat viewPropertyAnimatorCompat2 = new ViewPropertyAnimatorCompat(view);
        a.put(view, viewPropertyAnimatorCompat2);
        return viewPropertyAnimatorCompat2;
    }

    public static void a(View view, float f2) {
        view.setElevation(f2);
    }

    public static void a(View view, String str) {
        view.setTransitionName(str);
    }

    public static void a(View view, OnApplyWindowInsetsListener onApplyWindowInsetsListener) {
        if (onApplyWindowInsetsListener == null) {
            view.setOnApplyWindowInsetsListener(null);
        } else {
            view.setOnApplyWindowInsetsListener(new a(onApplyWindowInsetsListener));
        }
    }

    public static void a(View view, Drawable drawable) {
        view.setBackground(drawable);
    }

    public static void a(View view, ColorStateList colorStateList) {
        view.setBackgroundTintList(colorStateList);
        if (Build.VERSION.SDK_INT == 21) {
            Drawable background = view.getBackground();
            boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
            if (background != null && z) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                view.setBackground(background);
            }
        }
    }

    public static void a(View view, PorterDuff.Mode mode) {
        view.setBackgroundTintMode(mode);
        if (Build.VERSION.SDK_INT == 21) {
            Drawable background = view.getBackground();
            boolean z = (view.getBackgroundTintList() == null && view.getBackgroundTintMode() == null) ? false : true;
            if (background != null && z) {
                if (background.isStateful()) {
                    background.setState(view.getDrawableState());
                }
                view.setBackground(background);
            }
        }
    }

    public static void a(View view, int i2) {
        view.offsetLeftAndRight(i2);
        if (view.getVisibility() == 0) {
            float translationY = view.getTranslationY();
            view.setTranslationY(1.0f + translationY);
            view.setTranslationY(translationY);
            ViewParent parent = view.getParent();
            if (parent instanceof View) {
                D((View) parent);
            }
        }
    }

    public static void a(View view, Rect rect) {
        view.setClipBounds(rect);
    }

    public static void a(View view, int i2, int i3) {
        if (Build.VERSION.SDK_INT >= 23) {
            view.setScrollIndicators(i2, i3);
        }
    }

    public static boolean a(View view, KeyEvent keyEvent) {
        if (Build.VERSION.SDK_INT >= 28) {
            return false;
        }
        d a2 = d.a(view);
        if (a2 != null) {
            if (keyEvent.getAction() == 0) {
                a2.a();
            }
            View a3 = a2.a(view, keyEvent);
            if (keyEvent.getAction() == 0) {
                int keyCode = keyEvent.getKeyCode();
                if (a3 != null && !KeyEvent.isModifierKey(keyCode)) {
                    if (a2.b == null) {
                        a2.b = new SparseArray<>();
                    }
                    a2.b.put(keyCode, new WeakReference(a3));
                }
            }
            if (a3 != null) {
                return true;
            }
            return false;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.h.l.ViewCompat.b.a(android.view.View, java.lang.Object):void
     arg types: [android.view.View, java.lang.Boolean]
     candidates:
      i.h.l.ViewCompat2.a(java.lang.Object, java.lang.Object):boolean
      i.h.l.ViewCompat.b.a(java.lang.Boolean, java.lang.Boolean):boolean
      i.h.l.ViewCompat.b.a(java.lang.Object, java.lang.Object):boolean
      i.h.l.ViewCompat.b.a(android.view.View, java.lang.Object):void */
    public static void a(View view, boolean z) {
        ViewCompat2 viewCompat2 = new ViewCompat2(i.h.b.tag_accessibility_heading, Boolean.class, 28);
        Boolean valueOf = Boolean.valueOf(z);
        if (Build.VERSION.SDK_INT >= viewCompat2.c) {
            viewCompat2.a(view, (Object) valueOf);
        } else if (viewCompat2.a(viewCompat2.b(view), valueOf)) {
            AccessibilityDelegateCompat b2 = b(view);
            if (b2 == null) {
                b2 = new AccessibilityDelegateCompat();
            }
            a(view, b2);
            view.setTag(viewCompat2.a, valueOf);
            c(view, 0);
        }
    }
}
