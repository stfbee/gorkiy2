package leakcanary;

import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import java.lang.ref.ReferenceQueue;
import java.lang.ref.WeakReference;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: KeyedWeakReference.kt */
public final class KeyedWeakReference extends WeakReference<Object> {
    public static final a Companion = new a(null);
    public static volatile long heapDumpUptimeMillis;
    public final String key;
    public final String name;
    public volatile long retainedUptimeMillis;
    public final long watchUptimeMillis;

    /* compiled from: KeyedWeakReference.kt */
    public static final class a {
        public /* synthetic */ a(DefaultConstructorMarker defaultConstructorMarker) {
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public KeyedWeakReference(Object obj, String str, String str2, long j2, ReferenceQueue<Object> referenceQueue) {
        super(obj, referenceQueue);
        if (obj == null) {
            Intrinsics.a("referent");
            throw null;
        } else if (str == null) {
            Intrinsics.a("key");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (referenceQueue != null) {
            this.key = str;
            this.name = str2;
            this.watchUptimeMillis = j2;
            this.retainedUptimeMillis = -1;
        } else {
            Intrinsics.a("referenceQueue");
            throw null;
        }
    }

    public static final long getHeapDumpUptimeMillis() {
        return heapDumpUptimeMillis;
    }

    public static final void setHeapDumpUptimeMillis(long j2) {
        heapDumpUptimeMillis = j2;
    }

    public final String getKey() {
        return this.key;
    }

    public final String getName() {
        return this.name;
    }

    public final long getRetainedUptimeMillis() {
        return this.retainedUptimeMillis;
    }

    public final long getWatchUptimeMillis() {
        return this.watchUptimeMillis;
    }

    public final void setRetainedUptimeMillis(long j2) {
        this.retainedUptimeMillis = j2;
    }
}
