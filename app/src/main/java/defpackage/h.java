package defpackage;

import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.c.j;

/* renamed from: h  reason: default package */
/* compiled from: com.android.tools.r8.jetbrains.kotlin-style lambda group */
public final class h extends j implements Functions<g> {
    public static final h d = new h(0);

    /* renamed from: e  reason: collision with root package name */
    public static final h f734e = new h(1);

    /* renamed from: f  reason: collision with root package name */
    public static final h f735f = new h(2);
    public final /* synthetic */ int c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public h(int i2) {
        super(0);
        this.c = i2;
    }

    public final Object b() {
        int i2 = this.c;
        if (i2 == 0) {
            return Unit.a;
        }
        if (i2 == 1) {
            return Unit.a;
        }
        if (i2 == 2) {
            return Unit.a;
        }
        throw null;
    }
}
