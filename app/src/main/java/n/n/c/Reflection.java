package n.n.c;

import n.p.KClass;
import n.p.KProperty1;

public class Reflection {
    public static final ReflectionFactory a;

    static {
        ReflectionFactory reflectionFactory = null;
        try {
            reflectionFactory = (ReflectionFactory) Class.forName("kotlin.reflect.jvm.internal.ReflectionFactoryImpl").newInstance();
        } catch (ClassCastException | ClassNotFoundException | IllegalAccessException | InstantiationException unused) {
        }
        if (reflectionFactory == null) {
            reflectionFactory = new ReflectionFactory();
        }
        a = reflectionFactory;
    }

    public static KClass a(Class cls) {
        if (a != null) {
            return new ClassReference(cls);
        }
        throw null;
    }

    public static KProperty1 a(PropertyReference1 propertyReference1) {
        if (a != null) {
            return propertyReference1;
        }
        throw null;
    }
}
