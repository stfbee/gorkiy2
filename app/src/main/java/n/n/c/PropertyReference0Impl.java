package n.n.c;

import n.p.KDeclarationContainer;
import n.p.KProperty0;

public class PropertyReference0Impl extends PropertyReference0 {

    /* renamed from: e  reason: collision with root package name */
    public final KDeclarationContainer f2785e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2786f;
    public final String g;

    public PropertyReference0Impl(KDeclarationContainer kDeclarationContainer, String str, String str2) {
        this.f2785e = kDeclarationContainer;
        this.f2786f = str;
        this.g = str2;
    }

    public String f() {
        return this.f2786f;
    }

    public KDeclarationContainer g() {
        return this.f2785e;
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [n.p.KProperty0$a, n.p.KCallable] */
    public Object get() {
        return ((KProperty0) h()).a().a(new Object[0]);
    }

    public String i() {
        return this.g;
    }
}
