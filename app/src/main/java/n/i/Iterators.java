package n.i;

import java.util.Iterator;
import n.n.c.t.a;

/* compiled from: Iterators.kt */
public abstract class Iterators implements Iterator<Integer>, a {
    public abstract int b();

    public Object next() {
        return Integer.valueOf(b());
    }

    public void remove() {
        throw new UnsupportedOperationException("Operation is not supported for read-only collection");
    }
}
