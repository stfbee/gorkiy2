package n.l.e.a;

import n.l.Continuation;
import n.l.CoroutineContext;

/* compiled from: ContinuationImpl.kt */
public final class ContinuationImpl0 implements Continuation<Object> {
    public static final ContinuationImpl0 b = new ContinuationImpl0();

    public void a(Object obj) {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    public CoroutineContext c() {
        throw new IllegalStateException("This continuation is already complete".toString());
    }

    public String toString() {
        return "This continuation is already complete";
    }
}
