package n.l.e.a;

import n.l.Continuation;
import n.l.CoroutineContext;
import n.n.c.Intrinsics;

/* compiled from: ContinuationImpl.kt */
public abstract class ContinuationImpl1 extends ContinuationImpl {
    public transient Continuation<Object> c;
    public final CoroutineContext d;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ContinuationImpl1(Continuation<Object> continuation) {
        super(continuation);
        CoroutineContext c2 = continuation != null ? continuation.c() : null;
        this.d = c2;
    }

    public CoroutineContext c() {
        CoroutineContext coroutineContext = this.d;
        if (coroutineContext != null) {
            return coroutineContext;
        }
        Intrinsics.a();
        throw null;
    }
}
