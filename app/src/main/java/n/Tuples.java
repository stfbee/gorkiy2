package n;

import java.io.Serializable;
import n.n.c.Intrinsics;

/* compiled from: Tuples.kt */
public final class Tuples<A, B> implements Serializable {
    public final A b;
    public final B c;

    public Tuples(A a, B b2) {
        this.b = a;
        this.c = b2;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Tuples)) {
            return false;
        }
        Tuples tuples = (Tuples) obj;
        return Intrinsics.a(this.b, tuples.b) && Intrinsics.a(this.c, tuples.c);
    }

    public int hashCode() {
        A a = this.b;
        int i2 = 0;
        int hashCode = (a != null ? a.hashCode() : 0) * 31;
        B b2 = this.c;
        if (b2 != null) {
            i2 = b2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        return '(' + ((Object) this.b) + ", " + ((Object) this.c) + ')';
    }
}
