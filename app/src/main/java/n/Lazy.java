package n;

/* compiled from: Lazy.kt */
public interface Lazy<T> {
    T getValue();
}
