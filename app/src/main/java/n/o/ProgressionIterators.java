package n.o;

import java.util.NoSuchElementException;
import n.i.Iterators;

/* compiled from: ProgressionIterators.kt */
public final class ProgressionIterators extends Iterators {
    public final int b;
    public boolean c;
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public final int f2789e;

    public ProgressionIterators(int i2, int i3, int i4) {
        this.f2789e = i4;
        this.b = i3;
        boolean z = true;
        if (i4 <= 0 ? i2 < i3 : i2 > i3) {
            z = false;
        }
        this.c = z;
        this.d = !z ? this.b : i2;
    }

    public int b() {
        int i2 = this.d;
        if (i2 != this.b) {
            this.d = this.f2789e + i2;
        } else if (this.c) {
            this.c = false;
        } else {
            throw new NoSuchElementException();
        }
        return i2;
    }

    public boolean hasNext() {
        return this.c;
    }
}
