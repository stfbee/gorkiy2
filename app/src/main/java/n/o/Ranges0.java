package n.o;

/* compiled from: Ranges.kt */
public final class Ranges0 extends a {

    /* renamed from: e  reason: collision with root package name */
    public static final Ranges0 f2790e = new Ranges0(1, 0);

    /* renamed from: f  reason: collision with root package name */
    public static final Ranges0 f2791f = null;

    public Ranges0(int i2, int i3) {
        super(i2, i3, 1);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [n.o.Ranges0, n.o.Progressions] */
    public Integer c() {
        return Integer.valueOf(this.b);
    }

    /* JADX WARN: Type inference failed for: r3v3, types: [n.o.Ranges0, n.o.Progressions] */
    public boolean equals(Object obj) {
        if (obj instanceof Ranges0) {
            if (!isEmpty() || !((Ranges0) obj).isEmpty()) {
                ? r3 = (Ranges0) obj;
                if (!(this.b == r3.b && this.c == r3.c)) {
                    return false;
                }
            }
            return true;
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [n.o.Ranges0, n.o.Progressions] */
    public int hashCode() {
        if (isEmpty()) {
            return -1;
        }
        return (this.b * 31) + this.c;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [n.o.Ranges0, n.o.Progressions] */
    public boolean isEmpty() {
        return this.b > this.c;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [n.o.Ranges0, n.o.Progressions] */
    public String toString() {
        return this.b + ".." + this.c;
    }
}
