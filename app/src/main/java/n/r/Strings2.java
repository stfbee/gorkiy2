package n.r;

import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import n.o.Ranges0;
import n.o.c;

/* compiled from: Strings.kt */
public final class Strings2 extends j implements Functions0<c, String> {
    public final /* synthetic */ CharSequence c;

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public Strings2(CharSequence charSequence) {
        super(1);
        this.c = charSequence;
    }

    public Object a(Object obj) {
        Ranges0 ranges0 = (Ranges0) obj;
        if (ranges0 != null) {
            return Indent.a(this.c, ranges0);
        }
        Intrinsics.a("it");
        throw null;
    }
}
