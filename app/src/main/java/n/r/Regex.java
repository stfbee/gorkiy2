package n.r;

import java.io.Serializable;
import java.util.regex.Pattern;
import n.n.c.Intrinsics;

/* compiled from: Regex.kt */
public final class Regex implements Serializable {
    public final Pattern b;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.regex.Pattern, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Regex(String str) {
        if (str != null) {
            Pattern compile = Pattern.compile(str);
            Intrinsics.a((Object) compile, "Pattern.compile(pattern)");
            this.b = compile;
            return;
        }
        Intrinsics.a("pattern");
        throw null;
    }

    public final boolean a(CharSequence charSequence) {
        if (charSequence != null) {
            return this.b.matcher(charSequence).matches();
        }
        Intrinsics.a("input");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String toString() {
        String pattern = this.b.toString();
        Intrinsics.a((Object) pattern, "nativePattern.toString()");
        return pattern;
    }
}
