package n.r;

import j.c.a.a.c.n.c;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import n.i.Collections;
import n.i._Arrays;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.o.Ranges0;
import n.q.Iterables;
import n.q.Sequence;

/* compiled from: Indent.kt */
public class Indent {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
     arg types: [java.lang.String, java.lang.String[], int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean
     arg types: [java.lang.String, java.lang.String, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, char, int, boolean, int):int
      n.r.Indent.a(java.lang.CharSequence, java.lang.String, int, boolean, int):int
      n.r.Indent.a(java.lang.String, java.lang.String, java.lang.String, boolean, int):java.lang.String
      n.r.Indent.a(java.lang.CharSequence, char[], boolean, int, int):java.util.List
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.String, java.lang.String, int, boolean, int):boolean */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static /* synthetic */ String a(String str, String str2, int i2) {
        Object obj;
        String str3;
        String str4;
        String str5 = str;
        String str6 = (i2 & 1) != 0 ? "|" : str2;
        if (str5 == null) {
            Intrinsics.a("$this$trimMargin");
            throw null;
        } else if (str6 == null) {
            Intrinsics.a("marginPrefix");
            throw null;
        } else if (!b(str6)) {
            Sequence<Object> a = a((CharSequence) str5, new String[]{"\r\n", "\n", "\r"}, false, 0, 6);
            if (a != null) {
                ArrayList arrayList = new ArrayList();
                for (Object add : a) {
                    arrayList.add(add);
                }
                List b = Collections.b(arrayList);
                int size = (b.size() * 0) + str.length();
                if ("".length() == 0) {
                    obj = Indent0.c;
                } else {
                    obj = new Indent1("");
                }
                int size2 = b.size() - 1;
                ArrayList arrayList2 = new ArrayList();
                int i3 = 0;
                for (Object next : b) {
                    int i4 = i3 + 1;
                    if (i3 >= 0) {
                        String str7 = (String) next;
                        if ((i3 == 0 || i3 == size2) && b(str7)) {
                            str7 = null;
                        } else {
                            int length = str7.length();
                            int i5 = 0;
                            while (true) {
                                if (i5 >= length) {
                                    i5 = -1;
                                    break;
                                } else if (!Collections.a(str7.charAt(i5))) {
                                    break;
                                } else {
                                    i5++;
                                }
                            }
                            if (i5 != -1 && a(str7, str6, i5, false, 4)) {
                                str3 = str7.substring(str6.length() + i5);
                                Intrinsics.a((Object) str3, "(this as java.lang.String).substring(startIndex)");
                            } else {
                                str3 = null;
                            }
                            if (!(str3 == null || (str4 = (String) obj.a(str3)) == null)) {
                                str7 = str4;
                            }
                        }
                        if (str7 != null) {
                            arrayList2.add(str7);
                        }
                        i3 = i4;
                    } else {
                        Collections.a();
                        throw null;
                    }
                }
                StringBuilder sb = new StringBuilder(size);
                _Arrays.a(arrayList2, sb, "\n", "", "", -1, "...", (Functions0) null);
                String sb2 = sb.toString();
                Intrinsics.a((Object) sb2, "mapIndexedNotNull { inde…\"\\n\")\n        .toString()");
                return sb2;
            }
            Intrinsics.a("$this$toList");
            throw null;
        } else {
            throw new IllegalArgumentException("marginPrefix must be non-blank string.".toString());
        }
    }

    public static /* synthetic */ boolean b(String str, String str2, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        return b(str, str2, z);
    }

    public static final CharSequence c(CharSequence charSequence) {
        if (charSequence != null) {
            int length = charSequence.length() - 1;
            int i2 = 0;
            boolean z = false;
            while (i2 <= length) {
                boolean a = Collections.a(charSequence.charAt(!z ? i2 : length));
                if (!z) {
                    if (!a) {
                        z = true;
                    } else {
                        i2++;
                    }
                } else if (!a) {
                    break;
                } else {
                    length--;
                }
            }
            return charSequence.subSequence(i2, length + 1);
        }
        Intrinsics.a("$this$trim");
        throw null;
    }

    public static final boolean b(String str, String str2, boolean z) {
        if (str == null) {
            Intrinsics.a("$this$startsWith");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("prefix");
            throw null;
        } else if (!z) {
            return str.startsWith(str2);
        } else {
            return a(str, 0, str2, 0, str2.length(), z);
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.o.Ranges0, java.lang.Iterable] */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final boolean b(java.lang.CharSequence r4) {
        /*
            if (r4 == 0) goto L_0x0044
            int r0 = r4.length()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0042
            n.o.Ranges0 r0 = new n.o.Ranges0
            int r3 = r4.length()
            int r3 = r3 + -1
            r0.<init>(r1, r3)
            boolean r3 = r0 instanceof java.util.Collection
            if (r3 == 0) goto L_0x0024
            r3 = r0
            java.util.Collection r3 = (java.util.Collection) r3
            boolean r3 = r3.isEmpty()
            if (r3 == 0) goto L_0x0024
        L_0x0022:
            r4 = 1
            goto L_0x0040
        L_0x0024:
            java.util.Iterator r0 = r0.iterator()
        L_0x0028:
            boolean r3 = r0.hasNext()
            if (r3 == 0) goto L_0x0022
            r3 = r0
            n.i.Iterators r3 = (n.i.Iterators) r3
            int r3 = r3.b()
            char r3 = r4.charAt(r3)
            boolean r3 = n.i.Collections.a(r3)
            if (r3 != 0) goto L_0x0028
            r4 = 0
        L_0x0040:
            if (r4 == 0) goto L_0x0043
        L_0x0042:
            r1 = 1
        L_0x0043:
            return r1
        L_0x0044:
            java.lang.String r4 = "$this$isBlank"
            n.n.c.Intrinsics.a(r4)
            r4 = 0
            throw r4
        */
        throw new UnsupportedOperationException("Method not decompiled: n.r.Indent.b(java.lang.CharSequence):boolean");
    }

    public static /* synthetic */ int b(CharSequence charSequence, char c, int i2, boolean z, int i3) {
        boolean z2;
        if ((i3 & 2) != 0) {
            i2 = a(charSequence);
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if (charSequence == null) {
            Intrinsics.a("$this$lastIndexOf");
            throw null;
        } else if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).lastIndexOf(c, i2);
        } else {
            char[] cArr = {c};
            if (z || !(charSequence instanceof String)) {
                int a = a(charSequence);
                if (i2 > a) {
                    i2 = a;
                }
                while (i2 >= 0) {
                    char charAt = charSequence.charAt(i2);
                    int i4 = 0;
                    while (true) {
                        if (i4 >= 1) {
                            z2 = false;
                            break;
                        } else if (Collections.a(cArr[i4], charAt, z)) {
                            z2 = true;
                            break;
                        } else {
                            i4++;
                        }
                    }
                    if (z2) {
                        return i2;
                    }
                    i2--;
                }
                return -1;
            }
            return ((String) charSequence).lastIndexOf(c.a(cArr), i2);
        }
    }

    public static final boolean a(String str, String str2, boolean z) {
        if (str == null) {
            return str2 == null;
        }
        if (!z) {
            return str.equals(str2);
        }
        return str.equalsIgnoreCase(str2);
    }

    public static /* synthetic */ String a(String str, String str2, String str3, boolean z, int i2) {
        if ((i2 & 4) != 0) {
            z = false;
        }
        if (str == null) {
            Intrinsics.a("$this$replace");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("oldValue");
            throw null;
        } else if (str3 != null) {
            return Collections.a(a(str, new String[]{str2}, z, 0, 4), str3, null, null, 0, null, null, 62);
        } else {
            Intrinsics.a("newValue");
            throw null;
        }
    }

    public static final int a(CharSequence charSequence) {
        if (charSequence != null) {
            return charSequence.length() - 1;
        }
        Intrinsics.a("$this$lastIndex");
        throw null;
    }

    public static /* synthetic */ boolean a(String str, String str2, int i2, boolean z, int i3) {
        if ((i3 & 4) != 0) {
            z = false;
        }
        return a(str, str2, i2, z);
    }

    public static final boolean a(String str, String str2, int i2, boolean z) {
        if (str == null) {
            Intrinsics.a("$this$startsWith");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("prefix");
            throw null;
        } else if (!z) {
            return str.startsWith(str2, i2);
        } else {
            return a(str, i2, str2, 0, str2.length(), z);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.String, int, java.lang.String, int, int, boolean):boolean
     arg types: [java.lang.String, int, java.lang.String, int, int, int]
     candidates:
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, int, int, boolean, boolean):int
      n.r.Indent.a(java.lang.CharSequence, char[], int, boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.CharSequence, java.lang.String[], int, boolean, int, int):n.q.Sequence
      n.r.Indent.a(java.lang.CharSequence, int, java.lang.CharSequence, int, int, boolean):boolean
      n.r.Indent.a(java.lang.String, int, java.lang.String, int, int, boolean):boolean */
    public static /* synthetic */ boolean a(String str, String str2, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if (str == null) {
            Intrinsics.a("$this$endsWith");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("suffix");
            throw null;
        } else if (!z) {
            return str.endsWith(str2);
        } else {
            return a(str, str.length() - str2.length(), str2, 0, str2.length(), true);
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [n.o.Ranges0, n.o.Progressions] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final java.lang.String a(java.lang.CharSequence r1, n.o.Ranges0 r2) {
        /*
            r0 = 0
            if (r1 == 0) goto L_0x0028
            if (r2 == 0) goto L_0x0022
            java.lang.Integer r0 = r2.c()
            int r0 = r0.intValue()
            int r2 = r2.c
            java.lang.Integer r2 = java.lang.Integer.valueOf(r2)
            int r2 = r2.intValue()
            int r2 = r2 + 1
            java.lang.CharSequence r1 = r1.subSequence(r0, r2)
            java.lang.String r1 = r1.toString()
            return r1
        L_0x0022:
            java.lang.String r1 = "range"
            n.n.c.Intrinsics.a(r1)
            throw r0
        L_0x0028:
            java.lang.String r1 = "$this$substring"
            n.n.c.Intrinsics.a(r1)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: n.r.Indent.a(java.lang.CharSequence, n.o.Ranges0):java.lang.String");
    }

    public static final boolean a(String str, int i2, String str2, int i3, int i4, boolean z) {
        if (str == null) {
            Intrinsics.a("$this$regionMatches");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("other");
            throw null;
        } else if (!z) {
            return str.regionMatches(i2, str2, i3, i4);
        } else {
            return str.regionMatches(z, i2, str2, i3, i4);
        }
    }

    public static final boolean a(CharSequence charSequence, int i2, CharSequence charSequence2, int i3, int i4, boolean z) {
        if (charSequence == null) {
            Intrinsics.a("$this$regionMatchesImpl");
            throw null;
        } else if (charSequence2 == null) {
            Intrinsics.a("other");
            throw null;
        } else if (i3 < 0 || i2 < 0 || i2 > charSequence.length() - i4 || i3 > charSequence2.length() - i4) {
            return false;
        } else {
            for (int i5 = 0; i5 < i4; i5++) {
                if (!Collections.a(charSequence.charAt(i2 + i5), charSequence2.charAt(i3 + i5), z)) {
                    return false;
                }
            }
            return true;
        }
    }

    public static final int a(CharSequence charSequence, char[] cArr, int i2, boolean z) {
        boolean z2;
        if (charSequence == null) {
            Intrinsics.a("$this$indexOfAny");
            throw null;
        } else if (cArr == null) {
            Intrinsics.a("chars");
            throw null;
        } else if (z || cArr.length != 1 || !(charSequence instanceof String)) {
            if (i2 < 0) {
                i2 = 0;
            }
            int a = a(charSequence);
            if (i2 > a) {
                return -1;
            }
            while (true) {
                char charAt = charSequence.charAt(i2);
                int length = cArr.length;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        z2 = false;
                        break;
                    } else if (Collections.a(cArr[i3], charAt, z)) {
                        z2 = true;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (z2) {
                    return i2;
                }
                if (i2 == a) {
                    return -1;
                }
                i2++;
            }
        } else {
            return ((String) charSequence).indexOf(c.a(cArr), i2);
        }
    }

    public static /* synthetic */ int a(CharSequence charSequence, CharSequence charSequence2, int i2, int i3, boolean z, boolean z2, int i4) {
        return a(charSequence, charSequence2, i2, i3, z, (i4 & 16) != 0 ? false : z2);
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v1, resolved type: n.o.Progressions} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v6, resolved type: n.o.Ranges0} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v7, resolved type: n.o.Progressions} */
    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r11v8, resolved type: n.o.Progressions} */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final int a(java.lang.CharSequence r6, java.lang.CharSequence r7, int r8, int r9, boolean r10, boolean r11) {
        /*
            r0 = 0
            if (r11 != 0) goto L_0x0013
            if (r8 >= 0) goto L_0x0006
            r8 = 0
        L_0x0006:
            n.o.Ranges0 r11 = new n.o.Ranges0
            int r0 = r6.length()
            if (r9 <= r0) goto L_0x000f
            r9 = r0
        L_0x000f:
            r11.<init>(r8, r9)
            goto L_0x0021
        L_0x0013:
            int r11 = a(r6)
            if (r8 <= r11) goto L_0x001a
            r8 = r11
        L_0x001a:
            if (r9 >= 0) goto L_0x001d
            r9 = 0
        L_0x001d:
            n.o.Progressions r11 = n.o.d.a(r8, r9)
        L_0x0021:
            boolean r8 = r6 instanceof java.lang.String
            if (r8 == 0) goto L_0x004e
            boolean r8 = r7 instanceof java.lang.String
            if (r8 == 0) goto L_0x004e
            int r8 = r11.b
            int r9 = r11.c
            int r11 = r11.d
            if (r11 < 0) goto L_0x0034
            if (r8 > r9) goto L_0x006f
            goto L_0x0036
        L_0x0034:
            if (r8 < r9) goto L_0x006f
        L_0x0036:
            r0 = r7
            java.lang.String r0 = (java.lang.String) r0
            r1 = 0
            r2 = r6
            java.lang.String r2 = (java.lang.String) r2
            int r4 = r7.length()
            r3 = r8
            r5 = r10
            boolean r0 = a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x004a
            return r8
        L_0x004a:
            if (r8 == r9) goto L_0x006f
            int r8 = r8 + r11
            goto L_0x0036
        L_0x004e:
            int r8 = r11.b
            int r9 = r11.c
            int r11 = r11.d
            if (r11 < 0) goto L_0x0059
            if (r8 > r9) goto L_0x006f
            goto L_0x005b
        L_0x0059:
            if (r8 < r9) goto L_0x006f
        L_0x005b:
            r1 = 0
            int r4 = r7.length()
            r0 = r7
            r2 = r6
            r3 = r8
            r5 = r10
            boolean r0 = a(r0, r1, r2, r3, r4, r5)
            if (r0 == 0) goto L_0x006b
            return r8
        L_0x006b:
            if (r8 == r9) goto L_0x006f
            int r8 = r8 + r11
            goto L_0x005b
        L_0x006f:
            r6 = -1
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, int, int, boolean, boolean):int");
    }

    public static /* synthetic */ int a(CharSequence charSequence, char c, int i2, boolean z, int i3) {
        if ((i3 & 2) != 0) {
            i2 = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        if (charSequence == null) {
            Intrinsics.a("$this$indexOf");
            throw null;
        } else if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(c, i2);
        } else {
            return a(charSequence, new char[]{c}, i2, z);
        }
    }

    public static /* synthetic */ int a(CharSequence charSequence, String str, int i2, boolean z, int i3) {
        if ((i3 & 2) != 0) {
            i2 = 0;
        }
        if ((i3 & 4) != 0) {
            z = false;
        }
        return a(charSequence, str, i2, z);
    }

    public static final int a(CharSequence charSequence, String str, int i2, boolean z) {
        if (charSequence == null) {
            Intrinsics.a("$this$indexOf");
            throw null;
        } else if (str == null) {
            Intrinsics.a("string");
            throw null;
        } else if (!z && (charSequence instanceof String)) {
            return ((String) charSequence).indexOf(str, i2);
        } else {
            return a(charSequence, str, i2, charSequence.length(), z, false, 16);
        }
    }

    public static /* synthetic */ boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        return a(charSequence, charSequence2, z);
    }

    public static final boolean a(CharSequence charSequence, CharSequence charSequence2, boolean z) {
        if (charSequence == null) {
            Intrinsics.a("$this$contains");
            throw null;
        } else if (charSequence2 != null) {
            if (!(charSequence2 instanceof String)) {
                if (a(charSequence, charSequence2, 0, charSequence.length(), z, false, 16) >= 0) {
                    return true;
                }
            } else if (a(charSequence, (String) charSequence2, 0, z, 2) >= 0) {
                return true;
            }
            return false;
        } else {
            Intrinsics.a("other");
            throw null;
        }
    }

    public static /* synthetic */ boolean a(CharSequence charSequence, char c, boolean z, int i2) {
        if ((i2 & 2) != 0) {
            z = false;
        }
        if (charSequence != null) {
            return a(charSequence, c, 0, z, 2) >= 0;
        }
        Intrinsics.a("$this$contains");
        throw null;
    }

    public static /* synthetic */ Sequence a(CharSequence charSequence, char[] cArr, int i2, boolean z, int i3, int i4) {
        boolean z2 = false;
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            z = false;
        }
        if ((i4 & 8) != 0) {
            i3 = 0;
        }
        if (i3 >= 0) {
            z2 = true;
        }
        if (z2) {
            return new Strings(charSequence, i2, i3, new Strings0(cArr, z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i3 + '.').toString());
    }

    public static /* synthetic */ Sequence a(CharSequence charSequence, String[] strArr, int i2, boolean z, int i3, int i4) {
        boolean z2 = false;
        if ((i4 & 2) != 0) {
            i2 = 0;
        }
        if ((i4 & 4) != 0) {
            z = false;
        }
        if ((i4 & 8) != 0) {
            i3 = 0;
        }
        if (i3 >= 0) {
            z2 = true;
        }
        if (z2) {
            return new Strings(charSequence, i2, i3, new Strings1(_Arrays.a(strArr), z));
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i3 + '.').toString());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
     arg types: [n.q.Sequence, n.r.Strings2]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R> */
    public static /* synthetic */ Sequence a(CharSequence charSequence, String[] strArr, boolean z, int i2, int i3) {
        boolean z2 = (i3 & 2) != 0 ? false : z;
        int i4 = (i3 & 4) != 0 ? 0 : i2;
        if (charSequence == null) {
            Intrinsics.a("$this$splitToSequence");
            throw null;
        } else if (strArr != null) {
            return Collections.a(a(charSequence, strArr, 0, z2, i4, 2), (Functions0) new Strings2(charSequence));
        } else {
            Intrinsics.a("delimiters");
            throw null;
        }
    }

    public static /* synthetic */ List a(CharSequence charSequence, char[] cArr, boolean z, int i2, int i3) {
        boolean z2 = (i3 & 2) != 0 ? false : z;
        int i4 = (i3 & 4) != 0 ? 0 : i2;
        if (charSequence == null) {
            Intrinsics.a("$this$split");
            throw null;
        } else if (cArr == null) {
            Intrinsics.a("delimiters");
            throw null;
        } else if (cArr.length == 1) {
            return a(charSequence, String.valueOf(cArr[0]), z2, i4);
        } else {
            Iterables iterables = new Iterables(a(charSequence, cArr, 0, z2, i4, 2));
            ArrayList arrayList = new ArrayList(Collections.a(iterables, 10));
            Iterator it = iterables.iterator();
            while (it.hasNext()) {
                arrayList.add(a(charSequence, (Ranges0) it.next()));
            }
            return arrayList;
        }
    }

    public static final List<String> a(CharSequence charSequence, String str, boolean z, int i2) {
        int i3 = 0;
        if (i2 >= 0) {
            int a = a(charSequence, str, 0, z);
            if (a == -1 || i2 == 1) {
                return c.c((Object) charSequence.toString());
            }
            boolean z2 = i2 > 0;
            int i4 = 10;
            if (z2 && i2 <= 10) {
                i4 = i2;
            }
            ArrayList arrayList = new ArrayList(i4);
            do {
                arrayList.add(charSequence.subSequence(i3, a).toString());
                i3 = str.length() + a;
                if (z2 && arrayList.size() == i2 - 1) {
                    break;
                }
                a = a(charSequence, str, i3, z);
            } while (a != -1);
            arrayList.add(charSequence.subSequence(i3, charSequence.length()).toString());
            return arrayList;
        }
        throw new IllegalArgumentException(("Limit must be non-negative, but was " + i2 + '.').toString());
    }

    public static final String a(String str, char... cArr) {
        Object obj;
        if (str == null) {
            Intrinsics.a("$this$trimEnd");
            throw null;
        } else if (cArr != null) {
            int length = str.length() - 1;
            while (true) {
                if (length < 0) {
                    obj = "";
                    break;
                }
                char charAt = str.charAt(length);
                int length2 = cArr.length;
                int i2 = 0;
                while (true) {
                    if (i2 >= length2) {
                        i2 = -1;
                        break;
                    } else if (charAt == cArr[i2]) {
                        break;
                    } else {
                        i2++;
                    }
                }
                if (!(i2 >= 0)) {
                    obj = str.subSequence(0, length + 1);
                    break;
                }
                length--;
            }
            return obj.toString();
        } else {
            Intrinsics.a("chars");
            throw null;
        }
    }
}
