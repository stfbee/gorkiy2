package ru.covid19.droid.data.model.profileData;

import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: Children.kt */
public final class Children {
    public Boolean addressSimilarWithPassenger;
    public int birthDate;
    public String citizenship;
    public String citizenshipName;
    public Document document;
    public String fullName;
    public ProfileData0 gender;
    public String placementAddress;
    public String registrationAddress;
    public Boolean sympthoms;

    public Children() {
        this(null, 0, null, null, null, null, null, null, null, null, 1023, null);
    }

    public Children(Boolean bool, int i2, String str, Document document2, String str2, ProfileData0 profileData0, String str3, String str4, Boolean bool2, String str5) {
        if (str == null) {
            Intrinsics.a("citizenship");
            throw null;
        } else if (document2 == null) {
            Intrinsics.a("document");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("placementAddress");
            throw null;
        } else if (str4 != null) {
            this.addressSimilarWithPassenger = bool;
            this.birthDate = i2;
            this.citizenship = str;
            this.document = document2;
            this.fullName = str2;
            this.gender = profileData0;
            this.placementAddress = str3;
            this.registrationAddress = str4;
            this.sympthoms = bool2;
            this.citizenshipName = str5;
        } else {
            Intrinsics.a("registrationAddress");
            throw null;
        }
    }

    public static /* synthetic */ Children copy$default(Children children, Boolean bool, int i2, String str, Document document2, String str2, ProfileData0 profileData0, String str3, String str4, Boolean bool2, String str5, int i3, Object obj) {
        Children children2 = children;
        int i4 = i3;
        return children.copy((i4 & 1) != 0 ? children2.addressSimilarWithPassenger : bool, (i4 & 2) != 0 ? children2.birthDate : i2, (i4 & 4) != 0 ? children2.citizenship : str, (i4 & 8) != 0 ? children2.document : document2, (i4 & 16) != 0 ? children2.fullName : str2, (i4 & 32) != 0 ? children2.gender : profileData0, (i4 & 64) != 0 ? children2.placementAddress : str3, (i4 & 128) != 0 ? children2.registrationAddress : str4, (i4 & 256) != 0 ? children2.sympthoms : bool2, (i4 & 512) != 0 ? children2.citizenshipName : str5);
    }

    public final Boolean component1() {
        return this.addressSimilarWithPassenger;
    }

    public final String component10() {
        return this.citizenshipName;
    }

    public final int component2() {
        return this.birthDate;
    }

    public final String component3() {
        return this.citizenship;
    }

    public final Document component4() {
        return this.document;
    }

    public final String component5() {
        return this.fullName;
    }

    public final ProfileData0 component6() {
        return this.gender;
    }

    public final String component7() {
        return this.placementAddress;
    }

    public final String component8() {
        return this.registrationAddress;
    }

    public final Boolean component9() {
        return this.sympthoms;
    }

    public final Children copy(Boolean bool, int i2, String str, Document document2, String str2, ProfileData0 profileData0, String str3, String str4, Boolean bool2, String str5) {
        if (str == null) {
            Intrinsics.a("citizenship");
            throw null;
        } else if (document2 == null) {
            Intrinsics.a("document");
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("fullName");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("placementAddress");
            throw null;
        } else if (str4 != null) {
            return new Children(bool, i2, str, document2, str2, profileData0, str3, str4, bool2, str5);
        } else {
            Intrinsics.a("registrationAddress");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Children)) {
            return false;
        }
        Children children = (Children) obj;
        return Intrinsics.a(this.addressSimilarWithPassenger, children.addressSimilarWithPassenger) && this.birthDate == children.birthDate && Intrinsics.a(this.citizenship, children.citizenship) && Intrinsics.a(this.document, children.document) && Intrinsics.a(this.fullName, children.fullName) && Intrinsics.a(this.gender, children.gender) && Intrinsics.a(this.placementAddress, children.placementAddress) && Intrinsics.a(this.registrationAddress, children.registrationAddress) && Intrinsics.a(this.sympthoms, children.sympthoms) && Intrinsics.a(this.citizenshipName, children.citizenshipName);
    }

    public final Boolean getAddressSimilarWithPassenger() {
        return this.addressSimilarWithPassenger;
    }

    public final int getBirthDate() {
        return this.birthDate;
    }

    public final String getCitizenship() {
        return this.citizenship;
    }

    public final String getCitizenshipName() {
        return this.citizenshipName;
    }

    public final Document getDocument() {
        return this.document;
    }

    public final String getFullName() {
        return this.fullName;
    }

    public final ProfileData0 getGender() {
        return this.gender;
    }

    public final String getPlacementAddress() {
        return this.placementAddress;
    }

    public final String getRegistrationAddress() {
        return this.registrationAddress;
    }

    public final Boolean getSympthoms() {
        return this.sympthoms;
    }

    public int hashCode() {
        Boolean bool = this.addressSimilarWithPassenger;
        int i2 = 0;
        int hashCode = (((bool != null ? bool.hashCode() : 0) * 31) + this.birthDate) * 31;
        String str = this.citizenship;
        int hashCode2 = (hashCode + (str != null ? str.hashCode() : 0)) * 31;
        Document document2 = this.document;
        int hashCode3 = (hashCode2 + (document2 != null ? document2.hashCode() : 0)) * 31;
        String str2 = this.fullName;
        int hashCode4 = (hashCode3 + (str2 != null ? str2.hashCode() : 0)) * 31;
        ProfileData0 profileData0 = this.gender;
        int hashCode5 = (hashCode4 + (profileData0 != null ? profileData0.hashCode() : 0)) * 31;
        String str3 = this.placementAddress;
        int hashCode6 = (hashCode5 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.registrationAddress;
        int hashCode7 = (hashCode6 + (str4 != null ? str4.hashCode() : 0)) * 31;
        Boolean bool2 = this.sympthoms;
        int hashCode8 = (hashCode7 + (bool2 != null ? bool2.hashCode() : 0)) * 31;
        String str5 = this.citizenshipName;
        if (str5 != null) {
            i2 = str5.hashCode();
        }
        return hashCode8 + i2;
    }

    public final void setAddressSimilarWithPassenger(Boolean bool) {
        this.addressSimilarWithPassenger = bool;
    }

    public final void setBirthDate(int i2) {
        this.birthDate = i2;
    }

    public final void setCitizenship(String str) {
        if (str != null) {
            this.citizenship = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setCitizenshipName(String str) {
        this.citizenshipName = str;
    }

    public final void setDocument(Document document2) {
        if (document2 != null) {
            this.document = document2;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setFullName(String str) {
        if (str != null) {
            this.fullName = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setGender(ProfileData0 profileData0) {
        this.gender = profileData0;
    }

    public final void setPlacementAddress(String str) {
        if (str != null) {
            this.placementAddress = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setRegistrationAddress(String str) {
        if (str != null) {
            this.registrationAddress = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setSympthoms(Boolean bool) {
        this.sympthoms = bool;
    }

    public String toString() {
        StringBuilder a = outline.a("Children(addressSimilarWithPassenger=");
        a.append(this.addressSimilarWithPassenger);
        a.append(", birthDate=");
        a.append(this.birthDate);
        a.append(", citizenship=");
        a.append(this.citizenship);
        a.append(", document=");
        a.append(this.document);
        a.append(", fullName=");
        a.append(this.fullName);
        a.append(", gender=");
        a.append(this.gender);
        a.append(", placementAddress=");
        a.append(this.placementAddress);
        a.append(", registrationAddress=");
        a.append(this.registrationAddress);
        a.append(", sympthoms=");
        a.append(this.sympthoms);
        a.append(", citizenshipName=");
        return outline.a(a, this.citizenshipName, ")");
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public /* synthetic */ Children(java.lang.Boolean r14, int r15, java.lang.String r16, ru.covid19.droid.data.model.profileData.Document r17, java.lang.String r18, ru.covid19.droid.data.model.profileData.ProfileData0 r19, java.lang.String r20, java.lang.String r21, java.lang.Boolean r22, java.lang.String r23, int r24, n.n.c.DefaultConstructorMarker r25) {
        /*
            r13 = this;
            r0 = r24
            r1 = r0 & 1
            r2 = 0
            if (r1 == 0) goto L_0x0009
            r1 = r2
            goto L_0x000a
        L_0x0009:
            r1 = r14
        L_0x000a:
            r3 = r0 & 2
            if (r3 == 0) goto L_0x0010
            r3 = 0
            goto L_0x0011
        L_0x0010:
            r3 = r15
        L_0x0011:
            r4 = r0 & 4
            java.lang.String r5 = ""
            if (r4 == 0) goto L_0x0019
            r4 = r5
            goto L_0x001b
        L_0x0019:
            r4 = r16
        L_0x001b:
            r6 = r0 & 8
            if (r6 == 0) goto L_0x002b
            ru.covid19.droid.data.model.profileData.Document r6 = new ru.covid19.droid.data.model.profileData.Document
            r8 = 0
            r9 = 0
            r10 = 0
            r11 = 7
            r12 = 0
            r7 = r6
            r7.<init>(r8, r9, r10, r11, r12)
            goto L_0x002d
        L_0x002b:
            r6 = r17
        L_0x002d:
            r7 = r0 & 16
            if (r7 == 0) goto L_0x0033
            r7 = r5
            goto L_0x0035
        L_0x0033:
            r7 = r18
        L_0x0035:
            r8 = r0 & 32
            if (r8 == 0) goto L_0x003b
            r8 = r2
            goto L_0x003d
        L_0x003b:
            r8 = r19
        L_0x003d:
            r9 = r0 & 64
            if (r9 == 0) goto L_0x0043
            r9 = r5
            goto L_0x0045
        L_0x0043:
            r9 = r20
        L_0x0045:
            r10 = r0 & 128(0x80, float:1.794E-43)
            if (r10 == 0) goto L_0x004b
            r10 = r5
            goto L_0x004d
        L_0x004b:
            r10 = r21
        L_0x004d:
            r11 = r0 & 256(0x100, float:3.59E-43)
            if (r11 == 0) goto L_0x0052
            goto L_0x0054
        L_0x0052:
            r2 = r22
        L_0x0054:
            r0 = r0 & 512(0x200, float:7.175E-43)
            if (r0 == 0) goto L_0x0059
            goto L_0x005b
        L_0x0059:
            r5 = r23
        L_0x005b:
            r14 = r13
            r15 = r1
            r16 = r3
            r17 = r4
            r18 = r6
            r19 = r7
            r20 = r8
            r21 = r9
            r22 = r10
            r23 = r2
            r24 = r5
            r14.<init>(r15, r16, r17, r18, r19, r20, r21, r22, r23, r24)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.data.model.profileData.Children.<init>(java.lang.Boolean, int, java.lang.String, ru.covid19.droid.data.model.profileData.Document, java.lang.String, ru.covid19.droid.data.model.profileData.ProfileData0, java.lang.String, java.lang.String, java.lang.Boolean, java.lang.String, int, n.n.c.DefaultConstructorMarker):void");
    }
}
