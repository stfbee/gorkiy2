package ru.covid19.droid.data.model.profileData;

import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: Document.kt */
public final class Document {
    public String number;
    public String series;
    public Document0 type;

    public Document() {
        this(null, null, null, 7, null);
    }

    public Document(String str, String str2, Document0 document0) {
        if (str == null) {
            Intrinsics.a("number");
            throw null;
        } else if (str2 != null) {
            this.number = str;
            this.series = str2;
            this.type = document0;
        } else {
            Intrinsics.a("series");
            throw null;
        }
    }

    public static /* synthetic */ Document copy$default(Document document, String str, String str2, Document0 document0, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = document.number;
        }
        if ((i2 & 2) != 0) {
            str2 = document.series;
        }
        if ((i2 & 4) != 0) {
            document0 = document.type;
        }
        return document.copy(str, str2, document0);
    }

    public final String component1() {
        return this.number;
    }

    public final String component2() {
        return this.series;
    }

    public final Document0 component3() {
        return this.type;
    }

    public final Document copy(String str, String str2, Document0 document0) {
        if (str == null) {
            Intrinsics.a("number");
            throw null;
        } else if (str2 != null) {
            return new Document(str, str2, document0);
        } else {
            Intrinsics.a("series");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Document)) {
            return false;
        }
        Document document = (Document) obj;
        return Intrinsics.a(this.number, document.number) && Intrinsics.a(this.series, document.series) && Intrinsics.a(this.type, document.type);
    }

    public final String getNumber() {
        return this.number;
    }

    public final String getSeries() {
        return this.series;
    }

    public final Document0 getType() {
        return this.type;
    }

    public int hashCode() {
        String str = this.number;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.series;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        Document0 document0 = this.type;
        if (document0 != null) {
            i2 = document0.hashCode();
        }
        return hashCode2 + i2;
    }

    public final void setNumber(String str) {
        if (str != null) {
            this.number = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setSeries(String str) {
        if (str != null) {
            this.series = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    public final void setType(Document0 document0) {
        this.type = document0;
    }

    public String toString() {
        StringBuilder a = outline.a("Document(number=");
        a.append(this.number);
        a.append(", series=");
        a.append(this.series);
        a.append(", type=");
        a.append(this.type);
        a.append(")");
        return a.toString();
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ Document(String str, String str2, Document0 document0, int i2, DefaultConstructorMarker defaultConstructorMarker) {
        this((i2 & 1) != 0 ? "" : str, (i2 & 2) != 0 ? "" : str2, (i2 & 4) != 0 ? null : document0);
    }
}
