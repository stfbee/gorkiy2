package ru.covid19.droid.presentation.main.profileFilling.addChild;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.TextView;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.google.android.material.button.MaterialButton;
import com.google.android.material.snackbar.Snackbar;
import com.google.android.material.textfield.TextInputEditText;
import com.minsvyaz.gosuslugi.stopcorona.R;
import e.a.a.a.b.BaseEvents;
import e.a.a.a.b.BaseEvents1;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.b.f.a.DaggerMainComponent;
import e.a.b.f.a.MainComponent;
import e.a.b.f.b.AppComponentsHolder;
import e.a.b.f.c.AddProfileStepsNavigationModule;
import e.a.b.f.c.MainModule;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState0;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState2;
import e.a.b.h.b.h.l.ProfileFillingAddChildFragmentVm;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState;
import e.a.b.h.b.h.o.TransportHealthStepViewState;
import e.c.c.BaseViewState2;
import j.a.a.a.outline;
import j.e.a.InitialValueObservable;
import j.e.a.b.AnyToUnit;
import j.e.a.d.RadioGroupCheckedChangeObservable;
import j.e.b.PublishRelay;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import q.b.a.Instant;
import q.b.a.ZoneId;
import q.b.a.ZonedDateTime;
import q.b.a.t.DateTimeFormatter;
import ru.covid19.core.presentation.base.BaseDpFragment;
import ru.covid19.droid.data.model.ProfileFillingBottomSheetDto;
import ru.covid19.droid.data.model.profileData.Children;
import ru.covid19.droid.data.model.profileData.Document;
import ru.covid19.droid.data.model.profileData.Document0;

/* compiled from: ProfileFillingAddChildFragment.kt */
public final class ProfileFillingAddChildFragment extends BaseDpFragment<e.a.b.h.b.h.l.b, e.a.b.h.b.h.l.c> {
    public HashMap d0;

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class a<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragment a;

        public a(ProfileFillingAddChildFragment profileFillingAddChildFragment) {
            this.a = profileFillingAddChildFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            Children children;
            Document document;
            Children children2;
            Document document2;
            Children children3;
            Document document3;
            Children children4;
            Document document4;
            Document0 document0 = null;
            if (((Unit) obj) != null) {
                String a2 = this.a.a((int) R.string.frag_filling_profile_document_type);
                Intrinsics.a((Object) a2, "getString(R.string.frag_…ng_profile_document_type)");
                DocumentsStepFragmentViewState[] documentsStepFragmentViewStateArr = new DocumentsStepFragmentViewState[4];
                Document0 document02 = Document0.russianPassport;
                String a3 = this.a.a((int) R.string.frag_filling_profile_documents_movement_passport);
                Intrinsics.a((Object) a3, "getString(R.string.frag_…uments_movement_passport)");
                ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState = (ProfileFillingAddChildFragmentViewState) ((ProfileFillingAddChildFragmentVm) this.a.h()).d.c();
                boolean z = true;
                documentsStepFragmentViewStateArr[0] = new DocumentsStepFragmentViewState(document02, a3, ((profileFillingAddChildFragmentViewState == null || (children4 = profileFillingAddChildFragmentViewState.c) == null || (document4 = children4.getDocument()) == null) ? null : document4.getType()) == Document0.russianPassport);
                Document0 document03 = Document0.birthCertificate;
                String a4 = this.a.a((int) R.string.frag_filling_profile_documents_movement_birth_certificate);
                Intrinsics.a((Object) a4, "getString(R.string.frag_…vement_birth_certificate)");
                ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState2 = (ProfileFillingAddChildFragmentViewState) ((ProfileFillingAddChildFragmentVm) this.a.h()).d.c();
                documentsStepFragmentViewStateArr[1] = new DocumentsStepFragmentViewState(document03, a4, ((profileFillingAddChildFragmentViewState2 == null || (children3 = profileFillingAddChildFragmentViewState2.c) == null || (document3 = children3.getDocument()) == null) ? null : document3.getType()) == Document0.birthCertificate);
                Document0 document04 = Document0.internationalPassport;
                String a5 = this.a.a((int) R.string.frag_filling_profile_documents_movement_frgn_passport);
                Intrinsics.a((Object) a5, "getString(R.string.frag_…s_movement_frgn_passport)");
                ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState3 = (ProfileFillingAddChildFragmentViewState) ((ProfileFillingAddChildFragmentVm) this.a.h()).d.c();
                documentsStepFragmentViewStateArr[2] = new DocumentsStepFragmentViewState(document04, a5, ((profileFillingAddChildFragmentViewState3 == null || (children2 = profileFillingAddChildFragmentViewState3.c) == null || (document2 = children2.getDocument()) == null) ? null : document2.getType()) == Document0.internationalPassport);
                Document0 document05 = Document0.foreignCitizenDoc;
                String a6 = this.a.a((int) R.string.frag_filling_profile_documents_movement_fid);
                Intrinsics.a((Object) a6, "getString(R.string.frag_…e_documents_movement_fid)");
                ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState4 = (ProfileFillingAddChildFragmentViewState) ((ProfileFillingAddChildFragmentVm) this.a.h()).d.c();
                if (!(profileFillingAddChildFragmentViewState4 == null || (children = profileFillingAddChildFragmentViewState4.c) == null || (document = children.getDocument()) == null)) {
                    document0 = document.getType();
                }
                if (document0 != Document0.foreignCitizenDoc) {
                    z = false;
                }
                documentsStepFragmentViewStateArr[3] = new DocumentsStepFragmentViewState(document05, a6, z);
                return new ProfileFillingAddChildFragmentViewState0.p(a2, Collections.a((Object[]) documentsStepFragmentViewStateArr));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.i(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.h(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class d<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingAddChildFragment a;

        public d(ProfileFillingAddChildFragment profileFillingAddChildFragment) {
            this.a = profileFillingAddChildFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            Children children;
            Children children2;
            Boolean bool = null;
            if (((Unit) obj) != null) {
                String a2 = this.a.a((int) R.string.frag_filling_profile_symptoms);
                Intrinsics.a((Object) a2, "getString(R.string.frag_filling_profile_symptoms)");
                TransportHealthStepViewState[] transportHealthStepViewStateArr = new TransportHealthStepViewState[2];
                String a3 = this.a.a((int) R.string.frag_filling_profile_radio_button_yes);
                Intrinsics.a((Object) a3, "getString(R.string.frag_…profile_radio_button_yes)");
                ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState = (ProfileFillingAddChildFragmentViewState) ((ProfileFillingAddChildFragmentVm) this.a.h()).d.c();
                transportHealthStepViewStateArr[0] = new TransportHealthStepViewState(true, a3, Intrinsics.a((Object) ((profileFillingAddChildFragmentViewState == null || (children2 = profileFillingAddChildFragmentViewState.c) == null) ? null : children2.getSympthoms()), (Object) true));
                String a4 = this.a.a((int) R.string.frag_filling_profile_radio_button_no);
                Intrinsics.a((Object) a4, "getString(R.string.frag_…_profile_radio_button_no)");
                ProfileFillingAddChildFragmentViewState profileFillingAddChildFragmentViewState2 = (ProfileFillingAddChildFragmentViewState) ((ProfileFillingAddChildFragmentVm) this.a.h()).d.c();
                if (!(profileFillingAddChildFragmentViewState2 == null || (children = profileFillingAddChildFragmentViewState2.c) == null)) {
                    bool = children.getSympthoms();
                }
                transportHealthStepViewStateArr[1] = new TransportHealthStepViewState(false, a4, Intrinsics.a((Object) bool, (Object) false));
                return new ProfileFillingAddChildFragmentViewState0.s(a2, Collections.a((Object[]) transportHealthStepViewStateArr));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ProfileFillingAddChildFragmentViewState0.r.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class f<T, R> implements Function<T, R> {
        public static final f a = new f();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return BaseEvents1.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class g<T, R> implements Function<T, R> {
        public static final g a = new g();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ProfileFillingAddChildFragmentViewState0.q.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class h<T, R> implements Function<T, R> {
        public static final h a = new h();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ProfileFillingAddChildFragmentViewState0.d.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class i<T, R> implements Function<T, R> {
        public static final i a = new i();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.n(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class j<T, R> implements Function<T, R> {
        public static final j a = new j();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.k(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class k<T, R> implements Function<T, R> {
        public static final k a = new k();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.l(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class l<T, R> implements Function<T, R> {
        public static final l a = new l();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.m(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class m<T, R> implements Function<T, R> {
        public static final m a = new m();

        public Object a(Object obj) {
            CharSequence charSequence = (CharSequence) obj;
            if (charSequence != null) {
                return new ProfileFillingAddChildFragmentViewState0.j(charSequence.toString());
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class n<T, R> implements Function<T, R> {
        public static final n a = new n();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return BaseEvents.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class o<T, R> implements Function<T, R> {
        public static final o a = new o();

        public Object a(Object obj) {
            Integer num = (Integer) obj;
            boolean z = null;
            if (num != null) {
                if (num.intValue() == R.id.frag_profile_filling_add_child_rb_same_address_yes) {
                    z = true;
                } else if (num.intValue() == R.id.frag_profile_filling_add_child_rb_same_address_no) {
                    z = false;
                }
                return new ProfileFillingAddChildFragmentViewState0.e(z);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingAddChildFragment.kt */
    public static final class p extends n.n.c.j implements Functions0<Date, n.g> {
        public final /* synthetic */ ProfileFillingAddChildFragment c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public p(ProfileFillingAddChildFragment profileFillingAddChildFragment) {
            super(1);
            this.c = profileFillingAddChildFragment;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.util.Locale, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [q.b.a.ZoneId, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Object, java.lang.String):T
         arg types: [q.b.a.Instant, java.lang.String]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Object, java.lang.String):T */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.i.Collections.a(java.lang.Object, java.lang.String):T
         arg types: [q.b.a.ZoneId, java.lang.String]
         candidates:
          n.i.Collections.a(int, int):int
          n.i.Collections.a(long, int):int
          n.i.Collections.a(long, long):int
          n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
          n.i.Collections.a(java.lang.Iterable, int):int
          n.i.Collections.a(byte[], int):int
          n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
          n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
          n.i.Collections.a(java.lang.String, int):java.lang.String
          n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
          n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
          n.i.Collections.a(android.view.View, boolean):void
          n.i.Collections.a(android.widget.CompoundButton, boolean):void
          n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
          n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
          n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
          n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
          n.i.Collections.a(java.lang.Object, java.lang.String):T */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public Object a(Object obj) {
            Date date = (Date) obj;
            if (date != null) {
                PublishRelay<e.c.c.b> publishRelay = this.c.c0;
                Locale locale = Locale.getDefault();
                Intrinsics.a((Object) locale, "Locale.getDefault()");
                ZoneId i2 = ZoneId.i();
                Intrinsics.a((Object) i2, "ZoneId.systemDefault()");
                Instant b = Instant.b(date.getTime());
                if (b != null) {
                    Collections.a((Object) b, "instant");
                    Collections.a((Object) i2, "zone");
                    String a = ZonedDateTime.a(b.b, b.c, i2).b.a(DateTimeFormatter.a("dd.MM.yyyy", locale));
                    Intrinsics.a((Object) a, "localDateTime.format(Dat…Pattern(pattern, locale))");
                    publishRelay.a((e.c.c.b) new ProfileFillingAddChildFragmentViewState0.f(a));
                    return Unit.a;
                }
                throw null;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e.a.b.h.b.h.l.c> P() {
        return ProfileFillingAddChildFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputLayout, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
     arg types: [java.lang.Boolean, boolean]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x00e6  */
    /* JADX WARNING: Removed duplicated region for block: B:29:0x01aa  */
    /* JADX WARNING: Removed duplicated region for block: B:30:0x01b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void a(java.lang.Object r7) {
        /*
            r6 = this;
            e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState r7 = (e.a.b.h.b.h.l.ProfileFillingAddChildFragmentViewState) r7
            r0 = 0
            if (r7 == 0) goto L_0x01d0
            boolean r1 = r7.b
            r2 = 0
            r3 = 1
            if (r1 == 0) goto L_0x00a8
            int r1 = e.a.b.b.rag_profile_filling_add_child_ed_registration_address
            android.view.View r1 = r6.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Children r4 = r7.c
            java.lang.String r4 = r4.getRegistrationAddress()
            r1.setText(r4)
            int r1 = e.a.b.b.rag_profile_filling_add_child_ed_living_place
            android.view.View r1 = r6.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Children r4 = r7.c
            java.lang.String r4 = r4.getPlacementAddress()
            r1.setText(r4)
            int r1 = e.a.b.b.frag_profile_filling_add_child_ed_birthday
            android.view.View r1 = r6.c(r1)
            com.google.android.material.textfield.TextInputEditText r1 = (com.google.android.material.textfield.TextInputEditText) r1
            ru.covid19.droid.data.model.profileData.Children r4 = r7.c
            int r4 = r4.getBirthDate()
            java.lang.String r0 = n.i.Collections.a(r4, r0, r3)
            r1.setText(r0)
            int r0 = e.a.b.b.rag_profile_filling_add_child_til_registration_address
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            java.lang.String r1 = "rag_profile_filling_add_…_til_registration_address"
            n.n.c.Intrinsics.a(r0, r1)
            ru.covid19.droid.data.model.profileData.Children r1 = r7.c
            java.lang.Boolean r1 = r1.getAddressSimilarWithPassenger()
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r2)
            boolean r1 = n.n.c.Intrinsics.a(r1, r4)
            r0.setEnabled(r1)
            int r0 = e.a.b.b.rag_profile_filling_add_child_til_living_place
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            java.lang.String r1 = "rag_profile_filling_add_child_til_living_place"
            n.n.c.Intrinsics.a(r0, r1)
            ru.covid19.droid.data.model.profileData.Children r1 = r7.c
            java.lang.Boolean r1 = r1.getAddressSimilarWithPassenger()
            java.lang.Boolean r4 = java.lang.Boolean.valueOf(r2)
            boolean r1 = n.n.c.Intrinsics.a(r1, r4)
            r0.setEnabled(r1)
            int r0 = e.a.b.b.frag_profile_filling_add_child_et_document_series
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            ru.covid19.droid.data.model.profileData.Children r1 = r7.c
            ru.covid19.droid.data.model.profileData.Document r1 = r1.getDocument()
            java.lang.String r1 = r1.getSeries()
            r0.setText(r1)
            int r0 = e.a.b.b.frag_profile_filling_add_child_et_document_number
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            ru.covid19.droid.data.model.profileData.Children r1 = r7.c
            ru.covid19.droid.data.model.profileData.Document r1 = r1.getDocument()
            java.lang.String r1 = r1.getNumber()
            r0.setText(r1)
        L_0x00a8:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_gender
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            ru.covid19.droid.data.model.profileData.Children r1 = r7.c
            ru.covid19.droid.data.model.profileData.ProfileData0 r1 = r1.getGender()
            java.lang.String r4 = ""
            if (r1 != 0) goto L_0x00bb
            goto L_0x00c3
        L_0x00bb:
            int r1 = r1.ordinal()
            if (r1 == 0) goto L_0x00cd
            if (r1 == r3) goto L_0x00c5
        L_0x00c3:
            r1 = r4
            goto L_0x00d4
        L_0x00c5:
            r1 = 2131623973(0x7f0e0025, float:1.8875113E38)
            java.lang.String r1 = r6.a(r1)
            goto L_0x00d4
        L_0x00cd:
            r1 = 2131623974(0x7f0e0026, float:1.8875115E38)
            java.lang.String r1 = r6.a(r1)
        L_0x00d4:
            r0.setText(r1)
            ru.covid19.droid.data.model.profileData.Children r0 = r7.c
            ru.covid19.droid.data.model.profileData.Document r0 = r0.getDocument()
            ru.covid19.droid.data.model.profileData.Document0 r0 = r0.getType()
            java.lang.String r1 = "frag_profile_filling_add_child_til_document_series"
            if (r0 != 0) goto L_0x00e6
            goto L_0x00f4
        L_0x00e6:
            int r0 = r0.ordinal()
            if (r0 == 0) goto L_0x0165
            if (r0 == r3) goto L_0x0148
            r5 = 2
            if (r0 == r5) goto L_0x012b
            r5 = 3
            if (r0 == r5) goto L_0x010e
        L_0x00f4:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_document_type
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            r0.setText(r4)
            int r0 = e.a.b.b.frag_profile_filling_add_child_til_document_series
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            n.n.c.Intrinsics.a(r0, r1)
            r0.setEnabled(r3)
            goto L_0x0181
        L_0x010e:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_document_type
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            r5 = 2131624052(0x7f0e0074, float:1.8875273E38)
            r0.setText(r5)
            int r0 = e.a.b.b.frag_profile_filling_add_child_til_document_series
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            n.n.c.Intrinsics.a(r0, r1)
            r0.setEnabled(r3)
            goto L_0x0181
        L_0x012b:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_document_type
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            r5 = 2131624055(0x7f0e0077, float:1.8875279E38)
            r0.setText(r5)
            int r0 = e.a.b.b.frag_profile_filling_add_child_til_document_series
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            n.n.c.Intrinsics.a(r0, r1)
            r0.setEnabled(r2)
            goto L_0x0181
        L_0x0148:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_document_type
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            r5 = 2131624056(0x7f0e0078, float:1.887528E38)
            r0.setText(r5)
            int r0 = e.a.b.b.frag_profile_filling_add_child_til_document_series
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            n.n.c.Intrinsics.a(r0, r1)
            r0.setEnabled(r3)
            goto L_0x0181
        L_0x0165:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_document_type
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            r5 = 2131624057(0x7f0e0079, float:1.8875283E38)
            r0.setText(r5)
            int r0 = e.a.b.b.frag_profile_filling_add_child_til_document_series
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputLayout r0 = (com.google.android.material.textfield.TextInputLayout) r0
            n.n.c.Intrinsics.a(r0, r1)
            r0.setEnabled(r3)
        L_0x0181:
            int r0 = e.a.b.b.frag_profile_filling_add_child_ed_citizenship
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            ru.covid19.droid.data.model.profileData.Children r1 = r7.c
            java.lang.String r1 = r1.getCitizenshipName()
            r0.setText(r1)
            int r0 = e.a.b.b.frag_profile_filling_add_child_et_symptoms_step_answer
            android.view.View r0 = r6.c(r0)
            com.google.android.material.textfield.TextInputEditText r0 = (com.google.android.material.textfield.TextInputEditText) r0
            ru.covid19.droid.data.model.profileData.Children r7 = r7.c
            java.lang.Boolean r7 = r7.getSympthoms()
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r3)
            boolean r1 = n.n.c.Intrinsics.a(r7, r1)
            if (r1 == 0) goto L_0x01b2
            r7 = 2131624071(0x7f0e0087, float:1.8875311E38)
            java.lang.String r4 = r6.a(r7)
            goto L_0x01c6
        L_0x01b2:
            java.lang.Boolean r1 = java.lang.Boolean.valueOf(r2)
            boolean r1 = n.n.c.Intrinsics.a(r7, r1)
            if (r1 == 0) goto L_0x01c4
            r7 = 2131624070(0x7f0e0086, float:1.887531E38)
            java.lang.String r4 = r6.a(r7)
            goto L_0x01c6
        L_0x01c4:
            if (r7 != 0) goto L_0x01ca
        L_0x01c6:
            r0.setText(r4)
            return
        L_0x01ca:
            kotlin.NoWhenBranchMatchedException r7 = new kotlin.NoWhenBranchMatchedException
            r7.<init>()
            throw r7
        L_0x01d0:
            java.lang.String r7 = "vs"
            n.n.c.Intrinsics.a(r7)
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.droid.presentation.main.profileFilling.addChild.ProfileFillingAddChildFragment.a(java.lang.Object):void");
    }

    public void b(Bundle bundle) {
        if (AppComponentsHolder.b == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent == null) {
                Intrinsics.b("coreComponent");
                throw null;
            } else if (coreComponent != null) {
                MainModule mainModule = new MainModule();
                AddProfileStepsNavigationModule addProfileStepsNavigationModule = new AddProfileStepsNavigationModule();
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                AppComponentsHolder.b = new DaggerMainComponent(mainModule, addProfileStepsNavigationModule, coreComponent, null);
            } else {
                throw null;
            }
        }
        MainComponent mainComponent = AppComponentsHolder.b;
        if (mainComponent != null) {
            mainComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.a();
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.RadioGroup, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.RadioGroup, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.e.a.d.RadioGroupCheckedChangeObservable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.button.MaterialButton, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g2 = super.g();
        TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_gender);
        Intrinsics.a((Object) textInputEditText, "frag_profile_filling_add_child_ed_gender");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) textInputEditText).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText2 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_citizenship);
        Intrinsics.a((Object) textInputEditText2, "frag_profile_filling_add_child_ed_citizenship");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) textInputEditText2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText3 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_surname);
        TextInputEditText textInputEditText4 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_name);
        TextInputEditText textInputEditText5 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_patronymic);
        TextInputEditText textInputEditText6 = (TextInputEditText) c(e.a.b.b.rag_profile_filling_add_child_ed_registration_address);
        TextInputEditText textInputEditText7 = (TextInputEditText) c(e.a.b.b.rag_profile_filling_add_child_ed_living_place);
        TextView textView = (TextView) c(e.a.b.b.frag_profile_filling_add_child_tv_back);
        Intrinsics.a((Object) textView, "frag_profile_filling_add_child_tv_back");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) textView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        RadioGroup radioGroup = (RadioGroup) c(e.a.b.b.frag_profile_filling_add_child_rg_same_address);
        Intrinsics.a((Object) radioGroup, "frag_profile_filling_add_child_rg_same_address");
        j.c.a.a.c.n.c.m12a((Object) radioGroup, "view == null");
        RadioGroupCheckedChangeObservable radioGroupCheckedChangeObservable = new RadioGroupCheckedChangeObservable(radioGroup);
        Intrinsics.a((Object) radioGroupCheckedChangeObservable, "RxRadioGroup.checkedChanges(this)");
        TextInputEditText textInputEditText8 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_document_type);
        Intrinsics.a((Object) textInputEditText8, "frag_profile_filling_add_child_ed_document_type");
        Observable<R> c5 = j.c.a.a.c.n.c.a((View) textInputEditText8).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c5, "RxView.clicks(this).map(AnyToUnit)");
        TextInputEditText textInputEditText9 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_et_document_series);
        TextInputEditText textInputEditText10 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_et_document_number);
        TextInputEditText textInputEditText11 = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_et_symptoms_step_answer);
        Intrinsics.a((Object) textInputEditText11, "frag_profile_filling_add…d_et_symptoms_step_answer");
        Observable<R> c6 = j.c.a.a.c.n.c.a((View) textInputEditText11).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c6, "RxView.clicks(this).map(AnyToUnit)");
        MaterialButton materialButton = (MaterialButton) c(e.a.b.b.frag_simple_main_btn_open_questionnaire);
        Intrinsics.a((Object) materialButton, "frag_simple_main_btn_open_questionnaire");
        Observable<R> c7 = j.c.a.a.c.n.c.a((View) materialButton).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c7, "RxView.clicks(this).map(AnyToUnit)");
        ImageView imageView = (ImageView) c(e.a.b.b.frag_profile_filling_add_child_iv_close);
        Intrinsics.a((Object) imageView, "frag_profile_filling_add_child_iv_close");
        Observable<R> c8 = j.c.a.a.c.n.c.a((View) imageView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c8, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) g2, (Iterable) Collections.a((Object[]) new Observable[]{c2.c((Function) g.a), c3.c((Function) h.a), outline.a(textInputEditText3, "frag_profile_filling_add_child_ed_surname", textInputEditText3, "RxTextView.textChanges(this)").c((Function) i.a), outline.a(textInputEditText4, "frag_profile_filling_add_child_ed_name", textInputEditText4, "RxTextView.textChanges(this)").c((Function) j.a), outline.a(textInputEditText5, "frag_profile_filling_add_child_ed_patronymic", textInputEditText5, "RxTextView.textChanges(this)").c((Function) k.a), new InitialValueObservable.a().c((Function) l.a), new InitialValueObservable.a().c((Function) m.a), c4.c((Function) n.a), radioGroupCheckedChangeObservable.c((Function) o.a), c5.c((Function) new a(this)), outline.a(textInputEditText9, "frag_profile_filling_add_child_et_document_series", textInputEditText9, "RxTextView.textChanges(this)").c((Function) b.a), outline.a(textInputEditText10, "frag_profile_filling_add_child_et_document_number", textInputEditText10, "RxTextView.textChanges(this)").c((Function) c.a), c6.c((Function) new d(this)), c7.c((Function) e.a), c8.c((Function) f.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [?, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate((int) R.layout.frag_profile_filling_add_child, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }

    public void a(BaseViewState2 baseViewState2) {
        if (baseViewState2 == null) {
            Intrinsics.a("ve");
            throw null;
        } else if (baseViewState2 instanceof ProfileFillingAddChildFragmentViewState2.a) {
            Snackbar.a((CoordinatorLayout) c(e.a.b.b.frag_profile_filling_add_child_root), R.string.frag_filling_profile_container_invalid_step_message, -1).f();
        } else {
            super.a(baseViewState2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [com.google.android.material.textfield.TextInputEditText, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Calendar, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(View view, Bundle bundle) {
        if (view != null) {
            TextInputEditText textInputEditText = (TextInputEditText) c(e.a.b.b.frag_profile_filling_add_child_ed_birthday);
            Intrinsics.a((Object) textInputEditText, "frag_profile_filling_add_child_ed_birthday");
            Calendar instance = Calendar.getInstance();
            Intrinsics.a((Object) instance, "Calendar.getInstance()");
            Collections.a(textInputEditText, Long.valueOf(instance.getTimeInMillis()), null, null, new p(this), 6);
            ProfileFillingAddChildFragmentVm profileFillingAddChildFragmentVm = (ProfileFillingAddChildFragmentVm) h();
            String a2 = a((int) R.string.frag_filling_profile_gender_hint);
            Intrinsics.a((Object) a2, "getString(R.string.frag_…ling_profile_gender_hint)");
            String a3 = a((int) R.string.common_gender_male);
            Intrinsics.a((Object) a3, "getString(R.string.common_gender_male)");
            String a4 = a((int) R.string.common_gender_female);
            Intrinsics.a((Object) a4, "getString(R.string.common_gender_female)");
            String a5 = a((int) R.string.frag_filling_profile_citizenship_hint);
            Intrinsics.a((Object) a5, "getString(R.string.frag_…profile_citizenship_hint)");
            profileFillingAddChildFragmentVm.d.a(ProfileFillingAddChildFragmentViewState.a((ProfileFillingAddChildFragmentViewState) profileFillingAddChildFragmentVm.e(), new ProfileFillingBottomSheetDto(a2, a3, a4, a5), false, null, null, null, null, 62));
            return;
        }
        Intrinsics.a("view");
        throw null;
    }
}
