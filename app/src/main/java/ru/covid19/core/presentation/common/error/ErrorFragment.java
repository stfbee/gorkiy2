package ru.covid19.core.presentation.common.error;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import e.a.a.a.c.a.ErrorFragmentViewState;
import e.a.a.a.c.a.ErrorFragmentViewState0;
import e.a.a.a.c.a.ErrorFragmentViewState1;
import e.a.a.a.c.a.ErrorFragmentVm;
import e.a.a.a.c.a.d;
import e.a.a.a.c.a.e;
import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.a.f;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import j.c.a.a.c.n.c;
import j.e.a.b.AnyToUnit;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.i.Collections;
import n.i._Arrays;
import n.n.c.Intrinsics;
import ru.waveaccess.wamvirx.BaseMviFragment;

/* compiled from: ErrorFragment.kt */
public final class ErrorFragment extends BaseMviFragment<d, e> implements e.c.d.b.c.b {
    public HashMap d0;

    /* compiled from: java-style lambda group */
    public static final class a<T, R> implements Function<T, R> {
        public static final a b = new a(0);
        public static final a c = new a(1);
        public final /* synthetic */ int a;

        public a(int i2) {
            this.a = i2;
        }

        public final Object a(Object obj) {
            int i2 = this.a;
            if (i2 != 0) {
                if (i2 != 1) {
                    throw null;
                } else if (((Unit) obj) != null) {
                    return ErrorFragmentViewState1.a.a;
                } else {
                    Intrinsics.a("it");
                    throw null;
                }
            } else if (((Unit) obj) != null) {
                return ErrorFragmentViewState1.a.a;
            } else {
                Intrinsics.a("it");
                throw null;
            }
        }
    }

    /* compiled from: ErrorFragment.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return ErrorFragmentViewState1.c.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public /* synthetic */ void C() {
        super.C();
        N();
    }

    public void N() {
        HashMap hashMap = this.d0;
        if (hashMap != null) {
            hashMap.clear();
        }
    }

    public Class<e> P() {
        return ErrorFragmentVm.class;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void Q() {
        ErrorNavigationDto errorNavigationDto = (ErrorNavigationDto) Collections.a(this.g);
        if (errorNavigationDto != null) {
            ErrorFragmentVm errorFragmentVm = (ErrorFragmentVm) h();
            errorFragmentVm.f595j.a(errorNavigationDto.c);
            String str = errorNavigationDto.d;
            if (str == null) {
                int i2 = errorNavigationDto.h;
                if (i2 != 0) {
                    str = errorFragmentVm.f594i.getString(i2);
                    Intrinsics.a((Object) str, "context.getString(dto.titleResId)");
                } else {
                    str = errorFragmentVm.f594i.getString(f.frag_error_default_title);
                    Intrinsics.a((Object) str, "context.getString(R.stri…frag_error_default_title)");
                }
            }
            String str2 = str;
            String str3 = errorNavigationDto.f607e;
            if (str3 == null) {
                int i3 = errorNavigationDto.f609i;
                if (i3 != 0) {
                    str3 = errorFragmentVm.f594i.getString(i3);
                    Intrinsics.a((Object) str3, "context.getString(dto.messageResId)");
                } else {
                    str3 = errorFragmentVm.f594i.getString(f.frag_error_default_message);
                    Intrinsics.a((Object) str3, "context.getString(R.stri…ag_error_default_message)");
                }
            }
            String str4 = str3;
            String str5 = errorNavigationDto.f608f;
            if (str5 == null) {
                int i4 = errorNavigationDto.f610j;
                if (i4 != 0) {
                    str5 = errorFragmentVm.f594i.getString(i4);
                    Intrinsics.a((Object) str5, "context.getString(dto.backButtonTextResId)");
                } else {
                    str5 = errorFragmentVm.f594i.getString(f.frag_error_default_back_button_text);
                    Intrinsics.a((Object) str5, "context.getString(R.stri…default_back_button_text)");
                }
            }
            String str6 = str5;
            String str7 = errorNavigationDto.g;
            if (str7 == null) {
                int i5 = errorNavigationDto.f611k;
                if (i5 != 0) {
                    str7 = errorFragmentVm.f594i.getString(i5);
                    Intrinsics.a((Object) str7, "context.getString(dto.submitButtonTextResId)");
                } else {
                    str7 = errorFragmentVm.f594i.getString(f.frag_error_default_retry_button_text);
                    Intrinsics.a((Object) str7, "context.getString(R.stri…efault_retry_button_text)");
                }
            }
            String str8 = str7;
            errorFragmentVm.g.a((e.c.c.b) new ErrorFragmentViewState1.b(new ErrorFragmentViewState(errorNavigationDto.b, str2, str4, str8, str6, errorNavigationDto.f613m, errorNavigationDto.f612l, errorNavigationDto.f617q, errorNavigationDto.f614n, errorNavigationDto.f615o, errorNavigationDto.f618r, errorNavigationDto.f619s)));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.TextView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        ErrorFragmentViewState0 errorFragmentViewState0 = (ErrorFragmentViewState0) obj;
        if (errorFragmentViewState0 != null) {
            TextView textView = (TextView) c(e.a.a.d.frag_error_tv_title);
            Intrinsics.a((Object) textView, "frag_error_tv_title");
            textView.setText(errorFragmentViewState0.a.c);
            TextView textView2 = (TextView) c(e.a.a.d.frag_error_tv_error);
            Intrinsics.a((Object) textView2, "frag_error_tv_error");
            textView2.setText(errorFragmentViewState0.a.d);
            Button button = (Button) c(e.a.a.d.frag_error_btn_back);
            Intrinsics.a((Object) button, "frag_error_btn_back");
            button.setText(errorFragmentViewState0.a.f588f);
            Button button2 = (Button) c(e.a.a.d.frag_error_btn_retry);
            Intrinsics.a((Object) button2, "frag_error_btn_retry");
            button2.setText(errorFragmentViewState0.a.f587e);
            Button button3 = (Button) c(e.a.a.d.frag_error_btn_back);
            Intrinsics.a((Object) button3, "frag_error_btn_back");
            Collections.a(button3, errorFragmentViewState0.a.h);
            Button button4 = (Button) c(e.a.a.d.frag_error_btn_retry);
            Intrinsics.a((Object) button4, "frag_error_btn_retry");
            Collections.a(button4, errorFragmentViewState0.a.g);
            ImageView imageView = (ImageView) c(e.a.a.d.frag_error_iv_icon);
            Intrinsics.a((Object) imageView, "frag_error_iv_icon");
            if (!(imageView.getVisibility() == 0)) {
                ImageView imageView2 = (ImageView) c(e.a.a.d.frag_error_iv_icon);
                Intrinsics.a((Object) imageView2, "frag_error_iv_icon");
                Collections.b(imageView2);
                return;
            }
            return;
        }
        Intrinsics.a("vs");
        throw null;
    }

    public void b(Bundle bundle) {
        CoreComponent coreComponent = CoreComponentsHolder.b;
        if (coreComponent != null) {
            coreComponent.a(this.a0);
            super.b(bundle);
            return;
        }
        Intrinsics.b("coreComponent");
        throw null;
    }

    public View c(int i2) {
        if (this.d0 == null) {
            this.d0 = new HashMap();
        }
        View view = (View) this.d0.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View view2 = this.H;
        if (view2 == null) {
            return null;
        }
        View findViewById = view2.findViewById(i2);
        this.d0.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    public boolean e() {
        super.c0.a((e.c.c.b) ErrorFragmentViewState1.a.a);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List<l.b.Observable<? extends e.c.c.b>>, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List<Observable<? extends e.c.c.b>> g = super.g();
        Button button = (Button) c(e.a.a.d.frag_error_btn_retry);
        Intrinsics.a((Object) button, "frag_error_btn_retry");
        Observable<R> c = c.a((View) button).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c, "RxView.clicks(this).map(AnyToUnit)");
        Button button2 = (Button) c(e.a.a.d.frag_error_btn_back);
        Intrinsics.a((Object) button2, "frag_error_btn_back");
        Observable<R> c2 = c.a((View) button2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        ImageView imageView = (ImageView) c(e.a.a.d.frag_error_iv_back);
        Intrinsics.a((Object) imageView, "frag_error_iv_back");
        Observable<R> c3 = c.a((View) imageView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) g, (Iterable) Collections.a((Object[]) new Observable[]{c.c((Function) b.a), c2.c((Function) a.b), c3.c((Function) a.c)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, android.view.ViewGroup, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public View a(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        if (layoutInflater != null) {
            return layoutInflater.inflate(e.a.a.e.frag_error, viewGroup, false);
        }
        Intrinsics.a("inflater");
        throw null;
    }
}
