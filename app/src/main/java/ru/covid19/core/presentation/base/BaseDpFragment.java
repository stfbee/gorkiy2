package ru.covid19.core.presentation.base;

import androidx.appcompat.widget.Toolbar;
import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.a.b.BaseEvents;
import e.c.d.b.c.b;
import i.b.k.AppCompatActivity;
import i.l.a.FragmentActivity;
import n.n.c.Intrinsics;
import ru.waveaccess.wamvirx.BaseMviFragment;

/* compiled from: BaseDpFragment.kt */
public abstract class BaseDpFragment<VS, VM extends BaseDpFragmentVm1<VS>> extends BaseMviFragment<VS, VM> implements b {
    public void C() {
        super.C();
        if ("" != 0) {
            FragmentActivity I = I();
            if (!(I instanceof AppCompatActivity)) {
                I = null;
            }
            AppCompatActivity appCompatActivity = (AppCompatActivity) I;
            if (appCompatActivity != null) {
                appCompatActivity.a((Toolbar) null);
            }
            N();
            return;
        }
        Intrinsics.a("title");
        throw null;
    }

    public void N() {
    }

    public boolean e() {
        super.c0.a((e.c.c.b) BaseEvents.a);
        return true;
    }
}
