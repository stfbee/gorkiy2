package ru.covid19.core.presentation.dev;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import androidx.appcompat.app.AlertController;
import androidx.appcompat.widget.Toolbar;
import e.a.a.a.d.DevActivity0;
import e.a.a.a.d.DevActivity1;
import e.a.a.a.d.DevActivity2;
import e.a.a.a.d.DevActivity3;
import e.a.a.a.d.DevActivity4;
import e.a.a.a.d.DevActivity5;
import e.a.a.a.d.DevActivity6;
import e.a.a.a.d.DevActivity7;
import e.a.a.a.d.DevActivityViewState;
import e.a.a.a.d.DevActivityViewState0;
import e.a.a.a.d.DevActivityVm2;
import e.a.a.a.d.j;
import e.a.a.a.d.m;
import e.a.a.a.e.o.ActivityNavigator;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.f;
import e.a.a.i.CoreComponentsHolder;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.b.DaggerDevComponent;
import e.a.a.i.b.DevComponent;
import e.a.a.i.e.NavigationWrapper;
import e.a.a.j.b.IDevMenuRepository;
import e.b.a.CommandBuffer;
import e.b.a.Navigator;
import e.c.a.b.ContextExtensions;
import e.c.c.BaseMviActivity;
import e.c.c.BaseMviView1;
import e.c.d.b.c.VmFactoryWrapper;
import i.b.k.ActionBar;
import i.b.k.AlertDialog;
import i.o.ViewModelProvider;
import j.e.a.b.AnyToUnit;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import kotlin.TypeCastException;
import l.b.Observable;
import l.b.s.CompositeDisposable;
import l.b.t.Consumer;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.i.Collections;
import n.i._Arrays;
import n.n.c.Intrinsics;

/* compiled from: DevActivity.kt */
public final class DevActivity extends BaseMviActivity<j, m> {
    public final VmFactoryWrapper v = new VmFactoryWrapper();
    public final NavigationWrapper w = new NavigationWrapper();
    public Navigator x;
    public HashMap y;

    /* compiled from: DevActivity.kt */
    public static final class a<T> implements Consumer<g> {
        public final /* synthetic */ DevActivity b;

        public a(DevActivity devActivity) {
            this.b = devActivity;
        }

        /* JADX WARN: Type inference failed for: r9v2, types: [android.content.Context, ru.covid19.core.presentation.dev.DevActivity] */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [java.lang.String, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [i.b.k.AlertDialog, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public void a(Object obj) {
            String str;
            String str2;
            String str3;
            Unit unit = (Unit) obj;
            ? r9 = this.b;
            int i2 = f.frag_main_logout_description;
            int i3 = f.frag_main_logout_confirm_button;
            int i4 = f.frag_main_logout_back_button;
            DevActivity2 devActivity2 = new DevActivity2(this);
            h hVar = h.f734e;
            h hVar2 = h.f735f;
            if (r9 != 0) {
                if (i2 != 0) {
                    str = r9.getString(i2);
                    Intrinsics.a((Object) str, "getString(descriptionResId)");
                } else {
                    str = "";
                }
                if (i3 != 0) {
                    str2 = r9.getString(i3);
                    Intrinsics.a((Object) str2, "getString(buttonPositiveTextResId)");
                } else {
                    str2 = "";
                }
                if (i4 != 0) {
                    str3 = r9.getString(i4);
                    Intrinsics.a((Object) str3, "getString(buttonNegativeTextResId)");
                } else {
                    str3 = "";
                }
                AlertDialog.a aVar = new AlertDialog.a(r9);
                AlertController.b bVar = aVar.a;
                bVar.f19f = "";
                bVar.h = str;
                boolean z = true;
                defpackage.c cVar = new defpackage.c(1, devActivity2);
                AlertController.b bVar2 = aVar.a;
                bVar2.f20i = str2;
                bVar2.f21j = cVar;
                if (str3.length() <= 0) {
                    z = false;
                }
                if (z) {
                    defpackage.c cVar2 = new defpackage.c(0, hVar);
                    AlertController.b bVar3 = aVar.a;
                    bVar3.f22k = str3;
                    bVar3.f23l = cVar2;
                }
                aVar.a.f25n = new ContextExtensions(hVar2);
                AlertDialog a = aVar.a();
                Intrinsics.a((Object) a, "builder.create()");
                a.show();
                return;
            }
            Intrinsics.a("$this$showAlertDialog");
            throw null;
        }
    }

    /* compiled from: DevActivity.kt */
    public static final class b<T, R> implements Function<T, R> {
        public static final b a = new b();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return DevActivityViewState0.a.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivity.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return DevActivityViewState0.c.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivity.kt */
    public static final class d<T, R> implements Function<T, R> {
        public static final d a = new d();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return DevActivityViewState0.e.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: DevActivity.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            if (((Unit) obj) != null) {
                return DevActivityViewState0.d.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Spinner, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.CheckBox, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        DevActivityViewState devActivityViewState = (DevActivityViewState) obj;
        if (devActivityViewState != null) {
            Spinner spinner = (Spinner) c(e.a.a.d.act_dev_menu_sp_endpoint);
            Intrinsics.a((Object) spinner, "act_dev_menu_sp_endpoint");
            String str = devActivityViewState.a;
            SpinnerAdapter adapter = spinner.getAdapter();
            if (adapter != null) {
                ArrayAdapter arrayAdapter = (ArrayAdapter) adapter;
                if (arrayAdapter.getPosition(str) >= 0) {
                    spinner.setSelection(arrayAdapter.getPosition(str));
                }
                CheckBox checkBox = (CheckBox) c(e.a.a.d.act_dev_menu_cb_secure);
                Intrinsics.a((Object) checkBox, "act_dev_menu_cb_secure");
                checkBox.setChecked(devActivityViewState.b);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type android.widget.ArrayAdapter<kotlin.String>");
        }
        Intrinsics.a("vs");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [i.b.k.AppCompatActivity, ru.covid19.core.presentation.dev.DevActivity] */
    public View c(int i2) {
        if (this.y == null) {
            this.y = new HashMap();
        }
        View view = (View) this.y.get(Integer.valueOf(i2));
        if (view != null) {
            return view;
        }
        View findViewById = findViewById(i2);
        this.y.put(Integer.valueOf(i2), findViewById);
        return findViewById;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.ImageView, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T>
     arg types: [java.util.List, java.util.List]
     candidates:
      n.i._Arrays.a(java.lang.Iterable, java.util.Collection):C
      n.i._Arrays.a(java.util.Collection, java.lang.Iterable):java.util.List<T> */
    public List<Observable<? extends e.c.c.b>> g() {
        List c2 = j.c.a.a.c.n.c.c(b());
        ImageView imageView = (ImageView) c(e.a.a.d.act_dev_menu_btn_apply_settings);
        Intrinsics.a((Object) imageView, "act_dev_menu_btn_apply_settings");
        Observable<R> c3 = j.c.a.a.c.n.c.a((View) imageView).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c3, "RxView.clicks(this).map(AnyToUnit)");
        Button button = (Button) c(e.a.a.d.act_dev_menu_btn_clear_cache);
        Intrinsics.a((Object) button, "act_dev_menu_btn_clear_cache");
        Observable<R> c4 = j.c.a.a.c.n.c.a((View) button).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c4, "RxView.clicks(this).map(AnyToUnit)");
        Button button2 = (Button) c(e.a.a.d.act_dev_menu_btn_logout);
        Intrinsics.a((Object) button2, "act_dev_menu_btn_logout");
        Observable<R> c5 = j.c.a.a.c.n.c.a((View) button2).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c5, "RxView.clicks(this).map(AnyToUnit)");
        Button button3 = (Button) c(e.a.a.d.act_dev_menu_btn_delete_profile);
        Intrinsics.a((Object) button3, "act_dev_menu_btn_delete_profile");
        Observable<R> c6 = j.c.a.a.c.n.c.a((View) button3).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c6, "RxView.clicks(this).map(AnyToUnit)");
        return _Arrays.a((Collection) c2, (Iterable) Collections.a((Object[]) new Observable[]{c3.c((Function) b.a), c4.c((Function) c.a), c5.c((Function) d.a), c6.c((Function) e.a)}));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Button, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void o() {
        Collections.a((BaseMviView1) this);
        CompositeDisposable compositeDisposable = this.f726s;
        Button button = (Button) c(e.a.a.d.act_dev_menu_btn_logout);
        Intrinsics.a((Object) button, "act_dev_menu_btn_logout");
        Observable<R> c2 = j.c.a.a.c.n.c.a((View) button).c((Function) AnyToUnit.a);
        Intrinsics.a((Object) c2, "RxView.clicks(this).map(AnyToUnit)");
        compositeDisposable.a(c2.a(new a(this)));
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [i.l.a.FragmentActivity, ru.covid19.core.presentation.dev.DevActivity] */
    public void onActivityResult(int i2, int i3, Intent intent) {
        if (!r().b.a(i3, intent)) {
            DevActivity.super.onActivityResult(i2, i3, intent);
        }
    }

    /* JADX WARN: Type inference failed for: r9v0, types: [android.content.Context, i.b.k.AppCompatActivity, e.c.c.BaseMviActivity, ru.covid19.core.presentation.dev.DevActivity] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.widget.Spinner, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void onCreate(Bundle bundle) {
        if (CoreComponentsHolder.f625f == null) {
            CoreComponent coreComponent = CoreComponentsHolder.b;
            if (coreComponent != null) {
                j.c.a.a.c.n.c.a(coreComponent, CoreComponent.class);
                CoreComponentsHolder.f625f = new DaggerDevComponent(coreComponent, null);
            } else {
                Intrinsics.b("coreComponent");
                throw null;
            }
        }
        DevComponent devComponent = CoreComponentsHolder.f625f;
        if (devComponent != null) {
            devComponent.a(this.v);
            devComponent.a(this.w);
            super.onCreate(bundle);
            this.x = new ActivityNavigator(this, new DevActivity3(this), new DevActivity4(this), new DevActivity5(this), new DevActivity6(this), new DevActivity7(this));
            setContentView(e.a.a.e.act_dev_menu);
            Toolbar toolbar = (Toolbar) c(e.a.a.d.act_dev_menu_toolbar);
            a(toolbar);
            toolbar.setNavigationOnClickListener(new DevActivity1(this));
            ActionBar n2 = n();
            if (n2 != null) {
                n2.c(true);
            }
            Spinner spinner = (Spinner) c(e.a.a.d.act_dev_menu_sp_endpoint);
            Intrinsics.a((Object) spinner, "act_dev_menu_sp_endpoint");
            IDevMenuRepository[] values = IDevMenuRepository.values();
            ArrayList arrayList = new ArrayList(values.length);
            for (IDevMenuRepository name : values) {
                arrayList.add(name.name());
            }
            ArrayAdapter arrayAdapter = new ArrayAdapter((Context) this, 17367048, arrayList);
            arrayAdapter.setDropDownViewResource(17367050);
            spinner.setAdapter((SpinnerAdapter) arrayAdapter);
            spinner.setSelection(0, false);
            spinner.setOnItemSelectedListener(new DevActivity0(this, spinner));
            ((CheckBox) c(e.a.a.d.act_dev_menu_cb_secure)).setOnCheckedChangeListener(new e.a.a.a.d.DevActivity(this));
            return;
        }
        Intrinsics.a();
        throw null;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [i.b.k.AppCompatActivity, ru.covid19.core.presentation.dev.DevActivity] */
    public void onDestroy() {
        CoreComponentsHolder.f625f = null;
        DevActivity.super.onDestroy();
    }

    public void onPause() {
        super.onPause();
        r().a().a((Navigator) null);
    }

    public void onResume() {
        super.onResume();
        CommandBuffer a2 = r().a();
        Navigator navigator = this.x;
        if (navigator != null) {
            a2.a(navigator);
            Class<DevActivity> cls = DevActivity.class;
            if (r() == null) {
                throw null;
            }
            return;
        }
        Intrinsics.b("navigator");
        throw null;
    }

    public Class<m> p() {
        return DevActivityVm2.class;
    }

    public ViewModelProvider.b q() {
        return this.v.a();
    }

    public final AppCiceroneHolder r() {
        AppCiceroneHolder appCiceroneHolder = this.w.b;
        if (appCiceroneHolder != null) {
            return appCiceroneHolder;
        }
        Intrinsics.b("appCiceroneHolder");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [ru.covid19.core.presentation.dev.DevActivity, android.app.Activity] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final /* synthetic */ void a(ru.covid19.core.presentation.dev.DevActivity r2) {
        /*
            e.a.a.a.e.r.AppCiceroneHolder r0 = r2.r()
            e.b.a.CommandBuffer r0 = r0.a()
            r1 = 0
            r0.a(r1)
            r2.finish()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.core.presentation.dev.DevActivity.a(ru.covid19.core.presentation.dev.DevActivity):void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [android.content.Context, ru.covid19.core.presentation.dev.DevActivity] */
    /* JADX WARNING: Unknown variable types count: 1 */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static final /* synthetic */ void a(ru.covid19.core.presentation.dev.DevActivity r0, java.lang.String r1, java.lang.Object r2) {
        /*
            if (r0 == 0) goto L_0x000b
            r2 = 1
            android.widget.Toast r0 = android.widget.Toast.makeText(r0, r1, r2)
            r0.show()
            return
        L_0x000b:
            r0 = 0
            throw r0
        */
        throw new UnsupportedOperationException("Method not decompiled: ru.covid19.core.presentation.dev.DevActivity.a(ru.covid19.core.presentation.dev.DevActivity, java.lang.String, java.lang.Object):void");
    }
}
