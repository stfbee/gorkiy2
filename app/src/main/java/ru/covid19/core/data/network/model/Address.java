package ru.covid19.core.data.network.model;

import com.crashlytics.android.answers.SessionEventTransform;
import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import j.a.a.a.outline;
import java.util.List;
import n.n.c.Intrinsics;

/* compiled from: Address.kt */
public final class Address {
    public final String additionArea;
    public final String additionAreaStreet;
    public final String addressStr;
    public final String another;
    public final String area;
    public final String building;
    public final String city;
    public final String countryId;
    public final String district;
    public final String eTag;
    public final String fiasCode;
    public final String fiasCode2;
    public final String flat;
    public final String frame;
    public final String house;
    public final int id;
    public final Integer lat;
    public final Integer lng;
    public final String region;
    public final String settlement;
    public final List<String> stateFacts;
    public final String street;
    public final AddressType type;
    public final String vrfDdt;
    public final String zipCode;

    public Address(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, int i2, Integer num, Integer num2, String str16, String str17, List<String> list, String str18, AddressType addressType, String str19, String str20) {
        String str21 = str3;
        String str22 = str7;
        String str23 = str8;
        String str24 = str10;
        String str25 = str11;
        String str26 = str13;
        String str27 = str15;
        String str28 = str16;
        List<String> list2 = list;
        String str29 = str18;
        AddressType addressType2 = addressType;
        String str30 = str20;
        if (str21 == null) {
            Intrinsics.a("addressStr");
            throw null;
        } else if (str22 == null) {
            Intrinsics.a("city");
            throw null;
        } else if (str23 == null) {
            Intrinsics.a("countryId");
            throw null;
        } else if (str24 == null) {
            Intrinsics.a("eTag");
            throw null;
        } else if (str25 == null) {
            Intrinsics.a("fiasCode");
            throw null;
        } else if (str26 == null) {
            Intrinsics.a("flat");
            throw null;
        } else if (str27 == null) {
            Intrinsics.a("house");
            throw null;
        } else if (str28 == null) {
            Intrinsics.a("region");
            throw null;
        } else if (list2 == null) {
            Intrinsics.a("stateFacts");
            throw null;
        } else if (str29 == null) {
            Intrinsics.a("street");
            throw null;
        } else if (addressType2 == null) {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        } else if (str30 != null) {
            this.additionArea = str;
            this.additionAreaStreet = str2;
            this.addressStr = str21;
            this.another = str4;
            this.area = str5;
            this.building = str6;
            this.city = str22;
            this.countryId = str23;
            this.district = str9;
            this.eTag = str24;
            this.fiasCode = str25;
            this.fiasCode2 = str12;
            this.flat = str26;
            this.frame = str14;
            this.house = str27;
            this.id = i2;
            this.lat = num;
            this.lng = num2;
            this.region = str28;
            this.settlement = str17;
            this.stateFacts = list2;
            this.street = str29;
            this.type = addressType2;
            this.vrfDdt = str19;
            this.zipCode = str30;
        } else {
            Intrinsics.a("zipCode");
            throw null;
        }
    }

    public static /* synthetic */ Address copy$default(Address address, String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, int i2, Integer num, Integer num2, String str16, String str17, List list, String str18, AddressType addressType, String str19, String str20, int i3, Object obj) {
        Address address2 = address;
        int i4 = i3;
        return address.copy((i4 & 1) != 0 ? address2.additionArea : str, (i4 & 2) != 0 ? address2.additionAreaStreet : str2, (i4 & 4) != 0 ? address2.addressStr : str3, (i4 & 8) != 0 ? address2.another : str4, (i4 & 16) != 0 ? address2.area : str5, (i4 & 32) != 0 ? address2.building : str6, (i4 & 64) != 0 ? address2.city : str7, (i4 & 128) != 0 ? address2.countryId : str8, (i4 & 256) != 0 ? address2.district : str9, (i4 & 512) != 0 ? address2.eTag : str10, (i4 & 1024) != 0 ? address2.fiasCode : str11, (i4 & 2048) != 0 ? address2.fiasCode2 : str12, (i4 & CodedOutputStream.DEFAULT_BUFFER_SIZE) != 0 ? address2.flat : str13, (i4 & 8192) != 0 ? address2.frame : str14, (i4 & 16384) != 0 ? address2.house : str15, (i4 & 32768) != 0 ? address2.id : i2, (i4 & LogFileManager.MAX_LOG_SIZE) != 0 ? address2.lat : num, (i4 & 131072) != 0 ? address2.lng : num2, (i4 & 262144) != 0 ? address2.region : str16, (i4 & 524288) != 0 ? address2.settlement : str17, (i4 & 1048576) != 0 ? address2.stateFacts : list, (i4 & 2097152) != 0 ? address2.street : str18, (i4 & 4194304) != 0 ? address2.type : addressType, (i4 & 8388608) != 0 ? address2.vrfDdt : str19, (i4 & 16777216) != 0 ? address2.zipCode : str20);
    }

    public final String component1() {
        return this.additionArea;
    }

    public final String component10() {
        return this.eTag;
    }

    public final String component11() {
        return this.fiasCode;
    }

    public final String component12() {
        return this.fiasCode2;
    }

    public final String component13() {
        return this.flat;
    }

    public final String component14() {
        return this.frame;
    }

    public final String component15() {
        return this.house;
    }

    public final int component16() {
        return this.id;
    }

    public final Integer component17() {
        return this.lat;
    }

    public final Integer component18() {
        return this.lng;
    }

    public final String component19() {
        return this.region;
    }

    public final String component2() {
        return this.additionAreaStreet;
    }

    public final String component20() {
        return this.settlement;
    }

    public final List<String> component21() {
        return this.stateFacts;
    }

    public final String component22() {
        return this.street;
    }

    public final AddressType component23() {
        return this.type;
    }

    public final String component24() {
        return this.vrfDdt;
    }

    public final String component25() {
        return this.zipCode;
    }

    public final String component3() {
        return this.addressStr;
    }

    public final String component4() {
        return this.another;
    }

    public final String component5() {
        return this.area;
    }

    public final String component6() {
        return this.building;
    }

    public final String component7() {
        return this.city;
    }

    public final String component8() {
        return this.countryId;
    }

    public final String component9() {
        return this.district;
    }

    public final Address copy(String str, String str2, String str3, String str4, String str5, String str6, String str7, String str8, String str9, String str10, String str11, String str12, String str13, String str14, String str15, int i2, Integer num, Integer num2, String str16, String str17, List<String> list, String str18, AddressType addressType, String str19, String str20) {
        if (str3 == null) {
            Intrinsics.a("addressStr");
            throw null;
        } else if (str7 == null) {
            Intrinsics.a("city");
            throw null;
        } else if (str8 == null) {
            Intrinsics.a("countryId");
            throw null;
        } else if (str10 == null) {
            Intrinsics.a("eTag");
            throw null;
        } else if (str11 == null) {
            Intrinsics.a("fiasCode");
            throw null;
        } else if (str13 == null) {
            Intrinsics.a("flat");
            throw null;
        } else if (str15 == null) {
            Intrinsics.a("house");
            throw null;
        } else if (str16 == null) {
            Intrinsics.a("region");
            throw null;
        } else if (list == null) {
            Intrinsics.a("stateFacts");
            throw null;
        } else if (str18 == null) {
            Intrinsics.a("street");
            throw null;
        } else if (addressType == null) {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        } else if (str20 != null) {
            return new Address(str, str2, str3, str4, str5, str6, str7, str8, str9, str10, str11, str12, str13, str14, str15, i2, num, num2, str16, str17, list, str18, addressType, str19, str20);
        } else {
            Intrinsics.a("zipCode");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Address)) {
            return false;
        }
        Address address = (Address) obj;
        return Intrinsics.a(this.additionArea, address.additionArea) && Intrinsics.a(this.additionAreaStreet, address.additionAreaStreet) && Intrinsics.a(this.addressStr, address.addressStr) && Intrinsics.a(this.another, address.another) && Intrinsics.a(this.area, address.area) && Intrinsics.a(this.building, address.building) && Intrinsics.a(this.city, address.city) && Intrinsics.a(this.countryId, address.countryId) && Intrinsics.a(this.district, address.district) && Intrinsics.a(this.eTag, address.eTag) && Intrinsics.a(this.fiasCode, address.fiasCode) && Intrinsics.a(this.fiasCode2, address.fiasCode2) && Intrinsics.a(this.flat, address.flat) && Intrinsics.a(this.frame, address.frame) && Intrinsics.a(this.house, address.house) && this.id == address.id && Intrinsics.a(this.lat, address.lat) && Intrinsics.a(this.lng, address.lng) && Intrinsics.a(this.region, address.region) && Intrinsics.a(this.settlement, address.settlement) && Intrinsics.a(this.stateFacts, address.stateFacts) && Intrinsics.a(this.street, address.street) && Intrinsics.a(this.type, address.type) && Intrinsics.a(this.vrfDdt, address.vrfDdt) && Intrinsics.a(this.zipCode, address.zipCode);
    }

    public final String getAdditionArea() {
        return this.additionArea;
    }

    public final String getAdditionAreaStreet() {
        return this.additionAreaStreet;
    }

    public final String getAddressStr() {
        return this.addressStr;
    }

    public final String getAnother() {
        return this.another;
    }

    public final String getArea() {
        return this.area;
    }

    public final String getBuilding() {
        return this.building;
    }

    public final String getCity() {
        return this.city;
    }

    public final String getCountryId() {
        return this.countryId;
    }

    public final String getDistrict() {
        return this.district;
    }

    public final String getETag() {
        return this.eTag;
    }

    public final String getFiasCode() {
        return this.fiasCode;
    }

    public final String getFiasCode2() {
        return this.fiasCode2;
    }

    public final String getFlat() {
        return this.flat;
    }

    public final String getFrame() {
        return this.frame;
    }

    public final String getHouse() {
        return this.house;
    }

    public final int getId() {
        return this.id;
    }

    public final Integer getLat() {
        return this.lat;
    }

    public final Integer getLng() {
        return this.lng;
    }

    public final String getRegion() {
        return this.region;
    }

    public final String getSettlement() {
        return this.settlement;
    }

    public final List<String> getStateFacts() {
        return this.stateFacts;
    }

    public final String getStreet() {
        return this.street;
    }

    public final AddressType getType() {
        return this.type;
    }

    public final String getVrfDdt() {
        return this.vrfDdt;
    }

    public final String getZipCode() {
        return this.zipCode;
    }

    public int hashCode() {
        String str = this.additionArea;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.additionAreaStreet;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.addressStr;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.another;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.area;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.building;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.city;
        int hashCode7 = (hashCode6 + (str7 != null ? str7.hashCode() : 0)) * 31;
        String str8 = this.countryId;
        int hashCode8 = (hashCode7 + (str8 != null ? str8.hashCode() : 0)) * 31;
        String str9 = this.district;
        int hashCode9 = (hashCode8 + (str9 != null ? str9.hashCode() : 0)) * 31;
        String str10 = this.eTag;
        int hashCode10 = (hashCode9 + (str10 != null ? str10.hashCode() : 0)) * 31;
        String str11 = this.fiasCode;
        int hashCode11 = (hashCode10 + (str11 != null ? str11.hashCode() : 0)) * 31;
        String str12 = this.fiasCode2;
        int hashCode12 = (hashCode11 + (str12 != null ? str12.hashCode() : 0)) * 31;
        String str13 = this.flat;
        int hashCode13 = (hashCode12 + (str13 != null ? str13.hashCode() : 0)) * 31;
        String str14 = this.frame;
        int hashCode14 = (hashCode13 + (str14 != null ? str14.hashCode() : 0)) * 31;
        String str15 = this.house;
        int hashCode15 = (((hashCode14 + (str15 != null ? str15.hashCode() : 0)) * 31) + this.id) * 31;
        Integer num = this.lat;
        int hashCode16 = (hashCode15 + (num != null ? num.hashCode() : 0)) * 31;
        Integer num2 = this.lng;
        int hashCode17 = (hashCode16 + (num2 != null ? num2.hashCode() : 0)) * 31;
        String str16 = this.region;
        int hashCode18 = (hashCode17 + (str16 != null ? str16.hashCode() : 0)) * 31;
        String str17 = this.settlement;
        int hashCode19 = (hashCode18 + (str17 != null ? str17.hashCode() : 0)) * 31;
        List<String> list = this.stateFacts;
        int hashCode20 = (hashCode19 + (list != null ? list.hashCode() : 0)) * 31;
        String str18 = this.street;
        int hashCode21 = (hashCode20 + (str18 != null ? str18.hashCode() : 0)) * 31;
        AddressType addressType = this.type;
        int hashCode22 = (hashCode21 + (addressType != null ? addressType.hashCode() : 0)) * 31;
        String str19 = this.vrfDdt;
        int hashCode23 = (hashCode22 + (str19 != null ? str19.hashCode() : 0)) * 31;
        String str20 = this.zipCode;
        if (str20 != null) {
            i2 = str20.hashCode();
        }
        return hashCode23 + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("Address(additionArea=");
        a.append(this.additionArea);
        a.append(", additionAreaStreet=");
        a.append(this.additionAreaStreet);
        a.append(", addressStr=");
        a.append(this.addressStr);
        a.append(", another=");
        a.append(this.another);
        a.append(", area=");
        a.append(this.area);
        a.append(", building=");
        a.append(this.building);
        a.append(", city=");
        a.append(this.city);
        a.append(", countryId=");
        a.append(this.countryId);
        a.append(", district=");
        a.append(this.district);
        a.append(", eTag=");
        a.append(this.eTag);
        a.append(", fiasCode=");
        a.append(this.fiasCode);
        a.append(", fiasCode2=");
        a.append(this.fiasCode2);
        a.append(", flat=");
        a.append(this.flat);
        a.append(", frame=");
        a.append(this.frame);
        a.append(", house=");
        a.append(this.house);
        a.append(", id=");
        a.append(this.id);
        a.append(", lat=");
        a.append(this.lat);
        a.append(", lng=");
        a.append(this.lng);
        a.append(", region=");
        a.append(this.region);
        a.append(", settlement=");
        a.append(this.settlement);
        a.append(", stateFacts=");
        a.append(this.stateFacts);
        a.append(", street=");
        a.append(this.street);
        a.append(", type=");
        a.append(this.type);
        a.append(", vrfDdt=");
        a.append(this.vrfDdt);
        a.append(", zipCode=");
        return outline.a(a, this.zipCode, ")");
    }
}
