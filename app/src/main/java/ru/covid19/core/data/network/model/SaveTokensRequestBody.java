package ru.covid19.core.data.network.model;

import com.crashlytics.android.answers.SessionEventTransform;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import com.crashlytics.android.core.MetaDataStore;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: SaveTokensRequestBody.kt */
public final class SaveTokensRequestBody {
    public final String accessToken;
    public final String entToken;
    public final int expires;
    public final String id;
    public final String name;
    public final String refreshToken;
    public final String type;
    public final String userId;

    public SaveTokensRequestBody(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("id");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("accessToken");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        } else if (str5 == null) {
            Intrinsics.a(MetaDataStore.KEY_USER_ID);
            throw null;
        } else if (str6 == null) {
            Intrinsics.a("refreshToken");
            throw null;
        } else if (str7 != null) {
            this.expires = i2;
            this.name = str;
            this.id = str2;
            this.accessToken = str3;
            this.type = str4;
            this.userId = str5;
            this.refreshToken = str6;
            this.entToken = str7;
        } else {
            Intrinsics.a("entToken");
            throw null;
        }
    }

    public static /* synthetic */ SaveTokensRequestBody copy$default(SaveTokensRequestBody saveTokensRequestBody, int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7, int i3, Object obj) {
        SaveTokensRequestBody saveTokensRequestBody2 = saveTokensRequestBody;
        int i4 = i3;
        return saveTokensRequestBody.copy((i4 & 1) != 0 ? saveTokensRequestBody2.expires : i2, (i4 & 2) != 0 ? saveTokensRequestBody2.name : str, (i4 & 4) != 0 ? saveTokensRequestBody2.id : str2, (i4 & 8) != 0 ? saveTokensRequestBody2.accessToken : str3, (i4 & 16) != 0 ? saveTokensRequestBody2.type : str4, (i4 & 32) != 0 ? saveTokensRequestBody2.userId : str5, (i4 & 64) != 0 ? saveTokensRequestBody2.refreshToken : str6, (i4 & 128) != 0 ? saveTokensRequestBody2.entToken : str7);
    }

    public final int component1() {
        return this.expires;
    }

    public final String component2() {
        return this.name;
    }

    public final String component3() {
        return this.id;
    }

    public final String component4() {
        return this.accessToken;
    }

    public final String component5() {
        return this.type;
    }

    public final String component6() {
        return this.userId;
    }

    public final String component7() {
        return this.refreshToken;
    }

    public final String component8() {
        return this.entToken;
    }

    public final SaveTokensRequestBody copy(int i2, String str, String str2, String str3, String str4, String str5, String str6, String str7) {
        if (str == null) {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        } else if (str2 == null) {
            Intrinsics.a("id");
            throw null;
        } else if (str3 == null) {
            Intrinsics.a("accessToken");
            throw null;
        } else if (str4 == null) {
            Intrinsics.a(SessionEventTransform.TYPE_KEY);
            throw null;
        } else if (str5 == null) {
            Intrinsics.a(MetaDataStore.KEY_USER_ID);
            throw null;
        } else if (str6 == null) {
            Intrinsics.a("refreshToken");
            throw null;
        } else if (str7 != null) {
            return new SaveTokensRequestBody(i2, str, str2, str3, str4, str5, str6, str7);
        } else {
            Intrinsics.a("entToken");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SaveTokensRequestBody)) {
            return false;
        }
        SaveTokensRequestBody saveTokensRequestBody = (SaveTokensRequestBody) obj;
        return this.expires == saveTokensRequestBody.expires && Intrinsics.a(this.name, saveTokensRequestBody.name) && Intrinsics.a(this.id, saveTokensRequestBody.id) && Intrinsics.a(this.accessToken, saveTokensRequestBody.accessToken) && Intrinsics.a(this.type, saveTokensRequestBody.type) && Intrinsics.a(this.userId, saveTokensRequestBody.userId) && Intrinsics.a(this.refreshToken, saveTokensRequestBody.refreshToken) && Intrinsics.a(this.entToken, saveTokensRequestBody.entToken);
    }

    public final String getAccessToken() {
        return this.accessToken;
    }

    public final String getEntToken() {
        return this.entToken;
    }

    public final int getExpires() {
        return this.expires;
    }

    public final String getId() {
        return this.id;
    }

    public final String getName() {
        return this.name;
    }

    public final String getRefreshToken() {
        return this.refreshToken;
    }

    public final String getType() {
        return this.type;
    }

    public final String getUserId() {
        return this.userId;
    }

    public int hashCode() {
        int i2 = this.expires * 31;
        String str = this.name;
        int i3 = 0;
        int hashCode = (i2 + (str != null ? str.hashCode() : 0)) * 31;
        String str2 = this.id;
        int hashCode2 = (hashCode + (str2 != null ? str2.hashCode() : 0)) * 31;
        String str3 = this.accessToken;
        int hashCode3 = (hashCode2 + (str3 != null ? str3.hashCode() : 0)) * 31;
        String str4 = this.type;
        int hashCode4 = (hashCode3 + (str4 != null ? str4.hashCode() : 0)) * 31;
        String str5 = this.userId;
        int hashCode5 = (hashCode4 + (str5 != null ? str5.hashCode() : 0)) * 31;
        String str6 = this.refreshToken;
        int hashCode6 = (hashCode5 + (str6 != null ? str6.hashCode() : 0)) * 31;
        String str7 = this.entToken;
        if (str7 != null) {
            i3 = str7.hashCode();
        }
        return hashCode6 + i3;
    }

    public String toString() {
        StringBuilder a = outline.a("SaveTokensRequestBody(expires=");
        a.append(this.expires);
        a.append(", name=");
        a.append(this.name);
        a.append(", id=");
        a.append(this.id);
        a.append(", accessToken=");
        a.append(this.accessToken);
        a.append(", type=");
        a.append(this.type);
        a.append(", userId=");
        a.append(this.userId);
        a.append(", refreshToken=");
        a.append(this.refreshToken);
        a.append(", entToken=");
        return outline.a(a, this.entToken, ")");
    }
}
