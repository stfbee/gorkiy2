package ru.covid19.core.data.network.model;

/* compiled from: PersonalInfoByQrCodeResponse.kt */
public final class PersonalInfoByQrCodeResponse {
    public final String birthDate;
    public final PersonalInfoByQrCodeResponse0 type;
    public final String urlAvatar;

    public PersonalInfoByQrCodeResponse(PersonalInfoByQrCodeResponse0 personalInfoByQrCodeResponse0, String str, String str2) {
        this.type = personalInfoByQrCodeResponse0;
        this.birthDate = str;
        this.urlAvatar = str2;
    }

    public final String getBirthDate() {
        return this.birthDate;
    }

    public final PersonalInfoByQrCodeResponse0 getType() {
        return this.type;
    }

    public final String getUrlAvatar() {
        return this.urlAvatar;
    }
}
