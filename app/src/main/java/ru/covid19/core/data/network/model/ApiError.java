package ru.covid19.core.data.network.model;

import com.crashlytics.android.core.SessionProtobufHelper;
import j.a.a.a.outline;
import j.c.d.a0.SerializedName;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: ApiError.kt */
public final class ApiError {
    @SerializedName(alternate = {"code"}, value = "errorCode")
    public final String errorCode;
    @SerializedName(alternate = {"message"}, value = "errorMessage")
    public final String errorMessage;

    public ApiError(String str, String str2) {
        if (str == null) {
            Intrinsics.a("errorCode");
            throw null;
        } else if (str2 != null) {
            this.errorCode = str;
            this.errorMessage = str2;
        } else {
            Intrinsics.a("errorMessage");
            throw null;
        }
    }

    public static /* synthetic */ ApiError copy$default(ApiError apiError, String str, String str2, int i2, Object obj) {
        if ((i2 & 1) != 0) {
            str = apiError.errorCode;
        }
        if ((i2 & 2) != 0) {
            str2 = apiError.errorMessage;
        }
        return apiError.copy(str, str2);
    }

    public final String component1() {
        return this.errorCode;
    }

    public final String component2() {
        return this.errorMessage;
    }

    public final ApiError copy(String str, String str2) {
        if (str == null) {
            Intrinsics.a("errorCode");
            throw null;
        } else if (str2 != null) {
            return new ApiError(str, str2);
        } else {
            Intrinsics.a("errorMessage");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ApiError)) {
            return false;
        }
        ApiError apiError = (ApiError) obj;
        return Intrinsics.a(this.errorCode, apiError.errorCode) && Intrinsics.a(this.errorMessage, apiError.errorMessage);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean
     arg types: [java.lang.String, java.lang.String, int]
     candidates:
      n.r.Indent.a(java.lang.String, java.lang.String, int):java.lang.String
      n.r.Indent.a(java.lang.String, java.lang.String, boolean):boolean
      n.r.Indent.a(java.lang.CharSequence, java.lang.CharSequence, boolean):boolean */
    public final boolean getCodeContainsEsiaError() {
        return Indent.a((CharSequence) this.errorCode, (CharSequence) "esia", true);
    }

    public final String getErrorCode() {
        return this.errorCode;
    }

    public final String getErrorMessage() {
        return this.errorMessage;
    }

    public final boolean getHasError() {
        return (Indent.b(this.errorCode) ^ true) && (Intrinsics.a(this.errorCode, SessionProtobufHelper.SIGNAL_DEFAULT) ^ true);
    }

    public int hashCode() {
        String str = this.errorCode;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        String str2 = this.errorMessage;
        if (str2 != null) {
            i2 = str2.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a = outline.a("ApiError(errorCode=");
        a.append(this.errorCode);
        a.append(", errorMessage=");
        return outline.a(a, this.errorMessage, ")");
    }
}
