package ru.covid19.core.data.network.model;

/* compiled from: PersonalResponse.kt */
public enum PersonalResponse1 {
    VERIFIED("VERIFIED"),
    NOT_VERIFIED("NOT_VERIFIED");
    
    public final String status;

    /* access modifiers changed from: public */
    PersonalResponse1(String str) {
        this.status = str;
    }

    public final String getStatus() {
        return this.status;
    }
}
