package q.b.a.s;

import j.a.a.a.outline;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;

public enum IsoEra implements i {
    BCE,
    CE;

    public static IsoEra a(int i2) {
        if (i2 == 0) {
            return BCE;
        }
        if (i2 == 1) {
            return CE;
        }
        throw new DateTimeException(outline.b("Invalid era: ", i2));
    }

    public int b(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return ordinal();
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.IsoEra] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.ERA) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, java.lang.Enum, q.b.a.s.IsoEra] */
    public long d(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return (long) ordinal();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public int getValue() {
        return ordinal();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.IsoEra] */
    public ValueRange a(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return temporalField.g();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.ERA, (long) ordinal());
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.s.IsoEra] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.ERAS;
        }
        if (temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g) {
            return null;
        }
        return temporalQuery.a(this);
    }
}
