package q.b.a.s;

import j.a.a.a.outline;
import java.io.DataInput;
import java.io.InvalidObjectException;
import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.ServiceLoader;
import java.util.concurrent.ConcurrentHashMap;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import q.b.a.LocalTime;
import q.b.a.c;
import q.b.a.f;
import q.b.a.o;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalQueries;
import q.b.a.v.a;
import q.b.a.v.d;
import q.b.a.v.e;
import q.b.a.v.j;

public abstract class Chronology implements Comparable<h> {
    public static final ConcurrentHashMap<String, h> b = new ConcurrentHashMap<>();
    public static final ConcurrentHashMap<String, h> c = new ConcurrentHashMap<>();

    static {
        try {
            Locale.class.getMethod("getUnicodeLocaleType", String.class);
        } catch (Throwable unused) {
        }
    }

    public static void b(Chronology chronology) {
        b.putIfAbsent(chronology.g(), chronology);
        String f2 = chronology.f();
        if (f2 != null) {
            c.putIfAbsent(f2, chronology);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalAccessor, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static Chronology c(TemporalAccessor temporalAccessor) {
        Collections.a((Object) temporalAccessor, "temporal");
        Chronology chronology = (Chronology) temporalAccessor.a(TemporalQueries.b);
        return chronology != null ? chronology : IsoChronology.d;
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 11, this);
    }

    public abstract ChronoLocalDate a(int i2, int i3, int i4);

    public <D extends b> D a(d dVar) {
        D d = (ChronoLocalDate) dVar;
        if (equals(d.i())) {
            return d;
        }
        StringBuilder a = outline.a("Chrono mismatch, expected: ");
        a.append(g());
        a.append(", actual: ");
        a.append(d.i().g());
        throw new ClassCastException(a.toString());
    }

    public abstract ChronoLocalDate a(TemporalAccessor temporalAccessor);

    public abstract Era a(int i2);

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Chronology) || compareTo((Chronology) obj) != 0) {
            return false;
        }
        return true;
    }

    public abstract String f();

    public abstract String g();

    public int hashCode() {
        return getClass().hashCode() ^ g().hashCode();
    }

    public String toString() {
        return g();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R>
     arg types: [q.b.a.s.Chronology, q.b.a.c, q.b.a.o]
     candidates:
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.ChronoLocalDateTimeImpl, q.b.a.o, q.b.a.p):q.b.a.s.ChronoZonedDateTime<R>
      q.b.a.s.ChronoZonedDateTimeImpl.a(q.b.a.s.h, q.b.a.c, q.b.a.o):q.b.a.s.ChronoZonedDateTimeImpl<R> */
    public ChronoZonedDateTime<?> a(c cVar, o oVar) {
        return ChronoZonedDateTimeImpl.a((h) this, cVar, oVar);
    }

    public <D extends b> ChronoLocalDateTimeImpl<D> b(d dVar) {
        ChronoLocalDateTimeImpl<D> chronoLocalDateTimeImpl = (ChronoLocalDateTimeImpl) dVar;
        if (equals(chronoLocalDateTimeImpl.b.i())) {
            return chronoLocalDateTimeImpl;
        }
        StringBuilder a = outline.a("Chrono mismatch, required: ");
        a.append(g());
        a.append(", supplied: ");
        a.append(chronoLocalDateTimeImpl.b.i().g());
        throw new ClassCastException(a.toString());
    }

    public void a(Map<j, Long> map, a aVar, long j2) {
        Long l2 = map.get(aVar);
        if (l2 == null || l2.longValue() == j2) {
            map.put(aVar, Long.valueOf(j2));
            return;
        }
        throw new DateTimeException("Invalid state, field: " + aVar + " " + l2 + " conflicts with " + aVar + " " + j2);
    }

    public <D extends b> ChronoZonedDateTimeImpl<D> c(d dVar) {
        ChronoZonedDateTimeImpl<D> chronoZonedDateTimeImpl = (ChronoZonedDateTimeImpl) dVar;
        if (equals(chronoZonedDateTimeImpl.o().i())) {
            return chronoZonedDateTimeImpl;
        }
        StringBuilder a = outline.a("Chrono mismatch, required: ");
        a.append(g());
        a.append(", supplied: ");
        a.append(chronoZonedDateTimeImpl.o().i().g());
        throw new ClassCastException(a.toString());
    }

    /* renamed from: a */
    public int compareTo(Chronology chronology) {
        return g().compareTo(chronology.g());
    }

    public static Chronology a(DataInput dataInput) {
        String readUTF = dataInput.readUTF();
        if (b.isEmpty()) {
            b(IsoChronology.d);
            b(ThaiBuddhistChronology.d);
            b(MinguoChronology.d);
            b(JapaneseChronology.f3116e);
            b(HijrahChronology.d);
            b.putIfAbsent("Hijrah", HijrahChronology.d);
            c.putIfAbsent("islamic", HijrahChronology.d);
            Iterator it = ServiceLoader.load(Chronology.class, Chronology.class.getClassLoader()).iterator();
            while (it.hasNext()) {
                Chronology chronology = (Chronology) it.next();
                b.putIfAbsent(chronology.g(), chronology);
                String f2 = chronology.f();
                if (f2 != null) {
                    c.putIfAbsent(f2, chronology);
                }
            }
        }
        Chronology chronology2 = b.get(readUTF);
        if (chronology2 != null || (chronology2 = (Chronology) c.get(readUTF)) != null) {
            return chronology2;
        }
        throw new DateTimeException(outline.a("Unknown chronology: ", readUTF));
    }

    public ChronoLocalDateTime<?> b(e eVar) {
        try {
            return a((TemporalAccessor) eVar).a((f) LocalTime.a((TemporalAccessor) eVar));
        } catch (DateTimeException e2) {
            StringBuilder a = outline.a("Unable to obtain ChronoLocalDateTime from TemporalAccessor: ");
            a.append(eVar.getClass());
            throw new DateTimeException(a.toString(), e2);
        }
    }
}
