package q.b.a.u;

import j.a.a.a.outline;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.v.ChronoField;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;

public abstract class DefaultInterfaceTemporalAccessor implements TemporalAccessor {
    public ValueRange a(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.c(this);
        }
        if (c(temporalField)) {
            return temporalField.g();
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public int b(TemporalField temporalField) {
        return a(temporalField).a(d(temporalField), temporalField);
    }

    public <R> R a(TemporalQuery<R> temporalQuery) {
        if (temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.c) {
            return null;
        }
        return temporalQuery.a(this);
    }
}
