package q.b.a.u;

import j.a.a.a.outline;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.s.Era;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;

public abstract class DefaultInterfaceEra extends DefaultInterfaceTemporalAccessor implements Era {
    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.ERA, (long) getValue());
    }

    public int b(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return getValue();
        }
        return a(temporalField).a(d(temporalField), temporalField);
    }

    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            if (temporalField == ChronoField.ERA) {
                return true;
            }
            return false;
        } else if (temporalField == null || !temporalField.a(this)) {
            return false;
        } else {
            return true;
        }
    }

    public long d(TemporalField temporalField) {
        if (temporalField == ChronoField.ERA) {
            return (long) getValue();
        }
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
    }

    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.ERAS;
        }
        if (temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.f3151f || temporalQuery == TemporalQueries.g) {
            return null;
        }
        return temporalQuery.a(this);
    }
}
