package q.b.a.w;

import j.a.a.a.outline;
import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;
import n.i.Collections;
import org.threeten.bp.zone.ZoneRulesException;
import q.b.a.w.TzdbZoneRulesProvider;
import q.b.a.w.ZoneRulesInitializer;

public abstract class ZoneRulesProvider {
    public static final CopyOnWriteArrayList<h> a = new CopyOnWriteArrayList<>();
    public static final ConcurrentMap<String, h> b = new ConcurrentHashMap(512, 0.75f, 2);

    static {
        if (!ZoneRulesInitializer.a.getAndSet(true)) {
            ZoneRulesInitializer.b.compareAndSet(null, new ZoneRulesInitializer.a());
            ZoneRulesInitializer.b.get().a();
            return;
        }
        throw new IllegalStateException("Already initialized");
    }

    /* JADX INFO: additional move instructions added (1) to help type inference */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static ZoneRules a(String str, boolean z) {
        ZoneRules zoneRules;
        Collections.a((Object) str, "zoneId");
        ZoneRulesProvider zoneRulesProvider = b.get(str);
        if (zoneRulesProvider != null) {
            Collections.a((Object) str, "zoneId");
            TzdbZoneRulesProvider.a value = ((TzdbZoneRulesProvider) zoneRulesProvider).d.lastEntry().getValue();
            int binarySearch = Arrays.binarySearch(value.b, str);
            if (binarySearch < 0) {
                zoneRules = null;
            } else {
                try {
                    short s2 = value.c[binarySearch];
                    Object obj = value.d.get(s2);
                    boolean z2 = obj instanceof byte[];
                    Object obj2 = obj;
                    if (z2) {
                        DataInputStream dataInputStream = new DataInputStream(new ByteArrayInputStream((byte[]) obj));
                        Object a2 = Ser.a(dataInputStream.readByte(), dataInputStream);
                        value.d.set(s2, a2);
                        obj2 = a2;
                    }
                    zoneRules = (ZoneRules) obj2;
                } catch (Exception e2) {
                    throw new ZoneRulesException("Invalid binary time-zone data: TZDB:" + str + ", version: " + value.a, e2);
                }
            }
            if (zoneRules != null) {
                return zoneRules;
            }
            throw new ZoneRulesException(outline.a("Unknown time-zone ID: ", str));
        } else if (b.isEmpty()) {
            throw new ZoneRulesException("No time-zone data files registered");
        } else {
            throw new ZoneRulesException(outline.a("Unknown time-zone ID: ", str));
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.w.ZoneRulesProvider, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public static void a(ZoneRulesProvider zoneRulesProvider) {
        Collections.a((Object) zoneRulesProvider, "provider");
        Iterator it = new HashSet(((TzdbZoneRulesProvider) zoneRulesProvider).c).iterator();
        while (it.hasNext()) {
            String str = (String) it.next();
            Collections.a((Object) str, "zoneId");
            if (b.putIfAbsent(str, zoneRulesProvider) != null) {
                throw new ZoneRulesException("Unable to register zone as one already registered with that ID: " + str + ", currently loading from provider: " + zoneRulesProvider);
            }
        }
        a.add(zoneRulesProvider);
    }
}
