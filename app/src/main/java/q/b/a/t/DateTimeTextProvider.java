package q.b.a.t;

import java.util.Iterator;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.atomic.AtomicReference;
import q.b.a.v.TemporalField;
import q.b.a.v.j;

public abstract class DateTimeTextProvider {
    public static final AtomicReference<g> a = new AtomicReference<>();

    public static class a {
        public static final DateTimeTextProvider a = DateTimeTextProvider.a.get();

        static {
            DateTimeTextProvider.a.compareAndSet(null, new SimpleDateTimeTextProvider());
        }
    }

    public static DateTimeTextProvider a() {
        return a.a;
    }

    public abstract String a(TemporalField temporalField, long j2, TextStyle textStyle, Locale locale);

    public abstract Iterator<Map.Entry<String, Long>> a(j jVar, l lVar, Locale locale);
}
