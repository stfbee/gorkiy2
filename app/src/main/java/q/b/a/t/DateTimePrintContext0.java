package q.b.a.t;

import q.b.a.ZoneId;
import q.b.a.s.ChronoLocalDate;
import q.b.a.s.Chronology;
import q.b.a.u.DefaultInterfaceTemporalAccessor;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.ValueRange;

/* compiled from: DateTimePrintContext */
public class DateTimePrintContext0 extends DefaultInterfaceTemporalAccessor {
    public final /* synthetic */ ChronoLocalDate b;
    public final /* synthetic */ TemporalAccessor c;
    public final /* synthetic */ Chronology d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ ZoneId f3148e;

    public DateTimePrintContext0(ChronoLocalDate chronoLocalDate, TemporalAccessor temporalAccessor, Chronology chronology, ZoneId zoneId) {
        this.b = chronoLocalDate;
        this.c = temporalAccessor;
        this.d = chronology;
        this.f3148e = zoneId;
    }

    public <R> R a(TemporalQuery<R> temporalQuery) {
        if (temporalQuery == TemporalQueries.b) {
            return this.d;
        }
        if (temporalQuery == TemporalQueries.a) {
            return this.f3148e;
        }
        if (temporalQuery == TemporalQueries.c) {
            return this.c.a(temporalQuery);
        }
        return temporalQuery.a(this);
    }

    public boolean c(TemporalField temporalField) {
        if (this.b == null || !temporalField.f()) {
            return this.c.c(temporalField);
        }
        return this.b.c(temporalField);
    }

    /* JADX WARN: Type inference failed for: r0v4, types: [q.b.a.v.TemporalAccessor, q.b.a.s.ChronoLocalDate] */
    public long d(TemporalField temporalField) {
        if (this.b == null || !temporalField.f()) {
            return this.c.d(temporalField);
        }
        return this.b.d(temporalField);
    }

    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.s.ChronoLocalDate, q.b.a.u.DefaultInterfaceTemporalAccessor] */
    public ValueRange a(TemporalField temporalField) {
        if (this.b == null || !temporalField.f()) {
            return this.c.a(temporalField);
        }
        return this.b.a(temporalField);
    }
}
