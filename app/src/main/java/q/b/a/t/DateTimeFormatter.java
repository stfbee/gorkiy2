package q.b.a.t;

import com.crashlytics.android.answers.SessionEventTransform;
import java.io.IOException;
import java.text.ParsePosition;
import java.util.HashMap;
import java.util.Locale;
import java.util.Set;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.format.DateTimeParseException;
import q.b.a.ZoneId;
import q.b.a.o;
import q.b.a.s.Chronology;
import q.b.a.s.IsoChronology;
import q.b.a.s.h;
import q.b.a.t.DateTimeFormatterBuilder;
import q.b.a.t.DateTimeParseContext;
import q.b.a.t.c;
import q.b.a.v.ChronoField;
import q.b.a.v.IsoFields;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQuery;
import q.b.a.v.j;

public final class DateTimeFormatter {
    public static final DateTimeFormatter h;

    /* renamed from: i  reason: collision with root package name */
    public static final DateTimeFormatter f3126i;

    /* renamed from: j  reason: collision with root package name */
    public static final DateTimeFormatter f3127j;

    /* renamed from: k  reason: collision with root package name */
    public static final DateTimeFormatter f3128k;

    /* renamed from: l  reason: collision with root package name */
    public static final DateTimeFormatter f3129l;

    /* renamed from: m  reason: collision with root package name */
    public static final DateTimeFormatter f3130m;
    public final DateTimeFormatterBuilder.e a;
    public final Locale b;
    public final DecimalStyle c;
    public final ResolverStyle d;

    /* renamed from: e  reason: collision with root package name */
    public final Set<j> f3131e;

    /* renamed from: f  reason: collision with root package name */
    public final Chronology f3132f;
    public final ZoneId g;

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v5, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r2v10, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r2v15, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v4, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r6v8, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r6v12, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r6v18, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r7v1, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r6v25, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r6v29, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r6v30, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v31, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r6v35, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r1v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v6, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r1v9, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r1v10, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v11, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r1v12, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v12, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r3v13, types: [q.b.a.t.DateTimeFormatterBuilder$f, q.b.a.t.DateTimeFormatterBuilder$n] */
    /* JADX WARN: Type inference failed for: r3v14, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v32, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v16, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v17, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v18, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r3v19, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r2v17, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, boolean):q.b.a.t.DateTimeFormatterBuilder
     arg types: [?, int, int, int]
     candidates:
      q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, q.b.a.t.SignStyle):q.b.a.t.DateTimeFormatterBuilder
      q.b.a.t.DateTimeFormatterBuilder.a(q.b.a.v.TemporalField, int, int, boolean):q.b.a.t.DateTimeFormatterBuilder */
    static {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD);
        dateTimeFormatterBuilder.a('-');
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.MONTH_OF_YEAR, 2);
        dateTimeFormatterBuilder.a('-');
        dateTimeFormatterBuilder.a((TemporalField) ChronoField.DAY_OF_MONTH, 2);
        h = dateTimeFormatterBuilder.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder2 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder2.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder2.a(h);
        dateTimeFormatterBuilder2.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder2.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder3 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder3.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder3.a(h);
        dateTimeFormatterBuilder3.b();
        dateTimeFormatterBuilder3.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder3.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder4 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder4.a((TemporalField) ChronoField.HOUR_OF_DAY, 2);
        dateTimeFormatterBuilder4.a(':');
        dateTimeFormatterBuilder4.a((TemporalField) ChronoField.MINUTE_OF_HOUR, 2);
        dateTimeFormatterBuilder4.b();
        dateTimeFormatterBuilder4.a(':');
        dateTimeFormatterBuilder4.a((TemporalField) ChronoField.SECOND_OF_MINUTE, 2);
        dateTimeFormatterBuilder4.b();
        dateTimeFormatterBuilder4.a((TemporalField) ChronoField.NANO_OF_SECOND, 0, 9, true);
        f3126i = dateTimeFormatterBuilder4.a(ResolverStyle.STRICT);
        DateTimeFormatterBuilder dateTimeFormatterBuilder5 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder5.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder5.a(f3126i);
        dateTimeFormatterBuilder5.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder5.a(ResolverStyle.STRICT);
        DateTimeFormatterBuilder dateTimeFormatterBuilder6 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder6.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder6.a(f3126i);
        dateTimeFormatterBuilder6.b();
        dateTimeFormatterBuilder6.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder6.a(ResolverStyle.STRICT);
        DateTimeFormatterBuilder dateTimeFormatterBuilder7 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder7.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder7.a(h);
        dateTimeFormatterBuilder7.a('T');
        dateTimeFormatterBuilder7.a(f3126i);
        f3127j = dateTimeFormatterBuilder7.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder8 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder8.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder8.a(f3127j);
        dateTimeFormatterBuilder8.a(DateTimeFormatterBuilder.k.f3140e);
        f3128k = dateTimeFormatterBuilder8.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder9 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder9.a(f3128k);
        dateTimeFormatterBuilder9.b();
        dateTimeFormatterBuilder9.a('[');
        dateTimeFormatterBuilder9.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.SENSITIVE);
        dateTimeFormatterBuilder9.a(new DateTimeFormatterBuilder.r(DateTimeFormatterBuilder.h, "ZoneRegionId()"));
        dateTimeFormatterBuilder9.a(']');
        dateTimeFormatterBuilder9.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder10 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder10.a(f3127j);
        dateTimeFormatterBuilder10.b();
        dateTimeFormatterBuilder10.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder10.b();
        dateTimeFormatterBuilder10.a('[');
        dateTimeFormatterBuilder10.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.SENSITIVE);
        dateTimeFormatterBuilder10.a(new DateTimeFormatterBuilder.r(DateTimeFormatterBuilder.h, "ZoneRegionId()"));
        dateTimeFormatterBuilder10.a(']');
        f3129l = dateTimeFormatterBuilder10.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder11 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder11.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder11.a((TemporalField) ChronoField.YEAR, 4, 10, SignStyle.EXCEEDS_PAD);
        dateTimeFormatterBuilder11.a('-');
        dateTimeFormatterBuilder11.a((TemporalField) ChronoField.DAY_OF_YEAR, 3);
        dateTimeFormatterBuilder11.b();
        dateTimeFormatterBuilder11.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder11.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder12 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder12.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder12.a(IsoFields.c, 4, 10, SignStyle.EXCEEDS_PAD);
        dateTimeFormatterBuilder12.a("-W");
        dateTimeFormatterBuilder12.a(IsoFields.b, 2);
        dateTimeFormatterBuilder12.a('-');
        dateTimeFormatterBuilder12.a((TemporalField) ChronoField.DAY_OF_WEEK, 1);
        dateTimeFormatterBuilder12.b();
        dateTimeFormatterBuilder12.a(DateTimeFormatterBuilder.k.f3140e);
        dateTimeFormatterBuilder12.a(ResolverStyle.STRICT).a(IsoChronology.d);
        DateTimeFormatterBuilder dateTimeFormatterBuilder13 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder13.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder13.a(new DateTimeFormatterBuilder.h(-2));
        f3130m = dateTimeFormatterBuilder13.a(ResolverStyle.STRICT);
        DateTimeFormatterBuilder dateTimeFormatterBuilder14 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder14.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder14.a((TemporalField) ChronoField.YEAR, 4);
        dateTimeFormatterBuilder14.a((TemporalField) ChronoField.MONTH_OF_YEAR, 2);
        dateTimeFormatterBuilder14.a((TemporalField) ChronoField.DAY_OF_MONTH, 2);
        dateTimeFormatterBuilder14.b();
        dateTimeFormatterBuilder14.a("+HHMMss", "Z");
        dateTimeFormatterBuilder14.a(ResolverStyle.STRICT).a(IsoChronology.d);
        HashMap hashMap = new HashMap();
        hashMap.put(1L, "Mon");
        hashMap.put(2L, "Tue");
        hashMap.put(3L, "Wed");
        hashMap.put(4L, "Thu");
        hashMap.put(5L, "Fri");
        hashMap.put(6L, "Sat");
        hashMap.put(7L, "Sun");
        HashMap hashMap2 = new HashMap();
        hashMap2.put(1L, "Jan");
        hashMap2.put(2L, "Feb");
        hashMap2.put(3L, "Mar");
        hashMap2.put(4L, "Apr");
        hashMap2.put(5L, "May");
        hashMap2.put(6L, "Jun");
        hashMap2.put(7L, "Jul");
        hashMap2.put(8L, "Aug");
        hashMap2.put(9L, "Sep");
        hashMap2.put(10L, "Oct");
        hashMap2.put(11L, "Nov");
        hashMap2.put(12L, "Dec");
        DateTimeFormatterBuilder dateTimeFormatterBuilder15 = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder15.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.INSENSITIVE);
        dateTimeFormatterBuilder15.a((DateTimeFormatterBuilder.f) DateTimeFormatterBuilder.n.LENIENT);
        dateTimeFormatterBuilder15.b();
        dateTimeFormatterBuilder15.a((j) ChronoField.DAY_OF_WEEK, hashMap);
        dateTimeFormatterBuilder15.a(", ");
        dateTimeFormatterBuilder15.a();
        dateTimeFormatterBuilder15.a((TemporalField) ChronoField.DAY_OF_MONTH, 1, 2, SignStyle.NOT_NEGATIVE);
        dateTimeFormatterBuilder15.a(' ');
        dateTimeFormatterBuilder15.a((j) ChronoField.MONTH_OF_YEAR, hashMap2);
        dateTimeFormatterBuilder15.a(' ');
        dateTimeFormatterBuilder15.a((TemporalField) ChronoField.YEAR, 4);
        dateTimeFormatterBuilder15.a(' ');
        dateTimeFormatterBuilder15.a((TemporalField) ChronoField.HOUR_OF_DAY, 2);
        dateTimeFormatterBuilder15.a(':');
        dateTimeFormatterBuilder15.a((TemporalField) ChronoField.MINUTE_OF_HOUR, 2);
        dateTimeFormatterBuilder15.b();
        dateTimeFormatterBuilder15.a(':');
        dateTimeFormatterBuilder15.a((TemporalField) ChronoField.SECOND_OF_MINUTE, 2);
        dateTimeFormatterBuilder15.a();
        dateTimeFormatterBuilder15.a(' ');
        dateTimeFormatterBuilder15.a("+HHMM", "GMT");
        dateTimeFormatterBuilder15.a(ResolverStyle.SMART).a(IsoChronology.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.c$e, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.Locale, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.h, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.t.i, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public DateTimeFormatter(c.e eVar, Locale locale, h hVar, i iVar, Set<j> set, h hVar2, o oVar) {
        Collections.a((Object) eVar, "printerParser");
        this.a = (DateTimeFormatterBuilder.e) eVar;
        Collections.a((Object) locale, "locale");
        this.b = locale;
        Collections.a((Object) hVar, "decimalStyle");
        this.c = (DecimalStyle) hVar;
        Collections.a((Object) iVar, "resolverStyle");
        this.d = (ResolverStyle) iVar;
        this.f3131e = set;
        this.f3132f = hVar2;
        this.g = oVar;
    }

    public static DateTimeFormatter a(String str) {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder.b(str);
        return dateTimeFormatterBuilder.c();
    }

    public String toString() {
        String eVar = this.a.toString();
        return eVar.startsWith("[") ? eVar : eVar.substring(1, eVar.length() - 1);
    }

    public static DateTimeFormatter a(String str, Locale locale) {
        DateTimeFormatterBuilder dateTimeFormatterBuilder = new DateTimeFormatterBuilder();
        dateTimeFormatterBuilder.b(str);
        return dateTimeFormatterBuilder.a(locale);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
     arg types: [q.b.a.s.Chronology, q.b.a.s.Chronology]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean */
    public DateTimeFormatter a(Chronology chronology) {
        if (Collections.a((Object) this.f3132f, (Object) chronology)) {
            return this;
        }
        return new DateTimeFormatter(this.a, this.b, this.c, this.d, this.f3131e, chronology, this.g);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalAccessor, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.StringBuilder, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public String a(TemporalAccessor temporalAccessor) {
        StringBuilder sb = new StringBuilder(32);
        Collections.a((Object) temporalAccessor, "temporal");
        Collections.a((Object) sb, "appendable");
        try {
            this.a.a(new DateTimePrintContext(temporalAccessor, this), sb);
            return sb.toString();
        } catch (IOException e2) {
            throw new DateTimeException(e2.getMessage(), e2);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.CharSequence, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.TemporalQuery, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.t.DateTimeBuilder.a(q.b.a.t.i, java.util.Set<q.b.a.v.j>):q.b.a.t.a
     arg types: [q.b.a.t.ResolverStyle, java.util.Set<q.b.a.v.j>]
     candidates:
      q.b.a.t.DateTimeBuilder.a(q.b.a.v.TemporalField, q.b.a.LocalTime):void
      q.b.a.t.DateTimeBuilder.a(q.b.a.v.TemporalField, q.b.a.s.ChronoLocalDate):void
      q.b.a.t.DateTimeBuilder.a(q.b.a.t.i, java.util.Set<q.b.a.v.j>):q.b.a.t.a */
    public <T> T a(CharSequence charSequence, TemporalQuery temporalQuery) {
        String str;
        Collections.a((Object) charSequence, "text");
        Collections.a((Object) temporalQuery, SessionEventTransform.TYPE_KEY);
        try {
            DateTimeBuilder a2 = a(charSequence, (ParsePosition) null);
            a2.a((i) this.d, this.f3131e);
            return temporalQuery.a(a2);
        } catch (DateTimeParseException e2) {
            throw e2;
        } catch (RuntimeException e3) {
            if (charSequence.length() > 64) {
                str = charSequence.subSequence(0, 64).toString() + "...";
            } else {
                str = charSequence.toString();
            }
            throw new DateTimeParseException("Text '" + str + "' could not be parsed: " + e3.getMessage(), charSequence, 0, e3);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.CharSequence, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [java.text.ParsePosition, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public final DateTimeBuilder a(CharSequence charSequence, ParsePosition parsePosition) {
        DateTimeParseContext.a aVar;
        String str;
        ParsePosition parsePosition2 = parsePosition != null ? parsePosition : new ParsePosition(0);
        Collections.a((Object) charSequence, "text");
        Collections.a((Object) parsePosition2, "position");
        DateTimeParseContext dateTimeParseContext = new DateTimeParseContext(this);
        int a2 = this.a.a(dateTimeParseContext, charSequence, parsePosition2.getIndex());
        if (a2 < 0) {
            parsePosition2.setErrorIndex(~a2);
            aVar = null;
        } else {
            parsePosition2.setIndex(a2);
            aVar = dateTimeParseContext.a();
        }
        if (aVar == null || parsePosition2.getErrorIndex() >= 0 || (parsePosition == null && parsePosition2.getIndex() < charSequence.length())) {
            if (charSequence.length() > 64) {
                str = charSequence.subSequence(0, 64).toString() + "...";
            } else {
                str = charSequence.toString();
            }
            if (parsePosition2.getErrorIndex() >= 0) {
                throw new DateTimeParseException("Text '" + str + "' could not be parsed at index " + parsePosition2.getErrorIndex(), charSequence, parsePosition2.getErrorIndex());
            }
            throw new DateTimeParseException("Text '" + str + "' could not be parsed, unparsed text found at index " + parsePosition2.getIndex(), charSequence, parsePosition2.getIndex());
        }
        DateTimeBuilder dateTimeBuilder = new DateTimeBuilder();
        dateTimeBuilder.b.putAll(aVar.d);
        DateTimeParseContext dateTimeParseContext2 = DateTimeParseContext.this;
        Chronology chronology = dateTimeParseContext2.a().b;
        if (chronology == null && (chronology = dateTimeParseContext2.c) == null) {
            chronology = IsoChronology.d;
        }
        dateTimeBuilder.c = chronology;
        ZoneId zoneId = aVar.c;
        if (zoneId != null) {
            dateTimeBuilder.d = zoneId;
        } else {
            dateTimeBuilder.d = DateTimeParseContext.this.d;
        }
        dateTimeBuilder.g = aVar.f3146e;
        dateTimeBuilder.h = aVar.f3147f;
        return dateTimeBuilder;
    }
}
