package q.b.a;

import androidx.recyclerview.widget.RecyclerView;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import com.crashlytics.android.answers.RetryManager;
import com.crashlytics.android.core.SessionProtobufHelper;
import j.a.a.a.outline;
import java.io.DataInput;
import java.io.DataOutput;
import java.io.InvalidObjectException;
import java.io.Serializable;
import n.i.Collections;
import org.threeten.bp.DateTimeException;
import org.threeten.bp.temporal.UnsupportedTemporalTypeException;
import q.b.a.u.c;
import q.b.a.v.ChronoField;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;
import q.b.a.v.TemporalAccessor;
import q.b.a.v.TemporalAdjuster;
import q.b.a.v.TemporalField;
import q.b.a.v.TemporalQueries;
import q.b.a.v.TemporalQuery;
import q.b.a.v.TemporalUnit;
import q.b.a.v.ValueRange;
import q.b.a.v.d;
import q.b.a.v.f;

public final class LocalTime extends c implements d, f, Comparable<f>, Serializable {

    /* renamed from: f  reason: collision with root package name */
    public static final LocalTime f3092f;
    public static final LocalTime g = new LocalTime(23, 59, 59, 999999999);
    public static final LocalTime h;

    /* renamed from: i  reason: collision with root package name */
    public static final LocalTime[] f3093i = new LocalTime[24];
    public final byte b;
    public final byte c;
    public final byte d;

    /* renamed from: e  reason: collision with root package name */
    public final int f3094e;

    static {
        int i2 = 0;
        while (true) {
            LocalTime[] localTimeArr = f3093i;
            if (i2 < localTimeArr.length) {
                localTimeArr[i2] = new LocalTime(i2, 0, 0, 0);
                i2++;
            } else {
                h = localTimeArr[0];
                LocalTime localTime = localTimeArr[12];
                f3092f = localTimeArr[0];
                return;
            }
        }
    }

    public LocalTime(int i2, int i3, int i4, int i5) {
        this.b = (byte) i2;
        this.c = (byte) i3;
        this.d = (byte) i4;
        this.f3094e = i5;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalTime e(long j2) {
        ? r0 = ChronoField.NANO_OF_DAY;
        r0.range.b(j2, r0);
        int i2 = (int) (j2 / 3600000000000L);
        long j3 = j2 - (((long) i2) * 3600000000000L);
        int i3 = (int) (j3 / 60000000000L);
        long j4 = j3 - (((long) i3) * 60000000000L);
        int i4 = (int) (j4 / 1000000000);
        return a(i2, i3, i4, (int) (j4 - (((long) i4) * 1000000000)));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalTime f(long j2) {
        ? r0 = ChronoField.SECOND_OF_DAY;
        r0.range.b(j2, r0);
        int i2 = (int) (j2 / 3600);
        long j3 = j2 - ((long) (i2 * 3600));
        int i3 = (int) (j3 / 60);
        return a(i2, i3, (int) (j3 - ((long) (i3 * 60))), 0);
    }

    private Object readResolve() {
        throw new InvalidObjectException("Deserialization via serialization delegate");
    }

    private Object writeReplace() {
        return new Ser((byte) 5, this);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalTime] */
    public boolean c(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return temporalField.h();
        }
        return temporalField != null && temporalField.a(this);
    }

    /* JADX WARN: Type inference failed for: r4v0, types: [q.b.a.v.TemporalAccessor, q.b.a.LocalTime] */
    public long d(TemporalField temporalField) {
        if (!(temporalField instanceof ChronoField)) {
            return temporalField.b(this);
        }
        if (temporalField == ChronoField.NANO_OF_DAY) {
            return i();
        }
        if (temporalField == ChronoField.MICRO_OF_DAY) {
            return i() / 1000;
        }
        return (long) e(temporalField);
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof LocalTime)) {
            return false;
        }
        LocalTime localTime = (LocalTime) obj;
        if (this.b == localTime.b && this.c == localTime.c && this.d == localTime.d && this.f3094e == localTime.f3094e) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        long i2 = i();
        return (int) (i2 ^ (i2 >>> 32));
    }

    public long i() {
        return (((long) this.d) * 1000000000) + (((long) this.c) * 60000000000L) + (((long) this.b) * 3600000000000L) + ((long) this.f3094e);
    }

    public int j() {
        return (this.c * 60) + (this.b * 3600) + this.d;
    }

    public String toString() {
        StringBuilder sb = new StringBuilder(18);
        byte b2 = this.b;
        byte b3 = this.c;
        byte b4 = this.d;
        int i2 = this.f3094e;
        sb.append(b2 < 10 ? SessionProtobufHelper.SIGNAL_DEFAULT : "");
        sb.append((int) b2);
        String str = ":0";
        sb.append(b3 < 10 ? str : ":");
        sb.append((int) b3);
        if (b4 > 0 || i2 > 0) {
            if (b4 >= 10) {
                str = ":";
            }
            sb.append(str);
            sb.append((int) b4);
            if (i2 > 0) {
                sb.append('.');
                if (i2 % 1000000 == 0) {
                    sb.append(Integer.toString((i2 / 1000000) + AnswersRetryFilesSender.BACKOFF_MS).substring(1));
                } else if (i2 % AnswersRetryFilesSender.BACKOFF_MS == 0) {
                    sb.append(Integer.toString((i2 / AnswersRetryFilesSender.BACKOFF_MS) + 1000000).substring(1));
                } else {
                    sb.append(Integer.toString(i2 + 1000000000).substring(1));
                }
            }
        }
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalTime b(int i2, int i3, int i4, int i5) {
        ? r0 = ChronoField.HOUR_OF_DAY;
        r0.range.b((long) i2, r0);
        ? r02 = ChronoField.MINUTE_OF_HOUR;
        r02.range.b((long) i3, r02);
        ? r03 = ChronoField.SECOND_OF_MINUTE;
        r03.range.b((long) i4, r03);
        ? r04 = ChronoField.NANO_OF_SECOND;
        r04.range.b((long) i5, r04);
        return a(i2, i3, i4, i5);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.v.TemporalAccessor, R, q.b.a.LocalTime] */
    public <R> R a(TemporalQuery temporalQuery) {
        if (temporalQuery == TemporalQueries.c) {
            return ChronoUnit.NANOS;
        }
        if (temporalQuery == TemporalQueries.g) {
            return this;
        }
        if (temporalQuery == TemporalQueries.b || temporalQuery == TemporalQueries.a || temporalQuery == TemporalQueries.d || temporalQuery == TemporalQueries.f3150e || temporalQuery == TemporalQueries.f3151f) {
            return null;
        }
        return temporalQuery.a(this);
    }

    public LocalTime c(long j2) {
        if (j2 == 0) {
            return this;
        }
        long i2 = i();
        long j3 = (((j2 % 86400000000000L) + i2) + 86400000000000L) % 86400000000000L;
        if (i2 == j3) {
            return this;
        }
        return a((int) (j3 / 3600000000000L), (int) ((j3 / 60000000000L) % 60), (int) ((j3 / 1000000000) % 60), (int) (j3 % 1000000000));
    }

    public LocalTime d(long j2) {
        if (j2 == 0) {
            return this;
        }
        int i2 = (this.c * 60) + (this.b * 3600) + this.d;
        int i3 = ((((int) (j2 % 86400)) + i2) + 86400) % 86400;
        if (i2 == i3) {
            return this;
        }
        return a(i3 / 3600, (i3 / 60) % 60, i3 % 60, this.f3094e);
    }

    public final int e(TemporalField temporalField) {
        switch (((ChronoField) temporalField).ordinal()) {
            case 0:
                return this.f3094e;
            case 1:
                throw new DateTimeException(outline.a("Field too large for an int: ", temporalField));
            case 2:
                return this.f3094e / AnswersRetryFilesSender.BACKOFF_MS;
            case 3:
                throw new DateTimeException(outline.a("Field too large for an int: ", temporalField));
            case 4:
                return this.f3094e / 1000000;
            case 5:
                return (int) (i() / RetryManager.NANOSECONDS_IN_MS);
            case 6:
                return this.d;
            case 7:
                return j();
            case 8:
                return this.c;
            case 9:
                return (this.b * 60) + this.c;
            case 10:
                return this.b % 12;
            case 11:
                int i2 = this.b % 12;
                if (i2 % 12 == 0) {
                    return 12;
                }
                return i2;
            case 12:
                return this.b;
            case 13:
                byte b2 = this.b;
                if (b2 == 0) {
                    return 24;
                }
                return b2;
            case 14:
                return this.b / 12;
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalTime a(int i2, int i3) {
        ? r0 = ChronoField.HOUR_OF_DAY;
        r0.range.b((long) i2, r0);
        if (i3 == 0) {
            return f3093i[i2];
        }
        ? r02 = ChronoField.MINUTE_OF_HOUR;
        r02.range.b((long) i3, r02);
        return new LocalTime(i2, i3, 0, 0);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.LocalTime] */
    public int b(TemporalField temporalField) {
        if (temporalField instanceof ChronoField) {
            return e(temporalField);
        }
        return LocalTime.super.b(temporalField);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public LocalTime b(int i2) {
        if (this.f3094e == i2) {
            return this;
        }
        ? r0 = ChronoField.NANO_OF_SECOND;
        r0.range.b((long) i2, r0);
        return a(this.b, this.c, this.d, i2);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v2, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v3, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalTime a(int i2, int i3, int i4) {
        ? r0 = ChronoField.HOUR_OF_DAY;
        r0.range.b((long) i2, r0);
        if ((i3 | i4) == 0) {
            return f3093i[i2];
        }
        ? r02 = ChronoField.MINUTE_OF_HOUR;
        r02.range.b((long) i3, r02);
        ? r03 = ChronoField.SECOND_OF_MINUTE;
        r03.range.b((long) i4, r03);
        return new LocalTime(i2, i3, i4, 0);
    }

    public LocalTime b(long j2, TemporalUnit temporalUnit) {
        if (!(temporalUnit instanceof ChronoUnit)) {
            return (LocalTime) temporalUnit.a(this, j2);
        }
        switch (((ChronoUnit) temporalUnit).ordinal()) {
            case 0:
                return c(j2);
            case 1:
                return c((j2 % 86400000000L) * 1000);
            case 2:
                return c((j2 % 86400000) * RetryManager.NANOSECONDS_IN_MS);
            case 3:
                return d(j2);
            case 4:
                return b(j2);
            case 5:
                return a(j2);
            case 6:
                return a((j2 % 2) * 12);
            default:
                throw new UnsupportedTemporalTypeException("Unsupported unit: " + temporalUnit);
        }
    }

    public static LocalTime a(TemporalAccessor temporalAccessor) {
        LocalTime localTime = (LocalTime) temporalAccessor.a(TemporalQueries.g);
        if (localTime != null) {
            return localTime;
        }
        throw new DateTimeException(outline.a(temporalAccessor, outline.a("Unable to obtain LocalTime from TemporalAccessor: ", temporalAccessor, ", type ")));
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public static LocalTime a(long j2, int i2) {
        ? r0 = ChronoField.SECOND_OF_DAY;
        r0.range.b(j2, r0);
        ? r02 = ChronoField.NANO_OF_SECOND;
        r02.range.b((long) i2, r02);
        int i3 = (int) (j2 / 3600);
        long j3 = j2 - ((long) (i3 * 3600));
        int i4 = (int) (j3 / 60);
        return a(i3, i4, (int) (j3 - ((long) (i4 * 60))), i2);
    }

    public LocalTime b(long j2) {
        if (j2 == 0) {
            return this;
        }
        int i2 = (this.b * 60) + this.c;
        int i3 = ((((int) (j2 % 1440)) + i2) + 1440) % 1440;
        if (i2 == i3) {
            return this;
        }
        return a(i3 / 60, i3 % 60, this.d, this.f3094e);
    }

    public static LocalTime a(int i2, int i3, int i4, int i5) {
        if ((i3 | i4 | i5) == 0) {
            return f3093i[i2];
        }
        return new LocalTime(i2, i3, i4, i5);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.u.DefaultInterfaceTemporalAccessor, q.b.a.LocalTime] */
    public ValueRange a(TemporalField temporalField) {
        return LocalTime.super.a(temporalField);
    }

    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.Temporal, q.b.a.LocalTime] */
    /* JADX WARN: Type inference failed for: r2v3, types: [q.b.a.v.Temporal, q.b.a.LocalTime] */
    public Temporal a(TemporalAdjuster temporalAdjuster) {
        if (temporalAdjuster instanceof LocalTime) {
            return (LocalTime) temporalAdjuster;
        }
        return (LocalTime) temporalAdjuster.a(this);
    }

    /* JADX WARN: Type inference failed for: r0v2, types: [java.lang.Enum, q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r7v5, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    /* JADX WARN: Type inference failed for: r7v9, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public LocalTime a(TemporalField temporalField, long j2) {
        if (!(temporalField instanceof ChronoField)) {
            return (LocalTime) temporalField.a(this, j2);
        }
        ? r0 = (ChronoField) temporalField;
        r0.range.b(j2, r0);
        switch (r0.ordinal()) {
            case 0:
                return b((int) j2);
            case 1:
                return e(j2);
            case 2:
                return b(((int) j2) * AnswersRetryFilesSender.BACKOFF_MS);
            case 3:
                return e(j2 * 1000);
            case 4:
                return b(((int) j2) * 1000000);
            case 5:
                return e(j2 * RetryManager.NANOSECONDS_IN_MS);
            case 6:
                int i2 = (int) j2;
                if (this.d == i2) {
                    return this;
                }
                ? r7 = ChronoField.SECOND_OF_MINUTE;
                r7.range.b((long) i2, r7);
                return a(this.b, this.c, i2, this.f3094e);
            case 7:
                return d(j2 - ((long) j()));
            case 8:
                int i3 = (int) j2;
                if (this.c == i3) {
                    return this;
                }
                ? r72 = ChronoField.MINUTE_OF_HOUR;
                r72.range.b((long) i3, r72);
                return a(this.b, i3, this.d, this.f3094e);
            case 9:
                return b(j2 - ((long) ((this.b * 60) + this.c)));
            case 10:
                return a(j2 - ((long) (this.b % 12)));
            case 11:
                if (j2 == 12) {
                    j2 = 0;
                }
                return a(j2 - ((long) (this.b % 12)));
            case 12:
                return a((int) j2);
            case 13:
                if (j2 == 24) {
                    j2 = 0;
                }
                return a((int) j2);
            case 14:
                return a((j2 - ((long) (this.b / 12))) * 12);
            default:
                throw new UnsupportedTemporalTypeException(outline.a("Unsupported field: ", temporalField));
        }
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public LocalTime a(int i2) {
        if (this.b == i2) {
            return this;
        }
        ? r0 = ChronoField.HOUR_OF_DAY;
        r0.range.b((long) i2, r0);
        return a(i2, this.c, this.d, this.f3094e);
    }

    public LocalTime a(long j2) {
        if (j2 == 0) {
            return this;
        }
        return a(((((int) (j2 % 24)) + this.b) + 24) % 24, this.c, this.d, this.f3094e);
    }

    /* JADX WARN: Type inference failed for: r4v2, types: [q.b.a.v.Temporal, q.b.a.LocalTime] */
    /* JADX WARN: Type inference failed for: r4v5, types: [q.b.a.v.Temporal, q.b.a.LocalTime] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime
     arg types: [int, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime
     arg types: [?, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime
     arg types: [long, q.b.a.v.TemporalUnit]
     candidates:
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.v.Temporal
      q.b.a.LocalTime.b(long, q.b.a.v.TemporalUnit):q.b.a.LocalTime */
    public Temporal a(long j2, TemporalUnit temporalUnit) {
        return j2 == Long.MIN_VALUE ? b((long) RecyclerView.FOREVER_NS, temporalUnit).b(1L, temporalUnit) : b(-j2, temporalUnit);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [q.b.a.v.TemporalField, q.b.a.v.ChronoField] */
    public Temporal a(Temporal temporal) {
        return temporal.a((TemporalField) ChronoField.NANO_OF_DAY, i());
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(int, int):int
     arg types: [byte, byte]
     candidates:
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(int, int):int */
    /* renamed from: a */
    public int compareTo(LocalTime localTime) {
        int a = Collections.a((int) this.b, (int) localTime.b);
        if (a != 0) {
            return a;
        }
        int a2 = Collections.a((int) this.c, (int) localTime.c);
        if (a2 != 0) {
            return a2;
        }
        int a3 = Collections.a((int) this.d, (int) localTime.d);
        return a3 == 0 ? Collections.a(this.f3094e, localTime.f3094e) : a3;
    }

    public void a(DataOutput dataOutput) {
        if (this.f3094e != 0) {
            dataOutput.writeByte(this.b);
            dataOutput.writeByte(this.c);
            dataOutput.writeByte(this.d);
            dataOutput.writeInt(this.f3094e);
        } else if (this.d != 0) {
            dataOutput.writeByte(this.b);
            dataOutput.writeByte(this.c);
            dataOutput.writeByte(~this.d);
        } else if (this.c == 0) {
            dataOutput.writeByte(~this.b);
        } else {
            dataOutput.writeByte(this.b);
            dataOutput.writeByte(~this.c);
        }
    }

    public static LocalTime a(DataInput dataInput) {
        byte b2;
        int i2;
        int readByte = dataInput.readByte();
        byte b3 = 0;
        if (readByte < 0) {
            readByte = ~readByte;
            b2 = 0;
        } else {
            byte readByte2 = dataInput.readByte();
            if (readByte2 < 0) {
                int i3 = ~readByte2;
                i2 = 0;
                b3 = i3;
                b2 = 0;
            } else {
                byte readByte3 = dataInput.readByte();
                if (readByte3 < 0) {
                    b2 = ~readByte3;
                    b3 = readByte2;
                } else {
                    int readInt = dataInput.readInt();
                    b2 = readByte3;
                    byte b4 = readByte2;
                    i2 = readInt;
                    b3 = b4;
                }
            }
            return b(readByte, b3, b2, i2);
        }
        i2 = 0;
        return b(readByte, b3, b2, i2);
    }
}
