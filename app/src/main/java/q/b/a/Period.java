package q.b.a;

import java.io.Serializable;
import java.util.regex.Pattern;
import n.i.Collections;
import q.b.a.s.ChronoPeriod;
import q.b.a.v.ChronoUnit;
import q.b.a.v.Temporal;

public final class Period extends ChronoPeriod implements Serializable {

    /* renamed from: e  reason: collision with root package name */
    public static final Period f3095e = new Period(0, 0, 0);
    public final int b;
    public final int c;
    public final int d;

    static {
        Pattern.compile("([-+]?)P(?:([-+]?[0-9]+)Y)?(?:([-+]?[0-9]+)M)?(?:([-+]?[0-9]+)W)?(?:([-+]?[0-9]+)D)?", 2);
    }

    public Period(int i2, int i3, int i4) {
        this.b = i2;
        this.c = i3;
        this.d = i4;
    }

    public static Period a(int i2) {
        if ((0 | i2) == 0) {
            return f3095e;
        }
        return new Period(0, 0, i2);
    }

    private Object readResolve() {
        return ((this.b | this.c) | this.d) == 0 ? f3095e : this;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Period)) {
            return false;
        }
        Period period = (Period) obj;
        if (this.b == period.b && this.c == period.c && this.d == period.d) {
            return true;
        }
        return false;
    }

    public int hashCode() {
        return Integer.rotateLeft(this.d, 16) + Integer.rotateLeft(this.c, 8) + this.b;
    }

    public String toString() {
        if (this == f3095e) {
            return "P0D";
        }
        StringBuilder sb = new StringBuilder();
        sb.append('P');
        int i2 = this.b;
        if (i2 != 0) {
            sb.append(i2);
            sb.append('Y');
        }
        int i3 = this.c;
        if (i3 != 0) {
            sb.append(i3);
            sb.append('M');
        }
        int i4 = this.d;
        if (i4 != 0) {
            sb.append(i4);
            sb.append('D');
        }
        return sb.toString();
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r2v1, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r2v2, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX WARN: Type inference failed for: r0v8, types: [q.b.a.v.ChronoUnit, q.b.a.v.TemporalUnit] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.lang.Object, java.lang.String):T
     arg types: [q.b.a.v.Temporal, java.lang.String]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.lang.Object, java.lang.String):T */
    public Temporal a(Temporal temporal) {
        Collections.a((Object) temporal, "temporal");
        int i2 = this.b;
        if (i2 != 0) {
            int i3 = this.c;
            if (i3 != 0) {
                temporal = temporal.b((((long) i2) * 12) + ((long) i3), ChronoUnit.MONTHS);
            } else {
                temporal = temporal.b((long) i2, ChronoUnit.YEARS);
            }
        } else {
            int i4 = this.c;
            if (i4 != 0) {
                temporal = temporal.b((long) i4, ChronoUnit.MONTHS);
            }
        }
        int i5 = this.d;
        return i5 != 0 ? temporal.b((long) i5, ChronoUnit.DAYS) : temporal;
    }
}
