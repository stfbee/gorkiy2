package q.a.a.e;

import q.a.a.d;

public abstract class a implements d, q.a.b.a {
    public final byte[] a;
    public int b;
    public long c;

    public a() {
        this.a = new byte[4];
        this.b = 0;
    }

    public a(a aVar) {
        byte[] bArr = new byte[4];
        this.a = bArr;
        byte[] bArr2 = aVar.a;
        System.arraycopy(bArr2, 0, bArr, 0, bArr2.length);
        this.b = aVar.b;
        this.c = aVar.c;
    }

    public void a() {
        this.c = 0;
        this.b = 0;
        int i2 = 0;
        while (true) {
            byte[] bArr = this.a;
            if (i2 < bArr.length) {
                bArr[i2] = 0;
                i2++;
            } else {
                return;
            }
        }
    }

    public void a(byte b2) {
        byte[] bArr = this.a;
        int i2 = this.b;
        int i3 = i2 + 1;
        this.b = i3;
        bArr[i2] = b2;
        if (i3 == bArr.length) {
            b(bArr, 0);
            this.b = 0;
        }
        this.c++;
    }

    public void a(byte[] bArr, int i2, int i3) {
        int i4 = 0;
        int max = Math.max(0, i3);
        if (this.b != 0) {
            int i5 = 0;
            while (true) {
                if (i5 >= max) {
                    i4 = i5;
                    break;
                }
                byte[] bArr2 = this.a;
                int i6 = this.b;
                int i7 = i6 + 1;
                this.b = i7;
                int i8 = i5 + 1;
                bArr2[i6] = bArr[i5 + i2];
                if (i7 == 4) {
                    b(bArr2, 0);
                    this.b = 0;
                    i4 = i8;
                    break;
                }
                i5 = i8;
            }
        }
        int i9 = ((max - i4) & -4) + i4;
        while (i4 < i9) {
            b(bArr, i2 + i4);
            i4 += 4;
        }
        while (i4 < max) {
            byte[] bArr3 = this.a;
            int i10 = this.b;
            this.b = i10 + 1;
            bArr3[i10] = bArr[i4 + i2];
            i4++;
        }
        this.c += (long) max;
    }

    public abstract void b(byte[] bArr, int i2);

    public int e() {
        return 64;
    }

    public abstract void f();
}
