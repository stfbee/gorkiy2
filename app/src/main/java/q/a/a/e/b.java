package q.a.a.e;

import j.a.a.a.outline;
import n.i.Collections;
import q.a.b.a;

public class b extends a {
    public int d;

    /* renamed from: e  reason: collision with root package name */
    public int f3072e;

    /* renamed from: f  reason: collision with root package name */
    public int f3073f;
    public int g;
    public int h;

    /* renamed from: i  reason: collision with root package name */
    public int[] f3074i = new int[80];

    /* renamed from: j  reason: collision with root package name */
    public int f3075j;

    public b() {
        a();
    }

    public b(b bVar) {
        super(super);
        a(bVar);
    }

    public final int a(int i2, int i3, int i4) {
        return ((~i2) & i4) | (i3 & i2);
    }

    public int a(byte[] bArr, int i2) {
        long j2 = super.c << 3;
        byte b = Byte.MIN_VALUE;
        while (true) {
            a(b);
            if (super.b == 0) {
                break;
            }
            b = 0;
        }
        if (this.f3075j > 14) {
            f();
        }
        int[] iArr = this.f3074i;
        iArr[14] = (int) (j2 >>> 32);
        iArr[15] = (int) j2;
        f();
        Collections.a(this.d, bArr, i2);
        Collections.a(this.f3072e, bArr, i2 + 4);
        Collections.a(this.f3073f, bArr, i2 + 8);
        Collections.a(this.g, bArr, i2 + 12);
        Collections.a(this.h, bArr, i2 + 16);
        a();
        return 20;
    }

    public void a() {
        super.a();
        this.d = 1732584193;
        this.f3072e = -271733879;
        this.f3073f = -1732584194;
        this.g = 271733878;
        this.h = -1009589776;
        this.f3075j = 0;
        int i2 = 0;
        while (true) {
            int[] iArr = this.f3074i;
            if (i2 != iArr.length) {
                iArr[i2] = 0;
                i2++;
            } else {
                return;
            }
        }
    }

    public final void a(b bVar) {
        this.d = bVar.d;
        this.f3072e = bVar.f3072e;
        this.f3073f = bVar.f3073f;
        this.g = bVar.g;
        this.h = bVar.h;
        int[] iArr = bVar.f3074i;
        System.arraycopy(iArr, 0, this.f3074i, 0, iArr.length);
        this.f3075j = bVar.f3075j;
    }

    public final int b(int i2, int i3, int i4) {
        return (i2 & i4) | (i2 & i3) | (i3 & i4);
    }

    public a b() {
        return new b(this);
    }

    public void b(byte[] bArr, int i2) {
        int i3 = i2 + 1;
        int i4 = i3 + 1;
        int i5 = (bArr[i4 + 1] & 255) | (bArr[i2] << 24) | ((bArr[i3] & 255) << 16) | ((bArr[i4] & 255) << 8);
        int[] iArr = this.f3074i;
        int i6 = this.f3075j;
        iArr[i6] = i5;
        int i7 = i6 + 1;
        this.f3075j = i7;
        if (i7 == 16) {
            f();
        }
    }

    public String c() {
        return "SHA-1";
    }

    public int d() {
        return 20;
    }

    public void f() {
        for (int i2 = 16; i2 < 80; i2++) {
            int[] iArr = this.f3074i;
            int i3 = ((iArr[i2 - 3] ^ iArr[i2 - 8]) ^ iArr[i2 - 14]) ^ iArr[i2 - 16];
            iArr[i2] = (i3 >>> 31) | (i3 << 1);
        }
        int i4 = this.d;
        int i5 = this.f3072e;
        int i6 = this.f3073f;
        int i7 = this.g;
        int i8 = this.h;
        int i9 = 0;
        int i10 = 0;
        while (i9 < 4) {
            int i11 = i10 + 1;
            int a = outline.a(a(i5, i6, i7) + ((i4 << 5) | (i4 >>> 27)), this.f3074i[i10], 1518500249, i8);
            int i12 = (i5 >>> 2) | (i5 << 30);
            int i13 = i11 + 1;
            int a2 = outline.a(a(i4, i12, i6) + ((a << 5) | (a >>> 27)), this.f3074i[i11], 1518500249, i7);
            int i14 = (i4 >>> 2) | (i4 << 30);
            int i15 = i13 + 1;
            int a3 = outline.a(a(a, i14, i12) + ((a2 << 5) | (a2 >>> 27)), this.f3074i[i13], 1518500249, i6);
            i8 = (a >>> 2) | (a << 30);
            int i16 = i15 + 1;
            i5 = outline.a(a(a2, i8, i14) + ((a3 << 5) | (a3 >>> 27)), this.f3074i[i15], 1518500249, i12);
            i7 = (a2 >>> 2) | (a2 << 30);
            i4 = outline.a(a(a3, i7, i8) + ((i5 << 5) | (i5 >>> 27)), this.f3074i[i16], 1518500249, i14);
            i6 = (a3 >>> 2) | (a3 << 30);
            i9++;
            i10 = i16 + 1;
        }
        int i17 = 0;
        while (i17 < 4) {
            int[] iArr2 = this.f3074i;
            int i18 = i10 + 1;
            int a4 = outline.a(((i4 << 5) | (i4 >>> 27)) + ((i5 ^ i6) ^ i7), iArr2[i10], 1859775393, i8);
            int i19 = (i5 >>> 2) | (i5 << 30);
            int i20 = i18 + 1;
            int a5 = outline.a(((a4 << 5) | (a4 >>> 27)) + ((i4 ^ i19) ^ i6), iArr2[i18], 1859775393, i7);
            int i21 = (i4 >>> 2) | (i4 << 30);
            int i22 = i20 + 1;
            int a6 = outline.a(((a5 << 5) | (a5 >>> 27)) + ((a4 ^ i21) ^ i19), iArr2[i20], 1859775393, i6);
            i8 = (a4 >>> 2) | (a4 << 30);
            int i23 = i22 + 1;
            i5 = outline.a(((a6 << 5) | (a6 >>> 27)) + ((a5 ^ i8) ^ i21), iArr2[i22], 1859775393, i19);
            i7 = (a5 >>> 2) | (a5 << 30);
            i4 = outline.a(((i5 << 5) | (i5 >>> 27)) + ((a6 ^ i7) ^ i8), iArr2[i23], 1859775393, i21);
            i6 = (a6 >>> 2) | (a6 << 30);
            i17++;
            i10 = i23 + 1;
        }
        int i24 = 0;
        while (i24 < 4) {
            int i25 = i10 + 1;
            int a7 = outline.a(b(i5, i6, i7) + ((i4 << 5) | (i4 >>> 27)), this.f3074i[i10], -1894007588, i8);
            int i26 = (i5 >>> 2) | (i5 << 30);
            int i27 = i25 + 1;
            int a8 = outline.a(b(i4, i26, i6) + ((a7 << 5) | (a7 >>> 27)), this.f3074i[i25], -1894007588, i7);
            int i28 = (i4 >>> 2) | (i4 << 30);
            int i29 = i27 + 1;
            int a9 = outline.a(b(a7, i28, i26) + ((a8 << 5) | (a8 >>> 27)), this.f3074i[i27], -1894007588, i6);
            i8 = (a7 >>> 2) | (a7 << 30);
            int i30 = i29 + 1;
            i5 = outline.a(b(a8, i8, i28) + ((a9 << 5) | (a9 >>> 27)), this.f3074i[i29], -1894007588, i26);
            i7 = (a8 >>> 2) | (a8 << 30);
            i4 = outline.a(b(a9, i7, i8) + ((i5 << 5) | (i5 >>> 27)), this.f3074i[i30], -1894007588, i28);
            i6 = (a9 >>> 2) | (a9 << 30);
            i24++;
            i10 = i30 + 1;
        }
        int i31 = 0;
        while (i31 <= 3) {
            int[] iArr3 = this.f3074i;
            int i32 = i10 + 1;
            int a10 = outline.a(((i4 << 5) | (i4 >>> 27)) + ((i5 ^ i6) ^ i7), iArr3[i10], -899497514, i8);
            int i33 = (i5 >>> 2) | (i5 << 30);
            int i34 = i32 + 1;
            int a11 = outline.a(((a10 << 5) | (a10 >>> 27)) + ((i4 ^ i33) ^ i6), iArr3[i32], -899497514, i7);
            int i35 = (i4 >>> 2) | (i4 << 30);
            int i36 = i34 + 1;
            int a12 = outline.a(((a11 << 5) | (a11 >>> 27)) + ((a10 ^ i35) ^ i33), iArr3[i34], -899497514, i6);
            i8 = (a10 >>> 2) | (a10 << 30);
            int i37 = i36 + 1;
            i5 = outline.a(((a12 << 5) | (a12 >>> 27)) + ((a11 ^ i8) ^ i35), iArr3[i36], -899497514, i33);
            i7 = (a11 >>> 2) | (a11 << 30);
            i4 = outline.a(((i5 << 5) | (i5 >>> 27)) + ((a12 ^ i7) ^ i8), iArr3[i37], -899497514, i35);
            i6 = (a12 >>> 2) | (a12 << 30);
            i31++;
            i10 = i37 + 1;
        }
        this.d += i4;
        this.f3072e += i5;
        this.f3073f += i6;
        this.g += i7;
        this.h += i8;
        this.f3075j = 0;
        for (int i38 = 0; i38 < 16; i38++) {
            this.f3074i[i38] = 0;
        }
    }

    public void a(a aVar) {
        b bVar = (b) aVar;
        byte[] bArr = super.a;
        System.arraycopy(bArr, 0, super.a, 0, bArr.length);
        super.b = super.b;
        super.c = super.c;
        a(bVar);
    }
}
