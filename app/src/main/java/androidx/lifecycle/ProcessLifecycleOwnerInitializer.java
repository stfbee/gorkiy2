package androidx.lifecycle;

import android.app.Application;
import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Handler;
import i.o.Lifecycle;
import i.o.LifecycleDispatcher;
import i.o.ProcessLifecycleOwner;
import i.o.ProcessLifecycleOwner0;

public class ProcessLifecycleOwnerInitializer extends ContentProvider {
    public int delete(Uri uri, String str, String[] strArr) {
        return 0;
    }

    public String getType(Uri uri) {
        return null;
    }

    public Uri insert(Uri uri, ContentValues contentValues) {
        return null;
    }

    public boolean onCreate() {
        Context context = getContext();
        if (!LifecycleDispatcher.a.getAndSet(true)) {
            ((Application) context.getApplicationContext()).registerActivityLifecycleCallbacks(new LifecycleDispatcher.a());
        }
        Context context2 = getContext();
        ProcessLifecycleOwner processLifecycleOwner = ProcessLifecycleOwner.f1358j;
        if (processLifecycleOwner != null) {
            processLifecycleOwner.f1360f = new Handler();
            processLifecycleOwner.g.a(Lifecycle.a.ON_CREATE);
            ((Application) context2.getApplicationContext()).registerActivityLifecycleCallbacks(new ProcessLifecycleOwner0(processLifecycleOwner));
            return true;
        }
        throw null;
    }

    public Cursor query(Uri uri, String[] strArr, String str, String[] strArr2, String str2) {
        return null;
    }

    public int update(Uri uri, ContentValues contentValues, String str, String[] strArr) {
        return 0;
    }
}
