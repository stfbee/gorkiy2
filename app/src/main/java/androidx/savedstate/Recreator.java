package androidx.savedstate;

import android.annotation.SuppressLint;
import android.os.Bundle;
import i.o.GenericLifecycleObserver;
import i.o.Lifecycle;
import i.o.LifecycleOwner;
import i.o.LifecycleRegistry;
import i.t.SavedStateRegistry;
import i.t.SavedStateRegistryOwner;
import j.a.a.a.outline;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Iterator;

@SuppressLint({"RestrictedApi"})
public final class Recreator implements GenericLifecycleObserver {
    public final SavedStateRegistryOwner a;

    public Recreator(SavedStateRegistryOwner savedStateRegistryOwner) {
        this.a = savedStateRegistryOwner;
    }

    public void a(LifecycleOwner lifecycleOwner, Lifecycle.a aVar) {
        if (aVar == Lifecycle.a.ON_CREATE) {
            ((LifecycleRegistry) lifecycleOwner.a()).a.remove(this);
            SavedStateRegistry d = this.a.d();
            if (d.c) {
                Bundle bundle = d.b;
                Bundle bundle2 = null;
                if (bundle != null) {
                    Bundle bundle3 = bundle.getBundle("androidx.savedstate.Restarter");
                    d.b.remove("androidx.savedstate.Restarter");
                    if (d.b.isEmpty()) {
                        d.b = null;
                    }
                    bundle2 = bundle3;
                }
                if (bundle2 != null) {
                    ArrayList<String> stringArrayList = bundle2.getStringArrayList("classes_to_restore");
                    if (stringArrayList != null) {
                        Iterator<String> it = stringArrayList.iterator();
                        while (it.hasNext()) {
                            String next = it.next();
                            try {
                                Class<? extends U> asSubclass = Class.forName(next, false, Recreator.class.getClassLoader()).asSubclass(SavedStateRegistry.a.class);
                                try {
                                    Constructor<? extends U> declaredConstructor = asSubclass.getDeclaredConstructor(new Class[0]);
                                    declaredConstructor.setAccessible(true);
                                    try {
                                        ((SavedStateRegistry.a) declaredConstructor.newInstance(new Object[0])).a(this.a);
                                    } catch (Exception e2) {
                                        throw new RuntimeException(outline.a("Failed to instantiate ", next), e2);
                                    }
                                } catch (NoSuchMethodException e3) {
                                    StringBuilder a2 = outline.a("Class");
                                    a2.append(asSubclass.getSimpleName());
                                    a2.append(" must have default constructor in order to be automatically recreated");
                                    throw new IllegalStateException(a2.toString(), e3);
                                }
                            } catch (ClassNotFoundException e4) {
                                throw new RuntimeException(outline.a("Class ", next, " wasn't found"), e4);
                            }
                        }
                        return;
                    }
                    throw new IllegalStateException("Bundle with restored state for the component \"androidx.savedstate.Restarter\" must contain list of strings by the key \"classes_to_restore\"");
                }
                return;
            }
            throw new IllegalStateException("You can consumeRestoredStateForKey only after super.onCreate of corresponding component");
        }
        throw new AssertionError("Next event must be ON_CREATE");
    }
}
