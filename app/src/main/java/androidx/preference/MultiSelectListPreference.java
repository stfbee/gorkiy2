package androidx.preference;

import android.content.res.TypedArray;
import java.util.HashSet;

public class MultiSelectListPreference extends DialogPreference {
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MultiSelectListPreference(android.content.Context r4, android.util.AttributeSet r5) {
        /*
            r3 = this;
            int r0 = i.q.a.dialogPreferenceStyle
            r1 = 16842897(0x1010091, float:2.3693964E-38)
            int r0 = i.b.k.ResourcesFlusher.a(r4, r0, r1)
            r1 = 0
            r3.<init>(r4, r5, r0, r1)
            java.util.HashSet r2 = new java.util.HashSet
            r2.<init>()
            int[] r2 = i.q.d.MultiSelectListPreference
            android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r2, r0, r1)
            int r5 = i.q.d.MultiSelectListPreference_entries
            int r0 = i.q.d.MultiSelectListPreference_android_entries
            i.b.k.ResourcesFlusher.b(r4, r5, r0)
            int r5 = i.q.d.MultiSelectListPreference_entryValues
            int r0 = i.q.d.MultiSelectListPreference_android_entryValues
            java.lang.CharSequence[] r5 = r4.getTextArray(r5)
            if (r5 != 0) goto L_0x002c
            r4.getTextArray(r0)
        L_0x002c:
            r4.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.MultiSelectListPreference.<init>(android.content.Context, android.util.AttributeSet):void");
    }

    public Object a(TypedArray typedArray, int i2) {
        CharSequence[] textArray = typedArray.getTextArray(i2);
        HashSet hashSet = new HashSet();
        for (CharSequence charSequence : textArray) {
            hashSet.add(charSequence.toString());
        }
        return hashSet;
    }
}
