package androidx.preference;

import android.widget.CompoundButton;

public class CheckBoxPreference extends TwoStatePreference {
    public final a u = new a();

    public class a implements CompoundButton.OnCheckedChangeListener {
        public a() {
        }

        public void onCheckedChanged(CompoundButton compoundButton, boolean z) {
            if (CheckBoxPreference.this != null) {
                CheckBoxPreference.this.a(z);
                return;
            }
            throw null;
        }
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public CheckBoxPreference(android.content.Context r4, android.util.AttributeSet r5) {
        /*
            r3 = this;
            int r0 = i.q.a.checkBoxPreferenceStyle
            r1 = 16842895(0x101008f, float:2.369396E-38)
            int r0 = i.b.k.ResourcesFlusher.a(r4, r0, r1)
            r1 = 0
            r3.<init>(r4, r5, r0, r1)
            androidx.preference.CheckBoxPreference$a r2 = new androidx.preference.CheckBoxPreference$a
            r2.<init>()
            r3.u = r2
            int[] r2 = i.q.d.CheckBoxPreference
            android.content.res.TypedArray r4 = r4.obtainStyledAttributes(r5, r2, r0, r1)
            int r5 = i.q.d.CheckBoxPreference_summaryOn
            int r0 = i.q.d.CheckBoxPreference_android_summaryOn
            java.lang.String r5 = i.b.k.ResourcesFlusher.a(r4, r5, r0)
            r3.f274q = r5
            int r5 = i.q.d.CheckBoxPreference_summaryOff
            int r0 = i.q.d.CheckBoxPreference_android_summaryOff
            java.lang.String r5 = r4.getString(r5)
            if (r5 != 0) goto L_0x0032
            java.lang.String r5 = r4.getString(r0)
        L_0x0032:
            r3.f275r = r5
            int r5 = i.q.d.CheckBoxPreference_disableDependentsState
            int r0 = i.q.d.CheckBoxPreference_android_disableDependentsState
            boolean r0 = r4.getBoolean(r0, r1)
            boolean r5 = r4.getBoolean(r5, r0)
            r3.f277t = r5
            r4.recycle()
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.preference.CheckBoxPreference.<init>(android.content.Context, android.util.AttributeSet):void");
    }
}
