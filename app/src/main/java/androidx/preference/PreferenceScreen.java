package androidx.preference;

import android.content.Context;
import android.util.AttributeSet;
import i.b.k.ResourcesFlusher;
import i.q.a;

public final class PreferenceScreen extends PreferenceGroup {
    public PreferenceScreen(Context context, AttributeSet attributeSet) {
        super(context, attributeSet, ResourcesFlusher.a(context, a.preferenceScreenStyle, 16842891), 0);
    }
}
