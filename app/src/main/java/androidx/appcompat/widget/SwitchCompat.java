package androidx.appcompat.widget;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.Region;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.text.Layout;
import android.text.StaticLayout;
import android.text.TextPaint;
import android.text.TextUtils;
import android.text.method.TransformationMethod;
import android.util.AttributeSet;
import android.util.Property;
import android.view.ActionMode;
import android.view.VelocityTracker;
import android.view.ViewConfiguration;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CompoundButton;
import android.widget.TextView;
import i.b.j;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.b.o.AllCapsTransformationMethod;
import i.b.q.AppCompatTextHelper;
import i.b.q.DrawableUtils;
import i.b.q.TintTypedArray;
import i.b.q.ViewUtils;
import i.h.l.ViewCompat;

public class SwitchCompat extends CompoundButton {
    public static final Property<SwitchCompat, Float> O = new a(Float.class, "thumbPos");
    public static final int[] P = {16842912};
    public int A;
    public int B;
    public int C;
    public int D;
    public int E;
    public int F;
    public final TextPaint G;
    public ColorStateList H;
    public Layout I;
    public Layout J;
    public TransformationMethod K;
    public ObjectAnimator L;
    public final AppCompatTextHelper M;
    public final Rect N;
    public Drawable b;
    public ColorStateList c;
    public PorterDuff.Mode d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f113e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f114f;
    public Drawable g;
    public ColorStateList h;

    /* renamed from: i  reason: collision with root package name */
    public PorterDuff.Mode f115i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f116j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f117k;

    /* renamed from: l  reason: collision with root package name */
    public int f118l;

    /* renamed from: m  reason: collision with root package name */
    public int f119m;

    /* renamed from: n  reason: collision with root package name */
    public int f120n;

    /* renamed from: o  reason: collision with root package name */
    public boolean f121o;

    /* renamed from: p  reason: collision with root package name */
    public CharSequence f122p;

    /* renamed from: q  reason: collision with root package name */
    public CharSequence f123q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f124r;

    /* renamed from: s  reason: collision with root package name */
    public int f125s;

    /* renamed from: t  reason: collision with root package name */
    public int f126t;
    public float u;
    public float v;
    public VelocityTracker w;
    public int x;
    public float y;
    public int z;

    public static class a extends Property<SwitchCompat, Float> {
        public a(Class cls, String str) {
            super(cls, str);
        }

        public Object get(Object obj) {
            return Float.valueOf(((SwitchCompat) obj).y);
        }

        public void set(Object obj, Object obj2) {
            ((SwitchCompat) obj).setThumbPosition(((Float) obj2).floatValue());
        }
    }

    public SwitchCompat(Context context) {
        this(context, null);
    }

    private boolean getTargetCheckedState() {
        return this.y > 0.5f;
    }

    private int getThumbOffset() {
        float f2;
        if (ViewUtils.a(this)) {
            f2 = 1.0f - this.y;
        } else {
            f2 = this.y;
        }
        return (int) ((f2 * ((float) getThumbScrollRange())) + 0.5f);
    }

    private int getThumbScrollRange() {
        Rect rect;
        Drawable drawable = this.g;
        if (drawable == null) {
            return 0;
        }
        Rect rect2 = this.N;
        drawable.getPadding(rect2);
        Drawable drawable2 = this.b;
        if (drawable2 != null) {
            rect = DrawableUtils.c(drawable2);
        } else {
            rect = DrawableUtils.c;
        }
        return ((((this.z - this.B) - rect2.left) - rect2.right) - rect.left) - rect.right;
    }

    public final void a() {
        if (this.b == null) {
            return;
        }
        if (this.f113e || this.f114f) {
            Drawable mutate = ResourcesFlusher.d(this.b).mutate();
            this.b = mutate;
            if (this.f113e) {
                mutate.setTintList(this.c);
            }
            if (this.f114f) {
                this.b.setTintMode(this.d);
            }
            if (this.b.isStateful()) {
                this.b.setState(getDrawableState());
            }
        }
    }

    public final void b() {
        if (this.g == null) {
            return;
        }
        if (this.f116j || this.f117k) {
            Drawable mutate = ResourcesFlusher.d(this.g).mutate();
            this.g = mutate;
            if (this.f116j) {
                mutate.setTintList(this.h);
            }
            if (this.f117k) {
                this.g.setTintMode(this.f115i);
            }
            if (this.g.isStateful()) {
                this.g.setState(getDrawableState());
            }
        }
    }

    public void draw(Canvas canvas) {
        Rect rect;
        int i2;
        int i3;
        Rect rect2 = this.N;
        int i4 = this.C;
        int i5 = this.D;
        int i6 = this.E;
        int i7 = this.F;
        int thumbOffset = getThumbOffset() + i4;
        Drawable drawable = this.b;
        if (drawable != null) {
            rect = DrawableUtils.c(drawable);
        } else {
            rect = DrawableUtils.c;
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.getPadding(rect2);
            int i8 = rect2.left;
            thumbOffset += i8;
            if (rect != null) {
                int i9 = rect.left;
                if (i9 > i8) {
                    i4 += i9 - i8;
                }
                int i10 = rect.top;
                int i11 = rect2.top;
                i2 = i10 > i11 ? (i10 - i11) + i5 : i5;
                int i12 = rect.right;
                int i13 = rect2.right;
                if (i12 > i13) {
                    i6 -= i12 - i13;
                }
                int i14 = rect.bottom;
                int i15 = rect2.bottom;
                if (i14 > i15) {
                    i3 = i7 - (i14 - i15);
                    this.g.setBounds(i4, i2, i6, i3);
                }
            } else {
                i2 = i5;
            }
            i3 = i7;
            this.g.setBounds(i4, i2, i6, i3);
        }
        Drawable drawable3 = this.b;
        if (drawable3 != null) {
            drawable3.getPadding(rect2);
            int i16 = thumbOffset - rect2.left;
            int i17 = thumbOffset + this.B + rect2.right;
            this.b.setBounds(i16, i5, i17, i7);
            Drawable background = getBackground();
            if (background != null) {
                background.setHotspotBounds(i16, i5, i17, i7);
            }
        }
        super.draw(canvas);
    }

    public void drawableHotspotChanged(float f2, float f3) {
        super.drawableHotspotChanged(f2, f3);
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.setHotspot(f2, f3);
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.setHotspot(f2, f3);
        }
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        int[] drawableState = getDrawableState();
        Drawable drawable = this.b;
        boolean z2 = false;
        if (drawable != null && drawable.isStateful()) {
            z2 = false | drawable.setState(drawableState);
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null && drawable2.isStateful()) {
            z2 |= drawable2.setState(drawableState);
        }
        if (z2) {
            invalidate();
        }
    }

    public int getCompoundPaddingLeft() {
        if (!ViewUtils.a(this)) {
            return super.getCompoundPaddingLeft();
        }
        int compoundPaddingLeft = super.getCompoundPaddingLeft() + this.z;
        return !TextUtils.isEmpty(getText()) ? compoundPaddingLeft + this.f120n : compoundPaddingLeft;
    }

    public int getCompoundPaddingRight() {
        if (ViewUtils.a(this)) {
            return super.getCompoundPaddingRight();
        }
        int compoundPaddingRight = super.getCompoundPaddingRight() + this.z;
        return !TextUtils.isEmpty(getText()) ? compoundPaddingRight + this.f120n : compoundPaddingRight;
    }

    public boolean getShowText() {
        return this.f124r;
    }

    public boolean getSplitTrack() {
        return this.f121o;
    }

    public int getSwitchMinWidth() {
        return this.f119m;
    }

    public int getSwitchPadding() {
        return this.f120n;
    }

    public CharSequence getTextOff() {
        return this.f123q;
    }

    public CharSequence getTextOn() {
        return this.f122p;
    }

    public Drawable getThumbDrawable() {
        return this.b;
    }

    public int getThumbTextPadding() {
        return this.f118l;
    }

    public ColorStateList getThumbTintList() {
        return this.c;
    }

    public PorterDuff.Mode getThumbTintMode() {
        return this.d;
    }

    public Drawable getTrackDrawable() {
        return this.g;
    }

    public ColorStateList getTrackTintList() {
        return this.h;
    }

    public PorterDuff.Mode getTrackTintMode() {
        return this.f115i;
    }

    public void jumpDrawablesToCurrentState() {
        super.jumpDrawablesToCurrentState();
        Drawable drawable = this.b;
        if (drawable != null) {
            drawable.jumpToCurrentState();
        }
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.jumpToCurrentState();
        }
        ObjectAnimator objectAnimator = this.L;
        if (objectAnimator != null && objectAnimator.isStarted()) {
            this.L.end();
            this.L = null;
        }
    }

    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 1);
        if (isChecked()) {
            CompoundButton.mergeDrawableStates(onCreateDrawableState, P);
        }
        return onCreateDrawableState;
    }

    public void onDraw(Canvas canvas) {
        int i2;
        super.onDraw(canvas);
        Rect rect = this.N;
        Drawable drawable = this.g;
        if (drawable != null) {
            drawable.getPadding(rect);
        } else {
            rect.setEmpty();
        }
        int i3 = this.D;
        int i4 = this.F;
        int i5 = i3 + rect.top;
        int i6 = i4 - rect.bottom;
        Drawable drawable2 = this.b;
        if (drawable != null) {
            if (!this.f121o || drawable2 == null) {
                drawable.draw(canvas);
            } else {
                Rect c2 = DrawableUtils.c(drawable2);
                drawable2.copyBounds(rect);
                rect.left += c2.left;
                rect.right -= c2.right;
                int save = canvas.save();
                canvas.clipRect(rect, Region.Op.DIFFERENCE);
                drawable.draw(canvas);
                canvas.restoreToCount(save);
            }
        }
        int save2 = canvas.save();
        if (drawable2 != null) {
            drawable2.draw(canvas);
        }
        Layout layout = getTargetCheckedState() ? this.I : this.J;
        if (layout != null) {
            int[] drawableState = getDrawableState();
            ColorStateList colorStateList = this.H;
            if (colorStateList != null) {
                this.G.setColor(colorStateList.getColorForState(drawableState, 0));
            }
            this.G.drawableState = drawableState;
            if (drawable2 != null) {
                Rect bounds = drawable2.getBounds();
                i2 = bounds.left + bounds.right;
            } else {
                i2 = getWidth();
            }
            canvas.translate((float) ((i2 / 2) - (layout.getWidth() / 2)), (float) (((i5 + i6) / 2) - (layout.getHeight() / 2)));
            layout.draw(canvas);
        }
        canvas.restoreToCount(save2);
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName("android.widget.Switch");
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName("android.widget.Switch");
        CharSequence charSequence = isChecked() ? this.f122p : this.f123q;
        if (!TextUtils.isEmpty(charSequence)) {
            CharSequence text = accessibilityNodeInfo.getText();
            if (TextUtils.isEmpty(text)) {
                accessibilityNodeInfo.setText(charSequence);
                return;
            }
            StringBuilder sb = new StringBuilder();
            sb.append(text);
            sb.append(' ');
            sb.append(charSequence);
            accessibilityNodeInfo.setText(sb);
        }
    }

    public void onLayout(boolean z2, int i2, int i3, int i4, int i5) {
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        super.onLayout(z2, i2, i3, i4, i5);
        int i11 = 0;
        if (this.b != null) {
            Rect rect = this.N;
            Drawable drawable = this.g;
            if (drawable != null) {
                drawable.getPadding(rect);
            } else {
                rect.setEmpty();
            }
            Rect c2 = DrawableUtils.c(this.b);
            i6 = Math.max(0, c2.left - rect.left);
            i11 = Math.max(0, c2.right - rect.right);
        } else {
            i6 = 0;
        }
        if (ViewUtils.a(this)) {
            i8 = getPaddingLeft() + i6;
            i7 = ((this.z + i8) - i6) - i11;
        } else {
            i7 = (getWidth() - getPaddingRight()) - i11;
            i8 = (i7 - this.z) + i6 + i11;
        }
        int gravity = getGravity() & 112;
        if (gravity == 16) {
            int paddingTop = getPaddingTop();
            int i12 = this.A;
            int height = (((getHeight() + paddingTop) - getPaddingBottom()) / 2) - (i12 / 2);
            int i13 = height;
            i9 = i12 + height;
            i10 = i13;
        } else if (gravity != 80) {
            i10 = getPaddingTop();
            i9 = this.A + i10;
        } else {
            i9 = getHeight() - getPaddingBottom();
            i10 = i9 - this.A;
        }
        this.C = i8;
        this.D = i10;
        this.F = i9;
        this.E = i7;
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        int i5;
        int i6;
        if (this.f124r) {
            if (this.I == null) {
                this.I = a(this.f122p);
            }
            if (this.J == null) {
                this.J = a(this.f123q);
            }
        }
        Rect rect = this.N;
        Drawable drawable = this.b;
        int i7 = 0;
        if (drawable != null) {
            drawable.getPadding(rect);
            i5 = (this.b.getIntrinsicWidth() - rect.left) - rect.right;
            i4 = this.b.getIntrinsicHeight();
        } else {
            i5 = 0;
            i4 = 0;
        }
        if (this.f124r) {
            i6 = (this.f118l * 2) + Math.max(this.I.getWidth(), this.J.getWidth());
        } else {
            i6 = 0;
        }
        this.B = Math.max(i6, i5);
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.getPadding(rect);
            i7 = this.g.getIntrinsicHeight();
        } else {
            rect.setEmpty();
        }
        int i8 = rect.left;
        int i9 = rect.right;
        Drawable drawable3 = this.b;
        if (drawable3 != null) {
            Rect c2 = DrawableUtils.c(drawable3);
            i8 = Math.max(i8, c2.left);
            i9 = Math.max(i9, c2.right);
        }
        int max = Math.max(this.f119m, (this.B * 2) + i8 + i9);
        int max2 = Math.max(i7, i4);
        this.z = max;
        this.A = max2;
        super.onMeasure(i2, i3);
        if (getMeasuredHeight() < max2) {
            setMeasuredDimension(getMeasuredWidthAndState(), max2);
        }
    }

    public void onPopulateAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onPopulateAccessibilityEvent(accessibilityEvent);
        CharSequence charSequence = isChecked() ? this.f122p : this.f123q;
        if (charSequence != null) {
            accessibilityEvent.getText().add(charSequence);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:5:0x0014, code lost:
        if (r0 != 3) goto L_0x0155;
     */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r11) {
        /*
            r10 = this;
            android.view.VelocityTracker r0 = r10.w
            r0.addMovement(r11)
            int r0 = r11.getActionMasked()
            r1 = 0
            r2 = 1
            if (r0 == 0) goto L_0x0102
            r3 = 3
            r4 = 0
            r5 = 2
            if (r0 == r2) goto L_0x0095
            if (r0 == r5) goto L_0x0018
            if (r0 == r3) goto L_0x0095
            goto L_0x0155
        L_0x0018:
            int r0 = r10.f125s
            if (r0 == r2) goto L_0x0061
            if (r0 == r5) goto L_0x0020
            goto L_0x0155
        L_0x0020:
            float r11 = r11.getX()
            int r0 = r10.getThumbScrollRange()
            float r1 = r10.u
            float r1 = r11 - r1
            r3 = 1065353216(0x3f800000, float:1.0)
            if (r0 == 0) goto L_0x0033
            float r0 = (float) r0
            float r1 = r1 / r0
            goto L_0x003e
        L_0x0033:
            int r0 = (r1 > r4 ? 1 : (r1 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x003a
            r1 = 1065353216(0x3f800000, float:1.0)
            goto L_0x003e
        L_0x003a:
            r0 = -1082130432(0xffffffffbf800000, float:-1.0)
            r1 = -1082130432(0xffffffffbf800000, float:-1.0)
        L_0x003e:
            boolean r0 = i.b.q.ViewUtils.a(r10)
            if (r0 == 0) goto L_0x0045
            float r1 = -r1
        L_0x0045:
            float r0 = r10.y
            float r0 = r0 + r1
            int r1 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r1 >= 0) goto L_0x004d
            goto L_0x0055
        L_0x004d:
            int r1 = (r0 > r3 ? 1 : (r0 == r3 ? 0 : -1))
            if (r1 <= 0) goto L_0x0054
            r4 = 1065353216(0x3f800000, float:1.0)
            goto L_0x0055
        L_0x0054:
            r4 = r0
        L_0x0055:
            float r0 = r10.y
            int r0 = (r4 > r0 ? 1 : (r4 == r0 ? 0 : -1))
            if (r0 == 0) goto L_0x0060
            r10.u = r11
            r10.setThumbPosition(r4)
        L_0x0060:
            return r2
        L_0x0061:
            float r0 = r11.getX()
            float r1 = r11.getY()
            float r3 = r10.u
            float r3 = r0 - r3
            float r3 = java.lang.Math.abs(r3)
            int r4 = r10.f126t
            float r4 = (float) r4
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 > 0) goto L_0x0087
            float r3 = r10.v
            float r3 = r1 - r3
            float r3 = java.lang.Math.abs(r3)
            int r4 = r10.f126t
            float r4 = (float) r4
            int r3 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r3 <= 0) goto L_0x0155
        L_0x0087:
            r10.f125s = r5
            android.view.ViewParent r11 = r10.getParent()
            r11.requestDisallowInterceptTouchEvent(r2)
            r10.u = r0
            r10.v = r1
            return r2
        L_0x0095:
            int r0 = r10.f125s
            if (r0 != r5) goto L_0x00fa
            r10.f125s = r1
            int r0 = r11.getAction()
            if (r0 != r2) goto L_0x00a9
            boolean r0 = r10.isEnabled()
            if (r0 == 0) goto L_0x00a9
            r0 = 1
            goto L_0x00aa
        L_0x00a9:
            r0 = 0
        L_0x00aa:
            boolean r5 = r10.isChecked()
            if (r0 == 0) goto L_0x00e0
            android.view.VelocityTracker r0 = r10.w
            r6 = 1000(0x3e8, float:1.401E-42)
            r0.computeCurrentVelocity(r6)
            android.view.VelocityTracker r0 = r10.w
            float r0 = r0.getXVelocity()
            float r6 = java.lang.Math.abs(r0)
            int r7 = r10.x
            float r7 = (float) r7
            int r6 = (r6 > r7 ? 1 : (r6 == r7 ? 0 : -1))
            if (r6 <= 0) goto L_0x00db
            boolean r6 = i.b.q.ViewUtils.a(r10)
            if (r6 == 0) goto L_0x00d3
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 >= 0) goto L_0x00d9
            goto L_0x00d7
        L_0x00d3:
            int r0 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r0 <= 0) goto L_0x00d9
        L_0x00d7:
            r0 = 1
            goto L_0x00e1
        L_0x00d9:
            r0 = 0
            goto L_0x00e1
        L_0x00db:
            boolean r0 = r10.getTargetCheckedState()
            goto L_0x00e1
        L_0x00e0:
            r0 = r5
        L_0x00e1:
            if (r0 == r5) goto L_0x00e6
            r10.playSoundEffect(r1)
        L_0x00e6:
            r10.setChecked(r0)
            android.view.MotionEvent r0 = android.view.MotionEvent.obtain(r11)
            r0.setAction(r3)
            super.onTouchEvent(r0)
            r0.recycle()
            super.onTouchEvent(r11)
            return r2
        L_0x00fa:
            r10.f125s = r1
            android.view.VelocityTracker r0 = r10.w
            r0.clear()
            goto L_0x0155
        L_0x0102:
            float r0 = r11.getX()
            float r3 = r11.getY()
            boolean r4 = r10.isEnabled()
            if (r4 == 0) goto L_0x0155
            android.graphics.drawable.Drawable r4 = r10.b
            if (r4 != 0) goto L_0x0115
            goto L_0x014d
        L_0x0115:
            int r4 = r10.getThumbOffset()
            android.graphics.drawable.Drawable r5 = r10.b
            android.graphics.Rect r6 = r10.N
            r5.getPadding(r6)
            int r5 = r10.D
            int r6 = r10.f126t
            int r5 = r5 - r6
            int r7 = r10.C
            int r7 = r7 + r4
            int r7 = r7 - r6
            int r4 = r10.B
            int r4 = r4 + r7
            android.graphics.Rect r8 = r10.N
            int r9 = r8.left
            int r4 = r4 + r9
            int r8 = r8.right
            int r4 = r4 + r8
            int r4 = r4 + r6
            int r8 = r10.F
            int r8 = r8 + r6
            float r6 = (float) r7
            int r6 = (r0 > r6 ? 1 : (r0 == r6 ? 0 : -1))
            if (r6 <= 0) goto L_0x014d
            float r4 = (float) r4
            int r4 = (r0 > r4 ? 1 : (r0 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x014d
            float r4 = (float) r5
            int r4 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r4 <= 0) goto L_0x014d
            float r4 = (float) r8
            int r4 = (r3 > r4 ? 1 : (r3 == r4 ? 0 : -1))
            if (r4 >= 0) goto L_0x014d
            r1 = 1
        L_0x014d:
            if (r1 == 0) goto L_0x0155
            r10.f125s = r2
            r10.u = r0
            r10.v = r3
        L_0x0155:
            boolean r11 = super.onTouchEvent(r11)
            return r11
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.appcompat.widget.SwitchCompat.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setChecked(boolean z2) {
        super.setChecked(z2);
        boolean isChecked = isChecked();
        float f2 = 1.0f;
        if (getWindowToken() == null || !ViewCompat.w(this)) {
            ObjectAnimator objectAnimator = this.L;
            if (objectAnimator != null) {
                objectAnimator.cancel();
            }
            if (!isChecked) {
                f2 = 0.0f;
            }
            setThumbPosition(f2);
            return;
        }
        if (!isChecked) {
            f2 = 0.0f;
        }
        ObjectAnimator ofFloat = ObjectAnimator.ofFloat(this, O, f2);
        this.L = ofFloat;
        ofFloat.setDuration(250L);
        this.L.setAutoCancel(true);
        this.L.start();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
     arg types: [androidx.appcompat.widget.SwitchCompat, android.view.ActionMode$Callback]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback */
    public void setCustomSelectionActionModeCallback(ActionMode.Callback callback) {
        super.setCustomSelectionActionModeCallback(ResourcesFlusher.a((TextView) this, callback));
    }

    public void setShowText(boolean z2) {
        if (this.f124r != z2) {
            this.f124r = z2;
            requestLayout();
        }
    }

    public void setSplitTrack(boolean z2) {
        this.f121o = z2;
        invalidate();
    }

    public void setSwitchMinWidth(int i2) {
        this.f119m = i2;
        requestLayout();
    }

    public void setSwitchPadding(int i2) {
        this.f120n = i2;
        requestLayout();
    }

    public void setSwitchTypeface(Typeface typeface) {
        if ((this.G.getTypeface() != null && !this.G.getTypeface().equals(typeface)) || (this.G.getTypeface() == null && typeface != null)) {
            this.G.setTypeface(typeface);
            requestLayout();
            invalidate();
        }
    }

    public void setTextOff(CharSequence charSequence) {
        this.f123q = charSequence;
        requestLayout();
    }

    public void setTextOn(CharSequence charSequence) {
        this.f122p = charSequence;
        requestLayout();
    }

    public void setThumbDrawable(Drawable drawable) {
        Drawable drawable2 = this.b;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.b = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
        requestLayout();
    }

    public void setThumbPosition(float f2) {
        this.y = f2;
        invalidate();
    }

    public void setThumbResource(int i2) {
        setThumbDrawable(AppCompatResources.c(getContext(), i2));
    }

    public void setThumbTextPadding(int i2) {
        this.f118l = i2;
        requestLayout();
    }

    public void setThumbTintList(ColorStateList colorStateList) {
        this.c = colorStateList;
        this.f113e = true;
        a();
    }

    public void setThumbTintMode(PorterDuff.Mode mode) {
        this.d = mode;
        this.f114f = true;
        a();
    }

    public void setTrackDrawable(Drawable drawable) {
        Drawable drawable2 = this.g;
        if (drawable2 != null) {
            drawable2.setCallback(null);
        }
        this.g = drawable;
        if (drawable != null) {
            drawable.setCallback(this);
        }
        requestLayout();
    }

    public void setTrackResource(int i2) {
        setTrackDrawable(AppCompatResources.c(getContext(), i2));
    }

    public void setTrackTintList(ColorStateList colorStateList) {
        this.h = colorStateList;
        this.f116j = true;
        b();
    }

    public void setTrackTintMode(PorterDuff.Mode mode) {
        this.f115i = mode;
        this.f117k = true;
        b();
    }

    public void toggle() {
        setChecked(!isChecked());
    }

    public boolean verifyDrawable(Drawable drawable) {
        return super.verifyDrawable(drawable) || drawable == this.b || drawable == this.g;
    }

    public SwitchCompat(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, i.b.a.switchStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.TintTypedArray.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      i.b.q.TintTypedArray.a(int, float):float
      i.b.q.TintTypedArray.a(int, int):int
      i.b.q.TintTypedArray.a(int, boolean):boolean */
    public SwitchCompat(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Typeface typeface;
        Typeface typeface2;
        int resourceId;
        this.c = null;
        this.d = null;
        this.f113e = false;
        this.f114f = false;
        this.h = null;
        this.f115i = null;
        this.f116j = false;
        this.f117k = false;
        this.w = VelocityTracker.obtain();
        this.N = new Rect();
        boolean z2 = true;
        this.G = new TextPaint(1);
        this.G.density = getResources().getDisplayMetrics().density;
        TintTypedArray tintTypedArray = new TintTypedArray(context, context.obtainStyledAttributes(attributeSet, j.SwitchCompat, i2, 0));
        Drawable b2 = tintTypedArray.b(j.SwitchCompat_android_thumb);
        this.b = b2;
        if (b2 != null) {
            b2.setCallback(this);
        }
        Drawable b3 = tintTypedArray.b(j.SwitchCompat_track);
        this.g = b3;
        if (b3 != null) {
            b3.setCallback(this);
        }
        this.f122p = tintTypedArray.e(j.SwitchCompat_android_textOn);
        this.f123q = tintTypedArray.e(j.SwitchCompat_android_textOff);
        this.f124r = tintTypedArray.a(j.SwitchCompat_showText, true);
        this.f118l = tintTypedArray.c(j.SwitchCompat_thumbTextPadding, 0);
        this.f119m = tintTypedArray.c(j.SwitchCompat_switchMinWidth, 0);
        this.f120n = tintTypedArray.c(j.SwitchCompat_switchPadding, 0);
        this.f121o = tintTypedArray.a(j.SwitchCompat_splitTrack, false);
        ColorStateList a2 = tintTypedArray.a(j.SwitchCompat_thumbTint);
        if (a2 != null) {
            this.c = a2;
            this.f113e = true;
        }
        PorterDuff.Mode a3 = DrawableUtils.a(tintTypedArray.d(j.SwitchCompat_thumbTintMode, -1), null);
        if (this.d != a3) {
            this.d = a3;
            this.f114f = true;
        }
        if (this.f113e || this.f114f) {
            a();
        }
        ColorStateList a4 = tintTypedArray.a(j.SwitchCompat_trackTint);
        if (a4 != null) {
            this.h = a4;
            this.f116j = true;
        }
        PorterDuff.Mode a5 = DrawableUtils.a(tintTypedArray.d(j.SwitchCompat_trackTintMode, -1), null);
        if (this.f115i != a5) {
            this.f115i = a5;
            this.f117k = true;
        }
        if (this.f116j || this.f117k) {
            b();
        }
        int f2 = tintTypedArray.f(j.SwitchCompat_switchTextAppearance, 0);
        if (f2 != 0) {
            TypedArray obtainStyledAttributes = context.obtainStyledAttributes(f2, j.TextAppearance);
            int i3 = j.TextAppearance_android_textColor;
            ColorStateList colorStateList = (!obtainStyledAttributes.hasValue(i3) || (resourceId = obtainStyledAttributes.getResourceId(i3, 0)) == 0 || (colorStateList = AppCompatResources.b(context, resourceId)) == null) ? obtainStyledAttributes.getColorStateList(i3) : colorStateList;
            if (colorStateList != null) {
                this.H = colorStateList;
            } else {
                this.H = getTextColors();
            }
            int dimensionPixelSize = obtainStyledAttributes.getDimensionPixelSize(j.TextAppearance_android_textSize, 0);
            if (dimensionPixelSize != 0) {
                float f3 = (float) dimensionPixelSize;
                if (f3 != this.G.getTextSize()) {
                    this.G.setTextSize(f3);
                    requestLayout();
                }
            }
            int i4 = obtainStyledAttributes.getInt(j.TextAppearance_android_typeface, -1);
            int i5 = obtainStyledAttributes.getInt(j.TextAppearance_android_textStyle, -1);
            if (i4 == 1) {
                typeface = Typeface.SANS_SERIF;
            } else if (i4 == 2) {
                typeface = Typeface.SERIF;
            } else if (i4 != 3) {
                typeface = null;
            } else {
                typeface = Typeface.MONOSPACE;
            }
            float f4 = 0.0f;
            if (i5 > 0) {
                if (typeface == null) {
                    typeface2 = Typeface.defaultFromStyle(i5);
                } else {
                    typeface2 = Typeface.create(typeface, i5);
                }
                setSwitchTypeface(typeface2);
                int i6 = i5 & (~(typeface2 != null ? typeface2.getStyle() : 0));
                this.G.setFakeBoldText((i6 & 1) == 0 ? false : z2);
                this.G.setTextSkewX((i6 & 2) != 0 ? -0.25f : f4);
            } else {
                this.G.setFakeBoldText(false);
                this.G.setTextSkewX(0.0f);
                setSwitchTypeface(typeface);
            }
            if (obtainStyledAttributes.getBoolean(j.TextAppearance_textAllCaps, false)) {
                this.K = new AllCapsTransformationMethod(getContext());
            } else {
                this.K = null;
            }
            obtainStyledAttributes.recycle();
        }
        AppCompatTextHelper appCompatTextHelper = new AppCompatTextHelper(this);
        this.M = appCompatTextHelper;
        appCompatTextHelper.a(attributeSet, i2);
        tintTypedArray.b.recycle();
        ViewConfiguration viewConfiguration = ViewConfiguration.get(context);
        this.f126t = viewConfiguration.getScaledTouchSlop();
        this.x = viewConfiguration.getScaledMinimumFlingVelocity();
        refreshDrawableState();
        setChecked(isChecked());
    }

    public final Layout a(CharSequence charSequence) {
        TransformationMethod transformationMethod = this.K;
        if (transformationMethod != null) {
            charSequence = transformationMethod.getTransformation(charSequence, this);
        }
        CharSequence charSequence2 = charSequence;
        TextPaint textPaint = this.G;
        return new StaticLayout(charSequence2, textPaint, charSequence2 != null ? (int) Math.ceil((double) Layout.getDesiredWidth(charSequence2, textPaint)) : 0, Layout.Alignment.ALIGN_NORMAL, 1.0f, 0.0f, true);
    }
}
