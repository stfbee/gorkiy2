package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.TextView;
import com.crashlytics.android.core.CodedOutputStream;
import com.crashlytics.android.core.LogFileManager;
import i.b.a;
import i.b.f;
import i.b.g;
import i.b.h;
import i.b.j;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuView;
import i.b.q.TintTypedArray;
import i.h.l.ViewCompat;

public class ListMenuItemView extends LinearLayout implements MenuView.a, AbsListView.SelectionBoundsAdjuster {
    public MenuItemImpl b;
    public ImageView c;
    public RadioButton d;

    /* renamed from: e  reason: collision with root package name */
    public TextView f44e;

    /* renamed from: f  reason: collision with root package name */
    public CheckBox f45f;
    public TextView g;
    public ImageView h;

    /* renamed from: i  reason: collision with root package name */
    public ImageView f46i;

    /* renamed from: j  reason: collision with root package name */
    public LinearLayout f47j;

    /* renamed from: k  reason: collision with root package name */
    public Drawable f48k;

    /* renamed from: l  reason: collision with root package name */
    public int f49l;

    /* renamed from: m  reason: collision with root package name */
    public Context f50m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f51n;

    /* renamed from: o  reason: collision with root package name */
    public Drawable f52o;

    /* renamed from: p  reason: collision with root package name */
    public boolean f53p;

    /* renamed from: q  reason: collision with root package name */
    public LayoutInflater f54q;

    /* renamed from: r  reason: collision with root package name */
    public boolean f55r;

    public ListMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, a.listMenuViewStyle);
    }

    private LayoutInflater getInflater() {
        if (this.f54q == null) {
            this.f54q = LayoutInflater.from(getContext());
        }
        return this.f54q;
    }

    private void setSubMenuArrowVisible(boolean z) {
        ImageView imageView = this.h;
        if (imageView != null) {
            imageView.setVisibility(z ? 0 : 8);
        }
    }

    public void a(MenuItemImpl menuItemImpl, int i2) {
        CharSequence charSequence;
        String str;
        this.b = menuItemImpl;
        int i3 = 0;
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        if (c()) {
            charSequence = menuItemImpl.getTitleCondensed();
        } else {
            charSequence = menuItemImpl.f896e;
        }
        setTitle(charSequence);
        setCheckable(menuItemImpl.isCheckable());
        boolean f2 = menuItemImpl.f();
        menuItemImpl.b();
        if (!f2 || !this.b.f()) {
            i3 = 8;
        }
        if (i3 == 0) {
            TextView textView = this.g;
            MenuItemImpl menuItemImpl2 = this.b;
            char b2 = menuItemImpl2.b();
            if (b2 == 0) {
                str = "";
            } else {
                Resources resources = menuItemImpl2.f903n.a.getResources();
                StringBuilder sb = new StringBuilder();
                if (ViewConfiguration.get(menuItemImpl2.f903n.a).hasPermanentMenuKey()) {
                    sb.append(resources.getString(h.abc_prepend_shortcut_label));
                }
                int i4 = menuItemImpl2.f903n.f() ? menuItemImpl2.f900k : menuItemImpl2.f898i;
                MenuItemImpl.a(sb, i4, LogFileManager.MAX_LOG_SIZE, resources.getString(h.abc_menu_meta_shortcut_label));
                MenuItemImpl.a(sb, i4, CodedOutputStream.DEFAULT_BUFFER_SIZE, resources.getString(h.abc_menu_ctrl_shortcut_label));
                MenuItemImpl.a(sb, i4, 2, resources.getString(h.abc_menu_alt_shortcut_label));
                MenuItemImpl.a(sb, i4, 1, resources.getString(h.abc_menu_shift_shortcut_label));
                MenuItemImpl.a(sb, i4, 4, resources.getString(h.abc_menu_sym_shortcut_label));
                MenuItemImpl.a(sb, i4, 8, resources.getString(h.abc_menu_function_shortcut_label));
                if (b2 == 8) {
                    sb.append(resources.getString(h.abc_menu_delete_shortcut_label));
                } else if (b2 == 10) {
                    sb.append(resources.getString(h.abc_menu_enter_shortcut_label));
                } else if (b2 != ' ') {
                    sb.append(b2);
                } else {
                    sb.append(resources.getString(h.abc_menu_space_shortcut_label));
                }
                str = sb.toString();
            }
            textView.setText(str);
        }
        if (this.g.getVisibility() != i3) {
            this.g.setVisibility(i3);
        }
        setIcon(menuItemImpl.getIcon());
        setEnabled(menuItemImpl.isEnabled());
        setSubMenuArrowVisible(menuItemImpl.hasSubMenu());
        setContentDescription(menuItemImpl.f906q);
    }

    public void adjustListItemSelectionBounds(Rect rect) {
        ImageView imageView = this.f46i;
        if (imageView != null && imageView.getVisibility() == 0) {
            LinearLayout.LayoutParams layoutParams = (LinearLayout.LayoutParams) this.f46i.getLayoutParams();
            rect.top = this.f46i.getHeight() + layoutParams.topMargin + layoutParams.bottomMargin + rect.top;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void b() {
        RadioButton radioButton = (RadioButton) getInflater().inflate(g.abc_list_menu_item_radio, (ViewGroup) this, false);
        this.d = radioButton;
        LinearLayout linearLayout = this.f47j;
        if (linearLayout != null) {
            super.addView(radioButton, -1);
        } else {
            addView(radioButton, -1);
        }
    }

    public boolean c() {
        return false;
    }

    public MenuItemImpl getItemData() {
        return this.b;
    }

    public void onFinishInflate() {
        super.onFinishInflate();
        ViewCompat.a(this, this.f48k);
        TextView textView = (TextView) findViewById(f.title);
        this.f44e = textView;
        int i2 = this.f49l;
        if (i2 != -1) {
            textView.setTextAppearance(this.f50m, i2);
        }
        this.g = (TextView) findViewById(f.shortcut);
        ImageView imageView = (ImageView) findViewById(f.submenuarrow);
        this.h = imageView;
        if (imageView != null) {
            imageView.setImageDrawable(this.f52o);
        }
        this.f46i = (ImageView) findViewById(f.group_divider);
        this.f47j = (LinearLayout) findViewById(f.content);
    }

    public void onMeasure(int i2, int i3) {
        if (this.c != null && this.f51n) {
            ViewGroup.LayoutParams layoutParams = getLayoutParams();
            LinearLayout.LayoutParams layoutParams2 = (LinearLayout.LayoutParams) this.c.getLayoutParams();
            int i4 = layoutParams.height;
            if (i4 > 0 && layoutParams2.width <= 0) {
                layoutParams2.width = i4;
            }
        }
        super.onMeasure(i2, i3);
    }

    public void setCheckable(boolean z) {
        CompoundButton compoundButton;
        CompoundButton compoundButton2;
        if (z || this.d != null || this.f45f != null) {
            if (this.b.e()) {
                if (this.d == null) {
                    b();
                }
                compoundButton2 = this.d;
                compoundButton = this.f45f;
            } else {
                if (this.f45f == null) {
                    a();
                }
                compoundButton2 = this.f45f;
                compoundButton = this.d;
            }
            if (z) {
                compoundButton2.setChecked(this.b.isChecked());
                if (compoundButton2.getVisibility() != 0) {
                    compoundButton2.setVisibility(0);
                }
                if (compoundButton != null && compoundButton.getVisibility() != 8) {
                    compoundButton.setVisibility(8);
                    return;
                }
                return;
            }
            CheckBox checkBox = this.f45f;
            if (checkBox != null) {
                checkBox.setVisibility(8);
            }
            RadioButton radioButton = this.d;
            if (radioButton != null) {
                radioButton.setVisibility(8);
            }
        }
    }

    public void setChecked(boolean z) {
        CompoundButton compoundButton;
        if (this.b.e()) {
            if (this.d == null) {
                b();
            }
            compoundButton = this.d;
        } else {
            if (this.f45f == null) {
                a();
            }
            compoundButton = this.f45f;
        }
        compoundButton.setChecked(z);
    }

    public void setForceShowIcon(boolean z) {
        this.f55r = z;
        this.f51n = z;
    }

    public void setGroupDividerEnabled(boolean z) {
        ImageView imageView = this.f46i;
        if (imageView != null) {
            imageView.setVisibility((this.f53p || !z) ? 8 : 0);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public void setIcon(Drawable drawable) {
        boolean z = this.b.f903n.f894s || this.f55r;
        if (!z && !this.f51n) {
            return;
        }
        if (this.c != null || drawable != null || this.f51n) {
            if (this.c == null) {
                ImageView imageView = (ImageView) getInflater().inflate(g.abc_list_menu_item_icon, (ViewGroup) this, false);
                this.c = imageView;
                LinearLayout linearLayout = this.f47j;
                if (linearLayout != null) {
                    super.addView(imageView, 0);
                } else {
                    addView(imageView, 0);
                }
            }
            if (drawable != null || this.f51n) {
                ImageView imageView2 = this.c;
                if (!z) {
                    drawable = null;
                }
                imageView2.setImageDrawable(drawable);
                if (this.c.getVisibility() != 0) {
                    this.c.setVisibility(0);
                    return;
                }
                return;
            }
            this.c.setVisibility(8);
        }
    }

    public void setTitle(CharSequence charSequence) {
        if (charSequence != null) {
            this.f44e.setText(charSequence);
            if (this.f44e.getVisibility() != 0) {
                this.f44e.setVisibility(0);
            }
        } else if (this.f44e.getVisibility() != 8) {
            this.f44e.setVisibility(8);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.q.TintTypedArray.a(int, boolean):boolean
     arg types: [int, int]
     candidates:
      i.b.q.TintTypedArray.a(int, float):float
      i.b.q.TintTypedArray.a(int, int):int
      i.b.q.TintTypedArray.a(int, boolean):boolean */
    public ListMenuItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet);
        TintTypedArray a = TintTypedArray.a(getContext(), attributeSet, j.MenuView, i2, 0);
        this.f48k = a.b(j.MenuView_android_itemBackground);
        this.f49l = a.f(j.MenuView_android_itemTextAppearance, -1);
        this.f51n = a.a(j.MenuView_preserveIconSpacing, false);
        this.f50m = context;
        this.f52o = a.b(j.MenuView_subMenuArrow);
        TypedArray obtainStyledAttributes = context.getTheme().obtainStyledAttributes(null, new int[]{16843049}, a.dropDownListViewStyle, 0);
        this.f53p = obtainStyledAttributes.hasValue(0);
        a.b.recycle();
        obtainStyledAttributes.recycle();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View}
     arg types: [int, androidx.appcompat.view.menu.ListMenuItemView, int]
     candidates:
      ClspMth{android.view.LayoutInflater.inflate(org.xmlpull.v1.XmlPullParser, android.view.ViewGroup, boolean):android.view.View}
      ClspMth{android.view.LayoutInflater.inflate(int, android.view.ViewGroup, boolean):android.view.View} */
    public final void a() {
        CheckBox checkBox = (CheckBox) getInflater().inflate(g.abc_list_menu_item_checkbox, (ViewGroup) this, false);
        this.f45f = checkBox;
        LinearLayout linearLayout = this.f47j;
        if (linearLayout != null) {
            super.addView(checkBox, -1);
        } else {
            addView(checkBox, -1);
        }
    }
}
