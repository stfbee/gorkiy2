package androidx.appcompat.view.menu;

import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Parcelable;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import androidx.appcompat.widget.ActionMenuView;
import i.b.j;
import i.b.k.ResourcesFlusher;
import i.b.p.i.MenuBuilder;
import i.b.p.i.MenuItemImpl;
import i.b.p.i.MenuView;
import i.b.p.i.ShowableListMenu;
import i.b.q.ActionMenuPresenter;
import i.b.q.AppCompatTextView;
import i.b.q.ForwardingListener;

public class ActionMenuItemView extends AppCompatTextView implements MenuView.a, View.OnClickListener, ActionMenuView.a {

    /* renamed from: f  reason: collision with root package name */
    public MenuItemImpl f34f;
    public CharSequence g;
    public Drawable h;

    /* renamed from: i  reason: collision with root package name */
    public MenuBuilder.b f35i;

    /* renamed from: j  reason: collision with root package name */
    public ForwardingListener f36j;

    /* renamed from: k  reason: collision with root package name */
    public b f37k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f38l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f39m;

    /* renamed from: n  reason: collision with root package name */
    public int f40n;

    /* renamed from: o  reason: collision with root package name */
    public int f41o;

    /* renamed from: p  reason: collision with root package name */
    public int f42p;

    public class a extends ForwardingListener {
        public a() {
            super(ActionMenuItemView.this);
        }

        public ShowableListMenu b() {
            ActionMenuPresenter.a aVar;
            b bVar = ActionMenuItemView.this.f37k;
            if (bVar == null || (aVar = ActionMenuPresenter.this.v) == null) {
                return null;
            }
            return aVar.a();
        }

        public boolean c() {
            ShowableListMenu b;
            ActionMenuItemView actionMenuItemView = ActionMenuItemView.this;
            MenuBuilder.b bVar = actionMenuItemView.f35i;
            if (bVar == null || !bVar.a(actionMenuItemView.f34f) || (b = b()) == null || !b.b()) {
                return false;
            }
            return true;
        }
    }

    public static abstract class b {
    }

    public ActionMenuItemView(Context context) {
        this(context, null);
    }

    public void a(MenuItemImpl menuItemImpl, int i2) {
        CharSequence charSequence;
        this.f34f = menuItemImpl;
        setIcon(menuItemImpl.getIcon());
        if (c()) {
            charSequence = menuItemImpl.getTitleCondensed();
        } else {
            charSequence = menuItemImpl.f896e;
        }
        setTitle(charSequence);
        setId(menuItemImpl.a);
        setVisibility(menuItemImpl.isVisible() ? 0 : 8);
        setEnabled(menuItemImpl.isEnabled());
        if (menuItemImpl.hasSubMenu() && this.f36j == null) {
            this.f36j = new a();
        }
    }

    public boolean b() {
        return d() && this.f34f.getIcon() == null;
    }

    public boolean c() {
        return true;
    }

    public boolean d() {
        return !TextUtils.isEmpty(getText());
    }

    public final boolean e() {
        Configuration configuration = getContext().getResources().getConfiguration();
        int i2 = configuration.screenWidthDp;
        return i2 >= 480 || (i2 >= 640 && configuration.screenHeightDp >= 480) || configuration.orientation == 2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
     arg types: [androidx.appcompat.view.menu.ActionMenuItemView, java.lang.CharSequence]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void */
    public final void f() {
        CharSequence charSequence;
        boolean z = true;
        boolean z2 = !TextUtils.isEmpty(this.g);
        if (this.h != null) {
            if (!((this.f34f.y & 4) == 4) || (!this.f38l && !this.f39m)) {
                z = false;
            }
        }
        boolean z3 = z2 & z;
        CharSequence charSequence2 = null;
        setText(z3 ? this.g : null);
        CharSequence charSequence3 = this.f34f.f906q;
        if (TextUtils.isEmpty(charSequence3)) {
            if (z3) {
                charSequence = null;
            } else {
                charSequence = this.f34f.f896e;
            }
            setContentDescription(charSequence);
        } else {
            setContentDescription(charSequence3);
        }
        CharSequence charSequence4 = this.f34f.f907r;
        if (TextUtils.isEmpty(charSequence4)) {
            if (!z3) {
                charSequence2 = this.f34f.f896e;
            }
            ResourcesFlusher.a((View) this, charSequence2);
            return;
        }
        ResourcesFlusher.a((View) this, charSequence4);
    }

    public MenuItemImpl getItemData() {
        return this.f34f;
    }

    public void onClick(View view) {
        MenuBuilder.b bVar = this.f35i;
        if (bVar != null) {
            bVar.a(this.f34f);
        }
    }

    public void onConfigurationChanged(Configuration configuration) {
        super.onConfigurationChanged(configuration);
        this.f38l = e();
        f();
    }

    public void onMeasure(int i2, int i3) {
        int i4;
        boolean d = d();
        if (d && (i4 = this.f41o) >= 0) {
            super.setPadding(i4, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
        super.onMeasure(i2, i3);
        int mode = View.MeasureSpec.getMode(i2);
        int size = View.MeasureSpec.getSize(i2);
        int measuredWidth = getMeasuredWidth();
        int min = mode == Integer.MIN_VALUE ? Math.min(size, this.f40n) : this.f40n;
        if (mode != 1073741824 && this.f40n > 0 && measuredWidth < min) {
            super.onMeasure(View.MeasureSpec.makeMeasureSpec(min, 1073741824), i3);
        }
        if (!d && this.h != null) {
            super.setPadding((getMeasuredWidth() - this.h.getBounds().width()) / 2, getPaddingTop(), getPaddingRight(), getPaddingBottom());
        }
    }

    public void onRestoreInstanceState(Parcelable parcelable) {
        super.onRestoreInstanceState(null);
    }

    public boolean onTouchEvent(MotionEvent motionEvent) {
        ForwardingListener forwardingListener;
        if (!this.f34f.hasSubMenu() || (forwardingListener = this.f36j) == null || !forwardingListener.onTouch(this, motionEvent)) {
            return super.onTouchEvent(motionEvent);
        }
        return true;
    }

    public void setCheckable(boolean z) {
    }

    public void setChecked(boolean z) {
    }

    public void setExpandedFormat(boolean z) {
        if (this.f39m != z) {
            this.f39m = z;
            MenuItemImpl menuItemImpl = this.f34f;
            if (menuItemImpl != null) {
                menuItemImpl.f903n.h();
            }
        }
    }

    public void setIcon(Drawable drawable) {
        this.h = drawable;
        if (drawable != null) {
            int intrinsicWidth = drawable.getIntrinsicWidth();
            int intrinsicHeight = drawable.getIntrinsicHeight();
            int i2 = this.f42p;
            if (intrinsicWidth > i2) {
                intrinsicHeight = (int) (((float) intrinsicHeight) * (((float) i2) / ((float) intrinsicWidth)));
                intrinsicWidth = i2;
            }
            int i3 = this.f42p;
            if (intrinsicHeight > i3) {
                intrinsicWidth = (int) (((float) intrinsicWidth) * (((float) i3) / ((float) intrinsicHeight)));
                intrinsicHeight = i3;
            }
            drawable.setBounds(0, 0, intrinsicWidth, intrinsicHeight);
        }
        setCompoundDrawables(drawable, null, null, null);
        f();
    }

    public void setItemInvoker(MenuBuilder.b bVar) {
        this.f35i = bVar;
    }

    public void setPadding(int i2, int i3, int i4, int i5) {
        this.f41o = i2;
        super.setPadding(i2, i3, i4, i5);
    }

    public void setPopupCallback(b bVar) {
        this.f37k = bVar;
    }

    public void setTitle(CharSequence charSequence) {
        this.g = charSequence;
        f();
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, 0);
    }

    public ActionMenuItemView(Context context, AttributeSet attributeSet, int i2) {
        super(context, attributeSet, i2);
        Resources resources = context.getResources();
        this.f38l = e();
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, j.ActionMenuItemView, i2, 0);
        this.f40n = obtainStyledAttributes.getDimensionPixelSize(j.ActionMenuItemView_android_minWidth, 0);
        obtainStyledAttributes.recycle();
        this.f42p = (int) ((resources.getDisplayMetrics().density * 32.0f) + 0.5f);
        setOnClickListener(this);
        this.f41o = -1;
        setSaveEnabled(false);
    }

    public boolean a() {
        return d();
    }
}
