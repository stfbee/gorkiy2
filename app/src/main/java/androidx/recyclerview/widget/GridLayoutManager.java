package androidx.recyclerview.widget;

import android.content.Context;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseIntArray;
import android.view.View;
import android.view.ViewGroup;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.r.d.GapWorker;
import j.a.a.a.outline;
import java.util.Arrays;

public class GridLayoutManager extends LinearLayoutManager {
    public boolean H = false;
    public int I = -1;
    public int[] J;
    public View[] K;
    public final SparseIntArray L = new SparseIntArray();
    public final SparseIntArray M = new SparseIntArray();
    public c N = new a();
    public final Rect O = new Rect();

    public static final class a extends c {
    }

    public static abstract class c {
        public final SparseIntArray a = new SparseIntArray();
        public final SparseIntArray b = new SparseIntArray();
        public boolean c = false;
        public boolean d = false;

        public int a(int i2, int i3) {
            if (!this.d) {
                return c(i2, i3);
            }
            int i4 = this.b.get(i2, -1);
            if (i4 != -1) {
                return i4;
            }
            int c2 = c(i2, i3);
            this.b.put(i2, c2);
            return c2;
        }

        public int b(int i2, int i3) {
            if (!this.c) {
                return i2 % i3;
            }
            int i4 = this.a.get(i2, -1);
            if (i4 != -1) {
                return i4;
            }
            int i5 = i2 % i3;
            this.a.put(i2, i5);
            return i5;
        }

        /* JADX WARNING: Removed duplicated region for block: B:20:0x0049  */
        /* JADX WARNING: Removed duplicated region for block: B:28:0x005c  */
        /* JADX WARNING: Removed duplicated region for block: B:39:? A[RETURN, SYNTHETIC] */
        /* Code decompiled incorrectly, please refer to instructions dump. */
        public int c(int r9, int r10) {
            /*
                r8 = this;
                boolean r0 = r8.d
                r1 = 0
                r2 = 1
                if (r0 == 0) goto L_0x0044
                android.util.SparseIntArray r0 = r8.b
                int r3 = r0.size()
                r4 = -1
                int r3 = r3 + r4
                r5 = 0
            L_0x000f:
                if (r5 > r3) goto L_0x0020
                int r6 = r5 + r3
                int r6 = r6 >>> r2
                int r7 = r0.keyAt(r6)
                if (r7 >= r9) goto L_0x001d
                int r5 = r6 + 1
                goto L_0x000f
            L_0x001d:
                int r3 = r6 + -1
                goto L_0x000f
            L_0x0020:
                int r5 = r5 + r4
                if (r5 < 0) goto L_0x002e
                int r3 = r0.size()
                if (r5 >= r3) goto L_0x002e
                int r0 = r0.keyAt(r5)
                goto L_0x002f
            L_0x002e:
                r0 = -1
            L_0x002f:
                if (r0 == r4) goto L_0x0044
                android.util.SparseIntArray r3 = r8.b
                int r3 = r3.get(r0)
                int r4 = r0 + 1
                int r0 = r8.b(r0, r10)
                int r0 = r0 + r2
                if (r0 != r10) goto L_0x0047
                int r3 = r3 + 1
                r0 = 0
                goto L_0x0047
            L_0x0044:
                r0 = 0
                r3 = 0
                r4 = 0
            L_0x0047:
                if (r4 >= r9) goto L_0x0059
                int r0 = r0 + 1
                if (r0 != r10) goto L_0x0051
                int r3 = r3 + 1
                r0 = 0
                goto L_0x0056
            L_0x0051:
                if (r0 <= r10) goto L_0x0056
                int r3 = r3 + 1
                r0 = 1
            L_0x0056:
                int r4 = r4 + 1
                goto L_0x0047
            L_0x0059:
                int r0 = r0 + r2
                if (r0 <= r10) goto L_0x005e
                int r3 = r3 + 1
            L_0x005e:
                return r3
            */
            throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.c.c(int, int):int");
        }
    }

    public GridLayoutManager(Context context, AttributeSet attributeSet, int i2, int i3) {
        super(context, attributeSet, i2, i3);
        l(RecyclerView.o.a(context, attributeSet, i2, i3).b);
    }

    public final void A() {
        int i2;
        int i3;
        if (super.f280s == 1) {
            i3 = this.f326q - k();
            i2 = j();
        } else {
            i3 = this.f327r - i();
            i2 = l();
        }
        k(i3 - i2);
    }

    public int a(RecyclerView.v vVar, RecyclerView.a0 a0Var) {
        if (super.f280s == 1) {
            return this.I;
        }
        if (a0Var.a() < 1) {
            return 0;
        }
        return a(vVar, a0Var, a0Var.a() - 1) + 1;
    }

    public int b(RecyclerView.v vVar, RecyclerView.a0 a0Var) {
        if (super.f280s == 0) {
            return this.I;
        }
        if (a0Var.a() < 1) {
            return 0;
        }
        return a(vVar, a0Var, a0Var.a() - 1) + 1;
    }

    public RecyclerView.p c() {
        if (super.f280s == 0) {
            return new b(-2, -1);
        }
        return new b(-1, -2);
    }

    public int e(RecyclerView.a0 a0Var) {
        return i(a0Var);
    }

    public int f(int i2, int i3) {
        if (super.f280s != 1 || !w()) {
            int[] iArr = this.J;
            return iArr[i3 + i2] - iArr[i2];
        }
        int[] iArr2 = this.J;
        int i4 = this.I;
        return iArr2[i4 - i2] - iArr2[(i4 - i2) - i3];
    }

    public void g(RecyclerView.a0 a0Var) {
        super.C = null;
        super.A = -1;
        super.B = RecyclerView.UNDEFINED_DURATION;
        super.D.b();
        this.H = false;
    }

    public final void k(int i2) {
        int i3;
        int[] iArr = this.J;
        int i4 = this.I;
        if (!(iArr != null && iArr.length == i4 + 1 && iArr[iArr.length - 1] == i2)) {
            iArr = new int[(i4 + 1)];
        }
        int i5 = 0;
        iArr[0] = 0;
        int i6 = i2 / i4;
        int i7 = i2 % i4;
        int i8 = 0;
        for (int i9 = 1; i9 <= i4; i9++) {
            i5 += i7;
            if (i5 <= 0 || i4 - i5 >= i7) {
                i3 = i6;
            } else {
                i3 = i6 + 1;
                i5 -= i4;
            }
            i8 += i3;
            iArr[i9] = i8;
        }
        this.J = iArr;
    }

    public void l(int i2) {
        if (i2 != this.I) {
            this.H = true;
            if (i2 >= 1) {
                this.I = i2;
                this.N.a.clear();
                o();
                return;
            }
            throw new IllegalArgumentException(outline.b("Span count should be at least 1. Provided ", i2));
        }
    }

    public boolean q() {
        return super.C == null && !this.H;
    }

    public final void z() {
        View[] viewArr = this.K;
        if (viewArr == null || viewArr.length != this.I) {
            this.K = new View[this.I];
        }
    }

    public static class b extends RecyclerView.p {

        /* renamed from: e  reason: collision with root package name */
        public int f278e = -1;

        /* renamed from: f  reason: collision with root package name */
        public int f279f = 0;

        public b(Context context, AttributeSet attributeSet) {
            super(context, attributeSet);
        }

        public b(int i2, int i3) {
            super(i2, i3);
        }

        public b(ViewGroup.MarginLayoutParams marginLayoutParams) {
            super(marginLayoutParams);
        }

        public b(ViewGroup.LayoutParams layoutParams) {
            super(layoutParams);
        }
    }

    public int c(RecyclerView.a0 a0Var) {
        return j(a0Var);
    }

    public int f(RecyclerView.a0 a0Var) {
        return j(a0Var);
    }

    public void a(RecyclerView.v vVar, RecyclerView.a0 a0Var, View view, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
        ViewGroup.LayoutParams layoutParams = view.getLayoutParams();
        if (!(layoutParams instanceof b)) {
            super.a(view, accessibilityNodeInfoCompat);
            return;
        }
        b bVar = (b) layoutParams;
        int a2 = a(vVar, a0Var, bVar.a());
        if (super.f280s == 0) {
            accessibilityNodeInfoCompat.b(AccessibilityNodeInfoCompat.c.a(bVar.f278e, bVar.f279f, a2, 1, false, false));
            return;
        }
        accessibilityNodeInfoCompat.b(AccessibilityNodeInfoCompat.c.a(a2, 1, bVar.f278e, bVar.f279f, false, false));
    }

    public void b(RecyclerView recyclerView, int i2, int i3) {
        this.N.a.clear();
        this.N.b.clear();
    }

    public final int c(RecyclerView.v vVar, RecyclerView.a0 a0Var, int i2) {
        if (a0Var.g) {
            int i3 = this.L.get(i2, -1);
            if (i3 != -1) {
                return i3;
            }
            if (vVar.a(i2) == -1) {
                Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i2);
                return 1;
            } else if (((a) this.N) != null) {
                return 1;
            } else {
                throw null;
            }
        } else if (((a) this.N) != null) {
            return 1;
        } else {
            throw null;
        }
    }

    public int b(int i2, RecyclerView.v vVar, RecyclerView.a0 a0Var) {
        A();
        z();
        if (super.f280s == 0) {
            return 0;
        }
        return c(i2, vVar, a0Var);
    }

    public GridLayoutManager(Context context, int i2, int i3, boolean z) {
        super(i3, z);
        l(i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int */
    public final void b(View view, int i2, boolean z) {
        int i3;
        int i4;
        b bVar = (b) view.getLayoutParams();
        Rect rect = bVar.b;
        int i5 = rect.top + rect.bottom + bVar.topMargin + bVar.bottomMargin;
        int i6 = rect.left + rect.right + bVar.leftMargin + bVar.rightMargin;
        int f2 = f(bVar.f278e, bVar.f279f);
        if (super.f280s == 1) {
            i3 = RecyclerView.o.a(f2, i2, i6, bVar.width, false);
            i4 = RecyclerView.o.a(super.u.g(), this.f325p, i5, bVar.height, true);
        } else {
            int a2 = RecyclerView.o.a(f2, i2, i5, bVar.height, false);
            int a3 = RecyclerView.o.a(super.u.g(), this.f324o, i6, bVar.width, true);
            i4 = a2;
            i3 = a3;
        }
        a(view, i3, i4, z);
    }

    public void c(RecyclerView.v vVar, RecyclerView.a0 a0Var) {
        if (a0Var.g) {
            int d = d();
            for (int i2 = 0; i2 < d; i2++) {
                b bVar = (b) c(i2).getLayoutParams();
                int a2 = bVar.a();
                this.L.put(a2, bVar.f279f);
                this.M.put(a2, bVar.f278e);
            }
        }
        super.c(vVar, a0Var);
        this.L.clear();
        this.M.clear();
    }

    public void a(RecyclerView recyclerView, int i2, int i3) {
        this.N.a.clear();
        this.N.b.clear();
    }

    public void a(RecyclerView recyclerView) {
        this.N.a.clear();
        this.N.b.clear();
    }

    public void a(RecyclerView recyclerView, int i2, int i3, Object obj) {
        this.N.a.clear();
        this.N.b.clear();
    }

    public int b(RecyclerView.a0 a0Var) {
        return i(a0Var);
    }

    public void a(RecyclerView recyclerView, int i2, int i3, int i4) {
        this.N.a.clear();
        this.N.b.clear();
    }

    public final int b(RecyclerView.v vVar, RecyclerView.a0 a0Var, int i2) {
        if (!a0Var.g) {
            return this.N.b(i2, this.I);
        }
        int i3 = this.M.get(i2, -1);
        if (i3 != -1) {
            return i3;
        }
        int a2 = vVar.a(i2);
        if (a2 != -1) {
            return this.N.b(a2, this.I);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. It is not cached, not in the adapter. Pos:" + i2);
        return 0;
    }

    public RecyclerView.p a(Context context, AttributeSet attributeSet) {
        return new b(context, attributeSet);
    }

    public RecyclerView.p a(ViewGroup.LayoutParams layoutParams) {
        if (layoutParams instanceof ViewGroup.MarginLayoutParams) {
            return new b((ViewGroup.MarginLayoutParams) layoutParams);
        }
        return new b(layoutParams);
    }

    public boolean a(RecyclerView.p pVar) {
        return pVar instanceof b;
    }

    public void a(Rect rect, int i2, int i3) {
        int i4;
        int i5;
        if (this.J == null) {
            super.a(rect, i2, i3);
        }
        int k2 = k() + j();
        int i6 = i() + l();
        if (super.f280s == 1) {
            i5 = RecyclerView.o.a(i3, rect.height() + i6, g());
            int[] iArr = this.J;
            i4 = RecyclerView.o.a(i2, iArr[iArr.length - 1] + k2, h());
        } else {
            i4 = RecyclerView.o.a(i2, rect.width() + k2, h());
            int[] iArr2 = this.J;
            i5 = RecyclerView.o.a(i3, iArr2[iArr2.length - 1] + i6, g());
        }
        this.b.setMeasuredDimension(i4, i5);
    }

    public void a(boolean z) {
        if (!z) {
            a((String) null);
            if (super.y) {
                super.y = false;
                o();
                return;
            }
            return;
        }
        throw new UnsupportedOperationException("GridLayoutManager does not support stack from end. Consider using reverse layout");
    }

    public void a(RecyclerView.v vVar, RecyclerView.a0 a0Var, LinearLayoutManager.a aVar, int i2) {
        A();
        if (a0Var.a() > 0 && !a0Var.g) {
            boolean z = i2 == 1;
            int b2 = b(vVar, a0Var, aVar.b);
            if (z) {
                while (b2 > 0) {
                    int i3 = aVar.b;
                    if (i3 <= 0) {
                        break;
                    }
                    int i4 = i3 - 1;
                    aVar.b = i4;
                    b2 = b(vVar, a0Var, i4);
                }
            } else {
                int a2 = a0Var.a() - 1;
                int i5 = aVar.b;
                while (i5 < a2) {
                    int i6 = i5 + 1;
                    int b3 = b(vVar, a0Var, i6);
                    if (b3 <= b2) {
                        break;
                    }
                    i5 = i6;
                    b2 = b3;
                }
                aVar.b = i5;
            }
        }
        z();
    }

    public int a(int i2, RecyclerView.v vVar, RecyclerView.a0 a0Var) {
        A();
        z();
        if (super.f280s == 1) {
            return 0;
        }
        return c(i2, vVar, a0Var);
    }

    public View a(RecyclerView.v vVar, RecyclerView.a0 a0Var, int i2, int i3, int i4) {
        r();
        int f2 = super.u.f();
        int b2 = super.u.b();
        int i5 = i3 > i2 ? 1 : -1;
        View view = null;
        View view2 = null;
        while (i2 != i3) {
            View c2 = c(i2);
            int i6 = i(c2);
            if (i6 >= 0 && i6 < i4 && b(vVar, a0Var, i6) == 0) {
                if (((RecyclerView.p) c2.getLayoutParams()).c()) {
                    if (view2 == null) {
                        view2 = c2;
                    }
                } else if (super.u.d(c2) < b2 && super.u.a(c2) >= f2) {
                    return c2;
                } else {
                    if (view == null) {
                        view = c2;
                    }
                }
            }
            i2 += i5;
        }
        return view != null ? view : view2;
    }

    public void a(RecyclerView.a0 a0Var, LinearLayoutManager.c cVar, RecyclerView.o.c cVar2) {
        int i2 = this.I;
        int i3 = 0;
        while (i3 < this.I && cVar.a(a0Var) && i2 > 0) {
            ((GapWorker.b) cVar2).a(cVar.d, Math.max(0, cVar.g));
            if (((a) this.N) != null) {
                i2--;
                cVar.d += cVar.f283e;
                i3++;
            } else {
                throw null;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, int):int
      androidx.recyclerview.widget.GridLayoutManager.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int):int
      androidx.recyclerview.widget.RecyclerView.o.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.a(android.graphics.Rect, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, boolean, android.graphics.Rect):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, androidx.recyclerview.widget.RecyclerView$a0, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.b(android.view.View, int, boolean):void
     arg types: [android.view.View, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.GridLayoutManager.b(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, int):int
      androidx.recyclerview.widget.GridLayoutManager.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.LinearLayoutManager.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.b(int, int, int):boolean
      androidx.recyclerview.widget.RecyclerView.o.b(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):int
      androidx.recyclerview.widget.RecyclerView.o.b(androidx.recyclerview.widget.RecyclerView, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.b(android.view.View, int, boolean):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int
     arg types: [int, int, int, int, int]
     candidates:
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, android.view.View, android.graphics.Rect, boolean, boolean):boolean
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, int, int, boolean):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void
     arg types: [android.view.View, int, int, int]
     candidates:
      androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):android.view.View
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, android.view.View, i.h.l.x.AccessibilityNodeInfoCompat):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.GridLayoutManager.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.RecyclerView$a0, boolean):int
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, boolean):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):android.view.View
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.LinearLayoutManager.a(int, int, boolean, androidx.recyclerview.widget.RecyclerView$a0):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.LinearLayoutManager$a, int):void
      androidx.recyclerview.widget.LinearLayoutManager.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.LinearLayoutManager$c, androidx.recyclerview.widget.LinearLayoutManager$b):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.content.Context, android.util.AttributeSet, int, int):androidx.recyclerview.widget.RecyclerView$o$d
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):android.view.View
      androidx.recyclerview.widget.RecyclerView.o.a(int, int, androidx.recyclerview.widget.RecyclerView$a0, androidx.recyclerview.widget.RecyclerView$o$c):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0, android.view.View, i.h.l.x.AccessibilityNodeInfoCompat):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, int):void
      androidx.recyclerview.widget.RecyclerView.o.a(androidx.recyclerview.widget.RecyclerView, int, int, java.lang.Object):void
      androidx.recyclerview.widget.RecyclerView.o.a(android.view.View, int, int, androidx.recyclerview.widget.RecyclerView$p):boolean
      androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, int, boolean):void */
    public void a(RecyclerView.v vVar, RecyclerView.a0 a0Var, LinearLayoutManager.c cVar, LinearLayoutManager.b bVar) {
        int i2;
        int i3;
        int i4;
        int i5;
        int i6;
        int i7;
        int i8;
        int i9;
        int i10;
        int i11;
        int i12;
        int i13;
        int i14;
        int i15;
        int i16;
        int i17;
        int i18;
        View a2;
        RecyclerView.v vVar2 = vVar;
        RecyclerView.a0 a0Var2 = a0Var;
        LinearLayoutManager.c cVar2 = cVar;
        LinearLayoutManager.b bVar2 = bVar;
        int e2 = super.u.e();
        boolean z = e2 != 1073741824;
        int i19 = d() > 0 ? this.J[this.I] : 0;
        if (z) {
            A();
        }
        boolean z2 = cVar2.f283e == 1;
        int i20 = this.I;
        if (!z2) {
            i20 = b(vVar2, a0Var2, cVar2.d) + c(vVar2, a0Var2, cVar2.d);
        }
        int i21 = 0;
        while (i21 < this.I && cVar2.a(a0Var2) && i20 > 0) {
            int i22 = cVar2.d;
            int c2 = c(vVar2, a0Var2, i22);
            if (c2 <= this.I) {
                i20 -= c2;
                if (i20 < 0 || (a2 = cVar2.a(vVar2)) == null) {
                    break;
                }
                this.K[i21] = a2;
                i21++;
            } else {
                throw new IllegalArgumentException("Item at position " + i22 + " requires " + c2 + " spans but GridLayoutManager has only " + this.I + " spans.");
            }
        }
        if (i21 == 0) {
            bVar2.b = true;
            return;
        }
        if (z2) {
            i4 = i21;
            i5 = 0;
            i3 = 0;
            i2 = 1;
        } else {
            i5 = i21 - 1;
            i4 = -1;
            i3 = 0;
            i2 = -1;
        }
        while (i5 != i4) {
            View view = this.K[i5];
            b bVar3 = (b) view.getLayoutParams();
            int c3 = c(vVar2, a0Var2, i(view));
            bVar3.f279f = c3;
            bVar3.f278e = i3;
            i3 += c3;
            i5 += i2;
        }
        float f2 = 0.0f;
        int i23 = 0;
        for (int i24 = 0; i24 < i21; i24++) {
            View view2 = this.K[i24];
            if (cVar2.f288l == null) {
                if (z2) {
                    a(view2);
                } else {
                    a(view2, 0, false);
                }
            } else if (z2) {
                a(view2, -1, true);
            } else {
                a(view2, 0, true);
            }
            Rect rect = this.O;
            RecyclerView recyclerView = this.b;
            if (recyclerView == null) {
                rect.set(0, 0, 0, 0);
            } else {
                rect.set(recyclerView.getItemDecorInsetsForChild(view2));
            }
            b(view2, e2, false);
            int b2 = super.u.b(view2);
            if (b2 > i23) {
                i23 = b2;
            }
            float c4 = (((float) super.u.c(view2)) * 1.0f) / ((float) ((b) view2.getLayoutParams()).f279f);
            if (c4 > f2) {
                f2 = c4;
            }
        }
        if (z) {
            k(Math.max(Math.round(f2 * ((float) this.I)), i19));
            i23 = 0;
            for (int i25 = 0; i25 < i21; i25++) {
                View view3 = this.K[i25];
                b(view3, 1073741824, true);
                int b3 = super.u.b(view3);
                if (b3 > i23) {
                    i23 = b3;
                }
            }
        }
        for (int i26 = 0; i26 < i21; i26++) {
            View view4 = this.K[i26];
            if (super.u.b(view4) != i23) {
                b bVar4 = (b) view4.getLayoutParams();
                Rect rect2 = bVar4.b;
                int i27 = rect2.top + rect2.bottom + bVar4.topMargin + bVar4.bottomMargin;
                int i28 = rect2.left + rect2.right + bVar4.leftMargin + bVar4.rightMargin;
                int f3 = f(bVar4.f278e, bVar4.f279f);
                if (super.f280s == 1) {
                    i18 = RecyclerView.o.a(f3, 1073741824, i28, bVar4.width, false);
                    i17 = View.MeasureSpec.makeMeasureSpec(i23 - i27, 1073741824);
                } else {
                    int makeMeasureSpec = View.MeasureSpec.makeMeasureSpec(i23 - i28, 1073741824);
                    i17 = RecyclerView.o.a(f3, 1073741824, i27, bVar4.height, false);
                    i18 = makeMeasureSpec;
                }
                a(view4, i18, i17, true);
            }
        }
        bVar2.a = i23;
        if (super.f280s == 1) {
            if (cVar2.f284f == -1) {
                i9 = cVar2.b;
                i16 = i9 - i23;
            } else {
                i16 = cVar2.b;
                i9 = i23 + i16;
            }
            i7 = i16;
            i8 = 0;
            i6 = 0;
        } else {
            if (cVar2.f284f == -1) {
                i15 = cVar2.b;
                i14 = i15 - i23;
            } else {
                i14 = cVar2.b;
                i15 = i23 + i14;
            }
            i6 = i14;
            i7 = 0;
            i8 = i15;
            i9 = 0;
        }
        int i29 = 0;
        while (i29 < i21) {
            View view5 = this.K[i29];
            b bVar5 = (b) view5.getLayoutParams();
            if (super.f280s != 1) {
                i7 = this.J[bVar5.f278e] + l();
                i9 = super.u.c(view5) + i7;
            } else if (w()) {
                i8 = j() + this.J[this.I - bVar5.f278e];
                i6 = i8 - super.u.c(view5);
            } else {
                int j2 = j() + this.J[bVar5.f278e];
                i13 = i9;
                i10 = j2;
                i11 = i7;
                i12 = super.u.c(view5) + j2;
                a(view5, i10, i11, i12, i13);
                if (!bVar5.c() || bVar5.b()) {
                    bVar2.c = true;
                }
                bVar2.d |= view5.hasFocusable();
                i29++;
                i9 = i13;
                i8 = i12;
                i7 = i11;
                i6 = i10;
            }
            i13 = i9;
            i12 = i8;
            i11 = i7;
            i10 = i6;
            a(view5, i10, i11, i12, i13);
            if (!bVar5.c()) {
            }
            bVar2.c = true;
            bVar2.d |= view5.hasFocusable();
            i29++;
            i9 = i13;
            i8 = i12;
            i7 = i11;
            i6 = i10;
        }
        Arrays.fill(this.K, (Object) null);
    }

    public final void a(View view, int i2, int i3, boolean z) {
        boolean z2;
        RecyclerView.p pVar = (RecyclerView.p) view.getLayoutParams();
        if (z) {
            z2 = b(view, i2, i3, pVar);
        } else {
            z2 = a(view, i2, i3, pVar);
        }
        if (z2) {
            view.measure(i2, i3);
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:53:0x00d2, code lost:
        if (r13 == (r2 > r15)) goto L_0x00af;
     */
    /* JADX WARNING: Code restructure failed: missing block: B:69:0x00ff, code lost:
        if (r13 == (r2 > r8)) goto L_0x00f6;
     */
    /* JADX WARNING: Removed duplicated region for block: B:73:0x010a  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public android.view.View a(android.view.View r23, int r24, androidx.recyclerview.widget.RecyclerView.v r25, androidx.recyclerview.widget.RecyclerView.a0 r26) {
        /*
            r22 = this;
            r0 = r22
            r1 = r25
            r2 = r26
            android.view.View r3 = r22.b(r23)
            r4 = 0
            if (r3 != 0) goto L_0x000e
            return r4
        L_0x000e:
            android.view.ViewGroup$LayoutParams r5 = r3.getLayoutParams()
            androidx.recyclerview.widget.GridLayoutManager$b r5 = (androidx.recyclerview.widget.GridLayoutManager.b) r5
            int r6 = r5.f278e
            int r5 = r5.f279f
            int r5 = r5 + r6
            android.view.View r7 = super.a(r23, r24, r25, r26)
            if (r7 != 0) goto L_0x0020
            return r4
        L_0x0020:
            r7 = r24
            int r7 = r0.i(r7)
            r9 = 1
            if (r7 != r9) goto L_0x002b
            r7 = 1
            goto L_0x002c
        L_0x002b:
            r7 = 0
        L_0x002c:
            boolean r10 = r0.x
            if (r7 == r10) goto L_0x0032
            r7 = 1
            goto L_0x0033
        L_0x0032:
            r7 = 0
        L_0x0033:
            r10 = -1
            if (r7 == 0) goto L_0x003e
            int r7 = r22.d()
            int r7 = r7 - r9
            r11 = -1
            r12 = -1
            goto L_0x0045
        L_0x003e:
            int r7 = r22.d()
            r11 = r7
            r7 = 0
            r12 = 1
        L_0x0045:
            int r13 = r0.f280s
            if (r13 != r9) goto L_0x0051
            boolean r13 = r22.w()
            if (r13 == 0) goto L_0x0051
            r13 = 1
            goto L_0x0052
        L_0x0051:
            r13 = 0
        L_0x0052:
            int r14 = r0.a(r1, r2, r7)
            r10 = r7
            r16 = r12
            r8 = -1
            r9 = 0
            r12 = 0
            r15 = -1
            r7 = r4
        L_0x005e:
            if (r10 == r11) goto L_0x0144
            r17 = r11
            int r11 = r0.a(r1, r2, r10)
            android.view.View r1 = r0.c(r10)
            if (r1 != r3) goto L_0x006e
            goto L_0x0144
        L_0x006e:
            boolean r18 = r1.hasFocusable()
            if (r18 == 0) goto L_0x0085
            if (r11 == r14) goto L_0x0085
            if (r4 == 0) goto L_0x007a
            goto L_0x0144
        L_0x007a:
            r18 = r3
            r21 = r7
            r19 = r9
            r20 = r14
            r14 = 1
            goto L_0x0132
        L_0x0085:
            android.view.ViewGroup$LayoutParams r11 = r1.getLayoutParams()
            androidx.recyclerview.widget.GridLayoutManager$b r11 = (androidx.recyclerview.widget.GridLayoutManager.b) r11
            int r2 = r11.f278e
            r18 = r3
            int r3 = r11.f279f
            int r3 = r3 + r2
            boolean r19 = r1.hasFocusable()
            if (r19 == 0) goto L_0x009d
            if (r2 != r6) goto L_0x009d
            if (r3 != r5) goto L_0x009d
            return r1
        L_0x009d:
            boolean r19 = r1.hasFocusable()
            if (r19 == 0) goto L_0x00a5
            if (r4 == 0) goto L_0x00ad
        L_0x00a5:
            boolean r19 = r1.hasFocusable()
            if (r19 != 0) goto L_0x00b6
            if (r7 != 0) goto L_0x00b6
        L_0x00ad:
            r21 = r7
        L_0x00af:
            r19 = r9
            r20 = r14
            r7 = 1
            r14 = 1
            goto L_0x0108
        L_0x00b6:
            int r19 = java.lang.Math.max(r2, r6)
            int r20 = java.lang.Math.min(r3, r5)
            r21 = r7
            int r7 = r20 - r19
            boolean r19 = r1.hasFocusable()
            if (r19 == 0) goto L_0x00d5
            if (r7 <= r9) goto L_0x00cb
        L_0x00ca:
            goto L_0x00af
        L_0x00cb:
            if (r7 != r9) goto L_0x0102
            if (r2 <= r15) goto L_0x00d1
            r7 = 1
            goto L_0x00d2
        L_0x00d1:
            r7 = 0
        L_0x00d2:
            if (r13 != r7) goto L_0x0102
            goto L_0x00ca
        L_0x00d5:
            if (r4 != 0) goto L_0x0102
            r19 = r9
            i.r.d.ViewBoundsCheck r9 = r0.f316e
            r20 = r14
            r14 = 24579(0x6003, float:3.4443E-41)
            boolean r9 = r9.a(r1, r14)
            if (r9 == 0) goto L_0x00ef
            i.r.d.ViewBoundsCheck r9 = r0.f317f
            boolean r9 = r9.a(r1, r14)
            if (r9 == 0) goto L_0x00ef
            r9 = 1
            goto L_0x00f0
        L_0x00ef:
            r9 = 0
        L_0x00f0:
            r14 = 1
            r9 = r9 ^ r14
            if (r9 == 0) goto L_0x0107
            if (r7 <= r12) goto L_0x00f8
        L_0x00f6:
            r7 = 1
            goto L_0x0108
        L_0x00f8:
            if (r7 != r12) goto L_0x0107
            if (r2 <= r8) goto L_0x00fe
            r7 = 1
            goto L_0x00ff
        L_0x00fe:
            r7 = 0
        L_0x00ff:
            if (r13 != r7) goto L_0x0107
            goto L_0x00f6
        L_0x0102:
            r19 = r9
            r20 = r14
            r14 = 1
        L_0x0107:
            r7 = 0
        L_0x0108:
            if (r7 == 0) goto L_0x0132
            boolean r7 = r1.hasFocusable()
            if (r7 == 0) goto L_0x0121
            int r4 = r11.f278e
            int r3 = java.lang.Math.min(r3, r5)
            int r2 = java.lang.Math.max(r2, r6)
            int r9 = r3 - r2
            r15 = r4
            r7 = r21
            r4 = r1
            goto L_0x0136
        L_0x0121:
            int r7 = r11.f278e
            int r3 = java.lang.Math.min(r3, r5)
            int r2 = java.lang.Math.max(r2, r6)
            int r12 = r3 - r2
            r8 = r7
            r9 = r19
            r7 = r1
            goto L_0x0136
        L_0x0132:
            r9 = r19
            r7 = r21
        L_0x0136:
            int r10 = r10 + r16
            r1 = r25
            r2 = r26
            r11 = r17
            r3 = r18
            r14 = r20
            goto L_0x005e
        L_0x0144:
            r21 = r7
            if (r4 == 0) goto L_0x0149
            goto L_0x014b
        L_0x0149:
            r4 = r21
        L_0x014b:
            return r4
        */
        throw new UnsupportedOperationException("Method not decompiled: androidx.recyclerview.widget.GridLayoutManager.a(android.view.View, int, androidx.recyclerview.widget.RecyclerView$v, androidx.recyclerview.widget.RecyclerView$a0):android.view.View");
    }

    public final int a(RecyclerView.v vVar, RecyclerView.a0 a0Var, int i2) {
        if (!a0Var.g) {
            return this.N.a(i2, this.I);
        }
        int a2 = vVar.a(i2);
        if (a2 != -1) {
            return this.N.a(a2, this.I);
        }
        Log.w("GridLayoutManager", "Cannot find span size for pre layout position. " + i2);
        return 0;
    }
}
