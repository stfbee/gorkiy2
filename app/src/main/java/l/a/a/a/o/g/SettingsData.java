package l.a.a.a.o.g;

public class SettingsData {
    public final AppSettingsData a;
    public final SessionSettingsData b;
    public final PromptSettingsData c;
    public final FeaturesSettingsData d;

    /* renamed from: e  reason: collision with root package name */
    public final AnalyticsSettingsData f2670e;

    /* renamed from: f  reason: collision with root package name */
    public final long f2671f;

    public SettingsData(long j2, AppSettingsData appSettingsData, SessionSettingsData sessionSettingsData, PromptSettingsData promptSettingsData, FeaturesSettingsData featuresSettingsData, AnalyticsSettingsData analyticsSettingsData, BetaSettingsData betaSettingsData, int i2, int i3) {
        this.f2671f = j2;
        this.a = appSettingsData;
        this.b = sessionSettingsData;
        this.c = promptSettingsData;
        this.d = featuresSettingsData;
        this.f2670e = analyticsSettingsData;
    }
}
