package l.a.a.a.o.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;
import j.a.a.a.outline;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.k;
import l.a.a.a.o.b.s;

public class IdManager {

    /* renamed from: m  reason: collision with root package name */
    public static final Pattern f2632m = Pattern.compile("[^\\p{Alnum}]");

    /* renamed from: n  reason: collision with root package name */
    public static final String f2633n = Pattern.quote("/");
    public final ReentrantLock a = new ReentrantLock();
    public final InstallerPackageNameProvider b;
    public final boolean c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final Context f2634e;

    /* renamed from: f  reason: collision with root package name */
    public final String f2635f;
    public final String g;
    public final Collection<k> h;

    /* renamed from: i  reason: collision with root package name */
    public AdvertisingInfoProvider f2636i;

    /* renamed from: j  reason: collision with root package name */
    public AdvertisingInfo f2637j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f2638k;

    /* renamed from: l  reason: collision with root package name */
    public FirebaseInfo f2639l;

    public enum a {
        WIFI_MAC_ADDRESS(1),
        BLUETOOTH_MAC_ADDRESS(2),
        FONT_TOKEN(53),
        ANDROID_ID(100),
        ANDROID_DEVICE_ID(101),
        ANDROID_SERIAL(102),
        ANDROID_ADVERTISING_ID(103);
        
        public final int protobufIndex;

        /* access modifiers changed from: public */
        a(int i2) {
            this.protobufIndex = i2;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.a.a.a.o.b.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean
     arg types: [android.content.Context, java.lang.String, int]
     candidates:
      l.a.a.a.o.b.CommonUtils.a(android.content.Context, java.lang.String, java.lang.String):int
      l.a.a.a.o.b.CommonUtils.a(java.lang.String, java.lang.String, int):long
      l.a.a.a.o.b.CommonUtils.a(android.content.Context, int, java.lang.String):void
      l.a.a.a.o.b.CommonUtils.a(java.io.InputStream, java.io.OutputStream, byte[]):void
      l.a.a.a.o.b.CommonUtils.a(android.content.Context, java.lang.String, boolean):boolean */
    public IdManager(Context context, String str, String str2, Collection<k> collection) {
        if (context == null) {
            throw new IllegalArgumentException("appContext must not be null");
        } else if (str == null) {
            throw new IllegalArgumentException("appIdentifier must not be null");
        } else if (collection != null) {
            this.f2634e = context;
            this.f2635f = str;
            this.g = str2;
            this.h = collection;
            this.b = new InstallerPackageNameProvider();
            this.f2636i = new AdvertisingInfoProvider(context);
            this.f2639l = new FirebaseInfo();
            boolean a2 = CommonUtils.a(context, "com.crashlytics.CollectDeviceIdentifiers", true);
            this.c = a2;
            if (!a2) {
                DefaultLogger a3 = Fabric.a();
                StringBuilder a4 = outline.a("Device ID collection disabled for ");
                a4.append(context.getPackageName());
                String sb = a4.toString();
                if (a3.a("Fabric", 3)) {
                    Log.d("Fabric", sb, null);
                }
            }
            boolean a5 = CommonUtils.a(context, "com.crashlytics.CollectUserIdentifiers", true);
            this.d = a5;
            if (!a5) {
                DefaultLogger a6 = Fabric.a();
                StringBuilder a7 = outline.a("User information collection disabled for ");
                a7.append(context.getPackageName());
                String sb2 = a7.toString();
                if (a6.a("Fabric", 3)) {
                    Log.d("Fabric", sb2, null);
                }
            }
        } else {
            throw new IllegalArgumentException("kits must not be null");
        }
    }

    public final String a(String str) {
        return str.replaceAll(f2633n, "");
    }

    public String b() {
        String str;
        String str2 = this.g;
        if (str2 != null) {
            return str2;
        }
        SharedPreferences e2 = CommonUtils.e(this.f2634e);
        AdvertisingInfo a2 = a();
        String str3 = null;
        if (a2 != null) {
            String str4 = a2.a;
            this.a.lock();
            try {
                if (!TextUtils.isEmpty(str4)) {
                    String string = e2.getString("crashlytics.advertising.id", null);
                    if (TextUtils.isEmpty(string)) {
                        e2.edit().putString("crashlytics.advertising.id", str4).commit();
                    } else if (!string.equals(str4)) {
                        e2.edit().remove("crashlytics.installation.id").putString("crashlytics.advertising.id", str4).commit();
                    }
                }
            } finally {
                this.a.unlock();
            }
        }
        String string2 = e2.getString("crashlytics.installation.id", null);
        if (string2 != null) {
            return string2;
        }
        this.a.lock();
        try {
            String string3 = e2.getString("crashlytics.installation.id", null);
            if (string3 == null) {
                String uuid = UUID.randomUUID().toString();
                if (uuid != null) {
                    str3 = f2632m.matcher(uuid).replaceAll("").toLowerCase(Locale.US);
                }
                e2.edit().putString("crashlytics.installation.id", str3).commit();
                str = str3;
            } else {
                str = string3;
            }
            return str;
        } finally {
            this.a.unlock();
        }
    }

    public Map<s.a, String> c() {
        HashMap hashMap = new HashMap();
        for (Kit next : this.h) {
            if (next instanceof DeviceIdentifierProvider) {
                for (Map.Entry next2 : ((DeviceIdentifierProvider) next).getDeviceIdentifiers().entrySet()) {
                    a aVar = (a) next2.getKey();
                    String str = (String) next2.getValue();
                    if (str != null) {
                        hashMap.put(aVar, str);
                    }
                }
            }
        }
        return Collections.unmodifiableMap(hashMap);
    }

    public String d() {
        InstallerPackageNameProvider installerPackageNameProvider = this.b;
        Context context = this.f2634e;
        if (installerPackageNameProvider != null) {
            try {
                String a2 = installerPackageNameProvider.b.a(context, installerPackageNameProvider.a);
                if ("".equals(a2)) {
                    return null;
                }
                return a2;
            } catch (Exception e2) {
                if (!Fabric.a().a("Fabric", 6)) {
                    return null;
                }
                Log.e("Fabric", "Failed to determine installer package name", e2);
                return null;
            }
        } else {
            throw null;
        }
    }

    public String e() {
        return String.format(Locale.US, "%s/%s", a(Build.MANUFACTURER), a(Build.MODEL));
    }

    public synchronized AdvertisingInfo a() {
        if (!this.f2638k) {
            this.f2637j = this.f2636i.a();
            this.f2638k = true;
        }
        return this.f2637j;
    }
}
