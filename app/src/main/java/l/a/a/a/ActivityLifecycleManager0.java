package l.a.a.a;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.os.Bundle;
import java.util.HashSet;
import java.util.Set;

/* compiled from: ActivityLifecycleManager */
public class ActivityLifecycleManager0 {
    public final Application a;
    public a b;

    /* compiled from: ActivityLifecycleManager */
    public static class a {
        public final Set<Application.ActivityLifecycleCallbacks> a = new HashSet();
        public final Application b;

        public a(Application application) {
            this.b = application;
        }
    }

    /* compiled from: ActivityLifecycleManager */
    public static abstract class b {
        public abstract void onActivityCreated(Activity activity, Bundle bundle);

        public void onActivityDestroyed(Activity activity) {
        }

        public void onActivityPaused(Activity activity) {
        }

        public abstract void onActivityResumed(Activity activity);

        public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {
        }

        public abstract void onActivityStarted(Activity activity);

        public void onActivityStopped(Activity activity) {
        }
    }

    public ActivityLifecycleManager0(Context context) {
        Application application = (Application) context.getApplicationContext();
        this.a = application;
        this.b = new a(application);
    }

    public boolean a(b bVar) {
        boolean z;
        a aVar = this.b;
        if (aVar != null) {
            if (aVar.b != null) {
                ActivityLifecycleManager activityLifecycleManager = new ActivityLifecycleManager(aVar, bVar);
                aVar.b.registerActivityLifecycleCallbacks(activityLifecycleManager);
                aVar.a.add(activityLifecycleManager);
                z = true;
            } else {
                z = false;
            }
            if (z) {
                return true;
            }
        }
        return false;
    }
}
