package l.a.a.a.o.d;

public interface EventTransform<T> {
    byte[] toBytes(Object obj);
}
