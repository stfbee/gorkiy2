package l.a.a.a.o.b;

import android.util.Log;
import java.util.Locale;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.TimeUnit;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;

/* compiled from: ExecutorUtils */
public final class ExecutorUtils0 extends BackgroundPriorityRunnable {
    public final /* synthetic */ String b;
    public final /* synthetic */ ExecutorService c;
    public final /* synthetic */ long d;

    /* renamed from: e  reason: collision with root package name */
    public final /* synthetic */ TimeUnit f2631e;

    public ExecutorUtils0(String str, ExecutorService executorService, long j2, TimeUnit timeUnit) {
        this.b = str;
        this.c = executorService;
        this.d = j2;
        this.f2631e = timeUnit;
    }

    public void onRun() {
        try {
            String str = "Executing shutdown hook for " + this.b;
            if (Fabric.a().a("Fabric", 3)) {
                Log.d("Fabric", str, null);
            }
            this.c.shutdown();
            if (!this.c.awaitTermination(this.d, this.f2631e)) {
                String str2 = this.b + " did not shut down in the allocated time. Requesting immediate shutdown.";
                if (Fabric.a().a("Fabric", 3)) {
                    Log.d("Fabric", str2, null);
                }
                this.c.shutdownNow();
            }
        } catch (InterruptedException unused) {
            DefaultLogger a = Fabric.a();
            String format = String.format(Locale.US, "Interrupted while waiting for %s to shut down. Requesting immediate shutdown.", this.b);
            if (a.a("Fabric", 3)) {
                Log.d("Fabric", format, null);
            }
            this.c.shutdownNow();
        }
    }
}
