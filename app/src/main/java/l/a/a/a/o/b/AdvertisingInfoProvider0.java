package l.a.a.a.o.b;

import android.util.Log;
import l.a.a.a.Fabric;

/* compiled from: AdvertisingInfoProvider */
public class AdvertisingInfoProvider0 extends BackgroundPriorityRunnable {
    public final /* synthetic */ AdvertisingInfo b;
    public final /* synthetic */ AdvertisingInfoProvider c;

    public AdvertisingInfoProvider0(AdvertisingInfoProvider advertisingInfoProvider, AdvertisingInfo advertisingInfo) {
        this.c = advertisingInfoProvider;
        this.b = advertisingInfo;
    }

    public void onRun() {
        AdvertisingInfo b2 = this.c.b();
        if (!this.b.equals(b2)) {
            if (Fabric.a().a("Fabric", 3)) {
                Log.d("Fabric", "Asychronously getting Advertising Info and storing it to preferences", null);
            }
            this.c.b(b2);
        }
    }
}
