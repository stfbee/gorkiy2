package l.a.a.a.o.c;

public interface Task {
    boolean isFinished();

    void setError(Throwable th);

    void setFinished(boolean z);
}
