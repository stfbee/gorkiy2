package l.a.a.a.o.d;

public interface FileRollOverManager {
    void cancelTimeBasedFileRollOver();

    boolean rollFileOver();
}
