package l.a.a.a.o.c.m;

public class RetryState {
    public final int a;
    public final Backoff b;
    public final DefaultRetryPolicy c;

    public RetryState(int i2, Backoff backoff, DefaultRetryPolicy defaultRetryPolicy) {
        this.a = i2;
        this.b = backoff;
        this.c = defaultRetryPolicy;
    }

    public RetryState(Backoff backoff, DefaultRetryPolicy defaultRetryPolicy) {
        this.a = 0;
        this.b = backoff;
        this.c = defaultRetryPolicy;
    }
}
