package l.a.a.a.o.f;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceStoreImpl implements PreferenceStore {
    public final SharedPreferences a;
    public final String b;
    public final Context c;

    public PreferenceStoreImpl(Context context, String str) {
        if (context != null) {
            this.c = context;
            this.b = str;
            this.a = context.getSharedPreferences(str, 0);
            return;
        }
        throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
    }

    public SharedPreferences.Editor a() {
        return this.a.edit();
    }
}
