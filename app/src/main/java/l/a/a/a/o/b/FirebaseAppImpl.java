package l.a.a.a.o.b;

import java.lang.reflect.Method;

public final class FirebaseAppImpl {
    public final Method a;
    public final Object b;

    public FirebaseAppImpl(Class cls, Object obj) {
        this.b = obj;
        this.a = cls.getDeclaredMethod("isDataCollectionDefaultEnabled", new Class[0]);
    }
}
