package l.b;

import j.c.a.a.c.n.c;
import java.util.concurrent.TimeUnit;
import l.b.s.b;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.t.Predicate;
import l.b.t.a;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.c.ScalarCallable;
import l.b.u.d.LambdaObserver;
import l.b.u.e.c.ObservableConcatMap;
import l.b.u.e.c.ObservableElementAtSingle;
import l.b.u.e.c.ObservableEmpty;
import l.b.u.e.c.ObservableFilter;
import l.b.u.e.c.ObservableFlatMap;
import l.b.u.e.c.ObservableFlatMapSingle;
import l.b.u.e.c.ObservableFromArray;
import l.b.u.e.c.ObservableJust;
import l.b.u.e.c.ObservableMap;
import l.b.u.e.c.ObservableObserveOn;
import l.b.u.e.c.ObservableScalarXMap0;
import l.b.u.e.c.ObservableSubscribeOn;
import l.b.u.e.c.ObservableThrottleFirstTimed;
import l.b.u.h.ErrorMode;
import l.b.w.Schedulers;

public abstract class Observable<T> implements ObservableSource<T> {
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [T, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static <T> Observable<T> d(T t2) {
        ObjectHelper.a((Object) t2, "item is null");
        return new ObservableJust(t2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.util.concurrent.TimeUnit, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Scheduler, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final Observable<T> a(long j2, TimeUnit timeUnit) {
        Scheduler scheduler = Schedulers.a;
        ObjectHelper.a((Object) timeUnit, "unit is null");
        ObjectHelper.a((Object) scheduler, "scheduler is null");
        return new ObservableThrottleFirstTimed(this, j2, timeUnit, scheduler);
    }

    public final Single<T> b() {
        return new ObservableElementAtSingle(this, 0, null);
    }

    public abstract void b(Observer<? super T> observer);

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Function, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final <R> Observable<R> c(Function function) {
        ObjectHelper.a((Object) function, "mapper is null");
        return new ObservableMap(this, function);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Function<? super T, ? extends l.b.SingleSource<? extends R>>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final <R> Observable<R> b(Function<? super T, ? extends SingleSource<? extends R>> function) {
        ObjectHelper.a((Object) function, "mapper is null");
        return new ObservableFlatMapSingle(this, function, false);
    }

    public final Observable<T> c(Object obj) {
        ObjectHelper.a(obj, "item is null");
        return new ObservableConcatMap(a((Object[]) new ObservableSource[]{d(obj), this}), Functions.a, Flowable.a, ErrorMode.BOUNDARY);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.l, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final Observable<T> b(l lVar) {
        ObjectHelper.a((Object) lVar, "scheduler is null");
        return new ObservableSubscribeOn(this, lVar);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R>
     arg types: [l.b.t.Function, int, int, int]
     candidates:
      l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b
      l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R> */
    public final <R> Observable<R> a(Function function) {
        return a(function, false, Integer.MAX_VALUE, Flowable.a);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.l, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final Observable<T> a(l lVar) {
        int i2 = Flowable.a;
        ObjectHelper.a((Object) lVar, "scheduler is null");
        ObjectHelper.a(i2, "bufferSize");
        return new ObservableObserveOn(this, lVar, false, i2);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Object[], java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public static <T> Observable<T> a(Object... objArr) {
        ObjectHelper.a((Object) objArr, "items is null");
        if (objArr.length == 0) {
            return ObservableEmpty.b;
        }
        if (objArr.length == 1) {
            return d(objArr[0]);
        }
        return new ObservableFromArray(objArr);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R>
     arg types: [l.b.t.Function<java.lang.Object, java.lang.Object>, int, int, int]
     candidates:
      l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b
      l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R> */
    public static <T> Observable<T> a(ObservableSource... observableSourceArr) {
        Observable a = a((Object[]) observableSourceArr);
        Function<Object, Object> function = Functions.a;
        int length = observableSourceArr.length;
        if (a != null) {
            return a.a((Function) function, false, length, Flowable.a);
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Predicate, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final Observable<T> a(Predicate predicate) {
        ObjectHelper.a((Object) predicate, "predicate is null");
        return new ObservableFilter(this, predicate);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Function<? super T, ? extends l.b.ObservableSource<? extends R>>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final <R> Observable<R> a(Function<? super T, ? extends ObservableSource<? extends R>> function, boolean z, int i2, int i3) {
        ObjectHelper.a((Object) function, "mapper is null");
        ObjectHelper.a(i2, "maxConcurrency");
        ObjectHelper.a(i3, "bufferSize");
        if (!(this instanceof ScalarCallable)) {
            return new ObservableFlatMap(this, function, z, i2, i3);
        }
        Object call = ((ScalarCallable) this).call();
        if (call == null) {
            return ObservableEmpty.b;
        }
        return new ObservableScalarXMap0(call, function);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final <U> Observable<U> a(Class cls) {
        ObjectHelper.a((Object) cls, "clazz is null");
        Observable a = a(Functions.b(cls));
        ObjectHelper.a((Object) cls, "clazz is null");
        return a.c(Functions.a(cls));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b
     arg types: [l.b.t.Consumer, l.b.t.Consumer<java.lang.Throwable>, l.b.t.Action, l.b.t.Consumer<java.lang.Object>]
     candidates:
      l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R>
      l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b */
    public final b a(Consumer consumer) {
        return a(consumer, (Consumer<? super Throwable>) Functions.f2676e, (a) Functions.c, (Consumer<? super b>) Functions.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b
     arg types: [l.b.t.Consumer<? super T>, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.Action, l.b.t.Consumer<java.lang.Object>]
     candidates:
      l.b.Observable.a(l.b.t.Function, boolean, int, int):l.b.Observable<R>
      l.b.Observable.a(l.b.t.Consumer, l.b.t.Consumer<? super java.lang.Throwable>, l.b.t.a, l.b.t.Consumer<? super l.b.s.b>):l.b.s.b */
    public final b a(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2) {
        return a((Consumer) consumer, consumer2, (a) Functions.c, (Consumer<? super b>) Functions.d);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<? super T>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<? super java.lang.Throwable>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.a, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<? super l.b.s.b>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final b a(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, a aVar, Consumer<? super b> consumer3) {
        ObjectHelper.a((Object) consumer, "onNext is null");
        ObjectHelper.a((Object) consumer2, "onError is null");
        ObjectHelper.a((Object) aVar, "onComplete is null");
        ObjectHelper.a((Object) consumer3, "onSubscribe is null");
        LambdaObserver lambdaObserver = new LambdaObserver(consumer, consumer2, aVar, consumer3);
        a(lambdaObserver);
        return lambdaObserver;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.Observer, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public final void a(Observer observer) {
        ObjectHelper.a((Object) observer, "observer is null");
        try {
            ObjectHelper.a((Object) observer, "The RxJavaPlugins.onSubscribe hook returned a null Observer. Please change the handler provided to RxJavaPlugins.setOnObservableSubscribe for invalid null returns. Further reading: https://github.com/ReactiveX/RxJava/wiki/Plugins");
            b(observer);
        } catch (NullPointerException e2) {
            throw e2;
        } catch (Throwable th) {
            c.c(th);
            c.b(th);
            NullPointerException nullPointerException = new NullPointerException("Actually not, but can't throw other exceptions due to RS");
            nullPointerException.initCause(th);
            throw nullPointerException;
        }
    }
}
