package l.b.u.c;

public interface SimpleQueue<T> {
    void clear();

    boolean isEmpty();

    boolean offer(T t2);

    T poll();
}
