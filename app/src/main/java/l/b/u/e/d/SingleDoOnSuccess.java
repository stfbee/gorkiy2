package l.b.u.e.d;

import j.c.a.a.c.n.c;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.Disposable;
import l.b.t.Consumer;

public final class SingleDoOnSuccess<T> extends Single<T> {
    public final SingleSource<T> a;
    public final Consumer<? super T> b;

    public final class a implements SingleObserver<T> {
        public final SingleObserver<? super T> b;

        public a(SingleObserver<? super T> singleObserver) {
            this.b = singleObserver;
        }

        public void a(Disposable disposable) {
            this.b.a(disposable);
        }

        public void a(T t2) {
            try {
                SingleDoOnSuccess.this.b.a(t2);
                this.b.a((Object) t2);
            } catch (Throwable th) {
                c.c(th);
                this.b.a(th);
            }
        }

        public void a(Throwable th) {
            this.b.a(th);
        }
    }

    public SingleDoOnSuccess(SingleSource<T> singleSource, Consumer<? super T> consumer) {
        this.a = singleSource;
        this.b = consumer;
    }

    public void b(SingleObserver<? super T> singleObserver) {
        this.a.a(new a(singleObserver));
    }
}
