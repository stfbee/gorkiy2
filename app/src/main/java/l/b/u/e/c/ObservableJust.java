package l.b.u.e.c;

import l.b.Observable;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.c.ScalarCallable;

public final class ObservableJust<T> extends Observable<T> implements ScalarCallable<T> {
    public final T b;

    public ObservableJust(T t2) {
        this.b = t2;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.u.e.c.ObservableScalarXMap] */
    public void b(Observer<? super T> observer) {
        ? observableScalarXMap = new ObservableScalarXMap(observer, this.b);
        observer.a((Disposable) observableScalarXMap);
        observableScalarXMap.run();
    }

    public T call() {
        return this.b;
    }
}
