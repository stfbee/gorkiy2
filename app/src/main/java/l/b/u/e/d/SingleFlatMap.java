package l.b.u.e.d;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Single;
import l.b.SingleObserver;
import l.b.SingleSource;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;

public final class SingleFlatMap<T, R> extends Single<R> {
    public final SingleSource<? extends T> a;
    public final Function<? super T, ? extends SingleSource<? extends R>> b;

    public SingleFlatMap(SingleSource<? extends T> singleSource, Function<? super T, ? extends SingleSource<? extends R>> function) {
        this.b = function;
        this.a = singleSource;
    }

    public void b(SingleObserver<? super R> singleObserver) {
        this.a.a(new a(singleObserver, this.b));
    }

    public static final class a<T, R> extends AtomicReference<b> implements SingleObserver<T>, b {
        public final SingleObserver<? super R> b;
        public final Function<? super T, ? extends SingleSource<? extends R>> c;

        /* renamed from: l.b.u.e.d.SingleFlatMap$a$a  reason: collision with other inner class name */
        public static final class C0038a<R> implements SingleObserver<R> {
            public final AtomicReference<b> b;
            public final SingleObserver<? super R> c;

            public C0038a(AtomicReference<b> atomicReference, SingleObserver<? super R> singleObserver) {
                this.b = atomicReference;
                this.c = singleObserver;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
             arg types: [java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.Disposable]
             candidates:
              l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
              l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
            public void a(Disposable disposable) {
                DisposableHelper.a(this.b, (b) disposable);
            }

            public void a(R r2) {
                this.c.a((Object) r2);
            }

            public void a(Throwable th) {
                this.c.a(th);
            }
        }

        public a(SingleObserver<? super R> singleObserver, Function<? super T, ? extends SingleSource<? extends R>> function) {
            this.b = singleObserver;
            this.c = function;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.e.d.SingleFlatMap$a, l.b.s.Disposable, java.util.concurrent.atomic.AtomicReference] */
        public void a(Disposable disposable) {
            if (DisposableHelper.b(super, disposable)) {
                this.b.a((Disposable) this);
            }
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.SingleSource<? extends R>, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void a(T t2) {
            try {
                Object a = this.c.a(t2);
                ObjectHelper.a((Object) a, "The single returned by the mapper is null");
                SingleSource singleSource = (SingleSource) a;
                if (!g()) {
                    singleSource.a(new C0038a(super, this.b));
                }
            } catch (Throwable th) {
                c.c(th);
                this.b.a(th);
            }
        }

        public void a(Throwable th) {
            this.b.a(th);
        }
    }
}
