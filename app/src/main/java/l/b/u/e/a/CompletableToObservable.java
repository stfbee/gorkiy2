package l.b.u.e.a;

import l.b.CompletableSource;
import l.b.Observable;
import l.b.Observer;
import l.b.c;
import l.b.s.Disposable;
import l.b.u.a.DisposableHelper;
import l.b.u.d.BasicQueueDisposable;

public final class CompletableToObservable<T> extends Observable<T> {
    public final CompletableSource b;

    public static final class a extends BasicQueueDisposable<Void> implements c {
        public final Observer<?> b;
        public Disposable c;

        public a(Observer<?> observer) {
            this.b = observer;
        }

        public int a(int i2) {
            return i2 & 2;
        }

        public void a() {
            this.b.a();
        }

        public void clear() {
        }

        public void f() {
            this.c.f();
        }

        public boolean g() {
            return this.c.g();
        }

        public boolean isEmpty() {
            return true;
        }

        public Object poll() {
            return null;
        }

        public void a(Throwable th) {
            this.b.a(th);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.e.a.CompletableToObservable$a] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.c, disposable)) {
                this.c = disposable;
                this.b.a((Disposable) this);
            }
        }
    }

    public CompletableToObservable(CompletableSource completableSource) {
        this.b = completableSource;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.CompletableObserver, l.b.u.e.a.CompletableToObservable$a] */
    public void b(Observer<? super T> observer) {
        this.b.a(new a(observer));
    }
}
