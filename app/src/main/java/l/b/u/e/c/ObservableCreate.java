package l.b.u.e.c;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Observable;
import l.b.ObservableEmitter;
import l.b.ObservableOnSubscribe;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class ObservableCreate<T> extends Observable<T> {
    public final ObservableOnSubscribe<T> b;

    public ObservableCreate(ObservableOnSubscribe<T> observableOnSubscribe) {
        this.b = observableOnSubscribe;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.e.c.ObservableCreate$a, l.b.s.Disposable, l.b.ObservableEmitter] */
    public void b(Observer<? super T> observer) {
        ? aVar = new a(observer);
        observer.a((Disposable) aVar);
        try {
            this.b.a(aVar);
        } catch (Throwable th) {
            c.c(th);
            aVar.a(th);
        }
    }

    public static final class a<T> extends AtomicReference<b> implements ObservableEmitter<T>, b {
        public final Observer<? super T> b;

        public a(Observer<? super T> observer) {
            this.b = observer;
        }

        public void a(Object obj) {
            if (obj == null) {
                a((Throwable) new NullPointerException("onNext called with null. Null values are generally not allowed in 2.x operators and sources."));
            } else if (!g()) {
                this.b.b(obj);
            }
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public String toString() {
            return String.format("%s{%s}", a.class.getSimpleName(), super.toString());
        }

        /* JADX INFO: finally extract failed */
        public void a(Throwable th) {
            boolean z;
            Throwable nullPointerException = th == null ? new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.") : th;
            if (!g()) {
                try {
                    this.b.a(nullPointerException);
                    DisposableHelper.a(super);
                    z = true;
                } catch (Throwable th2) {
                    DisposableHelper.a(super);
                    throw th2;
                }
            } else {
                z = false;
            }
            if (!z) {
                c.b(th);
            }
        }
    }
}
