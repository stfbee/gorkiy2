package l.b.u.e.a;

import java.util.concurrent.atomic.AtomicReference;
import l.b.Completable;
import l.b.CompletableObserver;
import l.b.CompletableSource;
import l.b.Scheduler;
import l.b.c;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class CompletableObserveOn extends Completable {
    public final CompletableSource a;
    public final Scheduler b;

    public CompletableObserveOn(CompletableSource completableSource, Scheduler scheduler) {
        this.a = completableSource;
        this.b = scheduler;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.CompletableObserver, l.b.u.e.a.CompletableObserveOn$a] */
    public void b(CompletableObserver completableObserver) {
        this.a.a(new a(completableObserver, this.b));
    }

    public static final class a extends AtomicReference<b> implements c, b, Runnable {
        public final CompletableObserver b;
        public final Scheduler c;
        public Throwable d;

        public a(CompletableObserver completableObserver, Scheduler scheduler) {
            this.b = completableObserver;
            this.c = scheduler;
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.e.a.CompletableObserveOn$a, l.b.s.Disposable, java.util.concurrent.atomic.AtomicReference] */
        public void a(Disposable disposable) {
            if (DisposableHelper.b(super, disposable)) {
                this.b.a((Disposable) this);
            }
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public void run() {
            Throwable th = this.d;
            if (th != null) {
                this.d = null;
                this.b.a(th);
                return;
            }
            this.b.a();
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
         arg types: [l.b.u.e.a.CompletableObserveOn$a, l.b.s.Disposable]
         candidates:
          l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
          l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
        public void a(Throwable th) {
            this.d = th;
            DisposableHelper.a((AtomicReference<b>) super, (b) this.c.a(this));
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
         arg types: [l.b.u.e.a.CompletableObserveOn$a, l.b.s.Disposable]
         candidates:
          l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
          l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
        public void a() {
            DisposableHelper.a((AtomicReference<b>) super, (b) this.c.a(this));
        }
    }
}
