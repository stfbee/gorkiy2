package l.b.u.e.c;

import java.util.concurrent.Callable;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import l.b.ObservableSource;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.t.Function;
import l.b.u.a.DisposableHelper;
import l.b.u.b.ObjectHelper;
import l.b.u.c.QueueDisposable;
import l.b.u.c.SimpleQueue;
import l.b.u.f.SpscLinkedArrayQueue;
import l.b.u.h.AtomicThrowable;
import l.b.u.h.ErrorMode;
import l.b.u.h.ExceptionHelper;
import l.b.u.h.c;
import l.b.v.SerializedObserver;

public final class ObservableConcatMap<T, U> extends AbstractObservableWithUpstream<T, U> {
    public final Function<? super T, ? extends ObservableSource<? extends U>> c;
    public final int d;

    /* renamed from: e  reason: collision with root package name */
    public final ErrorMode f2684e;

    public ObservableConcatMap(ObservableSource<T> observableSource, Function<? super T, ? extends ObservableSource<? extends U>> function, int i2, c cVar) {
        super(observableSource);
        this.c = function;
        this.f2684e = cVar;
        this.d = Math.max(8, i2);
    }

    public void b(Observer<? super U> observer) {
        if (!j.c.a.a.c.n.c.a(super.b, observer, this.c)) {
            if (this.f2684e == ErrorMode.IMMEDIATE) {
                super.b.a(new b(new SerializedObserver(observer), this.c, this.d));
            } else {
                super.b.a(new a(observer, this.c, this.d, this.f2684e == ErrorMode.END));
            }
        }
    }

    public static final class a<T, R> extends AtomicInteger implements Observer<T>, l.b.s.b {
        public final Observer<? super R> b;
        public final Function<? super T, ? extends ObservableSource<? extends R>> c;
        public final int d;

        /* renamed from: e  reason: collision with root package name */
        public final AtomicThrowable f2685e = new AtomicThrowable();

        /* renamed from: f  reason: collision with root package name */
        public final C0034a<R> f2686f;
        public final boolean g;
        public SimpleQueue<T> h;

        /* renamed from: i  reason: collision with root package name */
        public Disposable f2687i;

        /* renamed from: j  reason: collision with root package name */
        public volatile boolean f2688j;

        /* renamed from: k  reason: collision with root package name */
        public volatile boolean f2689k;

        /* renamed from: l  reason: collision with root package name */
        public volatile boolean f2690l;

        /* renamed from: m  reason: collision with root package name */
        public int f2691m;

        /* renamed from: l.b.u.e.c.ObservableConcatMap$a$a  reason: collision with other inner class name */
        public static final class C0034a<R> extends AtomicReference<l.b.s.b> implements Observer<R> {
            public final Observer<? super R> b;
            public final a<?, R> c;

            public C0034a(Observer<? super R> observer, a<?, R> aVar) {
                this.b = observer;
                this.c = aVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
             arg types: [l.b.u.e.c.ObservableConcatMap$a$a, l.b.s.Disposable]
             candidates:
              l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
              l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
            public void a(Disposable disposable) {
                DisposableHelper.a((AtomicReference<l.b.s.b>) super, (l.b.s.b) disposable);
            }

            public void b(R r2) {
                this.b.b(r2);
            }

            public void a(Throwable th) {
                a<?, R> aVar = this.c;
                AtomicThrowable atomicThrowable = aVar.f2685e;
                if (atomicThrowable == null) {
                    throw null;
                } else if (ExceptionHelper.a(super, th)) {
                    if (!aVar.g) {
                        aVar.f2687i.f();
                    }
                    aVar.f2688j = false;
                    aVar.b();
                } else {
                    j.c.a.a.c.n.c.b(th);
                }
            }

            public void a() {
                a<?, R> aVar = this.c;
                aVar.f2688j = false;
                aVar.b();
            }
        }

        public a(Observer<? super R> observer, Function<? super T, ? extends ObservableSource<? extends R>> function, int i2, boolean z) {
            this.b = observer;
            this.c = function;
            this.d = i2;
            this.g = z;
            this.f2686f = new C0034a<>(observer, this);
        }

        /* JADX WARN: Type inference failed for: r3v3, types: [l.b.u.c.QueueDisposable, l.b.u.c.SimpleQueue<T>] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.f2687i, disposable)) {
                this.f2687i = disposable;
                if (disposable instanceof QueueDisposable) {
                    ? r3 = (QueueDisposable) disposable;
                    int a = r3.a(3);
                    if (a == 1) {
                        this.f2691m = a;
                        this.h = r3;
                        this.f2689k = true;
                        this.b.a((Disposable) this);
                        b();
                        return;
                    } else if (a == 2) {
                        this.f2691m = a;
                        this.h = r3;
                        this.b.a((Disposable) this);
                        return;
                    }
                }
                this.h = new SpscLinkedArrayQueue(this.d);
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (this.f2691m == 0) {
                this.h.offer(t2);
            }
            b();
        }

        public void f() {
            this.f2690l = true;
            this.f2687i.f();
            C0034a<R> aVar = this.f2686f;
            if (aVar != null) {
                DisposableHelper.a(aVar);
                return;
            }
            throw null;
        }

        public boolean g() {
            return this.f2690l;
        }

        /* JADX INFO: finally extract failed */
        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.ObservableSource<? extends R>, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void b() {
            if (getAndIncrement() == 0) {
                Observer<? super R> observer = this.b;
                SimpleQueue<T> simpleQueue = this.h;
                AtomicThrowable atomicThrowable = this.f2685e;
                while (true) {
                    if (!this.f2688j) {
                        if (this.f2690l) {
                            simpleQueue.clear();
                            return;
                        } else if (this.g || ((Throwable) atomicThrowable.get()) == null) {
                            boolean z = this.f2689k;
                            try {
                                T poll = simpleQueue.poll();
                                boolean z2 = poll == null;
                                if (z && z2) {
                                    this.f2690l = true;
                                    if (atomicThrowable != null) {
                                        Throwable a = ExceptionHelper.a(atomicThrowable);
                                        if (a != null) {
                                            observer.a(a);
                                            return;
                                        } else {
                                            observer.a();
                                            return;
                                        }
                                    } else {
                                        throw null;
                                    }
                                } else if (!z2) {
                                    try {
                                        Object a2 = this.c.a(poll);
                                        ObjectHelper.a((Object) a2, "The mapper returned a null ObservableSource");
                                        ObservableSource observableSource = (ObservableSource) a2;
                                        if (observableSource instanceof Callable) {
                                            try {
                                                Object call = ((Callable) observableSource).call();
                                                if (call != null && !this.f2690l) {
                                                    observer.b(call);
                                                }
                                            } catch (Throwable th) {
                                                j.c.a.a.c.n.c.c(th);
                                                if (atomicThrowable != null) {
                                                    ExceptionHelper.a(atomicThrowable, th);
                                                } else {
                                                    throw null;
                                                }
                                            }
                                        } else {
                                            this.f2688j = true;
                                            observableSource.a(this.f2686f);
                                        }
                                    } catch (Throwable th2) {
                                        j.c.a.a.c.n.c.c(th2);
                                        this.f2690l = true;
                                        this.f2687i.f();
                                        simpleQueue.clear();
                                        if (atomicThrowable != null) {
                                            ExceptionHelper.a(atomicThrowable, th2);
                                            observer.a(ExceptionHelper.a(atomicThrowable));
                                            return;
                                        }
                                        throw null;
                                    }
                                }
                            } catch (Throwable th3) {
                                j.c.a.a.c.n.c.c(th3);
                                this.f2690l = true;
                                this.f2687i.f();
                                if (atomicThrowable != null) {
                                    ExceptionHelper.a(atomicThrowable, th3);
                                    observer.a(ExceptionHelper.a(atomicThrowable));
                                    return;
                                }
                                throw null;
                            }
                        } else {
                            simpleQueue.clear();
                            this.f2690l = true;
                            observer.a(ExceptionHelper.a(atomicThrowable));
                            return;
                        }
                    }
                    if (decrementAndGet() == 0) {
                        return;
                    }
                }
            }
        }

        public void a(Throwable th) {
            AtomicThrowable atomicThrowable = this.f2685e;
            if (atomicThrowable == null) {
                throw null;
            } else if (ExceptionHelper.a(atomicThrowable, th)) {
                this.f2689k = true;
                b();
            } else {
                j.c.a.a.c.n.c.b(th);
            }
        }

        public void a() {
            this.f2689k = true;
            b();
        }
    }

    public static final class b<T, U> extends AtomicInteger implements Observer<T>, l.b.s.b {
        public final Observer<? super U> b;
        public final Function<? super T, ? extends ObservableSource<? extends U>> c;
        public final a<U> d;

        /* renamed from: e  reason: collision with root package name */
        public final int f2692e;

        /* renamed from: f  reason: collision with root package name */
        public SimpleQueue<T> f2693f;
        public Disposable g;
        public volatile boolean h;

        /* renamed from: i  reason: collision with root package name */
        public volatile boolean f2694i;

        /* renamed from: j  reason: collision with root package name */
        public volatile boolean f2695j;

        /* renamed from: k  reason: collision with root package name */
        public int f2696k;

        public static final class a<U> extends AtomicReference<l.b.s.b> implements Observer<U> {
            public final Observer<? super U> b;
            public final b<?, ?> c;

            public a(Observer<? super U> observer, b<?, ?> bVar) {
                this.b = observer;
                this.c = bVar;
            }

            /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
             method: l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean
             arg types: [l.b.u.e.c.ObservableConcatMap$b$a, l.b.s.Disposable]
             candidates:
              l.b.u.a.DisposableHelper.a(l.b.s.Disposable, l.b.s.Disposable):boolean
              l.b.u.a.DisposableHelper.a(java.util.concurrent.atomic.AtomicReference<l.b.s.b>, l.b.s.b):boolean */
            public void a(Disposable disposable) {
                DisposableHelper.a((AtomicReference<l.b.s.b>) super, (l.b.s.b) disposable);
            }

            public void b(U u) {
                this.b.b(u);
            }

            public void a(Throwable th) {
                this.c.f();
                this.b.a(th);
            }

            public void a() {
                b<?, ?> bVar = this.c;
                bVar.h = false;
                bVar.b();
            }
        }

        public b(Observer<? super U> observer, Function<? super T, ? extends ObservableSource<? extends U>> function, int i2) {
            this.b = observer;
            this.c = function;
            this.f2692e = i2;
            this.d = new a<>(observer, this);
        }

        /* JADX WARN: Type inference failed for: r3v3, types: [l.b.u.c.QueueDisposable, l.b.u.c.SimpleQueue<T>] */
        public void a(Disposable disposable) {
            if (DisposableHelper.a(this.g, disposable)) {
                this.g = disposable;
                if (disposable instanceof QueueDisposable) {
                    ? r3 = (QueueDisposable) disposable;
                    int a2 = r3.a(3);
                    if (a2 == 1) {
                        this.f2696k = a2;
                        this.f2693f = r3;
                        this.f2695j = true;
                        this.b.a((Disposable) this);
                        b();
                        return;
                    } else if (a2 == 2) {
                        this.f2696k = a2;
                        this.f2693f = r3;
                        this.b.a((Disposable) this);
                        return;
                    }
                }
                this.f2693f = new SpscLinkedArrayQueue(this.f2692e);
                this.b.a((Disposable) this);
            }
        }

        public void b(T t2) {
            if (!this.f2695j) {
                if (this.f2696k == 0) {
                    this.f2693f.offer(t2);
                }
                b();
            }
        }

        public void f() {
            this.f2694i = true;
            a<U> aVar = this.d;
            if (aVar != null) {
                DisposableHelper.a(aVar);
                this.g.f();
                if (getAndIncrement() == 0) {
                    this.f2693f.clear();
                    return;
                }
                return;
            }
            throw null;
        }

        public boolean g() {
            return this.f2694i;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
         arg types: [? extends l.b.ObservableSource<? extends U>, java.lang.String]
         candidates:
          l.b.u.b.ObjectHelper.a(int, java.lang.String):int
          l.b.u.b.ObjectHelper.a(long, long):int
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
          l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
        public void b() {
            if (getAndIncrement() == 0) {
                while (!this.f2694i) {
                    if (!this.h) {
                        boolean z = this.f2695j;
                        try {
                            T poll = this.f2693f.poll();
                            boolean z2 = poll == null;
                            if (z && z2) {
                                this.f2694i = true;
                                this.b.a();
                                return;
                            } else if (!z2) {
                                try {
                                    Object a2 = this.c.a(poll);
                                    ObjectHelper.a((Object) a2, "The mapper returned a null ObservableSource");
                                    ObservableSource observableSource = (ObservableSource) a2;
                                    this.h = true;
                                    observableSource.a(this.d);
                                } catch (Throwable th) {
                                    j.c.a.a.c.n.c.c(th);
                                    f();
                                    this.f2693f.clear();
                                    this.b.a(th);
                                    return;
                                }
                            }
                        } catch (Throwable th2) {
                            j.c.a.a.c.n.c.c(th2);
                            f();
                            this.f2693f.clear();
                            this.b.a(th2);
                            return;
                        }
                    }
                    if (decrementAndGet() == 0) {
                        return;
                    }
                }
                this.f2693f.clear();
            }
        }

        public void a(Throwable th) {
            if (this.f2695j) {
                j.c.a.a.c.n.c.b(th);
                return;
            }
            this.f2695j = true;
            f();
            this.b.a(th);
        }

        public void a() {
            if (!this.f2695j) {
                this.f2695j = true;
                b();
            }
        }
    }
}
