package l.b.u.e.d;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Single;
import l.b.SingleEmitter;
import l.b.SingleObserver;
import l.b.SingleOnSubscribe;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.a.DisposableHelper;

public final class SingleCreate<T> extends Single<T> {
    public final SingleOnSubscribe<T> a;

    public SingleCreate(SingleOnSubscribe<T> singleOnSubscribe) {
        this.a = singleOnSubscribe;
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.SingleEmitter, l.b.u.e.d.SingleCreate$a] */
    public void b(SingleObserver<? super T> singleObserver) {
        ? aVar = new a(singleObserver);
        singleObserver.a((Disposable) aVar);
        try {
            this.a.a(aVar);
        } catch (Throwable th) {
            c.c(th);
            aVar.a(th);
        }
    }

    public static final class a<T> extends AtomicReference<b> implements SingleEmitter<T>, b {
        public final SingleObserver<? super T> b;

        public a(SingleObserver<? super T> singleObserver) {
            this.b = singleObserver;
        }

        public void a(Object obj) {
            Disposable disposable;
            Object obj2 = get();
            DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
            if (obj2 != disposableHelper && (disposable = (Disposable) getAndSet(disposableHelper)) != DisposableHelper.DISPOSED) {
                if (obj == null) {
                    try {
                        this.b.a((Throwable) new NullPointerException("onSuccess called with null. Null values are generally not allowed in 2.x operators and sources."));
                    } catch (Throwable th) {
                        if (disposable != null) {
                            disposable.f();
                        }
                        throw th;
                    }
                } else {
                    this.b.a(obj);
                }
                if (disposable != null) {
                    disposable.f();
                }
            }
        }

        public void f() {
            DisposableHelper.a(super);
        }

        public boolean g() {
            return DisposableHelper.a((Disposable) get());
        }

        public String toString() {
            return String.format("%s{%s}", a.class.getSimpleName(), super.toString());
        }

        public void a(Throwable th) {
            boolean z;
            Disposable disposable;
            Throwable nullPointerException = th == null ? new NullPointerException("onError called with null. Null values are generally not allowed in 2.x operators and sources.") : th;
            Object obj = get();
            DisposableHelper disposableHelper = DisposableHelper.DISPOSED;
            if (obj == disposableHelper || (disposable = (Disposable) getAndSet(disposableHelper)) == DisposableHelper.DISPOSED) {
                z = false;
            } else {
                try {
                    this.b.a(nullPointerException);
                    z = true;
                } finally {
                    if (disposable != null) {
                        disposable.f();
                    }
                }
            }
            if (!z) {
                c.b(th);
            }
        }
    }
}
