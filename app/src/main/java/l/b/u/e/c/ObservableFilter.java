package l.b.u.e.c;

import l.b.ObservableSource;
import l.b.Observer;
import l.b.t.Predicate;
import l.b.u.d.BasicFuseableObserver;

public final class ObservableFilter<T> extends AbstractObservableWithUpstream<T, T> {
    public final Predicate<? super T> c;

    public static final class a<T> extends BasicFuseableObserver<T, T> {
        public final Predicate<? super T> g;

        public a(Observer<? super T> observer, Predicate<? super T> predicate) {
            super(observer);
            this.g = predicate;
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public int a(int r1) {
            /*
                r0 = this;
                int r1 = r0.b(r1)
                return r1
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFilter.a.a(int):int");
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void b(T r2) {
            /*
                r1 = this;
                int r0 = r1.f2678f
                if (r0 != 0) goto L_0x0017
                l.b.t.Predicate<? super T> r0 = r1.g     // Catch:{ all -> 0x0012 }
                boolean r0 = r0.a(r2)     // Catch:{ all -> 0x0012 }
                if (r0 == 0) goto L_0x001d
                l.b.Observer<? super R> r0 = r1.b
                r0.b(r2)
                goto L_0x001d
            L_0x0012:
                r2 = move-exception
                r1.b(r2)
                return
            L_0x0017:
                l.b.Observer<? super R> r2 = r1.b
                r0 = 0
                r2.b(r0)
            L_0x001d:
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableFilter.a.b(java.lang.Object):void");
        }

        /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.c.QueueDisposable<T>, l.b.u.c.SimpleQueue] */
        public T poll() {
            T poll;
            do {
                poll = super.d.poll();
                if (poll == null) {
                    break;
                }
            } while (!this.g.a(poll));
            return poll;
        }
    }

    public ObservableFilter(ObservableSource<T> observableSource, Predicate<? super T> predicate) {
        super(observableSource);
        this.c = predicate;
    }

    public void b(Observer<? super T> observer) {
        super.b.a(new a(observer, this.c));
    }
}
