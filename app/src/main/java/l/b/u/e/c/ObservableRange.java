package l.b.u.e.c;

import l.b.Observable;
import l.b.Observer;
import l.b.u.d.BasicIntQueueDisposable;

public final class ObservableRange extends Observable<Integer> {
    public final int b;
    public final long c;

    public static final class a extends BasicIntQueueDisposable<Integer> {
        public final Observer<? super Integer> b;
        public final long c;
        public long d;

        /* renamed from: e  reason: collision with root package name */
        public boolean f2744e;

        public a(Observer<? super Integer> observer, long j2, long j3) {
            this.b = observer;
            this.d = j2;
            this.c = j3;
        }

        public int a(int i2) {
            if ((i2 & 1) == 0) {
                return 0;
            }
            this.f2744e = true;
            return 1;
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void clear() {
            /*
                r2 = this;
                long r0 = r2.c
                r2.d = r0
                r0 = 1
                r2.lazySet(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableRange.a.clear():void");
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public void f() {
            /*
                r1 = this;
                r0 = 1
                r1.set(r0)
                return
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableRange.a.f():void");
        }

        public boolean g() {
            return get() != 0;
        }

        public boolean isEmpty() {
            return this.d == this.c;
        }

        /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
            jadx.core.utils.exceptions.JadxOverflowException: 
            	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
            	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
            */
        public java.lang.Object poll() {
            /*
                r5 = this;
                long r0 = r5.d
                long r2 = r5.c
                int r4 = (r0 > r2 ? 1 : (r0 == r2 ? 0 : -1))
                if (r4 == 0) goto L_0x0013
                r2 = 1
                long r2 = r2 + r0
                r5.d = r2
                int r1 = (int) r0
                java.lang.Integer r0 = java.lang.Integer.valueOf(r1)
                goto L_0x0018
            L_0x0013:
                r0 = 1
                r5.lazySet(r0)
                r0 = 0
            L_0x0018:
                return r0
            */
            throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableRange.a.poll():java.lang.Object");
        }
    }

    public ObservableRange(int i2, int i3) {
        this.b = i2;
        this.c = ((long) i2) + ((long) i3);
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public void b(l.b.Observer<? super java.lang.Integer> r8) {
        /*
            r7 = this;
            l.b.u.e.c.ObservableRange$a r6 = new l.b.u.e.c.ObservableRange$a
            int r0 = r7.b
            long r2 = (long) r0
            long r4 = r7.c
            r0 = r6
            r1 = r8
            r0.<init>(r1, r2, r4)
            r8.a(r6)
            boolean r8 = r6.f2744e
            if (r8 == 0) goto L_0x0014
            goto L_0x003d
        L_0x0014:
            l.b.Observer<? super java.lang.Integer> r8 = r6.b
            long r0 = r6.c
            long r2 = r6.d
        L_0x001a:
            int r4 = (r2 > r0 ? 1 : (r2 == r0 ? 0 : -1))
            if (r4 == 0) goto L_0x0030
            int r4 = r6.get()
            if (r4 != 0) goto L_0x0030
            int r4 = (int) r2
            java.lang.Integer r4 = java.lang.Integer.valueOf(r4)
            r8.b(r4)
            r4 = 1
            long r2 = r2 + r4
            goto L_0x001a
        L_0x0030:
            int r0 = r6.get()
            if (r0 != 0) goto L_0x003d
            r0 = 1
            r6.lazySet(r0)
            r8.a()
        L_0x003d:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.e.c.ObservableRange.b(l.b.Observer):void");
    }
}
