package l.b.u.g;

import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Scheduler;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.a.ListCompositeDisposable;
import l.b.u.b.ObjectHelper;
import l.b.u.g.a;

public final class ComputationScheduler extends Scheduler {
    public static final b c;
    public static final RxThreadFactory d;

    /* renamed from: e  reason: collision with root package name */
    public static final int f2758e;

    /* renamed from: f  reason: collision with root package name */
    public static final c f2759f;
    public final ThreadFactory a = d;
    public final AtomicReference<a.b> b = new AtomicReference<>(c);

    public static final class b {
        public final int a;
        public final c[] b;
        public long c;

        public b(int i2, ThreadFactory threadFactory) {
            this.a = i2;
            this.b = new c[i2];
            for (int i3 = 0; i3 < i2; i3++) {
                this.b[i3] = new c(threadFactory);
            }
        }

        public c a() {
            int i2 = this.a;
            if (i2 == 0) {
                return ComputationScheduler.f2759f;
            }
            c[] cVarArr = this.b;
            long j2 = this.c;
            this.c = 1 + j2;
            return cVarArr[(int) (j2 % ((long) i2))];
        }
    }

    public static final class c extends NewThreadWorker {
        public c(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        int availableProcessors = Runtime.getRuntime().availableProcessors();
        int intValue = Integer.getInteger("rx2.computation-threads", 0).intValue();
        if (intValue > 0 && intValue <= availableProcessors) {
            availableProcessors = intValue;
        }
        f2758e = availableProcessors;
        c cVar = new c(new RxThreadFactory("RxComputationShutdown"));
        f2759f = cVar;
        cVar.f();
        RxThreadFactory rxThreadFactory = new RxThreadFactory("RxComputationThreadPool", Math.max(1, Math.min(10, Integer.getInteger("rx2.computation-priority", 5).intValue())), true);
        d = rxThreadFactory;
        b bVar = new b(0, rxThreadFactory);
        c = bVar;
        for (c f2 : bVar.b) {
            f2.f();
        }
    }

    public ComputationScheduler() {
        b bVar = new b(f2758e, this.a);
        if (!this.b.compareAndSet(c, bVar)) {
            for (c f2 : bVar.b) {
                f2.f();
            }
        }
    }

    public Scheduler.b a() {
        return new a(this.b.get().a());
    }

    /* JADX WARN: Type inference failed for: r1v1, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Runnable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
        Future future;
        c a2 = this.b.get().a();
        if (a2 != null) {
            ObjectHelper.a((Object) runnable, "run is null");
            ScheduledDirectTask scheduledDirectTask = new ScheduledDirectTask(runnable);
            if (j2 <= 0) {
                try {
                    future = a2.b.submit(scheduledDirectTask);
                } catch (RejectedExecutionException e2) {
                    j.c.a.a.c.n.c.b((Throwable) e2);
                    return EmptyDisposable.INSTANCE;
                }
            } else {
                future = a2.b.schedule(scheduledDirectTask, j2, timeUnit);
            }
            scheduledDirectTask.a(future);
            return scheduledDirectTask;
        }
        throw null;
    }

    public static final class a extends Scheduler.b {
        public final ListCompositeDisposable b = new ListCompositeDisposable();
        public final CompositeDisposable c = new CompositeDisposable();
        public final ListCompositeDisposable d;

        /* renamed from: e  reason: collision with root package name */
        public final c f2760e;

        /* renamed from: f  reason: collision with root package name */
        public volatile boolean f2761f;

        public a(c cVar) {
            this.f2760e = cVar;
            ListCompositeDisposable listCompositeDisposable = new ListCompositeDisposable();
            this.d = listCompositeDisposable;
            listCompositeDisposable.c(this.b);
            this.d.c(this.c);
        }

        /* JADX WARN: Type inference failed for: r7v1, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable] */
        /* JADX WARN: Type inference failed for: r7v2, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        public Disposable a(Runnable runnable) {
            if (this.f2761f) {
                return EmptyDisposable.INSTANCE;
            }
            return this.f2760e.a(runnable, 0, TimeUnit.MILLISECONDS, this.b);
        }

        public void f() {
            if (!this.f2761f) {
                this.f2761f = true;
                this.d.f();
            }
        }

        public boolean g() {
            return this.f2761f;
        }

        /* JADX WARN: Type inference failed for: r7v1, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable] */
        /* JADX WARN: Type inference failed for: r7v2, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
            if (this.f2761f) {
                return EmptyDisposable.INSTANCE;
            }
            return this.f2760e.a(runnable, j2, timeUnit, this.c);
        }
    }
}
