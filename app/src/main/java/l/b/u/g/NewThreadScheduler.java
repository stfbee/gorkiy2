package l.b.u.g;

import java.util.concurrent.ThreadFactory;
import l.b.Scheduler;

public final class NewThreadScheduler extends Scheduler {
    public static final RxThreadFactory b = new RxThreadFactory("RxNewThreadScheduler", Math.max(1, Math.min(10, Integer.getInteger("rx2.newthread-priority", 5).intValue())));
    public final ThreadFactory a = b;

    public Scheduler.b a() {
        return new NewThreadWorker(this.a);
    }
}
