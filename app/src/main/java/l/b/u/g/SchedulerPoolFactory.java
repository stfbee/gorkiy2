package l.b.u.g;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicReference;

public final class SchedulerPoolFactory {
    public static final boolean a;
    public static final int b;
    public static final AtomicReference<ScheduledExecutorService> c = new AtomicReference<>();
    public static final Map<ScheduledThreadPoolExecutor, Object> d = new ConcurrentHashMap();

    public static final class a implements Runnable {
        public void run() {
            Iterator it = new ArrayList(SchedulerPoolFactory.d.keySet()).iterator();
            while (it.hasNext()) {
                ScheduledThreadPoolExecutor scheduledThreadPoolExecutor = (ScheduledThreadPoolExecutor) it.next();
                if (scheduledThreadPoolExecutor.isShutdown()) {
                    SchedulerPoolFactory.d.remove(scheduledThreadPoolExecutor);
                } else {
                    scheduledThreadPoolExecutor.purge();
                }
            }
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:10:0x0026 A[SYNTHETIC, Splitter:B:10:0x0026] */
    /* JADX WARNING: Removed duplicated region for block: B:19:0x0039 A[LOOP:0: B:19:0x0039->B:24:0x0066, LOOP_START] */
    /* JADX WARNING: Removed duplicated region for block: B:27:? A[ORIG_RETURN, RETURN, SYNTHETIC] */
    static {
        /*
            java.util.concurrent.atomic.AtomicReference r0 = new java.util.concurrent.atomic.AtomicReference
            r0.<init>()
            l.b.u.g.SchedulerPoolFactory.c = r0
            java.util.concurrent.ConcurrentHashMap r0 = new java.util.concurrent.ConcurrentHashMap
            r0.<init>()
            l.b.u.g.SchedulerPoolFactory.d = r0
            java.lang.String r0 = "rx2.purge-enabled"
            r1 = 1
            java.lang.String r0 = java.lang.System.getProperty(r0)     // Catch:{ all -> 0x001f }
            if (r0 != 0) goto L_0x0018
            goto L_0x001f
        L_0x0018:
            java.lang.String r2 = "true"
            boolean r0 = r2.equals(r0)     // Catch:{ all -> 0x001f }
            goto L_0x0020
        L_0x001f:
            r0 = 1
        L_0x0020:
            l.b.u.g.SchedulerPoolFactory.a = r0
            java.lang.String r2 = "rx2.purge-period-seconds"
            if (r0 == 0) goto L_0x0032
            java.lang.String r0 = java.lang.System.getProperty(r2)     // Catch:{ all -> 0x0032 }
            if (r0 != 0) goto L_0x002d
            goto L_0x0032
        L_0x002d:
            int r0 = java.lang.Integer.parseInt(r0)     // Catch:{ all -> 0x0032 }
            goto L_0x0033
        L_0x0032:
            r0 = 1
        L_0x0033:
            l.b.u.g.SchedulerPoolFactory.b = r0
            boolean r0 = l.b.u.g.SchedulerPoolFactory.a
            if (r0 == 0) goto L_0x006a
        L_0x0039:
            java.util.concurrent.atomic.AtomicReference<java.util.concurrent.ScheduledExecutorService> r0 = l.b.u.g.SchedulerPoolFactory.c
            java.lang.Object r0 = r0.get()
            java.util.concurrent.ScheduledExecutorService r0 = (java.util.concurrent.ScheduledExecutorService) r0
            if (r0 == 0) goto L_0x0044
            goto L_0x006a
        L_0x0044:
            l.b.u.g.RxThreadFactory r2 = new l.b.u.g.RxThreadFactory
            java.lang.String r3 = "RxSchedulerPurge"
            r2.<init>(r3)
            java.util.concurrent.ScheduledExecutorService r4 = java.util.concurrent.Executors.newScheduledThreadPool(r1, r2)
            java.util.concurrent.atomic.AtomicReference<java.util.concurrent.ScheduledExecutorService> r2 = l.b.u.g.SchedulerPoolFactory.c
            boolean r0 = r2.compareAndSet(r0, r4)
            if (r0 == 0) goto L_0x0066
            l.b.u.g.SchedulerPoolFactory$a r5 = new l.b.u.g.SchedulerPoolFactory$a
            r5.<init>()
            int r0 = l.b.u.g.SchedulerPoolFactory.b
            long r8 = (long) r0
            java.util.concurrent.TimeUnit r10 = java.util.concurrent.TimeUnit.SECONDS
            r6 = r8
            r4.scheduleAtFixedRate(r5, r6, r8, r10)
            goto L_0x006a
        L_0x0066:
            r4.shutdownNow()
            goto L_0x0039
        L_0x006a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.g.SchedulerPoolFactory.<clinit>():void");
    }

    public static ScheduledExecutorService a(ThreadFactory threadFactory) {
        ScheduledExecutorService newScheduledThreadPool = Executors.newScheduledThreadPool(1, threadFactory);
        if (a && (newScheduledThreadPool instanceof ScheduledThreadPoolExecutor)) {
            d.put((ScheduledThreadPoolExecutor) newScheduledThreadPool, newScheduledThreadPool);
        }
        return newScheduledThreadPool;
    }
}
