package l.b.u.g;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Scheduler;
import l.b.s.CompositeDisposable;
import l.b.s.Disposable;
import l.b.u.a.EmptyDisposable;
import l.b.u.g.b;

public final class IoScheduler extends Scheduler {
    public static final RxThreadFactory c;
    public static final RxThreadFactory d;

    /* renamed from: e  reason: collision with root package name */
    public static final long f2762e = Long.getLong("rx2.io-keep-alive-time", 60).longValue();

    /* renamed from: f  reason: collision with root package name */
    public static final TimeUnit f2763f = TimeUnit.SECONDS;
    public static final c g;
    public static final a h;
    public final ThreadFactory a = c;
    public final AtomicReference<b.a> b = new AtomicReference<>(h);

    public static final class a implements Runnable {
        public final long b;
        public final ConcurrentLinkedQueue<b.c> c;
        public final CompositeDisposable d;

        /* renamed from: e  reason: collision with root package name */
        public final ScheduledExecutorService f2764e;

        /* renamed from: f  reason: collision with root package name */
        public final Future<?> f2765f;
        public final ThreadFactory g;

        public a(long j2, TimeUnit timeUnit, ThreadFactory threadFactory) {
            ScheduledFuture<?> scheduledFuture;
            this.b = timeUnit != null ? timeUnit.toNanos(j2) : 0;
            this.c = new ConcurrentLinkedQueue<>();
            this.d = new CompositeDisposable();
            this.g = threadFactory;
            ScheduledExecutorService scheduledExecutorService = null;
            if (timeUnit != null) {
                scheduledExecutorService = Executors.newScheduledThreadPool(1, IoScheduler.d);
                long j3 = this.b;
                scheduledFuture = scheduledExecutorService.scheduleWithFixedDelay(this, j3, j3, TimeUnit.NANOSECONDS);
            } else {
                scheduledFuture = null;
            }
            this.f2764e = scheduledExecutorService;
            this.f2765f = scheduledFuture;
        }

        public void run() {
            if (!this.c.isEmpty()) {
                long nanoTime = System.nanoTime();
                Iterator<b.c> it = this.c.iterator();
                while (it.hasNext()) {
                    c next = it.next();
                    if (next.d > nanoTime) {
                        return;
                    }
                    if (this.c.remove(next) && this.d.a(next)) {
                        next.f();
                    }
                }
            }
        }
    }

    public static final class b extends Scheduler.b {
        public final CompositeDisposable b;
        public final a c;
        public final c d;

        /* renamed from: e  reason: collision with root package name */
        public final AtomicBoolean f2766e = new AtomicBoolean();

        public b(a aVar) {
            c cVar;
            c cVar2;
            this.c = aVar;
            this.b = new CompositeDisposable();
            if (aVar.d.c) {
                cVar = IoScheduler.g;
            } else {
                while (true) {
                    if (aVar.c.isEmpty()) {
                        cVar2 = new c(aVar.g);
                        aVar.d.c(cVar2);
                        break;
                    }
                    cVar2 = (c) aVar.c.poll();
                    if (cVar2 != null) {
                        break;
                    }
                }
                cVar = cVar2;
            }
            this.d = cVar;
        }

        /* JADX WARN: Type inference failed for: r7v1, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable] */
        /* JADX WARN: Type inference failed for: r7v2, types: [l.b.u.a.EmptyDisposable, l.b.s.Disposable] */
        public Disposable a(Runnable runnable, long j2, TimeUnit timeUnit) {
            if (this.b.c) {
                return EmptyDisposable.INSTANCE;
            }
            return this.d.a(runnable, j2, timeUnit, this.b);
        }

        public void f() {
            if (this.f2766e.compareAndSet(false, true)) {
                this.b.f();
                a aVar = this.c;
                c cVar = this.d;
                if (aVar != null) {
                    cVar.d = System.nanoTime() + aVar.b;
                    aVar.c.offer(cVar);
                    return;
                }
                throw null;
            }
        }

        public boolean g() {
            return this.f2766e.get();
        }
    }

    public static final class c extends NewThreadWorker {
        public long d = 0;

        public c(ThreadFactory threadFactory) {
            super(threadFactory);
        }
    }

    static {
        c cVar = new c(new RxThreadFactory("RxCachedThreadSchedulerShutdown"));
        g = cVar;
        cVar.f();
        int max = Math.max(1, Math.min(10, Integer.getInteger("rx2.io-priority", 5).intValue()));
        c = new RxThreadFactory("RxCachedThreadScheduler", max);
        d = new RxThreadFactory("RxCachedWorkerPoolEvictor", max);
        a aVar = new a(0, null, c);
        h = aVar;
        aVar.d.f();
        Future<?> future = aVar.f2765f;
        if (future != null) {
            future.cancel(true);
        }
        ScheduledExecutorService scheduledExecutorService = aVar.f2764e;
        if (scheduledExecutorService != null) {
            scheduledExecutorService.shutdownNow();
        }
    }

    public IoScheduler() {
        a aVar = new a(f2762e, f2763f, this.a);
        if (!this.b.compareAndSet(h, aVar)) {
            aVar.d.f();
            Future<?> future = aVar.f2765f;
            if (future != null) {
                future.cancel(true);
            }
            ScheduledExecutorService scheduledExecutorService = aVar.f2764e;
            if (scheduledExecutorService != null) {
                scheduledExecutorService.shutdownNow();
            }
        }
    }

    public Scheduler.b a() {
        return new b(this.b.get());
    }
}
