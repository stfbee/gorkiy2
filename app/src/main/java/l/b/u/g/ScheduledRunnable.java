package l.b.u.g;

import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.atomic.AtomicReferenceArray;
import l.b.s.b;
import l.b.u.a.DisposableContainer;

public final class ScheduledRunnable extends AtomicReferenceArray<Object> implements Runnable, Callable<Object>, b {
    public static final Object c = new Object();
    public static final Object d = new Object();

    /* renamed from: e  reason: collision with root package name */
    public static final Object f2768e = new Object();

    /* renamed from: f  reason: collision with root package name */
    public static final Object f2769f = new Object();
    public final Runnable b;

    public ScheduledRunnable(Runnable runnable, DisposableContainer disposableContainer) {
        super(3);
        this.b = runnable;
        lazySet(0, disposableContainer);
    }

    public void a(Future<?> future) {
        Object obj;
        do {
            obj = get(1);
            if (obj != f2769f) {
                if (obj == d) {
                    future.cancel(false);
                    return;
                } else if (obj == f2768e) {
                    future.cancel(true);
                    return;
                }
            } else {
                return;
            }
        } while (!compareAndSet(1, obj, future));
    }

    public Object call() {
        run();
        return null;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable, java.util.concurrent.atomic.AtomicReferenceArray] */
    /* JADX WARNING: Removed duplicated region for block: B:0:0x0000 A[LOOP_START, MTH_ENTER_BLOCK] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void f() {
        /*
            r5 = this;
        L_0x0000:
            r0 = 1
            java.lang.Object r1 = r5.get(r0)
            java.lang.Object r2 = l.b.u.g.ScheduledRunnable.f2769f
            r3 = 0
            if (r1 == r2) goto L_0x0035
            java.lang.Object r2 = l.b.u.g.ScheduledRunnable.d
            if (r1 == r2) goto L_0x0035
            java.lang.Object r2 = l.b.u.g.ScheduledRunnable.f2768e
            if (r1 != r2) goto L_0x0013
            goto L_0x0035
        L_0x0013:
            r2 = 2
            java.lang.Object r2 = r5.get(r2)
            java.lang.Thread r4 = java.lang.Thread.currentThread()
            if (r2 == r4) goto L_0x0020
            r2 = 1
            goto L_0x0021
        L_0x0020:
            r2 = 0
        L_0x0021:
            if (r2 == 0) goto L_0x0026
            java.lang.Object r4 = l.b.u.g.ScheduledRunnable.f2768e
            goto L_0x0028
        L_0x0026:
            java.lang.Object r4 = l.b.u.g.ScheduledRunnable.d
        L_0x0028:
            boolean r0 = r5.compareAndSet(r0, r1, r4)
            if (r0 == 0) goto L_0x0000
            if (r1 == 0) goto L_0x0035
            java.util.concurrent.Future r1 = (java.util.concurrent.Future) r1
            r1.cancel(r2)
        L_0x0035:
            java.lang.Object r0 = r5.get(r3)
            java.lang.Object r1 = l.b.u.g.ScheduledRunnable.f2769f
            if (r0 == r1) goto L_0x004f
            java.lang.Object r1 = l.b.u.g.ScheduledRunnable.c
            if (r0 == r1) goto L_0x004f
            if (r0 != 0) goto L_0x0044
            goto L_0x004f
        L_0x0044:
            boolean r1 = r5.compareAndSet(r3, r0, r1)
            if (r1 == 0) goto L_0x0035
            l.b.u.a.DisposableContainer r0 = (l.b.u.a.DisposableContainer) r0
            r0.a(r5)
        L_0x004f:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.g.ScheduledRunnable.f():void");
    }

    public boolean g() {
        Object obj = get(0);
        if (obj == c || obj == f2769f) {
            return true;
        }
        return false;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [l.b.s.Disposable, l.b.u.g.ScheduledRunnable, java.util.concurrent.atomic.AtomicReferenceArray] */
    public void run() {
        Object obj;
        Object obj2;
        lazySet(2, Thread.currentThread());
        try {
            this.b.run();
        } catch (Throwable th) {
            lazySet(2, null);
            Object obj3 = get(0);
            if (!(obj3 == c || !compareAndSet(0, obj3, f2769f) || obj3 == null)) {
                ((DisposableContainer) obj3).a(this);
            }
            do {
                obj2 = get(1);
                if (obj2 == d || obj2 == f2768e) {
                    throw th;
                }
            } while (!compareAndSet(1, obj2, f2769f));
            throw th;
        }
        lazySet(2, null);
        Object obj4 = get(0);
        if (!(obj4 == c || !compareAndSet(0, obj4, f2769f) || obj4 == null)) {
            ((DisposableContainer) obj4).a(this);
        }
        do {
            obj = get(1);
            if (obj == d || obj == f2768e) {
                return;
            }
        } while (!compareAndSet(1, obj, f2769f));
    }
}
