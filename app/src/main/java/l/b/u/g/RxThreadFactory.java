package l.b.u.g;

import j.a.a.a.outline;
import java.util.concurrent.ThreadFactory;
import java.util.concurrent.atomic.AtomicLong;

public final class RxThreadFactory extends AtomicLong implements ThreadFactory {
    public final String b;
    public final int c;
    public final boolean d;

    public static final class a extends Thread {
        public a(Runnable runnable, String str) {
            super(runnable, str);
        }
    }

    public RxThreadFactory(String str) {
        this.b = str;
        this.c = 5;
        this.d = false;
    }

    public Thread newThread(Runnable runnable) {
        String str = this.b + '-' + incrementAndGet();
        Thread aVar = this.d ? new a(runnable, str) : new Thread(runnable, str);
        aVar.setPriority(this.c);
        aVar.setDaemon(true);
        return aVar;
    }

    public String toString() {
        return outline.a(outline.a("RxThreadFactory["), this.b, "]");
    }

    public RxThreadFactory(String str, int i2) {
        this.b = str;
        this.c = i2;
        this.d = false;
    }

    public RxThreadFactory(String str, int i2, boolean z) {
        this.b = str;
        this.c = i2;
        this.d = z;
    }
}
