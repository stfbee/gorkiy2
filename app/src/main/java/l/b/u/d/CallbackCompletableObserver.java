package l.b.u.d;

import io.reactivex.exceptions.OnErrorNotImplementedException;
import java.util.concurrent.atomic.AtomicReference;
import l.b.c;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.a;
import l.b.u.a.DisposableHelper;

public final class CallbackCompletableObserver extends AtomicReference<b> implements c, b, Consumer<Throwable> {
    public final Consumer<? super Throwable> b;
    public final Action c;

    public CallbackCompletableObserver(Action action) {
        this.b = this;
        this.c = action;
    }

    public void a(Object obj) {
        j.c.a.a.c.n.c.b((Throwable) new OnErrorNotImplementedException((Throwable) obj));
    }

    public void f() {
        DisposableHelper.a(super);
    }

    public boolean g() {
        return get() == DisposableHelper.DISPOSED;
    }

    public void a() {
        try {
            this.c.run();
        } catch (Throwable th) {
            j.c.a.a.c.n.c.c(th);
            j.c.a.a.c.n.c.b(th);
        }
        lazySet(DisposableHelper.DISPOSED);
    }

    public CallbackCompletableObserver(Consumer<? super Throwable> consumer, a aVar) {
        this.b = consumer;
        this.c = aVar;
    }

    public void a(Throwable th) {
        try {
            this.b.a(th);
        } catch (Throwable th2) {
            j.c.a.a.c.n.c.c(th2);
            j.c.a.a.c.n.c.b(th2);
        }
        lazySet(DisposableHelper.DISPOSED);
    }

    public void a(Disposable disposable) {
        DisposableHelper.b(super, disposable);
    }
}
