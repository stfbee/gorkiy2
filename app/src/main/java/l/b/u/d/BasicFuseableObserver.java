package l.b.u.d;

import j.c.a.a.c.n.c;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.u.a.DisposableHelper;
import l.b.u.c.QueueDisposable;

public abstract class BasicFuseableObserver<T, R> implements Observer<T>, QueueDisposable<R> {
    public final Observer<? super R> b;
    public Disposable c;
    public QueueDisposable<T> d;

    /* renamed from: e  reason: collision with root package name */
    public boolean f2677e;

    /* renamed from: f  reason: collision with root package name */
    public int f2678f;

    public BasicFuseableObserver(Observer<? super R> observer) {
        this.b = observer;
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [l.b.s.Disposable, l.b.u.d.BasicFuseableObserver] */
    public final void a(Disposable disposable) {
        if (DisposableHelper.a(this.c, disposable)) {
            this.c = disposable;
            if (disposable instanceof QueueDisposable) {
                this.d = (QueueDisposable) disposable;
            }
            this.b.a((Disposable) this);
        }
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public final void b(java.lang.Throwable r2) {
        /*
            r1 = this;
            j.c.a.a.c.n.c.c(r2)
            l.b.s.Disposable r0 = r1.c
            r0.f()
            r1.a(r2)
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.BasicFuseableObserver.b(java.lang.Throwable):void");
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.c.QueueDisposable<T>, l.b.u.c.SimpleQueue] */
    public void clear() {
        this.d.clear();
    }

    public void f() {
        this.c.f();
    }

    public boolean g() {
        return this.c.g();
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.u.c.QueueDisposable<T>, l.b.u.c.SimpleQueue] */
    public boolean isEmpty() {
        return this.d.isEmpty();
    }

    public final boolean offer(R r2) {
        throw new UnsupportedOperationException("Should not be called!");
    }

    /*  JADX ERROR: StackOverflow in pass: MethodInvokeVisitor
        jadx.core.utils.exceptions.JadxOverflowException: 
        	at jadx.core.utils.ErrorsCounter.addError(ErrorsCounter.java:52)
        	at jadx.core.utils.ErrorsCounter.methodError(ErrorsCounter.java:86)
        */
    public final int b(int r3) {
        /*
            r2 = this;
            l.b.u.c.QueueDisposable<T> r0 = r2.d
            if (r0 == 0) goto L_0x0011
            r1 = r3 & 4
            if (r1 != 0) goto L_0x0011
            int r3 = r0.a(r3)
            if (r3 == 0) goto L_0x0010
            r2.f2678f = r3
        L_0x0010:
            return r3
        L_0x0011:
            r3 = 0
            return r3
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.u.d.BasicFuseableObserver.b(int):int");
    }

    public void a(Throwable th) {
        if (this.f2677e) {
            c.b(th);
            return;
        }
        this.f2677e = true;
        this.b.a(th);
    }

    public void a() {
        if (!this.f2677e) {
            this.f2677e = true;
            this.b.a();
        }
    }
}
