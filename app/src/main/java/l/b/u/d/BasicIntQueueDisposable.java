package l.b.u.d;

import java.util.concurrent.atomic.AtomicInteger;
import l.b.u.c.QueueDisposable;

public abstract class BasicIntQueueDisposable<T> extends AtomicInteger implements QueueDisposable<T> {
    public final boolean offer(T t2) {
        throw new UnsupportedOperationException("Should not be called");
    }
}
