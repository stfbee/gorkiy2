package l.b.u.d;

import java.util.concurrent.CountDownLatch;
import l.b.SingleObserver;
import l.b.c;
import l.b.s.Disposable;

public final class BlockingMultiObserver<T> extends CountDownLatch implements SingleObserver<T>, c {
    public T b;
    public Throwable c;
    public Disposable d;

    /* renamed from: e  reason: collision with root package name */
    public volatile boolean f2679e;

    public BlockingMultiObserver() {
        super(1);
    }

    public void a(Disposable disposable) {
        this.d = disposable;
        if (this.f2679e) {
            disposable.f();
        }
    }

    public void a(T t2) {
        this.b = t2;
        countDown();
    }

    public void a(Throwable th) {
        this.c = th;
        countDown();
    }

    public void a() {
        countDown();
    }
}
