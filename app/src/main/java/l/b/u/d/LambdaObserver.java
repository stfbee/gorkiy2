package l.b.u.d;

import io.reactivex.exceptions.CompositeException;
import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.a;
import l.b.u.a.DisposableHelper;

public final class LambdaObserver<T> extends AtomicReference<b> implements Observer<T>, b {
    public final Consumer<? super T> b;
    public final Consumer<? super Throwable> c;
    public final Action d;

    /* renamed from: e  reason: collision with root package name */
    public final Consumer<? super b> f2681e;

    public LambdaObserver(Consumer<? super T> consumer, Consumer<? super Throwable> consumer2, a aVar, Consumer<? super b> consumer3) {
        this.b = consumer;
        this.c = consumer2;
        this.d = aVar;
        this.f2681e = consumer3;
    }

    public void a(Disposable disposable) {
        if (DisposableHelper.b(super, disposable)) {
            try {
                this.f2681e.a(this);
            } catch (Throwable th) {
                c.c(th);
                disposable.f();
                a(th);
            }
        }
    }

    public void b(T t2) {
        if (!g()) {
            try {
                this.b.a(t2);
            } catch (Throwable th) {
                c.c(th);
                ((Disposable) get()).f();
                a(th);
            }
        }
    }

    public void f() {
        DisposableHelper.a(super);
    }

    public boolean g() {
        return get() == DisposableHelper.DISPOSED;
    }

    public void a(Throwable th) {
        if (!g()) {
            lazySet(DisposableHelper.DISPOSED);
            try {
                this.c.a(th);
            } catch (Throwable th2) {
                c.c(th2);
                c.b((Throwable) new CompositeException(th, th2));
            }
        } else {
            c.b(th);
        }
    }

    public void a() {
        if (!g()) {
            lazySet(DisposableHelper.DISPOSED);
            try {
                this.d.run();
            } catch (Throwable th) {
                c.c(th);
                c.b(th);
            }
        }
    }
}
