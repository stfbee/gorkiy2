package l.b.r.b;

import android.os.Handler;
import android.os.Looper;
import l.b.Scheduler;
import l.b.u.h.ExceptionHelper;

public final class AndroidSchedulers {
    public static final Scheduler a;

    public static final class a {
        public static final Scheduler a = new HandlerScheduler(new Handler(Looper.getMainLooper()), false);
    }

    static {
        try {
            Scheduler scheduler = a.a;
            if (scheduler != null) {
                a = scheduler;
                return;
            }
            throw new NullPointerException("Scheduler Callable returned null");
        } catch (Throwable th) {
            throw ExceptionHelper.a(th);
        }
    }

    public static Scheduler a() {
        Scheduler scheduler = a;
        if (scheduler != null) {
            return scheduler;
        }
        throw new NullPointerException("scheduler == null");
    }
}
