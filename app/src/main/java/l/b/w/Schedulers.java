package l.b.w;

import java.util.concurrent.Callable;
import l.b.Scheduler;
import l.b.l;
import l.b.u.b.ObjectHelper;
import l.b.u.g.ComputationScheduler;
import l.b.u.g.IoScheduler;
import l.b.u.g.NewThreadScheduler;
import l.b.u.g.SingleScheduler;
import l.b.u.g.TrampolineScheduler;

public final class Schedulers {
    public static final Scheduler a;
    public static final Scheduler b;

    public static final class a {
        public static final Scheduler a = new ComputationScheduler();
    }

    public static final class b implements Callable<l> {
        public Object call() {
            return a.a;
        }
    }

    public static final class c implements Callable<l> {
        public Object call() {
            return d.a;
        }
    }

    public static final class d {
        public static final Scheduler a = new IoScheduler();
    }

    public static final class e {
        public static final Scheduler a = new NewThreadScheduler();
    }

    public static final class f implements Callable<l> {
        public Object call() {
            return e.a;
        }
    }

    public static final class g {
        public static final Scheduler a = new SingleScheduler();
    }

    public static final class h implements Callable<l> {
        public Object call() {
            return g.a;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.w.Schedulers$h, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.w.Schedulers$b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.w.Schedulers$c, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.w.Schedulers$f, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    static {
        h hVar = new h();
        ObjectHelper.a((Object) hVar, "Scheduler Callable can't be null");
        j.c.a.a.c.n.c.a((Callable<l>) hVar);
        b bVar = new b();
        ObjectHelper.a((Object) bVar, "Scheduler Callable can't be null");
        a = j.c.a.a.c.n.c.a((Callable<l>) bVar);
        c cVar = new c();
        ObjectHelper.a((Object) cVar, "Scheduler Callable can't be null");
        b = j.c.a.a.c.n.c.a((Callable<l>) cVar);
        TrampolineScheduler trampolineScheduler = TrampolineScheduler.a;
        f fVar = new f();
        ObjectHelper.a((Object) fVar, "Scheduler Callable can't be null");
        j.c.a.a.c.n.c.a((Callable<l>) fVar);
    }
}
