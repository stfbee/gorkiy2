package l.b.x;

import j.c.a.a.c.n.c;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import l.b.Observer;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.u.b.ObjectHelper;

public final class PublishSubject<T> extends Subject<T> {
    public static final a[] d = new a[0];

    /* renamed from: e  reason: collision with root package name */
    public static final a[] f2783e = new a[0];
    public final AtomicReference<a<T>[]> b = new AtomicReference<>(f2783e);
    public Throwable c;

    public static final class a<T> extends AtomicBoolean implements b {
        public final Observer<? super T> b;
        public final PublishSubject<T> c;

        public a(Observer<? super T> observer, PublishSubject<T> publishSubject) {
            this.b = observer;
            this.c = publishSubject;
        }

        public void f() {
            if (compareAndSet(false, true)) {
                this.c.a(this);
            }
        }

        public boolean g() {
            return get();
        }
    }

    public void a(a aVar) {
        a[] aVarArr;
        a[] aVarArr2;
        do {
            aVarArr = (a[]) this.b.get();
            if (aVarArr != d && aVarArr != f2783e) {
                int length = aVarArr.length;
                int i2 = -1;
                int i3 = 0;
                while (true) {
                    if (i3 >= length) {
                        break;
                    } else if (aVarArr[i3] == aVar) {
                        i2 = i3;
                        break;
                    } else {
                        i3++;
                    }
                }
                if (i2 >= 0) {
                    if (length == 1) {
                        aVarArr2 = f2783e;
                    } else {
                        a[] aVarArr3 = new a[(length - 1)];
                        System.arraycopy(aVarArr, 0, aVarArr3, 0, i2);
                        System.arraycopy(aVarArr, i2 + 1, aVarArr3, i2, (length - i2) - 1);
                        aVarArr2 = aVarArr3;
                    }
                } else {
                    return;
                }
            } else {
                return;
            }
        } while (!this.b.compareAndSet(aVarArr, aVarArr2));
    }

    /* JADX DEBUG: Multi-variable search result rejected for TypeSearchVarInfo{r4v1, resolved type: l.b.x.PublishSubject$a[]} */
    /* JADX WARN: Type inference failed for: r0v0, types: [l.b.s.Disposable, l.b.x.PublishSubject$a, java.util.concurrent.atomic.AtomicBoolean] */
    /* JADX WARNING: Multi-variable type inference failed */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(l.b.Observer r6) {
        /*
            r5 = this;
            l.b.x.PublishSubject$a r0 = new l.b.x.PublishSubject$a
            r0.<init>(r6, r5)
            r6.a(r0)
        L_0x0008:
            java.util.concurrent.atomic.AtomicReference<l.b.x.PublishSubject$a<T>[]> r1 = r5.b
            java.lang.Object r1 = r1.get()
            l.b.x.PublishSubject$a[] r1 = (l.b.x.PublishSubject.a[]) r1
            l.b.x.PublishSubject$a[] r2 = l.b.x.PublishSubject.d
            r3 = 0
            if (r1 != r2) goto L_0x0016
            goto L_0x0029
        L_0x0016:
            int r2 = r1.length
            int r4 = r2 + 1
            l.b.x.PublishSubject$a[] r4 = new l.b.x.PublishSubject.a[r4]
            java.lang.System.arraycopy(r1, r3, r4, r3, r2)
            r4[r2] = r0
            java.util.concurrent.atomic.AtomicReference<l.b.x.PublishSubject$a<T>[]> r2 = r5.b
            boolean r1 = r2.compareAndSet(r1, r4)
            if (r1 == 0) goto L_0x0008
            r3 = 1
        L_0x0029:
            if (r3 == 0) goto L_0x0035
            boolean r6 = r0.get()
            if (r6 == 0) goto L_0x0040
            r5.a(r0)
            goto L_0x0040
        L_0x0035:
            java.lang.Throwable r0 = r5.c
            if (r0 == 0) goto L_0x003d
            r6.a(r0)
            goto L_0x0040
        L_0x003d:
            r6.a()
        L_0x0040:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: l.b.x.PublishSubject.b(l.b.Observer):void");
    }

    public void a(Disposable disposable) {
        if (this.b.get() == d) {
            disposable.f();
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Throwable, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void a(Throwable th) {
        ObjectHelper.a((Object) th, "onError called with null. Null values are generally not allowed in 2.x operators and sources.");
        a<T>[] aVarArr = this.b.get();
        a<T>[] aVarArr2 = d;
        if (aVarArr == aVarArr2) {
            c.b(th);
            return;
        }
        this.c = th;
        for (a aVar : (a[]) this.b.getAndSet(aVarArr2)) {
            if (aVar.get()) {
                c.b(th);
            } else {
                aVar.b.a(th);
            }
        }
    }

    public void b(Object obj) {
        ObjectHelper.a(obj, "onNext called with null. Null values are generally not allowed in 2.x operators and sources.");
        for (a aVar : (a[]) this.b.get()) {
            if (!aVar.get()) {
                aVar.b.b(obj);
            }
        }
    }

    public void a() {
        a<T>[] aVarArr = this.b.get();
        a<T>[] aVarArr2 = d;
        if (aVarArr != aVarArr2) {
            for (a aVar : (a[]) this.b.getAndSet(aVarArr2)) {
                if (!aVar.get()) {
                    aVar.b.a();
                }
            }
        }
    }
}
