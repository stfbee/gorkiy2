package com.google.android.material.chip;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.InsetDrawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Bundle;
import android.text.SpannableStringBuilder;
import android.text.TextPaint;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.PointerIcon;
import android.view.View;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import androidx.appcompat.widget.AppCompatCheckBox;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.h.j.BidiFormatter;
import i.h.j.TextDirectionHeuristicCompat;
import i.h.j.TextDirectionHeuristicsCompat;
import i.h.l.AccessibilityDelegateCompat;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.j.b.ExploreByTouchHelper;
import j.c.a.a.c.n.c;
import j.c.a.b.d0.TextAppearance;
import j.c.a.b.d0.TextAppearanceFontCallback;
import j.c.a.b.e0.RippleUtils;
import j.c.a.b.f;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.g0.Shapeable;
import j.c.a.b.j;
import j.c.a.b.k;
import j.c.a.b.m.MotionSpec;
import j.c.a.b.v.ChipDrawable;
import java.lang.ref.WeakReference;
import java.util.List;

public class Chip extends AppCompatCheckBox implements ChipDrawable.a, Shapeable {
    public static final int u = k.Widget_MaterialComponents_Chip_Action;
    public static final Rect v = new Rect();
    public static final int[] w = {16842913};
    public static final int[] x = {16842911};

    /* renamed from: e  reason: collision with root package name */
    public ChipDrawable f448e;

    /* renamed from: f  reason: collision with root package name */
    public InsetDrawable f449f;
    public RippleDrawable g;
    public View.OnClickListener h;

    /* renamed from: i  reason: collision with root package name */
    public CompoundButton.OnCheckedChangeListener f450i;

    /* renamed from: j  reason: collision with root package name */
    public boolean f451j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f452k;

    /* renamed from: l  reason: collision with root package name */
    public boolean f453l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f454m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f455n;

    /* renamed from: o  reason: collision with root package name */
    public int f456o;

    /* renamed from: p  reason: collision with root package name */
    public int f457p;

    /* renamed from: q  reason: collision with root package name */
    public final b f458q;

    /* renamed from: r  reason: collision with root package name */
    public final Rect f459r;

    /* renamed from: s  reason: collision with root package name */
    public final RectF f460s;

    /* renamed from: t  reason: collision with root package name */
    public final TextAppearanceFontCallback f461t;

    public class a extends TextAppearanceFontCallback {
        public a() {
        }

        public void a(int i2) {
        }

        public void a(Typeface typeface, boolean z) {
            CharSequence charSequence;
            Chip chip = Chip.this;
            ChipDrawable chipDrawable = chip.f448e;
            if (chipDrawable.F0) {
                charSequence = chipDrawable.G;
            } else {
                charSequence = chip.getText();
            }
            chip.setText(charSequence);
            Chip.this.requestLayout();
            Chip.this.invalidate();
        }
    }

    public Chip(Context context) {
        this(context, null);
    }

    /* access modifiers changed from: private */
    public RectF getCloseIconTouchBounds() {
        this.f460s.setEmpty();
        if (b()) {
            ChipDrawable chipDrawable = this.f448e;
            chipDrawable.c(chipDrawable.getBounds(), this.f460s);
        }
        return this.f460s;
    }

    /* access modifiers changed from: private */
    public Rect getCloseIconTouchBoundsInt() {
        RectF closeIconTouchBounds = getCloseIconTouchBounds();
        this.f459r.set((int) closeIconTouchBounds.left, (int) closeIconTouchBounds.top, (int) closeIconTouchBounds.right, (int) closeIconTouchBounds.bottom);
        return this.f459r;
    }

    private TextAppearance getTextAppearance() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.m0.f2193f;
        }
        return null;
    }

    private void setCloseIconHovered(boolean z) {
        if (this.f453l != z) {
            this.f453l = z;
            refreshDrawableState();
        }
    }

    private void setCloseIconPressed(boolean z) {
        if (this.f452k != z) {
            this.f452k = z;
            refreshDrawableState();
        }
    }

    public final void d() {
        if (this.f449f != null) {
            this.f449f = null;
            setMinWidth(0);
            setMinHeight((int) getChipMinHeight());
            f();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:40:0x00be, code lost:
        if (r1 != Integer.MIN_VALUE) goto L_0x00c0;
     */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x005d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean dispatchHoverEvent(android.view.MotionEvent r11) {
        /*
            r10 = this;
            java.lang.Class<i.j.b.ExploreByTouchHelper> r0 = i.j.b.ExploreByTouchHelper.class
            java.lang.String r1 = "Unable to send Accessibility Exit event"
            java.lang.String r2 = "Chip"
            int r3 = r11.getAction()
            r4 = -2147483648(0xffffffff80000000, float:-0.0)
            r5 = 10
            r6 = 0
            r7 = 1
            if (r3 != r5) goto L_0x005a
            java.lang.String r3 = "m"
            java.lang.reflect.Field r3 = r0.getDeclaredField(r3)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            r3.setAccessible(r7)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            com.google.android.material.chip.Chip$b r8 = r10.f458q     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            java.lang.Object r3 = r3.get(r8)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            java.lang.Integer r3 = (java.lang.Integer) r3     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            int r3 = r3.intValue()     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            if (r3 == r4) goto L_0x005a
            java.lang.String r3 = "f"
            java.lang.Class[] r8 = new java.lang.Class[r7]     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            java.lang.Class r9 = java.lang.Integer.TYPE     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            r8[r6] = r9     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            java.lang.reflect.Method r0 = r0.getDeclaredMethod(r3, r8)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            r0.setAccessible(r7)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            com.google.android.material.chip.Chip$b r3 = r10.f458q     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            java.lang.Object[] r8 = new java.lang.Object[r7]     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            java.lang.Integer r9 = java.lang.Integer.valueOf(r4)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            r8[r6] = r9     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            r0.invoke(r3, r8)     // Catch:{ NoSuchMethodException -> 0x0056, IllegalAccessException -> 0x0051, InvocationTargetException -> 0x004c, NoSuchFieldException -> 0x0047 }
            r0 = 1
            goto L_0x005b
        L_0x0047:
            r0 = move-exception
            android.util.Log.e(r2, r1, r0)
            goto L_0x005a
        L_0x004c:
            r0 = move-exception
            android.util.Log.e(r2, r1, r0)
            goto L_0x005a
        L_0x0051:
            r0 = move-exception
            android.util.Log.e(r2, r1, r0)
            goto L_0x005a
        L_0x0056:
            r0 = move-exception
            android.util.Log.e(r2, r1, r0)
        L_0x005a:
            r0 = 0
        L_0x005b:
            if (r0 != 0) goto L_0x00cb
            com.google.android.material.chip.Chip$b r0 = r10.f458q
            android.view.accessibility.AccessibilityManager r1 = r0.h
            boolean r1 = r1.isEnabled()
            if (r1 == 0) goto L_0x00c2
            android.view.accessibility.AccessibilityManager r1 = r0.h
            boolean r1 = r1.isTouchExplorationEnabled()
            if (r1 != 0) goto L_0x0070
            goto L_0x00c2
        L_0x0070:
            int r1 = r11.getAction()
            r2 = 7
            r3 = 256(0x100, float:3.59E-43)
            r8 = 128(0x80, float:1.794E-43)
            if (r1 == r2) goto L_0x0092
            r2 = 9
            if (r1 == r2) goto L_0x0092
            if (r1 == r5) goto L_0x0082
            goto L_0x00c2
        L_0x0082:
            int r1 = r0.f1243m
            if (r1 == r4) goto L_0x00c2
            if (r1 != r4) goto L_0x0089
            goto L_0x00c0
        L_0x0089:
            r0.f1243m = r4
            r0.a(r4, r8)
            r0.a(r1, r3)
            goto L_0x00c0
        L_0x0092:
            float r1 = r11.getX()
            float r2 = r11.getY()
            com.google.android.material.chip.Chip r5 = com.google.android.material.chip.Chip.this
            boolean r5 = r5.b()
            if (r5 == 0) goto L_0x00b0
            com.google.android.material.chip.Chip r5 = com.google.android.material.chip.Chip.this
            android.graphics.RectF r5 = r5.getCloseIconTouchBounds()
            boolean r1 = r5.contains(r1, r2)
            if (r1 == 0) goto L_0x00b0
            r1 = 1
            goto L_0x00b1
        L_0x00b0:
            r1 = 0
        L_0x00b1:
            int r2 = r0.f1243m
            if (r2 != r1) goto L_0x00b6
            goto L_0x00be
        L_0x00b6:
            r0.f1243m = r1
            r0.a(r1, r8)
            r0.a(r2, r3)
        L_0x00be:
            if (r1 == r4) goto L_0x00c2
        L_0x00c0:
            r0 = 1
            goto L_0x00c3
        L_0x00c2:
            r0 = 0
        L_0x00c3:
            if (r0 != 0) goto L_0x00cb
            boolean r11 = super.dispatchHoverEvent(r11)
            if (r11 == 0) goto L_0x00cc
        L_0x00cb:
            r6 = 1
        L_0x00cc:
            return r6
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.dispatchHoverEvent(android.view.MotionEvent):boolean");
    }

    public boolean dispatchKeyEvent(KeyEvent keyEvent) {
        b bVar = this.f458q;
        if (bVar != null) {
            int i2 = 0;
            if (keyEvent.getAction() != 1) {
                int keyCode = keyEvent.getKeyCode();
                if (keyCode != 61) {
                    int i3 = 66;
                    if (keyCode != 66) {
                        switch (keyCode) {
                            case 19:
                            case 20:
                            case 21:
                            case 22:
                                if (keyEvent.hasNoModifiers()) {
                                    if (keyCode == 19) {
                                        i3 = 33;
                                    } else if (keyCode == 21) {
                                        i3 = 17;
                                    } else if (keyCode != 22) {
                                        i3 = 130;
                                    }
                                    int repeatCount = keyEvent.getRepeatCount() + 1;
                                    int i4 = 0;
                                    while (i2 < repeatCount && bVar.a(i3, (Rect) null)) {
                                        i2++;
                                        i4 = 1;
                                    }
                                    i2 = i4;
                                    break;
                                }
                                break;
                        }
                    }
                    if (keyEvent.hasNoModifiers() && keyEvent.getRepeatCount() == 0) {
                        int i5 = bVar.f1242l;
                        if (i5 != Integer.MIN_VALUE) {
                            bVar.a(i5, 16, (Bundle) null);
                        }
                        i2 = 1;
                    }
                } else if (keyEvent.hasNoModifiers()) {
                    i2 = bVar.a(2, (Rect) null);
                } else if (keyEvent.hasModifiers(1)) {
                    i2 = bVar.a(1, (Rect) null);
                }
            }
            if (i2 == 0 || this.f458q.f1242l == Integer.MIN_VALUE) {
                return super.dispatchKeyEvent(keyEvent);
            }
            return true;
        }
        throw null;
    }

    public void drawableStateChanged() {
        super.drawableStateChanged();
        ChipDrawable chipDrawable = this.f448e;
        int i2 = 0;
        if (chipDrawable != null && ChipDrawable.f(chipDrawable.N)) {
            ChipDrawable chipDrawable2 = this.f448e;
            int isEnabled = isEnabled();
            if (this.f454m) {
                isEnabled++;
            }
            if (this.f453l) {
                isEnabled++;
            }
            if (this.f452k) {
                isEnabled++;
            }
            if (isChecked()) {
                isEnabled++;
            }
            int[] iArr = new int[isEnabled];
            if (isEnabled()) {
                iArr[0] = 16842910;
                i2 = 1;
            }
            if (this.f454m) {
                iArr[i2] = 16842908;
                i2++;
            }
            if (this.f453l) {
                iArr[i2] = 16843623;
                i2++;
            }
            if (this.f452k) {
                iArr[i2] = 16842919;
                i2++;
            }
            if (isChecked()) {
                iArr[i2] = 16842913;
            }
            i2 = chipDrawable2.b(iArr);
        }
        if (i2 != 0) {
            invalidate();
        }
    }

    public final void e() {
        if (b()) {
            ChipDrawable chipDrawable = this.f448e;
            if ((chipDrawable != null && chipDrawable.M) && this.h != null) {
                ViewCompat.a(this, this.f458q);
                return;
            }
        }
        ViewCompat.a(this, (AccessibilityDelegateCompat) null);
    }

    public final void f() {
        if (RippleUtils.a) {
            g();
            return;
        }
        this.f448e.e(true);
        ViewCompat.a(this, getBackgroundDrawable());
        h();
        if (getBackgroundDrawable() == this.f449f && this.f448e.getCallback() == null) {
            this.f448e.setCallback(this.f449f);
        }
    }

    public final void g() {
        this.g = new RippleDrawable(RippleUtils.a(this.f448e.F), getBackgroundDrawable(), null);
        this.f448e.e(false);
        ViewCompat.a(this, this.g);
        h();
    }

    public Drawable getBackgroundDrawable() {
        InsetDrawable insetDrawable = this.f449f;
        return insetDrawable == null ? this.f448e : insetDrawable;
    }

    public Drawable getCheckedIcon() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.U;
        }
        return null;
    }

    public ColorStateList getChipBackgroundColor() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.A;
        }
        return null;
    }

    public float getChipCornerRadius() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.m();
        }
        return 0.0f;
    }

    public Drawable getChipDrawable() {
        return this.f448e;
    }

    public float getChipEndPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.e0;
        }
        return 0.0f;
    }

    public Drawable getChipIcon() {
        Drawable drawable;
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable == null || (drawable = chipDrawable.I) == null) {
            return null;
        }
        return ResourcesFlusher.c(drawable);
    }

    public float getChipIconSize() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.K;
        }
        return 0.0f;
    }

    public ColorStateList getChipIconTint() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.J;
        }
        return null;
    }

    public float getChipMinHeight() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.B;
        }
        return 0.0f;
    }

    public float getChipStartPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.X;
        }
        return 0.0f;
    }

    public ColorStateList getChipStrokeColor() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.D;
        }
        return null;
    }

    public float getChipStrokeWidth() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.E;
        }
        return 0.0f;
    }

    @Deprecated
    public CharSequence getChipText() {
        return getText();
    }

    public Drawable getCloseIcon() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.n();
        }
        return null;
    }

    public CharSequence getCloseIconContentDescription() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.R;
        }
        return null;
    }

    public float getCloseIconEndPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.d0;
        }
        return 0.0f;
    }

    public float getCloseIconSize() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.Q;
        }
        return 0.0f;
    }

    public float getCloseIconStartPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.c0;
        }
        return 0.0f;
    }

    public ColorStateList getCloseIconTint() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.P;
        }
        return null;
    }

    public TextUtils.TruncateAt getEllipsize() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.E0;
        }
        return null;
    }

    public void getFocusedRect(Rect rect) {
        b bVar = this.f458q;
        if (bVar.f1242l == 1 || bVar.f1241k == 1) {
            rect.set(getCloseIconTouchBoundsInt());
        } else {
            super.getFocusedRect(rect);
        }
    }

    public MotionSpec getHideMotionSpec() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.W;
        }
        return null;
    }

    public float getIconEndPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.Z;
        }
        return 0.0f;
    }

    public float getIconStartPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.Y;
        }
        return 0.0f;
    }

    public ColorStateList getRippleColor() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.F;
        }
        return null;
    }

    public ShapeAppearanceModel getShapeAppearanceModel() {
        return this.f448e.b.a;
    }

    public MotionSpec getShowMotionSpec() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.V;
        }
        return null;
    }

    public float getTextEndPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.b0;
        }
        return 0.0f;
    }

    public float getTextStartPadding() {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            return chipDrawable.a0;
        }
        return 0.0f;
    }

    public final void h() {
        ChipDrawable chipDrawable;
        if (!TextUtils.isEmpty(getText()) && (chipDrawable = this.f448e) != null) {
            int l2 = (int) (chipDrawable.l() + chipDrawable.e0 + chipDrawable.b0);
            ChipDrawable chipDrawable2 = this.f448e;
            int k2 = (int) (chipDrawable2.k() + chipDrawable2.X + chipDrawable2.a0);
            if (this.f449f != null) {
                Rect rect = new Rect();
                this.f449f.getPadding(rect);
                k2 += rect.left;
                l2 += rect.right;
            }
            ViewCompat.a(this, k2, getPaddingTop(), l2, getPaddingBottom());
        }
    }

    public final void i() {
        TextPaint paint = getPaint();
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            paint.drawableState = chipDrawable.getState();
        }
        TextAppearance textAppearance = getTextAppearance();
        if (textAppearance != null) {
            textAppearance.a(getContext(), paint, this.f461t);
        }
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        c.a(this, this.f448e);
    }

    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 2);
        if (isChecked()) {
            CheckBox.mergeDrawableStates(onCreateDrawableState, w);
        }
        if (c()) {
            CheckBox.mergeDrawableStates(onCreateDrawableState, x);
        }
        return onCreateDrawableState;
    }

    public void onFocusChanged(boolean z, int i2, Rect rect) {
        super.onFocusChanged(z, i2, rect);
        b bVar = this.f458q;
        int i3 = bVar.f1242l;
        if (i3 != Integer.MIN_VALUE) {
            bVar.b(i3);
        }
        if (z) {
            bVar.a(i2, rect);
        }
    }

    public boolean onHoverEvent(MotionEvent motionEvent) {
        int actionMasked = motionEvent.getActionMasked();
        if (actionMasked == 7) {
            setCloseIconHovered(getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()));
        } else if (actionMasked == 10) {
            setCloseIconHovered(false);
        }
        return super.onHoverEvent(motionEvent);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        int i2;
        int i3;
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        if (c() || isClickable()) {
            accessibilityNodeInfo.setClassName(c() ? "android.widget.CompoundButton" : "android.widget.Button");
        } else {
            accessibilityNodeInfo.setClassName("android.view.View");
        }
        accessibilityNodeInfo.setCheckable(c());
        accessibilityNodeInfo.setClickable(isClickable());
        if (getParent() instanceof ChipGroup) {
            ChipGroup chipGroup = (ChipGroup) getParent();
            AccessibilityNodeInfoCompat accessibilityNodeInfoCompat = new AccessibilityNodeInfoCompat(accessibilityNodeInfo);
            if (chipGroup.d) {
                int i4 = 0;
                int i5 = 0;
                while (true) {
                    if (i4 >= chipGroup.getChildCount()) {
                        i5 = -1;
                        break;
                    }
                    if (chipGroup.getChildAt(i4) instanceof Chip) {
                        if (((Chip) chipGroup.getChildAt(i4)) == this) {
                            break;
                        }
                        i5++;
                    }
                    i4++;
                }
                i2 = i5;
            } else {
                i2 = -1;
            }
            Object tag = getTag(f.row_index_key);
            if (!(tag instanceof Integer)) {
                i3 = -1;
            } else {
                i3 = ((Integer) tag).intValue();
            }
            accessibilityNodeInfoCompat.b(AccessibilityNodeInfoCompat.c.a(i3, 1, i2, 1, false, isChecked()));
        }
    }

    @TargetApi(24)
    public PointerIcon onResolvePointerIcon(MotionEvent motionEvent, int i2) {
        if (!getCloseIconTouchBounds().contains(motionEvent.getX(), motionEvent.getY()) || !isEnabled()) {
            return null;
        }
        return PointerIcon.getSystemIcon(getContext(), 1002);
    }

    @TargetApi(17)
    public void onRtlPropertiesChanged(int i2) {
        super.onRtlPropertiesChanged(i2);
        if (this.f456o != i2) {
            this.f456o = i2;
            h();
        }
    }

    /* JADX WARNING: Code restructure failed: missing block: B:6:0x001e, code lost:
        if (r0 != 3) goto L_0x004c;
     */
    /* JADX WARNING: Removed duplicated region for block: B:24:0x004f  */
    @android.annotation.SuppressLint({"ClickableViewAccessibility"})
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public boolean onTouchEvent(android.view.MotionEvent r6) {
        /*
            r5 = this;
            int r0 = r6.getActionMasked()
            android.graphics.RectF r1 = r5.getCloseIconTouchBounds()
            float r2 = r6.getX()
            float r3 = r6.getY()
            boolean r1 = r1.contains(r2, r3)
            r2 = 0
            r3 = 1
            if (r0 == 0) goto L_0x0045
            if (r0 == r3) goto L_0x002b
            r4 = 2
            if (r0 == r4) goto L_0x0021
            r1 = 3
            if (r0 == r1) goto L_0x0040
            goto L_0x004c
        L_0x0021:
            boolean r0 = r5.f452k
            if (r0 == 0) goto L_0x004c
            if (r1 != 0) goto L_0x004a
            r5.setCloseIconPressed(r2)
            goto L_0x004a
        L_0x002b:
            boolean r0 = r5.f452k
            if (r0 == 0) goto L_0x0040
            r5.playSoundEffect(r2)
            android.view.View$OnClickListener r0 = r5.h
            if (r0 == 0) goto L_0x0039
            r0.onClick(r5)
        L_0x0039:
            com.google.android.material.chip.Chip$b r0 = r5.f458q
            r0.a(r3, r3)
            r0 = 1
            goto L_0x0041
        L_0x0040:
            r0 = 0
        L_0x0041:
            r5.setCloseIconPressed(r2)
            goto L_0x004d
        L_0x0045:
            if (r1 == 0) goto L_0x004c
            r5.setCloseIconPressed(r3)
        L_0x004a:
            r0 = 1
            goto L_0x004d
        L_0x004c:
            r0 = 0
        L_0x004d:
            if (r0 != 0) goto L_0x0055
            boolean r6 = super.onTouchEvent(r6)
            if (r6 == 0) goto L_0x0056
        L_0x0055:
            r2 = 1
        L_0x0056:
            return r2
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.onTouchEvent(android.view.MotionEvent):boolean");
    }

    public void setBackground(Drawable drawable) {
        if (drawable == getBackgroundDrawable() || drawable == this.g) {
            super.setBackground(drawable);
        } else {
            Log.w("Chip", "Do not set the background; Chip manages its own background drawable.");
        }
    }

    public void setBackgroundColor(int i2) {
        Log.w("Chip", "Do not set the background color; Chip manages its own background drawable.");
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (drawable == getBackgroundDrawable() || drawable == this.g) {
            super.setBackgroundDrawable(drawable);
        } else {
            Log.w("Chip", "Do not set the background drawable; Chip manages its own background drawable.");
        }
    }

    public void setBackgroundResource(int i2) {
        Log.w("Chip", "Do not set the background resource; Chip manages its own background drawable.");
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        Log.w("Chip", "Do not set the background tint list; Chip manages its own background drawable.");
    }

    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        Log.w("Chip", "Do not set the background tint mode; Chip manages its own background drawable.");
    }

    public void setCheckable(boolean z) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.a(z);
        }
    }

    public void setCheckableResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.a(chipDrawable.f0.getResources().getBoolean(i2));
        }
    }

    public void setChecked(boolean z) {
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener;
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable == null) {
            this.f451j = z;
        } else if (chipDrawable.S) {
            boolean isChecked = isChecked();
            super.setChecked(z);
            if (isChecked != z && (onCheckedChangeListener = this.f450i) != null) {
                onCheckedChangeListener.onCheckedChanged(this, z);
            }
        }
    }

    public void setCheckedIcon(Drawable drawable) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.b(drawable);
        }
    }

    @Deprecated
    public void setCheckedIconEnabled(boolean z) {
        setCheckedIconVisible(z);
    }

    @Deprecated
    public void setCheckedIconEnabledResource(int i2) {
        setCheckedIconVisible(i2);
    }

    public void setCheckedIconResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.b(AppCompatResources.c(chipDrawable.f0, i2));
        }
    }

    public void setCheckedIconVisible(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.b(chipDrawable.f0.getResources().getBoolean(i2));
        }
    }

    public void setChipBackgroundColor(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.A != colorStateList) {
            chipDrawable.A = colorStateList;
            chipDrawable.onStateChange(chipDrawable.getState());
        }
    }

    public void setChipBackgroundColorResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(AppCompatResources.b(chipDrawable.f0, i2));
        }
    }

    @Deprecated
    public void setChipCornerRadius(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(f2);
        }
    }

    @Deprecated
    public void setChipCornerRadiusResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setChipDrawable(ChipDrawable chipDrawable) {
        ChipDrawable chipDrawable2 = this.f448e;
        if (chipDrawable2 != chipDrawable) {
            if (chipDrawable2 != null) {
                chipDrawable2.D0 = new WeakReference<>(null);
            }
            this.f448e = chipDrawable;
            chipDrawable.F0 = false;
            if (chipDrawable != null) {
                chipDrawable.D0 = new WeakReference<>(this);
                a(this.f457p);
                return;
            }
            throw null;
        }
    }

    public void setChipEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.e0 != f2) {
            chipDrawable.e0 = f2;
            chipDrawable.invalidateSelf();
            chipDrawable.o();
        }
    }

    public void setChipEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.d(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setChipIcon(Drawable drawable) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(drawable);
        }
    }

    @Deprecated
    public void setChipIconEnabled(boolean z) {
        setChipIconVisible(z);
    }

    @Deprecated
    public void setChipIconEnabledResource(int i2) {
        setChipIconVisible(i2);
    }

    public void setChipIconResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(AppCompatResources.c(chipDrawable.f0, i2));
        }
    }

    public void setChipIconSize(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.e(f2);
        }
    }

    public void setChipIconSizeResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.e(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setChipIconTint(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.d(colorStateList);
        }
    }

    public void setChipIconTintResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.d(AppCompatResources.b(chipDrawable.f0, i2));
        }
    }

    public void setChipIconVisible(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(chipDrawable.f0.getResources().getBoolean(i2));
        }
    }

    public void setChipMinHeight(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.B != f2) {
            chipDrawable.B = f2;
            chipDrawable.invalidateSelf();
            chipDrawable.o();
        }
    }

    public void setChipMinHeightResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.f(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setChipStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.X != f2) {
            chipDrawable.X = f2;
            chipDrawable.invalidateSelf();
            chipDrawable.o();
        }
    }

    public void setChipStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.g(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setChipStrokeColor(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.e(colorStateList);
        }
    }

    public void setChipStrokeColorResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.e(AppCompatResources.b(chipDrawable.f0, i2));
        }
    }

    public void setChipStrokeWidth(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.h(f2);
        }
    }

    public void setChipStrokeWidthResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.h(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    @Deprecated
    public void setChipText(CharSequence charSequence) {
        setText(charSequence);
    }

    @Deprecated
    public void setChipTextResource(int i2) {
        setText(getResources().getString(i2));
    }

    public void setCloseIcon(Drawable drawable) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.d(drawable);
        }
        e();
    }

    public void setCloseIconContentDescription(CharSequence charSequence) {
        SpannableStringBuilder spannableStringBuilder;
        String str;
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.R != charSequence) {
            BidiFormatter a2 = BidiFormatter.a();
            TextDirectionHeuristicCompat textDirectionHeuristicCompat = a2.c;
            if (charSequence == null) {
                spannableStringBuilder = null;
            } else {
                boolean a3 = ((TextDirectionHeuristicsCompat.d) textDirectionHeuristicCompat).a(charSequence, 0, charSequence.length());
                SpannableStringBuilder spannableStringBuilder2 = new SpannableStringBuilder();
                String str2 = "";
                if ((a2.b & 2) != 0) {
                    boolean a4 = ((TextDirectionHeuristicsCompat.d) (a3 ? TextDirectionHeuristicsCompat.b : TextDirectionHeuristicsCompat.a)).a(charSequence, 0, charSequence.length());
                    if (a2.a || (!a4 && BidiFormatter.a(charSequence) != 1)) {
                        str = (!a2.a || (a4 && BidiFormatter.a(charSequence) != -1)) ? str2 : BidiFormatter.f1198f;
                    } else {
                        str = BidiFormatter.f1197e;
                    }
                    spannableStringBuilder2.append((CharSequence) str);
                }
                if (a3 != a2.a) {
                    spannableStringBuilder2.append(a3 ? (char) 8235 : 8234);
                    spannableStringBuilder2.append(charSequence);
                    spannableStringBuilder2.append(8236);
                } else {
                    spannableStringBuilder2.append(charSequence);
                }
                boolean a5 = ((TextDirectionHeuristicsCompat.d) (a3 ? TextDirectionHeuristicsCompat.b : TextDirectionHeuristicsCompat.a)).a(charSequence, 0, charSequence.length());
                if (!a2.a && (a5 || BidiFormatter.b(charSequence) == 1)) {
                    str2 = BidiFormatter.f1197e;
                } else if (a2.a && (!a5 || BidiFormatter.b(charSequence) == -1)) {
                    str2 = BidiFormatter.f1198f;
                }
                spannableStringBuilder2.append((CharSequence) str2);
                spannableStringBuilder = spannableStringBuilder2;
            }
            chipDrawable.R = spannableStringBuilder;
            chipDrawable.invalidateSelf();
        }
    }

    @Deprecated
    public void setCloseIconEnabled(boolean z) {
        setCloseIconVisible(z);
    }

    @Deprecated
    public void setCloseIconEnabledResource(int i2) {
        setCloseIconVisible(i2);
    }

    public void setCloseIconEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.i(f2);
        }
    }

    public void setCloseIconEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.i(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setCloseIconResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.d(AppCompatResources.c(chipDrawable.f0, i2));
        }
        e();
    }

    public void setCloseIconSize(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.j(f2);
        }
    }

    public void setCloseIconSizeResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.j(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setCloseIconStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.k(f2);
        }
    }

    public void setCloseIconStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.k(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setCloseIconTint(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.f(colorStateList);
        }
    }

    public void setCloseIconTintResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.f(AppCompatResources.b(chipDrawable.f0, i2));
        }
    }

    public void setCloseIconVisible(int i2) {
        setCloseIconVisible(getResources().getBoolean(i2));
    }

    public void setCompoundDrawables(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawables(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesRelative(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesRelative(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(int i2, int i3, int i4, int i5) {
        if (i2 != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (i4 == 0) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(i2, i3, i4, i5);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(int i2, int i3, int i4, int i5) {
        if (i2 != 0) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (i4 == 0) {
            super.setCompoundDrawablesWithIntrinsicBounds(i2, i3, i4, i5);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setElevation(float f2) {
        super.setElevation(f2);
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            MaterialShapeDrawable.b bVar = chipDrawable.b;
            if (bVar.f2236o != f2) {
                bVar.f2236o = f2;
                chipDrawable.j();
            }
        }
    }

    public void setEllipsize(TextUtils.TruncateAt truncateAt) {
        if (this.f448e != null) {
            if (truncateAt != TextUtils.TruncateAt.MARQUEE) {
                super.setEllipsize(truncateAt);
                ChipDrawable chipDrawable = this.f448e;
                if (chipDrawable != null) {
                    chipDrawable.E0 = truncateAt;
                    return;
                }
                return;
            }
            throw new UnsupportedOperationException("Text within a chip are not allowed to scroll.");
        }
    }

    public void setEnsureMinTouchTargetSize(boolean z) {
        this.f455n = z;
        a(this.f457p);
    }

    public void setGravity(int i2) {
        if (i2 != 8388627) {
            Log.w("Chip", "Chip text must be vertically center and start aligned");
        } else {
            super.setGravity(i2);
        }
    }

    public void setHideMotionSpec(MotionSpec motionSpec) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.W = motionSpec;
        }
    }

    public void setHideMotionSpecResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.W = MotionSpec.a(chipDrawable.f0, i2);
        }
    }

    public void setIconEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.l(f2);
        }
    }

    public void setIconEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.l(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setIconStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.m(f2);
        }
    }

    public void setIconStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.m(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setLayoutDirection(int i2) {
        if (this.f448e != null) {
            super.setLayoutDirection(i2);
        }
    }

    public void setLines(int i2) {
        if (i2 <= 1) {
            super.setLines(i2);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setMaxLines(int i2) {
        if (i2 <= 1) {
            super.setMaxLines(i2);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setMaxWidth(int i2) {
        super.setMaxWidth(i2);
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.G0 = i2;
        }
    }

    public void setMinLines(int i2) {
        if (i2 <= 1) {
            super.setMinLines(i2);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setOnCheckedChangeListenerInternal(CompoundButton.OnCheckedChangeListener onCheckedChangeListener) {
        this.f450i = onCheckedChangeListener;
    }

    public void setOnCloseIconClickListener(View.OnClickListener onClickListener) {
        this.h = onClickListener;
        e();
    }

    public void setRippleColor(ColorStateList colorStateList) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.g(colorStateList);
        }
        if (!this.f448e.B0) {
            g();
        }
    }

    public void setRippleColorResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.g(AppCompatResources.b(chipDrawable.f0, i2));
            if (!this.f448e.B0) {
                g();
            }
        }
    }

    public void setShapeAppearanceModel(ShapeAppearanceModel shapeAppearanceModel) {
        ChipDrawable chipDrawable = this.f448e;
        chipDrawable.b.a = shapeAppearanceModel;
        chipDrawable.invalidateSelf();
    }

    public void setShowMotionSpec(MotionSpec motionSpec) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.V = motionSpec;
        }
    }

    public void setShowMotionSpecResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.V = MotionSpec.a(chipDrawable.f0, i2);
        }
    }

    public void setSingleLine(boolean z) {
        if (z) {
            super.setSingleLine(z);
            return;
        }
        throw new UnsupportedOperationException("Chip does not support multi-line text");
    }

    public void setText(CharSequence charSequence, TextView.BufferType bufferType) {
        if (this.f448e != null) {
            if (charSequence == null) {
                charSequence = "";
            }
            super.setText(this.f448e.F0 ? null : charSequence, bufferType);
            ChipDrawable chipDrawable = this.f448e;
            if (chipDrawable != null) {
                chipDrawable.a(charSequence);
            }
        }
    }

    public void setTextAppearance(TextAppearance textAppearance) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.m0.a(textAppearance, chipDrawable.f0);
        }
        i();
    }

    public void setTextAppearanceResource(int i2) {
        setTextAppearance(getContext(), i2);
    }

    public void setTextEndPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.b0 != f2) {
            chipDrawable.b0 = f2;
            chipDrawable.invalidateSelf();
            chipDrawable.o();
        }
    }

    public void setTextEndPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.n(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public void setTextStartPadding(float f2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null && chipDrawable.a0 != f2) {
            chipDrawable.a0 = f2;
            chipDrawable.invalidateSelf();
            chipDrawable.o();
        }
    }

    public void setTextStartPaddingResource(int i2) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.o(chipDrawable.f0.getResources().getDimension(i2));
        }
    }

    public Chip(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, j.c.a.b.b.chipStyle);
    }

    public void a() {
        a(this.f457p);
        requestLayout();
        invalidateOutline();
    }

    public final boolean b() {
        ChipDrawable chipDrawable = this.f448e;
        return (chipDrawable == null || chipDrawable.n() == null) ? false : true;
    }

    public boolean c() {
        ChipDrawable chipDrawable = this.f448e;
        return chipDrawable != null && chipDrawable.S;
    }

    public void setCloseIconVisible(boolean z) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.d(z);
        }
        e();
    }

    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public Chip(android.content.Context r15, android.util.AttributeSet r16, int r17) {
        /*
            r14 = this;
            r0 = r14
            r7 = r16
            r8 = r17
            int r1 = com.google.android.material.chip.Chip.u
            r2 = r15
            android.content.Context r1 = j.c.a.b.m0.a.MaterialThemeOverlay.a(r15, r7, r8, r1)
            r14.<init>(r1, r7, r8)
            android.graphics.Rect r1 = new android.graphics.Rect
            r1.<init>()
            r0.f459r = r1
            android.graphics.RectF r1 = new android.graphics.RectF
            r1.<init>()
            r0.f460s = r1
            com.google.android.material.chip.Chip$a r1 = new com.google.android.material.chip.Chip$a
            r1.<init>()
            r0.f461t = r1
            android.content.Context r9 = r14.getContext()
            r10 = 8388627(0x800013, float:1.175497E-38)
            r11 = 1
            if (r7 != 0) goto L_0x002f
            goto L_0x008f
        L_0x002f:
            java.lang.String r1 = "http://schemas.android.com/apk/res/android"
            java.lang.String r2 = "background"
            java.lang.String r2 = r7.getAttributeValue(r1, r2)
            java.lang.String r3 = "Chip"
            if (r2 == 0) goto L_0x0040
            java.lang.String r2 = "Do not set the background; Chip manages its own background drawable."
            android.util.Log.w(r3, r2)
        L_0x0040:
            java.lang.String r2 = "drawableLeft"
            java.lang.String r2 = r7.getAttributeValue(r1, r2)
            if (r2 != 0) goto L_0x0347
            java.lang.String r2 = "drawableStart"
            java.lang.String r2 = r7.getAttributeValue(r1, r2)
            if (r2 != 0) goto L_0x033f
            java.lang.String r2 = "drawableEnd"
            java.lang.String r2 = r7.getAttributeValue(r1, r2)
            java.lang.String r4 = "Please set end drawable using R.attr#closeIcon."
            if (r2 != 0) goto L_0x0339
            java.lang.String r2 = "drawableRight"
            java.lang.String r2 = r7.getAttributeValue(r1, r2)
            if (r2 != 0) goto L_0x0333
            java.lang.String r2 = "singleLine"
            boolean r2 = r7.getAttributeBooleanValue(r1, r2, r11)
            if (r2 == 0) goto L_0x032b
            java.lang.String r2 = "lines"
            int r2 = r7.getAttributeIntValue(r1, r2, r11)
            if (r2 != r11) goto L_0x032b
            java.lang.String r2 = "minLines"
            int r2 = r7.getAttributeIntValue(r1, r2, r11)
            if (r2 != r11) goto L_0x032b
            java.lang.String r2 = "maxLines"
            int r2 = r7.getAttributeIntValue(r1, r2, r11)
            if (r2 != r11) goto L_0x032b
            java.lang.String r2 = "gravity"
            int r1 = r7.getAttributeIntValue(r1, r2, r10)
            if (r1 == r10) goto L_0x008f
            java.lang.String r1 = "Chip text must be vertically center and start aligned"
            android.util.Log.w(r3, r1)
        L_0x008f:
            int r5 = com.google.android.material.chip.Chip.u
            j.c.a.b.v.ChipDrawable r12 = new j.c.a.b.v.ChipDrawable
            r12.<init>(r9, r7, r8, r5)
            android.content.Context r1 = r12.f0
            int[] r3 = j.c.a.b.l.Chip
            r13 = 0
            int[] r6 = new int[r13]
            r2 = r16
            r4 = r17
            android.content.res.TypedArray r1 = j.c.a.b.b0.ThemeEnforcement.b(r1, r2, r3, r4, r5, r6)
            int r2 = j.c.a.b.l.Chip_shapeAppearance
            boolean r2 = r1.hasValue(r2)
            r12.H0 = r2
            android.content.Context r2 = r12.f0
            int r3 = j.c.a.b.l.Chip_chipSurfaceColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r3)
            android.content.res.ColorStateList r3 = r12.z
            if (r3 == r2) goto L_0x00c2
            r12.z = r2
            int[] r2 = r12.getState()
            r12.onStateChange(r2)
        L_0x00c2:
            android.content.Context r2 = r12.f0
            int r3 = j.c.a.b.l.Chip_chipBackgroundColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r3)
            r12.c(r2)
            int r2 = j.c.a.b.l.Chip_chipMinHeight
            r3 = 0
            float r2 = r1.getDimension(r2, r3)
            r12.f(r2)
            int r2 = j.c.a.b.l.Chip_chipCornerRadius
            boolean r2 = r1.hasValue(r2)
            if (r2 == 0) goto L_0x00e8
            int r2 = j.c.a.b.l.Chip_chipCornerRadius
            float r2 = r1.getDimension(r2, r3)
            r12.c(r2)
        L_0x00e8:
            android.content.Context r2 = r12.f0
            int r4 = j.c.a.b.l.Chip_chipStrokeColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r4)
            r12.e(r2)
            int r2 = j.c.a.b.l.Chip_chipStrokeWidth
            float r2 = r1.getDimension(r2, r3)
            r12.h(r2)
            android.content.Context r2 = r12.f0
            int r4 = j.c.a.b.l.Chip_rippleColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r4)
            r12.g(r2)
            int r2 = j.c.a.b.l.Chip_android_text
            java.lang.CharSequence r2 = r1.getText(r2)
            r12.a(r2)
            android.content.Context r2 = r12.f0
            int r4 = j.c.a.b.l.Chip_android_textAppearance
            boolean r5 = r1.hasValue(r4)
            if (r5 == 0) goto L_0x0126
            int r4 = r1.getResourceId(r4, r13)
            if (r4 == 0) goto L_0x0126
            j.c.a.b.d0.TextAppearance r5 = new j.c.a.b.d0.TextAppearance
            r5.<init>(r2, r4)
            goto L_0x0127
        L_0x0126:
            r5 = 0
        L_0x0127:
            r12.a(r5)
            int r2 = j.c.a.b.l.Chip_android_ellipsize
            int r2 = r1.getInt(r2, r13)
            if (r2 == r11) goto L_0x0143
            r4 = 2
            if (r2 == r4) goto L_0x013e
            r4 = 3
            if (r2 == r4) goto L_0x0139
            goto L_0x0147
        L_0x0139:
            android.text.TextUtils$TruncateAt r2 = android.text.TextUtils.TruncateAt.END
            r12.E0 = r2
            goto L_0x0147
        L_0x013e:
            android.text.TextUtils$TruncateAt r2 = android.text.TextUtils.TruncateAt.MIDDLE
            r12.E0 = r2
            goto L_0x0147
        L_0x0143:
            android.text.TextUtils$TruncateAt r2 = android.text.TextUtils.TruncateAt.START
            r12.E0 = r2
        L_0x0147:
            int r2 = j.c.a.b.l.Chip_chipIconVisible
            boolean r2 = r1.getBoolean(r2, r13)
            r12.c(r2)
            java.lang.String r2 = "http://schemas.android.com/apk/res-auto"
            if (r7 == 0) goto L_0x016d
            java.lang.String r4 = "chipIconEnabled"
            java.lang.String r4 = r7.getAttributeValue(r2, r4)
            if (r4 == 0) goto L_0x016d
            java.lang.String r4 = "chipIconVisible"
            java.lang.String r4 = r7.getAttributeValue(r2, r4)
            if (r4 != 0) goto L_0x016d
            int r4 = j.c.a.b.l.Chip_chipIconEnabled
            boolean r4 = r1.getBoolean(r4, r13)
            r12.c(r4)
        L_0x016d:
            android.content.Context r4 = r12.f0
            int r5 = j.c.a.b.l.Chip_chipIcon
            android.graphics.drawable.Drawable r4 = j.c.a.a.c.n.c.b(r4, r1, r5)
            r12.c(r4)
            int r4 = j.c.a.b.l.Chip_chipIconTint
            boolean r4 = r1.hasValue(r4)
            if (r4 == 0) goto L_0x018b
            android.content.Context r4 = r12.f0
            int r5 = j.c.a.b.l.Chip_chipIconTint
            android.content.res.ColorStateList r4 = j.c.a.a.c.n.c.a(r4, r1, r5)
            r12.d(r4)
        L_0x018b:
            int r4 = j.c.a.b.l.Chip_chipIconSize
            float r4 = r1.getDimension(r4, r3)
            r12.e(r4)
            int r4 = j.c.a.b.l.Chip_closeIconVisible
            boolean r4 = r1.getBoolean(r4, r13)
            r12.d(r4)
            if (r7 == 0) goto L_0x01b8
            java.lang.String r4 = "closeIconEnabled"
            java.lang.String r4 = r7.getAttributeValue(r2, r4)
            if (r4 == 0) goto L_0x01b8
            java.lang.String r4 = "closeIconVisible"
            java.lang.String r4 = r7.getAttributeValue(r2, r4)
            if (r4 != 0) goto L_0x01b8
            int r4 = j.c.a.b.l.Chip_closeIconEnabled
            boolean r4 = r1.getBoolean(r4, r13)
            r12.d(r4)
        L_0x01b8:
            android.content.Context r4 = r12.f0
            int r5 = j.c.a.b.l.Chip_closeIcon
            android.graphics.drawable.Drawable r4 = j.c.a.a.c.n.c.b(r4, r1, r5)
            r12.d(r4)
            android.content.Context r4 = r12.f0
            int r5 = j.c.a.b.l.Chip_closeIconTint
            android.content.res.ColorStateList r4 = j.c.a.a.c.n.c.a(r4, r1, r5)
            r12.f(r4)
            int r4 = j.c.a.b.l.Chip_closeIconSize
            float r4 = r1.getDimension(r4, r3)
            r12.j(r4)
            int r4 = j.c.a.b.l.Chip_android_checkable
            boolean r4 = r1.getBoolean(r4, r13)
            r12.a(r4)
            int r4 = j.c.a.b.l.Chip_checkedIconVisible
            boolean r4 = r1.getBoolean(r4, r13)
            r12.b(r4)
            if (r7 == 0) goto L_0x0204
            java.lang.String r4 = "checkedIconEnabled"
            java.lang.String r4 = r7.getAttributeValue(r2, r4)
            if (r4 == 0) goto L_0x0204
            java.lang.String r4 = "checkedIconVisible"
            java.lang.String r2 = r7.getAttributeValue(r2, r4)
            if (r2 != 0) goto L_0x0204
            int r2 = j.c.a.b.l.Chip_checkedIconEnabled
            boolean r2 = r1.getBoolean(r2, r13)
            r12.b(r2)
        L_0x0204:
            android.content.Context r2 = r12.f0
            int r4 = j.c.a.b.l.Chip_checkedIcon
            android.graphics.drawable.Drawable r2 = j.c.a.a.c.n.c.b(r2, r1, r4)
            r12.b(r2)
            android.content.Context r2 = r12.f0
            int r4 = j.c.a.b.l.Chip_showMotionSpec
            j.c.a.b.m.MotionSpec r2 = j.c.a.b.m.MotionSpec.a(r2, r1, r4)
            r12.V = r2
            android.content.Context r2 = r12.f0
            int r4 = j.c.a.b.l.Chip_hideMotionSpec
            j.c.a.b.m.MotionSpec r2 = j.c.a.b.m.MotionSpec.a(r2, r1, r4)
            r12.W = r2
            int r2 = j.c.a.b.l.Chip_chipStartPadding
            float r2 = r1.getDimension(r2, r3)
            r12.g(r2)
            int r2 = j.c.a.b.l.Chip_iconStartPadding
            float r2 = r1.getDimension(r2, r3)
            r12.m(r2)
            int r2 = j.c.a.b.l.Chip_iconEndPadding
            float r2 = r1.getDimension(r2, r3)
            r12.l(r2)
            int r2 = j.c.a.b.l.Chip_textStartPadding
            float r2 = r1.getDimension(r2, r3)
            r12.o(r2)
            int r2 = j.c.a.b.l.Chip_textEndPadding
            float r2 = r1.getDimension(r2, r3)
            r12.n(r2)
            int r2 = j.c.a.b.l.Chip_closeIconStartPadding
            float r2 = r1.getDimension(r2, r3)
            r12.k(r2)
            int r2 = j.c.a.b.l.Chip_closeIconEndPadding
            float r2 = r1.getDimension(r2, r3)
            r12.i(r2)
            int r2 = j.c.a.b.l.Chip_chipEndPadding
            float r2 = r1.getDimension(r2, r3)
            r12.d(r2)
            int r2 = j.c.a.b.l.Chip_android_maxWidth
            r3 = 2147483647(0x7fffffff, float:NaN)
            int r2 = r1.getDimensionPixelSize(r2, r3)
            r12.G0 = r2
            r1.recycle()
            int[] r3 = j.c.a.b.l.Chip
            int r5 = com.google.android.material.chip.Chip.u
            int[] r6 = new int[r13]
            r1 = r9
            r2 = r16
            r4 = r17
            android.content.res.TypedArray r1 = j.c.a.b.b0.ThemeEnforcement.b(r1, r2, r3, r4, r5, r6)
            int r2 = j.c.a.b.l.Chip_ensureMinTouchTargetSize
            boolean r2 = r1.getBoolean(r2, r13)
            r0.f455n = r2
            android.content.Context r2 = r14.getContext()
            r3 = 48
            float r2 = j.c.a.a.c.n.c.a(r2, r3)
            double r2 = (double) r2
            double r2 = java.lang.Math.ceil(r2)
            float r2 = (float) r2
            int r3 = j.c.a.b.l.Chip_chipMinTouchTargetSize
            float r2 = r1.getDimension(r3, r2)
            double r2 = (double) r2
            double r2 = java.lang.Math.ceil(r2)
            int r2 = (int) r2
            r0.f457p = r2
            r1.recycle()
            r14.setChipDrawable(r12)
            float r1 = i.h.l.ViewCompat.g(r14)
            r12.a(r1)
            int[] r3 = j.c.a.b.l.Chip
            int r5 = com.google.android.material.chip.Chip.u
            int[] r6 = new int[r13]
            r1 = r9
            r2 = r16
            android.content.res.TypedArray r1 = j.c.a.b.b0.ThemeEnforcement.b(r1, r2, r3, r4, r5, r6)
            int r2 = android.os.Build.VERSION.SDK_INT
            r3 = 23
            if (r2 >= r3) goto L_0x02d7
            int r2 = j.c.a.b.l.Chip_android_textColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r9, r1, r2)
            r14.setTextColor(r2)
        L_0x02d7:
            int r2 = j.c.a.b.l.Chip_shapeAppearance
            boolean r2 = r1.hasValue(r2)
            r1.recycle()
            com.google.android.material.chip.Chip$b r1 = new com.google.android.material.chip.Chip$b
            r1.<init>(r14)
            r0.f458q = r1
            r14.e()
            if (r2 != 0) goto L_0x02f4
            j.c.a.b.v.Chip r1 = new j.c.a.b.v.Chip
            r1.<init>(r14)
            r14.setOutlineProvider(r1)
        L_0x02f4:
            boolean r1 = r0.f451j
            r14.setChecked(r1)
            java.lang.CharSequence r1 = r12.G
            r14.setText(r1)
            android.text.TextUtils$TruncateAt r1 = r12.E0
            r14.setEllipsize(r1)
            r14.setIncludeFontPadding(r13)
            r14.i()
            j.c.a.b.v.ChipDrawable r1 = r0.f448e
            boolean r1 = r1.F0
            if (r1 != 0) goto L_0x0315
            r14.setLines(r11)
            r14.setHorizontallyScrolling(r11)
        L_0x0315:
            r14.setGravity(r10)
            r14.h()
            boolean r1 = r0.f455n
            if (r1 == 0) goto L_0x0324
            int r1 = r0.f457p
            r14.setMinHeight(r1)
        L_0x0324:
            int r1 = r14.getLayoutDirection()
            r0.f456o = r1
            return
        L_0x032b:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r2 = "Chip does not support multi-line text"
            r1.<init>(r2)
            throw r1
        L_0x0333:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            r1.<init>(r4)
            throw r1
        L_0x0339:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            r1.<init>(r4)
            throw r1
        L_0x033f:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r2 = "Please set start drawable using R.attr#chipIcon."
            r1.<init>(r2)
            throw r1
        L_0x0347:
            java.lang.UnsupportedOperationException r1 = new java.lang.UnsupportedOperationException
            java.lang.String r2 = "Please set left drawable using R.attr#chipIcon."
            r1.<init>(r2)
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.chip.Chip.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public void setCheckedIconVisible(boolean z) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.b(z);
        }
    }

    public void setChipIconVisible(boolean z) {
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.c(z);
        }
    }

    public void setCompoundDrawablesRelativeWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set start drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesRelativeWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set end drawable using R.attr#closeIcon.");
        }
    }

    public void setCompoundDrawablesWithIntrinsicBounds(Drawable drawable, Drawable drawable2, Drawable drawable3, Drawable drawable4) {
        if (drawable != null) {
            throw new UnsupportedOperationException("Please set left drawable using R.attr#chipIcon.");
        } else if (drawable3 == null) {
            super.setCompoundDrawablesWithIntrinsicBounds(drawable, drawable2, drawable3, drawable4);
        } else {
            throw new UnsupportedOperationException("Please set right drawable using R.attr#closeIcon.");
        }
    }

    public void setTextAppearance(Context context, int i2) {
        super.setTextAppearance(context, i2);
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.m0.a(new TextAppearance(chipDrawable.f0, i2), chipDrawable.f0);
        }
        i();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void}
     arg types: [j.c.a.b.v.ChipDrawable, int, int, int, int]
     candidates:
      ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, float, float, float, float):void}
      ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void} */
    public boolean a(int i2) {
        this.f457p = i2;
        if (!this.f455n) {
            if (this.f449f != null) {
                d();
            } else {
                f();
            }
            return false;
        }
        int max = Math.max(0, i2 - ((int) this.f448e.B));
        int max2 = Math.max(0, i2 - this.f448e.getIntrinsicWidth());
        if (max2 > 0 || max > 0) {
            int i3 = max2 > 0 ? max2 / 2 : 0;
            int i4 = max > 0 ? max / 2 : 0;
            if (this.f449f != null) {
                Rect rect = new Rect();
                this.f449f.getPadding(rect);
                if (rect.top == i4 && rect.bottom == i4 && rect.left == i3 && rect.right == i3) {
                    f();
                    return true;
                }
            }
            if (getMinHeight() != i2) {
                setMinHeight(i2);
            }
            if (getMinWidth() != i2) {
                setMinWidth(i2);
            }
            this.f449f = new InsetDrawable((Drawable) this.f448e, i3, i4, i3, i4);
            f();
            return true;
        }
        if (this.f449f != null) {
            d();
        } else {
            f();
        }
        return false;
    }

    public class b extends ExploreByTouchHelper {
        public b(Chip chip) {
            super(chip);
        }

        public void a(List<Integer> list) {
            boolean z = false;
            list.add(0);
            if (Chip.this.b()) {
                ChipDrawable chipDrawable = Chip.this.f448e;
                if (chipDrawable != null && chipDrawable.M) {
                    z = true;
                }
                if (z && Chip.this.h != null) {
                    list.add(1);
                }
            }
        }

        public void a(int i2, AccessibilityNodeInfoCompat accessibilityNodeInfoCompat) {
            CharSequence charSequence = "";
            if (i2 == 1) {
                CharSequence closeIconContentDescription = Chip.this.getCloseIconContentDescription();
                if (closeIconContentDescription != null) {
                    accessibilityNodeInfoCompat.a.setContentDescription(closeIconContentDescription);
                } else {
                    CharSequence text = Chip.this.getText();
                    Context context = Chip.this.getContext();
                    int i3 = j.mtrl_chip_close_icon_content_description;
                    Object[] objArr = new Object[1];
                    if (!TextUtils.isEmpty(text)) {
                        charSequence = text;
                    }
                    objArr[0] = charSequence;
                    accessibilityNodeInfoCompat.a.setContentDescription(context.getString(i3, objArr).trim());
                }
                accessibilityNodeInfoCompat.a.setBoundsInParent(Chip.this.getCloseIconTouchBoundsInt());
                accessibilityNodeInfoCompat.a(AccessibilityNodeInfoCompat.a.f1202e);
                accessibilityNodeInfoCompat.a.setEnabled(Chip.this.isEnabled());
                return;
            }
            accessibilityNodeInfoCompat.a.setContentDescription(charSequence);
            accessibilityNodeInfoCompat.a.setBoundsInParent(Chip.v);
        }

        public boolean a(int i2, int i3, Bundle bundle) {
            boolean z = false;
            if (i3 == 16) {
                if (i2 == 0) {
                    return Chip.this.performClick();
                }
                if (i2 == 1) {
                    Chip chip = Chip.this;
                    chip.playSoundEffect(0);
                    View.OnClickListener onClickListener = chip.h;
                    if (onClickListener != null) {
                        onClickListener.onClick(chip);
                        z = true;
                    }
                    chip.f458q.a(1, 1);
                }
            }
            return z;
        }
    }

    public void setTextAppearance(int i2) {
        super.setTextAppearance(i2);
        ChipDrawable chipDrawable = this.f448e;
        if (chipDrawable != null) {
            chipDrawable.m0.a(new TextAppearance(chipDrawable.f0, i2), chipDrawable.f0);
        }
        i();
    }
}
