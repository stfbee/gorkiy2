package com.google.android.material.bottomsheet;

import android.animation.ValueAnimator;
import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.TypedArray;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.VelocityTracker;
import android.view.View;
import android.view.ViewConfiguration;
import android.view.ViewGroup;
import android.view.ViewParent;
import androidx.coordinatorlayout.widget.CoordinatorLayout;
import com.crashlytics.android.answers.AnswersRetryFilesSender;
import i.b.k.ResourcesFlusher;
import i.h.l.ViewCompat;
import i.h.l.x.AccessibilityNodeInfoCompat;
import i.h.l.x.AccessibilityViewCommand;
import i.h.l.x.b;
import i.j.a.AbsSavedState;
import i.j.b.ViewDragHelper;
import j.a.a.a.outline;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.k;
import j.c.a.b.l;
import j.c.a.b.r.BottomSheetDialog;
import j.c.a.b.y.ElevationOverlayProvider;
import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BottomSheetBehavior<V extends View> extends CoordinatorLayout.c<V> {
    public static final int M = k.Widget_Design_BottomSheet_Modal;
    public boolean A;
    public int B;
    public int C;
    public WeakReference<V> D;
    public WeakReference<View> E;
    public final ArrayList<d> F = new ArrayList<>();
    public VelocityTracker G;
    public int H;
    public int I;
    public boolean J;
    public Map<View, Integer> K;
    public final ViewDragHelper.c L = new b();
    public int a = 0;
    public boolean b = true;
    public boolean c = false;
    public float d;

    /* renamed from: e  reason: collision with root package name */
    public int f410e;

    /* renamed from: f  reason: collision with root package name */
    public boolean f411f;
    public int g;
    public boolean h;

    /* renamed from: i  reason: collision with root package name */
    public MaterialShapeDrawable f412i;

    /* renamed from: j  reason: collision with root package name */
    public ShapeAppearanceModel f413j;

    /* renamed from: k  reason: collision with root package name */
    public boolean f414k;

    /* renamed from: l  reason: collision with root package name */
    public BottomSheetBehavior<V>.f f415l = null;

    /* renamed from: m  reason: collision with root package name */
    public ValueAnimator f416m;

    /* renamed from: n  reason: collision with root package name */
    public int f417n;

    /* renamed from: o  reason: collision with root package name */
    public int f418o;

    /* renamed from: p  reason: collision with root package name */
    public int f419p;

    /* renamed from: q  reason: collision with root package name */
    public float f420q = 0.5f;

    /* renamed from: r  reason: collision with root package name */
    public int f421r;

    /* renamed from: s  reason: collision with root package name */
    public float f422s = -1.0f;

    /* renamed from: t  reason: collision with root package name */
    public boolean f423t;
    public boolean u;
    public boolean v = true;
    public int w = 4;
    public ViewDragHelper x;
    public boolean y;
    public int z;

    public class a implements Runnable {
        public final /* synthetic */ View b;
        public final /* synthetic */ int c;

        public a(View view, int i2) {
            this.b = view;
            this.c = i2;
        }

        public void run() {
            BottomSheetBehavior.this.a(this.b, this.c);
        }
    }

    public class b extends ViewDragHelper.c {
        public b() {
        }

        public void a(View view, int i2, int i3, int i4, int i5) {
            BottomSheetBehavior.this.a(i3);
        }

        public boolean b(View view, int i2) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            int i3 = bottomSheetBehavior.w;
            if (i3 == 1 || bottomSheetBehavior.J) {
                return false;
            }
            if (i3 == 3 && bottomSheetBehavior.H == i2) {
                WeakReference<View> weakReference = bottomSheetBehavior.E;
                View view2 = weakReference != null ? weakReference.get() : null;
                if (view2 != null && view2.canScrollVertically(-1)) {
                    return false;
                }
            }
            WeakReference<V> weakReference2 = BottomSheetBehavior.this.D;
            if (weakReference2 == null || weakReference2.get() != view) {
                return false;
            }
            return true;
        }

        public void a(int i2) {
            if (i2 == 1) {
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
                if (bottomSheetBehavior.v) {
                    bottomSheetBehavior.d(1);
                }
            }
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, int, int, boolean):void
         arg types: [android.view.View, int, int, int]
         candidates:
          com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.content.Context, android.util.AttributeSet, boolean, android.content.res.ColorStateList):void
          com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
          androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
          com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, int, int, boolean):void */
        public void a(View view, float f2, float f3) {
            int i2;
            int i3;
            int i4 = 4;
            if (f3 < 0.0f) {
                BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
                if (bottomSheetBehavior.b) {
                    i2 = bottomSheetBehavior.f418o;
                } else {
                    int top = view.getTop();
                    BottomSheetBehavior bottomSheetBehavior2 = BottomSheetBehavior.this;
                    int i5 = bottomSheetBehavior2.f419p;
                    if (top > i5) {
                        i3 = i5;
                        i4 = 6;
                        BottomSheetBehavior.this.a(view, i4, i2, true);
                    }
                    i2 = bottomSheetBehavior2.f417n;
                }
            } else {
                BottomSheetBehavior bottomSheetBehavior3 = BottomSheetBehavior.this;
                if (bottomSheetBehavior3.f423t && bottomSheetBehavior3.a(view, f3)) {
                    if (Math.abs(f2) >= Math.abs(f3) || f3 <= 500.0f) {
                        int top2 = view.getTop();
                        BottomSheetBehavior bottomSheetBehavior4 = BottomSheetBehavior.this;
                        if (!(top2 > (bottomSheetBehavior4.d() + bottomSheetBehavior4.C) / 2)) {
                            BottomSheetBehavior bottomSheetBehavior5 = BottomSheetBehavior.this;
                            if (bottomSheetBehavior5.b) {
                                i2 = bottomSheetBehavior5.f418o;
                            } else if (Math.abs(view.getTop() - BottomSheetBehavior.this.f417n) < Math.abs(view.getTop() - BottomSheetBehavior.this.f419p)) {
                                i2 = BottomSheetBehavior.this.f417n;
                            } else {
                                i3 = BottomSheetBehavior.this.f419p;
                                i4 = 6;
                                BottomSheetBehavior.this.a(view, i4, i2, true);
                            }
                        }
                    }
                    i2 = BottomSheetBehavior.this.C;
                    i4 = 5;
                    BottomSheetBehavior.this.a(view, i4, i2, true);
                } else if (f3 == 0.0f || Math.abs(f2) > Math.abs(f3)) {
                    int top3 = view.getTop();
                    BottomSheetBehavior bottomSheetBehavior6 = BottomSheetBehavior.this;
                    if (!bottomSheetBehavior6.b) {
                        int i6 = bottomSheetBehavior6.f419p;
                        if (top3 < i6) {
                            if (top3 < Math.abs(top3 - bottomSheetBehavior6.f421r)) {
                                i2 = BottomSheetBehavior.this.f417n;
                            } else {
                                i3 = BottomSheetBehavior.this.f419p;
                            }
                        } else if (Math.abs(top3 - i6) < Math.abs(top3 - BottomSheetBehavior.this.f421r)) {
                            i3 = BottomSheetBehavior.this.f419p;
                        } else {
                            i2 = BottomSheetBehavior.this.f421r;
                            BottomSheetBehavior.this.a(view, i4, i2, true);
                        }
                        i4 = 6;
                        BottomSheetBehavior.this.a(view, i4, i2, true);
                    } else if (Math.abs(top3 - bottomSheetBehavior6.f418o) < Math.abs(top3 - BottomSheetBehavior.this.f421r)) {
                        i2 = BottomSheetBehavior.this.f418o;
                    } else {
                        i2 = BottomSheetBehavior.this.f421r;
                        BottomSheetBehavior.this.a(view, i4, i2, true);
                    }
                } else {
                    BottomSheetBehavior bottomSheetBehavior7 = BottomSheetBehavior.this;
                    if (bottomSheetBehavior7.b) {
                        i2 = bottomSheetBehavior7.f421r;
                    } else {
                        int top4 = view.getTop();
                        if (Math.abs(top4 - BottomSheetBehavior.this.f419p) < Math.abs(top4 - BottomSheetBehavior.this.f421r)) {
                            i3 = BottomSheetBehavior.this.f419p;
                            i4 = 6;
                        } else {
                            i2 = BottomSheetBehavior.this.f421r;
                        }
                    }
                    BottomSheetBehavior.this.a(view, i4, i2, true);
                }
            }
            i4 = 3;
            BottomSheetBehavior.this.a(view, i4, i2, true);
        }

        public int b(View view, int i2, int i3) {
            int d = BottomSheetBehavior.this.d();
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            return ResourcesFlusher.a(i2, d, bottomSheetBehavior.f423t ? bottomSheetBehavior.C : bottomSheetBehavior.f421r);
        }

        public int b(View view) {
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.this;
            if (bottomSheetBehavior.f423t) {
                return bottomSheetBehavior.C;
            }
            return bottomSheetBehavior.f421r;
        }

        public int a(View view, int i2, int i3) {
            return view.getLeft();
        }
    }

    public class c implements AccessibilityViewCommand {
        public final /* synthetic */ int a;

        public c(int i2) {
            this.a = i2;
        }

        public boolean a(View view, AccessibilityViewCommand.a aVar) {
            BottomSheetBehavior.this.c(this.a);
            return true;
        }
    }

    public static abstract class d {
    }

    public class f implements Runnable {
        public final View b;
        public boolean c;
        public int d;

        public f(View view, int i2) {
            this.b = view;
            this.d = i2;
        }

        public void run() {
            ViewDragHelper viewDragHelper = BottomSheetBehavior.this.x;
            if (viewDragHelper == null || !viewDragHelper.a(true)) {
                BottomSheetBehavior.this.d(this.d);
            } else {
                ViewCompat.a(this.b, this);
            }
            this.c = false;
        }
    }

    public BottomSheetBehavior() {
    }

    public Parcelable a(CoordinatorLayout coordinatorLayout, V v2) {
        return new e(View.BaseSavedState.EMPTY_STATE, this);
    }

    public void a(CoordinatorLayout coordinatorLayout, V v2, View view, int i2, int i3, int i4, int i5, int i6, int[] iArr) {
    }

    public boolean b(CoordinatorLayout coordinatorLayout, V v2, MotionEvent motionEvent) {
        if (!v2.isShown()) {
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        if (this.w == 1 && actionMasked == 0) {
            return true;
        }
        ViewDragHelper viewDragHelper = this.x;
        if (viewDragHelper != null) {
            viewDragHelper.a(motionEvent);
        }
        if (actionMasked == 0) {
            this.H = -1;
            VelocityTracker velocityTracker = this.G;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                this.G = null;
            }
        }
        if (this.G == null) {
            this.G = VelocityTracker.obtain();
        }
        this.G.addMovement(motionEvent);
        if (actionMasked == 2 && !this.y) {
            float abs = Math.abs(((float) this.I) - motionEvent.getY());
            ViewDragHelper viewDragHelper2 = this.x;
            if (abs > ((float) viewDragHelper2.b)) {
                viewDragHelper2.a(v2, motionEvent.getPointerId(motionEvent.getActionIndex()));
            }
        }
        return !this.y;
    }

    public void c(int i2) {
        if (i2 != this.w) {
            if (this.D != null) {
                e(i2);
            } else if (i2 == 4 || i2 == 3 || i2 == 6 || (this.f423t && i2 == 5)) {
                this.w = i2;
            }
        }
    }

    public int d() {
        return this.b ? this.f418o : this.f417n;
    }

    public final void e(int i2) {
        View view = (View) this.D.get();
        if (view != null) {
            ViewParent parent = view.getParent();
            if (parent == null || !parent.isLayoutRequested() || !ViewCompat.v(view)) {
                a(view, i2);
            } else {
                view.post(new a(view, i2));
            }
        }
    }

    public final void f(int i2) {
        ValueAnimator valueAnimator;
        if (i2 != 2) {
            boolean z2 = i2 == 3;
            if (this.f414k != z2) {
                this.f414k = z2;
                if (this.f412i != null && (valueAnimator = this.f416m) != null) {
                    if (valueAnimator.isRunning()) {
                        this.f416m.reverse();
                        return;
                    }
                    float f2 = z2 ? 0.0f : 1.0f;
                    this.f416m.setFloatValues(1.0f - f2, f2);
                    this.f416m.start();
                }
            }
        }
    }

    public void d(int i2) {
        if (this.w != i2) {
            this.w = i2;
            WeakReference<V> weakReference = this.D;
            if (weakReference != null && ((View) weakReference.get()) != null) {
                int i3 = 0;
                if (i2 == 3) {
                    b(true);
                } else if (i2 == 6 || i2 == 5 || i2 == 4) {
                    b(false);
                }
                f(i2);
                while (i3 < this.F.size()) {
                    BottomSheetDialog.d dVar = (BottomSheetDialog.d) this.F.get(i3);
                    if (dVar != null) {
                        if (i2 == 5) {
                            BottomSheetDialog.this.cancel();
                        }
                        i3++;
                    } else {
                        throw null;
                    }
                }
                e();
            }
        }
    }

    public void a(CoordinatorLayout coordinatorLayout, V v2, Parcelable parcelable) {
        e eVar = (e) parcelable;
        Parcelable parcelable2 = eVar.b;
        int i2 = this.a;
        if (i2 != 0) {
            if (i2 == -1 || (i2 & 1) == 1) {
                this.f410e = eVar.f424e;
            }
            int i3 = this.a;
            if (i3 == -1 || (i3 & 2) == 2) {
                this.b = eVar.f425f;
            }
            int i4 = this.a;
            if (i4 == -1 || (i4 & 4) == 4) {
                this.f423t = eVar.g;
            }
            int i5 = this.a;
            if (i5 == -1 || (i5 & 8) == 8) {
                this.u = eVar.h;
            }
        }
        int i6 = eVar.d;
        if (i6 == 1 || i6 == 2) {
            this.w = 4;
        } else {
            this.w = i6;
        }
    }

    public static class e extends AbsSavedState {
        public static final Parcelable.Creator<e> CREATOR = new a();
        public final int d;

        /* renamed from: e  reason: collision with root package name */
        public int f424e;

        /* renamed from: f  reason: collision with root package name */
        public boolean f425f;
        public boolean g;
        public boolean h;

        public static class a implements Parcelable.ClassLoaderCreator<e> {
            public Object createFromParcel(Parcel parcel, ClassLoader classLoader) {
                return new e(parcel, classLoader);
            }

            public Object[] newArray(int i2) {
                return new e[i2];
            }

            public Object createFromParcel(Parcel parcel) {
                return new e(parcel, (ClassLoader) null);
            }
        }

        public e(Parcel parcel, ClassLoader classLoader) {
            super(parcel, classLoader);
            this.d = parcel.readInt();
            this.f424e = parcel.readInt();
            boolean z = false;
            this.f425f = parcel.readInt() == 1;
            this.g = parcel.readInt() == 1;
            this.h = parcel.readInt() == 1 ? true : z;
        }

        public void writeToParcel(Parcel parcel, int i2) {
            parcel.writeParcelable(super.b, i2);
            parcel.writeInt(this.d);
            parcel.writeInt(this.f424e);
            parcel.writeInt(this.f425f ? 1 : 0);
            parcel.writeInt(this.g ? 1 : 0);
            parcel.writeInt(this.h ? 1 : 0);
        }

        public e(Parcelable parcelable, BottomSheetBehavior<?> bottomSheetBehavior) {
            super(parcelable);
            this.d = bottomSheetBehavior.w;
            this.f424e = bottomSheetBehavior.f410e;
            this.f425f = bottomSheetBehavior.b;
            this.g = bottomSheetBehavior.f423t;
            this.h = bottomSheetBehavior.u;
        }
    }

    public final int c() {
        if (this.f411f) {
            return Math.max(this.g, this.C - ((this.B * 9) / 16));
        }
        return this.f410e;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, i.h.l.x.b$a, int):void
     arg types: [android.view.View, i.h.l.x.AccessibilityNodeInfoCompat$a, int]
     candidates:
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int):boolean
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, i.h.l.x.b$a, int):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, i.h.l.x.b$a, int):void
     arg types: [android.view.View, i.h.l.x.AccessibilityNodeInfoCompat$a, char]
     candidates:
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int):boolean
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.os.Parcelable):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, int):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.MotionEvent):boolean
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View):boolean
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, i.h.l.x.b$a, int):void */
    public final void e() {
        View view;
        WeakReference<V> weakReference = this.D;
        if (weakReference != null && (view = (View) weakReference.get()) != null) {
            ViewCompat.f(view, 524288);
            ViewCompat.f(view, 262144);
            ViewCompat.f(view, 1048576);
            if (this.f423t && this.w != 5) {
                a(view, (b.a) AccessibilityNodeInfoCompat.a.f1205j, 5);
            }
            int i2 = this.w;
            char c2 = 6;
            if (i2 == 3) {
                if (this.b) {
                    c2 = 4;
                }
                a(view, (b.a) AccessibilityNodeInfoCompat.a.f1204i, (int) c2);
            } else if (i2 == 4) {
                if (this.b) {
                    c2 = 3;
                }
                a(view, (b.a) AccessibilityNodeInfoCompat.a.h, (int) c2);
            } else if (i2 == 6) {
                a(view, (b.a) AccessibilityNodeInfoCompat.a.f1204i, 4);
                a(view, (b.a) AccessibilityNodeInfoCompat.a.h, 3);
            }
        }
    }

    public BottomSheetBehavior(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        int i2;
        TypedArray obtainStyledAttributes = context.obtainStyledAttributes(attributeSet, l.BottomSheetBehavior_Layout);
        this.h = obtainStyledAttributes.hasValue(l.BottomSheetBehavior_Layout_shapeAppearance);
        boolean hasValue = obtainStyledAttributes.hasValue(l.BottomSheetBehavior_Layout_backgroundTint);
        if (hasValue) {
            a(context, attributeSet, hasValue, j.c.a.a.c.n.c.a(context, obtainStyledAttributes, l.BottomSheetBehavior_Layout_backgroundTint));
        } else {
            a(context, attributeSet, hasValue, (ColorStateList) null);
        }
        ValueAnimator ofFloat = ValueAnimator.ofFloat(0.0f, 1.0f);
        this.f416m = ofFloat;
        ofFloat.setDuration(500L);
        this.f416m.addUpdateListener(new j.c.a.b.r.BottomSheetBehavior(this));
        this.f422s = obtainStyledAttributes.getDimension(l.BottomSheetBehavior_Layout_android_elevation, -1.0f);
        TypedValue peekValue = obtainStyledAttributes.peekValue(l.BottomSheetBehavior_Layout_behavior_peekHeight);
        if (peekValue == null || (i2 = peekValue.data) != -1) {
            b(obtainStyledAttributes.getDimensionPixelSize(l.BottomSheetBehavior_Layout_behavior_peekHeight, -1));
        } else {
            b(i2);
        }
        a(obtainStyledAttributes.getBoolean(l.BottomSheetBehavior_Layout_behavior_hideable, false));
        boolean z2 = obtainStyledAttributes.getBoolean(l.BottomSheetBehavior_Layout_behavior_fitToContents, true);
        if (this.b != z2) {
            this.b = z2;
            if (this.D != null) {
                b();
            }
            d((!this.b || this.w != 6) ? this.w : 3);
            e();
        }
        this.u = obtainStyledAttributes.getBoolean(l.BottomSheetBehavior_Layout_behavior_skipCollapsed, false);
        this.v = obtainStyledAttributes.getBoolean(l.BottomSheetBehavior_Layout_behavior_draggable, true);
        this.a = obtainStyledAttributes.getInt(l.BottomSheetBehavior_Layout_behavior_saveFlags, 0);
        float f2 = obtainStyledAttributes.getFloat(l.BottomSheetBehavior_Layout_behavior_halfExpandedRatio, 0.5f);
        if (f2 <= 0.0f || f2 >= 1.0f) {
            throw new IllegalArgumentException("ratio must be a float value between 0 and 1");
        }
        this.f420q = f2;
        if (this.D != null) {
            this.f419p = (int) ((1.0f - f2) * ((float) this.C));
        }
        TypedValue peekValue2 = obtainStyledAttributes.peekValue(l.BottomSheetBehavior_Layout_behavior_expandedOffset);
        if (peekValue2 == null || peekValue2.type != 16) {
            int dimensionPixelOffset = obtainStyledAttributes.getDimensionPixelOffset(l.BottomSheetBehavior_Layout_behavior_expandedOffset, 0);
            if (dimensionPixelOffset >= 0) {
                this.f417n = dimensionPixelOffset;
            } else {
                throw new IllegalArgumentException("offset must be greater than or equal to 0");
            }
        } else {
            int i3 = peekValue2.data;
            if (i3 >= 0) {
                this.f417n = i3;
            } else {
                throw new IllegalArgumentException("offset must be greater than or equal to 0");
            }
        }
        obtainStyledAttributes.recycle();
        this.d = (float) ViewConfiguration.get(context).getScaledMaximumFlingVelocity();
    }

    public void a(CoordinatorLayout.f fVar) {
        this.D = null;
        this.x = null;
    }

    /* JADX WARNING: Removed duplicated region for block: B:12:0x0021  */
    /* JADX WARNING: Removed duplicated region for block: B:20:? A[ADDED_TO_REGION, RETURN, SYNTHETIC] */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public void b(int r4) {
        /*
            r3 = this;
            r0 = 1
            r1 = -1
            r2 = 0
            if (r4 != r1) goto L_0x000c
            boolean r4 = r3.f411f
            if (r4 != 0) goto L_0x0015
            r3.f411f = r0
            goto L_0x001f
        L_0x000c:
            boolean r1 = r3.f411f
            if (r1 != 0) goto L_0x0017
            int r1 = r3.f410e
            if (r1 == r4) goto L_0x0015
            goto L_0x0017
        L_0x0015:
            r0 = 0
            goto L_0x001f
        L_0x0017:
            r3.f411f = r2
            int r4 = java.lang.Math.max(r2, r4)
            r3.f410e = r4
        L_0x001f:
            if (r0 == 0) goto L_0x003a
            java.lang.ref.WeakReference<V> r4 = r3.D
            if (r4 == 0) goto L_0x003a
            r3.b()
            int r4 = r3.w
            r0 = 4
            if (r4 != r0) goto L_0x003a
            java.lang.ref.WeakReference<V> r4 = r3.D
            java.lang.Object r4 = r4.get()
            android.view.View r4 = (android.view.View) r4
            if (r4 == 0) goto L_0x003a
            r4.requestLayout()
        L_0x003a:
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.bottomsheet.BottomSheetBehavior.b(int):void");
    }

    public void a() {
        this.D = null;
        this.x = null;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, int i2) {
        MaterialShapeDrawable materialShapeDrawable;
        if (ViewCompat.h(coordinatorLayout) && !v2.getFitsSystemWindows()) {
            v2.setFitsSystemWindows(true);
        }
        if (this.D == null) {
            this.g = coordinatorLayout.getResources().getDimensionPixelSize(j.c.a.b.d.design_bottom_sheet_peek_height_min);
            this.D = new WeakReference<>(v2);
            if (this.h && (materialShapeDrawable = this.f412i) != null) {
                v2.setBackground(materialShapeDrawable);
            }
            MaterialShapeDrawable materialShapeDrawable2 = this.f412i;
            if (materialShapeDrawable2 != null) {
                float f2 = this.f422s;
                if (f2 == -1.0f) {
                    f2 = v2.getElevation();
                }
                materialShapeDrawable2.a(f2);
                boolean z2 = this.w == 3;
                this.f414k = z2;
                this.f412i.b(z2 ? 0.0f : 1.0f);
            }
            e();
            if (v2.getImportantForAccessibility() == 0) {
                v2.setImportantForAccessibility(1);
            }
        }
        if (this.x == null) {
            this.x = new ViewDragHelper(coordinatorLayout.getContext(), coordinatorLayout, this.L);
        }
        int top = v2.getTop();
        coordinatorLayout.b(v2, i2);
        this.B = coordinatorLayout.getWidth();
        int height = coordinatorLayout.getHeight();
        this.C = height;
        this.f418o = Math.max(0, height - v2.getHeight());
        this.f419p = (int) ((1.0f - this.f420q) * ((float) this.C));
        b();
        int i3 = this.w;
        if (i3 == 3) {
            ViewCompat.e(v2, d());
        } else if (i3 == 6) {
            ViewCompat.e(v2, this.f419p);
        } else if (!this.f423t || i3 != 5) {
            int i4 = this.w;
            if (i4 == 4) {
                ViewCompat.e(v2, this.f421r);
            } else if (i4 == 1 || i4 == 2) {
                ViewCompat.e(v2, top - v2.getTop());
            }
        } else {
            ViewCompat.e(v2, this.C);
        }
        this.E = new WeakReference<>(a((View) v2));
        return true;
    }

    public final void b() {
        int c2 = c();
        if (this.b) {
            this.f421r = Math.max(this.C - c2, this.f418o);
        } else {
            this.f421r = this.C - c2;
        }
    }

    public static <V extends View> BottomSheetBehavior<V> b(V v2) {
        ViewGroup.LayoutParams layoutParams = v2.getLayoutParams();
        if (layoutParams instanceof CoordinatorLayout.f) {
            CoordinatorLayout.c cVar = ((CoordinatorLayout.f) layoutParams).a;
            if (cVar instanceof BottomSheetBehavior) {
                return (BottomSheetBehavior) cVar;
            }
            throw new IllegalArgumentException("The view is not associated with BottomSheetBehavior");
        }
        throw new IllegalArgumentException("The view is not a child of CoordinatorLayout");
    }

    public final void b(boolean z2) {
        Map<View, Integer> map;
        WeakReference<V> weakReference = this.D;
        if (weakReference != null) {
            ViewParent parent = ((View) weakReference.get()).getParent();
            if (parent instanceof CoordinatorLayout) {
                CoordinatorLayout coordinatorLayout = (CoordinatorLayout) parent;
                int childCount = coordinatorLayout.getChildCount();
                if (z2) {
                    if (this.K == null) {
                        this.K = new HashMap(childCount);
                    } else {
                        return;
                    }
                }
                for (int i2 = 0; i2 < childCount; i2++) {
                    V childAt = coordinatorLayout.getChildAt(i2);
                    if (childAt != this.D.get()) {
                        if (z2) {
                            this.K.put(childAt, Integer.valueOf(childAt.getImportantForAccessibility()));
                            if (this.c) {
                                ViewCompat.h(childAt, 4);
                            }
                        } else if (this.c && (map = this.K) != null && map.containsKey(childAt)) {
                            ViewCompat.h(childAt, this.K.get(childAt).intValue());
                        }
                    }
                }
                if (!z2) {
                    this.K = null;
                }
            }
        }
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, MotionEvent motionEvent) {
        ViewDragHelper viewDragHelper;
        if (!v2.isShown() || !this.v) {
            this.y = true;
            return false;
        }
        int actionMasked = motionEvent.getActionMasked();
        View view = null;
        if (actionMasked == 0) {
            this.H = -1;
            VelocityTracker velocityTracker = this.G;
            if (velocityTracker != null) {
                velocityTracker.recycle();
                this.G = null;
            }
        }
        if (this.G == null) {
            this.G = VelocityTracker.obtain();
        }
        this.G.addMovement(motionEvent);
        if (actionMasked == 0) {
            int x2 = (int) motionEvent.getX();
            this.I = (int) motionEvent.getY();
            if (this.w != 2) {
                WeakReference<View> weakReference = this.E;
                View view2 = weakReference != null ? weakReference.get() : null;
                if (view2 != null && coordinatorLayout.a(view2, x2, this.I)) {
                    this.H = motionEvent.getPointerId(motionEvent.getActionIndex());
                    this.J = true;
                }
            }
            this.y = this.H == -1 && !coordinatorLayout.a(v2, x2, this.I);
        } else if (actionMasked == 1 || actionMasked == 3) {
            this.J = false;
            this.H = -1;
            if (this.y) {
                this.y = false;
                return false;
            }
        }
        if (!this.y && (viewDragHelper = this.x) != null && viewDragHelper.c(motionEvent)) {
            return true;
        }
        WeakReference<View> weakReference2 = this.E;
        if (weakReference2 != null) {
            view = weakReference2.get();
        }
        if (actionMasked != 2 || view == null || this.y || this.w == 1 || coordinatorLayout.a(view, (int) motionEvent.getX(), (int) motionEvent.getY()) || this.x == null || Math.abs(((float) this.I) - motionEvent.getY()) <= ((float) this.x.b)) {
            return false;
        }
        return true;
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, View view, View view2, int i2, int i3) {
        this.z = 0;
        this.A = false;
        if ((i2 & 2) != 0) {
            return true;
        }
        return false;
    }

    public void a(CoordinatorLayout coordinatorLayout, V v2, View view, int i2, int i3, int[] iArr, int i4) {
        if (i4 != 1) {
            WeakReference<View> weakReference = this.E;
            if (view == (weakReference != null ? weakReference.get() : null)) {
                int top = v2.getTop();
                int i5 = top - i3;
                if (i3 > 0) {
                    if (i5 < d()) {
                        iArr[1] = top - d();
                        ViewCompat.e(v2, -iArr[1]);
                        d(3);
                    } else if (this.v) {
                        iArr[1] = i3;
                        ViewCompat.e(v2, -i3);
                        d(1);
                    } else {
                        return;
                    }
                } else if (i3 < 0 && !view.canScrollVertically(-1)) {
                    int i6 = this.f421r;
                    if (i5 > i6 && !this.f423t) {
                        iArr[1] = top - i6;
                        ViewCompat.e(v2, -iArr[1]);
                        d(4);
                    } else if (this.v) {
                        iArr[1] = i3;
                        ViewCompat.e(v2, -i3);
                        d(1);
                    } else {
                        return;
                    }
                }
                a(v2.getTop());
                this.z = i3;
                this.A = true;
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, int, int, boolean):void
     arg types: [V, int, int, int]
     candidates:
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.content.Context, android.util.AttributeSet, boolean, android.content.res.ColorStateList):void
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, int, int, boolean):void */
    public void a(CoordinatorLayout coordinatorLayout, V v2, View view, int i2) {
        int i3;
        int i4;
        float f2;
        int i5 = 3;
        if (v2.getTop() == d()) {
            d(3);
            return;
        }
        WeakReference<View> weakReference = this.E;
        if (weakReference != null && view == weakReference.get() && this.A) {
            if (this.z > 0) {
                if (this.b) {
                    i3 = this.f418o;
                } else {
                    int top = v2.getTop();
                    int i6 = this.f419p;
                    if (top > i6) {
                        i3 = i6;
                    } else {
                        i3 = this.f417n;
                    }
                }
                a((View) v2, i5, i3, false);
                this.A = false;
            }
            if (this.f423t) {
                VelocityTracker velocityTracker = this.G;
                if (velocityTracker == null) {
                    f2 = 0.0f;
                } else {
                    velocityTracker.computeCurrentVelocity(AnswersRetryFilesSender.BACKOFF_MS, this.d);
                    f2 = this.G.getYVelocity(this.H);
                }
                if (a(v2, f2)) {
                    i3 = this.C;
                    i5 = 5;
                    a((View) v2, i5, i3, false);
                    this.A = false;
                }
            }
            if (this.z == 0) {
                int top2 = v2.getTop();
                if (!this.b) {
                    int i7 = this.f419p;
                    if (top2 < i7) {
                        if (top2 < Math.abs(top2 - this.f421r)) {
                            i3 = this.f417n;
                            a((View) v2, i5, i3, false);
                            this.A = false;
                        }
                        i3 = this.f419p;
                    } else if (Math.abs(top2 - i7) < Math.abs(top2 - this.f421r)) {
                        i3 = this.f419p;
                    } else {
                        i4 = this.f421r;
                    }
                } else if (Math.abs(top2 - this.f418o) < Math.abs(top2 - this.f421r)) {
                    i3 = this.f418o;
                    a((View) v2, i5, i3, false);
                    this.A = false;
                } else {
                    i4 = this.f421r;
                }
            } else if (this.b) {
                i4 = this.f421r;
            } else {
                int top3 = v2.getTop();
                if (Math.abs(top3 - this.f419p) < Math.abs(top3 - this.f421r)) {
                    i3 = this.f419p;
                } else {
                    i4 = this.f421r;
                }
            }
            i5 = 4;
            a((View) v2, i5, i3, false);
            this.A = false;
            i5 = 6;
            a((View) v2, i5, i3, false);
            this.A = false;
        }
    }

    public boolean a(CoordinatorLayout coordinatorLayout, V v2, View view, float f2, float f3) {
        WeakReference<View> weakReference = this.E;
        if (weakReference == null || view != weakReference.get() || this.w == 3) {
            return false;
        }
        return true;
    }

    public void a(boolean z2) {
        if (this.f423t != z2) {
            this.f423t = z2;
            if (!z2 && this.w == 5) {
                c(4);
            }
            e();
        }
    }

    public boolean a(View view, float f2) {
        if (this.u) {
            return true;
        }
        if (view.getTop() < this.f421r) {
            return false;
        }
        if (Math.abs(((f2 * 0.1f) + ((float) view.getTop())) - ((float) this.f421r)) / ((float) c()) > 0.5f) {
            return true;
        }
        return false;
    }

    public View a(View view) {
        if (ViewCompat.x(view)) {
            return view;
        }
        if (!(view instanceof ViewGroup)) {
            return null;
        }
        ViewGroup viewGroup = (ViewGroup) view;
        int childCount = viewGroup.getChildCount();
        for (int i2 = 0; i2 < childCount; i2++) {
            View a2 = a(viewGroup.getChildAt(i2));
            if (a2 != null) {
                return a2;
            }
        }
        return null;
    }

    public final void a(Context context, AttributeSet attributeSet, boolean z2, ColorStateList colorStateList) {
        if (this.h) {
            this.f413j = ShapeAppearanceModel.a(context, attributeSet, j.c.a.b.b.bottomSheetStyle, M).a();
            MaterialShapeDrawable materialShapeDrawable = new MaterialShapeDrawable(this.f413j);
            this.f412i = materialShapeDrawable;
            materialShapeDrawable.b.b = new ElevationOverlayProvider(context);
            materialShapeDrawable.j();
            if (!z2 || colorStateList == null) {
                TypedValue typedValue = new TypedValue();
                context.getTheme().resolveAttribute(16842801, typedValue, true);
                this.f412i.setTint(typedValue.data);
                return;
            }
            this.f412i.a(colorStateList);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, int, int, boolean):void
     arg types: [android.view.View, int, int, int]
     candidates:
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.content.Context, android.util.AttributeSet, boolean, android.content.res.ColorStateList):void
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.view.View, int):void
      androidx.coordinatorlayout.widget.CoordinatorLayout.c.a(androidx.coordinatorlayout.widget.CoordinatorLayout, android.view.View, android.graphics.Rect, boolean):boolean
      com.google.android.material.bottomsheet.BottomSheetBehavior.a(android.view.View, int, int, boolean):void */
    public void a(View view, int i2) {
        int i3;
        int i4;
        if (i2 == 4) {
            i3 = this.f421r;
        } else if (i2 == 6) {
            i3 = this.f419p;
            if (this.b && i3 <= (i4 = this.f418o)) {
                i2 = 3;
                i3 = i4;
            }
        } else if (i2 == 3) {
            i3 = d();
        } else if (!this.f423t || i2 != 5) {
            throw new IllegalArgumentException(outline.b("Illegal state argument: ", i2));
        } else {
            i3 = this.C;
        }
        a(view, i2, i3, false);
    }

    public void a(View view, int i2, int i3, boolean z2) {
        boolean z3;
        if (z2) {
            z3 = this.x.b(view.getLeft(), i3);
        } else {
            ViewDragHelper viewDragHelper = this.x;
            int left = view.getLeft();
            viewDragHelper.f1256r = view;
            viewDragHelper.c = -1;
            z3 = viewDragHelper.a(left, i3, 0, 0);
            if (!z3 && viewDragHelper.a == 0 && viewDragHelper.f1256r != null) {
                viewDragHelper.f1256r = null;
            }
        }
        if (z3) {
            d(2);
            f(i2);
            if (this.f415l == null) {
                this.f415l = new f(view, i2);
            }
            BottomSheetBehavior<V>.f fVar = this.f415l;
            if (!fVar.c) {
                fVar.d = i2;
                ViewCompat.a(view, fVar);
                this.f415l.c = true;
                return;
            }
            fVar.d = i2;
            return;
        }
        d(i2);
    }

    public void a(int i2) {
        if (((View) this.D.get()) != null && !this.F.isEmpty()) {
            if (i2 <= this.f421r) {
                int d2 = d();
            }
            int i3 = 0;
            while (i3 < this.F.size()) {
                if (((BottomSheetDialog.d) this.F.get(i3)) != null) {
                    i3++;
                } else {
                    throw null;
                }
            }
        }
    }

    public final void a(V v2, b.a aVar, int i2) {
        ViewCompat.a(v2, aVar, null, new c(i2));
    }
}
