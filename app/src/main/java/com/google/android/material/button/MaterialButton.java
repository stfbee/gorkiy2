package com.google.android.material.button;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.os.Build;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.widget.Button;
import android.widget.Checkable;
import android.widget.CompoundButton;
import i.b.k.ResourcesFlusher;
import i.b.l.a.AppCompatResources;
import i.b.q.AppCompatButton;
import i.h.l.ViewCompat;
import j.c.a.a.c.n.c;
import j.c.a.b.e0.RippleUtils;
import j.c.a.b.g0.MaterialShapeDrawable;
import j.c.a.b.g0.ShapeAppearanceModel;
import j.c.a.b.g0.Shapeable;
import j.c.a.b.k;
import j.c.a.b.s.MaterialButtonHelper;
import java.util.Iterator;
import java.util.LinkedHashSet;

public class MaterialButton extends AppCompatButton implements Checkable, Shapeable {

    /* renamed from: p  reason: collision with root package name */
    public static final int[] f427p = {16842911};

    /* renamed from: q  reason: collision with root package name */
    public static final int[] f428q = {16842912};

    /* renamed from: r  reason: collision with root package name */
    public static final int f429r = k.Widget_MaterialComponents_Button;
    public final MaterialButtonHelper d;

    /* renamed from: e  reason: collision with root package name */
    public final LinkedHashSet<a> f430e;

    /* renamed from: f  reason: collision with root package name */
    public b f431f;
    public PorterDuff.Mode g;
    public ColorStateList h;

    /* renamed from: i  reason: collision with root package name */
    public Drawable f432i;

    /* renamed from: j  reason: collision with root package name */
    public int f433j;

    /* renamed from: k  reason: collision with root package name */
    public int f434k;

    /* renamed from: l  reason: collision with root package name */
    public int f435l;

    /* renamed from: m  reason: collision with root package name */
    public boolean f436m;

    /* renamed from: n  reason: collision with root package name */
    public boolean f437n;

    /* renamed from: o  reason: collision with root package name */
    public int f438o;

    public interface a {
        void a(MaterialButton materialButton, boolean z);
    }

    public interface b {
        void a(MaterialButton materialButton, boolean z);
    }

    public MaterialButton(Context context) {
        this(context, null);
    }

    private String getA11yClassName() {
        return (a() ? CompoundButton.class : Button.class).getName();
    }

    public final void a(boolean z) {
        Drawable drawable = this.f432i;
        boolean z2 = false;
        if (drawable != null) {
            Drawable mutate = ResourcesFlusher.d(drawable).mutate();
            this.f432i = mutate;
            mutate.setTintList(this.h);
            PorterDuff.Mode mode = this.g;
            if (mode != null) {
                this.f432i.setTintMode(mode);
            }
            int i2 = this.f433j;
            if (i2 == 0) {
                i2 = this.f432i.getIntrinsicWidth();
            }
            int i3 = this.f433j;
            if (i3 == 0) {
                i3 = this.f432i.getIntrinsicHeight();
            }
            Drawable drawable2 = this.f432i;
            int i4 = this.f434k;
            drawable2.setBounds(i4, 0, i2 + i4, i3);
        }
        int i5 = this.f438o;
        boolean z3 = i5 == 1 || i5 == 2;
        if (!z) {
            Drawable[] compoundDrawablesRelative = getCompoundDrawablesRelative();
            Drawable drawable3 = compoundDrawablesRelative[0];
            Drawable drawable4 = compoundDrawablesRelative[2];
            if ((z3 && drawable3 != this.f432i) || (!z3 && drawable4 != this.f432i)) {
                z2 = true;
            }
            if (!z2) {
                return;
            }
            if (z3) {
                setCompoundDrawablesRelative(this.f432i, null, null, null);
            } else {
                setCompoundDrawablesRelative(null, null, this.f432i, null);
            }
        } else if (z3) {
            setCompoundDrawablesRelative(this.f432i, null, null, null);
        } else {
            setCompoundDrawablesRelative(null, null, this.f432i, null);
        }
    }

    public final boolean b() {
        MaterialButtonHelper materialButtonHelper = this.d;
        return materialButtonHelper != null && !materialButtonHelper.f2336o;
    }

    public final void c() {
        if (this.f432i != null && getLayout() != null) {
            int i2 = this.f438o;
            boolean z = true;
            if (i2 == 1 || i2 == 3) {
                this.f434k = 0;
                a(false);
                return;
            }
            TextPaint paint = getPaint();
            String charSequence = getText().toString();
            if (getTransformationMethod() != null) {
                charSequence = getTransformationMethod().getTransformation(charSequence, this).toString();
            }
            int min = Math.min((int) paint.measureText(charSequence), getLayout().getEllipsizedWidth());
            int i3 = this.f433j;
            if (i3 == 0) {
                i3 = this.f432i.getIntrinsicWidth();
            }
            int measuredWidth = (((((getMeasuredWidth() - min) - ViewCompat.n(this)) - i3) - this.f435l) - getPaddingStart()) / 2;
            boolean z2 = ViewCompat.k(this) == 1;
            if (this.f438o != 4) {
                z = false;
            }
            if (z2 != z) {
                measuredWidth = -measuredWidth;
            }
            if (this.f434k != measuredWidth) {
                this.f434k = measuredWidth;
                a(false);
            }
        }
    }

    public ColorStateList getBackgroundTintList() {
        return getSupportBackgroundTintList();
    }

    public PorterDuff.Mode getBackgroundTintMode() {
        return getSupportBackgroundTintMode();
    }

    public int getCornerRadius() {
        if (b()) {
            return this.d.g;
        }
        return 0;
    }

    public Drawable getIcon() {
        return this.f432i;
    }

    public int getIconGravity() {
        return this.f438o;
    }

    public int getIconPadding() {
        return this.f435l;
    }

    public int getIconSize() {
        return this.f433j;
    }

    public ColorStateList getIconTint() {
        return this.h;
    }

    public PorterDuff.Mode getIconTintMode() {
        return this.g;
    }

    public ColorStateList getRippleColor() {
        if (b()) {
            return this.d.f2333l;
        }
        return null;
    }

    public ShapeAppearanceModel getShapeAppearanceModel() {
        if (b()) {
            return this.d.b;
        }
        throw new IllegalStateException("Attempted to get ShapeAppearanceModel from a MaterialButton which has an overwritten background.");
    }

    public ColorStateList getStrokeColor() {
        if (b()) {
            return this.d.f2332k;
        }
        return null;
    }

    public int getStrokeWidth() {
        if (b()) {
            return this.d.h;
        }
        return 0;
    }

    public ColorStateList getSupportBackgroundTintList() {
        if (b()) {
            return this.d.f2331j;
        }
        return super.getSupportBackgroundTintList();
    }

    public PorterDuff.Mode getSupportBackgroundTintMode() {
        if (b()) {
            return this.d.f2330i;
        }
        return super.getSupportBackgroundTintMode();
    }

    public boolean isChecked() {
        return this.f436m;
    }

    public void onAttachedToWindow() {
        super.onAttachedToWindow();
        c.a(this, this.d.b());
    }

    public int[] onCreateDrawableState(int i2) {
        int[] onCreateDrawableState = super.onCreateDrawableState(i2 + 2);
        if (a()) {
            Button.mergeDrawableStates(onCreateDrawableState, f427p);
        }
        if (this.f436m) {
            Button.mergeDrawableStates(onCreateDrawableState, f428q);
        }
        return onCreateDrawableState;
    }

    public void onInitializeAccessibilityEvent(AccessibilityEvent accessibilityEvent) {
        super.onInitializeAccessibilityEvent(accessibilityEvent);
        accessibilityEvent.setClassName(getA11yClassName());
        accessibilityEvent.setChecked(this.f436m);
    }

    public void onInitializeAccessibilityNodeInfo(AccessibilityNodeInfo accessibilityNodeInfo) {
        super.onInitializeAccessibilityNodeInfo(accessibilityNodeInfo);
        accessibilityNodeInfo.setClassName(getA11yClassName());
        accessibilityNodeInfo.setCheckable(a());
        accessibilityNodeInfo.setChecked(this.f436m);
        accessibilityNodeInfo.setClickable(isClickable());
    }

    public void onLayout(boolean z, int i2, int i3, int i4, int i5) {
        MaterialButtonHelper materialButtonHelper;
        super.onLayout(z, i2, i3, i4, i5);
        if (Build.VERSION.SDK_INT == 21 && (materialButtonHelper = this.d) != null) {
            int i6 = i5 - i3;
            int i7 = i4 - i2;
            Drawable drawable = materialButtonHelper.f2334m;
            if (drawable != null) {
                drawable.setBounds(materialButtonHelper.c, materialButtonHelper.f2328e, i7 - materialButtonHelper.d, i6 - materialButtonHelper.f2329f);
            }
        }
    }

    public void onMeasure(int i2, int i3) {
        super.onMeasure(i2, i3);
        c();
    }

    public void onTextChanged(CharSequence charSequence, int i2, int i3, int i4) {
        super.onTextChanged(charSequence, i2, i3, i4);
        c();
    }

    public boolean performClick() {
        toggle();
        return super.performClick();
    }

    public void setBackground(Drawable drawable) {
        setBackgroundDrawable(drawable);
    }

    public void setBackgroundColor(int i2) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (materialButtonHelper.b() != null) {
                materialButtonHelper.b().setTint(i2);
                return;
            }
            return;
        }
        super.setBackgroundColor(i2);
    }

    public void setBackgroundDrawable(Drawable drawable) {
        if (!b()) {
            super.setBackgroundDrawable(drawable);
        } else if (drawable != getBackground()) {
            Log.w("MaterialButton", "Do not set the background; MaterialButton manages its own background drawable.");
            MaterialButtonHelper materialButtonHelper = this.d;
            materialButtonHelper.f2336o = true;
            materialButtonHelper.a.setSupportBackgroundTintList(materialButtonHelper.f2331j);
            materialButtonHelper.a.setSupportBackgroundTintMode(materialButtonHelper.f2330i);
            super.setBackgroundDrawable(drawable);
        } else {
            getBackground().setState(drawable.getState());
        }
    }

    public void setBackgroundResource(int i2) {
        setBackgroundDrawable(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setBackgroundTintList(ColorStateList colorStateList) {
        setSupportBackgroundTintList(colorStateList);
    }

    public void setBackgroundTintMode(PorterDuff.Mode mode) {
        setSupportBackgroundTintMode(mode);
    }

    public void setCheckable(boolean z) {
        if (b()) {
            this.d.f2338q = z;
        }
    }

    public void setChecked(boolean z) {
        if (a() && isEnabled() && this.f436m != z) {
            this.f436m = z;
            refreshDrawableState();
            if (!this.f437n) {
                this.f437n = true;
                Iterator<a> it = this.f430e.iterator();
                while (it.hasNext()) {
                    it.next().a(this, this.f436m);
                }
                this.f437n = false;
            }
        }
    }

    public void setCornerRadius(int i2) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (!materialButtonHelper.f2337p || materialButtonHelper.g != i2) {
                materialButtonHelper.g = i2;
                materialButtonHelper.f2337p = true;
                materialButtonHelper.a(materialButtonHelper.b.a((float) i2));
            }
        }
    }

    public void setCornerRadiusResource(int i2) {
        if (b()) {
            setCornerRadius(getResources().getDimensionPixelSize(i2));
        }
    }

    public void setElevation(float f2) {
        super.setElevation(f2);
        if (b()) {
            MaterialShapeDrawable b2 = this.d.b();
            MaterialShapeDrawable.b bVar = b2.b;
            if (bVar.f2236o != f2) {
                bVar.f2236o = f2;
                b2.j();
            }
        }
    }

    public void setIcon(Drawable drawable) {
        if (this.f432i != drawable) {
            this.f432i = drawable;
            a(true);
        }
    }

    public void setIconGravity(int i2) {
        if (this.f438o != i2) {
            this.f438o = i2;
            c();
        }
    }

    public void setIconPadding(int i2) {
        if (this.f435l != i2) {
            this.f435l = i2;
            setCompoundDrawablePadding(i2);
        }
    }

    public void setIconResource(int i2) {
        setIcon(i2 != 0 ? AppCompatResources.c(getContext(), i2) : null);
    }

    public void setIconSize(int i2) {
        if (i2 < 0) {
            throw new IllegalArgumentException("iconSize cannot be less than 0");
        } else if (this.f433j != i2) {
            this.f433j = i2;
            a(true);
        }
    }

    public void setIconTint(ColorStateList colorStateList) {
        if (this.h != colorStateList) {
            this.h = colorStateList;
            a(false);
        }
    }

    public void setIconTintMode(PorterDuff.Mode mode) {
        if (this.g != mode) {
            this.g = mode;
            a(false);
        }
    }

    public void setIconTintResource(int i2) {
        setIconTint(AppCompatResources.b(getContext(), i2));
    }

    public void setInternalBackground(Drawable drawable) {
        super.setBackgroundDrawable(drawable);
    }

    public void setOnPressedChangeListenerInternal(b bVar) {
        this.f431f = bVar;
    }

    public void setPressed(boolean z) {
        b bVar = this.f431f;
        if (bVar != null) {
            bVar.a(this, z);
        }
        super.setPressed(z);
    }

    public void setRippleColor(ColorStateList colorStateList) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (materialButtonHelper.f2333l != colorStateList) {
                materialButtonHelper.f2333l = colorStateList;
                if (materialButtonHelper.a.getBackground() instanceof RippleDrawable) {
                    ((RippleDrawable) materialButtonHelper.a.getBackground()).setColor(RippleUtils.a(colorStateList));
                }
            }
        }
    }

    public void setRippleColorResource(int i2) {
        if (b()) {
            setRippleColor(AppCompatResources.b(getContext(), i2));
        }
    }

    public void setShapeAppearanceModel(ShapeAppearanceModel shapeAppearanceModel) {
        if (b()) {
            this.d.a(shapeAppearanceModel);
            return;
        }
        throw new IllegalStateException("Attempted to set ShapeAppearanceModel on a MaterialButton which has an overwritten background.");
    }

    public void setShouldDrawSurfaceColorStroke(boolean z) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            materialButtonHelper.f2335n = z;
            materialButtonHelper.d();
        }
    }

    public void setStrokeColor(ColorStateList colorStateList) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (materialButtonHelper.f2332k != colorStateList) {
                materialButtonHelper.f2332k = colorStateList;
                materialButtonHelper.d();
            }
        }
    }

    public void setStrokeColorResource(int i2) {
        if (b()) {
            setStrokeColor(AppCompatResources.b(getContext(), i2));
        }
    }

    public void setStrokeWidth(int i2) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (materialButtonHelper.h != i2) {
                materialButtonHelper.h = i2;
                materialButtonHelper.d();
            }
        }
    }

    public void setStrokeWidthResource(int i2) {
        if (b()) {
            setStrokeWidth(getResources().getDimensionPixelSize(i2));
        }
    }

    public void setSupportBackgroundTintList(ColorStateList colorStateList) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (materialButtonHelper.f2331j != colorStateList) {
                materialButtonHelper.f2331j = colorStateList;
                if (materialButtonHelper.b() != null) {
                    materialButtonHelper.b().setTintList(materialButtonHelper.f2331j);
                    return;
                }
                return;
            }
            return;
        }
        super.setSupportBackgroundTintList(colorStateList);
    }

    public void setSupportBackgroundTintMode(PorterDuff.Mode mode) {
        if (b()) {
            MaterialButtonHelper materialButtonHelper = this.d;
            if (materialButtonHelper.f2330i != mode) {
                materialButtonHelper.f2330i = mode;
                if (materialButtonHelper.b() != null && materialButtonHelper.f2330i != null) {
                    materialButtonHelper.b().setTintMode(materialButtonHelper.f2330i);
                    return;
                }
                return;
            }
            return;
        }
        super.setSupportBackgroundTintMode(mode);
    }

    public void toggle() {
        setChecked(!this.f436m);
    }

    public MaterialButton(Context context, AttributeSet attributeSet) {
        this(context, attributeSet, j.c.a.b.b.materialButtonStyle);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(android.view.View, int):int
     arg types: [com.google.android.material.button.MaterialButton, int]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(android.view.View, int):int */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void}
     arg types: [android.graphics.drawable.LayerDrawable, int, int, int, int]
     candidates:
      ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, float, float, float, float):void}
      ClspMth{android.graphics.drawable.InsetDrawable.<init>(android.graphics.drawable.Drawable, int, int, int, int):void} */
    /* JADX WARNING: Illegal instructions before constructor call */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public MaterialButton(android.content.Context r21, android.util.AttributeSet r22, int r23) {
        /*
            r20 = this;
            r0 = r20
            r7 = r22
            r8 = r23
            int r1 = com.google.android.material.button.MaterialButton.f429r
            r2 = r21
            android.content.Context r1 = j.c.a.b.m0.a.MaterialThemeOverlay.a(r2, r7, r8, r1)
            r0.<init>(r1, r7, r8)
            java.util.LinkedHashSet r1 = new java.util.LinkedHashSet
            r1.<init>()
            r0.f430e = r1
            r9 = 0
            r0.f436m = r9
            r0.f437n = r9
            android.content.Context r10 = r20.getContext()
            int[] r3 = j.c.a.b.l.MaterialButton
            int r5 = com.google.android.material.button.MaterialButton.f429r
            int[] r6 = new int[r9]
            r1 = r10
            r2 = r22
            r4 = r23
            android.content.res.TypedArray r1 = j.c.a.b.b0.ThemeEnforcement.b(r1, r2, r3, r4, r5, r6)
            int r2 = j.c.a.b.l.MaterialButton_iconPadding
            int r2 = r1.getDimensionPixelSize(r2, r9)
            r0.f435l = r2
            int r2 = j.c.a.b.l.MaterialButton_iconTintMode
            r3 = -1
            int r2 = r1.getInt(r2, r3)
            android.graphics.PorterDuff$Mode r4 = android.graphics.PorterDuff.Mode.SRC_IN
            android.graphics.PorterDuff$Mode r2 = j.c.a.a.c.n.c.a(r2, r4)
            r0.g = r2
            android.content.Context r2 = r20.getContext()
            int r4 = j.c.a.b.l.MaterialButton_iconTint
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r4)
            r0.h = r2
            android.content.Context r2 = r20.getContext()
            int r4 = j.c.a.b.l.MaterialButton_icon
            android.graphics.drawable.Drawable r2 = j.c.a.a.c.n.c.b(r2, r1, r4)
            r0.f432i = r2
            int r2 = j.c.a.b.l.MaterialButton_iconGravity
            r4 = 1
            int r2 = r1.getInteger(r2, r4)
            r0.f438o = r2
            int r2 = j.c.a.b.l.MaterialButton_iconSize
            int r2 = r1.getDimensionPixelSize(r2, r9)
            r0.f433j = r2
            int r2 = com.google.android.material.button.MaterialButton.f429r
            j.c.a.b.g0.ShapeAppearanceModel$b r2 = j.c.a.b.g0.ShapeAppearanceModel.a(r10, r7, r8, r2)
            j.c.a.b.g0.ShapeAppearanceModel r2 = r2.a()
            j.c.a.b.s.MaterialButtonHelper r5 = new j.c.a.b.s.MaterialButtonHelper
            r5.<init>(r0, r2)
            r0.d = r5
            if (r5 == 0) goto L_0x01da
            int r2 = j.c.a.b.l.MaterialButton_android_insetLeft
            int r2 = r1.getDimensionPixelOffset(r2, r9)
            r5.c = r2
            int r2 = j.c.a.b.l.MaterialButton_android_insetRight
            int r2 = r1.getDimensionPixelOffset(r2, r9)
            r5.d = r2
            int r2 = j.c.a.b.l.MaterialButton_android_insetTop
            int r2 = r1.getDimensionPixelOffset(r2, r9)
            r5.f2328e = r2
            int r2 = j.c.a.b.l.MaterialButton_android_insetBottom
            int r2 = r1.getDimensionPixelOffset(r2, r9)
            r5.f2329f = r2
            int r2 = j.c.a.b.l.MaterialButton_cornerRadius
            boolean r2 = r1.hasValue(r2)
            if (r2 == 0) goto L_0x00bf
            int r2 = j.c.a.b.l.MaterialButton_cornerRadius
            int r2 = r1.getDimensionPixelSize(r2, r3)
            r5.g = r2
            j.c.a.b.g0.ShapeAppearanceModel r6 = r5.b
            float r2 = (float) r2
            j.c.a.b.g0.ShapeAppearanceModel r2 = r6.a(r2)
            r5.a(r2)
            r5.f2337p = r4
        L_0x00bf:
            int r2 = j.c.a.b.l.MaterialButton_strokeWidth
            int r2 = r1.getDimensionPixelSize(r2, r9)
            r5.h = r2
            int r2 = j.c.a.b.l.MaterialButton_backgroundTintMode
            int r2 = r1.getInt(r2, r3)
            android.graphics.PorterDuff$Mode r6 = android.graphics.PorterDuff.Mode.SRC_IN
            android.graphics.PorterDuff$Mode r2 = j.c.a.a.c.n.c.a(r2, r6)
            r5.f2330i = r2
            com.google.android.material.button.MaterialButton r2 = r5.a
            android.content.Context r2 = r2.getContext()
            int r6 = j.c.a.b.l.MaterialButton_backgroundTint
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r6)
            r5.f2331j = r2
            com.google.android.material.button.MaterialButton r2 = r5.a
            android.content.Context r2 = r2.getContext()
            int r6 = j.c.a.b.l.MaterialButton_strokeColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r6)
            r5.f2332k = r2
            com.google.android.material.button.MaterialButton r2 = r5.a
            android.content.Context r2 = r2.getContext()
            int r6 = j.c.a.b.l.MaterialButton_rippleColor
            android.content.res.ColorStateList r2 = j.c.a.a.c.n.c.a(r2, r1, r6)
            r5.f2333l = r2
            int r2 = j.c.a.b.l.MaterialButton_android_checkable
            boolean r2 = r1.getBoolean(r2, r9)
            r5.f2338q = r2
            int r2 = j.c.a.b.l.MaterialButton_elevation
            int r2 = r1.getDimensionPixelSize(r2, r9)
            com.google.android.material.button.MaterialButton r6 = r5.a
            int r6 = i.h.l.ViewCompat.o(r6)
            com.google.android.material.button.MaterialButton r7 = r5.a
            int r7 = r7.getPaddingTop()
            com.google.android.material.button.MaterialButton r8 = r5.a
            int r8 = r8.getPaddingEnd()
            com.google.android.material.button.MaterialButton r10 = r5.a
            int r10 = r10.getPaddingBottom()
            com.google.android.material.button.MaterialButton r11 = r5.a
            j.c.a.b.g0.MaterialShapeDrawable r12 = new j.c.a.b.g0.MaterialShapeDrawable
            j.c.a.b.g0.ShapeAppearanceModel r13 = r5.b
            r12.<init>(r13)
            com.google.android.material.button.MaterialButton r13 = r5.a
            android.content.Context r13 = r13.getContext()
            r12.a(r13)
            android.content.res.ColorStateList r13 = r5.f2331j
            r12.setTintList(r13)
            android.graphics.PorterDuff$Mode r13 = r5.f2330i
            if (r13 == 0) goto L_0x0143
            r12.setTintMode(r13)
        L_0x0143:
            int r13 = r5.h
            float r13 = (float) r13
            android.content.res.ColorStateList r14 = r5.f2332k
            r12.a(r13, r14)
            j.c.a.b.g0.MaterialShapeDrawable r13 = new j.c.a.b.g0.MaterialShapeDrawable
            j.c.a.b.g0.ShapeAppearanceModel r14 = r5.b
            r13.<init>(r14)
            r13.setTint(r9)
            int r14 = r5.h
            float r14 = (float) r14
            boolean r15 = r5.f2335n
            if (r15 == 0) goto L_0x0165
            com.google.android.material.button.MaterialButton r15 = r5.a
            int r4 = j.c.a.b.b.colorSurface
            int r4 = j.c.a.a.c.n.c.a(r15, r4)
            goto L_0x0166
        L_0x0165:
            r4 = 0
        L_0x0166:
            r13.a(r14, r4)
            j.c.a.b.g0.MaterialShapeDrawable r4 = new j.c.a.b.g0.MaterialShapeDrawable
            j.c.a.b.g0.ShapeAppearanceModel r14 = r5.b
            r4.<init>(r14)
            r5.f2334m = r4
            r4.setTint(r3)
            android.graphics.drawable.RippleDrawable r3 = new android.graphics.drawable.RippleDrawable
            android.content.res.ColorStateList r4 = r5.f2333l
            android.content.res.ColorStateList r4 = j.c.a.b.e0.RippleUtils.a(r4)
            android.graphics.drawable.LayerDrawable r15 = new android.graphics.drawable.LayerDrawable
            r14 = 2
            android.graphics.drawable.Drawable[] r14 = new android.graphics.drawable.Drawable[r14]
            r14[r9] = r13
            r13 = 1
            r14[r13] = r12
            r15.<init>(r14)
            android.graphics.drawable.InsetDrawable r12 = new android.graphics.drawable.InsetDrawable
            int r14 = r5.c
            int r9 = r5.f2328e
            int r13 = r5.d
            int r0 = r5.f2329f
            r16 = r14
            r14 = r12
            r17 = r9
            r18 = r13
            r19 = r0
            r14.<init>(r15, r16, r17, r18, r19)
            android.graphics.drawable.Drawable r0 = r5.f2334m
            r3.<init>(r4, r12, r0)
            r5.f2339r = r3
            r11.setInternalBackground(r3)
            j.c.a.b.g0.MaterialShapeDrawable r0 = r5.b()
            if (r0 == 0) goto L_0x01b4
            float r2 = (float) r2
            r0.a(r2)
        L_0x01b4:
            com.google.android.material.button.MaterialButton r0 = r5.a
            int r2 = r5.c
            int r6 = r6 + r2
            int r2 = r5.f2328e
            int r7 = r7 + r2
            int r2 = r5.d
            int r8 = r8 + r2
            int r2 = r5.f2329f
            int r10 = r10 + r2
            r0.setPaddingRelative(r6, r7, r8, r10)
            r1.recycle()
            r0 = r20
            int r1 = r0.f435l
            r0.setCompoundDrawablePadding(r1)
            android.graphics.drawable.Drawable r1 = r0.f432i
            if (r1 == 0) goto L_0x01d5
            r9 = 1
            goto L_0x01d6
        L_0x01d5:
            r9 = 0
        L_0x01d6:
            r0.a(r9)
            return
        L_0x01da:
            r1 = 0
            throw r1
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.material.button.MaterialButton.<init>(android.content.Context, android.util.AttributeSet, int):void");
    }

    public boolean a() {
        MaterialButtonHelper materialButtonHelper = this.d;
        return materialButtonHelper != null && materialButtonHelper.f2338q;
    }
}
