package com.google.android.gms.measurement.module;

import android.content.Context;
import androidx.annotation.Keep;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.nb;
import j.c.a.a.g.a.r4;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class Analytics {
    public static volatile Analytics a;

    public Analytics(r4 r4Var) {
        ResourcesFlusher.b(r4Var);
    }

    @Keep
    public static Analytics getInstance(Context context) {
        if (a == null) {
            synchronized (Analytics.class) {
                if (a == null) {
                    a = new Analytics(r4.a(context, (nb) null));
                }
            }
        }
        return a;
    }
}
