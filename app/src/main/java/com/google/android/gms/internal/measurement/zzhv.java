package com.google.android.gms.internal.measurement;

/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public final class zzhv extends RuntimeException {
    public zzhv() {
        super("Message was missing required fields.  (Lite runtime could not determine which fields were missing).");
    }
}
