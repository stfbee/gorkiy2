package com.google.android.gms.measurement;

import android.content.Context;
import android.os.Bundle;
import android.os.SystemClock;
import androidx.annotation.Keep;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import j.c.a.a.c.n.c;
import j.c.a.a.g.a.a;
import j.c.a.a.g.a.r4;
import j.c.a.a.g.a.s6;
import j.c.a.a.g.a.v5;
import j.c.a.a.g.a.w6;
import j.c.a.a.g.a.x5;
import j.c.a.a.g.a.y6;
import j.c.a.a.g.b;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Deprecated
/* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
public class AppMeasurement {
    public static volatile AppMeasurement d;
    public final r4 a;
    public final s6 b;
    public final boolean c;

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public static class ConditionalUserProperty {
        @Keep
        public boolean mActive;
        @Keep
        public String mAppId;
        @Keep
        public long mCreationTimestamp;
        @Keep
        public String mExpiredEventName;
        @Keep
        public Bundle mExpiredEventParams;
        @Keep
        public String mName;
        @Keep
        public String mOrigin;
        @Keep
        public long mTimeToLive;
        @Keep
        public String mTimedOutEventName;
        @Keep
        public Bundle mTimedOutEventParams;
        @Keep
        public String mTriggerEventName;
        @Keep
        public long mTriggerTimeout;
        @Keep
        public String mTriggeredEventName;
        @Keep
        public Bundle mTriggeredEventParams;
        @Keep
        public long mTriggeredTimestamp;
        @Keep
        public Object mValue;

        public ConditionalUserProperty() {
        }

        public static /* synthetic */ Bundle a(ConditionalUserProperty conditionalUserProperty) {
            if (conditionalUserProperty != null) {
                Bundle bundle = new Bundle();
                String str = conditionalUserProperty.mAppId;
                if (str != null) {
                    bundle.putString("app_id", str);
                }
                String str2 = conditionalUserProperty.mOrigin;
                if (str2 != null) {
                    bundle.putString("origin", str2);
                }
                String str3 = conditionalUserProperty.mName;
                if (str3 != null) {
                    bundle.putString(DefaultAppMeasurementEventListenerRegistrar.NAME, str3);
                }
                Object obj = conditionalUserProperty.mValue;
                if (obj != null) {
                    c.a(bundle, obj);
                }
                String str4 = conditionalUserProperty.mTriggerEventName;
                if (str4 != null) {
                    bundle.putString("trigger_event_name", str4);
                }
                bundle.putLong("trigger_timeout", conditionalUserProperty.mTriggerTimeout);
                String str5 = conditionalUserProperty.mTimedOutEventName;
                if (str5 != null) {
                    bundle.putString("timed_out_event_name", str5);
                }
                Bundle bundle2 = conditionalUserProperty.mTimedOutEventParams;
                if (bundle2 != null) {
                    bundle.putBundle("timed_out_event_params", bundle2);
                }
                String str6 = conditionalUserProperty.mTriggeredEventName;
                if (str6 != null) {
                    bundle.putString("triggered_event_name", str6);
                }
                Bundle bundle3 = conditionalUserProperty.mTriggeredEventParams;
                if (bundle3 != null) {
                    bundle.putBundle("triggered_event_params", bundle3);
                }
                bundle.putLong("time_to_live", conditionalUserProperty.mTimeToLive);
                String str7 = conditionalUserProperty.mExpiredEventName;
                if (str7 != null) {
                    bundle.putString("expired_event_name", str7);
                }
                Bundle bundle4 = conditionalUserProperty.mExpiredEventParams;
                if (bundle4 != null) {
                    bundle.putBundle("expired_event_params", bundle4);
                }
                bundle.putLong("creation_timestamp", conditionalUserProperty.mCreationTimestamp);
                bundle.putBoolean("active", conditionalUserProperty.mActive);
                bundle.putLong("triggered_timestamp", conditionalUserProperty.mTriggeredTimestamp);
                return bundle;
            }
            throw null;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.String, java.lang.Class, java.lang.Object):T
         arg types: [android.os.Bundle, java.lang.String, java.lang.Class<java.lang.Long>, long]
         candidates:
          j.c.a.a.c.n.c.a(float, float, float, float):float
          j.c.a.a.c.n.c.a(int, byte[], int, j.c.a.a.f.e.s2):int
          j.c.a.a.c.n.c.a(byte[], int, j.c.a.a.f.e.e4<?>, j.c.a.a.f.e.s2):int
          j.c.a.a.c.n.c.a(j.c.a.b.w.CircularRevealWidget, float, float, float):android.animation.Animator
          j.c.a.a.c.n.c.a(byte, byte, char[], int):void
          j.c.a.a.c.n.c.a(java.lang.StringBuilder, int, java.lang.String, java.lang.Object):void
          j.c.a.a.c.n.c.a(java.nio.ByteBuffer, java.nio.ByteBuffer, java.nio.ByteBuffer, int):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.String, java.lang.Class, java.lang.Object):T */
        public /* synthetic */ ConditionalUserProperty(Bundle bundle, b bVar) {
            Class<Long> cls = Long.class;
            Class<String> cls2 = String.class;
            ResourcesFlusher.b(bundle);
            this.mAppId = (String) c.a(bundle, "app_id", cls2, (Object) null);
            this.mOrigin = (String) c.a(bundle, "origin", cls2, (Object) null);
            this.mName = (String) c.a(bundle, DefaultAppMeasurementEventListenerRegistrar.NAME, cls2, (Object) null);
            this.mValue = c.a(bundle, "value", Object.class, (Object) null);
            this.mTriggerEventName = (String) c.a(bundle, "trigger_event_name", cls2, (Object) null);
            this.mTriggerTimeout = ((Long) c.a(bundle, "trigger_timeout", (Class) cls, (Object) 0L)).longValue();
            this.mTimedOutEventName = (String) c.a(bundle, "timed_out_event_name", cls2, (Object) null);
            this.mTimedOutEventParams = (Bundle) c.a(bundle, "timed_out_event_params", Bundle.class, (Object) null);
            this.mTriggeredEventName = (String) c.a(bundle, "triggered_event_name", cls2, (Object) null);
            this.mTriggeredEventParams = (Bundle) c.a(bundle, "triggered_event_params", Bundle.class, (Object) null);
            this.mTimeToLive = ((Long) c.a(bundle, "time_to_live", (Class) cls, (Object) 0L)).longValue();
            this.mExpiredEventName = (String) c.a(bundle, "expired_event_name", cls2, (Object) null);
            this.mExpiredEventParams = (Bundle) c.a(bundle, "expired_event_params", Bundle.class, (Object) null);
        }
    }

    /* compiled from: com.google.android.gms:play-services-measurement-impl@@17.0.1 */
    public interface OnEventListener extends v5 {
        void onEvent(String str, String str2, Bundle bundle, long j2);
    }

    public AppMeasurement(r4 r4Var) {
        ResourcesFlusher.b(r4Var);
        this.a = r4Var;
        this.b = null;
        this.c = false;
    }

    public static AppMeasurement a(Context context) {
        if (d == null) {
            synchronized (AppMeasurement.class) {
                if (d == null) {
                    s6 b2 = b(context, null);
                    if (b2 != null) {
                        d = new AppMeasurement(b2);
                    } else {
                        d = new AppMeasurement(r4.a(context, (Bundle) null));
                    }
                }
            }
        }
        return d;
    }

    /* JADX WARNING: No exception handlers in catch block: Catch:{  } */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public static j.c.a.a.g.a.s6 b(android.content.Context r8, android.os.Bundle r9) {
        /*
            r0 = 0
            java.lang.String r1 = "com.google.firebase.analytics.FirebaseAnalytics"
            java.lang.Class r1 = java.lang.Class.forName(r1)     // Catch:{ ClassNotFoundException -> 0x0027 }
            java.lang.String r2 = "getScionFrontendApiImplementation"
            r3 = 2
            java.lang.Class[] r4 = new java.lang.Class[r3]     // Catch:{  }
            java.lang.Class<android.content.Context> r5 = android.content.Context.class
            r6 = 0
            r4[r6] = r5     // Catch:{  }
            java.lang.Class<android.os.Bundle> r5 = android.os.Bundle.class
            r7 = 1
            r4[r7] = r5     // Catch:{  }
            java.lang.reflect.Method r1 = r1.getDeclaredMethod(r2, r4)     // Catch:{  }
            java.lang.Object[] r2 = new java.lang.Object[r3]     // Catch:{  }
            r2[r6] = r8     // Catch:{  }
            r2[r7] = r9     // Catch:{  }
            java.lang.Object r8 = r1.invoke(r0, r2)     // Catch:{  }
            j.c.a.a.g.a.s6 r8 = (j.c.a.a.g.a.s6) r8     // Catch:{  }
            return r8
        L_0x0027:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.android.gms.measurement.AppMeasurement.b(android.content.Context, android.os.Bundle):j.c.a.a.g.a.s6");
    }

    @Deprecated
    @Keep
    public static AppMeasurement getInstance(Context context) {
        return a(context);
    }

    @Keep
    public void beginAdUnitExposure(String str) {
        if (this.c) {
            this.b.a(str);
            return;
        }
        a u = this.a.u();
        if (((j.c.a.a.c.n.b) this.a.f2092n) != null) {
            u.a(str, SystemClock.elapsedRealtime());
            return;
        }
        throw null;
    }

    @Keep
    public void clearConditionalUserProperty(String str, String str2, Bundle bundle) {
        if (this.c) {
            this.b.a(str, str2, bundle);
            return;
        }
        x5 m2 = this.a.m();
        m2.b();
        m2.a((String) null, str, str2, bundle);
    }

    @Keep
    public void clearConditionalUserPropertyAs(String str, String str2, String str3, Bundle bundle) {
        if (!this.c) {
            x5 m2 = this.a.m();
            if (m2 != null) {
                ResourcesFlusher.b(str);
                m2.n();
                throw null;
            }
            throw null;
        }
        throw new IllegalStateException("Unexpected call on client side");
    }

    @Keep
    public void endAdUnitExposure(String str) {
        if (this.c) {
            this.b.c(str);
            return;
        }
        a u = this.a.u();
        if (((j.c.a.a.c.n.b) this.a.f2092n) != null) {
            u.b(str, SystemClock.elapsedRealtime());
            return;
        }
        throw null;
    }

    @Keep
    public long generateEventId() {
        if (this.c) {
            return this.b.e();
        }
        return this.a.n().t();
    }

    @Keep
    public String getAppInstanceId() {
        if (this.c) {
            return this.b.c();
        }
        x5 m2 = this.a.m();
        m2.b();
        return m2.g.get();
    }

    @Keep
    public List<ConditionalUserProperty> getConditionalUserProperties(String str, String str2) {
        List<Bundle> list;
        int i2;
        if (this.c) {
            list = this.b.a(str, str2);
        } else {
            x5 m2 = this.a.m();
            m2.b();
            list = m2.a((String) null, str, str2);
        }
        if (list == null) {
            i2 = 0;
        } else {
            i2 = list.size();
        }
        ArrayList arrayList = new ArrayList(i2);
        for (Bundle conditionalUserProperty : list) {
            arrayList.add(new ConditionalUserProperty(conditionalUserProperty, null));
        }
        return arrayList;
    }

    @Keep
    public List<ConditionalUserProperty> getConditionalUserPropertiesAs(String str, String str2, String str3) {
        if (!this.c) {
            x5 m2 = this.a.m();
            if (m2 != null) {
                ResourcesFlusher.b(str);
                m2.n();
                throw null;
            }
            throw null;
        }
        throw new IllegalStateException("Unexpected call on client side");
    }

    @Keep
    public String getCurrentScreenClass() {
        if (this.c) {
            return this.b.b();
        }
        y6 q2 = this.a.m().a.q();
        q2.b();
        w6 w6Var = q2.d;
        if (w6Var != null) {
            return w6Var.b;
        }
        return null;
    }

    @Keep
    public String getCurrentScreenName() {
        if (this.c) {
            return this.b.a();
        }
        y6 q2 = this.a.m().a.q();
        q2.b();
        w6 w6Var = q2.d;
        if (w6Var != null) {
            return w6Var.a;
        }
        return null;
    }

    @Keep
    public String getGmpAppId() {
        if (this.c) {
            return this.b.d();
        }
        return this.a.m().B();
    }

    @Keep
    public int getMaxUserProperties(String str) {
        if (this.c) {
            return this.b.b(str);
        }
        this.a.m();
        ResourcesFlusher.b(str);
        return 25;
    }

    @Keep
    public Map<String, Object> getUserProperties(String str, String str2, boolean z) {
        if (this.c) {
            return this.b.a(str, str2, z);
        }
        x5 m2 = this.a.m();
        m2.b();
        return m2.a((String) null, str, str2, z);
    }

    @Keep
    public Map<String, Object> getUserPropertiesAs(String str, String str2, String str3, boolean z) {
        if (!this.c) {
            x5 m2 = this.a.m();
            if (m2 != null) {
                ResourcesFlusher.b(str);
                m2.n();
                throw null;
            }
            throw null;
        }
        throw new IllegalStateException("Unexpected call on client side");
    }

    @Keep
    public void logEventInternal(String str, String str2, Bundle bundle) {
        if (this.c) {
            this.b.b(str, str2, bundle);
        } else {
            this.a.m().a(str, str2, bundle);
        }
    }

    public void registerOnMeasurementEventListener(OnEventListener onEventListener) {
        if (this.c) {
            this.b.a(onEventListener);
        } else {
            this.a.m().a(onEventListener);
        }
    }

    @Keep
    public void setConditionalUserProperty(ConditionalUserProperty conditionalUserProperty) {
        ResourcesFlusher.b(conditionalUserProperty);
        if (this.c) {
            this.b.a(ConditionalUserProperty.a(conditionalUserProperty));
            return;
        }
        x5 m2 = this.a.m();
        Bundle a2 = ConditionalUserProperty.a(conditionalUserProperty);
        if (((j.c.a.a.c.n.b) m2.a.f2092n) != null) {
            m2.a(a2, System.currentTimeMillis());
            return;
        }
        throw null;
    }

    @Keep
    public void setConditionalUserPropertyAs(ConditionalUserProperty conditionalUserProperty) {
        ResourcesFlusher.b(conditionalUserProperty);
        if (!this.c) {
            x5 m2 = this.a.m();
            Bundle a2 = ConditionalUserProperty.a(conditionalUserProperty);
            if (m2 != null) {
                ResourcesFlusher.b(a2);
                ResourcesFlusher.b(a2.getString("app_id"));
                m2.n();
                throw null;
            }
            throw null;
        }
        throw new IllegalStateException("Unexpected call on client side");
    }

    public AppMeasurement(s6 s6Var) {
        ResourcesFlusher.b(s6Var);
        this.b = s6Var;
        this.a = null;
        this.c = true;
    }

    public static AppMeasurement a(Context context, Bundle bundle) {
        if (d == null) {
            synchronized (AppMeasurement.class) {
                if (d == null) {
                    s6 b2 = b(context, bundle);
                    if (b2 != null) {
                        d = new AppMeasurement(b2);
                    } else {
                        d = new AppMeasurement(r4.a(context, bundle));
                    }
                }
            }
        }
        return d;
    }
}
