package com.google.android.gms.common.api;

import android.text.TextUtils;
import i.e.ArrayMap;
import j.c.a.a.c.b;
import j.c.a.a.c.k.e.z;
import java.util.ArrayList;
import java.util.Iterator;

public class AvailabilityException extends Exception {
    public final ArrayMap<z<?>, b> b;

    public String getMessage() {
        ArrayList arrayList = new ArrayList();
        Iterator<z<?>> it = this.b.keySet().iterator();
        if (it.hasNext()) {
            z next = it.next();
            this.b.get(next).c();
            if (next != null) {
                throw null;
            }
            throw null;
        }
        return "None of the queried APIs are available. " + TextUtils.join("; ", arrayList);
    }
}
