package com.google.firebase;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.UserManager;
import android.util.Base64;
import android.util.Log;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.b.k.ResourcesFlusher;
import i.e.ArrayMap;
import j.c.a.a.c.k.e.a;
import j.c.a.a.c.l.n;
import j.c.c.e.j;
import j.c.c.e.q;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.Executor;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import javax.annotation.concurrent.GuardedBy;

/* compiled from: com.google.firebase:firebase-common@@17.1.0 */
public class FirebaseApp {

    /* renamed from: i  reason: collision with root package name */
    public static final Object f550i = new Object();

    /* renamed from: j  reason: collision with root package name */
    public static final Executor f551j = new d(null);
    @GuardedBy("LOCK")

    /* renamed from: k  reason: collision with root package name */
    public static final Map<String, FirebaseApp> f552k = new ArrayMap();
    public final Context a;
    public final String b;
    public final j.c.c.c c;
    public final j d;

    /* renamed from: e  reason: collision with root package name */
    public final AtomicBoolean f553e = new AtomicBoolean(false);

    /* renamed from: f  reason: collision with root package name */
    public final AtomicBoolean f554f = new AtomicBoolean();
    public final q<j.c.c.i.a> g;
    public final List<b> h = new CopyOnWriteArrayList();

    /* compiled from: com.google.firebase:firebase-common@@17.1.0 */
    public interface b {
        void a(boolean z);
    }

    /* compiled from: com.google.firebase:firebase-common@@17.1.0 */
    public static class d implements Executor {
        public static final Handler a = new Handler(Looper.getMainLooper());

        public /* synthetic */ d(a aVar) {
        }

        public void execute(Runnable runnable) {
            a.post(runnable);
        }
    }

    @TargetApi(24)
    /* compiled from: com.google.firebase:firebase-common@@17.1.0 */
    public static class e extends BroadcastReceiver {
        public static AtomicReference<e> b = new AtomicReference<>();
        public final Context a;

        public e(Context context) {
            this.a = context;
        }

        public void onReceive(Context context, Intent intent) {
            synchronized (FirebaseApp.f550i) {
                for (FirebaseApp b2 : FirebaseApp.f552k.values()) {
                    b2.b();
                }
            }
            this.a.unregisterReceiver(super);
        }
    }

    /* JADX WARNING: Removed duplicated region for block: B:14:0x005d  */
    /* JADX WARNING: Removed duplicated region for block: B:15:0x0067  */
    /* JADX WARNING: Removed duplicated region for block: B:27:0x00b2  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public FirebaseApp(android.content.Context r12, java.lang.String r13, j.c.c.c r14) {
        /*
            r11 = this;
            r11.<init>()
            java.util.concurrent.atomic.AtomicBoolean r0 = new java.util.concurrent.atomic.AtomicBoolean
            r1 = 0
            r0.<init>(r1)
            r11.f553e = r0
            java.util.concurrent.atomic.AtomicBoolean r0 = new java.util.concurrent.atomic.AtomicBoolean
            r0.<init>()
            r11.f554f = r0
            java.util.concurrent.CopyOnWriteArrayList r0 = new java.util.concurrent.CopyOnWriteArrayList
            r0.<init>()
            r11.h = r0
            java.util.concurrent.CopyOnWriteArrayList r0 = new java.util.concurrent.CopyOnWriteArrayList
            r0.<init>()
            i.b.k.ResourcesFlusher.b(r12)
            r11.a = r12
            i.b.k.ResourcesFlusher.b(r13)
            r11.b = r13
            i.b.k.ResourcesFlusher.b(r14)
            r11.c = r14
            java.lang.String r13 = "ComponentDiscovery"
            android.content.pm.PackageManager r0 = r12.getPackageManager()     // Catch:{ NameNotFoundException -> 0x0053 }
            if (r0 != 0) goto L_0x003b
            java.lang.String r0 = "Context has no PackageManager."
            android.util.Log.w(r13, r0)     // Catch:{ NameNotFoundException -> 0x0053 }
            goto L_0x0058
        L_0x003b:
            android.content.ComponentName r2 = new android.content.ComponentName     // Catch:{ NameNotFoundException -> 0x0053 }
            java.lang.Class<com.google.firebase.components.ComponentDiscoveryService> r3 = com.google.firebase.components.ComponentDiscoveryService.class
            r2.<init>(r12, r3)     // Catch:{ NameNotFoundException -> 0x0053 }
            r3 = 128(0x80, float:1.794E-43)
            android.content.pm.ServiceInfo r0 = r0.getServiceInfo(r2, r3)     // Catch:{ NameNotFoundException -> 0x0053 }
            if (r0 != 0) goto L_0x0050
            java.lang.String r0 = "ComponentDiscoveryService has no service info."
            android.util.Log.w(r13, r0)     // Catch:{ NameNotFoundException -> 0x0053 }
            goto L_0x0058
        L_0x0050:
            android.os.Bundle r0 = r0.metaData     // Catch:{ NameNotFoundException -> 0x0053 }
            goto L_0x0059
        L_0x0053:
            java.lang.String r0 = "Application info not found."
            android.util.Log.w(r13, r0)
        L_0x0058:
            r0 = 0
        L_0x0059:
            java.lang.String r2 = "com.google.firebase.components.ComponentRegistrar"
            if (r0 != 0) goto L_0x0067
            java.lang.String r0 = "Could not retrieve metadata, returning empty list of registrars."
            android.util.Log.w(r13, r0)
            java.util.List r0 = java.util.Collections.emptyList()
            goto L_0x009d
        L_0x0067:
            java.util.ArrayList r3 = new java.util.ArrayList
            r3.<init>()
            java.util.Set r4 = r0.keySet()
            java.util.Iterator r4 = r4.iterator()
        L_0x0074:
            boolean r5 = r4.hasNext()
            if (r5 == 0) goto L_0x009c
            java.lang.Object r5 = r4.next()
            java.lang.String r5 = (java.lang.String) r5
            java.lang.Object r6 = r0.get(r5)
            boolean r6 = r2.equals(r6)
            if (r6 == 0) goto L_0x0074
            java.lang.String r6 = "com.google.firebase.components:"
            boolean r6 = r5.startsWith(r6)
            if (r6 == 0) goto L_0x0074
            r6 = 31
            java.lang.String r5 = r5.substring(r6)
            r3.add(r5)
            goto L_0x0074
        L_0x009c:
            r0 = r3
        L_0x009d:
            java.lang.String r3 = "Could not instantiate %s"
            java.lang.String r4 = "Could not instantiate %s."
            java.util.ArrayList r5 = new java.util.ArrayList
            r5.<init>()
            java.util.Iterator r0 = r0.iterator()
        L_0x00aa:
            boolean r6 = r0.hasNext()
            r7 = 1
            r8 = 2
            if (r6 == 0) goto L_0x0129
            java.lang.Object r6 = r0.next()
            java.lang.String r6 = (java.lang.String) r6
            java.lang.Class r9 = java.lang.Class.forName(r6)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            java.lang.Class<j.c.c.e.f> r10 = j.c.c.e.f.class
            boolean r10 = r10.isAssignableFrom(r9)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            if (r10 != 0) goto L_0x00d4
            java.lang.String r9 = "Class %s is not an instance of %s"
            java.lang.Object[] r8 = new java.lang.Object[r8]     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            r8[r1] = r6     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            r8[r7] = r2     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            java.lang.String r8 = java.lang.String.format(r9, r8)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            android.util.Log.w(r13, r8)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            goto L_0x00aa
        L_0x00d4:
            java.lang.Class[] r8 = new java.lang.Class[r1]     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            java.lang.reflect.Constructor r8 = r9.getDeclaredConstructor(r8)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            java.lang.Object[] r9 = new java.lang.Object[r1]     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            java.lang.Object r8 = r8.newInstance(r9)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            j.c.c.e.f r8 = (j.c.c.e.f) r8     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            r5.add(r8)     // Catch:{ ClassNotFoundException -> 0x011a, IllegalAccessException -> 0x010d, InstantiationException -> 0x0100, NoSuchMethodException -> 0x00f3, InvocationTargetException -> 0x00e6 }
            goto L_0x00aa
        L_0x00e6:
            r8 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r7[r1] = r6
            java.lang.String r6 = java.lang.String.format(r3, r7)
            android.util.Log.w(r13, r6, r8)
            goto L_0x00aa
        L_0x00f3:
            r8 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r7[r1] = r6
            java.lang.String r6 = java.lang.String.format(r3, r7)
            android.util.Log.w(r13, r6, r8)
            goto L_0x00aa
        L_0x0100:
            r8 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r7[r1] = r6
            java.lang.String r6 = java.lang.String.format(r4, r7)
            android.util.Log.w(r13, r6, r8)
            goto L_0x00aa
        L_0x010d:
            r8 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r7[r1] = r6
            java.lang.String r6 = java.lang.String.format(r4, r7)
            android.util.Log.w(r13, r6, r8)
            goto L_0x00aa
        L_0x011a:
            r8 = move-exception
            java.lang.Object[] r7 = new java.lang.Object[r7]
            r7[r1] = r6
            java.lang.String r6 = "Class %s is not an found."
            java.lang.String r6 = java.lang.String.format(r6, r7)
            android.util.Log.w(r13, r6, r8)
            goto L_0x00aa
        L_0x0129:
            j.c.c.e.j r13 = new j.c.c.e.j
            java.util.concurrent.Executor r0 = com.google.firebase.FirebaseApp.f551j
            r2 = 6
            j.c.c.e.d[] r2 = new j.c.c.e.d[r2]
            java.lang.Class<android.content.Context> r3 = android.content.Context.class
            java.lang.Class[] r4 = new java.lang.Class[r1]
            j.c.c.e.d r3 = j.c.c.e.d.a(r12, r3, r4)
            r2[r1] = r3
            java.lang.Class<com.google.firebase.FirebaseApp> r3 = com.google.firebase.FirebaseApp.class
            java.lang.Class[] r4 = new java.lang.Class[r1]
            j.c.c.e.d r3 = j.c.c.e.d.a(r11, r3, r4)
            r2[r7] = r3
            java.lang.Class<j.c.c.c> r3 = j.c.c.c.class
            java.lang.Class[] r4 = new java.lang.Class[r1]
            j.c.c.e.d r14 = j.c.c.e.d.a(r14, r3, r4)
            r2[r8] = r14
            r14 = 3
            java.lang.String r3 = "fire-android"
            java.lang.String r4 = ""
            j.c.c.e.d r3 = j.c.a.a.c.n.c.a(r3, r4)
            r2[r14] = r3
            r14 = 4
            java.lang.String r3 = "fire-core"
            java.lang.String r4 = "17.0.0"
            j.c.c.e.d r3 = j.c.a.a.c.n.c.a(r3, r4)
            r2[r14] = r3
            r14 = 5
            java.lang.Class<j.c.c.j.f> r3 = j.c.c.j.f.class
            j.c.c.e.d$b r3 = j.c.c.e.d.a(r3)
            java.lang.Class<j.c.c.j.e> r4 = j.c.c.j.e.class
            j.c.c.e.n r6 = new j.c.c.e.n
            r6.<init>(r4, r8, r1)
            r3.a(r6)
            j.c.c.j.b r1 = j.c.c.j.b.a
            r3.a(r1)
            j.c.c.e.d r1 = r3.a()
            r2[r14] = r1
            r13.<init>(r0, r5, r2)
            r11.d = r13
            j.c.c.e.q r13 = new j.c.c.e.q
            j.c.c.b r14 = new j.c.c.b
            r14.<init>(r11, r12)
            r13.<init>(r14)
            r11.g = r13
            return
        */
        throw new UnsupportedOperationException("Method not decompiled: com.google.firebase.FirebaseApp.<init>(android.content.Context, java.lang.String, j.c.c.c):void");
    }

    public static FirebaseApp a(Context context) {
        synchronized (f550i) {
            if (f552k.containsKey("[DEFAULT]")) {
                FirebaseApp instance = getInstance();
                return instance;
            }
            j.c.c.c a2 = j.c.c.c.a(context);
            if (a2 == null) {
                Log.w("FirebaseApp", "Default FirebaseApp failed to initialize because no default options were found. This usually means that com.google.gms:google-services was not applied to your gradle project.");
                return null;
            }
            FirebaseApp a3 = a(context, a2, "[DEFAULT]");
            return a3;
        }
    }

    public static FirebaseApp getInstance() {
        FirebaseApp firebaseApp;
        synchronized (f550i) {
            firebaseApp = f552k.get("[DEFAULT]");
            if (firebaseApp == null) {
                throw new IllegalStateException("Default FirebaseApp is not initialized in this process " + j.c.a.a.c.n.e.a() + ". Make sure to call FirebaseApp.initializeApp(Context) first.");
            }
        }
        return firebaseApp;
    }

    public final void b() {
        if (!(Build.VERSION.SDK_INT >= 24 ? ((UserManager) this.a.getSystemService(UserManager.class)).isUserUnlocked() : true)) {
            Context context = this.a;
            if (e.b.get() == null) {
                e eVar = new e(context);
                if (e.b.compareAndSet(null, eVar)) {
                    context.registerReceiver(eVar, new IntentFilter("android.intent.action.USER_UNLOCKED"));
                    return;
                }
                return;
            }
            return;
        }
        j jVar = this.d;
        a();
        boolean equals = "[DEFAULT]".equals(this.b);
        for (Map.Entry next : jVar.a.entrySet()) {
            j.c.c.e.d dVar = (j.c.c.e.d) next.getKey();
            q qVar = (q) next.getValue();
            boolean z = false;
            if (!(dVar.c == 1)) {
                if (dVar.c == 2) {
                    z = true;
                }
                if (z) {
                    if (!equals) {
                    }
                }
            }
            qVar.get();
        }
        jVar.d.a();
    }

    public boolean equals(Object obj) {
        if (!(obj instanceof FirebaseApp)) {
            return false;
        }
        String str = this.b;
        FirebaseApp firebaseApp = (FirebaseApp) obj;
        firebaseApp.a();
        return str.equals(firebaseApp.b);
    }

    public int hashCode() {
        return this.b.hashCode();
    }

    public boolean isDataCollectionDefaultEnabled() {
        a();
        return this.g.get().c.get();
    }

    public String toString() {
        n d2 = ResourcesFlusher.d(this);
        d2.a(DefaultAppMeasurementEventListenerRegistrar.NAME, this.b);
        d2.a("options", this.c);
        return d2.toString();
    }

    @TargetApi(14)
    /* compiled from: com.google.firebase:firebase-common@@17.1.0 */
    public static class c implements a.C0021a {
        public static AtomicReference<c> a = new AtomicReference<>();

        public void a(boolean z) {
            synchronized (FirebaseApp.f550i) {
                Iterator it = new ArrayList(FirebaseApp.f552k.values()).iterator();
                while (it.hasNext()) {
                    FirebaseApp firebaseApp = (FirebaseApp) it.next();
                    if (firebaseApp.f553e.get()) {
                        Log.d("FirebaseApp", "Notifying background state change listeners.");
                        for (b a2 : firebaseApp.h) {
                            a2.a(z);
                        }
                    }
                }
            }
        }

        public static /* synthetic */ void a(Context context) {
            if (context.getApplicationContext() instanceof Application) {
                Application application = (Application) context.getApplicationContext();
                if (a.get() == null) {
                    c cVar = new c();
                    if (a.compareAndSet(null, cVar)) {
                        j.c.a.a.c.k.e.a.a(application);
                        j.c.a.a.c.k.e.a.f1777f.a(cVar);
                    }
                }
            }
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void */
    public static FirebaseApp a(Context context, j.c.c.c cVar, String str) {
        FirebaseApp firebaseApp;
        c.a(context);
        String trim = str.trim();
        if (context.getApplicationContext() != null) {
            context = context.getApplicationContext();
        }
        synchronized (f550i) {
            boolean z = !f552k.containsKey(trim);
            ResourcesFlusher.a(z, (Object) ("FirebaseApp name " + trim + " already exists!"));
            ResourcesFlusher.b(context, "Application context cannot be null.");
            firebaseApp = new FirebaseApp(context, trim, cVar);
            f552k.put(trim, firebaseApp);
        }
        firebaseApp.b();
        return firebaseApp;
    }

    public <T> T a(Class<T> cls) {
        a();
        return this.d.a(cls);
    }

    public static /* synthetic */ j.c.c.i.a a(FirebaseApp firebaseApp, Context context) {
        String str;
        StringBuilder sb = new StringBuilder();
        firebaseApp.a();
        byte[] bytes = firebaseApp.b.getBytes(Charset.defaultCharset());
        String str2 = null;
        if (bytes == null) {
            str = null;
        } else {
            str = Base64.encodeToString(bytes, 11);
        }
        sb.append(str);
        sb.append("+");
        firebaseApp.a();
        byte[] bytes2 = firebaseApp.c.b.getBytes(Charset.defaultCharset());
        if (bytes2 != null) {
            str2 = Base64.encodeToString(bytes2, 11);
        }
        sb.append(str2);
        return new j.c.c.i.a(context, sb.toString(), (j.c.c.f.c) firebaseApp.d.a(j.c.c.f.c.class));
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void
     arg types: [boolean, java.lang.String]
     candidates:
      i.b.k.ResourcesFlusher.a(android.content.Context, java.lang.String):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidget, int):int
      i.b.k.ResourcesFlusher.a(i.f.a.h.ConstraintWidgetGroup, int):int
      i.b.k.ResourcesFlusher.a(android.animation.Keyframe, float):android.animation.Keyframe
      i.b.k.ResourcesFlusher.a(android.content.Context, android.content.ComponentName):android.content.Intent
      i.b.k.ResourcesFlusher.a(android.os.Parcel, int):android.os.Bundle
      i.b.k.ResourcesFlusher.a(android.widget.TextView, android.view.ActionMode$Callback):android.view.ActionMode$Callback
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, android.content.res.Resources):i.h.e.b.FontResourcesParserCompat
      i.b.k.ResourcesFlusher.a(androidx.fragment.app.Fragment, i.o.ViewModelProvider$b):i.o.ViewModelProvider
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.Object):T
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.String):T
      i.b.k.ResourcesFlusher.a(android.content.res.Resources, int):java.util.List<java.util.List<byte[]>>
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.content.res.ColorStateList):void
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, android.graphics.PorterDuff$Mode):void
      i.b.k.ResourcesFlusher.a(android.view.View, java.lang.CharSequence):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, int):void
      i.b.k.ResourcesFlusher.a(android.widget.PopupWindow, boolean):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, int):void
      i.b.k.ResourcesFlusher.a(android.widget.TextView, i.h.j.PrecomputedTextCompat):void
      i.b.k.ResourcesFlusher.a(java.lang.Object, java.lang.StringBuilder):void
      i.b.k.ResourcesFlusher.a(boolean, java.lang.String):void
      i.b.k.ResourcesFlusher.a(int, int):boolean
      i.b.k.ResourcesFlusher.a(android.graphics.drawable.Drawable, int):boolean
      i.b.k.ResourcesFlusher.a(java.io.File, java.io.InputStream):boolean
      i.b.k.ResourcesFlusher.a(org.xmlpull.v1.XmlPullParser, java.lang.String):boolean
      i.b.k.ResourcesFlusher.a(i.h.f.PathParser[], i.h.f.PathParser[]):boolean
      i.b.k.ResourcesFlusher.a(boolean, java.lang.Object):void */
    public final void a() {
        ResourcesFlusher.a(!this.f554f.get(), (Object) "FirebaseApp was deleted");
    }
}
