package com.google.firebase.analytics;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import androidx.annotation.Keep;
import com.google.firebase.iid.FirebaseInstanceId;
import i.b.k.ResourcesFlusher;
import j.c.a.a.f.e.d;
import j.c.a.a.f.e.nb;
import j.c.a.a.f.e.pb;
import j.c.a.a.g.a.h9;
import j.c.a.a.g.a.r4;
import j.c.a.a.g.a.s6;
import j.c.c.d.b;

/* compiled from: com.google.android.gms:play-services-measurement-api@@17.0.1 */
public final class FirebaseAnalytics {
    public static volatile FirebaseAnalytics d;
    public final r4 a;
    public final pb b;
    public final boolean c;

    public FirebaseAnalytics(r4 r4Var) {
        ResourcesFlusher.b(r4Var);
        this.a = r4Var;
        this.b = null;
        this.c = false;
    }

    @Keep
    public static FirebaseAnalytics getInstance(Context context) {
        if (d == null) {
            synchronized (FirebaseAnalytics.class) {
                if (d == null) {
                    if (pb.a(context)) {
                        d = new FirebaseAnalytics(pb.a(context, null, null, null, null));
                    } else {
                        d = new FirebaseAnalytics(r4.a(context, (nb) null));
                    }
                }
            }
        }
        return d;
    }

    @Keep
    public static s6 getScionFrontendApiImplementation(Context context, Bundle bundle) {
        pb a2;
        if (pb.a(context) && (a2 = pb.a(context, null, null, null, bundle)) != null) {
            return new b(a2);
        }
        return null;
    }

    @Keep
    public final String getFirebaseInstanceId() {
        FirebaseInstanceId.f().b();
        return FirebaseInstanceId.g();
    }

    @Keep
    public final void setCurrentScreen(Activity activity, String str, String str2) {
        if (this.c) {
            pb pbVar = this.b;
            if (pbVar != null) {
                pbVar.c.execute(new d(pbVar, activity, str, str2));
                return;
            }
            throw null;
        } else if (!h9.a()) {
            this.a.a().f2047i.a("setCurrentScreen must be called from the main thread");
        } else {
            this.a.q().a(activity, str, str2);
        }
    }

    public FirebaseAnalytics(pb pbVar) {
        ResourcesFlusher.b(pbVar);
        this.a = null;
        this.b = pbVar;
        this.c = true;
    }
}
