package com.crashlytics.android.answers;

import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.io.File;
import java.util.List;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.AbstractSpiCall;
import l.a.a.a.o.d.FilesSender;
import l.a.a.a.o.e.HttpMethod;
import l.a.a.a.o.e.HttpRequestFactory;

public class SessionAnalyticsFilesSender extends AbstractSpiCall implements FilesSender {
    public static final String FILE_CONTENT_TYPE = "application/vnd.crashlytics.android.events";
    public static final String FILE_PARAM_NAME = "session_analytics_file_";
    public final String apiKey;

    public SessionAnalyticsFilesSender(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory, String str3) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
        this.apiKey = str3;
    }

    public boolean send(List<File> list) {
        HttpRequest httpRequest = getHttpRequest();
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_CLIENT_VERSION, super.kit.getVersion());
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_API_KEY, this.apiKey);
        int i2 = 0;
        for (File next : list) {
            httpRequest.a(outline.b(FILE_PARAM_NAME, i2), next.getName(), FILE_CONTENT_TYPE, next);
            i2++;
        }
        DefaultLogger a = Fabric.a();
        StringBuilder a2 = outline.a("Sending ");
        a2.append(list.size());
        a2.append(" analytics files to ");
        a2.append(getUrl());
        String sb = a2.toString();
        if (a.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, sb, null);
        }
        int c = httpRequest.c();
        DefaultLogger a3 = Fabric.a();
        String b = outline.b("Response code for analytics file send is ", c);
        if (a3.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, b, null);
        }
        if (c.c(c) == 0) {
            return true;
        }
        return false;
    }
}
