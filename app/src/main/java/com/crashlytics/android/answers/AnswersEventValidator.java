package com.crashlytics.android.answers;

import android.util.Log;
import j.a.a.a.outline;
import java.util.Locale;
import java.util.Map;
import l.a.a.a.Fabric;

public class AnswersEventValidator {
    public boolean failFast;
    public final int maxNumAttributes;
    public final int maxStringLength;

    public AnswersEventValidator(int i2, int i3, boolean z) {
        this.maxNumAttributes = i2;
        this.maxStringLength = i3;
        this.failFast = z;
    }

    private void logOrThrowException(RuntimeException runtimeException) {
        if (this.failFast) {
            throw runtimeException;
        } else if (Fabric.a().a(Answers.TAG, 6)) {
            Log.e(Answers.TAG, "Invalid user input detected", runtimeException);
        }
    }

    public boolean isFullMap(Map<String, Object> map, String str) {
        if (map.size() < this.maxNumAttributes || map.containsKey(str)) {
            return false;
        }
        logOrThrowException(new IllegalArgumentException(String.format(Locale.US, "Limit of %d attributes reached, skipping attribute", Integer.valueOf(this.maxNumAttributes))));
        return true;
    }

    public boolean isNull(Object obj, String str) {
        if (obj != null) {
            return false;
        }
        logOrThrowException(new NullPointerException(outline.a(str, " must not be null")));
        return true;
    }

    public String limitStringLength(String str) {
        int length = str.length();
        int i2 = this.maxStringLength;
        if (length <= i2) {
            return str;
        }
        logOrThrowException(new IllegalArgumentException(String.format(Locale.US, "String is too long, truncating to %d characters", Integer.valueOf(i2))));
        return str.substring(0, this.maxStringLength);
    }
}
