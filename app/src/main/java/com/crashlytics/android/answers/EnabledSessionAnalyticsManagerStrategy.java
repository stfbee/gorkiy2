package com.crashlytics.android.answers;

import android.content.Context;
import android.util.Log;
import com.crashlytics.android.answers.SessionEvent;
import j.a.a.a.outline;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicReference;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.ApiKey;
import l.a.a.a.o.b.CommonUtils;
import l.a.a.a.o.d.FilesSender;
import l.a.a.a.o.d.TimeBasedFileRollOverRunnable;
import l.a.a.a.o.e.HttpRequestFactory;
import l.a.a.a.o.g.AnalyticsSettingsData;

public class EnabledSessionAnalyticsManagerStrategy implements SessionAnalyticsManagerStrategy {
    public static final int UNDEFINED_ROLLOVER_INTERVAL_SECONDS = -1;
    public ApiKey apiKey = new ApiKey();
    public final Context context;
    public boolean customEventsEnabled = true;
    public EventFilter eventFilter = new KeepAllEventFilter();
    public final ScheduledExecutorService executorService;
    public final SessionAnalyticsFilesManager filesManager;
    public FilesSender filesSender;
    public final FirebaseAnalyticsApiAdapter firebaseAnalyticsApiAdapter;
    public boolean forwardToFirebaseAnalyticsEnabled = false;
    public final HttpRequestFactory httpRequestFactory;
    public boolean includePurchaseEventsInForwardedEvents = false;
    public final Kit kit;
    public final SessionEventMetadata metadata;
    public boolean predefinedEventsEnabled = true;
    public final AtomicReference<ScheduledFuture<?>> rolloverFutureRef = new AtomicReference<>();
    public volatile int rolloverIntervalSeconds = -1;

    public EnabledSessionAnalyticsManagerStrategy(Kit kit2, Context context2, ScheduledExecutorService scheduledExecutorService, SessionAnalyticsFilesManager sessionAnalyticsFilesManager, HttpRequestFactory httpRequestFactory2, SessionEventMetadata sessionEventMetadata, FirebaseAnalyticsApiAdapter firebaseAnalyticsApiAdapter2) {
        this.kit = kit2;
        this.context = context2;
        this.executorService = scheduledExecutorService;
        this.filesManager = sessionAnalyticsFilesManager;
        this.httpRequestFactory = httpRequestFactory2;
        this.metadata = sessionEventMetadata;
        this.firebaseAnalyticsApiAdapter = firebaseAnalyticsApiAdapter2;
    }

    public void cancelTimeBasedFileRollOver() {
        if (this.rolloverFutureRef.get() != null) {
            CommonUtils.b(this.context, "Cancelling time-based rollover because no events are currently being generated.");
            this.rolloverFutureRef.get().cancel(false);
            this.rolloverFutureRef.set(null);
        }
    }

    public void deleteAllEvents() {
        this.filesManager.deleteAllEventsFiles();
    }

    public void processEvent(SessionEvent.Builder builder) {
        SessionEvent build = builder.build(this.metadata);
        if (!this.customEventsEnabled && SessionEvent.Type.CUSTOM.equals(build.type)) {
            String str = "Custom events tracking disabled - skipping event: " + build;
            if (Fabric.a().a(Answers.TAG, 3)) {
                Log.d(Answers.TAG, str, null);
            }
        } else if (!this.predefinedEventsEnabled && SessionEvent.Type.PREDEFINED.equals(build.type)) {
            String str2 = "Predefined events tracking disabled - skipping event: " + build;
            if (Fabric.a().a(Answers.TAG, 3)) {
                Log.d(Answers.TAG, str2, null);
            }
        } else if (this.eventFilter.skipEvent(build)) {
            String str3 = "Skipping filtered event: " + build;
            if (Fabric.a().a(Answers.TAG, 3)) {
                Log.d(Answers.TAG, str3, null);
            }
        } else {
            try {
                this.filesManager.writeEvent(build);
            } catch (IOException e2) {
                String str4 = "Failed to write event: " + build;
                if (Fabric.a().a(Answers.TAG, 6)) {
                    Log.e(Answers.TAG, str4, e2);
                }
            }
            scheduleTimeBasedRollOverIfNeeded();
            boolean z = SessionEvent.Type.CUSTOM.equals(build.type) || SessionEvent.Type.PREDEFINED.equals(build.type);
            boolean equals = PurchaseEvent.TYPE.equals(build.predefinedType);
            if (this.forwardToFirebaseAnalyticsEnabled && z) {
                if (!equals || this.includePurchaseEventsInForwardedEvents) {
                    try {
                        this.firebaseAnalyticsApiAdapter.processEvent(build);
                    } catch (Exception e3) {
                        String str5 = "Failed to map event to Firebase: " + build;
                        if (Fabric.a().a(Answers.TAG, 6)) {
                            Log.e(Answers.TAG, str5, e3);
                        }
                    }
                }
            }
        }
    }

    public boolean rollFileOver() {
        try {
            return this.filesManager.rollFileOver();
        } catch (IOException unused) {
            CommonUtils.c(this.context, "Failed to roll file over.");
            return false;
        }
    }

    public void scheduleTimeBasedFileRollOver(long j2, long j3) {
        if (this.rolloverFutureRef.get() == null) {
            TimeBasedFileRollOverRunnable timeBasedFileRollOverRunnable = new TimeBasedFileRollOverRunnable(this.context, this);
            Context context2 = this.context;
            CommonUtils.b(context2, "Scheduling time based file roll over every " + j3 + " seconds");
            try {
                this.rolloverFutureRef.set(this.executorService.scheduleAtFixedRate(timeBasedFileRollOverRunnable, j2, j3, TimeUnit.SECONDS));
            } catch (RejectedExecutionException unused) {
                CommonUtils.c(this.context, "Failed to schedule time based file roll over");
            }
        }
    }

    public void scheduleTimeBasedRollOverIfNeeded() {
        if (this.rolloverIntervalSeconds != -1) {
            scheduleTimeBasedFileRollOver((long) this.rolloverIntervalSeconds, (long) this.rolloverIntervalSeconds);
        }
    }

    public void sendEvents() {
        if (this.filesSender == null) {
            CommonUtils.b(this.context, "skipping files send because we don't yet know the target endpoint");
            return;
        }
        CommonUtils.b(this.context, "Sending all files");
        List<File> batchOfFilesToSend = this.filesManager.getBatchOfFilesToSend();
        int i2 = 0;
        while (true) {
            try {
                if (batchOfFilesToSend.size() <= 0) {
                    break;
                }
                CommonUtils.b(this.context, String.format(Locale.US, "attempt to send batch of %d files", Integer.valueOf(batchOfFilesToSend.size())));
                boolean send = this.filesSender.send(batchOfFilesToSend);
                if (send) {
                    i2 += batchOfFilesToSend.size();
                    this.filesManager.deleteSentFiles(batchOfFilesToSend);
                }
                if (!send) {
                    break;
                }
                batchOfFilesToSend = this.filesManager.getBatchOfFilesToSend();
            } catch (Exception e2) {
                Context context2 = this.context;
                StringBuilder a = outline.a("Failed to send batch of analytics files to server: ");
                a.append(e2.getMessage());
                CommonUtils.c(context2, a.toString());
            }
        }
        if (i2 == 0) {
            this.filesManager.deleteOldestInRollOverIfOverMax();
        }
    }

    public void setAnalyticsSettingsData(AnalyticsSettingsData analyticsSettingsData, String str) {
        String str2;
        String str3;
        this.filesSender = AnswersRetryFilesSender.build(new SessionAnalyticsFilesSender(this.kit, str, analyticsSettingsData.a, this.httpRequestFactory, this.apiKey.c(this.context)));
        this.filesManager.setAnalyticsSettingsData(analyticsSettingsData);
        this.forwardToFirebaseAnalyticsEnabled = analyticsSettingsData.f2656e;
        this.includePurchaseEventsInForwardedEvents = analyticsSettingsData.f2657f;
        DefaultLogger a = Fabric.a();
        StringBuilder a2 = outline.a("Firebase analytics forwarding ");
        String str4 = "enabled";
        a2.append(this.forwardToFirebaseAnalyticsEnabled ? str4 : "disabled");
        String sb = a2.toString();
        if (a.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, sb, null);
        }
        DefaultLogger a3 = Fabric.a();
        StringBuilder a4 = outline.a("Firebase analytics including purchase events ");
        if (this.includePurchaseEventsInForwardedEvents) {
            str2 = str4;
        } else {
            str2 = "disabled";
        }
        a4.append(str2);
        String sb2 = a4.toString();
        if (a3.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, sb2, null);
        }
        this.customEventsEnabled = analyticsSettingsData.g;
        DefaultLogger a5 = Fabric.a();
        StringBuilder a6 = outline.a("Custom event tracking ");
        if (this.customEventsEnabled) {
            str3 = str4;
        } else {
            str3 = "disabled";
        }
        a6.append(str3);
        String sb3 = a6.toString();
        if (a5.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, sb3, null);
        }
        this.predefinedEventsEnabled = analyticsSettingsData.h;
        DefaultLogger a7 = Fabric.a();
        StringBuilder a8 = outline.a("Predefined event tracking ");
        if (!this.predefinedEventsEnabled) {
            str4 = "disabled";
        }
        a8.append(str4);
        String sb4 = a8.toString();
        if (a7.a(Answers.TAG, 3)) {
            Log.d(Answers.TAG, sb4, null);
        }
        if (analyticsSettingsData.f2659j > 1) {
            if (Fabric.a().a(Answers.TAG, 3)) {
                Log.d(Answers.TAG, "Event sampling enabled", null);
            }
            this.eventFilter = new SamplingEventFilter(analyticsSettingsData.f2659j);
        }
        this.rolloverIntervalSeconds = analyticsSettingsData.b;
        scheduleTimeBasedFileRollOver(0, (long) this.rolloverIntervalSeconds);
    }
}
