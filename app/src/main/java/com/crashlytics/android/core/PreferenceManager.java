package com.crashlytics.android.core;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import l.a.a.a.o.f.PreferenceStore;
import l.a.a.a.o.f.PreferenceStoreImpl;

@SuppressLint({"CommitPrefEdits"})
public class PreferenceManager {
    public static final String PREF_ALWAYS_SEND_REPORTS_KEY = "always_send_reports_opt_in";
    public static final String PREF_MIGRATION_COMPLETE = "preferences_migration_complete";
    public static final boolean SHOULD_ALWAYS_SEND_REPORTS_DEFAULT = false;
    public final CrashlyticsCore kit;
    public final PreferenceStore preferenceStore;

    public PreferenceManager(PreferenceStore preferenceStore2, CrashlyticsCore crashlyticsCore) {
        this.preferenceStore = preferenceStore2;
        this.kit = crashlyticsCore;
    }

    public static PreferenceManager create(PreferenceStore preferenceStore2, CrashlyticsCore crashlyticsCore) {
        return new PreferenceManager(preferenceStore2, crashlyticsCore);
    }

    public void setShouldAlwaysSendReports(boolean z) {
        PreferenceStore preferenceStore2 = this.preferenceStore;
        SharedPreferences.Editor putBoolean = ((PreferenceStoreImpl) preferenceStore2).a().putBoolean(PREF_ALWAYS_SEND_REPORTS_KEY, z);
        if (((PreferenceStoreImpl) preferenceStore2) != null) {
            putBoolean.apply();
            return;
        }
        throw null;
    }

    public boolean shouldAlwaysSendReports() {
        if (!((PreferenceStoreImpl) this.preferenceStore).a.contains(PREF_MIGRATION_COMPLETE)) {
            Context context = this.kit.getContext();
            String name = CrashlyticsCore.class.getName();
            if (context != null) {
                SharedPreferences sharedPreferences = context.getSharedPreferences(name, 0);
                if (!((PreferenceStoreImpl) this.preferenceStore).a.contains(PREF_ALWAYS_SEND_REPORTS_KEY) && sharedPreferences.contains(PREF_ALWAYS_SEND_REPORTS_KEY)) {
                    boolean z = sharedPreferences.getBoolean(PREF_ALWAYS_SEND_REPORTS_KEY, false);
                    PreferenceStoreImpl preferenceStoreImpl = (PreferenceStoreImpl) this.preferenceStore;
                    SharedPreferences.Editor putBoolean = preferenceStoreImpl.a().putBoolean(PREF_ALWAYS_SEND_REPORTS_KEY, z);
                    if (preferenceStoreImpl != null) {
                        putBoolean.apply();
                    } else {
                        throw null;
                    }
                }
                PreferenceStoreImpl preferenceStoreImpl2 = (PreferenceStoreImpl) this.preferenceStore;
                SharedPreferences.Editor putBoolean2 = preferenceStoreImpl2.a().putBoolean(PREF_MIGRATION_COMPLETE, true);
                if (preferenceStoreImpl2 != null) {
                    putBoolean2.apply();
                } else {
                    throw null;
                }
            } else {
                throw new IllegalStateException("Cannot get directory before context has been set. Call Fabric.with() first");
            }
        }
        return ((PreferenceStoreImpl) this.preferenceStore).a.getBoolean(PREF_ALWAYS_SEND_REPORTS_KEY, false);
    }
}
