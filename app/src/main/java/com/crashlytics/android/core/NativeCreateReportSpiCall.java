package com.crashlytics.android.core;

import android.util.Log;
import io.fabric.sdk.android.services.network.HttpRequest;
import j.a.a.a.outline;
import j.c.a.a.c.n.c;
import java.io.File;
import l.a.a.a.DefaultLogger;
import l.a.a.a.Fabric;
import l.a.a.a.Kit;
import l.a.a.a.o.b.AbstractSpiCall;
import l.a.a.a.o.e.HttpMethod;
import l.a.a.a.o.e.HttpRequestFactory;

public class NativeCreateReportSpiCall extends AbstractSpiCall implements CreateReportSpiCall {
    public static final String APP_META_FILE_MULTIPART_PARAM = "app_meta_file";
    public static final String BINARY_IMAGES_FILE_MULTIPART_PARAM = "binary_images_file";
    public static final String DEVICE_META_FILE_MULTIPART_PARAM = "device_meta_file";
    public static final String GZIP_FILE_CONTENT_TYPE = "application/octet-stream";
    public static final String KEYS_FILE_MULTIPART_PARAM = "keys_file";
    public static final String LOGS_FILE_MULTIPART_PARAM = "logs_file";
    public static final String METADATA_FILE_MULTIPART_PARAM = "crash_meta_file";
    public static final String MINIDUMP_FILE_MULTIPART_PARAM = "minidump_file";
    public static final String OS_META_FILE_MULTIPART_PARAM = "os_meta_file";
    public static final String REPORT_IDENTIFIER_PARAM = "report_id";
    public static final String SESSION_META_FILE_MULTIPART_PARAM = "session_meta_file";
    public static final String USER_META_FILE_MULTIPART_PARAM = "user_meta_file";

    public NativeCreateReportSpiCall(Kit kit, String str, String str2, HttpRequestFactory httpRequestFactory) {
        super(kit, str, str2, httpRequestFactory, HttpMethod.POST);
    }

    private HttpRequest applyHeadersTo(HttpRequest httpRequest, String str) {
        StringBuilder a = outline.a(AbstractSpiCall.CRASHLYTICS_USER_AGENT);
        a.append(super.kit.getVersion());
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_USER_AGENT, a.toString());
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_CLIENT_TYPE, AbstractSpiCall.ANDROID_CLIENT_TYPE);
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_CLIENT_VERSION, super.kit.getVersion());
        httpRequest.d().setRequestProperty(AbstractSpiCall.HEADER_API_KEY, str);
        return httpRequest;
    }

    private HttpRequest applyMultipartDataTo(HttpRequest httpRequest, Report report) {
        httpRequest.c(REPORT_IDENTIFIER_PARAM, report.getIdentifier());
        for (File file : report.getFiles()) {
            if (file.getName().equals("minidump")) {
                httpRequest.a(MINIDUMP_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("metadata")) {
                httpRequest.a(METADATA_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("binaryImages")) {
                httpRequest.a(BINARY_IMAGES_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("session")) {
                httpRequest.a(SESSION_META_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("app")) {
                httpRequest.a(APP_META_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("device")) {
                httpRequest.a(DEVICE_META_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("os")) {
                httpRequest.a(OS_META_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(MetaDataStore.USERDATA_SUFFIX)) {
                httpRequest.a(USER_META_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals("logs")) {
                httpRequest.a(LOGS_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            } else if (file.getName().equals(MetaDataStore.KEYDATA_SUFFIX)) {
                httpRequest.a(KEYS_FILE_MULTIPART_PARAM, file.getName(), "application/octet-stream", file);
            }
        }
        return httpRequest;
    }

    public boolean invoke(CreateReportRequest createReportRequest) {
        HttpRequest applyMultipartDataTo = applyMultipartDataTo(applyHeadersTo(getHttpRequest(), createReportRequest.apiKey), createReportRequest.report);
        DefaultLogger a = Fabric.a();
        StringBuilder a2 = outline.a("Sending report to: ");
        a2.append(getUrl());
        String sb = a2.toString();
        if (a.a(CrashlyticsCore.TAG, 3)) {
            Log.d(CrashlyticsCore.TAG, sb, null);
        }
        int c = applyMultipartDataTo.c();
        DefaultLogger a3 = Fabric.a();
        String b = outline.b("Result was: ", c);
        if (a3.a(CrashlyticsCore.TAG, 3)) {
            Log.d(CrashlyticsCore.TAG, b, null);
        }
        return c.c(c) == 0;
    }
}
