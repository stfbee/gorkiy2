package f.a;

import f.ObjectWatcher;
import f.b;
import n.n.b.Functions;
import n.n.c.DefaultConstructorMarker;

/* compiled from: ActivityDestroyWatcher.kt */
public final class ActivityDestroyWatcher0 {
    public final ActivityDestroyWatcher a = new ActivityDestroyWatcher(this);
    public final ObjectWatcher b;
    public final Functions<b.a> c;

    public /* synthetic */ ActivityDestroyWatcher0(ObjectWatcher objectWatcher, Functions functions, DefaultConstructorMarker defaultConstructorMarker) {
        this.b = objectWatcher;
        this.c = functions;
    }
}
