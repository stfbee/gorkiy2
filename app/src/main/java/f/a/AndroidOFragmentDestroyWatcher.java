package f.a;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.view.View;
import com.crashlytics.android.answers.SessionEvent;
import f.ObjectWatcher;
import f.b;
import f.d;
import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

@SuppressLint({"NewApi"})
/* compiled from: AndroidOFragmentDestroyWatcher.kt */
public final class AndroidOFragmentDestroyWatcher implements Functions0<Activity, g> {
    public final a b;
    public final ObjectWatcher c;
    public final Functions<b.a> d;

    /* compiled from: AndroidOFragmentDestroyWatcher.kt */
    public static final class a extends FragmentManager.FragmentLifecycleCallbacks {
        public final /* synthetic */ AndroidOFragmentDestroyWatcher a;

        public a(AndroidOFragmentDestroyWatcher androidOFragmentDestroyWatcher) {
            this.a = androidOFragmentDestroyWatcher;
        }

        public void onFragmentDestroyed(FragmentManager fragmentManager, Fragment fragment) {
            if (fragmentManager == null) {
                Intrinsics.a("fm");
                throw null;
            } else if (fragment == null) {
                Intrinsics.a("fragment");
                throw null;
            } else if (this.a.d.b().c) {
                this.a.c.a(fragment);
            }
        }

        public void onFragmentViewDestroyed(FragmentManager fragmentManager, Fragment fragment) {
            if (fragmentManager == null) {
                Intrinsics.a("fm");
                throw null;
            } else if (fragment != null) {
                View view = fragment.getView();
                if (view != null && this.a.d.b().d) {
                    this.a.c.a(view);
                }
            } else {
                Intrinsics.a("fragment");
                throw null;
            }
        }
    }

    public AndroidOFragmentDestroyWatcher(d dVar, Functions<b.a> functions) {
        if (dVar == null) {
            Intrinsics.a("objectWatcher");
            throw null;
        } else if (functions != null) {
            this.c = dVar;
            this.d = functions;
            this.b = new a(this);
        } else {
            Intrinsics.a("configProvider");
            throw null;
        }
    }

    public Object a(Object obj) {
        Activity activity = (Activity) obj;
        if (activity != null) {
            activity.getFragmentManager().registerFragmentLifecycleCallbacks(this.b, true);
            return Unit.a;
        }
        Intrinsics.a(SessionEvent.ACTIVITY_KEY);
        throw null;
    }
}
