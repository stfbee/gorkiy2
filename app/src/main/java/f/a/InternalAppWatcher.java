package f.a;

import android.app.Application;
import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import f.AppWatcher;
import f.Clock;
import f.ObjectWatcher;
import f.e;
import java.util.concurrent.Executor;
import kotlin.TypeCastException;
import n.Lazy;
import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.PropertyReference1Impl;
import n.n.c.Reflection;
import n.n.c.TypeIntrinsics;
import n.n.c.j;
import n.p.KProperty;

/* compiled from: InternalAppWatcher.kt */
public final class InternalAppWatcher {
    public static final /* synthetic */ KProperty[] a;
    public static final Functions0<Application, g> b;
    public static final Lazy c = j.c.a.a.c.n.c.a((Functions) a.d);
    public static Application d;

    /* renamed from: e  reason: collision with root package name */
    public static final d f730e = new d();

    /* renamed from: f  reason: collision with root package name */
    public static final Handler f731f = new Handler(Looper.getMainLooper());
    public static final Executor g;
    public static final ObjectWatcher h;

    /* renamed from: i  reason: collision with root package name */
    public static final InternalAppWatcher f732i = new InternalAppWatcher();

    /* compiled from: com.android.tools.r8.jetbrains.kotlin-style lambda group */
    public static final class a extends j implements Functions<Boolean> {
        public static final a d = new a(0);

        /* renamed from: e  reason: collision with root package name */
        public static final a f733e = new a(1);
        public final /* synthetic */ int c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i2) {
            super(0);
            this.c = i2;
        }

        public final Object b() {
            int i2 = this.c;
            boolean z = true;
            if (i2 != 0) {
                if (i2 == 1) {
                    AppWatcher appWatcher = AppWatcher.b;
                    return Boolean.valueOf(AppWatcher.a.a);
                }
                throw null;
            } else if (InternalAppWatcher.f732i != null) {
                Application application = InternalAppWatcher.d;
                if (application != null) {
                    if ((application.getApplicationInfo().flags & 2) == 0) {
                        z = false;
                    }
                    return Boolean.valueOf(z);
                }
                Intrinsics.b("application");
                throw null;
            } else {
                throw null;
            }
        }
    }

    /* compiled from: InternalAppWatcher.kt */
    public static final class b implements Functions0<Application, g>, e {
        public static final b b = new b();

        public Object a(Object obj) {
            if (((Application) obj) != null) {
                return Unit.a;
            }
            Intrinsics.a("application");
            throw null;
        }

        public void d() {
        }
    }

    /* compiled from: InternalAppWatcher.kt */
    public static final class c implements Executor {
        public static final c a = new c();

        public final void execute(Runnable runnable) {
            InternalAppWatcher internalAppWatcher = InternalAppWatcher.f732i;
            Handler handler = InternalAppWatcher.f731f;
            AppWatcher appWatcher = AppWatcher.b;
            handler.postDelayed(runnable, AppWatcher.a.f727e);
        }
    }

    /* compiled from: InternalAppWatcher.kt */
    public static final class d implements Clock {
        public long a() {
            return SystemClock.uptimeMillis();
        }
    }

    /* JADX WARN: Type inference failed for: r3v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    static {
        Object obj;
        PropertyReference1Impl propertyReference1Impl = new PropertyReference1Impl(Reflection.a(InternalAppWatcher.class), "isDebuggableBuild", "isDebuggableBuild()Z");
        Reflection.a(propertyReference1Impl);
        a = new KProperty[]{propertyReference1Impl};
        try {
            Class<?> cls = Class.forName("leakcanary.internal.InternalLeakCanary");
            Intrinsics.a((Object) cls, "Class.forName(\"leakcanar…rnal.InternalLeakCanary\")");
            obj = cls.getDeclaredField("INSTANCE").get(null);
        } catch (Throwable unused) {
            obj = b.b;
        }
        if (obj != null) {
            TypeIntrinsics.a(obj, 1);
            b = (Functions0) obj;
            c cVar = c.a;
            g = cVar;
            h = new ObjectWatcher(f730e, cVar, a.f733e);
            return;
        }
        throw new TypeCastException("null cannot be cast to non-null type (android.app.Application) -> kotlin.Unit");
    }
}
