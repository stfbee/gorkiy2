package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import o.g0;
import o.i0;

public interface Converter<F, T> {

    public static abstract class a {
        @Nullable
        public Converter<i0, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
            return null;
        }

        @Nullable
        public Converter<?, g0> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, e0 e0Var) {
            return null;
        }
    }

    @Nullable
    T a(F f2);
}
