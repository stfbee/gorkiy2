package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.Type;
import javax.annotation.Nullable;
import n.Unit;
import n.g;
import o.RequestBody;
import o.ResponseBody;
import o.g0;
import o.i0;
import r.Converter;
import r.l0.Streaming;

public final class BuiltInConverters extends Converter.a {
    public boolean a = true;

    public static final class a implements Converter<i0, i0> {
        public static final a a = new a();

        public Object a(Object obj) {
            ResponseBody responseBody = (ResponseBody) obj;
            try {
                return Utils.a(responseBody);
            } finally {
                responseBody.close();
            }
        }
    }

    public static final class b implements Converter<g0, g0> {
        public static final b a = new b();

        public Object a(Object obj) {
            return (RequestBody) obj;
        }
    }

    public static final class c implements Converter<i0, i0> {
        public static final c a = new c();

        public Object a(Object obj) {
            return (ResponseBody) obj;
        }
    }

    public static final class d implements Converter<Object, String> {
        public static final d a = new d();

        public Object a(Object obj) {
            return obj.toString();
        }
    }

    public static final class e implements Converter<i0, g> {
        public static final e a = new e();

        public Object a(Object obj) {
            ((ResponseBody) obj).close();
            return Unit.a;
        }
    }

    public static final class f implements Converter<i0, Void> {
        public static final f a = new f();

        public Object a(Object obj) {
            ((ResponseBody) obj).close();
            return null;
        }
    }

    @Nullable
    public Converter<i0, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        if (type == ResponseBody.class) {
            if (Utils.a(annotationArr, Streaming.class)) {
                return c.a;
            }
            return a.a;
        } else if (type == Void.class) {
            return f.a;
        } else {
            if (!this.a || type != Unit.class) {
                return null;
            }
            try {
                return e.a;
            } catch (NoClassDefFoundError unused) {
                this.a = false;
                return null;
            }
        }
    }

    @Nullable
    public Converter<?, g0> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2, e0 e0Var) {
        if (RequestBody.class.isAssignableFrom(Utils.b(type))) {
            return b.a;
        }
        return null;
    }
}
