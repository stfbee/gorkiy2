package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executor;
import javax.annotation.Nullable;
import o.Call;
import o.HttpUrl;
import o.f;
import o.g0;
import o.i0;
import o.x;
import r.BuiltInConverters;
import r.c;
import r.j;

/* compiled from: Retrofit */
public final class Retrofit0 {
    public final Map<Method, ServiceMethod<?>> a = new ConcurrentHashMap();
    public final Call.a b;
    public final HttpUrl c;
    public final List<j.a> d;

    /* renamed from: e  reason: collision with root package name */
    public final List<c.a> f3202e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f3203f;

    public Retrofit0(f.a aVar, x xVar, List<j.a> list, List<c.a> list2, @Nullable Executor executor, boolean z) {
        this.b = aVar;
        this.c = xVar;
        this.d = list;
        this.f3202e = list2;
        this.f3203f = z;
    }

    public ServiceMethod<?> a(Method method) {
        ServiceMethod<?> serviceMethod;
        ServiceMethod<?> serviceMethod2 = this.a.get(method);
        if (serviceMethod2 != null) {
            return serviceMethod2;
        }
        synchronized (this.a) {
            serviceMethod = this.a.get(method);
            if (serviceMethod == null) {
                serviceMethod = ServiceMethod.a(this, method);
                this.a.put(method, serviceMethod);
            }
        }
        return serviceMethod;
    }

    public <T> Converter<i0, T> b(Type type, Annotation[] annotationArr) {
        Utils.a(type, "type == null");
        Utils.a(annotationArr, "annotations == null");
        int indexOf = this.d.indexOf(null) + 1;
        int size = this.d.size();
        for (int i2 = indexOf; i2 < size; i2++) {
            Converter a2 = this.d.get(i2).a(type, annotationArr, this);
            if (a2 != null) {
                return a2;
            }
        }
        StringBuilder sb = new StringBuilder("Could not locate ResponseBody converter for ");
        sb.append(type);
        sb.append(".\n");
        sb.append("  Tried:");
        int size2 = this.d.size();
        while (indexOf < size2) {
            sb.append("\n   * ");
            sb.append(this.d.get(indexOf).getClass().getName());
            indexOf++;
        }
        throw new IllegalArgumentException(sb.toString());
    }

    public <T> Converter<T, String> c(Type type, Annotation[] annotationArr) {
        Utils.a(type, "type == null");
        Utils.a(annotationArr, "annotations == null");
        int size = this.d.size();
        int i2 = 0;
        while (i2 < size) {
            if (this.d.get(i2) != null) {
                i2++;
            } else {
                throw null;
            }
        }
        return BuiltInConverters.d.a;
    }

    public CallAdapter<?, ?> a(Type type, Annotation[] annotationArr) {
        Utils.a(type, "returnType == null");
        Utils.a(annotationArr, "annotations == null");
        int indexOf = this.f3202e.indexOf(null) + 1;
        int size = this.f3202e.size();
        for (int i2 = indexOf; i2 < size; i2++) {
            CallAdapter<?, ?> a2 = this.f3202e.get(i2).a(type, annotationArr, this);
            if (a2 != null) {
                return a2;
            }
        }
        StringBuilder sb = new StringBuilder("Could not locate call adapter for ");
        sb.append(type);
        sb.append(".\n");
        sb.append("  Tried:");
        int size2 = this.f3202e.size();
        while (indexOf < size2) {
            sb.append("\n   * ");
            sb.append(this.f3202e.get(indexOf).getClass().getName());
            indexOf++;
        }
        throw new IllegalArgumentException(sb.toString());
    }

    public <T> Converter<T, g0> a(Type type, Annotation[] annotationArr, Annotation[] annotationArr2) {
        Utils.a(type, "type == null");
        Utils.a(annotationArr, "parameterAnnotations == null");
        Utils.a(annotationArr2, "methodAnnotations == null");
        int indexOf = this.d.indexOf(null) + 1;
        int size = this.d.size();
        for (int i2 = indexOf; i2 < size; i2++) {
            Converter a2 = this.d.get(i2).a(type, annotationArr, annotationArr2, this);
            if (a2 != null) {
                return a2;
            }
        }
        StringBuilder sb = new StringBuilder("Could not locate RequestBody converter for ");
        sb.append(type);
        sb.append(".\n");
        sb.append("  Tried:");
        int size2 = this.d.size();
        while (indexOf < size2) {
            sb.append("\n   * ");
            sb.append(this.d.get(indexOf).getClass().getName());
            indexOf++;
        }
        throw new IllegalArgumentException(sb.toString());
    }
}
