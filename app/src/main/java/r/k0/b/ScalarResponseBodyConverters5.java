package r.k0.b;

import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters5 implements Converter<i0, Long> {
    public static final ScalarResponseBodyConverters5 a = new ScalarResponseBodyConverters5();

    public Object a(Object obj) {
        return Long.valueOf(((ResponseBody) obj).h());
    }
}
