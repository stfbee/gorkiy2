package r.k0.b;

import j.a.a.a.outline;
import java.io.IOException;
import o.ResponseBody;
import o.i0;
import r.Converter;

/* compiled from: ScalarResponseBodyConverters */
public final class ScalarResponseBodyConverters1 implements Converter<i0, Character> {
    public static final ScalarResponseBodyConverters1 a = new ScalarResponseBodyConverters1();

    public Object a(Object obj) {
        String h = ((ResponseBody) obj).h();
        if (h.length() == 1) {
            return Character.valueOf(h.charAt(0));
        }
        StringBuilder a2 = outline.a("Expected body of length 1 for Character conversion but was ");
        a2.append(h.length());
        throw new IOException(a2.toString());
    }
}
