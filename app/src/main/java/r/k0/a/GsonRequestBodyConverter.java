package r.k0.a;

import com.google.gson.stream.JsonWriter;
import j.c.d.Gson;
import j.c.d.TypeAdapter;
import j.c.d.k;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import o.MediaType;
import o.RequestBody;
import o.g0;
import p.Buffer;
import p.Buffer0;
import r.Converter;

public final class GsonRequestBodyConverter<T> implements Converter<T, g0> {
    public static final MediaType c = MediaType.a("application/json; charset=UTF-8");
    public static final Charset d = Charset.forName("UTF-8");
    public final Gson a;
    public final TypeAdapter<T> b;

    public GsonRequestBodyConverter(k kVar, TypeAdapter<T> typeAdapter) {
        this.a = kVar;
        this.b = typeAdapter;
    }

    public Object a(Object obj) {
        Buffer buffer = new Buffer();
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(new Buffer0(buffer), d);
        Gson gson = this.a;
        if (gson.g) {
            outputStreamWriter.write(")]}'\n");
        }
        JsonWriter jsonWriter = new JsonWriter(outputStreamWriter);
        if (gson.h) {
            jsonWriter.setIndent("  ");
        }
        jsonWriter.setSerializeNulls(gson.f2536f);
        this.b.a(jsonWriter, obj);
        jsonWriter.close();
        return RequestBody.a(c, buffer.l());
    }
}
