package r;

import javax.annotation.Nullable;
import o.ResponseBody;
import o.h0;
import o.i0;

public final class Response<T> {
    public final o.Response a;
    @Nullable
    public final T b;
    @Nullable
    public final ResponseBody c;

    public Response(h0 h0Var, @Nullable T t2, @Nullable i0 i0Var) {
        this.a = h0Var;
        this.b = t2;
        this.c = i0Var;
    }

    public static <T> Response<T> a(@Nullable T t2, h0 h0Var) {
        Utils.a(h0Var, "rawResponse == null");
        if (h0Var.f()) {
            return new Response<>(h0Var, t2, null);
        }
        throw new IllegalArgumentException("rawResponse must be successful response");
    }

    public String toString() {
        return this.a.toString();
    }

    public boolean a() {
        return this.a.f();
    }
}
