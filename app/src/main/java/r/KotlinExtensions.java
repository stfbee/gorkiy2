package r;

import j.c.a.a.c.n.c;
import java.lang.reflect.Method;
import kotlin.KotlinNullPointerException;
import kotlinx.coroutines.CancellableContinuation;
import n.n.c.Intrinsics;
import retrofit2.HttpException;

/* compiled from: KotlinExtensions.kt */
public final class KotlinExtensions implements Callback<T> {
    public final /* synthetic */ CancellableContinuation b;

    public KotlinExtensions(CancellableContinuation cancellableContinuation) {
        this.b = cancellableContinuation;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [r.Invocation, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.reflect.Method, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.Class<?>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Call<T> call, Response<T> response) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (response == null) {
            Intrinsics.a("response");
            throw null;
        } else if (response.a()) {
            T t2 = response.b;
            if (t2 == null) {
                Class<Invocation> cls = Invocation.class;
                Invocation cast = cls.cast(call.f().f2897f.get(cls));
                if (cast != null) {
                    Intrinsics.a((Object) cast, "call.request().tag(Invocation::class.java)!!");
                    Method method = cast.a;
                    StringBuilder sb = new StringBuilder();
                    sb.append("Response from ");
                    Intrinsics.a((Object) method, "method");
                    Class<?> declaringClass = method.getDeclaringClass();
                    Intrinsics.a((Object) declaringClass, "method.declaringClass");
                    sb.append(declaringClass.getName());
                    sb.append('.');
                    sb.append(method.getName());
                    sb.append(" was null but response body type was declared as non-null");
                    this.b.a(c.a((Throwable) new KotlinNullPointerException(sb.toString())));
                    return;
                }
                Intrinsics.a();
                throw null;
            }
            this.b.a(t2);
        } else {
            this.b.a(c.a((Throwable) new HttpException(response)));
        }
    }

    public void a(Call<T> call, Throwable th) {
        if (call == null) {
            Intrinsics.a("call");
            throw null;
        } else if (th != null) {
            this.b.a(c.a(th));
        } else {
            Intrinsics.a("t");
            throw null;
        }
    }
}
