package r;

import java.lang.annotation.Annotation;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Optional;
import javax.annotation.Nullable;
import o.ResponseBody;
import o.i0;
import org.codehaus.mojo.animal_sniffer.IgnoreJRERequirement;
import r.Converter;

@IgnoreJRERequirement
public final class OptionalConverterFactory extends Converter.a {
    public static final Converter.a a = new OptionalConverterFactory();

    @IgnoreJRERequirement
    public static final class a<T> implements Converter<i0, Optional<T>> {
        public final Converter<i0, T> a;

        public a(Converter<i0, T> converter) {
            this.a = converter;
        }

        public Object a(Object obj) {
            return Optional.ofNullable(this.a.a((ResponseBody) obj));
        }
    }

    @Nullable
    public Converter<i0, ?> a(Type type, Annotation[] annotationArr, e0 e0Var) {
        if (Utils.b(type) != Optional.class) {
            return null;
        }
        return new a(e0Var.b(Utils.a(0, (ParameterizedType) type), annotationArr));
    }
}
