package r.j0.a;

import java.lang.reflect.Type;
import javax.annotation.Nullable;
import l.b.Scheduler;
import r.CallAdapter;

public final class RxJava2CallAdapter<R> implements CallAdapter<R, Object> {
    public final Type a;
    @Nullable
    public final Scheduler b;
    public final boolean c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final boolean f3205e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f3206f;
    public final boolean g;
    public final boolean h;

    /* renamed from: i  reason: collision with root package name */
    public final boolean f3207i;

    public RxJava2CallAdapter(Type type, @Nullable Scheduler scheduler, boolean z, boolean z2, boolean z3, boolean z4, boolean z5, boolean z6, boolean z7) {
        this.a = type;
        this.b = scheduler;
        this.c = z;
        this.d = z2;
        this.f3205e = z3;
        this.f3206f = z4;
        this.g = z5;
        this.h = z6;
        this.f3207i = z7;
    }

    public Type a() {
        return this.a;
    }

    /* JADX WARNING: Removed duplicated region for block: B:13:0x0028  */
    /* JADX WARNING: Removed duplicated region for block: B:16:0x0030  */
    /* JADX WARNING: Removed duplicated region for block: B:18:0x003d  */
    /* Code decompiled incorrectly, please refer to instructions dump. */
    public java.lang.Object a(r.Call<R> r3) {
        /*
            r2 = this;
            boolean r0 = r2.c
            if (r0 == 0) goto L_0x000a
            r.j0.a.CallEnqueueObservable r0 = new r.j0.a.CallEnqueueObservable
            r0.<init>(r3)
            goto L_0x000f
        L_0x000a:
            r.j0.a.CallExecuteObservable r0 = new r.j0.a.CallExecuteObservable
            r0.<init>(r3)
        L_0x000f:
            boolean r3 = r2.d
            if (r3 == 0) goto L_0x001a
            r.j0.a.ResultObservable r3 = new r.j0.a.ResultObservable
            r3.<init>(r0)
        L_0x0018:
            r0 = r3
            goto L_0x0024
        L_0x001a:
            boolean r3 = r2.f3205e
            if (r3 == 0) goto L_0x0024
            r.j0.a.BodyObservable r3 = new r.j0.a.BodyObservable
            r3.<init>(r0)
            goto L_0x0018
        L_0x0024:
            l.b.Scheduler r3 = r2.b
            if (r3 == 0) goto L_0x002c
            l.b.Observable r0 = r0.b(r3)
        L_0x002c:
            boolean r3 = r2.f3206f
            if (r3 == 0) goto L_0x003d
            l.b.BackpressureStrategy r3 = l.b.BackpressureStrategy.LATEST
            l.b.u.e.b.FlowableFromObservable r3 = new l.b.u.e.b.FlowableFromObservable
            r3.<init>(r0)
            l.b.u.e.b.FlowableOnBackpressureLatest r0 = new l.b.u.e.b.FlowableOnBackpressureLatest
            r0.<init>(r3)
            return r0
        L_0x003d:
            boolean r3 = r2.g
            if (r3 == 0) goto L_0x0048
            l.b.u.e.c.ObservableSingleSingle r3 = new l.b.u.e.c.ObservableSingleSingle
            r1 = 0
            r3.<init>(r0, r1)
            return r3
        L_0x0048:
            boolean r3 = r2.h
            if (r3 == 0) goto L_0x0052
            l.b.u.e.c.ObservableSingleMaybe r3 = new l.b.u.e.c.ObservableSingleMaybe
            r3.<init>(r0)
            return r3
        L_0x0052:
            boolean r3 = r2.f3207i
            if (r3 == 0) goto L_0x005c
            l.b.u.e.c.ObservableIgnoreElementsCompletable r3 = new l.b.u.e.c.ObservableIgnoreElementsCompletable
            r3.<init>(r0)
            return r3
        L_0x005c:
            return r0
        */
        throw new UnsupportedOperationException("Method not decompiled: r.j0.a.RxJava2CallAdapter.a(r.Call):java.lang.Object");
    }
}
