package r;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import javax.annotation.Nullable;

public class Retrofit implements InvocationHandler {
    public final Platform a = Platform.a;
    public final Object[] b = new Object[0];
    public final /* synthetic */ Class c;
    public final /* synthetic */ Retrofit0 d;

    public Retrofit(Retrofit0 retrofit0, Class cls) {
        this.d = retrofit0;
        this.c = cls;
    }

    @Nullable
    public Object invoke(Object obj, Method method, @Nullable Object[] objArr) {
        if (method.getDeclaringClass() == Object.class) {
            return method.invoke(this, objArr);
        }
        if (this.a.a(method)) {
            return this.a.a(method, this.c, obj, objArr);
        }
        ServiceMethod<?> a2 = this.d.a(method);
        if (objArr == null) {
            objArr = this.b;
        }
        HttpServiceMethod httpServiceMethod = (HttpServiceMethod) a2;
        return httpServiceMethod.a(new OkHttpCall(httpServiceMethod.a, objArr, httpServiceMethod.b, httpServiceMethod.c), objArr);
    }
}
