package k.a;

import m.a.Provider;

public interface Factory<T> extends Provider<T> {
}
