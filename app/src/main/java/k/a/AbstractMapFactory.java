package k.a;

import j.c.a.a.c.n.c;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import m.a.Provider;

public abstract class AbstractMapFactory<K, V, V2> implements Factory<Map<K, V2>> {
    public final Map<K, Provider<V>> a;

    public static abstract class a<K, V, V2> {
        public final LinkedHashMap<K, Provider<V>> a;

        public a(int i2) {
            this.a = c.b(i2);
        }
    }

    public AbstractMapFactory(Map<K, Provider<V>> map) {
        this.a = Collections.unmodifiableMap(map);
    }
}
