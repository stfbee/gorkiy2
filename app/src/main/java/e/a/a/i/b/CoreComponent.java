package e.a.a.i.b;

import android.content.Context;
import e.a.a.CoreApp;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.a.g.a.Session;
import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.h.IDeeplinkManager;
import e.a.a.j.a.IAuthRepository0;
import e.a.a.j.b.IDevMenuRepository0;
import e.a.a.m.a.GlideHelper;
import e.c.b.RetrofitFactory;
import e.c.b.i.a.ICacheStorage;
import e.c.d.b.c.VmFactoryWrapper;

/* compiled from: CoreComponent.kt */
public interface CoreComponent {
    AppCiceroneHolder a();

    void a(CoreApp coreApp);

    void a(VmFactoryWrapper vmFactoryWrapper);

    Context b();

    IFragmentCoordinator c();

    ITopLevelCoordinator d();

    IApiConfig e();

    ICacheStorage f();

    IAuthPrefs g();

    Session h();

    RetrofitFactory i();

    IDeeplinkManager j();

    NestedFragmentCiceroneHolder k();

    IDevMenuRepository0 l();

    FragmentCiceroneHolder m();

    IAuthRepository0 n();

    GlideHelper o();
}
