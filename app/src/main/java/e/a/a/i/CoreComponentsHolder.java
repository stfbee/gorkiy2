package e.a.a.i;

import e.a.a.i.b.AppComponent;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.b.CoreComponent0;
import e.a.a.i.b.CoreComponent1;
import e.a.a.i.b.CoreComponent2;
import e.a.a.i.b.DeeplinkComponent;
import e.a.a.i.b.DevComponent;

/* compiled from: CoreComponentsHolder.kt */
public final class CoreComponentsHolder {
    public static AppComponent a;
    public static CoreComponent b;
    public static CoreComponent2 c;
    public static CoreComponent0 d;

    /* renamed from: e  reason: collision with root package name */
    public static CoreComponent1 f624e;

    /* renamed from: f  reason: collision with root package name */
    public static DevComponent f625f;
    public static DeeplinkComponent g;
}
