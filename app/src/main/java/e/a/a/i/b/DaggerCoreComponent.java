package e.a.a.i.b;

import android.content.Context;
import e.a.a.CoreApp;
import e.a.a.a.c.a.ErrorFragmentVm;
import e.a.a.a.c.a.ErrorFragmentVm_Factory;
import e.a.a.a.c.b.ProcessingFragmentVm;
import e.a.a.a.c.b.ProcessingFragmentVm_Factory;
import e.a.a.a.c.c.BottomSheetSelectorDialogVm;
import e.a.a.a.c.c.BottomSheetSelectorDialogVm_Factory;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.a.g.a.Session;
import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.h.IDeeplinkConfig;
import e.a.a.h.IDeeplinkManager;
import e.a.a.i.c.AuthModule;
import e.a.a.i.c.AuthModule_ProvideAuthPrefsFactory;
import e.a.a.i.c.AuthModule_ProvideAuthRepositoryFactory;
import e.a.a.i.c.AuthModule_ProvideEpguAuthApiServiceFactory;
import e.a.a.i.c.AuthModule_ProvideEsiaAuthApiServiceFactory;
import e.a.a.i.c.CoreNavigationModule;
import e.a.a.i.c.CoreStorageModule;
import e.a.a.i.c.CoreStorageModule_ProvideCacheDaoFactory;
import e.a.a.i.c.CoreStorageModule_ProvideCacheStorageFactory;
import e.a.a.i.c.CoreStorageModule_ProvideDatabaseFactory;
import e.a.a.i.c.NetworkModule;
import e.a.a.i.c.NetworkModule_ProvideGlideHelperFactory;
import e.a.a.i.c.NetworkModule_ProvideSessionFactory;
import e.a.a.i.c.a0;
import e.a.a.i.c.b0;
import e.a.a.i.c.c0;
import e.a.a.i.c.d0;
import e.a.a.i.c.e0;
import e.a.a.i.c.f0;
import e.a.a.i.c.h0;
import e.a.a.i.c.i0;
import e.a.a.i.c.j0;
import e.a.a.i.c.k0;
import e.a.a.i.c.m0.DevRepositoryModule;
import e.a.a.i.c.m0.DevRepositoryModule_ProvideEpguDevApiServiceFactory;
import e.a.a.i.c.q;
import e.a.a.i.c.r;
import e.a.a.i.c.s;
import e.a.a.i.c.t;
import e.a.a.i.c.z;
import e.a.a.i.d.VmFactory;
import e.a.a.j.a.IAuthRepository0;
import e.a.a.j.b.IDevMenuRepository0;
import e.a.a.l.IAppLifecycle;
import e.a.a.m.a.GlideHelper;
import e.c.a.c.a.a.IPrefsStorage;
import e.c.b.RetrofitFactory;
import e.c.b.h;
import e.c.b.i.a.ICacheStorage;
import e.c.d.b.c.VmFactoryWrapper;
import i.s.RoomDatabase;
import j.c.d.k;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;
import k.a.DelegateFactory;
import k.a.DoubleCheck;
import m.a.Provider;

public final class DaggerCoreComponent implements CoreComponent {
    public Provider<e.a.a.m.a.b> A;
    public Provider<e.a.a.h.d> B;
    public Provider<e.a.a.h.e> C;
    public Provider<e.a.a.g.a.f.a> D;
    public Provider<e.a.a.g.a.f.c> E;
    public Provider<e.a.a.j.a.c> F;
    public Provider<e.a.a.a.e.o.c> G;
    public Provider<e.a.a.a.c.b.d> H;
    public Provider<e.a.a.a.c.a.e> I;
    public Provider<e.a.a.a.c.c.f> J;
    public final AppComponent a;
    public final CoreComponent2 b;
    public Provider<k> c;
    public Provider<e.a.a.a.e.r.c> d;

    /* renamed from: e  reason: collision with root package name */
    public Provider<e.a.a.a.e.r.d> f628e;

    /* renamed from: f  reason: collision with root package name */
    public Provider<e.a.a.a.e.q.b> f629f;
    public Provider<e.c.a.c.a.a.a> g;
    public Provider<h> h;

    /* renamed from: i  reason: collision with root package name */
    public Provider<e.a.a.g.a.f.b> f630i;

    /* renamed from: j  reason: collision with root package name */
    public Provider<e.a.a.j.b.c> f631j;

    /* renamed from: k  reason: collision with root package name */
    public Provider<e.a.a.g.a.g.f> f632k;

    /* renamed from: l  reason: collision with root package name */
    public Provider<e.c.b.j.c> f633l;

    /* renamed from: m  reason: collision with root package name */
    public Provider<Context> f634m;

    /* renamed from: n  reason: collision with root package name */
    public Provider<e.c.b.k.b> f635n;

    /* renamed from: o  reason: collision with root package name */
    public Provider<e.c.b.l.a> f636o;

    /* renamed from: p  reason: collision with root package name */
    public Provider<e.a.a.g.a.d> f637p;

    /* renamed from: q  reason: collision with root package name */
    public Provider<e.a.a.g.a.c> f638q;

    /* renamed from: r  reason: collision with root package name */
    public Provider<e.a.a.g.a.h.b> f639r;

    /* renamed from: s  reason: collision with root package name */
    public Provider<e.a.a.g.a.h.a> f640s;

    /* renamed from: t  reason: collision with root package name */
    public Provider<e.a.a.g.a.h.d> f641t;
    public Provider<e.c.b.m.a> u;
    public Provider<i.s.h> v;
    public Provider<e.a.a.g.b.a.a> w;
    public Provider<e.a.a.g.b.a.b.a> x;
    public Provider<e.c.b.i.a.a> y;
    public Provider<e.a.a.g.b.b.a.b> z;

    public static class b implements Provider<Context> {
        public final AppComponent a;

        public b(AppComponent appComponent) {
            this.a = appComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [android.content.Context, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            Context b = this.a.b();
            j.c.a.a.c.n.c.a((Object) b, "Cannot return null from a non-@Nullable component method");
            return b;
        }
    }

    public static class c implements Provider<i.s.h> {
        public final CoreComponent0 a;

        public c(CoreComponent0 coreComponent0) {
            this.a = coreComponent0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [i.s.RoomDatabase, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            RoomDatabase a2 = this.a.a();
            j.c.a.a.c.n.c.a((Object) a2, "Cannot return null from a non-@Nullable component method");
            return a2;
        }
    }

    public static class d implements Provider<e.c.a.c.a.a.a> {
        public final CoreComponent0 a;

        public d(CoreComponent0 coreComponent0) {
            this.a = coreComponent0;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.c.a.c.a.a.IPrefsStorage, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            IPrefsStorage b = this.a.b();
            j.c.a.a.c.n.c.a((Object) b, "Cannot return null from a non-@Nullable component method");
            return b;
        }
    }

    public static class e implements Provider<e.a.a.h.d> {
        public final CoreComponent1 a;

        public e(CoreComponent1 coreComponent1) {
            this.a = coreComponent1;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.h.IDeeplinkConfig, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            IDeeplinkConfig a2 = this.a.a();
            j.c.a.a.c.n.c.a((Object) a2, "Cannot return null from a non-@Nullable component method");
            return a2;
        }
    }

    public static class f implements Provider<e.a.a.a.e.o.c> {
        public final CoreComponent2 a;

        public f(CoreComponent2 coreComponent2) {
            this.a = coreComponent2;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.e.o.ITopLevelCoordinator, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            ITopLevelCoordinator b = this.a.b();
            j.c.a.a.c.n.c.a((Object) b, "Cannot return null from a non-@Nullable component method");
            return b;
        }
    }

    public /* synthetic */ DaggerCoreComponent(NetworkModule networkModule, CoreNavigationModule coreNavigationModule, CoreStorageModule coreStorageModule, AuthModule authModule, DevRepositoryModule devRepositoryModule, AppComponent appComponent, CoreComponent2 coreComponent2, CoreComponent0 coreComponent0, CoreComponent1 coreComponent1, a aVar) {
        NetworkModule networkModule2 = networkModule;
        CoreNavigationModule coreNavigationModule2 = coreNavigationModule;
        CoreStorageModule coreStorageModule2 = coreStorageModule;
        AuthModule authModule2 = authModule;
        DevRepositoryModule devRepositoryModule2 = devRepositoryModule;
        AppComponent appComponent2 = appComponent;
        CoreComponent2 coreComponent22 = coreComponent2;
        CoreComponent0 coreComponent02 = coreComponent0;
        this.a = appComponent2;
        this.b = coreComponent22;
        this.c = DoubleCheck.a(new h0(networkModule2));
        this.d = DoubleCheck.a(new r(coreNavigationModule2));
        this.f628e = DoubleCheck.a(new t(coreNavigationModule2));
        this.f629f = DoubleCheck.a(new s(coreNavigationModule2, this.d));
        this.g = new d(coreComponent02);
        DelegateFactory delegateFactory = new DelegateFactory();
        this.h = delegateFactory;
        Provider<e.a.a.g.a.f.b> a2 = DoubleCheck.a(new DevRepositoryModule_ProvideEpguDevApiServiceFactory(devRepositoryModule2, delegateFactory));
        this.f630i = a2;
        Provider<e.a.a.j.b.c> a3 = DoubleCheck.a(new e.a.a.i.c.m0.b(devRepositoryModule2, this.g, a2));
        this.f631j = a3;
        Provider<e.a.a.g.a.g.f> a4 = DoubleCheck.a(new z(networkModule2, a3));
        this.f632k = a4;
        this.f633l = DoubleCheck.a(new a0(networkModule2, a4));
        b bVar = new b(appComponent2);
        this.f634m = bVar;
        this.f635n = DoubleCheck.a(new c0(networkModule2, bVar));
        this.f636o = DoubleCheck.a(new i0(networkModule2));
        this.f637p = DoubleCheck.a(new NetworkModule_ProvideSessionFactory(networkModule2));
        Provider<e.a.a.g.a.c> a5 = DoubleCheck.a(new b0(networkModule2, this.f632k));
        this.f638q = a5;
        this.f639r = DoubleCheck.a(new e0(networkModule2, this.f637p, a5));
        this.f640s = DoubleCheck.a(new d0(networkModule2, this.f637p, this.f638q));
        Provider<e.a.a.g.a.h.d> a6 = DoubleCheck.a(new f0(networkModule2, this.f637p, this.f638q));
        this.f641t = a6;
        Provider<e.c.b.m.a> a7 = DoubleCheck.a(new j0(networkModule2, this.f639r, this.f640s, a6));
        this.u = a7;
        Provider<h> provider = this.h;
        Provider<h> provider2 = provider;
        Provider<T> a8 = DoubleCheck.a(new k0(networkModule, this.f633l, this.f634m, this.c, this.f635n, this.f636o, a7, this.f632k));
        if (a8 != null) {
            DelegateFactory delegateFactory2 = (DelegateFactory) provider2;
            if (delegateFactory2.a == null) {
                delegateFactory2.a = a8;
                c cVar = new c(coreComponent02);
                this.v = cVar;
                Provider<e.a.a.g.b.a.a> a9 = DoubleCheck.a(new CoreStorageModule_ProvideDatabaseFactory(coreStorageModule2, cVar));
                this.w = a9;
                Provider<e.a.a.g.b.a.b.a> a10 = DoubleCheck.a(new CoreStorageModule_ProvideCacheDaoFactory(coreStorageModule2, a9));
                this.x = a10;
                this.y = DoubleCheck.a(new CoreStorageModule_ProvideCacheStorageFactory(coreStorageModule2, a10));
                Provider<e.a.a.g.b.b.a.b> a11 = DoubleCheck.a(new AuthModule_ProvideAuthPrefsFactory(authModule2, this.f634m, this.g, this.f637p));
                this.z = a11;
                this.A = DoubleCheck.a(new NetworkModule_ProvideGlideHelperFactory(networkModule2, this.f633l, a11));
                e eVar = new e(coreComponent1);
                this.B = eVar;
                this.C = DoubleCheck.a(new q(coreNavigationModule2, eVar));
                this.D = DoubleCheck.a(new AuthModule_ProvideEpguAuthApiServiceFactory(authModule2, this.h));
                Provider<e.a.a.g.a.f.c> a12 = DoubleCheck.a(new AuthModule_ProvideEsiaAuthApiServiceFactory(authModule2, this.h));
                this.E = a12;
                this.F = DoubleCheck.a(new AuthModule_ProvideAuthRepositoryFactory(authModule, this.D, a12, this.z, this.g, this.f637p, this.c, this.y));
                f fVar = new f(coreComponent22);
                this.G = fVar;
                this.H = new ProcessingFragmentVm_Factory(this.f629f, fVar);
                this.I = new ErrorFragmentVm_Factory(this.f634m, this.f629f, this.G);
                this.J = new BottomSheetSelectorDialogVm_Factory(this.f629f);
                return;
            }
            throw new IllegalStateException();
        }
        throw null;
    }

    public void a(VmFactoryWrapper vmFactoryWrapper) {
        Map map;
        LinkedHashMap b2 = j.c.a.a.c.n.c.b(3);
        b2.put(ProcessingFragmentVm.class, this.H);
        b2.put(ErrorFragmentVm.class, this.I);
        b2.put(BottomSheetSelectorDialogVm.class, this.J);
        if (b2.size() != 0) {
            map = Collections.unmodifiableMap(b2);
        } else {
            map = Collections.emptyMap();
        }
        vmFactoryWrapper.a = new VmFactory(map);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [android.content.Context, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Context b() {
        Context b2 = this.a.b();
        j.c.a.a.c.n.c.a((Object) b2, "Cannot return null from a non-@Nullable component method");
        return b2;
    }

    public IFragmentCoordinator c() {
        return this.f629f.get();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.o.ITopLevelCoordinator, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public ITopLevelCoordinator d() {
        ITopLevelCoordinator b2 = this.b.b();
        j.c.a.a.c.n.c.a((Object) b2, "Cannot return null from a non-@Nullable component method");
        return b2;
    }

    public IApiConfig e() {
        return this.f632k.get();
    }

    public ICacheStorage f() {
        return this.y.get();
    }

    public IAuthPrefs g() {
        return this.z.get();
    }

    public Session h() {
        return this.f637p.get();
    }

    public RetrofitFactory i() {
        return this.h.get();
    }

    public IDeeplinkManager j() {
        return this.C.get();
    }

    public NestedFragmentCiceroneHolder k() {
        return this.f628e.get();
    }

    public IDevMenuRepository0 l() {
        return this.f631j.get();
    }

    public FragmentCiceroneHolder m() {
        return this.d.get();
    }

    public IAuthRepository0 n() {
        return this.F.get();
    }

    public GlideHelper o() {
        return this.A.get();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.AppCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public AppCiceroneHolder a() {
        AppCiceroneHolder a2 = this.b.a();
        j.c.a.a.c.n.c.a((Object) a2, "Cannot return null from a non-@Nullable component method");
        return a2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.l.IAppLifecycle, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public void a(CoreApp coreApp) {
        IAppLifecycle d2 = this.a.d();
        j.c.a.a.c.n.c.a((Object) d2, "Cannot return null from a non-@Nullable component method");
        coreApp.b = d2;
    }
}
