package e.a.a.i.c;

import e.a.a.g.b.a.CoreDatabase;
import e.a.a.g.b.a.b.CacheDao;
import e.a.a.g.b.a.b.a;
import j.c.a.a.c.n.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

public final class CoreStorageModule_ProvideCacheDaoFactory implements Factory<a> {
    public final CoreStorageModule a;
    public final Provider<e.a.a.g.b.a.a> b;

    public CoreStorageModule_ProvideCacheDaoFactory(u uVar, Provider<e.a.a.g.b.a.a> provider) {
        this.a = uVar;
        this.b = provider;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.g.b.a.b.CacheDao, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        CoreStorageModule coreStorageModule = this.a;
        CoreDatabase coreDatabase = this.b.get();
        if (coreStorageModule == null) {
            throw null;
        } else if (coreDatabase != null) {
            CacheDao a2 = coreDatabase.a();
            c.a((Object) a2, "Cannot return null from a non-@Nullable @Provides method");
            return a2;
        } else {
            Intrinsics.a("database");
            throw null;
        }
    }
}
