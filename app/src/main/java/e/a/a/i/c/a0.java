package e.a.a.i.c;

import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.a.g.f;
import e.c.b.j.ApiConfigRepository;
import e.c.b.j.b;
import e.c.b.j.c;
import java.util.Map;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: NetworkModule_ProvideApiConfigRepository$core_prodReleaseFactory */
public final class a0 implements Factory<c> {
    public final NetworkModule a;
    public final Provider<f> b;

    public a0(y yVar, Provider<f> provider) {
        this.a = yVar;
        this.b = provider;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.c.b.j.ApiConfigRepository, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        NetworkModule networkModule = this.a;
        IApiConfig iApiConfig = this.b.get();
        if (networkModule == null) {
            throw null;
        } else if (iApiConfig != null) {
            ApiConfigRepository apiConfigRepository = new ApiConfigRepository("");
            Map<b, String> a2 = iApiConfig.a();
            if (a2 != null) {
                apiConfigRepository.a.b(a2);
                j.c.a.a.c.n.c.a((Object) apiConfigRepository, "Cannot return null from a non-@Nullable @Provides method");
                return apiConfigRepository;
            }
            Intrinsics.a("urls");
            throw null;
        } else {
            Intrinsics.a("apiConfig");
            throw null;
        }
    }
}
