package e.a.a.i.c;

import e.a.a.g.a.h.EpguAddCookiesInterceptor;
import e.a.a.g.a.h.EpguSaveCookieInterceptor0;
import e.a.a.g.a.h.EsiaAddBearerInterceptor;
import e.a.a.g.a.h.b;
import e.a.a.g.a.h.d;
import e.c.b.m.InterceptorsConfigurator;
import e.c.b.m.a;
import j.c.a.a.c.n.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;
import o.Interceptor;
import o.n0.HttpLoggingInterceptor;

/* compiled from: NetworkModule_ProvideInterceptorsConfigurator$core_prodReleaseFactory */
public final class j0 implements Factory<a> {
    public final NetworkModule a;
    public final Provider<b> b;
    public final Provider<e.a.a.g.a.h.a> c;
    public final Provider<d> d;

    public j0(y yVar, Provider<b> provider, Provider<e.a.a.g.a.h.a> provider2, Provider<d> provider3) {
        this.a = yVar;
        this.b = provider;
        this.c = provider2;
        this.d = provider3;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.c.b.m.InterceptorsConfigurator, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        NetworkModule networkModule = this.a;
        EpguSaveCookieInterceptor0 epguSaveCookieInterceptor0 = this.b.get();
        EpguAddCookiesInterceptor epguAddCookiesInterceptor = this.c.get();
        EsiaAddBearerInterceptor esiaAddBearerInterceptor = this.d.get();
        if (networkModule == null) {
            throw null;
        } else if (epguSaveCookieInterceptor0 == null) {
            Intrinsics.a("epguSaveCookieInterceptor");
            throw null;
        } else if (epguAddCookiesInterceptor == null) {
            Intrinsics.a("epguAddCookiesInterceptor");
            throw null;
        } else if (esiaAddBearerInterceptor != null) {
            Interceptor[] interceptorArr = new Interceptor[4];
            HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor(null, 1);
            HttpLoggingInterceptor.a aVar = HttpLoggingInterceptor.a.BODY;
            if (aVar != null) {
                httpLoggingInterceptor.c = aVar;
                interceptorArr[0] = httpLoggingInterceptor;
                interceptorArr[1] = epguSaveCookieInterceptor0;
                interceptorArr[2] = epguAddCookiesInterceptor;
                interceptorArr[3] = esiaAddBearerInterceptor;
                InterceptorsConfigurator interceptorsConfigurator = new InterceptorsConfigurator(interceptorArr);
                c.a((Object) interceptorsConfigurator, "Cannot return null from a non-@Nullable @Provides method");
                return interceptorsConfigurator;
            }
            Intrinsics.a("<set-?>");
            throw null;
        } else {
            Intrinsics.a("esiaAddBearerInterceptor");
            throw null;
        }
    }
}
