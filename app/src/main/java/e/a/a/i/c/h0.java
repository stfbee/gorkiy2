package e.a.a.i.c;

import e.a.a.g.a.e.DateDeserializer;
import e.a.a.g.a.e.LocalDateTimeSerialization;
import e.a.a.g.a.e.LocalDateTimeSerialization0;
import j.c.a.a.c.n.c;
import j.c.d.DefaultDateTypeAdapter;
import j.c.d.Gson;
import j.c.d.GsonBuilder;
import j.c.d.b0.a0.TypeAdapters;
import j.c.d.k;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import k.a.Factory;
import n.n.c.Intrinsics;
import q.b.a.LocalDateTime;

/* compiled from: NetworkModule_ProvideGson$core_prodReleaseFactory */
public final class h0 implements Factory<k> {
    public final NetworkModule a;

    public h0(NetworkModule networkModule) {
        this.a = networkModule;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [j.c.d.Gson, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [j.c.d.Gson, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        if (this.a != null) {
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.f2546o = true;
            gsonBuilder.a(Date.class, new DateDeserializer());
            gsonBuilder.a(LocalDateTime.class, new LocalDateTimeSerialization());
            gsonBuilder.a(LocalDateTime.class, new LocalDateTimeSerialization0());
            ArrayList arrayList = new ArrayList(gsonBuilder.f2539f.size() + gsonBuilder.f2538e.size() + 3);
            arrayList.addAll(gsonBuilder.f2538e);
            Collections.reverse(arrayList);
            ArrayList arrayList2 = new ArrayList(gsonBuilder.f2539f);
            Collections.reverse(arrayList2);
            arrayList.addAll(arrayList2);
            int i2 = gsonBuilder.h;
            int i3 = gsonBuilder.f2540i;
            if (!(i2 == 2 || i3 == 2)) {
                DefaultDateTypeAdapter defaultDateTypeAdapter = new DefaultDateTypeAdapter(Date.class, i2, i3);
                DefaultDateTypeAdapter defaultDateTypeAdapter2 = new DefaultDateTypeAdapter(Timestamp.class, i2, i3);
                DefaultDateTypeAdapter defaultDateTypeAdapter3 = new DefaultDateTypeAdapter(java.sql.Date.class, i2, i3);
                arrayList.add(TypeAdapters.a(Date.class, defaultDateTypeAdapter));
                arrayList.add(new TypeAdapters.y(Timestamp.class, defaultDateTypeAdapter2));
                arrayList.add(new TypeAdapters.y(java.sql.Date.class, defaultDateTypeAdapter3));
            }
            Gson gson = new Gson(gsonBuilder.a, gsonBuilder.c, gsonBuilder.d, gsonBuilder.g, gsonBuilder.f2541j, gsonBuilder.f2545n, gsonBuilder.f2543l, gsonBuilder.f2544m, gsonBuilder.f2546o, gsonBuilder.f2542k, gsonBuilder.b, null, gsonBuilder.h, gsonBuilder.f2540i, gsonBuilder.f2538e, gsonBuilder.f2539f, arrayList);
            Intrinsics.a((Object) gson, "GsonBuilder()\n        .s…      )\n        .create()");
            c.a((Object) gson, "Cannot return null from a non-@Nullable @Provides method");
            return gson;
        }
        throw null;
    }
}
