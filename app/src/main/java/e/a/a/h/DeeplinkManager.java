package e.a.a.h;

import android.net.Uri;
import e.a.a.a.e.p.DeeplinkDto;
import e.a.a.a.e.p.c;
import e.b.a.f;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Set;
import n.i.Collections2;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: DeeplinkManager.kt */
public final class DeeplinkManager implements IDeeplinkManager {
    public final HashMap<String, Functions0<c, List<f>>> a = new HashMap<>();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.util.Set<java.lang.String>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public List<f> a(Uri uri) {
        String str;
        if (uri != null) {
            if (!(this.a.get(uri.getPath()) != null)) {
                return Collections2.b;
            }
            Functions0<c, List<f>> functions0 = this.a.get(uri.getPath());
            if (functions0 != null) {
                Functions0 functions02 = functions0;
                Set<String> queryParameterNames = uri.getQueryParameterNames();
                LinkedHashMap linkedHashMap = new LinkedHashMap();
                Intrinsics.a((Object) queryParameterNames, "queryNames");
                Iterator<T> it = queryParameterNames.iterator();
                while (true) {
                    str = "";
                    if (!it.hasNext()) {
                        break;
                    }
                    String str2 = (String) it.next();
                    Intrinsics.a((Object) str2, "it");
                    String queryParameter = uri.getQueryParameter(str2);
                    if (queryParameter != null) {
                        str = queryParameter;
                    }
                    linkedHashMap.put(str2, str);
                }
                String path = uri.getPath();
                if (path != null) {
                    str = path;
                }
                return (List) functions02.a(new DeeplinkDto(uri, str, linkedHashMap));
            }
            Intrinsics.a();
            throw null;
        }
        Intrinsics.a("uri");
        throw null;
    }
}
