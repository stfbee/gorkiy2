package e.a.a.a.d;

import android.widget.CompoundButton;
import e.a.a.a.d.DevActivityViewState0;
import e.c.c.b;

/* compiled from: DevActivity.kt */
public final class DevActivity implements CompoundButton.OnCheckedChangeListener {
    public final /* synthetic */ ru.covid19.core.presentation.dev.DevActivity b;

    public DevActivity(ru.covid19.core.presentation.dev.DevActivity devActivity) {
        this.b = devActivity;
    }

    public final void onCheckedChanged(CompoundButton compoundButton, boolean z) {
        this.b.u.a((b) new DevActivityViewState0.f(z));
    }
}
