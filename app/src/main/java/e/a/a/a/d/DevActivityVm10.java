package e.a.a.a.d;

import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: DevActivityVm.kt */
public final class DevActivityVm10<T, R> implements Function<Throwable, String> {
    public static final DevActivityVm10 a = new DevActivityVm10();

    public Object a(Object obj) {
        if (((Throwable) obj) != null) {
            return "";
        }
        Intrinsics.a("it");
        throw null;
    }
}
