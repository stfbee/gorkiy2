package e.a.a.a.c.a;

import android.content.Context;
import e.a.a.a.e.o.c;
import e.a.a.a.e.q.b;
import k.a.Factory;
import m.a.Provider;

public final class ErrorFragmentVm_Factory implements Factory<e> {
    public final Provider<Context> a;
    public final Provider<b> b;
    public final Provider<c> c;

    public ErrorFragmentVm_Factory(Provider<Context> provider, Provider<b> provider2, Provider<c> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    public Object get() {
        return new ErrorFragmentVm(this.a.get(), this.b.get(), this.c.get());
    }
}
