package e.a.a.a.a;

import android.net.Uri;
import android.util.Patterns;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.a.a.g.a.Session;
import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.j.a.IAuthRepository0;
import e.c.d.a.ViewAction;
import e.c.d.a.ViewState;
import e.c.d.c.BaseVm;
import l.b.Completable;
import l.b.Single;
import l.b.s.Disposable;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.e.d.SingleFlatMapCompletable;
import n.Unit;
import n.g;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import n.r.Indent;
import ru.covid19.core.data.network.model.SessionResponse;

/* compiled from: BaseAuthFragmentVm.kt */
public abstract class BaseAuthFragmentVm8 extends BaseVm {
    public String d;

    /* renamed from: e  reason: collision with root package name */
    public String f566e;

    /* renamed from: f  reason: collision with root package name */
    public String f567f;
    public boolean g;
    public final ViewAction<Integer> h;

    /* renamed from: i  reason: collision with root package name */
    public final ViewAction<g> f568i;

    /* renamed from: j  reason: collision with root package name */
    public final ViewAction<CharSequence> f569j;

    /* renamed from: k  reason: collision with root package name */
    public final ViewAction<CharSequence> f570k;

    /* renamed from: l  reason: collision with root package name */
    public final ViewAction<CharSequence> f571l;

    /* renamed from: m  reason: collision with root package name */
    public final ViewAction<g> f572m;

    /* renamed from: n  reason: collision with root package name */
    public final ViewAction<g> f573n;

    /* renamed from: o  reason: collision with root package name */
    public final ViewAction<g> f574o;

    /* renamed from: p  reason: collision with root package name */
    public final ViewState<Boolean> f575p;

    /* renamed from: q  reason: collision with root package name */
    public final ViewState<Boolean> f576q;

    /* renamed from: r  reason: collision with root package name */
    public final ViewState<Boolean> f577r;

    /* renamed from: s  reason: collision with root package name */
    public final ViewState<Boolean> f578s;

    /* renamed from: t  reason: collision with root package name */
    public final ViewState<Boolean> f579t;
    public final ViewState<Boolean> u;
    public final ITopLevelCoordinator v;
    public final IFragmentCoordinator w;
    public final IAuthRepository0 x;
    public final IAuthPrefs y;
    public final IApiConfig z;

    /* compiled from: com.android.tools.r8.jetbrains.kotlin-style lambda group */
    public static final class a extends j implements Functions0<g, g> {
        public final /* synthetic */ int c;
        public final /* synthetic */ Object d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(int i2, Object obj) {
            super(1);
            this.c = i2;
            this.d = obj;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [android.net.Uri, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public final Object a(Object obj) {
            int i2 = this.c;
            boolean z = true;
            if (i2 != 0) {
                if (i2 != 1) {
                    if (i2 != 2) {
                        if (i2 != 3) {
                            throw null;
                        } else if (((Unit) obj) != null) {
                            Collections.a(((BaseAuthFragmentVm8) this.d).w, (Integer) null, (Object) null, 3, (Object) null);
                            return Unit.a;
                        } else {
                            Intrinsics.a("it");
                            throw null;
                        }
                    } else if (((Unit) obj) != null) {
                        ITopLevelCoordinator iTopLevelCoordinator = ((BaseAuthFragmentVm8) this.d).v;
                        Uri parse = Uri.parse(((BaseAuthFragmentVm8) this.d).z.d() + "registration/");
                        Intrinsics.a((Object) parse, "Uri.parse(\"${apiConfig.esiaUrl}registration/\")");
                        iTopLevelCoordinator.a(parse);
                        return Unit.a;
                    } else {
                        Intrinsics.a("it");
                        throw null;
                    }
                } else if (((Unit) obj) != null) {
                    ITopLevelCoordinator iTopLevelCoordinator2 = ((BaseAuthFragmentVm8) this.d).v;
                    Uri parse2 = Uri.parse(((BaseAuthFragmentVm8) this.d).z.d() + "recovery/");
                    Intrinsics.a((Object) parse2, "Uri.parse(\"${apiConfig.esiaUrl}recovery/\")");
                    iTopLevelCoordinator2.a(parse2);
                    return Unit.a;
                } else {
                    Intrinsics.a("it");
                    throw null;
                }
            } else if (((Unit) obj) != null) {
                BaseAuthFragmentVm8 baseAuthFragmentVm8 = (BaseAuthFragmentVm8) this.d;
                if (baseAuthFragmentVm8.g) {
                    String str = baseAuthFragmentVm8.f566e;
                    if (str != null) {
                        if (str.length() != 11) {
                            z = false;
                        }
                        if (z) {
                            BaseAuthFragmentVm8.b((BaseAuthFragmentVm8) this.d);
                        } else {
                            ((BaseAuthFragmentVm8) this.d).f576q.a((Boolean) false);
                        }
                    } else {
                        Intrinsics.a("$this$isSnilsValid");
                        throw null;
                    }
                } else {
                    String str2 = baseAuthFragmentVm8.d;
                    if (str2 != null) {
                        if (!Patterns.EMAIL_ADDRESS.matcher(str2).matches()) {
                            String str3 = ((BaseAuthFragmentVm8) this.d).d;
                            if (str3 == null) {
                                Intrinsics.a("$this$isPhoneValid");
                                throw null;
                            } else if (!Patterns.PHONE.matcher(str3).matches()) {
                                ((BaseAuthFragmentVm8) this.d).f575p.a((Boolean) false);
                            }
                        }
                        BaseAuthFragmentVm8.b((BaseAuthFragmentVm8) this.d);
                    } else {
                        Intrinsics.a("$this$isEmailValid");
                        throw null;
                    }
                }
                return Unit.a;
            } else {
                Intrinsics.a("it");
                throw null;
            }
        }
    }

    /* compiled from: com.android.tools.r8.jetbrains.kotlin-style lambda group */
    public static final class b extends j implements Functions0<CharSequence, g> {
        public final /* synthetic */ int c;
        public final /* synthetic */ Object d;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(int i2, Object obj) {
            super(1);
            this.c = i2;
            this.d = obj;
        }

        public final Object a(Object obj) {
            int i2 = this.c;
            if (i2 == 0) {
                CharSequence charSequence = (CharSequence) obj;
                if (charSequence != null) {
                    BaseAuthFragmentVm8 baseAuthFragmentVm8 = (BaseAuthFragmentVm8) this.d;
                    String obj2 = charSequence.toString();
                    if (obj2 != null) {
                        baseAuthFragmentVm8.f567f = obj2;
                        BaseAuthFragmentVm8 baseAuthFragmentVm82 = (BaseAuthFragmentVm8) this.d;
                        baseAuthFragmentVm82.f578s.a(Boolean.valueOf(BaseAuthFragmentVm8.a(baseAuthFragmentVm82)));
                        ((BaseAuthFragmentVm8) this.d).f577r.a((Boolean) true);
                        return Unit.a;
                    }
                    Intrinsics.a("<set-?>");
                    throw null;
                }
                Intrinsics.a("it");
                throw null;
            } else if (i2 == 1) {
                CharSequence charSequence2 = (CharSequence) obj;
                if (charSequence2 != null) {
                    ((BaseAuthFragmentVm8) this.d).d = charSequence2.toString();
                    BaseAuthFragmentVm8 baseAuthFragmentVm83 = (BaseAuthFragmentVm8) this.d;
                    baseAuthFragmentVm83.f578s.a(Boolean.valueOf(BaseAuthFragmentVm8.a(baseAuthFragmentVm83)));
                    ((BaseAuthFragmentVm8) this.d).f577r.a((Boolean) true);
                    return Unit.a;
                }
                Intrinsics.a("it");
                throw null;
            } else if (i2 == 2) {
                CharSequence charSequence3 = (CharSequence) obj;
                if (charSequence3 != null) {
                    BaseAuthFragmentVm8 baseAuthFragmentVm84 = (BaseAuthFragmentVm8) this.d;
                    String obj3 = charSequence3.toString();
                    if (obj3 != null) {
                        baseAuthFragmentVm84.f566e = obj3;
                        BaseAuthFragmentVm8 baseAuthFragmentVm85 = (BaseAuthFragmentVm8) this.d;
                        baseAuthFragmentVm85.f578s.a(Boolean.valueOf(BaseAuthFragmentVm8.a(baseAuthFragmentVm85)));
                        ((BaseAuthFragmentVm8) this.d).f577r.a((Boolean) true);
                        return Unit.a;
                    }
                    Intrinsics.a("<set-?>");
                    throw null;
                }
                Intrinsics.a("it");
                throw null;
            } else {
                throw null;
            }
        }
    }

    /* compiled from: BaseAuthFragmentVm.kt */
    public static final class c extends j implements Functions0<Integer, g> {
        public final /* synthetic */ BaseAuthFragmentVm8 c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(BaseAuthFragmentVm8 baseAuthFragmentVm8) {
            super(1);
            this.c = baseAuthFragmentVm8;
        }

        public Object a(Object obj) {
            int intValue = ((Number) obj).intValue();
            BaseAuthFragmentVm8 baseAuthFragmentVm8 = this.c;
            boolean z = true;
            if (intValue != 1) {
                z = false;
            }
            baseAuthFragmentVm8.g = z;
            BaseAuthFragmentVm8 baseAuthFragmentVm82 = this.c;
            baseAuthFragmentVm82.f578s.a(Boolean.valueOf(BaseAuthFragmentVm8.a(baseAuthFragmentVm82)));
            return Unit.a;
        }
    }

    public BaseAuthFragmentVm8(ITopLevelCoordinator iTopLevelCoordinator, IFragmentCoordinator iFragmentCoordinator, IAuthRepository0 iAuthRepository0, IAuthPrefs iAuthPrefs, Session session, IApiConfig iApiConfig) {
        if (iTopLevelCoordinator == null) {
            Intrinsics.a("topLevelCoordinator");
            throw null;
        } else if (iFragmentCoordinator == null) {
            Intrinsics.a("fragmentCoordinator");
            throw null;
        } else if (iAuthRepository0 == null) {
            Intrinsics.a("authRepository");
            throw null;
        } else if (iAuthPrefs == null) {
            Intrinsics.a("authPrefs");
            throw null;
        } else if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (iApiConfig != null) {
            this.v = iTopLevelCoordinator;
            this.w = iFragmentCoordinator;
            this.x = iAuthRepository0;
            this.y = iAuthPrefs;
            this.z = iApiConfig;
            this.d = "";
            this.f566e = "";
            this.f567f = "";
            this.h = new ViewAction<>();
            this.f568i = new ViewAction<>();
            this.f569j = new ViewAction<>();
            this.f570k = new ViewAction<>();
            this.f571l = new ViewAction<>();
            this.f572m = new ViewAction<>();
            this.f573n = new ViewAction<>();
            this.f574o = new ViewAction<>();
            this.f575p = new ViewState<>(null, 1);
            this.f576q = new ViewState<>(null, 1);
            this.f577r = new ViewState<>(null, 1);
            this.f578s = new ViewState<>(false);
            this.f579t = new ViewState<>(false);
            this.u = new ViewState<>(false);
        } else {
            Intrinsics.a("apiConfig");
            throw null;
        }
    }

    public static final /* synthetic */ boolean a(BaseAuthFragmentVm8 baseAuthFragmentVm8) {
        if (baseAuthFragmentVm8.g) {
            if (Indent.b(baseAuthFragmentVm8.f566e) || !(!Indent.b(baseAuthFragmentVm8.f567f))) {
                return false;
            }
        } else if (Indent.b(baseAuthFragmentVm8.d) || !(!Indent.b(baseAuthFragmentVm8.f567f))) {
            return false;
        }
        return true;
    }

    /* JADX WARN: Type inference failed for: r5v0, types: [l.b.u.e.d.SingleFlatMapCompletable, l.b.Completable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.a.BaseAuthFragmentVm, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ void b(BaseAuthFragmentVm8 baseAuthFragmentVm8) {
        BaseAuthFragmentVm8 baseAuthFragmentVm82 = baseAuthFragmentVm8;
        Single<SessionResponse> a2 = baseAuthFragmentVm82.x.a(baseAuthFragmentVm82.g ? baseAuthFragmentVm82.f566e : baseAuthFragmentVm82.d, baseAuthFragmentVm82.f567f, true);
        BaseAuthFragmentVm baseAuthFragmentVm = new BaseAuthFragmentVm(baseAuthFragmentVm82);
        if (a2 != null) {
            ObjectHelper.a((Object) baseAuthFragmentVm, "mapper is null");
            ? singleFlatMapCompletable = new SingleFlatMapCompletable(a2, baseAuthFragmentVm);
            BaseAuthFragmentVm0 baseAuthFragmentVm0 = new BaseAuthFragmentVm0(baseAuthFragmentVm82);
            Consumer<Object> consumer = Functions.d;
            Action action = Functions.c;
            Completable a3 = singleFlatMapCompletable.a(baseAuthFragmentVm0, consumer, action, action, action, action);
            BaseAuthFragmentVm1 baseAuthFragmentVm1 = new BaseAuthFragmentVm1(baseAuthFragmentVm82);
            Consumer<Object> consumer2 = Functions.d;
            Action action2 = Functions.c;
            Disposable a4 = a3.a(consumer2, baseAuthFragmentVm1, action2, action2, action2, action2).a(new d(0, baseAuthFragmentVm82)).a(new d(1, baseAuthFragmentVm82), BaseAuthFragmentVm2.b);
            Intrinsics.a((Object) a4, "authRepository.getSessio… error: \")\n            })");
            j.c.a.a.c.n.c.a(a4, super.b);
            return;
        }
        throw null;
    }

    /* JADX WARN: Type inference failed for: r5v4, types: [l.b.u.e.d.SingleFlatMapCompletable, l.b.Completable] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.a.BaseAuthFragmentVm3, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void c() {
        if (this.y.a()) {
            Single<SessionResponse> b2 = this.x.b(this.y.b(), null, true);
            BaseAuthFragmentVm3 baseAuthFragmentVm3 = new BaseAuthFragmentVm3(this);
            if (b2 != null) {
                ObjectHelper.a((Object) baseAuthFragmentVm3, "mapper is null");
                ? singleFlatMapCompletable = new SingleFlatMapCompletable(b2, baseAuthFragmentVm3);
                BaseAuthFragmentVm4 baseAuthFragmentVm4 = new BaseAuthFragmentVm4(this);
                Consumer<Object> consumer = Functions.d;
                Action action = Functions.c;
                Completable a2 = singleFlatMapCompletable.a(baseAuthFragmentVm4, consumer, action, action, action, action);
                BaseAuthFragmentVm5 baseAuthFragmentVm5 = new BaseAuthFragmentVm5(this);
                Consumer<Object> consumer2 = Functions.d;
                Action action2 = Functions.c;
                Disposable a3 = a2.a(consumer2, baseAuthFragmentVm5, action2, action2, action2, action2).a(new BaseAuthFragmentVm6(this), new BaseAuthFragmentVm7(this));
                Intrinsics.a((Object) a3, "authRepository.createSes…ept(false)\n            })");
                j.c.a.a.c.n.c.a(a3, super.b);
            } else {
                throw null;
            }
        } else {
            this.u.a((Boolean) true);
        }
        super.b.a(this.h.a(new c(this)), this.f568i.a(new a(0, this)), this.f569j.a(new b(0, this)), this.f571l.a(new b(1, this)), this.f570k.a(new b(2, this)), this.f572m.a(new a(1, this)), this.f573n.a(new a(2, this)), this.f574o.a(new a(3, this)));
    }

    public abstract Completable d();

    public abstract void e();
}
