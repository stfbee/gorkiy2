package e.a.a.a.a;

import e.a.a.e;
import e.a.a.f;

/* compiled from: LoginPagerAdapter.kt */
public enum LoginPagerAdapter {
    PHONE(f.frag_auth_login_phone_email, e.frag_phone_mail_login),
    SNILS(f.frag_auth_login_snils, e.frag_snils_login);
    
    public final int layoutResId;
    public final int titleResId;

    /* access modifiers changed from: public */
    LoginPagerAdapter(int i2, int i3) {
        this.titleResId = i2;
        this.layoutResId = i3;
    }
}
