package e.a.a.a.c.a;

import android.content.Context;
import com.crashlytics.android.core.CrashlyticsController;
import e.a.a.a.c.a.ErrorFragmentViewState1;
import e.a.a.a.c.a.ErrorFragmentViewState2;
import e.a.a.a.c.a.b;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.p.ErrorNavigationDto0;
import e.a.a.a.e.p.ErrorResultDto;
import e.a.a.a.e.q.IFragmentCoordinator;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseMviVm1;
import e.c.c.BaseViewState1;
import e.c.c.k;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;

/* compiled from: ErrorFragmentVm.kt */
public final class ErrorFragmentVm extends BaseMviVm1<d> {

    /* renamed from: i  reason: collision with root package name */
    public final Context f594i;

    /* renamed from: j  reason: collision with root package name */
    public final IFragmentCoordinator f595j;

    /* renamed from: k  reason: collision with root package name */
    public final ITopLevelCoordinator f596k;

    /* compiled from: ErrorFragmentVm.kt */
    public static final class a<T, R> implements Function<T, R> {
        public static final a a = new a();

        public Object a(Object obj) {
            ErrorFragmentViewState1.b bVar = (ErrorFragmentViewState1.b) obj;
            if (bVar != null) {
                return new BaseMviVm0.a(new ErrorFragmentViewState2.a(bVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ErrorFragmentVm.kt */
    public static final class b extends j implements Functions0<b.a, g> {
        public final /* synthetic */ ErrorFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ErrorFragmentVm errorFragmentVm) {
            super(1);
            this.c = errorFragmentVm;
        }

        public Object a(Object obj) {
            if (((ErrorFragmentViewState1.a) obj) != null) {
                ErrorFragmentVm.a(this.c);
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ErrorFragmentVm.kt */
    public static final class c extends j implements Functions0<b.c, g> {
        public final /* synthetic */ ErrorFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(ErrorFragmentVm errorFragmentVm) {
            super(1);
            this.c = errorFragmentVm;
        }

        public Object a(Object obj) {
            if (((ErrorFragmentViewState1.c) obj) != null) {
                ErrorFragmentVm errorFragmentVm = this.c;
                errorFragmentVm.f595j.a(new ErrorResultDto(((ErrorFragmentViewState0) errorFragmentVm.e()).a.b, ErrorNavigationDto0.SUBMIT, ((ErrorFragmentViewState0) errorFragmentVm.e()).a.f592l, ((ErrorFragmentViewState0) errorFragmentVm.e()).a.f591k));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    public ErrorFragmentVm(Context context, IFragmentCoordinator iFragmentCoordinator, ITopLevelCoordinator iTopLevelCoordinator) {
        if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (iFragmentCoordinator == null) {
            Intrinsics.a("fragmentCoordinator");
            throw null;
        } else if (iTopLevelCoordinator != null) {
            this.f594i = context;
            this.f595j = iFragmentCoordinator;
            this.f596k = iTopLevelCoordinator;
        } else {
            Intrinsics.a("topLevelCoordinator");
            throw null;
        }
    }

    public Object a(Object obj, BaseViewState1 baseViewState1) {
        ErrorFragmentViewState0 errorFragmentViewState0 = (ErrorFragmentViewState0) obj;
        if (errorFragmentViewState0 == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof ErrorFragmentViewState2.a) {
            ErrorFragmentViewState errorFragmentViewState = ((ErrorFragmentViewState2.a) baseViewState1).a;
            if (errorFragmentViewState != null) {
                return new ErrorFragmentViewState0(errorFragmentViewState);
            }
            Intrinsics.a(CrashlyticsController.EVENT_TYPE_LOGGED);
            throw null;
        } else {
            super.a(errorFragmentViewState0, baseViewState1);
            return errorFragmentViewState0;
        }
    }

    public Object d() {
        return new ErrorFragmentViewState0(null, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(ErrorFragmentViewState1.b.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(ErrorFragmentViewState1.a.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(ErrorFragmentViewState1.c.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a5 = Observable.a(super.a(observable), a2.c((Function) a.a), BaseMviVm.a(a3, new b(this)), BaseMviVm.a(a4, new c(this)));
            Intrinsics.a((Object) a5, "Observable.mergeArray(\n …)\n            }\n        )");
            return a5;
        }
        Intrinsics.a("o");
        throw null;
    }

    public static final /* synthetic */ void a(ErrorFragmentVm errorFragmentVm) {
        if (((ErrorFragmentViewState0) super.e()).a.f589i) {
            errorFragmentVm.f596k.b();
        } else {
            errorFragmentVm.f595j.a(new ErrorResultDto(((ErrorFragmentViewState0) super.e()).a.b, ErrorNavigationDto0.BACK, ((ErrorFragmentViewState0) super.e()).a.f593m, ((ErrorFragmentViewState0) super.e()).a.f590j));
        }
    }
}
