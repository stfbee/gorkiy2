package e.a.a.a.e.q;

import e.a.a.a.e.BaseScreens;
import e.a.a.a.e.BaseScreens0;
import e.a.a.a.e.BaseScreens2;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.a.a.e.p.ErrorNavigationDto;
import e.a.a.a.e.p.ErrorResultDto;
import e.a.a.a.e.p.ProcessingNavigationDto0;
import e.a.a.a.e.p.f;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.a.e.s.DpRouter;
import e.a.a.a.e.s.FragmentRouter;
import e.b.a.Screen;
import e.b.a.h.Back;
import e.b.a.h.Command;
import l.b.Observable;
import l.b.ObservableEmitter;
import l.b.ObservableOnSubscribe;
import l.b.t.Action;
import l.b.t.Consumer;
import l.b.t.Predicate;
import l.b.u.b.Functions;
import l.b.u.b.ObjectHelper;
import l.b.u.e.c.ObservableCreate;
import l.b.u.e.c.ObservableDoOnLifecycle;
import n.n.c.Intrinsics;

/* compiled from: FragmentCoordinator.kt */
public class FragmentCoordinator implements IFragmentCoordinator {
    public String a;
    public final FragmentCiceroneHolder b;

    /* compiled from: FragmentCoordinator.kt */
    public static final class a<T> implements Predicate<f> {
        public final /* synthetic */ String b;

        public a(String str) {
            this.b = str;
        }

        public boolean a(Object obj) {
            ErrorResultDto errorResultDto = (ErrorResultDto) obj;
            if (errorResultDto != null) {
                String str = this.b;
                return str == null || Intrinsics.a(errorResultDto.b, str);
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: FragmentCoordinator.kt */
    public static final class b<T> implements ObservableOnSubscribe<T> {
        public final /* synthetic */ FragmentCoordinator a;
        public final /* synthetic */ int b;

        /* compiled from: FragmentCoordinator.kt */
        public static final class a implements DpRouter {
            public final /* synthetic */ ObservableEmitter a;

            public a(ObservableEmitter observableEmitter) {
                this.a = observableEmitter;
            }

            public void a(Object obj) {
                if ((!(obj instanceof Object) ? null : obj) != null) {
                    ((ObservableCreate.a) this.a).a(obj);
                }
            }
        }

        public b(FragmentCoordinator fragmentCoordinator, int i2) {
            this.a = fragmentCoordinator;
            this.b = i2;
        }

        public final void a(ObservableEmitter<T> observableEmitter) {
            Object obj = null;
            if (observableEmitter != null) {
                FragmentRouter n2 = this.a.n();
                int i2 = this.b;
                Object obj2 = n2.c.get(i2);
                if (obj2 != null) {
                    n2.c.delete(i2);
                    obj = obj2;
                }
                if (obj != null) {
                    ((ObservableCreate.a) observableEmitter).a(obj);
                }
                FragmentRouter n3 = this.a.n();
                n3.b.put(this.b, new a(observableEmitter));
                return;
            }
            Intrinsics.a("subscriber");
            throw null;
        }
    }

    /* compiled from: FragmentCoordinator.kt */
    public static final class c implements Action {
        public final /* synthetic */ FragmentCoordinator a;
        public final /* synthetic */ int b;

        public c(FragmentCoordinator fragmentCoordinator, int i2) {
            this.a = fragmentCoordinator;
            this.b = i2;
        }

        public final void run() {
            FragmentCoordinator fragmentCoordinator = this.a;
            fragmentCoordinator.n().b.remove(this.b);
        }
    }

    public FragmentCoordinator(FragmentCiceroneHolder fragmentCiceroneHolder) {
        if (fragmentCiceroneHolder != null) {
            this.b = fragmentCiceroneHolder;
            this.a = "";
            return;
        }
        Intrinsics.a("fragmentCiceroneHolder");
        throw null;
    }

    public void a(String str) {
        if (str != null) {
            this.a = str;
        } else {
            Intrinsics.a("<set-?>");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.a.a.a.e.p.f>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<f> b(String str) {
        Observable<f> a2 = a(1).a(new a(str));
        Intrinsics.a((Object) a2, "addResultListener<ErrorR…it.errorKey == errorKey }");
        return a2;
    }

    public Observable<e.a.a.a.e.p.a> f() {
        return a(3);
    }

    public final FragmentRouter n() {
        return (FragmentRouter) this.b.a(o());
    }

    public String o() {
        return this.a;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.q.FragmentCoordinator$b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [l.b.t.Consumer<java.lang.Object>, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.q.FragmentCoordinator$c, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.u.e.c.ObservableDoOnLifecycle, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final <T> Observable<T> a(int i2) {
        b bVar = new b(this, i2);
        ObjectHelper.a((Object) bVar, "source is null");
        ObservableCreate observableCreate = new ObservableCreate(bVar);
        c cVar = new c(this, i2);
        Consumer<Object> consumer = Functions.d;
        ObjectHelper.a((Object) consumer, "onSubscribe is null");
        ObjectHelper.a((Object) cVar, "onDispose is null");
        ObservableDoOnLifecycle observableDoOnLifecycle = new ObservableDoOnLifecycle(observableCreate, consumer, cVar);
        Intrinsics.a((Object) observableDoOnLifecycle, "Observable.create<T> { s…tener(code)\n            }");
        return observableDoOnLifecycle;
    }

    public void a(Integer num, Object obj) {
        if (num != null) {
            FragmentRouter n2 = n();
            int intValue = num.intValue();
            n2.a();
            n2.a(intValue, obj);
            return;
        }
        n().a();
    }

    public void a(ErrorNavigationDto errorNavigationDto) {
        if (errorNavigationDto != null) {
            n().a((Screen) new BaseScreens0(ErrorNavigationDto.a(errorNavigationDto, null, o(), null, null, null, null, 0, 0, 0, 0, false, false, false, false, false, false, null, null, 262141)));
            return;
        }
        Intrinsics.a("errorNavigationDto");
        throw null;
    }

    public void a(ErrorResultDto errorResultDto) {
        if (errorResultDto != null) {
            FragmentRouter n2 = n();
            if (!errorResultDto.f620e) {
                n2.a();
                n2.a(1, errorResultDto);
            } else if (n2 != null) {
                n2.a.a(new Command[]{new Back(), new Back()});
                n2.a(1, errorResultDto);
            } else {
                throw null;
            }
        } else {
            Intrinsics.a("resultDto");
            throw null;
        }
    }

    public void a(ProcessingNavigationDto0 processingNavigationDto0) {
        if (processingNavigationDto0 != null) {
            FragmentRouter n2 = n();
            n2.a();
            n2.a(2, processingNavigationDto0);
            return;
        }
        Intrinsics.a("resultCode");
        throw null;
    }

    public void a(BottomSheetSelectorNavigationDto0 bottomSheetSelectorNavigationDto0) {
        if (bottomSheetSelectorNavigationDto0 != null) {
            n().a((BaseScreens2) new BaseScreens(bottomSheetSelectorNavigationDto0));
        } else {
            Intrinsics.a("dto");
            throw null;
        }
    }

    public void a(BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto) {
        if (bottomSheetSelectorNavigationDto != null) {
            FragmentRouter n2 = n();
            n2.a();
            n2.a(3, bottomSheetSelectorNavigationDto);
            return;
        }
        n().a();
    }
}
