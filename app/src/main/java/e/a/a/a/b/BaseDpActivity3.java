package e.a.a.a.b;

import n.Unit;
import n.g;
import n.n.b.Functions1;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: BaseDpActivity.kt */
public final /* synthetic */ class BaseDpActivity3 extends h implements Functions1<String, Object, g> {
    public BaseDpActivity3(BaseDpActivity4 baseDpActivity4) {
        super(2, baseDpActivity4);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [e.a.a.a.b.BaseDpActivity3, n.n.c.CallableReference] */
    public Object a(Object obj, Object obj2) {
        if (((String) obj) == null) {
            Intrinsics.a("p1");
            throw null;
        } else if (((BaseDpActivity4) this.c) != null) {
            return Unit.a;
        } else {
            throw null;
        }
    }

    public final String f() {
        return "showMessage";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(BaseDpActivity4.class);
    }

    public final String i() {
        return "showMessage(Ljava/lang/String;Ljava/lang/Object;)V";
    }
}
