package e.a.a.a.d;

import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import e.a.a.a.d.DevActivityViewState0;
import e.a.a.d;
import e.c.c.b;
import n.n.c.Intrinsics;
import ru.covid19.core.presentation.dev.DevActivity;

/* compiled from: DevActivity.kt */
public final class DevActivity0 implements AdapterView.OnItemSelectedListener {
    public final /* synthetic */ DevActivity b;
    public final /* synthetic */ Spinner c;

    public DevActivity0(DevActivity devActivity, Spinner spinner) {
        this.b = devActivity;
        this.c = spinner;
    }

    public void onItemSelected(AdapterView<?> adapterView, View view, int i2, long j2) {
        if (adapterView == null) {
            Intrinsics.a("parent");
            throw null;
        } else if (Intrinsics.a(this.c, (Spinner) this.b.c(d.act_dev_menu_sp_endpoint))) {
            this.b.u.a((b) new DevActivityViewState0.b(adapterView.getSelectedItem().toString()));
        }
    }

    public void onNothingSelected(AdapterView<?> adapterView) {
    }
}
