package e.a.a.a.d;

import android.content.Intent;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;
import ru.covid19.core.presentation.dev.DevActivity;

/* compiled from: DevActivity.kt */
public final /* synthetic */ class DevActivity3 extends h implements Functions0<Intent, g> {
    public DevActivity3(DevActivity devActivity) {
        super(1, devActivity);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.d.DevActivity3, n.n.c.CallableReference] */
    public Object a(Object obj) {
        Intent intent = (Intent) obj;
        if (intent != null) {
            ((DevActivity) this.c).startActivity(intent);
            return Unit.a;
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "openActivity";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(DevActivity.class);
    }

    public final String i() {
        return "openActivity(Landroid/content/Intent;)V";
    }
}
