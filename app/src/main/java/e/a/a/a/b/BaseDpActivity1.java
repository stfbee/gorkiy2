package e.a.a.a.b;

import e.b.a.Navigator;
import n.Unit;
import n.g;
import n.n.b.Functions;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: BaseDpActivity.kt */
public final /* synthetic */ class BaseDpActivity1 extends h implements Functions<g> {
    public BaseDpActivity1(BaseDpActivity4 baseDpActivity4) {
        super(0, baseDpActivity4);
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [e.a.a.a.b.BaseDpActivity4, android.app.Activity] */
    public Object b() {
        ? r0 = (BaseDpActivity4) this.c;
        r0.r().a().a((Navigator) null);
        r0.finish();
        return Unit.a;
    }

    public final String f() {
        return "finishActivity";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(BaseDpActivity4.class);
    }

    public final String i() {
        return "finishActivity()V";
    }
}
