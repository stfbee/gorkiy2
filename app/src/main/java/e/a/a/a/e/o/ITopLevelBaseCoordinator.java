package e.a.a.a.e.o;

import android.net.Uri;
import l.b.Observable;

/* compiled from: ITopLevelBaseCoordinator.kt */
public interface ITopLevelBaseCoordinator {
    void a(Uri uri);

    void a(String str, Object obj);

    void a(boolean z);

    void b();

    void b(boolean z);

    Observable<Uri> c();
}
