package e.a.a.a.e.o;

import e.a.a.a.e.s.ActivityRouter;
import e.a.a.a.e.s.DpRouter;
import l.b.ObservableEmitter;
import l.b.ObservableOnSubscribe;
import l.b.u.e.c.ObservableCreate;
import n.n.c.Intrinsics;

/* compiled from: TopLevelBaseCoordinator.kt */
public final class TopLevelBaseCoordinator<T> implements ObservableOnSubscribe<T> {
    public final /* synthetic */ TopLevelBaseCoordinator1 a;
    public final /* synthetic */ int b;

    /* compiled from: TopLevelBaseCoordinator.kt */
    public static final class a implements DpRouter {
        public final /* synthetic */ ObservableEmitter a;

        public a(ObservableEmitter observableEmitter) {
            this.a = observableEmitter;
        }

        public void a(Object obj) {
            if ((!(obj instanceof Object) ? null : obj) != null) {
                ((ObservableCreate.a) this.a).a(obj);
            }
        }
    }

    public TopLevelBaseCoordinator(TopLevelBaseCoordinator1 topLevelBaseCoordinator1, int i2) {
        this.a = topLevelBaseCoordinator1;
        this.b = i2;
    }

    public final void a(ObservableEmitter<T> observableEmitter) {
        Object obj = null;
        if (observableEmitter != null) {
            ActivityRouter activityRouter = this.a.a.b;
            int i2 = this.b;
            Object obj2 = activityRouter.c.get(i2);
            if (obj2 != null) {
                activityRouter.c.delete(i2);
                obj = obj2;
            }
            if (obj != null) {
                ((ObservableCreate.a) observableEmitter).a(obj);
            }
            ActivityRouter activityRouter2 = this.a.a.b;
            activityRouter2.b.put(this.b, new a(observableEmitter));
            return;
        }
        Intrinsics.a("subscriber");
        throw null;
    }
}
