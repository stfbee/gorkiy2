package e.a.a.a.b;

import android.os.Parcelable;
import e.a.a.a.e.p.ErrorResultDto;
import e.a.a.a.e.p.f;
import e.c.c.BaseViewState0;
import e.c.c.b;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.p.KDeclarationContainer;

/* compiled from: BaseDpFragmentVm.kt */
public final /* synthetic */ class BaseDpFragmentVm2 extends h implements Functions0<f, g> {
    public BaseDpFragmentVm2(BaseDpFragmentVm1 baseDpFragmentVm1) {
        super(1, baseDpFragmentVm1);
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [e.a.a.a.b.BaseDpFragmentVm2, n.n.c.CallableReference] */
    public Object a(Object obj) {
        ErrorResultDto errorResultDto = (ErrorResultDto) obj;
        if (errorResultDto != null) {
            BaseDpFragmentVm1 baseDpFragmentVm1 = (BaseDpFragmentVm1) this.c;
            if (baseDpFragmentVm1 != null) {
                Parcelable parcelable = errorResultDto.d;
                if (parcelable != null) {
                    baseDpFragmentVm1.g.a((b) ((BaseViewState0) parcelable));
                }
                return Unit.a;
            }
            throw null;
        }
        Intrinsics.a("p1");
        throw null;
    }

    public final String f() {
        return "onErrorScreenReturn";
    }

    /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
    public final KDeclarationContainer g() {
        return Reflection.a(BaseDpFragmentVm1.class);
    }

    public final String i() {
        return "onErrorScreenReturn(Lru/covid19/core/presentation/navigation/dto/ErrorResultDto;)V";
    }
}
