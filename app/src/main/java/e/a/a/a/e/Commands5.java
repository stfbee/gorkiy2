package e.a.a.a.e;

import e.b.a.Screen;
import e.b.a.h.Command;
import n.n.c.Intrinsics;

/* compiled from: Commands.kt */
public final class Commands5 implements Command {
    public final int a;
    public final Screen b;
    public final Object c;

    public Commands5(int i2, Screen screen, Object obj) {
        if (screen != null) {
            this.a = i2;
            this.b = screen;
            this.c = obj;
            return;
        }
        Intrinsics.a("screen");
        throw null;
    }
}
