package e.a.a.a.b;

import android.content.Intent;
import android.os.Bundle;
import e.a.a.a.e.BaseScreens1;
import e.a.a.a.e.BaseScreens2;
import e.a.a.a.e.p.DeeplinkDto;
import e.a.a.a.e.q.SingleFragmentNavigator;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.a.e.s.FragmentRouter;
import e.a.a.d;
import e.a.a.e;
import e.a.a.h.IDeeplinkManager;
import e.a.a.i.e.DeeplinkManagerWrapper;
import e.b.a.Screen;
import e.b.a.f;
import e.c.d.c.a;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import kotlin.TypeCastException;
import n.i._Arrays;
import n.n.c.Intrinsics;

/* compiled from: BaseDpFragmentActivity.kt */
public abstract class BaseDpFragmentActivity<VM extends a> extends BaseDpActivity4<VM> {
    public final int x = d.container;
    public DeeplinkDto y;
    public final DeeplinkManagerWrapper z = new DeeplinkManagerWrapper();

    public void a(DeeplinkDto deeplinkDto) {
        T t2 = null;
        if (deeplinkDto != null) {
            IDeeplinkManager iDeeplinkManager = this.z.a;
            if (iDeeplinkManager != null) {
                List<f> a = iDeeplinkManager.a(deeplinkDto.b);
                if (a.size() > 1) {
                    Iterator<T> it = a.iterator();
                    while (true) {
                        if (!it.hasNext()) {
                            break;
                        }
                        T next = it.next();
                        Screen screen = (Screen) next;
                        Class<?> cls = getClass();
                        if (!(screen instanceof BaseScreens1)) {
                            screen = null;
                        }
                        BaseScreens1 baseScreens1 = (BaseScreens1) screen;
                        if (Intrinsics.a(cls, baseScreens1 != null ? baseScreens1.a() : null)) {
                            t2 = next;
                            break;
                        }
                    }
                    Screen screen2 = (Screen) t2;
                    if (screen2 != null) {
                        List<f> subList = a.subList(a.indexOf(screen2) + 1, a.size());
                        ArrayList arrayList = new ArrayList();
                        for (T next2 : subList) {
                            if (next2 instanceof BaseScreens2) {
                                arrayList.add(next2);
                            }
                        }
                        if (!arrayList.isEmpty()) {
                            FragmentRouter fragmentRouter = s().b;
                            Object[] array = arrayList.toArray(new BaseScreens2[0]);
                            if (array != null) {
                                Screen[] screenArr = (Screen[]) array;
                                fragmentRouter.a((Screen[]) Arrays.copyOf(screenArr, screenArr.length));
                                return;
                            }
                            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
                        }
                        return;
                    }
                    return;
                }
                return;
            }
            Intrinsics.b("deeplinkManager");
            throw null;
        }
        Intrinsics.a("deeplinkDto");
        throw null;
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [e.c.d.b.b.BaseMvvmActivity, e.a.a.a.b.BaseDpFragmentActivity, i.l.a.FragmentActivity] */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void k() {
        BaseDpFragmentActivity.super.k();
        FragmentCiceroneHolder s2 = s();
        String name = h().getClass().getName();
        Intrinsics.a((Object) name, "vm::class.java.name");
        s2.c(name).a(t());
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [i.b.k.AppCompatActivity, e.a.a.a.b.BaseDpFragmentActivity, e.a.a.a.b.BaseDpActivity4, android.app.Activity] */
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        Intent intent = getIntent();
        this.y = (DeeplinkDto) (intent != null ? intent.getParcelableExtra("EXTRA_SCREEN_DEEPLINK") : null);
        setContentView(e.act_base);
    }

    /* JADX WARN: Type inference failed for: r1v0, types: [e.a.a.a.b.BaseDpFragmentActivity, i.l.a.FragmentActivity] */
    public void onNewIntent(Intent intent) {
        BaseDpFragmentActivity.super.onNewIntent(intent);
        DeeplinkDto deeplinkDto = (DeeplinkDto) (intent != null ? intent.getParcelableExtra("EXTRA_SCREEN_DEEPLINK") : null);
        this.y = deeplinkDto;
        if (deeplinkDto != null) {
            a(deeplinkDto);
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void onPause() {
        super.onPause();
        FragmentCiceroneHolder s2 = s();
        String name = h().getClass().getName();
        Intrinsics.a((Object) name, "vm::class.java.name");
        s2.c(name).a = null;
    }

    /* JADX WARN: Type inference failed for: r2v0, types: [i.b.k.AppCompatActivity, e.a.a.a.b.BaseDpFragmentActivity] */
    public void onPostCreate(Bundle bundle) {
        BaseDpFragmentActivity.super.onPostCreate(bundle);
        t().a = this.x;
        if (bundle != null) {
            String[] stringArray = bundle.getStringArray(this.A);
            if (stringArray != null) {
                if (!(stringArray.length == 0)) {
                    t().b = new LinkedList<>(_Arrays.a(stringArray));
                    return;
                }
                return;
            }
            return;
        }
        DeeplinkDto deeplinkDto = this.y;
        if (deeplinkDto != null) {
            a(deeplinkDto);
        }
    }

    /* JADX WARN: Type inference failed for: r3v0, types: [i.b.k.AppCompatActivity, e.a.a.a.b.BaseDpFragmentActivity] */
    public void onSaveInstanceState(Bundle bundle) {
        if (bundle != null) {
            String str = this.A;
            Object[] array = t().b.toArray(new String[0]);
            if (array != null) {
                bundle.putStringArray(str, (String[]) array);
                BaseDpFragmentActivity.super.onSaveInstanceState(bundle);
                return;
            }
            throw new TypeCastException("null cannot be cast to non-null type kotlin.Array<T>");
        }
        Intrinsics.a("outState");
        throw null;
    }

    public final FragmentCiceroneHolder s() {
        FragmentCiceroneHolder fragmentCiceroneHolder = super.v.a;
        if (fragmentCiceroneHolder != null) {
            return fragmentCiceroneHolder;
        }
        Intrinsics.b("fragmentCiceroneNavigator");
        throw null;
    }

    public abstract SingleFragmentNavigator t();
}
