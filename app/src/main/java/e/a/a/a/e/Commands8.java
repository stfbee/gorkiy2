package e.a.a.a.e;

import e.b.a.h.Command;
import n.n.c.Intrinsics;

/* compiled from: Commands.kt */
public final class Commands8 implements Command {
    public final String a;
    public final Object b;

    public Commands8(String str, Object obj) {
        if (str != null) {
            this.a = str;
            this.b = obj;
            return;
        }
        Intrinsics.a("message");
        throw null;
    }
}
