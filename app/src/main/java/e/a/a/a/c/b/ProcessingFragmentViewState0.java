package e.a.a.a.c.b;

import e.a.a.a.e.p.ProcessingNavigationDto;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ProcessingFragmentViewState.kt */
public abstract class ProcessingFragmentViewState0 extends BaseViewState0 {

    /* compiled from: ProcessingFragmentViewState.kt */
    public static final class a extends ProcessingFragmentViewState0 {
        public final ProcessingNavigationDto a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ProcessingNavigationDto processingNavigationDto) {
            super(null);
            if (processingNavigationDto != null) {
                this.a = processingNavigationDto;
                return;
            }
            Intrinsics.a("dto");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            ProcessingNavigationDto processingNavigationDto = this.a;
            if (processingNavigationDto != null) {
                return processingNavigationDto.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("InitEvent(dto=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    public /* synthetic */ ProcessingFragmentViewState0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
