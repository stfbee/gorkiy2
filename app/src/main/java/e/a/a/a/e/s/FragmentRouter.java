package e.a.a.a.e.s;

import e.a.a.a.e.BaseScreens2;
import e.a.a.a.e.Commands7;
import e.b.a.h.Command;
import n.n.c.Intrinsics;

/* compiled from: FragmentRouter.kt */
public final class FragmentRouter extends DpRouter0 {
    public final void a(BaseScreens2 baseScreens2) {
        if (baseScreens2 != null) {
            this.a.a(new Command[]{new Commands7(baseScreens2)});
            return;
        }
        Intrinsics.a("screen");
        throw null;
    }
}
