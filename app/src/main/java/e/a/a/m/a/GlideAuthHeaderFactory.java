package e.a.a.m.a;

import e.a.a.g.b.b.a.IAuthPrefs;
import j.a.a.a.outline;
import j.b.a.m.n.LazyHeaderFactory;
import java.util.Map;
import n.n.c.Intrinsics;
import n.r.Indent;

/* compiled from: GlideAuthHeaderFactory.kt */
public final class GlideAuthHeaderFactory implements LazyHeaderFactory {
    public final IAuthPrefs a;

    public GlideAuthHeaderFactory(IAuthPrefs iAuthPrefs) {
        if (iAuthPrefs != null) {
            this.a = iAuthPrefs;
        } else {
            Intrinsics.a("authPrefs");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public String a() {
        StringBuilder sb = new StringBuilder();
        if (!Indent.b(this.a.e().d)) {
            StringBuilder a2 = outline.a("sp_session=");
            a2.append(this.a.e().d);
            a2.append("; ");
            sb.append(a2.toString());
        }
        if (!Indent.b(this.a.e().a)) {
            StringBuilder a3 = outline.a("acc_t=");
            a3.append(this.a.e().a);
            a3.append("; ");
            sb.append(a3.toString());
        }
        if (this.a.e().c.size() > 0) {
            for (Map.Entry next : this.a.e().c.entrySet()) {
                String str = (String) next.getKey();
                String str2 = (String) next.getValue();
                boolean z = false;
                if (str.length() > 0) {
                    if (str2.length() > 0) {
                        z = true;
                    }
                    if (z) {
                        sb.append(str + '=' + str2 + "; ");
                    }
                }
            }
        }
        String sb2 = sb.toString();
        Intrinsics.a((Object) sb2, "sb.toString()");
        return Indent.a(sb2, ' ', ';');
    }
}
