package e.a.a.m.a;

import e.a.a.g.b.b.a.IAuthPrefs;
import e.c.b.j.IApiConfigRepository0;
import n.n.c.Intrinsics;

/* compiled from: GlideHelper.kt */
public final class GlideHelper {
    public final IApiConfigRepository0 a;
    public final IAuthPrefs b;

    public GlideHelper(IApiConfigRepository0 iApiConfigRepository0, IAuthPrefs iAuthPrefs) {
        if (iApiConfigRepository0 == null) {
            Intrinsics.a("apiConfigRepository");
            throw null;
        } else if (iAuthPrefs != null) {
            this.a = iApiConfigRepository0;
            this.b = iAuthPrefs;
        } else {
            Intrinsics.a("authPrefs");
            throw null;
        }
    }
}
