package e.a.a.g.a.h;

import e.a.a.g.a.IAuthenticateRequestChecker;
import e.a.a.g.a.Session;
import j.a.a.a.outline;
import java.util.LinkedHashMap;
import java.util.Map;
import n.n.c.Intrinsics;
import n.r.Indent;
import o.Headers;
import o.HttpUrl;
import o.Interceptor;
import o.Request;
import o.RequestBody;
import o.Response;
import o.m0.Util;

/* compiled from: EpguAddCookiesInterceptor.kt */
public final class EpguAddCookiesInterceptor implements Interceptor {
    public final Session b;
    public final IAuthenticateRequestChecker c;

    public EpguAddCookiesInterceptor(Session session, IAuthenticateRequestChecker iAuthenticateRequestChecker) {
        if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (iAuthenticateRequestChecker != null) {
            this.b = session;
            this.c = iAuthenticateRequestChecker;
        } else {
            Intrinsics.a("authenticateRequestChecker");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Response a(Interceptor.a aVar) {
        LinkedHashMap linkedHashMap;
        if (aVar == null) {
            Intrinsics.a("chain");
            throw null;
        } else if (!this.c.a(aVar.f().b)) {
            return aVar.a(aVar.f());
        } else {
            Request f2 = aVar.f();
            if (f2 == null) {
                throw null;
            } else if (f2 != null) {
                new LinkedHashMap();
                HttpUrl httpUrl = f2.b;
                String str = f2.c;
                RequestBody requestBody = f2.f2896e;
                if (f2.f2897f.isEmpty()) {
                    linkedHashMap = new LinkedHashMap();
                } else {
                    Map<Class<?>, Object> map = f2.f2897f;
                    if (map != null) {
                        linkedHashMap = new LinkedHashMap(map);
                    } else {
                        Intrinsics.a("$this$toMutableMap");
                        throw null;
                    }
                }
                Headers.a c2 = f2.d.c();
                StringBuilder sb = new StringBuilder();
                boolean z = true;
                if (!Indent.b(this.b.d)) {
                    StringBuilder a = outline.a("sp_session=");
                    a.append(this.b.d);
                    a.append("; ");
                    sb.append(a.toString());
                }
                if (!Indent.b(this.b.a)) {
                    StringBuilder a2 = outline.a("acc_t=");
                    a2.append(this.b.a);
                    a2.append("; ");
                    sb.append(a2.toString());
                }
                if (this.b.c.size() > 0) {
                    for (Map.Entry next : this.b.c.entrySet()) {
                        String str2 = (String) next.getKey();
                        String str3 = (String) next.getValue();
                        if (str2.length() > 0) {
                            if (str3.length() > 0) {
                                sb.append(str2 + '=' + str3 + "; ");
                            }
                        }
                    }
                }
                String a3 = aVar.f().a("Cookie");
                if (a3 != null) {
                    sb.append(a3);
                }
                String sb2 = sb.toString();
                Intrinsics.a((Object) sb2, "sb.toString()");
                if (sb2.length() <= 0) {
                    z = false;
                }
                if (z) {
                    String a4 = Indent.a(sb2, ' ', ';');
                    if (a4 != null) {
                        c2.c("Cookie", a4);
                    } else {
                        Intrinsics.a("value");
                        throw null;
                    }
                }
                c2.a("mobVersion", "android_1.0.1");
                if (httpUrl != null) {
                    return aVar.a(new Request(httpUrl, str, c2.a(), requestBody, Util.a(linkedHashMap)));
                }
                throw new IllegalStateException("url == null".toString());
            } else {
                Intrinsics.a("request");
                throw null;
            }
        }
    }
}
