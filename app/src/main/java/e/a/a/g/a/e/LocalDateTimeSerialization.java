package e.a.a.g.a.e;

import com.google.gson.JsonParseException;
import j.a.a.a.outline;
import j.c.d.JsonDeserializationContext;
import j.c.d.JsonDeserializer;
import j.c.d.JsonElement;
import java.lang.reflect.Type;
import java.util.Iterator;
import java.util.List;
import n.i.Collections;
import n.n.c.Intrinsics;
import org.threeten.bp.format.DateTimeParseException;
import q.b.a.LocalDateTime;
import q.b.a.e;
import q.b.a.t.DateTimeFormatter;
import q.b.a.t.b;
import t.a.Timber;

/* compiled from: LocalDateTimeSerialization.kt */
public final class LocalDateTimeSerialization implements JsonDeserializer<e> {
    public final List<b> a = Collections.a((Object[]) new DateTimeFormatter[]{DateTimeFormatter.a("yyyy-MM-dd'T'HH:mm:ss.SSSZ"), DateTimeFormatter.f3129l});

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [q.b.a.LocalDateTime, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Object a(JsonElement jsonElement, Type type, JsonDeserializationContext jsonDeserializationContext) {
        String str;
        DateTimeFormatter next;
        Iterator<b> it = this.a.iterator();
        while (true) {
            str = null;
            if (it.hasNext()) {
                next = it.next();
                if (jsonElement == null) {
                    break;
                }
                try {
                    str = jsonElement.d();
                    break;
                } catch (DateTimeParseException unused) {
                }
            } else {
                StringBuilder a2 = outline.a("Unable to parse date ");
                a2.append(jsonElement != null ? jsonElement.d() : null);
                Timber.d.a(a2.toString(), new Object[0]);
                StringBuilder a3 = outline.a("Unable to parse date ");
                if (jsonElement != null) {
                    str = jsonElement.d();
                }
                a3.append(str);
                throw new JsonParseException(a3.toString());
            }
        }
        LocalDateTime a4 = LocalDateTime.a(str, next);
        Intrinsics.a((Object) a4, "LocalDateTime.parse(json?.asString, pattern)");
        return a4;
    }
}
