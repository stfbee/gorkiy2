package e.a.a.g.a.g;

import e.c.b.j.b;
import java.util.Map;
import n.Tuples;
import n.i.Collections;

/* compiled from: IApiConfig.kt */
public abstract class IApiConfig {
    public final Map<b, String> a() {
        IApiConfig1 iApiConfig1 = IApiConfig1.a;
        return Collections.a(new Tuples(IApiConfig4.a, d()), new Tuples(IApiConfig2.a, c()), new Tuples(IApiConfig3.a, b()), new Tuples(IApiConfig5.a, e()), new Tuples(iApiConfig1, e() + "authenticate/"));
    }

    public abstract String b();

    public abstract String c();

    public abstract String d();

    public abstract String e();

    public abstract long f();
}
