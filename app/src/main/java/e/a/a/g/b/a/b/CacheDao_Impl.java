package e.a.a.g.b.a.b;

import i.s.EntityDeletionOrUpdateAdapter;
import i.s.EntityInsertionAdapter;
import i.s.RoomDatabase;
import i.s.SharedSQLiteStatement;
import i.v.a.SupportSQLiteDatabase;
import i.v.a.e.FrameworkSQLiteDatabase0;
import i.v.a.e.FrameworkSQLiteStatement;

public final class CacheDao_Impl implements CacheDao {
    public final RoomDatabase a;
    public final SharedSQLiteStatement b;

    public class a extends EntityInsertionAdapter<Object> {
        public a(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "INSERT OR ABORT INTO `cacheMap` (`response`,`timeS`,`name`,`owner`) VALUES (?,?,?,?)";
        }
    }

    public class b extends EntityInsertionAdapter<Object> {
        public b(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "INSERT OR REPLACE INTO `cacheMap` (`response`,`timeS`,`name`,`owner`) VALUES (?,?,?,?)";
        }
    }

    public class c extends EntityDeletionOrUpdateAdapter<Object> {
        public c(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "DELETE FROM `cacheMap` WHERE `name` = ? AND `owner` = ?";
        }
    }

    public class d extends EntityDeletionOrUpdateAdapter<Object> {
        public d(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "UPDATE OR ABORT `cacheMap` SET `response` = ?,`timeS` = ?,`name` = ?,`owner` = ? WHERE `name` = ? AND `owner` = ?";
        }
    }

    public class e extends SharedSQLiteStatement {
        public e(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "DELETE FROM cacheMap";
        }
    }

    public class f extends SharedSQLiteStatement {
        public f(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "DELETE FROM cacheMap WHERE name = ?";
        }
    }

    public class g extends SharedSQLiteStatement {
        public g(CacheDao_Impl cacheDao_Impl, RoomDatabase roomDatabase) {
            super(roomDatabase);
        }

        public String b() {
            return "DELETE FROM cacheMap WHERE owner = ?";
        }
    }

    public CacheDao_Impl(RoomDatabase roomDatabase) {
        this.a = roomDatabase;
        new a(this, roomDatabase);
        new b(this, roomDatabase);
        new c(this, roomDatabase);
        new d(this, roomDatabase);
        this.b = new e(this, roomDatabase);
        new f(this, roomDatabase);
        new g(this, roomDatabase);
    }

    public void a() {
        FrameworkSQLiteStatement frameworkSQLiteStatement;
        this.a.c();
        SharedSQLiteStatement sharedSQLiteStatement = this.b;
        sharedSQLiteStatement.b.b();
        if (sharedSQLiteStatement.a.compareAndSet(false, true)) {
            if (sharedSQLiteStatement.c == null) {
                sharedSQLiteStatement.c = sharedSQLiteStatement.a();
            }
            frameworkSQLiteStatement = sharedSQLiteStatement.c;
        } else {
            frameworkSQLiteStatement = sharedSQLiteStatement.a();
        }
        RoomDatabase roomDatabase = this.a;
        roomDatabase.b();
        SupportSQLiteDatabase a2 = roomDatabase.c.a();
        roomDatabase.d.b(a2);
        ((FrameworkSQLiteDatabase0) a2).b.beginTransaction();
        try {
            frameworkSQLiteStatement.c.executeUpdateDelete();
            ((FrameworkSQLiteDatabase0) this.a.c.a()).b.setTransactionSuccessful();
        } finally {
            this.a.d();
            SharedSQLiteStatement sharedSQLiteStatement2 = this.b;
            if (frameworkSQLiteStatement == sharedSQLiteStatement2.c) {
                sharedSQLiteStatement2.a.set(false);
            }
        }
    }
}
