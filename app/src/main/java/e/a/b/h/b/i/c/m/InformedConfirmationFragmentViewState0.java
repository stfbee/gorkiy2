package e.a.b.h.b.i.c.m;

import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;

/* compiled from: InformedConfirmationFragmentViewState.kt */
public abstract class InformedConfirmationFragmentViewState0 extends BaseViewState0 {

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class a extends InformedConfirmationFragmentViewState0 {
        public final boolean a;

        public a(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && this.a == ((a) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("AllInfoIsCorrectChangedEvent(isCorrect="), this.a, ")");
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class b extends InformedConfirmationFragmentViewState0 {
        public final boolean a;

        public b(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && this.a == ((b) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("QuarantineTerminationResponsibilityInformedChangedEvent(isInformed="), this.a, ")");
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class c extends InformedConfirmationFragmentViewState0 {
        public final boolean a;

        public c(boolean z) {
            super(null);
            this.a = z;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && this.a == ((c) obj).a;
            }
            return true;
        }

        public int hashCode() {
            boolean z = this.a;
            if (z) {
                return 1;
            }
            return z ? 1 : 0;
        }

        public String toString() {
            return outline.a(outline.a("SelfIsolationInformedChangedEvent(isInformed="), this.a, ")");
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class d extends InformedConfirmationFragmentViewState0 {
        public static final d a = new d();

        public d() {
            super(null);
        }
    }

    /* compiled from: InformedConfirmationFragmentViewState.kt */
    public static final class e extends InformedConfirmationFragmentViewState0 {
        public static final e a = new e();

        public e() {
            super(null);
        }
    }

    public /* synthetic */ InformedConfirmationFragmentViewState0(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
