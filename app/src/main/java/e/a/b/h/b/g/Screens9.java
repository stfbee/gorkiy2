package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.profileFilling.ProfileFillingContainerFragment;

/* compiled from: Screens.kt */
public final class Screens9 extends BaseScreens2 {
    public static final Screens9 d = new Screens9();

    public Screens9() {
        super(null, 1);
    }

    public Fragment a() {
        return new ProfileFillingContainerFragment();
    }
}
