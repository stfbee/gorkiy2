package e.a.b.h.b.i.c;

import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState1;
import l.b.SingleSource;
import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: FillingProfileFullNamePageFragmentVm.kt */
public final class FillingProfileFullNamePageFragmentVm0<T, R> implements Function<T, SingleSource<? extends R>> {
    public final /* synthetic */ FillingProfileFullNamePageFragmentVm a;

    public FillingProfileFullNamePageFragmentVm0(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
        this.a = fillingProfileFullNamePageFragmentVm;
    }

    public Object a(Object obj) {
        if (((FillingProfileFullNamePageFragmentViewState1.d) obj) != null) {
            return this.a.f705p.b();
        }
        Intrinsics.a("it");
        throw null;
    }
}
