package e.a.b.h.b.i.c;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.a;
import e.a.b.h.b.i.c.FillingProfileFullNamePageFragmentViewState1;
import e.c.c.b;
import l.b.t.Consumer;
import ru.covid19.core.data.network.model.CitizenshipResponse0;

/* compiled from: FillingProfileFullNamePageFragmentVm.kt */
public final class FillingProfileFullNamePageFragmentVm3<T> implements Consumer<a> {
    public final /* synthetic */ FillingProfileFullNamePageFragmentVm b;

    public FillingProfileFullNamePageFragmentVm3(FillingProfileFullNamePageFragmentVm fillingProfileFullNamePageFragmentVm) {
        this.b = fillingProfileFullNamePageFragmentVm;
    }

    public void a(Object obj) {
        BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto = (BottomSheetSelectorNavigationDto) obj;
        if (bottomSheetSelectorNavigationDto instanceof FillingProfileFullNamePageFragmentViewState0) {
            this.b.g.a((b) new FillingProfileFullNamePageFragmentViewState1.c(((FillingProfileFullNamePageFragmentViewState0) bottomSheetSelectorNavigationDto).b));
        } else if (bottomSheetSelectorNavigationDto instanceof CitizenshipResponse0) {
            this.b.g.a((b) new FillingProfileFullNamePageFragmentViewState1.b(((CitizenshipResponse0) bottomSheetSelectorNavigationDto).getCitizenship()));
        }
    }
}
