package e.a.b.h.b.h.m;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.a;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import e.c.c.b;
import l.b.t.Consumer;
import ru.covid19.core.data.network.model.CountriesResponse1;

/* compiled from: DocumentsStepFragmentVm.kt */
public final class DocumentsStepFragmentVm4<T> implements Consumer<a> {
    public final /* synthetic */ DocumentsStepFragmentVm b;

    public DocumentsStepFragmentVm4(DocumentsStepFragmentVm documentsStepFragmentVm) {
        this.b = documentsStepFragmentVm;
    }

    public void a(Object obj) {
        BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto = (BottomSheetSelectorNavigationDto) obj;
        if (bottomSheetSelectorNavigationDto instanceof DocumentsStepFragmentViewState) {
            this.b.g.a((b) new DocumentsStepFragmentViewState1.b(((DocumentsStepFragmentViewState) bottomSheetSelectorNavigationDto).b));
        } else if (bottomSheetSelectorNavigationDto instanceof CountriesResponse1) {
            this.b.g.a((b) new DocumentsStepFragmentViewState1.a(((CountriesResponse1) bottomSheetSelectorNavigationDto).getCountry()));
        }
    }
}
