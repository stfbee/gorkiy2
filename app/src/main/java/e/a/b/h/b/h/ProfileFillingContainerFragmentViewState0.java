package e.a.b.h.b.h;

import android.os.Parcel;
import android.os.Parcelable;
import j.a.a.a.outline;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingContainerFragmentViewState.kt */
public final class ProfileFillingContainerFragmentViewState0 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public final int b;
    public final boolean c;
    public final boolean d;

    /* renamed from: e  reason: collision with root package name */
    public final String f683e;

    /* renamed from: f  reason: collision with root package name */
    public final boolean f684f;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new ProfileFillingContainerFragmentViewState0(parcel.readInt(), parcel.readInt() != 0, parcel.readInt() != 0, parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new ProfileFillingContainerFragmentViewState0[i2];
        }
    }

    public ProfileFillingContainerFragmentViewState0() {
        this(0, false, false, null, false, 31);
    }

    public ProfileFillingContainerFragmentViewState0(int i2, boolean z, boolean z2, String str, boolean z3) {
        this.b = i2;
        this.c = z;
        this.d = z2;
        this.f683e = str;
        this.f684f = z3;
    }

    public static /* synthetic */ ProfileFillingContainerFragmentViewState0 a(ProfileFillingContainerFragmentViewState0 profileFillingContainerFragmentViewState0, int i2, boolean z, boolean z2, String str, boolean z3, int i3) {
        if ((i3 & 1) != 0) {
            i2 = profileFillingContainerFragmentViewState0.b;
        }
        int i4 = i2;
        if ((i3 & 2) != 0) {
            z = profileFillingContainerFragmentViewState0.c;
        }
        boolean z4 = z;
        if ((i3 & 4) != 0) {
            z2 = profileFillingContainerFragmentViewState0.d;
        }
        boolean z5 = z2;
        if ((i3 & 8) != 0) {
            str = profileFillingContainerFragmentViewState0.f683e;
        }
        String str2 = str;
        if ((i3 & 16) != 0) {
            z3 = profileFillingContainerFragmentViewState0.f684f;
        }
        boolean z6 = z3;
        if (profileFillingContainerFragmentViewState0 != null) {
            return new ProfileFillingContainerFragmentViewState0(i4, z4, z5, str2, z6);
        }
        throw null;
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof ProfileFillingContainerFragmentViewState0)) {
            return false;
        }
        ProfileFillingContainerFragmentViewState0 profileFillingContainerFragmentViewState0 = (ProfileFillingContainerFragmentViewState0) obj;
        return this.b == profileFillingContainerFragmentViewState0.b && this.c == profileFillingContainerFragmentViewState0.c && this.d == profileFillingContainerFragmentViewState0.d && Intrinsics.a(this.f683e, profileFillingContainerFragmentViewState0.f683e) && this.f684f == profileFillingContainerFragmentViewState0.f684f;
    }

    public int hashCode() {
        int i2 = this.b * 31;
        boolean z = this.c;
        boolean z2 = true;
        if (z) {
            z = true;
        }
        int i3 = (i2 + (z ? 1 : 0)) * 31;
        boolean z3 = this.d;
        if (z3) {
            z3 = true;
        }
        int i4 = (i3 + (z3 ? 1 : 0)) * 31;
        String str = this.f683e;
        int hashCode = (i4 + (str != null ? str.hashCode() : 0)) * 31;
        boolean z4 = this.f684f;
        if (!z4) {
            z2 = z4;
        }
        return hashCode + (z2 ? 1 : 0);
    }

    public String toString() {
        StringBuilder a2 = outline.a("ProfileFillingContainerFragmentViewState(currentStep=");
        a2.append(this.b);
        a2.append(", isFirstStep=");
        a2.append(this.c);
        a2.append(", isLastStep=");
        a2.append(this.d);
        a2.append(", stepTitle=");
        a2.append(this.f683e);
        a2.append(", isLoading=");
        return outline.a(a2, this.f684f, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeInt(this.b);
            parcel.writeInt(this.c ? 1 : 0);
            parcel.writeInt(this.d ? 1 : 0);
            parcel.writeString(this.f683e);
            parcel.writeInt(this.f684f ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }

    /* JADX INFO: this call moved to the top of the method (can break code semantics) */
    public /* synthetic */ ProfileFillingContainerFragmentViewState0(int i2, boolean z, boolean z2, String str, boolean z3, int i3) {
        this((i3 & 1) != 0 ? 0 : i2, (i3 & 2) != 0 ? true : z, (i3 & 4) != 0 ? false : z2, (i3 & 8) != 0 ? null : str, (i3 & 16) != 0 ? false : z3);
    }
}
