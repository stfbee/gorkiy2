package e.a.b.h.d;

import e.a.a.a.e.o.c;
import k.a.Factory;
import m.a.Provider;

public final class SplashActivityVm_Factory implements Factory<a> {
    public final Provider<c> a;

    public SplashActivityVm_Factory(Provider<c> provider) {
        this.a = provider;
    }

    public Object get() {
        return new SplashActivityVm(this.a.get());
    }
}
