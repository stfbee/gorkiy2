package e.a.b.h.b.h.m;

import android.os.Parcel;
import android.os.Parcelable;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import j.a.a.a.outline;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.Document0;

/* compiled from: DocumentsStepFragmentViewState.kt */
public final class DocumentsStepFragmentViewState implements BottomSheetSelectorNavigationDto {
    public static final Parcelable.Creator CREATOR = new a();
    public final Document0 b;
    public final String c;
    public final boolean d;

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel != null) {
                return new DocumentsStepFragmentViewState((Document0) Enum.valueOf(Document0.class, parcel.readString()), parcel.readString(), parcel.readInt() != 0);
            }
            Intrinsics.a("in");
            throw null;
        }

        public final Object[] newArray(int i2) {
            return new DocumentsStepFragmentViewState[i2];
        }
    }

    public DocumentsStepFragmentViewState(Document0 document0, String str, boolean z) {
        if (document0 == null) {
            Intrinsics.a("documentType");
            throw null;
        } else if (str != null) {
            this.b = document0;
            this.c = str;
            this.d = z;
        } else {
            Intrinsics.a("label");
            throw null;
        }
    }

    public int describeContents() {
        return 0;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof DocumentsStepFragmentViewState)) {
            return false;
        }
        DocumentsStepFragmentViewState documentsStepFragmentViewState = (DocumentsStepFragmentViewState) obj;
        return Intrinsics.a(this.b, documentsStepFragmentViewState.b) && Intrinsics.a(this.c, documentsStepFragmentViewState.c) && this.d == documentsStepFragmentViewState.d;
    }

    public String getLabel() {
        return this.c;
    }

    public int hashCode() {
        Document0 document0 = this.b;
        int i2 = 0;
        int hashCode = (document0 != null ? document0.hashCode() : 0) * 31;
        String str = this.c;
        if (str != null) {
            i2 = str.hashCode();
        }
        int i3 = (hashCode + i2) * 31;
        boolean z = this.d;
        if (z) {
            z = true;
        }
        return i3 + (z ? 1 : 0);
    }

    public boolean isSelected() {
        return this.d;
    }

    public String toString() {
        StringBuilder a2 = outline.a("DocumentTypeSelectorItem(documentType=");
        a2.append(this.b);
        a2.append(", label=");
        a2.append(this.c);
        a2.append(", isSelected=");
        return outline.a(a2, this.d, ")");
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeString(this.b.name());
            parcel.writeString(this.c);
            parcel.writeInt(this.d ? 1 : 0);
            return;
        }
        Intrinsics.a("parcel");
        throw null;
    }
}
