package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.b.h.b.e.QuarantineTerminationResponsibilityInfoDialog;

/* compiled from: Screens.kt */
public final class Screens12 extends BaseScreens2 {
    public static final Screens12 d = new Screens12();

    public Screens12() {
        super(null, 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [e.a.b.h.b.e.QuarantineTerminationResponsibilityInfoDialog, androidx.fragment.app.Fragment] */
    public Fragment a() {
        return new QuarantineTerminationResponsibilityInfoDialog();
    }
}
