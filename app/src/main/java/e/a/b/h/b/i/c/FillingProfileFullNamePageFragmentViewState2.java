package e.a.b.h.b.i.c;

import android.net.Uri;
import e.c.c.BaseViewState1;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
public abstract class FillingProfileFullNamePageFragmentViewState2 extends BaseViewState1 {

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class a extends FillingProfileFullNamePageFragmentViewState2 {
        public static final a a = new a();

        public a() {
            super(null);
        }
    }

    /* compiled from: FillingProfileFullNamePageFragmentViewState.kt */
    public static final class b extends FillingProfileFullNamePageFragmentViewState2 {
        public final Uri a;

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(null, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            return 0;
        }

        public String toString() {
            return "PhotoAddedEventResult(photoUri=null)";
        }
    }

    public /* synthetic */ FillingProfileFullNamePageFragmentViewState2(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
