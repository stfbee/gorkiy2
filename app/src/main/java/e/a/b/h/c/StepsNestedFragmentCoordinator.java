package e.a.b.h.c;

import e.a.a.a.e.Commands5;
import e.a.a.a.e.d;
import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.a.a.e.s.NestedFragmentRouter;
import e.a.b.h.b.h.ProfileFillingContainerFragmentVm;
import e.b.a.CommandBuffer;
import e.b.a.Navigator;
import e.b.a.Screen;
import e.b.a.h.Command;
import n.Tuples;
import n.n.c.Intrinsics;

/* compiled from: StepsNestedFragmentCoordinator.kt */
public abstract class StepsNestedFragmentCoordinator implements IStepsNestedFragmentCoordinator {
    public final NestedFragmentCiceroneHolder a;
    public int b;
    public final IStepScreensConfig c;

    public StepsNestedFragmentCoordinator(NestedFragmentCiceroneHolder nestedFragmentCiceroneHolder, IStepScreensConfig iStepScreensConfig) {
        if (nestedFragmentCiceroneHolder == null) {
            Intrinsics.a("nestedFragmentCiceroneHolder");
            throw null;
        } else if (iStepScreensConfig != null) {
            this.a = nestedFragmentCiceroneHolder;
            this.c = iStepScreensConfig;
            this.b = -1;
        } else {
            Intrinsics.a("screensConfig");
            throw null;
        }
    }

    public void a(int i2) {
        b(i2);
    }

    public boolean b() {
        if (this.b == 0) {
            return false;
        }
        b(this.b - 1);
        return true;
    }

    public boolean a() {
        if (this.b == this.c.c()) {
            return false;
        }
        b(this.b + 1);
        return true;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void b(int i2) {
        if (this.b != i2) {
            Tuples<d, String> a2 = this.c.a(i2);
            this.b = i2;
            NestedFragmentCiceroneHolder nestedFragmentCiceroneHolder = this.a;
            String name = ProfileFillingContainerFragmentVm.class.getName();
            Intrinsics.a((Object) name, "ProfileFillingContainerFragmentVm::class.java.name");
            NestedFragmentRouter nestedFragmentRouter = (NestedFragmentRouter) nestedFragmentCiceroneHolder.a(name);
            Screen screen = (Screen) a2.b;
            IStepsNestedFragmentCoordinator0 iStepsNestedFragmentCoordinator0 = new IStepsNestedFragmentCoordinator0((String) a2.c, i2, i2 == 0, i2 == this.c.c());
            if (nestedFragmentRouter == null) {
                throw null;
            } else if (screen != null) {
                Command[] commandArr = {new Commands5(i2, screen, iStepsNestedFragmentCoordinator0)};
                CommandBuffer commandBuffer = nestedFragmentRouter.a;
                Navigator navigator = commandBuffer.a;
                if (navigator != null) {
                    navigator.a(commandArr);
                } else {
                    commandBuffer.b.add(commandArr);
                }
            } else {
                Intrinsics.a("screen");
                throw null;
            }
        }
    }
}
