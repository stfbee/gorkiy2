package e.a.b.h.b.h.n;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.m.a.GlideHelper;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState3;
import e.a.b.h.b.h.n.ProfileFillingResultFragmentViewState4;
import e.a.b.h.b.h.n.d;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState1;
import e.c.c.BaseViewState4;
import e.c.c.k;
import e.c.c.p;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.j;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: ProfileFillingResultFragmentVm.kt */
public final class ProfileFillingResultFragmentVm extends BaseDpFragmentVm1<f> {

    /* renamed from: j  reason: collision with root package name */
    public final IProfileRepository f696j;

    /* renamed from: k  reason: collision with root package name */
    public final GlideHelper f697k;

    /* renamed from: l  reason: collision with root package name */
    public final IAuthPrefs f698l;

    /* renamed from: m  reason: collision with root package name */
    public final IMainCoordinator f699m;

    /* compiled from: ProfileFillingResultFragmentVm.kt */
    public static final class a extends j implements Functions0<d.b, g> {
        public final /* synthetic */ ProfileFillingResultFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ProfileFillingResultFragmentVm profileFillingResultFragmentVm) {
            super(1);
            this.c = profileFillingResultFragmentVm;
        }

        public Object a(Object obj) {
            if (((ProfileFillingResultFragmentViewState3.b) obj) != null) {
                this.c.f699m.h();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingResultFragmentVm.kt */
    public static final class b extends j implements Functions0<d.a, g> {
        public final /* synthetic */ ProfileFillingResultFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ProfileFillingResultFragmentVm profileFillingResultFragmentVm) {
            super(1);
            this.c = profileFillingResultFragmentVm;
        }

        public Object a(Object obj) {
            if (((ProfileFillingResultFragmentViewState3.a) obj) != null) {
                this.c.f699m.j();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingResultFragmentVm.kt */
    public static final class c<T, R> implements Function<T, R> {
        public final /* synthetic */ ProfileFillingResultFragmentVm a;

        public c(ProfileFillingResultFragmentVm profileFillingResultFragmentVm) {
            this.a = profileFillingResultFragmentVm;
        }

        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                return new BaseMviVm0.a(new ProfileFillingResultFragmentViewState4.a(this.a.f696j.f()));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileFillingResultFragmentVm(IProfileRepository iProfileRepository, GlideHelper glideHelper, IAuthPrefs iAuthPrefs, IMainCoordinator iMainCoordinator) {
        super(iMainCoordinator);
        if (iProfileRepository == null) {
            Intrinsics.a("profileRepository");
            throw null;
        } else if (glideHelper == null) {
            Intrinsics.a("glideHelper");
            throw null;
        } else if (iAuthPrefs == null) {
            Intrinsics.a("authPrefs");
            throw null;
        } else if (iMainCoordinator != null) {
            this.f696j = iProfileRepository;
            this.f697k = glideHelper;
            this.f698l = iAuthPrefs;
            this.f699m = iMainCoordinator;
        } else {
            Intrinsics.a("mainNavigator");
            throw null;
        }
    }

    public Object a(Object obj, BaseViewState1 baseViewState1) {
        ProfileFillingResultFragmentViewState0 profileFillingResultFragmentViewState0 = (ProfileFillingResultFragmentViewState0) obj;
        if (profileFillingResultFragmentViewState0 == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof ProfileFillingResultFragmentViewState4.a) {
            ProfileData profileData = ((ProfileFillingResultFragmentViewState4.a) baseViewState1).a;
            if (profileData != null) {
                return new ProfileFillingResultFragmentViewState0(profileData);
            }
            Intrinsics.a("profile");
            throw null;
        } else {
            super.a(profileFillingResultFragmentViewState0, baseViewState1);
            return profileFillingResultFragmentViewState0;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<R>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> b(Observable<p> observable) {
        if (observable != null) {
            Observable<R> c2 = observable.c((Function) new c(this));
            Intrinsics.a((Object) c2, "observable.map {\n       …mpProfileData))\n        }");
            return c2;
        }
        Intrinsics.a("observable");
        throw null;
    }

    public Object d() {
        return new ProfileFillingResultFragmentViewState0(null, 1);
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(ProfileFillingResultFragmentViewState3.b.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(ProfileFillingResultFragmentViewState3.a.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a4 = Observable.a(super.a(observable), BaseMviVm.a(a2, new a(this)), BaseMviVm.a(a3, new b(this)));
            Intrinsics.a((Object) a4, "Observable.mergeArray(\n …)\n            }\n        )");
            return a4;
        }
        Intrinsics.a("o");
        throw null;
    }
}
