package e.a.b.h.b.g;

import android.os.Parcel;
import android.os.Parcelable;
import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import n.n.c.Intrinsics;
import ru.covid19.droid.presentation.main.auth.AuthFragment;

/* compiled from: Screens.kt */
public final class Screens extends BaseScreens2 implements Parcelable {
    public static final Parcelable.Creator CREATOR = new a();
    public static final Screens d = new Screens();

    public static class a implements Parcelable.Creator {
        public final Object createFromParcel(Parcel parcel) {
            if (parcel == null) {
                Intrinsics.a("in");
                throw null;
            } else if (parcel.readInt() != 0) {
                return Screens.d;
            } else {
                return null;
            }
        }

        public final Object[] newArray(int i2) {
            return new Screens[i2];
        }
    }

    public Screens() {
        super(null, 1);
    }

    public Fragment a() {
        return new AuthFragment();
    }

    public int describeContents() {
        return 0;
    }

    public void writeToParcel(Parcel parcel, int i2) {
        if (parcel != null) {
            parcel.writeInt(1);
        } else {
            Intrinsics.a("parcel");
            throw null;
        }
    }
}
