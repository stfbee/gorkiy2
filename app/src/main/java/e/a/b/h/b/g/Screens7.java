package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import ru.covid19.droid.presentation.main.profileFilling.addChild.ProfileFillingAddChildFragment;

/* compiled from: Screens.kt */
public final class Screens7 extends BaseScreens2 {
    public static final Screens7 d = new Screens7();

    public Screens7() {
        super(null, 1);
    }

    public Fragment a() {
        return new ProfileFillingAddChildFragment();
    }
}
