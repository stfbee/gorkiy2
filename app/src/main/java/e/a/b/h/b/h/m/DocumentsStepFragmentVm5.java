package e.a.b.h.b.h.m;

import android.net.Uri;
import e.a.b.h.b.h.m.DocumentsStepFragmentViewState1;
import e.c.c.b;
import j.e.b.PublishRelay;
import l.b.t.Consumer;
import n.n.c.Intrinsics;

/* compiled from: DocumentsStepFragmentVm.kt */
public final class DocumentsStepFragmentVm5<T> implements Consumer<Uri> {
    public final /* synthetic */ DocumentsStepFragmentVm b;

    public DocumentsStepFragmentVm5(DocumentsStepFragmentVm documentsStepFragmentVm) {
        this.b = documentsStepFragmentVm;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [android.net.Uri, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(Object obj) {
        Uri uri = (Uri) obj;
        PublishRelay a = this.b.g;
        Intrinsics.a((Object) uri, "photoUri");
        a.a((b) new DocumentsStepFragmentViewState1.i(uri));
    }
}
