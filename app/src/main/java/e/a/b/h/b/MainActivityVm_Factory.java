package e.a.b.h.b;

import e.a.a.a.e.o.c;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class MainActivityVm_Factory implements Factory<b> {
    public final Provider<c> a;
    public final Provider<g> b;

    public MainActivityVm_Factory(Provider<c> provider, Provider<g> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    public Object get() {
        return new MainActivityVm(this.a.get(), this.b.get());
    }
}
