package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.b.h.b.e.SelfIsolationInfoDialog;

/* compiled from: Screens.kt */
public final class Screens14 extends BaseScreens2 {
    public static final Screens14 d = new Screens14();

    public Screens14() {
        super(null, 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [e.a.b.h.b.e.SelfIsolationInfoDialog, androidx.fragment.app.Fragment] */
    public Fragment a() {
        return new SelfIsolationInfoDialog();
    }
}
