package e.a.b.h.b.h;

import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import e.a.b.h.c.b;
import k.a.Factory;
import m.a.Provider;

public final class ProfileFillingContainerFragmentVm_Factory implements Factory<g> {
    public final Provider<g> a;
    public final Provider<b> b;
    public final Provider<a> c;

    public ProfileFillingContainerFragmentVm_Factory(Provider<g> provider, Provider<b> provider2, Provider<a> provider3) {
        this.a = provider;
        this.b = provider2;
        this.c = provider3;
    }

    public Object get() {
        return new ProfileFillingContainerFragmentVm(this.a.get(), this.b.get(), this.c.get());
    }
}
