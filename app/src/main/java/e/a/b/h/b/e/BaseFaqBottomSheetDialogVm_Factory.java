package e.a.b.h.b.e;

import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class BaseFaqBottomSheetDialogVm_Factory implements Factory<c> {
    public final Provider<g> a;

    public BaseFaqBottomSheetDialogVm_Factory(Provider<g> provider) {
        this.a = provider;
    }

    public Object get() {
        return new BaseFaqBottomSheetDialogVm(this.a.get());
    }
}
