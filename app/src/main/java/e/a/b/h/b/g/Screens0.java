package e.a.b.h.b.g;

import androidx.fragment.app.Fragment;
import e.a.a.a.e.BaseScreens2;
import e.a.b.h.b.e.ArrivalFromAbroadBottomDialog;

/* compiled from: Screens.kt */
public final class Screens0 extends BaseScreens2 {
    public static final Screens0 d = new Screens0();

    public Screens0() {
        super(null, 1);
    }

    /* JADX WARN: Type inference failed for: r0v0, types: [androidx.fragment.app.Fragment, e.a.b.h.b.e.ArrivalFromAbroadBottomDialog] */
    public Fragment a() {
        return new ArrivalFromAbroadBottomDialog();
    }
}
