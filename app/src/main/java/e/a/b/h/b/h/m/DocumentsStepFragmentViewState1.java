package e.a.b.h.b.h.m;

import android.net.Uri;
import e.c.c.BaseViewState0;
import j.a.a.a.outline;
import java.util.Date;
import java.util.List;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.CountriesResponse0;
import ru.covid19.droid.data.model.profileData.Document0;

/* compiled from: DocumentsStepFragmentViewState.kt */
public abstract class DocumentsStepFragmentViewState1 extends BaseViewState0 {

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class a extends DocumentsStepFragmentViewState1 {
        public final CountriesResponse0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(CountriesResponse0 countriesResponse0) {
            super(null);
            if (countriesResponse0 != null) {
                this.a = countriesResponse0;
                return;
            }
            Intrinsics.a("country");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof a) && Intrinsics.a(this.a, ((a) obj).a);
            }
            return true;
        }

        public int hashCode() {
            CountriesResponse0 countriesResponse0 = this.a;
            if (countriesResponse0 != null) {
                return countriesResponse0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("CountrySelected(country=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class b extends DocumentsStepFragmentViewState1 {
        public final Document0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(Document0 document0) {
            super(null);
            if (document0 != null) {
                this.a = document0;
                return;
            }
            Intrinsics.a("documentType");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof b) && Intrinsics.a(this.a, ((b) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Document0 document0 = this.a;
            if (document0 != null) {
                return document0.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("DocumentTypeSelected(documentType=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class c extends DocumentsStepFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof c) && Intrinsics.a(this.a, ((c) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnCityAbroadChanged(text="), this.a, ")");
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class d extends DocumentsStepFragmentViewState1 {
        public final Date a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(Date date) {
            super(null);
            if (date != null) {
                this.a = date;
                return;
            }
            Intrinsics.a("date");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof d) && Intrinsics.a(this.a, ((d) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Date date = this.a;
            if (date != null) {
                return date.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("OnEntryDateChanged(date=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class e extends DocumentsStepFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof e) && Intrinsics.a(this.a, ((e) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnNumberChanged(text="), this.a, ")");
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class f extends DocumentsStepFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof f) && Intrinsics.a(this.a, ((f) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnPlaceOfEntryChanged(text="), this.a, ")");
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class g extends DocumentsStepFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("text");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof g) && Intrinsics.a(this.a, ((g) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("OnSeriesChanged(text="), this.a, ")");
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class h extends DocumentsStepFragmentViewState1 {
        public static final h a = new h();

        public h() {
            super(null);
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class i extends DocumentsStepFragmentViewState1 {
        public final Uri a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(Uri uri) {
            super(null);
            if (uri != null) {
                this.a = uri;
                return;
            }
            Intrinsics.a("uri");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof i) && Intrinsics.a(this.a, ((i) obj).a);
            }
            return true;
        }

        public int hashCode() {
            Uri uri = this.a;
            if (uri != null) {
                return uri.hashCode();
            }
            return 0;
        }

        public String toString() {
            StringBuilder a2 = outline.a("PhotoAddedEvent(uri=");
            a2.append(this.a);
            a2.append(")");
            return a2.toString();
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class j extends DocumentsStepFragmentViewState1 {
        public final String a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(String str) {
            super(null);
            if (str != null) {
                this.a = str;
                return;
            }
            Intrinsics.a("title");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this != obj) {
                return (obj instanceof j) && Intrinsics.a(this.a, ((j) obj).a);
            }
            return true;
        }

        public int hashCode() {
            String str = this.a;
            if (str != null) {
                return str.hashCode();
            }
            return 0;
        }

        public String toString() {
            return outline.a(outline.a("SelectCountryEvent(title="), this.a, ")");
        }
    }

    /* compiled from: DocumentsStepFragmentViewState.kt */
    public static final class k extends DocumentsStepFragmentViewState1 {
        public final List<a> a;
        public final String b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public k(List<a> list, String str) {
            super(null);
            if (list == null) {
                Intrinsics.a("items");
                throw null;
            } else if (str != null) {
                this.a = list;
                this.b = str;
            } else {
                Intrinsics.a("title");
                throw null;
            }
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof k)) {
                return false;
            }
            k kVar = (k) obj;
            return Intrinsics.a(this.a, kVar.a) && Intrinsics.a(this.b, kVar.b);
        }

        public int hashCode() {
            List<a> list = this.a;
            int i2 = 0;
            int hashCode = (list != null ? list.hashCode() : 0) * 31;
            String str = this.b;
            if (str != null) {
                i2 = str.hashCode();
            }
            return hashCode + i2;
        }

        public String toString() {
            StringBuilder a2 = outline.a("SelectDocumentTypeEvent(items=");
            a2.append(this.a);
            a2.append(", title=");
            return outline.a(a2, this.b, ")");
        }
    }

    public /* synthetic */ DocumentsStepFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
