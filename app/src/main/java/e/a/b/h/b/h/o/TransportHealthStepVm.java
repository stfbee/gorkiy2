package e.a.b.h.b.h.o;

import e.a.a.a.e.p.BottomSheetSelectorNavigationDto;
import e.a.a.a.e.p.BottomSheetSelectorNavigationDto0;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.BaseStepValidatedFragmentVm;
import e.a.b.h.b.h.o.TransportHealthStepViewState4;
import e.a.b.h.b.h.o.e;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState4;
import e.c.c.p;
import java.util.ArrayList;
import java.util.List;
import l.b.Observable;
import l.b.s.Disposable;
import l.b.t.Consumer;
import n.Unit;
import n.i.Collections;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.r.Indent;
import ru.covid19.droid.data.model.profileData.Children;
import ru.covid19.droid.data.model.profileData.Passenger;
import ru.covid19.droid.data.model.profileData.ProfileData;
import ru.covid19.droid.data.model.profileData.Transport;

/* compiled from: TransportHealthStepVm.kt */
public final class TransportHealthStepVm extends BaseStepValidatedFragmentVm<f> {

    /* renamed from: n  reason: collision with root package name */
    public final IMainCoordinator f701n;

    /* renamed from: o  reason: collision with root package name */
    public final IProfileRepository f702o;

    /* compiled from: TransportHealthStepVm.kt */
    public static final class a extends n.n.c.j implements Functions0<e.i, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.i iVar = (TransportHealthStepViewState4.i) obj;
            if (iVar != null) {
                ProfileData f2 = this.c.f702o.f();
                f2.setTransport(f2.getTransport().copy(iVar.a, ""));
                TransportHealthStepVm transportHealthStepVm = this.c;
                transportHealthStepVm.d.a(TransportHealthStepVm.b(transportHealthStepVm));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class b extends n.n.c.j implements Functions0<e.f, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.f fVar = (TransportHealthStepViewState4.f) obj;
            if (fVar != null) {
                this.c.f702o.f().setSympthoms(fVar.a);
                TransportHealthStepVm transportHealthStepVm = this.c;
                transportHealthStepVm.d.a(TransportHealthStepVm.b(transportHealthStepVm));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class c extends n.n.c.j implements Functions0<e.b, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public c(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.b bVar = (TransportHealthStepViewState4.b) obj;
            if (bVar != null) {
                ProfileData f2 = this.c.f702o.f();
                f2.setTransport(Transport.copy$default(f2.getTransport(), null, bVar.a, 1, null));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class d extends n.n.c.j implements Functions0<e.h, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public d(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.h hVar = (TransportHealthStepViewState4.h) obj;
            if (hVar != null) {
                IMainCoordinator iMainCoordinator = this.c.f701n;
                TransportHealthStepViewState0 transportHealthStepViewState0 = hVar.a;
                iMainCoordinator.a(new BottomSheetSelectorNavigationDto0(transportHealthStepViewState0.a, transportHealthStepViewState0.b));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class e extends n.n.c.j implements Functions0<e.a, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public e(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            if (((TransportHealthStepViewState4.a) obj) != null) {
                this.c.f701n.c();
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class f extends n.n.c.j implements Functions0<e.c, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public f(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.c cVar = (TransportHealthStepViewState4.c) obj;
            if (cVar != null) {
                ProfileData f2 = this.c.f702o.f();
                List<Children> children = f2.getChildren();
                Children children2 = cVar.a;
                if (children != null) {
                    ArrayList arrayList = new ArrayList(Collections.a(children, 10));
                    boolean z = false;
                    for (T next : children) {
                        boolean z2 = true;
                        if (!z && Intrinsics.a(next, children2)) {
                            z = true;
                            z2 = false;
                        }
                        if (z2) {
                            arrayList.add(next);
                        }
                    }
                    f2.setChildren(arrayList);
                    TransportHealthStepVm transportHealthStepVm = this.c;
                    transportHealthStepVm.d.a(TransportHealthStepVm.b(transportHealthStepVm));
                    return Unit.a;
                }
                Intrinsics.a("$this$minus");
                throw null;
            }
            Intrinsics.a("event");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class g extends n.n.c.j implements Functions0<e.g, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public g(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.g gVar = (TransportHealthStepViewState4.g) obj;
            if (gVar != null) {
                this.c.f701n.a(new BottomSheetSelectorNavigationDto0(gVar.a, gVar.b));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class h extends n.n.c.j implements Functions0<e.d, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public h(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            TransportHealthStepViewState4.d dVar = (TransportHealthStepViewState4.d) obj;
            if (dVar != null) {
                Passenger passenger = TransportHealthStepVm.b(this.c).a.getPassenger();
                if (dVar.a) {
                    passenger.setQuarantineAddress(passenger.getPlacementAddress());
                } else {
                    passenger.setQuarantineAddress(TransportHealthStepVm.b(this.c).b);
                }
                passenger.setQuarantineAddressMatchesPlacement(Boolean.valueOf(dVar.a));
                TransportHealthStepVm transportHealthStepVm = this.c;
                transportHealthStepVm.d.a(TransportHealthStepVm.b(transportHealthStepVm));
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class i extends n.n.c.j implements Functions0<e.e, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public i(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
         arg types: [java.lang.Boolean, boolean]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean */
        public Object a(Object obj) {
            TransportHealthStepViewState4.e eVar = (TransportHealthStepViewState4.e) obj;
            if (eVar != null) {
                TransportHealthStepVm.b(this.c).a.getPassenger().setQuarantineAddress(eVar.a);
                if (Intrinsics.a((Object) TransportHealthStepVm.b(this.c).a.getPassenger().getQuarantineAddressMatchesPlacement(), (Object) false)) {
                    TransportHealthStepViewState1 b = TransportHealthStepVm.b(this.c);
                    String str = eVar.a;
                    if (str != null) {
                        b.b = str;
                    } else {
                        Intrinsics.a("<set-?>");
                        throw null;
                    }
                }
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class j extends n.n.c.j implements Functions0<p, n.g> {
        public final /* synthetic */ TransportHealthStepVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public j(TransportHealthStepVm transportHealthStepVm) {
            super(1);
            this.c = transportHealthStepVm;
        }

        public Object a(Object obj) {
            if (((BaseViewState4) obj) != null) {
                if (this.c.f702o.f().getTransport().getType() == null) {
                    ProfileData f2 = this.c.f702o.f();
                    f2.setTransport(Transport.copy$default(f2.getTransport(), TransportHealthStepViewState2.plane, null, 2, null));
                }
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: TransportHealthStepVm.kt */
    public static final class k<T> implements Consumer<e.a.a.a.e.p.a> {
        public final /* synthetic */ TransportHealthStepVm b;

        public k(TransportHealthStepVm transportHealthStepVm) {
            this.b = transportHealthStepVm;
        }

        public void a(Object obj) {
            BottomSheetSelectorNavigationDto bottomSheetSelectorNavigationDto = (BottomSheetSelectorNavigationDto) obj;
            if (bottomSheetSelectorNavigationDto instanceof TransportHealthStepViewState3) {
                this.b.g.a((e.c.c.b) new TransportHealthStepViewState4.i(((TransportHealthStepViewState3) bottomSheetSelectorNavigationDto).b));
            } else if (bottomSheetSelectorNavigationDto instanceof TransportHealthStepViewState) {
                this.b.g.a((e.c.c.b) new TransportHealthStepViewState4.f(Boolean.valueOf(((TransportHealthStepViewState) bottomSheetSelectorNavigationDto).b)));
            }
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public TransportHealthStepVm(IMainCoordinator iMainCoordinator, IProfileRepository iProfileRepository) {
        super(iMainCoordinator);
        if (iMainCoordinator == null) {
            Intrinsics.a("mainNavigator");
            throw null;
        } else if (iProfileRepository != null) {
            this.f701n = iMainCoordinator;
            this.f702o = iProfileRepository;
        } else {
            Intrinsics.a("profileRepository");
            throw null;
        }
    }

    public static final /* synthetic */ TransportHealthStepViewState1 b(TransportHealthStepVm transportHealthStepVm) {
        return (TransportHealthStepViewState1) transportHealthStepVm.e();
    }

    public void a(Object obj) {
        TransportHealthStepViewState1 transportHealthStepViewState1 = (TransportHealthStepViewState1) obj;
        if (transportHealthStepViewState1 != null) {
            ProfileData f2 = this.f702o.f();
            if (f2 != null) {
                transportHealthStepViewState1.a = f2;
            } else {
                Intrinsics.a("<set-?>");
                throw null;
            }
        } else {
            Intrinsics.a("vs");
            throw null;
        }
    }

    public Object d() {
        return new TransportHealthStepViewState1(null, null, 3);
    }

    public boolean h() {
        ProfileData f2 = this.f702o.f();
        return f2.getSympthoms() != null && f2.getTransport().getType() != null && !Indent.b(f2.getTransport().getName()) && !Indent.b(f2.getPassenger().getQuarantineAddress());
    }

    public Observable<BaseMviVm0<? extends e.c.c.k>> b(Observable<p> observable) {
        if (observable != null) {
            return BaseMviVm.a(observable, new j(this));
        }
        Intrinsics.a("observable");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public void a(List<? extends Observable<? extends e.c.c.b>> list) {
        if (list != null) {
            super.a(list);
            Disposable a2 = this.f701n.f().a(new k(this));
            Intrinsics.a((Object) a2, "mainNavigator.addBottomS…\n            }\n\n        }");
            j.c.a.a.c.n.c.a(a2, this.f722f);
            return;
        }
        Intrinsics.a("es");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends e.c.c.k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(TransportHealthStepViewState4.i.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(TransportHealthStepViewState4.f.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(TransportHealthStepViewState4.b.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(TransportHealthStepViewState4.h.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(TransportHealthStepViewState4.a.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<U> a7 = observable.a(TransportHealthStepViewState4.c.class);
            Intrinsics.a((Object) a7, "ofType(R::class.java)");
            Observable<U> a8 = observable.a(TransportHealthStepViewState4.g.class);
            Intrinsics.a((Object) a8, "ofType(R::class.java)");
            Observable<U> a9 = observable.a(TransportHealthStepViewState4.d.class);
            Intrinsics.a((Object) a9, "ofType(R::class.java)");
            Observable<U> a10 = observable.a(TransportHealthStepViewState4.e.class);
            Intrinsics.a((Object) a10, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends e.c.c.k>> a11 = Observable.a(super.a(observable), BaseMviVm.a(a2, new a(this)), BaseMviVm.a(a3, new b(this)), BaseMviVm.a(a4, new c(this)), BaseMviVm.a(a5, new d(this)), BaseMviVm.a(a6, new e(this)), BaseMviVm.a(a7, new f(this)), BaseMviVm.a(a8, new g(this)), BaseMviVm.a(a9, new h(this)), BaseMviVm.a(a10, new i(this)));
            Intrinsics.a((Object) a11, "Observable.mergeArray(\n …}\n            }\n        )");
            return a11;
        }
        Intrinsics.a("o");
        throw null;
    }
}
