package e.a.b.h.b.d;

import e.a.a.a.a.BaseAuthFragmentVm8;
import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.g.a.Session;
import e.a.a.g.a.g.IApiConfig;
import e.a.a.g.b.b.a.IAuthPrefs;
import e.a.a.j.a.IAuthRepository0;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import l.b.Completable;
import l.b.Single;
import l.b.SingleObserver;
import l.b.t.Consumer;
import l.b.u.b.ObjectHelper;
import l.b.u.d.ConsumerSingleObserver;
import l.b.u.e.a.CompletableFromSingle;
import n.n.c.Intrinsics;
import ru.covid19.core.data.network.model.PersonalResponse;
import ru.covid19.droid.data.model.profileData.ProfileData;

/* compiled from: AuthFragmentVm.kt */
public final class AuthFragmentVm extends BaseAuthFragmentVm8 {
    public final Session A;
    public final IProfileRepository B;
    public final IMainCoordinator C;

    /* compiled from: AuthFragmentVm.kt */
    public static final class a<T> implements Consumer<ProfileData> {
        public final /* synthetic */ AuthFragmentVm b;

        public a(AuthFragmentVm authFragmentVm) {
            this.b = authFragmentVm;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [ru.covid19.droid.data.model.profileData.ProfileData, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public void a(Object obj) {
            ProfileData profileData = (ProfileData) obj;
            IProfileRepository iProfileRepository = this.b.B;
            Intrinsics.a((Object) profileData, "it");
            iProfileRepository.a(profileData);
            this.b.C.l();
        }
    }

    /* compiled from: AuthFragmentVm.kt */
    public static final class b<T> implements Consumer<Throwable> {
        public final /* synthetic */ AuthFragmentVm b;

        public b(AuthFragmentVm authFragmentVm) {
            this.b = authFragmentVm;
        }

        public void a(Object obj) {
            Throwable th = (Throwable) obj;
            this.b.C.a(true);
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public AuthFragmentVm(IAuthRepository0 iAuthRepository0, IAuthPrefs iAuthPrefs, Session session, IProfileRepository iProfileRepository, IMainCoordinator iMainCoordinator, ITopLevelCoordinator iTopLevelCoordinator, IApiConfig iApiConfig) {
        super(iTopLevelCoordinator, iMainCoordinator, iAuthRepository0, iAuthPrefs, session, iApiConfig);
        if (iAuthRepository0 == null) {
            Intrinsics.a("authRepository");
            throw null;
        } else if (iAuthPrefs == null) {
            Intrinsics.a("authPrefs");
            throw null;
        } else if (session == null) {
            Intrinsics.a("session");
            throw null;
        } else if (iProfileRepository == null) {
            Intrinsics.a("profileRepository");
            throw null;
        } else if (iMainCoordinator == null) {
            Intrinsics.a("mainCoordinator");
            throw null;
        } else if (iTopLevelCoordinator == null) {
            Intrinsics.a("topLevelCoordinator");
            throw null;
        } else if (iApiConfig != null) {
            this.A = session;
            this.B = iProfileRepository;
            this.C = iMainCoordinator;
        } else {
            Intrinsics.a("apiConfig");
            throw null;
        }
    }

    /* JADX WARN: Type inference failed for: r1v2, types: [l.b.u.e.a.CompletableFromSingle, java.lang.Object, l.b.Completable] */
    public Completable d() {
        Single<PersonalResponse> a2 = this.B.a(this.A.b, "(documents.elements,addresses.elements)");
        if (a2 != null) {
            ? completableFromSingle = new CompletableFromSingle(a2);
            Intrinsics.a((Object) completableFromSingle, "profileRepository.getPer…         .ignoreElement()");
            return completableFromSingle;
        }
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.d.AuthFragmentVm$a, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.d.AuthFragmentVm$b, java.lang.String]
     candidates:
      l.b.u.b.ObjectHelper.a(int, java.lang.String):int
      l.b.u.b.ObjectHelper.a(long, long):int
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.Object):boolean
      l.b.u.b.ObjectHelper.a(java.lang.Object, java.lang.String):T */
    public void e() {
        Single<ProfileData> a2 = this.B.a();
        a aVar = new a(this);
        b bVar = new b(this);
        if (a2 != null) {
            ObjectHelper.a((Object) aVar, "onSuccess is null");
            ObjectHelper.a((Object) bVar, "onError is null");
            a2.a((SingleObserver) new ConsumerSingleObserver(aVar, bVar));
            return;
        }
        throw null;
    }
}
