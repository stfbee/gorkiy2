package e.a.b.h.b.i.c.m;

import e.a.b.d.b.a;
import e.a.b.h.b.g.g;
import k.a.Factory;
import m.a.Provider;

public final class InformedConfirmationFragmentVm_Factory implements Factory<c> {
    public final Provider<a> a;
    public final Provider<g> b;

    public InformedConfirmationFragmentVm_Factory(Provider<a> provider, Provider<g> provider2) {
        this.a = provider;
        this.b = provider2;
    }

    public Object get() {
        return new InformedConfirmationFragmentVm(this.a.get(), this.b.get());
    }
}
