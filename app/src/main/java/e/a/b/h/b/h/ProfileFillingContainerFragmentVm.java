package e.a.b.h.b.h;

import e.a.a.a.b.BaseDpFragmentVm1;
import e.a.b.d.b.IProfileRepository;
import e.a.b.h.b.g.IMainCoordinator;
import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState;
import e.a.b.h.b.h.ProfileFillingContainerFragmentViewState1;
import e.a.b.h.b.h.d;
import e.a.b.h.c.IStepsNestedFragmentCoordinator;
import e.a.b.h.c.IStepsNestedFragmentCoordinator0;
import e.c.c.BaseMviVm;
import e.c.c.BaseMviVm0;
import e.c.c.BaseViewState1;
import e.c.c.k;
import java.util.List;
import l.b.Observable;
import l.b.t.Function;
import n.Unit;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;
import n.n.c.Reflection;
import n.n.c.h;
import n.n.c.j;
import n.p.KDeclarationContainer;

/* compiled from: ProfileFillingContainerFragmentVm.kt */
public final class ProfileFillingContainerFragmentVm extends BaseDpFragmentVm1<f> {

    /* renamed from: j  reason: collision with root package name */
    public final IMainCoordinator f685j;

    /* renamed from: k  reason: collision with root package name */
    public final IStepsNestedFragmentCoordinator f686k;

    /* renamed from: l  reason: collision with root package name */
    public final IProfileRepository f687l;

    /* compiled from: ProfileFillingContainerFragmentVm.kt */
    public static final class a extends j implements Functions0<d.d, g> {
        public final /* synthetic */ ProfileFillingContainerFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(ProfileFillingContainerFragmentVm profileFillingContainerFragmentVm) {
            super(1);
            this.c = profileFillingContainerFragmentVm;
        }

        public Object a(Object obj) {
            if (((ProfileFillingContainerFragmentViewState.d) obj) != null) {
                if (!this.c.f686k.b() && !((ProfileFillingContainerFragmentViewState0) this.c.e()).f684f) {
                    this.c.f();
                }
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragmentVm.kt */
    public static final class b extends j implements Functions0<d.b, g> {
        public final /* synthetic */ ProfileFillingContainerFragmentVm c;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(ProfileFillingContainerFragmentVm profileFillingContainerFragmentVm) {
            super(1);
            this.c = profileFillingContainerFragmentVm;
        }

        public Object a(Object obj) {
            if (((ProfileFillingContainerFragmentViewState.b) obj) != null) {
                if (!this.c.f686k.a()) {
                    this.c.g.a((e.c.c.b) ProfileFillingContainerFragmentViewState.c.b);
                }
                return Unit.a;
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragmentVm.kt */
    public static final class c<T, R> implements Function<T, R> {
        public static final c a = new c();

        public Object a(Object obj) {
            ProfileFillingContainerFragmentViewState.e eVar = (ProfileFillingContainerFragmentViewState.e) obj;
            if (eVar != null) {
                return new BaseMviVm0.a(new ProfileFillingContainerFragmentViewState1.b(eVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragmentVm.kt */
    public static final /* synthetic */ class d extends h implements Functions0<Observable<d.c>, Observable<BaseMviVm0<? extends k>>> {
        public d(ProfileFillingContainerFragmentVm profileFillingContainerFragmentVm) {
            super(1, profileFillingContainerFragmentVm);
        }

        /* JADX WARN: Type inference failed for: r1v0, types: [e.a.b.h.b.h.ProfileFillingContainerFragmentVm$d, n.n.c.CallableReference] */
        public Object a(Object obj) {
            Observable observable = (Observable) obj;
            if (observable != null) {
                return ProfileFillingContainerFragmentVm.a((ProfileFillingContainerFragmentVm) this.c, observable);
            }
            Intrinsics.a("p1");
            throw null;
        }

        public final String f() {
            return "postFormProcessor";
        }

        /* JADX WARN: Type inference failed for: r0v1, types: [n.p.KClass, n.p.KDeclarationContainer] */
        public final KDeclarationContainer g() {
            return Reflection.a(ProfileFillingContainerFragmentVm.class);
        }

        public final String i() {
            return "postFormProcessor(Lio/reactivex/Observable;)Lio/reactivex/Observable;";
        }
    }

    /* compiled from: ProfileFillingContainerFragmentVm.kt */
    public static final class e<T, R> implements Function<T, R> {
        public static final e a = new e();

        public Object a(Object obj) {
            ProfileFillingContainerFragmentViewState.a aVar = (ProfileFillingContainerFragmentViewState.a) obj;
            if (aVar != null) {
                return new BaseMviVm0.a(new ProfileFillingContainerFragmentViewState1.a(aVar.a));
            }
            Intrinsics.a("it");
            throw null;
        }
    }

    /* JADX INFO: super call moved to the top of the method (can break code semantics) */
    public ProfileFillingContainerFragmentVm(IMainCoordinator iMainCoordinator, IStepsNestedFragmentCoordinator iStepsNestedFragmentCoordinator, IProfileRepository iProfileRepository) {
        super(iMainCoordinator);
        if (iMainCoordinator == null) {
            Intrinsics.a("fragmentNavigator");
            throw null;
        } else if (iStepsNestedFragmentCoordinator == null) {
            Intrinsics.a("stepsFragmentNavigator");
            throw null;
        } else if (iProfileRepository != null) {
            this.f685j = iMainCoordinator;
            this.f686k = iStepsNestedFragmentCoordinator;
            this.f687l = iProfileRepository;
        } else {
            Intrinsics.a("profileRepository");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS
     arg types: [e.a.b.h.b.h.ProfileFillingContainerFragmentViewState0, e.c.c.BaseViewState1]
     candidates:
      e.a.b.h.b.h.ProfileFillingContainerFragmentVm.a(e.a.b.h.b.h.ProfileFillingContainerFragmentVm, l.b.Observable):l.b.Observable
      e.c.c.BaseMviVm1.a(java.lang.Object, e.c.c.k):VS */
    public Object a(Object obj, BaseViewState1 baseViewState1) {
        ProfileFillingContainerFragmentViewState0 profileFillingContainerFragmentViewState0 = (ProfileFillingContainerFragmentViewState0) obj;
        if (profileFillingContainerFragmentViewState0 == null) {
            Intrinsics.a("vs");
            throw null;
        } else if (baseViewState1 == null) {
            Intrinsics.a("result");
            throw null;
        } else if (baseViewState1 instanceof ProfileFillingContainerFragmentViewState1.b) {
            IStepsNestedFragmentCoordinator0 iStepsNestedFragmentCoordinator0 = ((ProfileFillingContainerFragmentViewState1.b) baseViewState1).a;
            return ProfileFillingContainerFragmentViewState0.a(profileFillingContainerFragmentViewState0, iStepsNestedFragmentCoordinator0.b, iStepsNestedFragmentCoordinator0.c, iStepsNestedFragmentCoordinator0.d, iStepsNestedFragmentCoordinator0.a, false, 16);
        } else if (baseViewState1 instanceof ProfileFillingContainerFragmentViewState1.a) {
            return ProfileFillingContainerFragmentViewState0.a(profileFillingContainerFragmentViewState0, 0, false, false, null, ((ProfileFillingContainerFragmentViewState1.a) baseViewState1).a, 15);
        } else {
            if (baseViewState1 instanceof ProfileFillingContainerFragmentViewState1.c) {
                this.f685j.l();
                return ProfileFillingContainerFragmentViewState0.a(profileFillingContainerFragmentViewState0, 0, false, false, null, false, 31);
            }
            super.a((Object) profileFillingContainerFragmentViewState0, (k) baseViewState1);
            return profileFillingContainerFragmentViewState0;
        }
    }

    public Object d() {
        return new ProfileFillingContainerFragmentViewState0(0, false, false, null, false, 31);
    }

    public void a(List<? extends Observable<? extends e.c.c.b>> list) {
        if (list != null) {
            super.a(list);
            this.f686k.a(((ProfileFillingContainerFragmentViewState0) e()).b);
            return;
        }
        Intrinsics.a("es");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<U>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable<e.c.c.BaseMviVm0<? extends e.c.c.k>>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public Observable<BaseMviVm0<? extends k>> a(Observable<? extends e.c.c.b> observable) {
        if (observable != null) {
            Observable<U> a2 = observable.a(ProfileFillingContainerFragmentViewState.d.class);
            Intrinsics.a((Object) a2, "ofType(R::class.java)");
            Observable<U> a3 = observable.a(ProfileFillingContainerFragmentViewState.b.class);
            Intrinsics.a((Object) a3, "ofType(R::class.java)");
            Observable<U> a4 = observable.a(ProfileFillingContainerFragmentViewState.e.class);
            Intrinsics.a((Object) a4, "ofType(R::class.java)");
            Observable<U> a5 = observable.a(ProfileFillingContainerFragmentViewState.c.class);
            Intrinsics.a((Object) a5, "ofType(R::class.java)");
            Observable<U> a6 = observable.a(ProfileFillingContainerFragmentViewState.a.class);
            Intrinsics.a((Object) a6, "ofType(R::class.java)");
            Observable<BaseMviVm0<? extends k>> a7 = Observable.a(super.a(observable), BaseMviVm.a(a2, new a(this)), BaseMviVm.a(a3, new b(this)), a4.c((Function) c.a), BaseMviVm.b(a5, new d(this)), a6.c((Function) e.a));
            Intrinsics.a((Object) a7, "Observable.mergeArray(\n …t.isLoading)) }\n        )");
            return a7;
        }
        Intrinsics.a("o");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.Observable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public static final /* synthetic */ Observable a(ProfileFillingContainerFragmentVm profileFillingContainerFragmentVm, Observable observable) {
        if (profileFillingContainerFragmentVm != null) {
            Observable a2 = observable.a(new ProfileFillingContainerFragmentVm2(profileFillingContainerFragmentVm));
            Intrinsics.a((Object) a2, "observable.flatMap {\n   …(Lce.Loading())\n        }");
            return a2;
        }
        throw null;
    }
}
