package e.a.b.h.b.h.l;

import e.c.c.BaseViewState1;
import j.a.a.a.outline;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;
import ru.covid19.droid.data.model.profileData.Children;

/* compiled from: ProfileFillingAddChildFragmentViewState.kt */
public abstract class ProfileFillingAddChildFragmentViewState1 extends BaseViewState1 {

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class b extends ProfileFillingAddChildFragmentViewState1 {
        public static final b a = new b();

        public b() {
            super(null);
        }
    }

    public /* synthetic */ ProfileFillingAddChildFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }

    /* compiled from: ProfileFillingAddChildFragmentViewState.kt */
    public static final class a extends ProfileFillingAddChildFragmentViewState1 {
        public final Children a;
        public final boolean b;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public a(Children children, boolean z) {
            super(null);
            if (children != null) {
                this.a = children;
                this.b = z;
                return;
            }
            Intrinsics.a("child");
            throw null;
        }

        public boolean equals(Object obj) {
            if (this == obj) {
                return true;
            }
            if (!(obj instanceof a)) {
                return false;
            }
            a aVar = (a) obj;
            return Intrinsics.a(this.a, aVar.a) && this.b == aVar.b;
        }

        public int hashCode() {
            Children children = this.a;
            int hashCode = (children != null ? children.hashCode() : 0) * 31;
            boolean z = this.b;
            if (z) {
                z = true;
            }
            return hashCode + (z ? 1 : 0);
        }

        public String toString() {
            StringBuilder a2 = outline.a("EditChildResult(child=");
            a2.append(this.a);
            a2.append(", setPredefined=");
            return outline.a(a2, this.b, ")");
        }

        /* JADX INFO: this call moved to the top of the method (can break code semantics) */
        public /* synthetic */ a(Children children, boolean z, int i2) {
            this(children, (i2 & 2) != 0 ? false : z);
        }
    }
}
