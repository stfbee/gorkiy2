package e.a.b.h.b.h.o;

import j.a.a.a.outline;
import java.util.List;
import n.i.Collections2;
import n.n.c.Intrinsics;

/* compiled from: TransportHealthStepViewState.kt */
public final class TransportHealthStepViewState0 {
    public final String a;
    public final List<j> b;

    public TransportHealthStepViewState0(String str, List<j> list) {
        if (str == null) {
            Intrinsics.a("title");
            throw null;
        } else if (list != null) {
            this.a = str;
            this.b = list;
        } else {
            Intrinsics.a("items");
            throw null;
        }
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof TransportHealthStepViewState0)) {
            return false;
        }
        TransportHealthStepViewState0 transportHealthStepViewState0 = (TransportHealthStepViewState0) obj;
        return Intrinsics.a(this.a, transportHealthStepViewState0.a) && Intrinsics.a(this.b, transportHealthStepViewState0.b);
    }

    public int hashCode() {
        String str = this.a;
        int i2 = 0;
        int hashCode = (str != null ? str.hashCode() : 0) * 31;
        List<j> list = this.b;
        if (list != null) {
            i2 = list.hashCode();
        }
        return hashCode + i2;
    }

    public String toString() {
        StringBuilder a2 = outline.a("TransportHealthBottomSheetDto(title=");
        a2.append(this.a);
        a2.append(", items=");
        a2.append(this.b);
        a2.append(")");
        return a2.toString();
    }

    public TransportHealthStepViewState0() {
        this("", Collections2.b);
    }
}
