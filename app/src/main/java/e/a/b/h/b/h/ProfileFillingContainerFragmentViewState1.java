package e.a.b.h.b.h;

import e.a.b.h.c.IStepsNestedFragmentCoordinator0;
import e.c.c.BaseViewState1;
import n.n.c.DefaultConstructorMarker;
import n.n.c.Intrinsics;

/* compiled from: ProfileFillingContainerFragmentViewState.kt */
public abstract class ProfileFillingContainerFragmentViewState1 extends BaseViewState1 {

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class a extends ProfileFillingContainerFragmentViewState1 {
        public final boolean a;

        public a(boolean z) {
            super(null);
            this.a = z;
        }
    }

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class b extends ProfileFillingContainerFragmentViewState1 {
        public final IStepsNestedFragmentCoordinator0 a;

        /* JADX INFO: super call moved to the top of the method (can break code semantics) */
        public b(IStepsNestedFragmentCoordinator0 iStepsNestedFragmentCoordinator0) {
            super(null);
            if (iStepsNestedFragmentCoordinator0 != null) {
                this.a = iStepsNestedFragmentCoordinator0;
                return;
            }
            Intrinsics.a("stepInfo");
            throw null;
        }
    }

    /* compiled from: ProfileFillingContainerFragmentViewState.kt */
    public static final class c extends ProfileFillingContainerFragmentViewState1 {
        public c() {
            super(null);
        }
    }

    public /* synthetic */ ProfileFillingContainerFragmentViewState1(DefaultConstructorMarker defaultConstructorMarker) {
    }
}
