package e.a.b.f.c;

import e.a.a.a.e.r.NestedFragmentCiceroneHolder;
import e.a.a.a.e.r.d;
import e.a.b.h.b.g.AddProfileFragmentNavigator;
import e.a.b.h.c.IStepScreensConfig;
import e.a.b.h.c.a;
import j.c.a.a.c.n.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

/* compiled from: AddProfileStepsNavigationModule_ProvideAddProfileFragmentNavigator$app_prodReleaseFactory */
public final class b implements Factory<e.a.b.h.c.b> {
    public final AddProfileStepsNavigationModule a;
    public final Provider<d> b;
    public final Provider<a> c;

    public b(a aVar, Provider<d> provider, Provider<a> provider2) {
        this.a = aVar;
        this.b = provider;
        this.c = provider2;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.h.b.g.AddProfileFragmentNavigator, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        AddProfileStepsNavigationModule addProfileStepsNavigationModule = this.a;
        NestedFragmentCiceroneHolder nestedFragmentCiceroneHolder = this.b.get();
        IStepScreensConfig iStepScreensConfig = this.c.get();
        if (addProfileStepsNavigationModule == null) {
            throw null;
        } else if (nestedFragmentCiceroneHolder == null) {
            Intrinsics.a("nestedFragmentCiceroneHolder");
            throw null;
        } else if (iStepScreensConfig != null) {
            AddProfileFragmentNavigator addProfileFragmentNavigator = new AddProfileFragmentNavigator(nestedFragmentCiceroneHolder, iStepScreensConfig);
            c.a((Object) addProfileFragmentNavigator, "Cannot return null from a non-@Nullable @Provides method");
            return addProfileFragmentNavigator;
        } else {
            Intrinsics.a("screensConfig");
            throw null;
        }
    }
}
