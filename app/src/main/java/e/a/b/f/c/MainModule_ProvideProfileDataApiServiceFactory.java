package e.a.b.f.c;

import e.a.a.g.a.g.IApiConfig2;
import e.a.b.d.a.a.ProfileDataApiService;
import e.a.b.d.a.a.a;
import e.c.b.RestServiceFactory;
import e.c.b.RetrofitFactory;
import e.c.b.h;
import j.c.a.a.c.n.c;
import k.a.Factory;
import m.a.Provider;
import n.n.c.Intrinsics;

public final class MainModule_ProvideProfileDataApiServiceFactory implements Factory<a> {
    public final MainModule a;
    public final Provider<h> b;

    public MainModule_ProvideProfileDataApiServiceFactory(l lVar, Provider<h> provider) {
        this.a = lVar;
        this.b = provider;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.b.d.a.a.ProfileDataApiService, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public Object get() {
        MainModule mainModule = this.a;
        RetrofitFactory retrofitFactory = this.b.get();
        if (mainModule == null) {
            throw null;
        } else if (retrofitFactory != null) {
            ProfileDataApiService profileDataApiService = (ProfileDataApiService) new RestServiceFactory(retrofitFactory, IApiConfig2.a, ProfileDataApiService.class).a();
            c.a((Object) profileDataApiService, "Cannot return null from a non-@Nullable @Provides method");
            return profileDataApiService;
        } else {
            Intrinsics.a("retrofitFactory");
            throw null;
        }
    }
}
