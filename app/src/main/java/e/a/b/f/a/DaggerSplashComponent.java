package e.a.b.f.a;

import e.a.a.a.e.o.ITopLevelCoordinator;
import e.a.a.a.e.o.c;
import e.a.a.a.e.r.AppCiceroneHolder;
import e.a.a.a.e.r.FragmentCiceroneHolder;
import e.a.a.i.b.CoreComponent;
import e.a.a.i.d.VmFactory_Factory;
import e.a.a.i.e.NavigationWrapper;
import e.a.b.h.d.SplashActivityVm;
import e.a.b.h.d.SplashActivityVm_Factory;
import e.c.d.b.c.VmFactoryWrapper;
import i.o.v;
import i.o.w;
import java.util.LinkedHashMap;
import java.util.Map;
import k.a.DoubleCheck;
import k.a.MapProviderFactory;
import m.a.Provider;

public final class DaggerSplashComponent implements SplashComponent {
    public final CoreComponent a;
    public Provider<c> b;
    public Provider<e.a.b.h.d.a> c;
    public Provider<Map<Class<? extends v>, Provider<v>>> d;

    /* renamed from: e  reason: collision with root package name */
    public Provider<e.a.a.i.d.a> f667e;

    /* renamed from: f  reason: collision with root package name */
    public Provider<w.b> f668f;

    public static class b implements Provider<c> {
        public final CoreComponent a;

        public b(CoreComponent coreComponent) {
            this.a = coreComponent;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
         arg types: [e.a.a.a.e.o.ITopLevelCoordinator, java.lang.String]
         candidates:
          j.c.a.a.c.n.c.a(android.content.Context, int):float
          j.c.a.a.c.n.c.a(android.view.View, int):int
          j.c.a.a.c.n.c.a(byte[], int):long
          j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
          j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
          j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
          j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
          j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
          j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
          j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
          j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
          j.c.a.a.c.n.c.a(android.view.View, float):void
          j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
          j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
          j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
          j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
          j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
          j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
        public Object get() {
            ITopLevelCoordinator d = this.a.d();
            j.c.a.a.c.n.c.a((Object) d, "Cannot return null from a non-@Nullable component method");
            return d;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [java.lang.Class<e.a.b.h.d.SplashActivityVm>, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [m.a.Provider<e.a.b.h.d.a>, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public /* synthetic */ DaggerSplashComponent(CoreComponent coreComponent, a aVar) {
        this.a = coreComponent;
        b bVar = new b(coreComponent);
        this.b = bVar;
        this.c = new SplashActivityVm_Factory(bVar);
        MapProviderFactory.b a2 = MapProviderFactory.a(1);
        Class<SplashActivityVm> cls = SplashActivityVm.class;
        Provider<e.a.b.h.d.a> provider = this.c;
        LinkedHashMap<K, Provider<V>> linkedHashMap = a2.a;
        j.c.a.a.c.n.c.a((Object) cls, "key");
        j.c.a.a.c.n.c.a((Object) provider, "provider");
        linkedHashMap.put(cls, provider);
        MapProviderFactory a3 = a2.a();
        this.d = a3;
        VmFactory_Factory vmFactory_Factory = new VmFactory_Factory(a3);
        this.f667e = vmFactory_Factory;
        this.f668f = DoubleCheck.a(vmFactory_Factory);
    }

    public void a(VmFactoryWrapper vmFactoryWrapper) {
        vmFactoryWrapper.a = this.f668f.get();
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.FragmentCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T
     arg types: [e.a.a.a.e.r.AppCiceroneHolder, java.lang.String]
     candidates:
      j.c.a.a.c.n.c.a(android.content.Context, int):float
      j.c.a.a.c.n.c.a(android.view.View, int):int
      j.c.a.a.c.n.c.a(byte[], int):long
      j.c.a.a.c.n.c.a(int, android.graphics.PorterDuff$Mode):android.graphics.PorterDuff$Mode
      j.c.a.a.c.n.c.a(java.lang.String, java.lang.String):j.c.c.e.d<?>
      j.c.a.a.c.n.c.a(j.c.b.a.c0.EllipticCurves, byte[]):java.security.interfaces.ECPrivateKey
      j.c.a.a.c.n.c.a(int, int):java.text.DateFormat
      j.c.a.a.c.n.c.a(android.database.sqlite.SQLiteDatabase, java.lang.String):java.util.Set<java.lang.String>
      j.c.a.a.c.n.c.a(l.b.s.Disposable, l.b.s.CompositeDisposable):l.b.s.Disposable
      j.c.a.a.c.n.c.a(android.animation.AnimatorSet, java.util.List<android.animation.Animator>):void
      j.c.a.a.c.n.c.a(android.os.Bundle, java.lang.Object):void
      j.c.a.a.c.n.c.a(android.view.View, float):void
      j.c.a.a.c.n.c.a(android.view.View, j.c.a.b.g0.MaterialShapeDrawable):void
      j.c.a.a.c.n.c.a(j.c.a.a.g.a.n3, android.database.sqlite.SQLiteDatabase):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.Class):void
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):void
      j.c.a.a.c.n.c.a(java.lang.String, java.util.concurrent.ExecutorService):void
      j.c.a.a.c.n.c.a(java.security.spec.ECPoint, java.security.spec.EllipticCurve):void
      j.c.a.a.c.n.c.a(byte[], byte[]):byte[]
      j.c.a.a.c.n.c.a(java.lang.Object, java.lang.String):T */
    public void a(NavigationWrapper navigationWrapper) {
        FragmentCiceroneHolder m2 = this.a.m();
        j.c.a.a.c.n.c.a((Object) m2, "Cannot return null from a non-@Nullable component method");
        navigationWrapper.a = m2;
        AppCiceroneHolder a2 = this.a.a();
        j.c.a.a.c.n.c.a((Object) a2, "Cannot return null from a non-@Nullable component method");
        navigationWrapper.b = a2;
    }
}
