package e.b.a;

import e.b.a.h.Command;
import e.b.a.h.c;
import java.util.LinkedList;
import java.util.Queue;

public class CommandBuffer {
    public Navigator a;
    public Queue<c[]> b = new LinkedList();

    public void a(Navigator navigator) {
        this.a = navigator;
        while (!this.b.isEmpty() && navigator != null) {
            Command[] poll = this.b.poll();
            Navigator navigator2 = this.a;
            if (navigator2 != null) {
                navigator2.a(poll);
            } else {
                this.b.add(poll);
            }
        }
    }

    public void a(Command[] commandArr) {
        Navigator navigator = this.a;
        if (navigator != null) {
            navigator.a(commandArr);
        } else {
            this.b.add(commandArr);
        }
    }
}
