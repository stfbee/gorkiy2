package e.c.c;

import l.b.t.Function;
import n.n.c.Intrinsics;

/* compiled from: ViewExtensions.kt */
public final class ViewExtensions<T, R> implements Function<T, R> {
    public static final ViewExtensions a = new ViewExtensions();

    public Object a(Object obj) {
        CharSequence charSequence = (CharSequence) obj;
        if (charSequence != null) {
            return charSequence.toString();
        }
        Intrinsics.a("it");
        throw null;
    }
}
