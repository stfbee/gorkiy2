package e.c.d.a;

import j.e.b.Relay;
import l.b.s.Disposable;
import l.b.s.b;
import l.b.t.Consumer;
import n.g;
import n.n.b.Functions0;
import n.n.c.Intrinsics;

/* compiled from: Bind.kt */
public abstract class Bind<T> {
    public abstract Relay<T> a();

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final b a(Consumer<? super Boolean> consumer) {
        if (consumer != null) {
            Disposable a = a().a((Consumer) consumer);
            Intrinsics.a((Object) a, "relay.subscribe(onNext)");
            return a;
        }
        Intrinsics.a("onNext");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.s.Disposable, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final b a(Functions0<? super Boolean, g> functions0) {
        if (functions0 != null) {
            Disposable a = a().a((Consumer) new Bind0(functions0));
            Intrinsics.a((Object) a, "relay.subscribe(onNext)");
            return a;
        }
        Intrinsics.a("onNext");
        throw null;
    }

    public final void a(Boolean bool) {
        a().a(bool);
    }
}
