package e.c.a.b;

import android.content.DialogInterface;
import n.n.b.Functions;

/* compiled from: ContextExtensions.kt */
public final class ContextExtensions implements DialogInterface.OnCancelListener {
    public final /* synthetic */ Functions b;

    public ContextExtensions(Functions functions) {
        this.b = functions;
    }

    public final void onCancel(DialogInterface dialogInterface) {
        this.b.b();
    }
}
