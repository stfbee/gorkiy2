package e.c.a.c.a.b;

import android.content.Context;
import android.content.SharedPreferences;
import android.security.keystore.KeyGenParameterSpec;
import com.crashlytics.android.core.DefaultAppMeasurementEventListenerRegistrar;
import i.u.a.EncryptedSharedPreferences;
import i.u.a.MasterKeys;
import j.a.a.a.outline;
import j.c.b.a.KeysetHandle;
import j.c.b.a.t.AeadFactory;
import j.c.b.a.u.TinkConfig;
import j.c.b.a.v.DeterministicAeadFactory;
import j.c.b.a.x.a.AndroidKeysetManager;
import java.security.KeyStore;
import java.util.Arrays;
import javax.crypto.KeyGenerator;
import n.n.c.Intrinsics;

/* compiled from: EncryptedPreferencesWrapper.kt */
public final class EncryptedPreferencesWrapper implements ISecuredPreferencesWrapper {
    public final Context a;

    public EncryptedPreferencesWrapper(Context context) {
        if (context != null) {
            this.a = context;
        } else {
            Intrinsics.a("context");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.lang.String, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [i.u.a.EncryptedSharedPreferences, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public SharedPreferences a(String str) {
        if (str != null) {
            KeyGenParameterSpec keyGenParameterSpec = MasterKeys.a;
            if (keyGenParameterSpec.getKeySize() != 256) {
                StringBuilder a2 = outline.a("invalid key size, want 256 bits got ");
                a2.append(keyGenParameterSpec.getKeySize());
                a2.append(" bits");
                throw new IllegalArgumentException(a2.toString());
            } else if (keyGenParameterSpec.getBlockModes().equals(new String[]{"GCM"})) {
                StringBuilder a3 = outline.a("invalid block mode, want GCM got ");
                a3.append(Arrays.toString(keyGenParameterSpec.getBlockModes()));
                throw new IllegalArgumentException(a3.toString());
            } else if (keyGenParameterSpec.getPurposes() != 3) {
                StringBuilder a4 = outline.a("invalid purposes mode, want PURPOSE_ENCRYPT | PURPOSE_DECRYPT got ");
                a4.append(keyGenParameterSpec.getPurposes());
                throw new IllegalArgumentException(a4.toString());
            } else if (!keyGenParameterSpec.getEncryptionPaddings().equals(new String[]{"NoPadding"})) {
                String keystoreAlias = keyGenParameterSpec.getKeystoreAlias();
                KeyStore instance = KeyStore.getInstance("AndroidKeyStore");
                instance.load(null);
                if (!instance.containsAlias(keystoreAlias)) {
                    KeyGenerator instance2 = KeyGenerator.getInstance("AES", "AndroidKeyStore");
                    instance2.init(keyGenParameterSpec);
                    instance2.generateKey();
                }
                String keystoreAlias2 = keyGenParameterSpec.getKeystoreAlias();
                Intrinsics.a((Object) keystoreAlias2, "MasterKeys.getOrCreate(MasterKeys.AES256_GCM_SPEC)");
                Context context = this.a;
                EncryptedSharedPreferences.c cVar = EncryptedSharedPreferences.c.AES256_SIV;
                EncryptedSharedPreferences.d dVar = EncryptedSharedPreferences.d.AES256_GCM;
                TinkConfig.a();
                AndroidKeysetManager.b bVar = new AndroidKeysetManager.b();
                bVar.f2387e = cVar.mDeterministicAeadKeyTemplate;
                bVar.a(context, "__androidx_security_crypto_encrypted_prefs_key_keyset__", str);
                String str2 = "android-keystore://" + keystoreAlias2;
                if (str2.startsWith("android-keystore://")) {
                    bVar.c = str2;
                    KeysetHandle a5 = new AndroidKeysetManager(bVar, null).a();
                    AndroidKeysetManager.b bVar2 = new AndroidKeysetManager.b();
                    bVar2.f2387e = dVar.mAeadKeyTemplate;
                    bVar2.a(context, "__androidx_security_crypto_encrypted_prefs_value_keyset__", str);
                    String str3 = "android-keystore://" + keystoreAlias2;
                    if (str3.startsWith("android-keystore://")) {
                        bVar2.c = str3;
                        KeysetHandle a6 = new AndroidKeysetManager(bVar2, null).a();
                        String str4 = str;
                        EncryptedSharedPreferences encryptedSharedPreferences = new EncryptedSharedPreferences(str4, keystoreAlias2, context.getSharedPreferences(str, 0), AeadFactory.a(a6), DeterministicAeadFactory.a(a5));
                        Intrinsics.a((Object) encryptedSharedPreferences, "EncryptedSharedPreferenc…heme.AES256_GCM\n        )");
                        return encryptedSharedPreferences;
                    }
                    throw new IllegalArgumentException("key URI must start with android-keystore://");
                }
                throw new IllegalArgumentException("key URI must start with android-keystore://");
            } else {
                StringBuilder a7 = outline.a("invalid padding mode, want NoPadding got ");
                a7.append(Arrays.toString(keyGenParameterSpec.getEncryptionPaddings()));
                throw new IllegalArgumentException(a7.toString());
            }
        } else {
            Intrinsics.a(DefaultAppMeasurementEventListenerRegistrar.NAME);
            throw null;
        }
    }
}
