package e.c.b;

import e.c.b.j.IApiConfigRepository;
import e.c.b.j.b;
import java.util.Map;
import l.b.Observable;
import l.b.t.Consumer;
import l.b.t.Function;
import l.b.x.BehaviorSubject;
import n.Unit;
import n.g;
import n.n.c.Intrinsics;

/* compiled from: RestServiceFactory.kt */
public class RestServiceFactory<T> {
    public T a;
    public final BehaviorSubject<Boolean> b;
    public final RetrofitFactory c;
    public final IApiConfigRepository d;

    /* renamed from: e  reason: collision with root package name */
    public final Class<T> f713e;

    /* compiled from: RestServiceFactory.kt */
    public static final class a<T> implements Consumer<g> {
        public final /* synthetic */ RestServiceFactory b;

        public a(RestServiceFactory restServiceFactory) {
            this.b = restServiceFactory;
        }

        /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
         method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
         arg types: [l.b.Observable<R>, java.lang.String]
         candidates:
          n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
          n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
        public void a(Object obj) {
            Unit unit = (Unit) obj;
            RestServiceFactory restServiceFactory = this.b;
            restServiceFactory.b.b((Map<b, String>) false);
            RetrofitFactory retrofitFactory = restServiceFactory.c;
            IApiConfigRepository iApiConfigRepository = restServiceFactory.d;
            if (iApiConfigRepository != null) {
                Observable<R> c = retrofitFactory.d.a(RetrofitFactory2.b).c((Function) new RetrofitFactory3(retrofitFactory, iApiConfigRepository));
                Intrinsics.a((Object) c, "retrofitsReadySubject\n  …$apiLabel\")\n            }");
                c.a(new RestServiceFactory0(restServiceFactory), RestServiceFactory1.b);
                return;
            }
            Intrinsics.a("apiLabel");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.x.BehaviorSubject<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public RestServiceFactory(h hVar, b bVar, Class<T> cls) {
        if (hVar == null) {
            Intrinsics.a("retrofitFactory");
            throw null;
        } else if (bVar == null) {
            Intrinsics.a("apiLabel");
            throw null;
        } else if (cls != null) {
            this.c = hVar;
            this.d = bVar;
            this.f713e = cls;
            BehaviorSubject<Boolean> behaviorSubject = new BehaviorSubject<>();
            Intrinsics.a((Object) behaviorSubject, "BehaviorSubject.create<Boolean>()");
            this.b = behaviorSubject;
            behaviorSubject.b((Map<b, String>) false);
            this.c.c.a(new a(this));
        } else {
            Intrinsics.a("clazz");
            throw null;
        }
    }

    public final T a() {
        T t2 = this.a;
        if (t2 != null) {
            return t2;
        }
        Intrinsics.b("service");
        throw null;
    }
}
