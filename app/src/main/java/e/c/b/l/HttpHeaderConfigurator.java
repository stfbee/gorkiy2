package e.c.b.l;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import n.Tuples;
import n.i.Collections;
import n.n.c.Intrinsics;

/* compiled from: HttpHeaderConfigurator.kt */
public class HttpHeaderConfigurator {
    public final List<Tuples<String, String>> a;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean
     arg types: [java.util.ArrayList, n.Tuples<java.lang.String, java.lang.String>[]]
     candidates:
      n.i.Collections.a(int, int):int
      n.i.Collections.a(long, int):int
      n.i.Collections.a(long, long):int
      n.i.Collections.a(java.lang.Comparable, java.lang.Comparable):int
      n.i.Collections.a(java.lang.Iterable, int):int
      n.i.Collections.a(byte[], int):int
      n.i.Collections.a(java.lang.Exception, n.l.Continuation<?>):java.lang.Object
      n.i.Collections.a(java.lang.Object, java.lang.String):T
      n.i.Collections.a(r.Call, n.l.Continuation):java.lang.Object
      n.i.Collections.a(java.lang.String, int):java.lang.String
      n.i.Collections.a(l.b.Observable, e.c.d.a.ViewAction):l.b.s.b
      n.i.Collections.a(n.q.Sequence, n.n.b.Functions0):n.q.Sequence<R>
      n.i.Collections.a(android.view.View, boolean):void
      n.i.Collections.a(android.widget.CompoundButton, boolean):void
      n.i.Collections.a(java.io.Closeable, java.lang.Throwable):void
      n.i.Collections.a(java.lang.Object, java.lang.Object):boolean
      n.i.Collections.a(java.util.Collection, java.lang.Iterable):boolean
      n.i.Collections.a(android.content.Context, android.net.Uri):byte[]
      n.i.Collections.a(java.util.Collection, java.lang.Object[]):boolean */
    public HttpHeaderConfigurator(Tuples<String, String>... tuplesArr) {
        if (tuplesArr != null) {
            ArrayList arrayList = new ArrayList();
            this.a = arrayList;
            Collections.a((Collection) arrayList, (Object[]) tuplesArr);
            return;
        }
        Intrinsics.a("staticHeaders");
        throw null;
    }
}
