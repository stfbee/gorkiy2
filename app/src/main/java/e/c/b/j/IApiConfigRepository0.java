package e.c.b.j;

import java.util.Map;
import l.b.Observable;

/* compiled from: IApiConfigRepository.kt */
public interface IApiConfigRepository0 {
    Map<b, String> a();

    Observable<Boolean> b();

    Observable<Map<b, String>> c();
}
