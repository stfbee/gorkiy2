package e.c.b;

import e.c.b.j.IApiConfigRepository;
import j.a.a.a.outline;
import l.b.t.Function;
import n.n.c.Intrinsics;
import r.Retrofit0;

/* compiled from: RetrofitFactory.kt */
public final class RetrofitFactory3<T, R> implements Function<T, R> {
    public final /* synthetic */ RetrofitFactory a;
    public final /* synthetic */ IApiConfigRepository b;

    public RetrofitFactory3(RetrofitFactory retrofitFactory, IApiConfigRepository iApiConfigRepository) {
        this.a = retrofitFactory;
        this.b = iApiConfigRepository;
    }

    public Object a(Object obj) {
        if (((Boolean) obj) != null) {
            Retrofit0 retrofit0 = this.a.a.get(this.b);
            if (retrofit0 != null) {
                return retrofit0;
            }
            StringBuilder a2 = outline.a("No Retrofit for label ");
            a2.append(this.b);
            throw new IllegalArgumentException(a2.toString());
        }
        Intrinsics.a("it");
        throw null;
    }
}
