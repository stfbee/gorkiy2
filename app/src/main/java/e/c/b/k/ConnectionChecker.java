package e.c.b.k;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.provider.Settings;
import n.n.c.Intrinsics;
import ru.waveaccess.waandroidnetwork.Exceptions;
import ru.waveaccess.waandroidnetwork.Exceptions0;

/* compiled from: ConnectionChecker.kt */
public final class ConnectionChecker implements IConnectionChecker {
    public final ConnectivityManager a;
    public final Context b;

    public ConnectionChecker(Context context) {
        if (context != null) {
            this.b = context;
            this.a = (ConnectivityManager) context.getSystemService("connectivity");
            return;
        }
        Intrinsics.a("context");
        throw null;
    }

    public void a() {
        NetworkInfo activeNetworkInfo;
        ConnectivityManager connectivityManager = this.a;
        boolean z = true;
        if (connectivityManager != null && (activeNetworkInfo = connectivityManager.getActiveNetworkInfo()) != null && activeNetworkInfo.isAvailable() && activeNetworkInfo.isConnected()) {
            if (Settings.Global.getInt(this.b.getContentResolver(), "airplane_mode_on", 0) == 0) {
                z = false;
            }
            if (z) {
                throw new Exceptions();
            }
            return;
        }
        throw new Exceptions0();
    }
}
