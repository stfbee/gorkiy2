package e.c.b;

import android.content.Context;
import e.c.b.j.IApiConfigRepository0;
import e.c.b.j.b;
import e.c.b.k.IConnectionChecker;
import e.c.b.l.HttpHeaderConfigurator;
import e.c.b.m.InterceptorsConfigurator;
import j.c.d.Gson;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.TimeUnit;
import l.b.Scheduler;
import l.b.w.Schedulers;
import l.b.x.BehaviorSubject;
import n.g;
import n.n.c.Intrinsics;
import o.Authenticator;
import o.Cache;
import o.HttpUrl;
import o.Interceptor;
import o.OkHttpClient;
import o.b0;
import o.m0.Util;
import r.BuiltInConverters;
import r.Platform;
import r.Retrofit0;
import r.Utils;
import r.e0;
import r.j0.a.RxJava2CallAdapterFactory;
import r.k0.a.GsonConverterFactory;
import r.k0.b.ScalarsConverterFactory;

/* compiled from: RetrofitFactory.kt */
public class RetrofitFactory {
    public final HashMap<b, e0> a;
    public final BehaviorSubject<b0> b;
    public final BehaviorSubject<g> c;
    public final BehaviorSubject<Boolean> d;

    /* renamed from: e  reason: collision with root package name */
    public final IApiConfigRepository0 f714e;

    /* renamed from: f  reason: collision with root package name */
    public final Context f715f;
    public final Gson g;
    public final IConnectionChecker h;

    /* renamed from: i  reason: collision with root package name */
    public final HttpHeaderConfigurator f716i;

    /* renamed from: j  reason: collision with root package name */
    public final InterceptorsConfigurator f717j;

    /* renamed from: k  reason: collision with root package name */
    public final Authenticator f718k;

    /* renamed from: l  reason: collision with root package name */
    public final long f719l;

    /* renamed from: m  reason: collision with root package name */
    public final long f720m;

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.x.BehaviorSubject<o.b0>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.x.BehaviorSubject<n.g>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [l.b.x.BehaviorSubject<java.lang.Boolean>, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public /* synthetic */ RetrofitFactory(IApiConfigRepository0 iApiConfigRepository0, Context context, Gson gson, IConnectionChecker iConnectionChecker, HttpHeaderConfigurator httpHeaderConfigurator, InterceptorsConfigurator interceptorsConfigurator, Authenticator authenticator, long j2, long j3, int i2) {
        httpHeaderConfigurator = (i2 & 16) != 0 ? null : httpHeaderConfigurator;
        interceptorsConfigurator = (i2 & 32) != 0 ? new InterceptorsConfigurator(new Interceptor[0]) : interceptorsConfigurator;
        authenticator = (i2 & 64) != 0 ? null : authenticator;
        j2 = (i2 & 128) != 0 ? 15 : j2;
        j3 = (i2 & 256) != 0 ? 10485760 : j3;
        if (iApiConfigRepository0 == null) {
            Intrinsics.a("apiConfigRepository");
            throw null;
        } else if (context == null) {
            Intrinsics.a("context");
            throw null;
        } else if (gson == null) {
            Intrinsics.a("gson");
            throw null;
        } else if (iConnectionChecker == null) {
            Intrinsics.a("connectionChecker");
            throw null;
        } else if (interceptorsConfigurator != null) {
            this.f714e = iApiConfigRepository0;
            this.f715f = context;
            this.g = gson;
            this.h = iConnectionChecker;
            this.f716i = httpHeaderConfigurator;
            this.f717j = interceptorsConfigurator;
            this.f718k = authenticator;
            this.f719l = j2;
            this.f720m = j3;
            this.a = new HashMap<>();
            BehaviorSubject<b0> behaviorSubject = new BehaviorSubject<>();
            behaviorSubject.b((Map<b, String>) a());
            Intrinsics.a((Object) behaviorSubject, "BehaviorSubject.create<O…OkHttpClient())\n        }");
            this.b = behaviorSubject;
            BehaviorSubject<g> behaviorSubject2 = new BehaviorSubject<>();
            Intrinsics.a((Object) behaviorSubject2, "BehaviorSubject.create<Unit>()");
            this.c = behaviorSubject2;
            BehaviorSubject<Boolean> behaviorSubject3 = new BehaviorSubject<>();
            behaviorSubject3.b((Map<b, String>) false);
            Intrinsics.a((Object) behaviorSubject3, "BehaviorSubject.create<B…).apply { onNext(false) }");
            this.d = behaviorSubject3;
            this.f714e.c().a(new RetrofitFactory0(this));
            this.f714e.b().a(new RetrofitFactory1(this));
        } else {
            Intrinsics.a("interceptorsConfigurator");
            throw null;
        }
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [java.io.File, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final OkHttpClient a() {
        long j2 = this.f719l;
        long j3 = this.f720m;
        IConnectionChecker iConnectionChecker = this.h;
        Context context = this.f715f;
        HttpHeaderConfigurator httpHeaderConfigurator = this.f716i;
        InterceptorsConfigurator interceptorsConfigurator = this.f717j;
        Authenticator authenticator = this.f718k;
        if (iConnectionChecker != null) {
            OkHttpClient.a aVar = new OkHttpClient.a();
            TimeUnit timeUnit = TimeUnit.SECONDS;
            if (timeUnit != null) {
                aVar.f2892s = Util.a("timeout", j2, timeUnit);
                TimeUnit timeUnit2 = TimeUnit.SECONDS;
                if (timeUnit2 != null) {
                    aVar.f2893t = Util.a("timeout", j2, timeUnit2);
                    TimeUnit timeUnit3 = TimeUnit.SECONDS;
                    if (timeUnit3 != null) {
                        aVar.u = Util.a("timeout", j2, timeUnit3);
                        Interceptor.b bVar = Interceptor.a;
                        aVar.c.add(new e.c.b.n.Interceptor(iConnectionChecker, httpHeaderConfigurator));
                        File cacheDir = context.getCacheDir();
                        Intrinsics.a((Object) cacheDir, "context.cacheDir");
                        aVar.f2884k = new Cache(cacheDir, j3);
                        Iterator<T> it = interceptorsConfigurator.a.iterator();
                        while (it.hasNext()) {
                            Interceptor interceptor = (Interceptor) it.next();
                            if (interceptor != null) {
                                aVar.c.add(interceptor);
                            } else {
                                Intrinsics.a("interceptor");
                                throw null;
                            }
                        }
                        Iterator<T> it2 = interceptorsConfigurator.b.iterator();
                        while (it2.hasNext()) {
                            Interceptor interceptor2 = (Interceptor) it2.next();
                            if (interceptor2 != null) {
                                aVar.d.add(interceptor2);
                            } else {
                                Intrinsics.a("interceptor");
                                throw null;
                            }
                        }
                        if (authenticator != null) {
                            aVar.g = authenticator;
                        }
                        return new OkHttpClient(aVar);
                    }
                    Intrinsics.a("unit");
                    throw null;
                }
                Intrinsics.a("unit");
                throw null;
            }
            Intrinsics.a("unit");
            throw null;
        }
        Intrinsics.a("connectionChecker");
        throw null;
    }

    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [o.OkHttpClient, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    /* JADX DEBUG: Failed to find minimal casts for resolve overloaded methods, cast all args instead
     method: n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void
     arg types: [r.Retrofit0, java.lang.String]
     candidates:
      n.n.c.Intrinsics.a(java.lang.Throwable, java.lang.String):T
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.Object):boolean
      n.n.c.Intrinsics.a(java.lang.Object, java.lang.String):void */
    public final void a(Map<b, String> map) {
        for (Map.Entry next : map.entrySet()) {
            Gson gson = this.g;
            OkHttpClient c2 = this.b.c();
            if (c2 != null) {
                Intrinsics.a((Object) c2, "httpClientSubject.value!!");
                OkHttpClient okHttpClient = c2;
                String str = (String) next.getValue();
                if (str == null) {
                    Intrinsics.a("baseUrl");
                    throw null;
                } else if (gson != null) {
                    Platform platform = Platform.a;
                    ArrayList arrayList = new ArrayList();
                    ArrayList arrayList2 = new ArrayList();
                    Utils.a(str, "baseUrl == null");
                    HttpUrl b2 = HttpUrl.f2850l.b(str);
                    Utils.a(b2, "baseUrl == null");
                    List<String> list = b2.g;
                    if ("".equals(list.get(list.size() - 1))) {
                        Utils.a(okHttpClient, "client == null");
                        Utils.a(okHttpClient, "factory == null");
                        ScalarsConverterFactory scalarsConverterFactory = new ScalarsConverterFactory();
                        Utils.a(scalarsConverterFactory, "factory == null");
                        arrayList.add(scalarsConverterFactory);
                        GsonConverterFactory gsonConverterFactory = new GsonConverterFactory(gson);
                        Utils.a(gsonConverterFactory, "factory == null");
                        arrayList.add(gsonConverterFactory);
                        Scheduler scheduler = Schedulers.b;
                        if (scheduler != null) {
                            RxJava2CallAdapterFactory rxJava2CallAdapterFactory = new RxJava2CallAdapterFactory(scheduler, false);
                            Utils.a(rxJava2CallAdapterFactory, "factory == null");
                            arrayList2.add(rxJava2CallAdapterFactory);
                            Executor a2 = platform.a();
                            ArrayList arrayList3 = new ArrayList(arrayList2);
                            arrayList3.addAll(platform.a(a2));
                            ArrayList arrayList4 = new ArrayList(platform.c() + arrayList.size() + 1);
                            arrayList4.add(new BuiltInConverters());
                            arrayList4.addAll(arrayList);
                            arrayList4.addAll(platform.b());
                            Retrofit0 retrofit0 = new Retrofit0(okHttpClient, b2, Collections.unmodifiableList(arrayList4), Collections.unmodifiableList(arrayList3), a2, false);
                            Intrinsics.a((Object) retrofit0, "Retrofit.Builder()\n     …s.io()))\n        .build()");
                            this.a.put(next.getKey(), retrofit0);
                        } else {
                            throw new NullPointerException("scheduler == null");
                        }
                    } else {
                        throw new IllegalArgumentException("baseUrl must end in /: " + b2);
                    }
                } else {
                    Intrinsics.a("gson");
                    throw null;
                }
            } else {
                Intrinsics.a();
                throw null;
            }
        }
    }
}
